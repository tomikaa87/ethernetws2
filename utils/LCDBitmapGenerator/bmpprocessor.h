#ifndef BMPPROCESSOR_H
#define BMPPROCESSOR_H

#include <QObject>

typedef struct
{
    quint16 signature;
    quint32 sizeBytes;
    quint16 reserved1;
    quint16 reserved2;
    quint32 dataOffsetBytes;
    quint32 infoHeaderSizeBytes;
    quint32 imageWidth;
    quint32 imageHeight;
    quint16 numOfPlanes;
    quint16 imageBPP;
    quint32 compressionType;
    quint32 imageSizeBytes;
    quint32 hResPerMeter;
    quint32 vResPerMeter;
    quint32 numOfColors;
    quint32 numOfImportantColors;
}
WinBMPHeader;

const int FileOpenError = 1;
const int FileReadError = 2;

class BMPProcessor : public QObject
{
    Q_OBJECT
public:
    explicit BMPProcessor(QObject *parent = 0);

    int loadDataFromFile(const QString &fileName);
    WinBMPHeader * bitmapHeader();
    QByteArray bitmapImageData();

private:
    QByteArray bmpData;
};

#endif // BMPPROCESSOR_H
