#-------------------------------------------------
#
# Project created by QtCreator 2010-09-28T18:19:35
#
#-------------------------------------------------

QT       += core gui widgets

TARGET = LCDBitmapGenerator
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    bmpprocessor.cpp

HEADERS  += mainwindow.h \
    bmpprocessor.h

FORMS    += mainwindow.ui

RC_FILE = app.rc
