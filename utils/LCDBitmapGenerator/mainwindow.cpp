#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QFileDialog>
#include <QMessageBox>
#include <QImage>
#include <cmath>
#include <QDebug>
#include <QMimeData>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    connect(ui->actionOpen_image, SIGNAL(triggered()),
            this, SLOT(browseImageFile()));
    connect(ui->actionExit, SIGNAL(triggered()),
            this, SLOT(close()));
    connect(ui->btnSaveBinary, SIGNAL(clicked()),
            this, SLOT(saveBinaryFile()));
    connect(ui->btnGenerateCstyleArray, SIGNAL(clicked()),
            this, SLOT(generateCstyleArray()));
    connect(ui->actionClose_image, &QAction::triggered,
            this, &MainWindow::closeImageFile);

    setAcceptDrops(true);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::dropEvent(QDropEvent* event)
{
    if (event->mimeData()->hasUrls())
    {
        auto url = event->mimeData()->urls().first();
        if (url.isLocalFile())
        {
            event->accept();
            openImageFile(url.toLocalFile());
        }
    }
}

void MainWindow::dragEnterEvent(QDragEnterEvent* event)
{
    if (event->mimeData()->hasUrls())
        event->accept();
}

/*** Private slots ************************************************************/

void MainWindow::browseImageFile()
{
    auto&& imageFileName = QFileDialog::getOpenFileName(this,
        "Select image file", QDir::currentPath(), "*.bmp | *.png");

    openImageFile(imageFileName);
}

/**
 * Opens a custom image
 */
void MainWindow::openImageFile(const QString& fileName)
{
    if (m_imageFile.isOpen())
        m_imageFile.close();

    m_imageFile.setFileName(fileName);

    if (!m_imageFile.open(QIODevice::ReadOnly))
    {
        QMessageBox::warning(this, tr("Image file error"),
                    tr("Cannot open image file."));
        return;
    }

    m_imageFile.seek(0);

    QImage img;
    img.loadFromData(m_imageFile.readAll());

    if (img.isNull())
    {
        QMessageBox::warning(this, tr("Image file error"),
                             tr("Image file has an invalid format."));
        m_imageFile.close();
        return;
    }

    ui->imageFileDimLabel->setText(QString("%1x%2")
                                   .arg(img.width())
                                   .arg(img.height()));

    ui->imageFileNameLabel->setText(m_imageFile.fileName());
    ui->imageFileSizeLabel->setText(QString("%1 B")
                                    .arg(m_imageFile.size()));
}

void MainWindow::closeImageFile()
{
    if (m_imageFile.isOpen())
    {
        m_imageFile.close();

        ui->imageFileDimLabel->clear();
        ui->imageFileNameLabel->clear();
        ui->imageFileSizeLabel->clear();
    }
}

void MainWindow::generateCstyleArray()
{
    if (m_imageFile.isOpen())
    {
        m_imageFile.seek(0);

        QImage img;
        img.loadFromData(m_imageFile.readAll());

        // Generate C code
        QString src = "const unsigned char image";
        src.append(QString("[%1]").arg(ceil(img.width()*img.height()/8)));
        src.append(" =\r\n{");
        int count = 0, byteCount = 0;
        quint8 byte = 0;
        for (int y = 0; y < img.height(); y++)
        {
            for (int x = 0; x < img.width(); x++)
            {
                if (img.pixel(x, y) == QColor(Qt::black).rgb())
                {
                    //qDebug() << "Pixel (" << x << ";" << y << "):" << 1;
                    byte |= 1 << (7 - (count % 8));
                    //qDebug() << "Byte: " << QString("%1").arg(byte, 8, 2, QChar('0'));
                }
                else
                {
                    //qDebug() << "Pixel (" << x << ";" << y << "):" << 0;
                    //qDebug() << "Byte: " << QString("%1").arg(byte, 8, 2, QChar('0'));
                }

                count++;

                if (count % 8 == 0 && count > 0)
                {
                    if (byteCount % 16 == 0)
                        src.append("\r\n    ");

                    src.append(QString("0x%1").arg(byte, 2, 16, QChar('0')));

                    if (byteCount < (img.width()*img.height()/8) - 1)
                        src.append(", ");

                    byteCount++;
                    byte = 0;

                    //qDebug() << "****" << byteCount;
                }
            }
        }
        src.append(QString("\r\n}; // Bytes: %1").arg(byteCount));
        ui->sourceTextEdit->setPlainText(src);
    }
    else
        QMessageBox::warning(this, tr("Image file error"),
                             tr("No image file loaded."));
}

void MainWindow::saveBinaryFile()
{
    if (m_imageFile.isOpen())
    {
        m_imageFile.seek(0);

        QImage img;
        img.loadFromData(m_imageFile.readAll());

        QByteArray imageData;
        quint8 byte = 0;
        quint64 count = 0;
        for (int y = 0; y < img.height(); y++)
        {
            for (int x = 0; x < img.width(); x++)
            {
                if (img.pixel(x, y) == QColor(Qt::black).rgb())
                {
                    //qDebug() << "Pixel (" << x << ";" << y << "):" << 1;
                    byte |= 1 << (7 - (count % 8));
                    //qDebug() << "Byte: " << QString("%1").arg(byte, 8, 2, QChar('0'));
                }
                else
                {
                    //qDebug() << "Pixel (" << x << ";" << y << "):" << 0;
                    //qDebug() << "Byte: " << QString("%1").arg(byte, 8, 2, QChar('0'));
                }

                count++;

                if (count % 8 == 0 && count > 0)
                {
                    imageData.append(byte);
                    byte = 0;
                }
            }
        }

        QString binaryFileName = QFileDialog::getSaveFileName(this,
            tr("Select binary file location"), QDir::currentPath(), "*.bin");

        if (binaryFileName.isNull())
            return;

        QFile f(binaryFileName);
        if (!f.open(QIODevice::ReadWrite))
        {
            QMessageBox::warning(this, tr("Binary file error"),
                                 tr("Cannot open binary file for writing."));
            return;
        }

        qDebug() << "Writing" << imageData.length() << "Bytes of data...";

        f.write(imageData);
        f.flush();
        f.close();
    }
    else
        QMessageBox::warning(this, tr("Image file error"),
                             tr("No image file loaded."));
}
