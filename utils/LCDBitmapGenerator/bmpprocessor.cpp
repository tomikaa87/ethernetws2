#include "bmpprocessor.h"

#include <QFile>
#include <QDebug>

BMPProcessor::BMPProcessor(QObject *parent) :
    QObject(parent)
{

}

int BMPProcessor::loadDataFromFile(const QString &fileName)
{
    QFile imageFile;

    // Open file
    imageFile.setFileName(fileName);
    if (!imageFile.open(QIODevice::ReadOnly))
    {
        return FileOpenError;
    }

    // Read data
    bmpData = imageFile.readAll();
    if (bmpData.size() == 0)
    {
        return FileReadError;
    }

    return 0;
}

WinBMPHeader * BMPProcessor::bitmapHeader()
{
    // Return if bmpData is invalid
    if (bmpData.size() < sizeof(WinBMPHeader))
    {
        return NULL;
    }

    // Read BMP header data
    QByteArray bmpHeader = bmpData.left(sizeof(WinBMPHeader));
    WinBMPHeader * wbHeader = new WinBMPHeader;
    memcpy(wbHeader, bmpHeader.constData(), sizeof(WinBMPHeader));

    qDebug() << bmpHeader.toHex();

    for (int i = 0; i < sizeof(WinBMPHeader); i++)
    {
        qDebug("0x%02X",
                static_cast<quint8 *>(static_cast<void *>(wbHeader))[i]);
    }

    qDebug("0x%X", wbHeader->signature);
    qDebug("0x%X", wbHeader->sizeBytes);
    qDebug() << wbHeader->imageHeight;

    return wbHeader;
}

QByteArray BMPProcessor::bitmapImageData()
{
    if (bmpData.size() < sizeof(WinBMPHeader))
        return QByteArray();

    return bmpData.right(bmpData.length() - 62);
}

