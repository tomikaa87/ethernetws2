#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QFile>

namespace Ui {
    class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

protected:
    void dropEvent(QDropEvent* event) override;
    void dragEnterEvent(QDragEnterEvent *event) override;

private:
    Ui::MainWindow *ui;
    QFile m_imageFile;

private slots:
    void browseImageFile();
    void openImageFile(const QString& fileName);
    void closeImageFile();
    void generateCstyleArray();
    void saveBinaryFile();
};

#endif // MAINWINDOW_H
