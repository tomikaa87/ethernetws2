#include "MainWindow.h"
#include "ui_MainWindow.h"
#include "BaudRateCalculator.h"

#include <QMessageBox>
#include <QDebug>

MainWindow::MainWindow(SettingsProvider *sp, QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    m_sp(sp)
{
    setWindowFlags(Qt::Window | Qt::WindowCloseButtonHint |
                   Qt::WindowMinimizeButtonHint);

    ui->setupUi(this);

    ui->aboutLabel->setEnabled(false);

    connect(ui->calculateButton, SIGNAL(clicked()),
            this, SLOT(calculateButtonClicked()));

    ui->baudRateSpin->setValue(m_sp->baudRate());
    ui->brg16CheckBox->setChecked(m_sp->brg16State());
    ui->brghCheckBox->setChecked(m_sp->brghState());
    ui->foscSpin->setValue(m_sp->fosc());
    ui->freqUnitCombo->setCurrentIndex(m_sp->foscUnitIndex());

    restoreGeometry(m_sp->mainWindowGeometry());
}

MainWindow::~MainWindow()
{
    m_sp->setBaudRate(ui->baudRateSpin->value());
    m_sp->setBrg16State(ui->brg16CheckBox->isChecked());
    m_sp->setBrghState(ui->brghCheckBox->isChecked());
    m_sp->setFosc(ui->foscSpin->value());
    m_sp->setFostUnitIndex(ui->freqUnitCombo->currentIndex());

    delete ui;
}

void MainWindow::closeEvent(QCloseEvent *)
{
    m_sp->setMainWindowGeometry(saveGeometry());
}

void MainWindow::calculateButtonClicked()
{
    int fosc = ui->foscSpin->value();
    switch (ui->freqUnitCombo->currentIndex()) {
    case 1:
        fosc *= 1000;
        break;
    case 2:
        fosc *= 1000000;
        break;
    default:
        break;
    }

    try {
        BaudRateCalculator::SerialRegisterValue srv =
                BaudRateCalculator::calculateRegisters(ui->baudRateSpin->value(),
                        ui->brg16CheckBox->isChecked(),
                        ui->brghCheckBox->isChecked(), fosc);
        ui->errorEdit->setText(QString::number(srv.error * 100) + "%");
        ui->spbrg16Edit->setText(QString("%1 (0x%2)").arg(srv.spbrg16).arg(srv.spbrg16, 0, 16));
        ui->spbrgEdit->setText(QString("%1 (0x%2)").arg(srv.spbrg).arg(srv.spbrg, 0, 16));
        ui->spbrghEdit->setText(QString("%1 (0x%2)").arg(srv.spbrgh).arg(srv.spbrgh, 0, 16));
        ui->realBaudEdit->setText(QString("%1").arg(srv.realBaud));
    } catch (BaudRateCalculator::CalculationError ex) {
        QMessageBox::warning(this, tr("Calculation error"), ex.message);
    }
}
