#include "BaudRateCalculator.h"

#include <QDebug>

BaudRateCalculator::BaudRateCalculator()
{
}

/*** API functions ************************************************************/

BaudRateCalculator::SerialRegisterValue
BaudRateCalculator::calculateRegisters(int baudRate, bool brg16,
                                       bool highSpeed, int fosc)
{
    // Sanity check
    if (baudRate <= 0) {
        throw CalculationError("Invalid Baud rate");
    }
    if (fosc <= 0) {
        throw CalculationError("Invalid Fosc value");
    }

    qDebug() << "Desired baud rate:" << baudRate;
    qDebug() << "BRG16:" << brg16 << " BRGH:" << highSpeed;
    qDebug() << "Fosc:" << fosc;

    // Calculate divider
    int divider;
    if (!brg16 && !highSpeed) {
        divider = 64;
    } else if ((!brg16 && highSpeed) || (brg16 && !highSpeed)) {
        divider = 16;
    } else {
        divider = 4;
    }

    qDebug() << "Divider:" << divider;

    int spbrg = fosc / baudRate / divider - 1;
    float realBaud = fosc / (divider * (spbrg + 1));
    float error = (realBaud - baudRate) / baudRate;

    qDebug() << "SPBRG:" << spbrg;
    qDebug() << "Calculated baud rate:" << realBaud;
    qDebug() << "Error:" << error;

    SerialRegisterValue srv;
    srv.error = error;
    srv.spbrg = spbrg;
    srv.realBaud = realBaud;
    srv.spbrg = spbrg & 0xFF;
    srv.spbrgh = (spbrg >> 8) & 0xFF;
    srv.spbrg16 = spbrg;

    return srv;
}
