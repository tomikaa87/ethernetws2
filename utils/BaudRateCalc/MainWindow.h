#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "SettingsProvider.h"

#include <QMainWindow>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(SettingsProvider *sp, QWidget *parent = 0);
    ~MainWindow();

protected:
    void closeEvent(QCloseEvent *);

private:
    Ui::MainWindow *ui;
    SettingsProvider *m_sp;

private slots:
    void calculateButtonClicked();
};

#endif // MAINWINDOW_H
