#-------------------------------------------------
#
# Project created by QtCreator 2012-03-15T11:52:07
#
#-------------------------------------------------

QT       += core gui widgets

TARGET = BaudRateCalc
TEMPLATE = app

CONFIG += qt

SOURCES += main.cpp\
        MainWindow.cpp \
    SettingsProvider.cpp \
    BaudRateCalculator.cpp

HEADERS  += MainWindow.h \
    SettingsProvider.h \
    BaudRateCalculator.h

FORMS    += MainWindow.ui

static {
    CONFIG += static

    DEFINES += STATIC
    message("Static build.")
}

win32: RC_FILE = BaudRateCalc.rc

RESOURCES += \
    gfx.qrc
