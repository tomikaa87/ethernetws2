#include <QApplication>

#include "MainWindow.h"
#include "SettingsProvider.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    SettingsProvider sp;

    MainWindow w(&sp);
    w.show();

    return a.exec();
}
