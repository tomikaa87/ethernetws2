#ifndef BAUDRATECALCULATOR_H
#define BAUDRATECALCULATOR_H

#include <QString>

class BaudRateCalculator
{
public:
    struct SerialRegisterValue{
        int spbrgh;
        int spbrg;
        int spbrg16;
        float error;
        float realBaud;
    };

    class CalculationError {
    public:
        CalculationError(QString message) {
            this->message = message;
        }

        QString message;
    };

    BaudRateCalculator();

    static SerialRegisterValue calculateRegisters(int baudRate, bool brg16,
                                                  bool highSpeed, int fosc);
};

#endif // BAUDRATECALCULATOR_H
