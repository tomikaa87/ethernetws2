#ifndef SETTINGSPROVIDER_H
#define SETTINGSPROVIDER_H

#include <QObject>
#include <QSettings>

class SettingsProvider : public QObject
{
    Q_OBJECT
public:
    explicit SettingsProvider(QObject *parent = 0);

    int fosc() const;
    void setFosc(int fosc);

    int foscUnitIndex() const;
    void setFostUnitIndex(int index);

    bool brghState() const;
    void setBrghState(bool enabled);

    bool brg16State() const;
    void setBrg16State(bool enabled);

    int baudRate() const;
    void setBaudRate(int baud);

    QByteArray mainWindowGeometry() const;
    void setMainWindowGeometry(QByteArray geometry);

private:
    QSettings *m_settings;
};

#endif // SETTINGSPROVIDER_H
