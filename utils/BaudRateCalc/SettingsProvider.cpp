#include "SettingsProvider.h"

SettingsProvider::SettingsProvider(QObject *parent) :
    QObject(parent)
{
    m_settings = new QSettings("ToMikaa", "BaudRateCalc", this);
}

/*** API functions ************************************************************/

int SettingsProvider::fosc() const
{
    int value = m_settings->value("Fosc", "4").toInt();
    if (value < 1 || value > 200)
        value = 4;

    return value;
}

void SettingsProvider::setFosc(int fosc)
{
    m_settings->setValue("Fosc", fosc);
}

int SettingsProvider::foscUnitIndex() const
{
    int value = m_settings->value("FoscUnitIndex", 2).toInt();
    if (value < 0 || value > 2)
        value = 2;

    return value;
}

void SettingsProvider::setFostUnitIndex(int index)
{
    m_settings->setValue("FoscUnitIndex", index);
}

bool SettingsProvider::brghState() const
{
    return m_settings->value("BRGH", true).toBool();
}

void SettingsProvider::setBrghState(bool enabled)
{
    m_settings->setValue("BRGH", enabled);
}

bool SettingsProvider::brg16State() const
{
    return m_settings->value("BRG16", false).toBool();
}

void SettingsProvider::setBrg16State(bool enabled)
{
    m_settings->setValue("BRG16", enabled);
}

int SettingsProvider::baudRate() const
{
    int value = m_settings->value("BaudRate", 9600).toInt();
    if (value < 1 || value > 1000000)
        value = 9600;

    return value;
}

void SettingsProvider::setBaudRate(int baud)
{
    m_settings->setValue("BaudRate", baud);
}

QByteArray SettingsProvider::mainWindowGeometry() const
{
    return m_settings->value("MainWindowGeometry").toByteArray();
}

void SettingsProvider::setMainWindowGeometry(QByteArray geometry)
{
    m_settings->setValue("MainWindowGeometry", geometry);
}
