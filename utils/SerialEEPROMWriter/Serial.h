#ifndef SERIAL_H
#define SERIAL_H

#include <QtSerialPort/QSerialPort>
#include <QStringList>
#include <QObject>

class QTimer;

class Serial : public QObject
{
    Q_OBJECT

public:
    explicit Serial(const QString &port,
                    int baudRate = 9600,
                    QObject *parent = nullptr);

    static QStringList enumeratePorts();

    struct Result {
        enum ErrorCode
        {
            ENoError,
            EAbort,
            EChecksumError,
            EConnectionError
        } error;
        int address;
        int dataLength;
        int transferedLength;

        Result(ErrorCode err = ENoError,
               int addr = 0,
               int len = 0,
               int tlen = 0):
            error(err), address(addr), dataLength(len), transferedLength(tlen)
        {}
    };

    bool send(unsigned baseAddress, const QByteArray &data);
    bool receive(unsigned baseAddress, int length);

public slots:
    void abort();

signals:
    void transmitFinished(Serial::Result result);
    void receiveFinished(Serial::Result result, QByteArray data);
    void transferProgress(int dataLength, int transmittedLength);

private:
    QSerialPort *m_serial;
    QString m_portName;
    int m_baudRate;
    QTimer *m_timer;
    QByteArray m_data;
    QByteArray m_chunk;
    unsigned m_baseAddress;
    int m_transmittedLength;
    int m_readLength;
    bool m_busy;
    bool m_readMode;
    QByteArray m_receiveBuffer;
    bool m_aborted;

    enum ParseResult
    {
        FrameOk,
        FrameFormatError,
        FrameTruncated,
        FrameChecksumError,
        FrameDataTruncated
    };

    enum TransferMode
    {
        ReadData,
        WriteData
    };

    QByteArray makeDataFrame(TransferMode mode,
                             quint32 address,
                             quint8 readLength = 0,
                             const QByteArray &data = QByteArray());

    ParseResult parseDataFrame(const QByteArray &frame,
                               quint8 &resultCode,
                               QByteArray &readData);

    void readError(Result::ErrorCode error);
    void writeError(Result::ErrorCode error);

private slots:
    void timerTimeout();

    void serialReadyRead();
    void serialError(QSerialPort::SerialPortError error);

    bool openPort();
    void closePort();

    void writeNextFrame();
    void readNextFrame();
};

#endif // SERIAL_H
