#include "Serial.h"
#include "debug.h"

#include <QRegExp>
#include <QtSerialPort/QSerialPortInfo>
#include <QTimer>

static const int DataChunkLength = 128;

//#define FRAME_TEST

//------------------------------------------------------------------------------
// Constructors / destructor
//------------------------------------------------------------------------------

Serial::Serial(const QString &port, int baudRate, QObject *parent) :
    QObject(parent),
    m_portName(port),
    m_baudRate(baudRate),
    m_baseAddress(0),
    m_transmittedLength(0),
    m_readLength(0),
    m_busy(false),
    m_readMode(false)
{
    qRegisterMetaType<Serial::Result>("Serial::Result");

#ifdef FRAME_TEST
    QByteArray data("TestDataABCDEF");
    auto frame = makeDataFrame(0xABCDEF11, data);
    unsigned addr;
    QByteArray parsedData;
    if (parseDataFrame(frame, addr, parsedData) != FrameOk)
    {
        _debug() << "Frame encode/decode test failed";
    }
    else
    {
        _debug() << "Frame encode/decode test was successful";
    }
#endif

    m_timer = new QTimer(this);
    m_timer->setSingleShot(true);
    m_timer->setInterval(10000);
    bool ok = (bool)connect(m_timer, SIGNAL(timeout()), this, SLOT(timerTimeout()));

    m_serial = new QSerialPort(this);
    ok &= (bool)connect(m_serial, SIGNAL(readyRead()), this, SLOT(serialReadyRead()));

    Q_ASSERT(ok);

//    _debug() << "TEST WRITE:" <<
//                makeDataFrame(WriteData, 0x123456, 0, "Test");
//    _debug() << "TEST READ :" <<
//                makeDataFrame(ReadData, 0x123456, 4);

//    quint8 readLength;
//    QByteArray readData;
//    parseDataFrame("W;02:04:ABCD;", readLength, readData);
}

//------------------------------------------------------------------------------
// Static API methods
//------------------------------------------------------------------------------

QStringList Serial::enumeratePorts()
{
    _debug() << "Enumerating serial communication ports";

    QStringList ports;
    for (const auto port : QSerialPortInfo::availablePorts())
        ports.append(port.portName());

    _debug() << "Port enumeration finished. Found" << ports.count() << "ports:" << ports;

    return ports;
}

//------------------------------------------------------------------------------
// API methods
//------------------------------------------------------------------------------

bool Serial::send(unsigned baseAddress, const QByteArray &data)
{
    // TODO error indication
    if (m_busy)
        return false;

    _debug() << "Sending data. Address:" << QString::number(baseAddress, 16).toUpper()
                << "Data:" << data.toHex().toUpper();

    m_aborted = false;
    m_baseAddress = baseAddress;
    m_data = data;
    m_busy = true;
    m_readMode = false;
    m_transmittedLength = 0;

    return QMetaObject::invokeMethod(this, "writeNextFrame", Qt::QueuedConnection);
}

//--------------------------------------------------------------------
bool Serial::receive(unsigned baseAddress, int length)
{
    // TODO error indication
    if (m_busy)
        return false;

    _debug() << "Receiving data. Address:" << QString::number(baseAddress, 16).toUpper()
                << "Data length:" << length;

    m_aborted = false;
    m_baseAddress = baseAddress;
    m_readLength = length;
    m_busy = true;
    m_readMode = true;

    return QMetaObject::invokeMethod(this, "readNextFrame", Qt::QueuedConnection);
}

//--------------------------------------------------------------------
void Serial::abort()
{
    m_aborted = true;
}

//------------------------------------------------------------------------------
// Private stuff
//------------------------------------------------------------------------------

/*
 * Text-based data frame format:
 *
 *   Read      : *R;[ADDRESS(8)]:[LENGTH(2)];
 *     Reply   : R;[RESULT(2)]:[LENGTH(2)]:[DATA(2)]:[DATA...]:[CRC16(4)];
 *     Error   : R;[RESULT(2)];
 *
 *   Write     : *W;[ADDRESS(8)]:[LENGTH(2)]:[DATA(2)]:[DATA...]:[CRC16(4)];
 *     Reply   : W;[RESULT(2)];
 *     MemFull : W;[RESULT(2)]:[WRITTEN_LEN(2)]:[CRC16(4)];
 *
 */
QByteArray Serial::makeDataFrame(Serial::TransferMode mode,
                                 quint32 address,
                                 quint8 readLength,
                                 const QByteArray &data)
{
    QByteArray frame;
    frame.append('*');

    switch (mode)
    {
        case WriteData:
        {
            // Transfer mode, address and data length
            frame.append(QString("W;%1:%2")
                         .arg(address, 8, 16, QChar('0')).toUpper()
                         .arg(data.length() & 0xff, 2, 16, QChar('0')).toUpper());
            // Data
            for (quint8 d: data)
                frame.append(QString(":%1").arg(d, 2, 16, QChar('0')).toUpper());
            // Checksum
            auto checksum = qChecksum(data.constData(), data.length());
            frame.append(QString(":%1").arg(checksum, 4, 16, QChar('0')).toUpper());

            break;
        }

        case ReadData:
        {
            // Transfer mode, address and read length
            frame.append(QString("R;%1:%2")
                         .arg(address, 8, 16, QChar('0')).toUpper()
                         .arg(readLength & 0xff, 2, 16, QChar('0')).toUpper());
            break;
        }
    }

    frame.append(';');
    frame.append("\r\n");

    _debug() << "Frame created:" << frame;

    fprintf(stderr, "Frame: (len: %d) '%s'\n", frame.length(), frame.constData());
    _flushall();

    return frame;
}

//--------------------------------------------------------------------
Serial::ParseResult Serial::parseDataFrame(const QByteArray &frame,
                                           quint8 &resultCode,
                                           QByteArray &readData)
{
    _debug() << "Parsing frame:" << frame;

    // Check if we have the whole frame
    int delimCount = 0;
    for (char c: frame)
        if (c == ';')
            delimCount++;
    if (delimCount != 2)
        return FrameTruncated;

    auto frameStr = QString(frame).toUpper();
    frameStr.replace("\r", "");
    frameStr.replace("\n", "");

    fprintf(stderr, "Parse: '%s'\n", frameStr.toLocal8Bit().constData());
    _flushall();

    // Check start, mode indicator and result
    QRegExp re("^([RW])\\;([0-9A-F]{2})(.*)");

    if (!re.exactMatch(frameStr))
        return FrameFormatError;

    auto mode = re.cap(1);
    resultCode = re.cap(2).toUInt(nullptr, 16);
    auto truncatedFrame = re.cap(3);

    if (mode == "R")
    {
        _debug() << "Read result:" << resultCode;

        switch (resultCode)
        {
            // Read OK, read data
            case 0:
            {
                // Parse frame
                //               _length___________  _data bytes and checksum (if any)______________________________
                re.setPattern("^(?:\\:([0-9A-F]{2}))(?:(\\:(?:[0-9A-F]{2}\\:)*[0-9A-F]{2})(?:\\:([0-9A-F]{4}))){0,1}\\;$");
                if (!re.exactMatch(truncatedFrame))
                    return FrameFormatError;

                _debug() << "Frame format OK";

                // Read OK, no data returned
                auto length = re.cap(1).toInt(nullptr, 16);
                if (length == 0)
                    return FrameOk;

                _debug() << "Frame has data";

                // Read data
                auto data = re.cap(2);
                data.replace(":", "");
                readData = QByteArray::fromHex(data.toLocal8Bit());

                _debug() << "Read data:" << readData.toHex().toUpper();

                // Check length
                if (readData.length() != length)
                    return FrameDataTruncated;

                _debug() << "Data length OK";

                // Verify checksum
                auto checksum = qChecksum(readData.constData(), readData.length());
                auto readChecksum = re.cap(3).toUInt(nullptr, 16);
                if (checksum != readChecksum)
                    return FrameChecksumError;

                _debug() << "Data checksum OK";

                break;
            }

            default:
                break;
        }
    }
    else if (mode == "W")
    {
        _debug() << "Write result:" << resultCode;

        switch (resultCode)
        {
            // Write OK, but we have reached the end of the memory,
            // but managed to write some bytes
            case 2:
            {
                // Parse frame      _written_l_     _crc16_____
                re.setPattern("^\\:([0-9A-F]{2})\\:([0-9A-F]{4})\\;$");
                if (!re.exactMatch(truncatedFrame))
                    return FrameFormatError;

                auto writtenLength = re.cap(1);
                auto writtenChecksum = re.cap(2);
                if (!writtenLength.isEmpty() && !writtenChecksum.isEmpty())
                {
                    readData.append(writtenLength);
                    readData.append(':');
                    readData.append(writtenChecksum.toLocal8Bit());
                }

                break;
            }

            default:
                break;
        }

    }
    else
        return FrameFormatError;

    return FrameOk;
}

//--------------------------------------------------------------------
void Serial::readError(Result::ErrorCode error)
{
    emit receiveFinished(Result(error,
                                m_baseAddress,
                                m_readLength,
                                m_data.length()),
                         m_data);

    m_busy = false;
}

//--------------------------------------------------------------------
void Serial::writeError(Result::ErrorCode error)
{
    emit transmitFinished(Result(error,
                                 m_baseAddress,
                                 m_data.length(),
                                 m_transmittedLength));

    m_busy = false;
}

//--------------------------------------------------------------------
void Serial::timerTimeout()
{
    _warning() << "Transmit timeout";

    if (m_readMode)
        readError(Result::EConnectionError);
    else
        writeError(Result::EConnectionError);
}

//--------------------------------------------------------------------
void Serial::serialReadyRead()
{
    auto data = m_serial->readAll();
    _debug() << "Received data:" << data;

    if (!m_busy)
    {
        _debug() << "Transfer is not in progress, ignoring received data";
        return;
    }

    m_receiveBuffer.append(data);

    if (m_readMode)
    {
        m_timer->stop();

        // Parse reply frame
        quint8 resultCode;
        QByteArray readData;
        auto parseResult = parseDataFrame(m_receiveBuffer, resultCode, readData);
        _debug() << "Parse result:" << parseResult;

        if (parseResult == FrameTruncated)
        {
            // Wait until the whole data is received
            m_timer->start();
            return;
        }

        if (parseResult != FrameOk)
        {
            _warning() << "Invalid response";
            readError(Result::EConnectionError);
            return;
        }

        m_receiveBuffer.clear();

        if (resultCode != 0)
        {
            _warning() << "Read error. Result:" << resultCode;
            readError(Result::EConnectionError);
            return;
        }

        _debug() << "Read data length:" << readData.length();
        m_data.append(readData);

        emit transferProgress(m_readLength, m_data.length());

        if (m_data.length() < m_readLength)
        {
            _debug() << "Data is incomplete. Got"
                        << m_data.length() << "bytes of"
                        << m_readLength << "bytes";

            QMetaObject::invokeMethod(this, "readNextFrame", Qt::QueuedConnection);
        }
        else
        {
            // Receive finished
            m_busy = false;

            _debug() << "Receive finished";

            readError(Result::ENoError);
        }
    }
    else
    {
        m_timer->stop();

        // Parse reply frame
        quint8 resultCode;
        QByteArray readData;
        auto parseResult = parseDataFrame(m_receiveBuffer, resultCode, readData);
        _debug() << "Parse result:" << parseResult;

        if (parseResult == FrameTruncated)
        {
            // Wait until the whole data is received
            m_timer->start();
            return;
        }

        m_receiveBuffer.clear();

        if (parseResult != FrameOk)
        {
            _warning() << "Invalid response";
            writeError(Result::EConnectionError);
            return;
        }

        m_transmittedLength += m_chunk.length();
        emit transferProgress(m_data.length(), m_transmittedLength);

        if (m_transmittedLength < m_data.length())
        {
            QMetaObject::invokeMethod(this, "writeNextFrame", Qt::QueuedConnection);
        }
        else
        {
            // Transmit finished
            m_busy = false;

            _debug() << "Transmit finished";

            writeError(Result::ENoError);
        }
    }
}

//--------------------------------------------------------------------
void Serial::serialError(QSerialPort::SerialPortError error)
{
    _warning() << "Serial port error:" << error;

    if (m_readMode)
        readError(Result::EConnectionError);
    else
        writeError(Result::EConnectionError);
}

//--------------------------------------------------------------------
bool Serial::openPort()
{
    if (m_serial->isOpen())
        return false;

    _debug() << "Opening port:" << m_portName;

    m_serial->setPortName(m_portName);
    if (!m_serial->open(QIODevice::ReadWrite))
    {
        _warning() << "Serial port open error:" << m_serial->errorString();
        return false;
    }

    if (!m_serial->setBaudRate(m_baudRate) ||
        !m_serial->setDataBits(QSerialPort::Data8) ||
        !m_serial->setParity(QSerialPort::NoParity) ||
        !m_serial->setFlowControl(QSerialPort::NoFlowControl) ||
        !m_serial->setStopBits(QSerialPort::OneStop))
        return false;

    return true;
}

//--------------------------------------------------------------------
void Serial::closePort()
{
    m_serial->close();
}

//--------------------------------------------------------------------
void Serial::writeNextFrame()
{
    if (m_aborted)
    {
        _warning() << "Transmit aborted";
        writeError(Result::EAbort);
        return;
    }

    if (!m_serial->isOpen() && !openPort())
    {
        _warning() << "Failed to open port";
        writeError(Result::EConnectionError);
        return;
    }

    _debug() << "Writing next frame";

    m_chunk = m_data.mid(m_transmittedLength, DataChunkLength);

    m_receiveBuffer.clear();
    m_serial->write(makeDataFrame(WriteData, m_baseAddress + m_transmittedLength, 0, m_chunk));
    m_serial->flush();
    m_timer->start();
}

//--------------------------------------------------------------------
void Serial::readNextFrame()
{
    if (m_aborted)
    {
        _warning() << "Receive aborted";
        readError(Result::EAbort);
        return;
    }

    if (!m_serial->isOpen() && !openPort())
    {
        _warning() << "Failed to open port";
        readError(Result::EConnectionError);
        return;
    }

    _debug() << "Reading next frame";

    quint8 readLength = qMin(DataChunkLength, m_readLength - m_data.length());
    _debug() << "Read length:" << readLength;

    m_serial->write(makeDataFrame(ReadData, m_baseAddress + m_data.length(), readLength));
    m_serial->flush();
    m_timer->start();
}
