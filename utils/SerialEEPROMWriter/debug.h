#ifndef DEBUG_H
#define DEBUG_H

#include <QDebug>
#include <QFileInfo>

inline QString FILE_NAME(const char *name)
{
    QFileInfo fi(name);
    return fi.fileName();
}

#ifdef DEBUG_VERBOSE
#define _debug() \
    qDebug() << QString("[D][%1:%2 %3()]:").arg(FILE_NAME(__FILE__)) \
    .arg(__LINE__).arg(__FUNCTION__).toLocal8Bit().constData()

#define _info() \
    qWarning() << QString("[I][%1:%2 %3()]:").arg(FILE_NAME(__FILE__)) \
    .arg(__LINE__).arg(__FUNCTION__).toLocal8Bit().constData()

#define _warning() \
    qWarning() << QString("[W][%1:%2 %3()]:").arg(FILE_NAME(__FILE__)) \
    .arg(__LINE__).arg(__FUNCTION__).toLocal8Bit().constData()

#define _error() \
    qWarning() << QString("[E][%1:%2 %3()]:").arg(FILE_NAME(__FILE__)) \
    .arg(__LINE__).arg(__FUNCTION__).toLocal8Bit().constData()
#else
#define _debug() \
    qDebug() << QString("[D]:").toLocal8Bit().constData()

#define _info() \
    qWarning() << QString("[I]:").toLocal8Bit().constData()

#define _warning() \
    qWarning() << QString("[W]:").toLocal8Bit().constData()

#define _error() \
    qWarning() << QString("[E]:").toLocal8Bit().constData()
#endif

#endif // DEBUG_H
