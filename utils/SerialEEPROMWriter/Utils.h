#ifndef UTILS_H
#define UTILS_H

#include <QByteArray>
#include <QImage>

class Utils
{
public:
    static QByteArray covertImageToBinary(const QImage &image);
    static QString generateCStruct(const QByteArray &data, const QString &name = "data");
    static bool parseInputNumber(const QString &number, quint32 *outputValue = nullptr);

private:
    Utils();
};

#endif // UTILS_H
