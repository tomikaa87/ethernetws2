#ifndef SERIALWRITER_P_H
#define SERIALWRITER_P_H

#include <QByteArray>
#include <QThread>
#include <QQueue>

#include "qextserialport.h"

typedef struct
{
    int id;
    quint32 baseAddress;
    QByteArray data;
    quint16 checksum;
} QueueItem;


/*** DataWriter class *********************************************************/

class DataWriter : public QThread
{
    Q_OBJECT

public:
    explicit DataWriter(QObject *parent = 0);

    void run();
    void enqueueTasks(QQueue<QueueItem> queue);
    QQueue<QueueItem> queue() const;
    void clearQueue();
    void abortProcessing();

private:
    QQueue<QueueItem> m_queue;
    QextSerialPort *m_serial;
    bool m_isAbortRequested;
    QByteArray m_response;

    void log(const QString message);
    quint16 calculateChecksum(const QByteArray data) const;
    bool setupSerialPort();

signals:
    void logMessage(const QString message);
    void processingStarted();
    void processingFinished();
    void serialPortError();
    void processingAborted();
    void progress(int percent);
};

#endif // SERIALWRITER_P_H
