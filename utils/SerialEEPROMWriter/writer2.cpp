#include "writer2.h"
#include "Config.h"
#include "debug.h"

#include <qextserialport.h>
#include <QTimer>
#include <QRegExp>
#include <QMutexLocker>

#define GRAB_MUTEX(MUTEX) QMutexLocker LOCKER(MUTEX); Q_UNUSED(LOCKER)

static const int MaxBlockSize = 128;
static const int PacketTimeout = 2000;
static const int MaxWriteAttempts = 2;

/*** Constructor / destructor *************************************************/

/**
 * @brief Writer2::Writer2
 * @param parent
 */
Writer2::Writer2(Config *config, QObject *parent) :
    QObject(parent), m_config(config), m_jobId(0), m_isWriting(false),
    m_isAborted(false)
{
    // Create serial port handler
    m_serial = new QextSerialPort();
    connect(m_serial, SIGNAL(readyRead()), this, SLOT(serialReadyRead()),
            Qt::QueuedConnection);
    connect(m_serial, SIGNAL(bytesWritten(qint64)),
            this, SLOT(serialBytesWritten(qint64)),
            Qt::QueuedConnection);

    // Create connection timer
    m_serialTimer = new QTimer(this);
    connect(m_serialTimer, SIGNAL(timeout()), this, SLOT(serialTimeout()));

    m_job.isFinished = true;
}

Writer2::~Writer2()
{
    _debug() << "Destroyed";
}

/*** API methods **************************************************************/

/**
 * @brief Writer2::write writes the given data immediately, without adding a job
 * to the queue.
 * @param address destination EEPROM address
 * @param data data to be written
 * @param blockSize number of bytes written at most in a single packet
 * @return ID of the job
 */
int Writer2::write(quint32 address, const QByteArray &data, int blockSize)
{
    if (!checkInput(address, data, blockSize)) {
        _warning() << "Invalid input data";
        return -1;
    }

    if (isWriting()) {
        _warning() << "Writing is in progress";
        return -1;
    }

    m_job.address = address;
    m_job.data = data;
    m_job.blockSize = blockSize;
    m_job.id = m_jobId++;
    m_job.blockAddress = address;
    m_job.blockCrc = 0;
    m_job.isFinished = false;
    m_job.writeAttemps = 0;

    _debug() << "Created" << m_job;

    QMetaObject::invokeMethod(this, "executeJob", Qt::QueuedConnection);

    return m_job.id;
}

/**
 * @brief Writer2::isWriting checks is the queue is being processed
 * at the moment.
 * @return
 */
bool Writer2::isWriting() const
{
    return m_isWriting;
}

/**
 * @brief Writer2::abortWriting aborts the writing process.
 */
void Writer2::abortWriting()
{
    m_isAborted = true;
}

/*** Private stuff ************************************************************/

bool Writer2::setupSerialPort()
{
    if (m_serial->isOpen())
        m_serial->close();

    log("Setting up serial port");

    // Serial port name
    m_serial->setPortName(m_config->config.serialPort);
    _debug() << "[Serial] Port:" << m_config->config.serialPort;

    // Serial port speed
    switch (m_config->config.serialSpeed)
    {
        case 0:
            m_serial->setBaudRate(BAUD110);
            _debug() << "[Serial] Baud rate: 110 bps";
            break;
        case 1:
            m_serial->setBaudRate(BAUD300);
            _debug() << "[Serial] Baud rate: 300 bps";
            break;
        case 2:
            m_serial->setBaudRate(BAUD600);
            _debug() << "[Serial] Baud rate: 600 bps";
            break;
        case 3:
            m_serial->setBaudRate(BAUD1200);
            _debug() << "[Serial] Baud rate: 1200 bps";
            break;
        case 4:
            m_serial->setBaudRate(BAUD2400);
            _debug() << "[Serial] Baud rate: 2400 bps";
            break;
        case 5:
            m_serial->setBaudRate(BAUD4800);
            _debug() << "[Serial] Baud rate: 4800 bps";
            break;
        case 6:
            m_serial->setBaudRate(BAUD9600);
            _debug() << "[Serial] Baud rate: 9600 bps";
            break;
        case 7:
            m_serial->setBaudRate(BAUD19200);
            _debug() << "[Serial] Baud rate: 19200 bps";
            break;
        case 8:
            m_serial->setBaudRate(BAUD38400);
            _debug() << "[Serial] Baud rate: 38400 bps";
            break;
        case 9:
            m_serial->setBaudRate(BAUD57600);
            _debug() << "[Serial] Baud rate: 57600 bps";
            break;
        case 10:
            m_serial->setBaudRate(BAUD115200);
            _debug() << "[Serial] Baud rate: 115200 bps";
            break;
        default:
            m_serial->setBaudRate(BAUD9600);
            _debug() << "[Serial] Baud rate: 9600 bps";
            log(tr("Invalid serial port speed. Falling back to default. (9600 bps)"));
            break;
    }

    // Serial port parity
    switch (m_config->config.serialParity)
    {
        case 0:
            m_serial->setParity(PAR_NONE);
            _debug() << "[Serial] Parity: none";
            break;
        case 1:
            m_serial->setParity(PAR_ODD);
            _debug() << "[Serial] Parity: odd";
            break;
        case 2:
            m_serial->setParity(PAR_EVEN);
            _debug() << "[Serial] Parity: even";
            break;
        case 3:
            m_serial->setParity(PAR_SPACE);
            _debug() << "[Serial] Parity: space";
            break;
        default:
            m_serial->setParity(PAR_NONE);
            _debug() << "[Serial] Parity: none";
            log(tr("Invalid serial port parity. Falling back to default. (None)"));
            break;
    }

    // Serial port flow control
    switch (m_config->config.serialFlowControl)
    {
        case 0:
            m_serial->setFlowControl(FLOW_OFF);
            _debug() << "[Serial] Flow control: off";
            break;
        case 1:
            m_serial->setFlowControl(FLOW_HARDWARE);
            _debug() << "[Serial] Flow control: hardware";
            break;
        case 2:
            m_serial->setFlowControl(FLOW_XONXOFF);
            _debug() << "[Serial] Flow control: Xon/Xoff";
            break;
        default:
            m_serial->setFlowControl(FLOW_OFF);
            _debug() << "[Serial] Flow control: off";
            log(tr("Invalid serial port flow control setting. Falling back to default. (Off)"));
            break;
    }

    // Serial port data bits
    switch (m_config->config.serialDataBits)
    {
        case 0:
            m_serial->setDataBits(DATA_5);
            _debug() << "[Serial] Data bits: 5";
            break;
        case 1:
            m_serial->setDataBits(DATA_6);
            _debug() << "[Serial] Data bits: 6";
            break;
        case 2:
            m_serial->setDataBits(DATA_7);
            _debug() << "[Serial] Data bits: 7";
            break;
        case 3:
            m_serial->setDataBits(DATA_8);
            _debug() << "[Serial] Data bits: 8";
            break;
        default:
            m_serial->setDataBits(DATA_8);
            _debug() << "[Serial] Data bits: 8";
            log(tr("Invalid serial port data bits. Falling back to default. (8)"));
            break;
    }

    // Serial port stop bits
    switch (m_config->config.serialStopBits)
    {
        case 0:
            m_serial->setStopBits(STOP_1);
            _debug() << "[Serial] Stop bits: 1";
            break;
        case 1:
            m_serial->setStopBits(STOP_1_5);
            _debug() << "[Serial] Stop bits: 1.5";
            break;
        case 2:
            m_serial->setStopBits(STOP_2);
            _debug() << "[Serial] Stop bits: 2";
            break;
        default:
            m_serial->setStopBits(STOP_1);
            _debug() << "[Serial] Stop bits: 1";
            log(tr("Invalid serial port stop bits. Falling back to default. (1)"));
            break;
    }

    log(tr("Opening serial port"));

    if (!m_serial->open(QIODevice::ReadWrite))
    {
        log(tr("Error: cannot open serial port"));
        return false;
    }

    return true;
}

/**
 * @brief Writer2::writeBlock writes the current data block.
 * @return
 */
Writer2::WriteStatus Writer2::writeBlock()
{
    // Check if processing is aborted
    if (m_isAborted) {
        return WriteAborted;
    }

    // Assemble the data packet
    QByteArray packet = QString("*;;%1:%2:%3;")
            .arg(m_job.blockAddress, 6, 16, QChar('0'))
            .arg(m_job.blockCrc, 4, 16, QChar('0'))
            .arg(m_job.block.toHex().constData())
            .toAscii();

    // Write the data to the serial port
    m_serial->write(packet);
    m_serial->flush();
    m_job.writeAttemps++;

    // Start the timer
    m_serialTimer->start(PacketTimeout);

    return WriteOk;
}

/**
 * @brief Writer2::loadNextBlock loads the next data block.
 * @return
 */
bool Writer2::loadNextBlock()
{
    // Current job is finished
    if (m_job.isFinished) {
        return false;
    }

    _debug() << "loading:" <<
                QString::number(m_job.blockAddress, 16).constData();
    m_job.writeAttemps = 0;

    // Load data block
    if (m_job.data.length() < m_job.blockSize) {
        // The whole data fits in a single block
        m_job.block = m_job.data;
        m_job.isFinished = true;
    } else {
        // Load the next data block
        m_job.block = m_job.data.mid(m_job.blockAddress,
                                                   m_job.blockSize);
        m_job.blockAddress += m_job.blockSize;
        // Check if no more data is in the job
        if (m_job.block.length() < m_job.blockSize) {
            m_job.isFinished = true;
        }
    }

    // Calculate block checksum
    m_job.blockCrc = qChecksum(m_job.block.constData(),
                               m_job.block.length());

    return true;
}

void Writer2::executeJob(bool nextBlock)
{
    // Load next data block if necessary
    if (nextBlock) {
        if (!loadNextBlock()) {
            _debug() << "Finished:" << m_job;
            emit finished(m_job.id, true);
            return;
        }
    }

    switch (writeBlock()) {
        case WriteOk:
            _debug() << "Packet written";
            break;
        case WriteAborted:
            _debug() << "Write aborted";
            m_serialTimer->stop();
            emit finished(m_job.id, true);
            return;
        case WriteError:
            _debug() << "Write error";
            if (++m_job.writeAttemps <= MaxWriteAttempts) {
                _debug() << "Trying again";
                QTimer::singleShot(500, this, SLOT(executeJob()));
            }
            break;
    }
}

void Writer2::serialTimeout()
{
    _debug() << "Serial timeout";
    m_serialTimer->stop();

    if (++m_job.writeAttemps <= MaxWriteAttempts) {
        _debug() << "Trying again";
        QTimer::singleShot(500, this, SLOT(executeJob()));
    } else {
        _warning() << "Writing failed, giving up";
        emit finished(m_job.id, true);
        return;
    }
}

void Writer2::serialReadyRead()
{
    _debug() << "Incoming data";

    m_serialResponse.append(m_serial->readAll());

    QRegExp re(".*;;(\\d+):(.+);.*");
    if (re.exactMatch(m_serialResponse)) {
        // Stop the timer
        m_serialTimer->stop();

        _debug() << "Response:" << m_serialResponse;
        m_serialResponse.clear();

        QString result = re.cap(1);
        QString message = re.cap(2);

        _debug() << "Message:" << message;

        QMetaObject::invokeMethod(this, "responseReceived",
                                  Qt::QueuedConnection,
                                  Q_ARG(int, result.toInt(0)));
    }
}

void Writer2::serialBytesWritten(qint64 bytes)
{
    _debug() << "Bytes written:" << bytes;
}

void Writer2::responseReceived(int result)
{
    _debug() << "Response received:" << result;

    switch (result) {
        case 0: // OK
            QMetaObject::invokeMethod(this, "executeJob", Qt::QueuedConnection,
                                      Q_ARG(bool, true));
            break;

        case 1: // Checksum error
            if (++m_job.writeAttemps <= MaxWriteAttempts) {
                _debug() << "Trying again";
                QTimer::singleShot(500, this, SLOT(executeJob()));
            } else {
                _warning() << "Writing failed, giving up";
                emit finished(m_job.id, true);
                return;
            }
            break;
    }
}

/**
 * @brief Writer2::checkInput checks the parameters of the input data.
 * @param address
 * @param data
 * @param blockSize
 * @return
 */
bool Writer2::checkInput(quint32 address, const QByteArray &data, int blockSize)
{
    if (address > m_config->config.memorySize - 1 || data.isEmpty() ||
            data.length() > int(m_config->config.memorySize) ||
            blockSize > 30) {
        return false;
    }

    return true;
}
