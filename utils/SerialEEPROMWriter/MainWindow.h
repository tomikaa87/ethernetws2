#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QFile>
#include <QThread>

#include "Config.h"
#include "AboutDialog.h"
#include "Serial.h"

namespace Ui {
    class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    void log(const QString message);

protected:
    void closeEvent(QCloseEvent *);

private:
    Ui::MainWindow *ui;
    Config *m_config;
    AboutDialog *m_aboutDialog;

    QString m_lastDirPath;
    QByteArray m_inputData;

    QThread m_serialThread;

    bool checkInputFile(const QString &fileName, qint64 *fileSize = nullptr);

    void initiateDownload();
    void initiateUpload();

    void updateLastDirPath(const QString &filePath);

private slots:
    void browseInputFile();
    void browseOutputFile();
    void startButtonClicked();
    void downloadAllButtonClicked();
    void downloadRemainiglButtonClicked();

    void memorySizeChanged(const QString &text);
    void uploadBaseAddressChanged(const QString &text);
    void downloadBaseAddressChanged(const QString &text);
    void downloadSizeChanged(const QString &text);

    Serial* createSerialTransport();
    void destroySerialTransport(Serial *serial);

    void enumerateSerialPorts();

    void serialTransmitFinished(Serial::Result result);
    void serialReceiveFinished(Serial::Result result, QByteArray data);
    void serialTransferProgress(int dataLength, int transmittedLength);
};

#endif // MAINWINDOW_H
