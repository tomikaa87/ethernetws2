#-------------------------------------------------
#
# Project created by QtCreator 2011-11-08T16:49:21
#
#-------------------------------------------------

QT       += core gui widgets serialport

TARGET = SerialEEPROMWriter
TEMPLATE = app

DEFINES += APP_VERSION=\\\"2.0.0\\\"

SOURCES += main.cpp\
        MainWindow.cpp \
    Config.cpp \
    AboutDialog.cpp \
    Serial.cpp \
    Utils.cpp

HEADERS  += MainWindow.h \
    Config.h \
    AboutDialog.h \
    debug.h \
    Serial.h \
    Utils.h

FORMS    += MainWindow.ui \
    AboutDialog.ui

RESOURCES += \
    gfx/gfx.qrc

CONFIG += c++11

RC_FILE = app.rc

win32 {
    CONFIG(debug, debug|release) {
        message(Debug build)
        DEFINES *= DEBUG_VERBOSE
    } else {
        message(Release build)
    }
}














