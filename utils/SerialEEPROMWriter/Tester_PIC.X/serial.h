/* 
 * File:   serial.h
 * Author: ToMikaa
 *
 * Created on 2014. janu�r 5., 13:51
 */

#ifndef SERIAL_H
#define	SERIAL_H

void serial_init();
void putch(char c);
char getch();
char getbufch();
void putbufch(char c);

void serial_task();

#endif	/* SERIAL_H */

