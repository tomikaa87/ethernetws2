#include <htc.h>
#include <ctype.h>
#include <stdio.h>

#include <types.h>
#include <crc16.h>

#include "serial.h"

#define _XTAL_FREQ                      32000000ul

#define SERIAL_SPBRG                    SPBRG
#define SERIAL_SPBRGH                   SPBRGH
#define SERIAL_TXSTA                    TXSTA
#define SERIAL_RCSTA                    RCSTA
#define SERIAL_BAUDCON                  BAUDCON
#define SERIAL_TRMT                     TRMT
#define SERIAL_RCIF                     RCIF

#define UART_TX_BAUD               57600ul
#define UART_TX_HIGH_SPEED         1
#define UART_RX_BUF_SIZE           8

#if (UART_TX_HIGH_SPEED == 1)
    #define UART_TX_DIVIDER        16ul
#else
    #define UART_TX_DIVIDER        64ul
#endif
#define UART_TX_SPBRG \
    (unsigned char)(_XTAL_FREQ / UART_TX_BAUD / UART_TX_DIVIDER - 1)
#define UART_TX_REAL_BAUD \
    (unsigned long)(_XTAL_FREQ / (UART_TX_DIVIDER * (UART_TX_SPBRG + 1)))
#define UART_TX_BAUD_ERROR \
    (signed char)((((float)UART_TX_REAL_BAUD - (float)UART_TX_BAUD) / (float)UART_TX_BAUD) * 100.0)

char Warning_UartTxErrorTooHigh[UART_TX_BAUD_ERROR <= 3];

volatile char rx_buf[UART_RX_BUF_SIZE] = {0, };
volatile uint8_t rx_buf_next_in = 0;
volatile uint8_t rx_buf_next_out = 0;
#define rx_buf_has_char     (rx_buf_next_in != rx_buf_next_out)

unsigned int cksum;
void serial_init()
{
    // BRG16 = 1
    SERIAL_BAUDCON = 0b0000100;
    // TXEN = 1, BRGH = UART_TX_HIGH_SPEED
    SERIAL_TXSTA = 0b00100000 | (UART_TX_HIGH_SPEED ? 0b100 : 0);
    // SPEN = 1, CREN = 1
    SERIAL_RCSTA = 0b10010000;
    // Set SPBRG
    SERIAL_SPBRG = UART_TX_SPBRG & 0xFF;
    // Set SPBRGH
    SERIAL_SPBRGH = (UART_TX_SPBRG >> 8) & 0xFF;
}

void putch(char c)
{
    while (!SERIAL_TRMT)
        continue;
    TXREG = c;
    while (!SERIAL_TRMT)
        continue;
}

char getch()
{
    while (!SERIAL_RCIF)
        continue;
    return RCREG;
}

char getbufch()
{
    char c;

    // Wait untit a byte is ready to read from the USART buffer
    while (!rx_buf_has_char)
        continue;
    c = rx_buf[rx_buf_next_out];
    // Step to next index
    rx_buf_next_out = (rx_buf_next_out + 1) % UART_RX_BUF_SIZE;
    return c;
}

void putbufch(char c)
{
    uint8_t next_in;

    rx_buf[rx_buf_next_in] = c;
    next_in = rx_buf_next_in;
    rx_buf_next_in = (rx_buf_next_in + 1) % UART_RX_BUF_SIZE;
    if (rx_buf_next_in == rx_buf_next_out)
        rx_buf_next_in = next_in;
}

unsigned char hex_to_int(char c)
{
    if (isxdigit(c))
    {
        char digit = toupper(c);
        return (digit <= '9' ? digit - '0' : digit - 'A' + 0x0A);
    }

    return 0;
}

char c;
    static enum {
        S_INIT,
        S_DATA_XFER_PARSE_HDR,
        S_DATA_XFER_READ,
        S_DATA_XFER_WRITE
    } state = S_INIT;

    static struct {
        unsigned char value;
        unsigned char data_index;
        unsigned char byte_index;
        unsigned int checksum;
        unsigned char length;

        union {
            struct {
                unsigned long address;
                unsigned char buffer[32];
            } eep;
        };
    } parser_data;


void serial_task()
{
//    static enum {
//        S_INIT,
//        S_DATA_XFER_PARSE_HDR,
//        S_DATA_XFER_READ,
//        S_DATA_XFER_WRITE
//    } state = S_INIT;
//
//    static struct {
//        unsigned char value;
//        unsigned char data_index;
//        unsigned char byte_index;
//        unsigned int checksum;
//        unsigned char length;
//
//        union {
//            struct {
//                unsigned long address;
//                unsigned char buffer[32];
//            } eep;
//        };
//    } parser_data;

    // DEBUG static is only for debugging
    c = getbufch();

    // Reset parser
    if (c == '*')
    {
reset:
        state = S_INIT;
        parser_data.data_index = 0;
        parser_data.byte_index = 0;
        return;
    }

    switch (state)
    {
        case S_INIT:
            // Switch parser mode
            switch (c)
            {
                // Data transfer mode
                case ';':
                    state = S_DATA_XFER_PARSE_HDR;
                    break;

                default:
                    // Unknown mode, reset
                    goto reset;
            }
            break;

        case S_DATA_XFER_PARSE_HDR:
            // Bytes after the checksum should be passed to checksum calculator
            if (parser_data.data_index > 3)
                crc16_byte(c);

            if (parser_data.data_index % 2 == 0)
            {
                // Read upper nibble
                parser_data.value = hex_to_int(c) << 4;
            }
            else
            {
                // Read lower nibble, we got the whole byte
                parser_data.value += hex_to_int(c);

                switch (parser_data.byte_index)
                {
                    case 0: // CRC MSB
                        parser_data.checksum = parser_data.value << 8;
                        break;
                    case 1: // CRC LSB
                        parser_data.checksum += parser_data.value;
                        crc16_reset();
                        break;
                    case 2: // Address MSB
                        parser_data.eep.address = (unsigned long)parser_data.value << 24;
                        break;
                    case 3: // Address upper byte
                        parser_data.eep.address += (unsigned long)parser_data.value << 16;
                        break;
                    case 4: // Address lower byte
                        parser_data.eep.address += (unsigned long)parser_data.value << 8;
                        break;
                    case 5: // Address LSB
                        parser_data.eep.address += (unsigned long)parser_data.value;
                        break;
                    case 6: // Data length
                        parser_data.length = parser_data.value;
                        break;
                    default: // Data
                        parser_data.eep.buffer[parser_data.byte_index - 7] = parser_data.value;
                        break;
                }

                // If we got the whole data, store it in the EEPROM
                if (parser_data.byte_index == parser_data.length + 6)
                {
                    // Verify checksum
                    if (parser_data.checksum != crc16_value())
                    {
                        // Error handling
                        printf("CRC error\r\n");
                    }

                    printf("*;708C0000000000");

                    goto reset;
                }

                ++parser_data.byte_index;
            }
            ++parser_data.data_index;
            break;

        case S_DATA_XFER_READ:

            break;

        case S_DATA_XFER_WRITE:

            break;
    }
}