#
# Generated - do not edit!
#
# NOCDDL
#
CND_BASEDIR=`pwd`
# Simulator configuration
CND_ARTIFACT_DIR_Simulator=dist/Simulator/production
CND_ARTIFACT_NAME_Simulator=Tester_PIC.X.production.hex
CND_ARTIFACT_PATH_Simulator=dist/Simulator/production/Tester_PIC.X.production.hex
CND_PACKAGE_DIR_Simulator=${CND_DISTDIR}/Simulator/package
CND_PACKAGE_NAME_Simulator=testerpic.x.tar
CND_PACKAGE_PATH_Simulator=${CND_DISTDIR}/Simulator/package/testerpic.x.tar
# PICkit2 configuration
CND_ARTIFACT_DIR_PICkit2=dist/PICkit2/production
CND_ARTIFACT_NAME_PICkit2=Tester_PIC.X.production.hex
CND_ARTIFACT_PATH_PICkit2=dist/PICkit2/production/Tester_PIC.X.production.hex
CND_PACKAGE_DIR_PICkit2=${CND_DISTDIR}/PICkit2/package
CND_PACKAGE_NAME_PICkit2=testerpic.x.tar
CND_PACKAGE_PATH_PICkit2=${CND_DISTDIR}/PICkit2/package/testerpic.x.tar
