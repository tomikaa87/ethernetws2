#include <htc.h>
#include <stdio.h>

#include "serial.h"

__CONFIG(1, RCIO);
__CONFIG(2, BOREN & WDTDIS);
__CONFIG(3, PBDIGITAL);
__CONFIG(4, LVPDIS & DEBUGEN);
__CONFIG(5, UNPROTECT);
__CONFIG(6, WRTEN);
__CONFIG(7, UNPROTECT);

void interrupt isr()
{
    // Serial interrupt
    if (RCIE && RCIF) {
        putbufch(RCREG);
    }
}

#include <string.h>

void main()
{
    OSCCON = 0b01110000;
    PLLEN = 1;

    // Peripherals
    ADCON1 = 0x0F;      // Disable analog ports
    CVRCON = 0;         // Disable voltage reference
    CMCON = 0x07;       // Disable comparator

    // Interrupts
    IPEN = 1;           // Enable interrupt priorities
    GIEH = 1;           // Global interrupt enable (high)
    GIEL = 1;           // Global interrupt enable (low)
    RCIP = 1;           // USART RX interrupt = high priority
    RCIE = 1;

    serial_init();

    printf("***************** SerialEEPROMWriter Tester\r\n");

//    const char *test_str = "*;D26A00B00B00054142434445";
//    unsigned char i;
//    for (i = 0; i < strlen(test_str); ++i)
//    {
//        putbufch(test_str[i]);
//        serial_task();
//    }

    for (;;)
    {
        serial_task();
    }
}
