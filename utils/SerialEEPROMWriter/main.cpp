#include <QApplication>
#include <QScopedPointer>
#include "MainWindow.h"

#include <ctime>

time_t startTime;
QScopedPointer<MainWindow> g_mainWindow;

void myMessageOutput(QtMsgType type, const QMessageLogContext &, const QString &msg)
{
    if (g_mainWindow.isNull())
        return;

    static time_t startTime = time(0);
    time_t currentTime = time(0);
    auto diff = difftime(currentTime, startTime);

    switch (type)
    {
        case QtDebugMsg:
#ifndef DEBUG_VERBOSE
            return;
#endif
        case QtWarningMsg:
        case QtCriticalMsg:
            g_mainWindow->log(QString("[%1]%2")
                              .arg(diff, 0, 'f', 0)
                              .arg(msg));
            break;

        case QtFatalMsg:
            g_mainWindow->log(QString("[%1][!FATAL!] %2")
                              .arg(diff, 0, 'f', 0)
                              .arg(msg));
            abort();
    }
}

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    g_mainWindow.reset(new MainWindow());
    qInstallMessageHandler(myMessageOutput);

    g_mainWindow->show();

    return a.exec();
}
