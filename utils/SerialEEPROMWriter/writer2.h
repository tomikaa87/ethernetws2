#ifndef WRITER2_H
#define WRITER2_H

#include <QObject>
#include <QQueue>
#include <QMutex>
#include <QDebug>

class QextSerialPort;
class QTimer;
class Config;

class Writer2 : public QObject
{
    Q_OBJECT

public:
    explicit Writer2(Config *config, QObject *parent = 0);
    ~Writer2();

    int write(quint32 address, const QByteArray &data, int blockSize = 30);

    bool isWriting() const;

    struct Job {
        int id;
        quint32 address;
        QByteArray data;

        int blockSize;
        quint32 blockAddress;
        quint16 blockCrc;
        QByteArray block;
        bool isFinished;
        int writeAttemps;
    };

public slots:
    void abortWriting();

signals:
    void progress(int percent);
    void finished(int jobId, bool error);
    void log(const QString &msg);

private:
    Config *m_config;
    QextSerialPort *m_serial;
    QByteArray m_serialResponse;
    QTimer *m_serialTimer;
    Job m_job;
    int m_jobId;
    bool m_isWriting;
    bool m_isAborted;

    enum WriteStatus {
        WriteOk,
        WriteAborted,
        WriteError
    };

    bool checkInput(quint32 address, const QByteArray &data, int blockSize);

    bool setupSerialPort();

    WriteStatus writeBlock();
    bool loadNextBlock();

private slots:
    void executeJob(bool nextBlock = false);
    void serialTimeout();
    void serialReadyRead();
    void serialBytesWritten(qint64 bytes);
    void responseReceived(int result);
};

inline QDebug &operator <<(QDebug &debug, Writer2::Job &j) {
    debug.nospace() << "Job {id=" << j.id << ", address="
                    << QString::number(j.address, 16).constData()
                    << ", len=" << j.data.length() << "}";
    return debug.space();
}

#endif // WRITER2_H
