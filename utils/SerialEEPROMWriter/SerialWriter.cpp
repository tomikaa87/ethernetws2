#include "SerialWriter.h"
#include "Config.h"

#include <QDebug>

static const quint16 Crc16Polynomial = 0x1021;

/*** DataWriter class *********************************************************/

DataWriter::DataWriter(QObject *parent):
    QThread(parent),
    m_serial(0),
    m_isAbortRequested(false)
{
}

void DataWriter::run()
{
    unsigned int tasksProcessed = 0;

    emit processingStarted();

    if (!m_serial)
    {
        QThread *serialThread = new QThread(this);
        m_serial = new QextSerialPort();
        m_serial->moveToThread(serialThread);
        qDebug() << "Writer thread:" << thread() << ", serial thread:" <<
                    serialThread;
    }

    if (!setupSerialPort())
    {
        emit serialPortError();
        emit processingFinished();
        return;
    }

    while (!m_queue.isEmpty() && !m_isAbortRequested)
    {
        QueueItem item = m_queue.dequeue();

        log(tr("[%1] Writing...").arg(item.id));

        quint32 lastBlockAddress = 0;
        quint32 sentBytes = 0;
        char blockSize = 32;
        quint32 address = item.baseAddress;
        while (!m_isAbortRequested)
        {
            QByteArray blockData = item.data.mid(lastBlockAddress, blockSize);
            quint16 checksum = calculateChecksum(blockData);

            /*log(QString("[%1] Data: %2 - %3 -> %4 - %5, CRC: %6")
                .arg(item.id)
                .arg(lastBlockAddress, 8, 16, QChar('0'))
                .arg(lastBlockAddress + blockData.length() - 1, 8, 16, QChar('0'))
                .arg(lastBlockAddress + item.baseAddress, 8, 16, QChar('0'))
                .arg(lastBlockAddress + item.baseAddress + blockData.length() - 1, 8, 16, QChar('0'))
                .arg(checksum, 4, 16, QChar('0')));*/

            address += blockSize;

            qDebug() << "Current address:" <<
                        QString("%1").arg(address, 8, 16, QChar('0'));
            qDebug() << "Data block length:" << blockData.length();

            QByteArray packet;

            // Frame indicator bytes
            packet.append(0xFA);
            packet.append(0xFB);
            // EEPROM address
            packet.append((char)(address >> 24) & 0xFF);     // Address
            packet.append((char)(address >> 16) & 0xFF);
            packet.append((char)(address >> 8) & 0xFF);
            packet.append((char)(address & 0xFF));
            // Data length
            packet.append(blockData.length());      // Data field length
            // Checksum
            packet.append((checksum >> 8) & 0xFF);  // Checksum MSB
            packet.append((char)(checksum & 0xFF)); // Checksum LSB
            // Data
            packet.append(blockData);

            qDebug() << "Packet:" << packet.toHex();

            // Write data to the serial port
            m_serial->write(packet);
            m_serial->flush();

            QByteArray response;
            QByteArray terminator;
            terminator.append(0xFA);
            terminator.append(0xFB);
            int retryCount = 0;

            while (!(response.contains(terminator) && response.length() < 3) ||
                   retryCount < 300000)
            {
                m_serial->waitForReadyRead(50000);
                response.append(m_serial->readAll());
                retryCount++;
            }

            qDebug() << "Retries:" << retryCount;
            qDebug() << "Response length:" << response.length();
            qDebug() << "Response:" << response.toHex();

            if (retryCount >= 3000000)
            {
                qWarning() << "Timeout";
                m_isAbortRequested = true;
            }
            else
            {
                if (response.length() < 3 || response.at(2) != 1)
                {
                    log(tr("[%1] Checksum error at %2. Aborting...")
                        .arg(item.id)
                        .arg(lastBlockAddress, 8, 16, QChar('0')));
                    m_isAbortRequested = true;
                }
            }

            if (blockData.length() < blockSize)
                break;

            lastBlockAddress += blockSize;
            sentBytes += blockData.length();

            emit progress((int)((float)sentBytes /
                                (float)(item.data.length()) * 100.0));
        }

        if (m_isAbortRequested)
        {
            log(tr("[%1] Write aborted.").arg(item.id));
            emit processingFinished();
            emit processingAborted();
        }
        else
        {
            log(tr("[%1] Write finished.").arg(item.id));
            tasksProcessed++;
        }
    }

    log(tr("Closing serial port"));
    m_serial->close();

    log(tr("%1 task(s) processed").arg(tasksProcessed));
    emit processingFinished();

    m_isAbortRequested = false;
}

void DataWriter::enqueueTasks(QQueue<QueueItem> queue)
{
    while (!queue.isEmpty())
        m_queue.enqueue(queue.dequeue());

    if (!isRunning())
        start();
}

void DataWriter::log(const QString message)
{
    emit logMessage(message);
}

quint16 DataWriter::calculateChecksum(const QByteArray data) const
{
    int i, j;
    quint16 runningValue = 0;

    for (i = 0; i < data.length(); i++)
    {
        runningValue ^= data.at(i);
        j = 8;

        do
        {
            if (runningValue & 0x8000)
                runningValue <<= 1;
            else
            {
                runningValue <<= 1;
                runningValue ^= Crc16Polynomial;
            }
        }
        while (--j);
    }

    return runningValue;
}

bool DataWriter::setupSerialPort()
{
    log("Setting up serial port");

    // Serial port name
    m_serial->setPortName(Config::instance()->config.serialPort);

    // Serial port speed
    switch (Config::instance()->config.serialSpeed)
    {
    case 0:
        m_serial->setBaudRate(BAUD110);
        break;
    case 1:
        m_serial->setBaudRate(BAUD300);
        break;
    case 2:
        m_serial->setBaudRate(BAUD600);
        break;
    case 3:
        m_serial->setBaudRate(BAUD1200);
        break;
    case 4:
        m_serial->setBaudRate(BAUD2400);
        break;
    case 5:
        m_serial->setBaudRate(BAUD4800);
        break;
    case 6:
        m_serial->setBaudRate(BAUD9600);
        break;
    case 7:
        m_serial->setBaudRate(BAUD19200);
        break;
    case 8:
        m_serial->setBaudRate(BAUD38400);
        break;
    case 9:
        m_serial->setBaudRate(BAUD57600);
        break;
    case 10:
        m_serial->setBaudRate(BAUD115200);
        break;
    default:
        m_serial->setBaudRate(BAUD9600);
        log(tr("Invalid serial port speed. Falling back to default. (9600 bps)"));
        break;
    }

    // Serial port parity
    switch (Config::instance()->config.serialParity)
    {
    case 0:
        m_serial->setParity(PAR_NONE);
        break;
    case 1:
        m_serial->setParity(PAR_ODD);
        break;
    case 2:
        m_serial->setParity(PAR_EVEN);
        break;
    case 3:
        m_serial->setParity(PAR_SPACE);
        break;
    default:
        m_serial->setParity(PAR_NONE);
        log(tr("Invalid serial port parity. Falling back to default. (None)"));
        break;
    }

    // Serial port flow control
    switch (Config::instance()->config.serialFlowControl)
    {
    case 0:
        m_serial->setFlowControl(FLOW_OFF);
        break;
    case 1:
        m_serial->setFlowControl(FLOW_HARDWARE);
        break;
    case 2:
        m_serial->setFlowControl(FLOW_XONXOFF);
        break;
    default:
        m_serial->setFlowControl(FLOW_OFF);
        log(tr("Invalid serial port flow control setting. Falling back to default. (Off)"));
        break;
    }

    // Serial port data bits
    switch (Config::instance()->config.serialDataBits)
    {
    case 0:
        m_serial->setDataBits(DATA_5);
        break;
    case 1:
        m_serial->setDataBits(DATA_6);
        break;
    case 2:
        m_serial->setDataBits(DATA_7);
        break;
    case 3:
        m_serial->setDataBits(DATA_8);
        break;
    default:
        m_serial->setDataBits(DATA_8);
        log(tr("Invalid serial port data bits. Falling back to default. (8)"));
        break;
    }

    // Serial port stop bits
    switch (Config::instance()->config.serialStopBits)
    {
    case 0:
        m_serial->setStopBits(STOP_1);
        break;
    case 1:
        m_serial->setStopBits(STOP_1_5);
        break;
    case 2:
        m_serial->setStopBits(STOP_2);
        break;
    default:
        m_serial->setStopBits(STOP_1);
        log(tr("Invalid serial port stop bits. Falling back to default. (1)"));
        break;
    }

    log(tr("Opening serial port"));

    if (!m_serial->open(QIODevice::ReadWrite))
    {
        log(tr("Error: cannot open serial port"));
        return false;
    }

    return true;
}

QQueue<QueueItem> DataWriter::queue() const
{
    return m_queue;
}

void DataWriter::clearQueue()
{
    m_queue.clear();
}

void DataWriter::abortProcessing()
{
    m_isAbortRequested = true;
}

/******************************************************************************/

/*** Constructor & destructor *************************************************/

SerialWriter::SerialWriter(QObject *parent) :
    QObject(parent),
    m_idCounter(1)
{
    m_dataWriter = new DataWriter(this);
    connect(m_dataWriter, SIGNAL(logMessage(QString)),
            this, SIGNAL(logMessage(QString)));
    connect(m_dataWriter, SIGNAL(processingStarted()),
            this, SIGNAL(processingStarted()));
    connect(m_dataWriter, SIGNAL(processingFinished()),
            this, SIGNAL(processingFinished()));
    connect(m_dataWriter, SIGNAL(serialPortError()),
            this, SLOT(requeueTasks()));
    connect(m_dataWriter, SIGNAL(processingAborted()),
            this, SLOT(requeueTasks()));
    connect(m_dataWriter, SIGNAL(progress(int)),
            this, SIGNAL(progress(int)));
}

/*** Public methods ***********************************************************/

int SerialWriter::addTask(const quint32 baseAddress, const QByteArray data)
{
    QueueItem item;
    item.baseAddress = baseAddress;
    item.data = data;
    item.checksum = calculateChecksum(data);
    item.id = m_idCounter++;
    m_taskQueue.enqueue(item);

    log(tr("[%1] Memory: %2 - %3, length: %4 Bytes, CRC: %5")
            .arg(item.id)
            .arg(item.baseAddress, 8, 16, QChar('0'))
            .arg(item.baseAddress + data.length() - 1, 8, 16, QChar('0'))
            .arg(data.length(), 0, 10)
            .arg(item.checksum, 4, 16, QChar('0')));

    return item.id;
}

void SerialWriter::processTasks()
{
    if (m_taskQueue.count() == 0)
    {
        log(tr("No tasks to process"));
        return;
    }

    log(tr("Processing %1 task(s)").arg(m_taskQueue.count()));
    m_dataWriter->enqueueTasks(m_taskQueue);
    m_taskQueue.clear();
}

/*** Private slots ************************************************************/

void SerialWriter::requeueTasks()
{
    // Restore queue
    QQueue<QueueItem> writerQueue = m_dataWriter->queue();

    while (!writerQueue.isEmpty())
        m_taskQueue.enqueue(writerQueue.dequeue());

    m_dataWriter->clearQueue();

    log("Pending tasks re-queued");
}

/*** Public slots *************************************************************/

void SerialWriter::abortProcessing()
{
    m_dataWriter->abortProcessing();
}

/*** Private methods **********************************************************/

quint16 SerialWriter::calculateChecksum(const QByteArray data) const
{
    int i, j;
    quint16 runningValue = 0;

    for (i = 0; i < data.length(); i++)
    {
        runningValue ^= data.at(i);
        j = 8;

        do
        {
            if (runningValue & 0x8000)
                runningValue <<= 1;
            else
            {
                runningValue <<= 1;
                runningValue ^= Crc16Polynomial;
            }
        }
        while (--j);
    }

    return runningValue;
}

void SerialWriter::log(const QString message)
{
    emit logMessage(message);
}
