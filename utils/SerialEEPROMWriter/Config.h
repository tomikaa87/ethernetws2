#ifndef CONFIG_H
#define CONFIG_H

#include <QObject>
#include <QSettings>

class Config : public QObject
{
    Q_OBJECT

public:
    explicit Config(QObject *parent = 0);

    struct ConfigData
    {
        QByteArray mainWindowGeometry;
        quint32 memorySize;
        quint32 uploadBaseAddress;          // Not saved
        quint32 downloadBaseAddress;
        quint32 downloadSize;
        QString serialPort;
        quint8 serialSpeed;
        quint8 serialParity;
        quint8 serialFlowControl;
        quint8 serialDataBits;
        quint8 serialStopBits;
        quint8 fileType;

        ConfigData():
            uploadBaseAddress(0),
            downloadBaseAddress(0),
            downloadSize(0)
        {}
    } config;


    void loadSettings();
    void saveSettings();

private:
    QSettings *m_settings;

};

#endif // CONFIG_H
