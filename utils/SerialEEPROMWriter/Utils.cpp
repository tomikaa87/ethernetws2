//
// Serial EEPROM Writer - Ethernet Weather Station
// Utility class
//
// Author: Tamas Karpati
// This file was created on 2014-01-26
//

#include "Utils.h"
#include "debug.h"

#include <QColor>
#include <QImage>
#include <QRegExp>

//------------------------------------------------------------------------------
// API methods
//------------------------------------------------------------------------------

QByteArray Utils::covertImageToBinary(const QImage &image)
{
    // Check if the image is valid
    if (image.isNull())
    {
        _warning() << "Invalid image";
        return QByteArray();
    }

    _info() << "Bitmap details:" << image.size() << "Colors:" <<
               image.colorCount();

    if (image.colorCount() != 2)
        _warning() << "Image should be monochrome";

    // Convert monochrome image into a bit stream
    QByteArray imageData;
    quint8 byte = 0;
    quint64 count = 0;

    for (int y = 0; y < image.height(); y++)
    {
        for (int x = 0; x < image.width(); x++)
        {
            if (image.pixel(x, y) == QColor(Qt::black).rgb())
                byte |= 1 << (7 - (count % 8));

            count++;

            if (count % 8 == 0 && count > 0)
            {
                imageData.append(byte);
                byte = 0;
                count = 0;
            }
        }
    }

    if (count > 0)
        imageData.append(byte);

    _info() << "Image conversion finished. Data size:" << imageData.length();

    return imageData;
}

//--------------------------------------------------------------------
QString Utils::generateCStruct(const QByteArray &data, const QString &name)
{
    QString cs;

    cs.append("unsigned char ");
    cs.append(name);
    cs.append("[");
    cs.append(QString::number(data.length()).toLocal8Bit());
    cs.append("] = {\n        ");
    int cnt = 0;
    for (int i = 0; i < data.length(); i++) {
        cs.append("0x");
        cs.append(QString("%1")
                 .arg((quint8)data[i], 2, 16, QChar('0'))
                 .toUpper().toLocal8Bit());
        if (i < data.length() - 1)
            cs.append(", ");
        if (cnt++ == 7) {
            cs.append("\n        ");
            cnt = 0;
        }
    }
    cs.append("\n};");

    return cs;
}

//--------------------------------------------------------------------
bool Utils::parseInputNumber(const QString &number, quint32 *outputValue)
{
    // Try to parse as decimal number
    QRegExp re("^([0-9]+)$", Qt::CaseInsensitive);
    if (re.exactMatch(number))
    {
        if (outputValue)
        {
            bool ok;
            *outputValue = re.cap(1).toUInt(&ok);
            return ok;
        }
        return true;
    }

    // Try to parse as hexadecimal number
    re.setPattern("^0x([0-9A-F]+)$");
    if (re.exactMatch(number))
    {
        if (outputValue)
        {
            bool ok;
            *outputValue = re.cap(1).toUInt(&ok, 16);
            return ok;
        }
        return true;
    }

    return false;
}

//------------------------------------------------------------------------------
// Private stuff
//------------------------------------------------------------------------------

Utils::Utils()
{
}
