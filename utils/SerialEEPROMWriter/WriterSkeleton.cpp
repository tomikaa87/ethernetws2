#include "WriterSkeleton.h"

Writer::Writer(QObject *parent) :
    QObject(parent)
{
}

Writer::~Writer()
{

}

int Writer::addTask(const quint32, const QByteArray)
{
    return 0;
}

void Writer::processTasks()
{

}

QList<int> Writer::taskIds() const
{
    return QList<int>();
}

bool Writer::removeTask(int)
{
    return false;
}

void Writer::removeAllTasks()
{

}

bool Writer::isProcessing() const
{
    return false;
}

void Writer::abortProcessing()
{

}
