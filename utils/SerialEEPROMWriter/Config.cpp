#include "Config.h"

/*** Configuration keys *******************************************************/

static const QString MainWindowGeometry = "MainWindowGeometry";
static const QString MemorySize = "MemorySize";

static const QString SerialPort = "Serial/Port";
static const QString SerialSpeed = "Serial/Speed";
static const QString SerialParity = "Serial/Parity";
static const QString SerialFlow = "Serial/FlowControl";
static const QString SerialDataBits = "Serial/DataBits";
static const QString SerialStopBits = "Serial/StopBits";

static const QString FileType = "File/Type";

/*** Constructor & destructor *************************************************/

Config::Config(QObject *parent) :
    QObject(parent)
{
    m_settings = new QSettings("config.ini", QSettings::IniFormat, this);
}

/*** Public methods ***********************************************************/

void Config::loadSettings()
{
    bool ok;

    config.mainWindowGeometry = m_settings->value(
                MainWindowGeometry, QVariant()).toByteArray();

    config.memorySize = m_settings->value(MemorySize,
                                          QVariant(128 * 1024)).toUInt(&ok);
    if (!ok)
        config.memorySize = 128 * 1024;

    config.serialPort = m_settings->value(SerialPort, QVariant("COM1")).toString();
    config.serialSpeed = m_settings->value(SerialSpeed, QVariant(6)).toUInt(&ok);
    if (!ok)
        config.serialSpeed = 6;
    config.serialParity = m_settings->value(SerialParity, QVariant(2)).toUInt(&ok);
    if (!ok)
        config.serialParity = 0;
    config.serialFlowControl = m_settings->value(SerialFlow, QVariant(2)).toUInt(&ok);
    if (!ok)
        config.serialFlowControl = 0;
    config.serialDataBits = m_settings->value(SerialDataBits, QVariant(3)).toUInt(&ok);
    if (!ok)
        config.serialDataBits = 3;
    config.serialStopBits = m_settings->value(SerialStopBits, QVariant(0)).toUInt(&ok);
    if (!ok)
        config.serialStopBits = 0;

    config.fileType = m_settings->value(FileType, QVariant(0)).toUInt(&ok);
    if (!ok)
        config.fileType = 0;
}

void Config::saveSettings()
{
    m_settings->setValue(MainWindowGeometry, config.mainWindowGeometry);
    m_settings->setValue(MemorySize, config.memorySize);

    m_settings->setValue(SerialPort, config.serialPort);
    m_settings->setValue(SerialSpeed, config.serialSpeed);
    m_settings->setValue(SerialParity, config.serialParity);
    m_settings->setValue(SerialFlow, config.serialFlowControl);
    m_settings->setValue(SerialDataBits, config.serialDataBits);
    m_settings->setValue(SerialStopBits, config.serialStopBits);

    m_settings->setValue(FileType, config.fileType);
}
