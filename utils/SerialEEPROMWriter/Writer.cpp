#include "Writer.h"
#include "Config.h"

#include <QDebug>
#include <QFuture>
#include <QtConcurrentRun>
#include <QRegExp>

/*** Constructor / destructor *************************************************/

Writer::Writer(QObject *parent) :
    QObject(parent),
    m_idCounter(1)
{
    qDebug() << "[Writer] Creating...";

    m_serial = new QextSerialPort();
    connect(m_serial, SIGNAL(readyRead()), this, SLOT(serialReadyRead()));
    connect(m_serial, SIGNAL(bytesWritten(qint64)),
            this, SLOT(serialBytesWritten(qint64)));

    m_serialTimer = new QTimer(this);
    connect(m_serialTimer, SIGNAL(timeout()), this, SLOT(serialTimerTick()));
    m_serialTimer->setInterval(100);
    m_serialTimer->start();
}

Writer::~Writer()
{
    qDebug() << "[Writer] Destroying...";

    m_serial->close();
    delete m_serial;
}

/*** Public methods ***********************************************************/

int Writer::addTask(const quint32 baseAddress, const QByteArray data)
{
    WriterQueueItem item;
    item.baseAddress = baseAddress;
    item.data = data;
    item.checksum = qChecksum(data.constData(), data.length());
    item.id = m_idCounter++;
    m_taskQueue.enqueue(item);

    log(tr("[%1] Memory: %2 - %3, length: %4 Bytes, CRC: %5")
            .arg(item.id)
            .arg(item.baseAddress, 8, 16, QChar('0'))
            .arg(item.baseAddress + data.length() - 1, 8, 16, QChar('0'))
            .arg(data.length(), 0, 10)
            .arg(item.checksum, 4, 16, QChar('0')));

    return item.id;
}

void Writer::processTasks()
{
    // Check if we have enqueued tasks
    if (m_taskQueue.count() == 0)
    {
        log(tr("No tasks to process"));
        return;
    }

    log(tr("Processing %1 task(s)").arg(m_taskQueue.count()));

    // Configure the serial port
    if (!setupSerialPort())
    {
        emit processingFinished();
        return;
    }

    // Start the processing asynchronously
    m_writeFuture = QtConcurrent::run(this, &Writer::serialWrite);
}

QList<int> Writer::taskIds() const
{
    QList<int> ids;
    foreach (WriterQueueItem item, m_taskQueue)
        ids << item.id;
    return ids;
}

bool Writer::removeTask(int id)
{
    if (id < 1 || id > m_idCounter || m_taskQueue.count() == 0)
        return false;

    for (int i = 0; i < m_taskQueue.count(); i++)
        if (m_taskQueue.at(i).id == id)
        {
            m_taskQueue.removeAt(i);
            log(tr("Task %1 removed").arg(id));
            return true;
        }

    return false;
}

void Writer::removeAllTasks()
{
    int taskCount = m_taskQueue.count();
    m_taskQueue.clear();
    log(tr("%1 task(s) removed.").arg(taskCount));
}

bool Writer::isProcessing() const
{
    return m_writeFuture.isRunning();
}

/*** Public slots *************************************************************/

void Writer::abortProcessing()
{
    m_isAbortRequested = true;
}

/*** Private methods **********************************************************/

/**
 * Writes the given \p message to the log window.
 */
void Writer::log(const QString message)
{
    emit logMessage(message);
}

bool Writer::setupSerialPort()
{
    if (m_serial->isOpen())
        m_serial->close();

    log("Setting up serial port");

    // Serial port name
    m_serial->setPortName(m_config->config.serialPort);
    qDebug() << "[Serial] Port:" << m_config->config.serialPort;

    // Serial port speed
    switch (m_config->config.serialSpeed)
    {
        case 0:
            m_serial->setBaudRate(BAUD110);
            qDebug() << "[Serial] Baud rate: 110 bps";
            break;
        case 1:
            m_serial->setBaudRate(BAUD300);
            qDebug() << "[Serial] Baud rate: 300 bps";
            break;
        case 2:
            m_serial->setBaudRate(BAUD600);
            qDebug() << "[Serial] Baud rate: 600 bps";
            break;
        case 3:
            m_serial->setBaudRate(BAUD1200);
            qDebug() << "[Serial] Baud rate: 1200 bps";
            break;
        case 4:
            m_serial->setBaudRate(BAUD2400);
            qDebug() << "[Serial] Baud rate: 2400 bps";
            break;
        case 5:
            m_serial->setBaudRate(BAUD4800);
            qDebug() << "[Serial] Baud rate: 4800 bps";
            break;
        case 6:
            m_serial->setBaudRate(BAUD9600);
            qDebug() << "[Serial] Baud rate: 9600 bps";
            break;
        case 7:
            m_serial->setBaudRate(BAUD19200);
            qDebug() << "[Serial] Baud rate: 19200 bps";
            break;
        case 8:
            m_serial->setBaudRate(BAUD38400);
            qDebug() << "[Serial] Baud rate: 38400 bps";
            break;
        case 9:
            m_serial->setBaudRate(BAUD57600);
            qDebug() << "[Serial] Baud rate: 57600 bps";
            break;
        case 10:
            m_serial->setBaudRate(BAUD115200);
            qDebug() << "[Serial] Baud rate: 115200 bps";
            break;
        default:
            m_serial->setBaudRate(BAUD9600);
            qDebug() << "[Serial] Baud rate: 9600 bps";
            log(tr("Invalid serial port speed. Falling back to default. (9600 bps)"));
            break;
    }

    // Serial port parity
    switch (m_config->config.serialParity)
    {
        case 0:
            m_serial->setParity(PAR_NONE);
            qDebug() << "[Serial] Parity: none";
            break;
        case 1:
            m_serial->setParity(PAR_ODD);
            qDebug() << "[Serial] Parity: odd";
            break;
        case 2:
            m_serial->setParity(PAR_EVEN);
            qDebug() << "[Serial] Parity: even";
            break;
        case 3:
            m_serial->setParity(PAR_SPACE);
            qDebug() << "[Serial] Parity: space";
            break;
        default:
            m_serial->setParity(PAR_NONE);
            qDebug() << "[Serial] Parity: none";
            log(tr("Invalid serial port parity. Falling back to default. (None)"));
            break;
    }

    // Serial port flow control
    switch (m_config->config.serialFlowControl)
    {
        case 0:
            m_serial->setFlowControl(FLOW_OFF);
            qDebug() << "[Serial] Flow control: off";
            break;
        case 1:
            m_serial->setFlowControl(FLOW_HARDWARE);
            qDebug() << "[Serial] Flow control: hardware";
            break;
        case 2:
            m_serial->setFlowControl(FLOW_XONXOFF);
            qDebug() << "[Serial] Flow control: Xon/Xoff";
            break;
        default:
            m_serial->setFlowControl(FLOW_OFF);
            qDebug() << "[Serial] Flow control: off";
            log(tr("Invalid serial port flow control setting. Falling back to default. (Off)"));
            break;
    }

    // Serial port data bits
    switch (m_config->config.serialDataBits)
    {
        case 0:
            m_serial->setDataBits(DATA_5);
            qDebug() << "[Serial] Data bits: 5";
            break;
        case 1:
            m_serial->setDataBits(DATA_6);
            qDebug() << "[Serial] Data bits: 6";
            break;
        case 2:
            m_serial->setDataBits(DATA_7);
            qDebug() << "[Serial] Data bits: 7";
            break;
        case 3:
            m_serial->setDataBits(DATA_8);
            qDebug() << "[Serial] Data bits: 8";
            break;
        default:
            m_serial->setDataBits(DATA_8);
            qDebug() << "[Serial] Data bits: 8";
            log(tr("Invalid serial port data bits. Falling back to default. (8)"));
            break;
    }

    // Serial port stop bits
    switch (m_config->config.serialStopBits)
    {
        case 0:
            m_serial->setStopBits(STOP_1);
            qDebug() << "[Serial] Stop bits: 1";
            break;
        case 1:
            m_serial->setStopBits(STOP_1_5);
            qDebug() << "[Serial] Stop bits: 1.5";
            break;
        case 2:
            m_serial->setStopBits(STOP_2);
            qDebug() << "[Serial] Stop bits: 2";
            break;
        default:
            m_serial->setStopBits(STOP_1);
            qDebug() << "[Serial] Stop bits: 1";
            log(tr("Invalid serial port stop bits. Falling back to default. (1)"));
            break;
    }

    log(tr("Opening serial port"));

    m_serialTimerTicks = 0;
    if (!m_serial->open(QIODevice::ReadWrite))
    {
        log(tr("Error: cannot open serial port"));
        return false;
    }

    return true;
}

/**
 * Writes the data associated to the enqueued tasks.
 */
void Writer::serialWrite()
{
    unsigned int processedTasks = 0;
    m_isAbortRequested = false;

    // Notify the main window that the processing is being started
    emit processingStarted();

    // Process the queue, item-by-item
    while (!m_taskQueue.isEmpty() && !m_isAbortRequested)
    {
        // Get the first item, but it will be removed only if the writing was successful
        WriterQueueItem item = m_taskQueue.first();

        log(tr("[%1] Writing...").arg(item.id));

        quint32 lastBlockAddress = 0;
        quint32 sentBytes = 0;
        char blockSize = 32;
        quint32 address = item.baseAddress;

        while (!m_isAbortRequested)
        {
            QByteArray blockData = item.data.mid(lastBlockAddress, blockSize);
            if (blockData.length() == 0)
                break;

            qDebug() << "Block data length:" << blockData.length();

            quint16 checksum =
                    qChecksum(blockData.constData(), blockData.length());

            /*log(QString("[%1] Data: %2 - %3 -> %4 - %5, CRC: %6")
                .arg(item.id)
                .arg(lastBlockAddress, 8, 16, QChar('0'))
                .arg(lastBlockAddress + blockData.length() - 1, 8, 16, QChar('0'))
                .arg(lastBlockAddress + item.baseAddress, 8, 16, QChar('0'))
                .arg(lastBlockAddress + item.baseAddress + blockData.length() - 1, 8, 16, QChar('0'))
                .arg(checksum, 4, 16, QChar('0')));*/

            qDebug() << "\n*** Task" << item.id << "packet *" <<
                 tr("Mapping: %1 - %2 -> %3 - %4, CRC: %5")
                .arg(lastBlockAddress, 8, 16, QChar('0'))
                .arg(lastBlockAddress + blockData.length() - 1, 8, 16, QChar('0'))
                .arg(lastBlockAddress + item.baseAddress, 8, 16, QChar('0'))
                .arg(lastBlockAddress + item.baseAddress + blockData.length() - 1, 8, 16, QChar('0'))
                .arg(checksum, 4, 16, QChar('0'))
                << "***";

            qDebug() << "[Writer] Current address:" <<
                        QString("%1").arg(address, 8, 16, QChar('0'));
            qDebug() << "[Writer] Data block length:" << blockData.length();



            QString packet = QString("*;;%1:%2:%3;") //QString("*;;%1:%2:%3:%4;")
                    //.arg(blockData.length(), 0, 16)
                    .arg(address, 0, 16)
                    .arg(checksum, 0, 16)
                    .arg(blockData.toHex().constData());

            /*QByteArray packet;

            // Frame indicator bytes
            packet.append(0xFA);
            packet.append(0xFB);
            // EEPROM address
            packet.append((char)(address >> 24) & 0xFF);     // Address
            packet.append((char)(address >> 16) & 0xFF);
            packet.append((char)(address >> 8) & 0xFF);
            packet.append((char)(address & 0xFF));
            // Data length
            packet.append(blockData.length());      // Data field length
            // Checksum
            packet.append((checksum >> 8) & 0xFF);  // Checksum MSB
            packet.append((char)(checksum & 0xFF)); // Checksum LSB
            // Data
            packet.append(blockData);*/

            qDebug() << "[Writer] Packet:" << packet;

            // Write data to the serial port
            m_serialResponse.clear();
            m_serial->write(packet.toAscii());
            m_serial->flush();
            m_serial->waitForBytesWritten(INT_MAX);

            // Reset timer to measure response time
            m_serialTimerTicks = 0;

            // Wait for the response, but for maximum 5 seconds
            while (m_serialTimerTicks < 50)
            {
                QRegExp re(".*;;(\\d+):(.+);.*");
                if (re.exactMatch(m_serialResponse))
                    break;

                m_serial->flush();
            }


            if (m_serialTimerTicks >= 50)
            {
                log(tr("[%1] Timeout occured while writing data. Aborting...")
                    .arg(item.id));
                qDebug() << "[Writer] Response:" << m_serialResponse.length() <<
                            "Bytes, data:" << m_serialResponse;
                m_isAbortRequested = true;
            }
            else
            {
                /*qDebug() << "[Writer] Response:" << m_serialResponse.length() <<
                            "Bytes, data:" << m_serialResponse;*/

                QRegExp re(".*;;(\\d+):(.+);.*");
                if (!re.exactMatch(m_serialResponse))
                {
                    log(tr("[%1] Invalid response at %2: \"%3\". Aborting...")
                        .arg(item.id)
                        .arg(lastBlockAddress, 8, 16, QChar('0'))
                        .arg(m_serialResponse.constData()));
                    m_isAbortRequested = true;
                }
                else
                {
                    int errorCode = re.cap(1).toUInt();
                    if (errorCode > 0)
                    {
                        log(tr("[%1] Error at %2: \"%3\". Aborting.")
                            .arg(item.id)
                            .arg(lastBlockAddress, 8, 16, QChar('0'))
                            .arg(re.cap(2)));
                         m_isAbortRequested = true;
                    }
                }

                /*if (m_serialResponse.length() >= 5)
                {
                    if (m_serialResponse.at(4) != 1)
                    {
                        log(tr("[%1] Checksum error at %2. Aborting...")
                            .arg(item.id)
                            .arg(lastBlockAddress, 8, 16, QChar('0')));
                        m_isAbortRequested = true;
                    }

                    m_serialResponse.remove(0, 5);
                }
                else
                {
                    log(tr("[%1] Invalid response at %2. Aborting...")
                        .arg(item.id)
                        .arg(lastBlockAddress, 8, 16, QChar('0')));
                    m_isAbortRequested = true;
                }*/
            }

            if (blockData.length() < blockSize)
                break;

            lastBlockAddress += blockSize;
            sentBytes += blockData.length();
            address += blockSize;

            emit progress((int)((float)sentBytes /
                                (float)(item.data.length()) * 100.0));
        }

        if (m_isAbortRequested)
        {
            log(tr("[%1] Write aborted.").arg(item.id));
            emit processingFinished();
        }
        else
        {
            log(tr("[%1] Write finished.").arg(item.id));
            processedTasks++;

            // Dequeue finished task
            m_taskQueue.removeFirst();
        }
    }

    log(tr("%1 task(s) processed").arg(processedTasks));
    emit processingFinished();
}

/*** Private slots ************************************************************/

void Writer::serialReadyRead()
{
//    qDebug() << m_serial->bytesAvailable() <<
//                "Bytes of data available to read on serial port";
    QByteArray data = m_serial->readAll();
    m_serialResponse.append(data);
    //qDebug() << "[Serial] Received:" << data.toHex();
}

void Writer::serialBytesWritten(qint64 count)
{
    qDebug() << "[Serial]" << count << "Bytes written";
}

void Writer::serialTimerTick()
{
    m_serialTimerTicks++;
    //qDebug() << "Ticks:" << m_serialTimerTicks;
    if (m_serialTimerTicks > 100 && m_serial->isOpen())
        closeSerialPort();
}

void Writer::closeSerialPort()
{
    log("Closing serial port...");
    m_serial->close();
}
