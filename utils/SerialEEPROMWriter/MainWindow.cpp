#include "MainWindow.h"
#include "ui_MainWindow.h"
#include "debug.h"

#include "Serial.h"
#include "Utils.h"

#include <QFileDialog>
#include <QDir>
#include <QMessageBox>

//------------------------------------------------------------------------------
// Constants
//------------------------------------------------------------------------------

static const char *EditValueInvalidCSS = "background-color: #ff6a6a;";

//------------------------------------------------------------------------------
// Constructor / destructor
//------------------------------------------------------------------------------

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    m_lastDirPath(QDir::currentPath())
{
    setWindowIcon(QIcon(":/Application/chip"));

    ui->setupUi(this);

    connect(ui->inputFileBrowseButton, SIGNAL(clicked()),
            this, SLOT(browseInputFile()));
    connect(ui->startButton, SIGNAL(clicked()),
            this, SLOT(startButtonClicked()));

//    ui->startButton->setEnabled(false);
    ui->abortButton->setEnabled(false);
    ui->transferProgressBar->hide();

    // Load settings
    m_config = new Config(this);
    m_config->loadSettings();
    restoreGeometry(m_config->config.mainWindowGeometry);
    ui->memorySizeEdit->setText(QString::number(m_config->config.memorySize));

    QMetaObject::invokeMethod(this, "enumerateSerialPorts", Qt::QueuedConnection);

    // Serial port speed
    if (m_config->config.serialSpeed > ui->serialPortSpeedCombo->count() - 1)
        m_config->config.serialSpeed = 6;
    ui->serialPortSpeedCombo->setCurrentIndex(m_config->config.serialSpeed);
    // Serial port parity
    if (m_config->config.serialParity > ui->serialPortParityCombo->count() - 1)
        m_config->config.serialParity = 0;
    ui->serialPortSpeedCombo->setCurrentIndex(m_config->config.serialSpeed);
    // Serial port flow control
    if (m_config->config.serialFlowControl > ui->serialPortFlowCombo->count() - 1)
        m_config->config.serialFlowControl = 0;
    ui->serialPortFlowCombo->setCurrentIndex(m_config->config.serialFlowControl);
    // Serial port data bits
    if (m_config->config.serialDataBits > ui->serialPortDataBitsCombo->count() - 1)
        m_config->config.serialDataBits = 3;
    ui->serialPortDataBitsCombo->setCurrentIndex(m_config->config.serialDataBits);
    // Serial port stop bits
    if (m_config->config.serialStopBits > ui->serialPortStopBitsCombo->count() - 1)
        m_config->config.serialStopBits = 0;
    ui->serialPortStopBitsCombo->setCurrentIndex(m_config->config.serialStopBits);

    m_config->config.uploadBaseAddress = 0;
    ui->uploadBaseAddressEdit->setText(
                QString::number(m_config->config.uploadBaseAddress));
    ui->availableMemoryLabel->setText(tr("%1 Bytes")
        .arg(m_config->config.memorySize - m_config->config.uploadBaseAddress));

    if (m_config->config.fileType == 1)
        ui->fileTypeBitmapRadio->setChecked(true);
    else
        ui->fileTypeRawRadio->setChecked(true);

    m_serialThread.start();

    // Create About dialog
    m_aboutDialog = new AboutDialog(this);
    connect(ui->actionAbout, SIGNAL(triggered()),
            m_aboutDialog, SLOT(show()));

    bool ok = connect(ui->downloadBaseAddrEdit, SIGNAL(textEdited(QString)),
                      this, SLOT(downloadBaseAddressChanged(QString)));
    ok &= (bool)connect(ui->downloadDataLenEdit, SIGNAL(textEdited(QString)),
                        this, SLOT(downloadSizeChanged(QString)));
    ok &= (bool)connect(ui->memorySizeEdit, SIGNAL(textEdited(QString)),
                        this, SLOT(memorySizeChanged(QString)));
    ok &= (bool)connect(ui->uploadBaseAddressEdit, SIGNAL(textEdited(QString)),
                        this, SLOT(uploadBaseAddressChanged(QString)));
    ok &= (bool)connect(ui->downloadAllButton, SIGNAL(clicked()),
                        this, SLOT(downloadAllButtonClicked()));
    ok &= (bool)connect(ui->downloadRemainingButton, SIGNAL(clicked()),
                        this, SLOT(downloadRemainiglButtonClicked()));
    ok &= (bool)connect(ui->browseOutputFileButton, SIGNAL(clicked()),
                        this, SLOT(browseOutputFile()));
    Q_ASSERT(ok);
}

//--------------------------------------------------------------------
MainWindow::~MainWindow()
{
    delete ui;

    if (m_serialThread.isRunning())
    {
        m_serialThread.quit();
        m_serialThread.wait();
    }
}

//------------------------------------------------------------------------------
// Event handlers
//------------------------------------------------------------------------------

void MainWindow::closeEvent(QCloseEvent *)
{
    bool ok;

    m_config->config.mainWindowGeometry = saveGeometry();

    quint32 oldMemSize = m_config->config.memorySize;
    m_config->config.memorySize = ui->memorySizeEdit->text().toUInt(&ok);
    if (!ok)
        m_config->config.memorySize = oldMemSize;

    //m_config->config.serialPort = ui->serialPortNameCombo->text();
    m_config->config.serialParity = ui->serialPortParityCombo->currentIndex();
    m_config->config.serialSpeed = ui->serialPortSpeedCombo->currentIndex();
    m_config->config.serialFlowControl = ui->serialPortFlowCombo->currentIndex();
    m_config->config.serialDataBits = ui->serialPortDataBitsCombo->currentIndex();
    m_config->config.serialStopBits = ui->serialPortStopBitsCombo->currentIndex();

    m_config->config.fileType = ui->fileTypeRawRadio->isChecked() ? 0 : 1;

    m_config->saveSettings();
}

//--------------------------------------------------------------------
bool MainWindow::checkInputFile(const QString &fileName, qint64 *fileSize)
{
    if (fileName.isEmpty())
        return false;

    QFileInfo fi(fileName);
    if (!fi.isFile())
    {
        _error() << "Input file is a directory.";
        return false;
    }
    if (fi.size() == 0)
    {
        _error() << "Input file is empty.";
        return false;
    }

    if (fileSize)
        *fileSize = fi.size();

    return true;
}

//--------------------------------------------------------------------
void MainWindow::initiateDownload()
{
    _info() << "Starting download";

    bool ok = true;
    // Verify address and length
    if (m_config->config.downloadBaseAddress + m_config->config.downloadSize >
            m_config->config.memorySize)
    {
        _error() << "DownloadBaseAddress + DownloadSize > MemorySize";
        ok = false;
    }
    if (m_config->config.downloadSize == 0)
    {
        _error() << "DownloadSize = 0";
        ok = false;
    }

    if (ui->outputFileNameEdit->text().isEmpty())
    {
        _error() << "Unknown output file";
        ok = false;
    }

    if (ok)
    {
        auto serial = createSerialTransport();
        if (!serial->receive(m_config->config.downloadBaseAddress,
                             m_config->config.downloadSize))
        {
            _error() << "Failed to start download. Serial transport error.";
            ok = false;
        }
        else
            _info() << "Download started";
    }

    if (!ok)
    {
        QMessageBox::critical(this, "Download failed",
                              "Failed to start download. Check log for details.",
                              QMessageBox::Ok);
        return;
    }

    ui->abortButton->setEnabled(true);
    ui->startButton->setEnabled(false);
    ui->transferProgressBar->show();
}

//--------------------------------------------------------------------
void MainWindow::initiateUpload()
{
    _info() << "Starting upload";

    QByteArray data;
    bool ok = true;

    if (ui->inputFileEdit->text().isEmpty())
    {
        _error() << "Unknown output file";
        ok = false;
    }
    else
    {
        // Load input file
        if (ui->fileTypeRawRadio->isChecked())
        {
            // Raw data mode
            QFile f(ui->inputFileEdit->text());
            if (!f.open(QIODevice::ReadOnly))
            {
                _error() << "Failed to open input file. Error:" <<
                            f.errorString();
                ok = false;
            }
            else
            {
                if (f.size() + m_config->config.uploadBaseAddress >
                        m_config->config.memorySize)
                {
                    _error() << "Data too large, it won't fit in the EEPROM.";
                    ok = false;
                }
                else
                {
                    data = f.readAll();
                    ui->inputFileSizeLabel->setText(QString("%1 Byte(s)")
                                                    .arg(f.size()));
                    ui->inputDataSizeLabel->setText(QString("%1 Byte(s)")
                                                    .arg(data.length()));
                    quint32 available = m_config->config.memorySize -
                            data.length() - m_config->config.uploadBaseAddress;
                    ui->availableMemoryLabel->setText(QString("%1 Byte(s)")
                                                      .arg(available));
                }
                f.close();
            }
        }
        else if (ui->fileTypeBitmapRadio->isChecked())
        {
            // Bitmap mode
            QImage image(ui->inputFileEdit->text());
            data = Utils::covertImageToBinary(image);

            QString dump = Utils::generateCStruct(data, "image");
            fprintf(stderr, "%s\n", dump.toLocal8Bit().constData());
            _flushall();

            if (data.isEmpty())
            {
                ok = false;
            }
            else
            {
                if (data.length() + m_config->config.uploadBaseAddress >
                         m_config->config.memorySize)
                {
                    _error() << "Data too large, it won't fit in the EEPROM.";
                    ok = false;
                }
                else
                {
                    QFileInfo fi(ui->inputFileEdit->text());

                    ui->inputFileSizeLabel->setText(QString("%1 Byte(s)")
                                                    .arg(fi.size()));
                    ui->inputDataSizeLabel->setText(QString("%1 Byte(s)")
                                                    .arg(data.length()));
                    quint32 available = m_config->config.memorySize -
                            data.length() - m_config->config.uploadBaseAddress;
                    ui->availableMemoryLabel->setText(QString("%1 Byte(s)")
                                                      .arg(available));
                }
            }
        }
        else
        {
            _error() << "Ooops! Unknown file mode.";
            return;
        }
    }

    if (ok)
    {
        auto serial = createSerialTransport();
        if (!serial->send(m_config->config.uploadBaseAddress, data))
        {
            _error() << "Failed to start upload. Serial transport error.";
            ok = false;
        }
        else
            _info() << "Upload started";
    }

    if (!ok)
    {
        QMessageBox::critical(this, "Upload failed",
                              "Failed to start upload. Check log for details.",
                              QMessageBox::Ok);
        return;
    }

    ui->abortButton->setEnabled(true);
    ui->startButton->setEnabled(false);
    ui->transferProgressBar->show();
}

//--------------------------------------------------------------------
void MainWindow::updateLastDirPath(const QString &filePath)
{
    QFileInfo fi(filePath);
    auto dir = fi.dir();
    m_lastDirPath = dir.absolutePath();
}

//--------------------------------------------------------------------
void MainWindow::browseInputFile()
{
    auto fileName = QFileDialog::getOpenFileName(this, "Select input file",
                                                 m_lastDirPath, "*.*");

    qint64 fileSize;

    if (!fileName.isEmpty() && checkInputFile(fileName, &fileSize))
    {
        updateLastDirPath(fileName);

        ui->inputFileSizeLabel->setText(QString("%1 Byte(s)")
                                        .arg(fileSize));
        ui->inputFileEdit->setText(fileName);
        _info() << "Input file:" << fileName;
    }
}

//--------------------------------------------------------------------
void MainWindow::browseOutputFile()
{
    auto fileName = QFileDialog::getSaveFileName(this, "Select output file",
                                                 m_lastDirPath, "*.*");
    if (!fileName.isEmpty())
    {
        updateLastDirPath(fileName);

        ui->outputFileNameEdit->setText(fileName);
        _info() << "Output file:" << fileName;
    }
}

//--------------------------------------------------------------------
void MainWindow::log(const QString message)
{
    QListWidgetItem *item = new QListWidgetItem(message);
    ui->logListWidget->addItem(item);
    ui->logListWidget->scrollToBottom();
}

//--------------------------------------------------------------------
void MainWindow::startButtonClicked()
{
    // Set transfer mode
    switch (ui->tabWidget->currentIndex())
    {
        case 0: // Write mode
            initiateUpload();
            break;

        case 1: // Read mode
            initiateDownload();
            break;

        default:
            break;
    }
}

//--------------------------------------------------------------------
void MainWindow::downloadAllButtonClicked()
{
    ui->downloadDataLenEdit->setText(QString::number(m_config->config.memorySize));
    ui->downloadBaseAddrEdit->setText("0");

    m_config->config.downloadBaseAddress = 0;
    m_config->config.downloadSize = m_config->config.memorySize;
}

//--------------------------------------------------------------------
void MainWindow::downloadRemainiglButtonClicked()
{
    qint64 length = m_config->config.memorySize -
            m_config->config.downloadBaseAddress;
    if (length > 0)
    {
        ui->downloadDataLenEdit->setText(QString::number(length));
        ui->downloadBaseAddrEdit->setText(QString::number(m_config->config.downloadBaseAddress));
        m_config->config.downloadSize = length;
    }
}

//--------------------------------------------------------------------
void MainWindow::memorySizeChanged(const QString &text)
{
    auto edit = qobject_cast<QLineEdit*>(sender());
    if (!edit)
        return;

    quint32 value;

    if (!Utils::parseInputNumber(text, &value) || value == 0)
    {
        edit->setStyleSheet(EditValueInvalidCSS);
    }
    else
    {
        edit->setStyleSheet("");
        m_config->config.memorySize = value;
    }
}

//--------------------------------------------------------------------
void MainWindow::uploadBaseAddressChanged(const QString &text)
{
    auto edit = qobject_cast<QLineEdit*>(sender());
    if (!edit)
        return;

    quint32 value;

    if (!Utils::parseInputNumber(text, &value) ||
            value >= m_config->config.memorySize)
    {
        edit->setStyleSheet(EditValueInvalidCSS);
    }
    else
    {
        edit->setStyleSheet("");
        m_config->config.uploadBaseAddress = value;
    }
}

//--------------------------------------------------------------------
void MainWindow::downloadBaseAddressChanged(const QString &text)
{
    auto edit = qobject_cast<QLineEdit*>(sender());
    if (!edit)
        return;

    quint32 value;

    if (!Utils::parseInputNumber(text, &value) ||
            value >= m_config->config.memorySize)
    {
        edit->setStyleSheet(EditValueInvalidCSS);
    }
    else
    {
        edit->setStyleSheet("");
        m_config->config.downloadBaseAddress = value;
    }
}

//--------------------------------------------------------------------
void MainWindow::downloadSizeChanged(const QString &text)
{
    auto edit = qobject_cast<QLineEdit*>(sender());
    if (!edit)
        return;

    quint32 value;

    if (!Utils::parseInputNumber(text, &value) || value == 0 ||
            value + m_config->config.downloadBaseAddress > m_config->config.memorySize)
    {
        edit->setStyleSheet(EditValueInvalidCSS);
    }
    else
    {
        edit->setStyleSheet("");
        m_config->config.downloadSize = value;
    }
}

//--------------------------------------------------------------------
Serial *MainWindow::createSerialTransport()
{
    auto serial = new Serial(ui->serialPortNameCombo->currentText(),
                             ui->serialPortSpeedCombo->currentText().toUInt());

    bool ok = (bool)connect(serial, SIGNAL(receiveFinished(Serial::Result,QByteArray)),
                            this, SLOT(serialReceiveFinished(Serial::Result,QByteArray)));
    ok &= (bool)connect(serial, SIGNAL(transferProgress(int,int)),
                        this, SLOT(serialTransferProgress(int,int)));
    ok &= (bool)connect(serial, SIGNAL(transmitFinished(Serial::Result)),
                        this, SLOT(serialTransmitFinished(Serial::Result)));
    ok &= (bool)connect(ui->abortButton, SIGNAL(clicked()),
                        serial, SLOT(abort()));
    Q_ASSERT(ok);

    // TODO serial transport should be run in a separate thread
    //serial->moveToThread(&m_serialThread);

    return serial;
}

//--------------------------------------------------------------------
void MainWindow::destroySerialTransport(Serial *serial)
{
    Q_ASSERT(serial);

    serial->disconnect();
    serial->deleteLater();
    serial = nullptr;
}

//--------------------------------------------------------------------
void MainWindow::enumerateSerialPorts()
{
    ui->serialPortNameCombo->clear();

    auto ports = Serial::enumeratePorts();
    auto savedIndex = ports.indexOf(m_config->config.serialPort);

    ui->serialPortNameCombo->addItems(ports);
    if (savedIndex > -1)
        ui->serialPortNameCombo->setCurrentIndex(savedIndex);
    else
        ui->serialPortNameCombo->setCurrentIndex(0);
}

//--------------------------------------------------------------------
void MainWindow::serialTransmitFinished(Serial::Result result)
{
    auto serial = qobject_cast<Serial*>(sender());
    if (serial)
        destroySerialTransport(serial);

    switch (result.error)
    {
        case Serial::Result::ENoError:
            _info() << "Upload finished.";
            break;

        case Serial::Result::EAbort:
            _warning() << "Upload aborted.";
            break;

        case Serial::Result::EChecksumError:
            _warning() << "Upload failed. Checksum error.";
            break;

        case Serial::Result::EConnectionError:
            _warning() << "Upload failed. Connection error.";
            break;
    }

    _info() << result.transferedLength << "of" << result.dataLength
               << "Byte(s) uploaded";

    ui->abortButton->setEnabled(false);
    ui->startButton->setEnabled(true);
    ui->transferProgressBar->hide();
}

//--------------------------------------------------------------------
void MainWindow::serialReceiveFinished(Serial::Result result, QByteArray data)
{
    auto serial = qobject_cast<Serial*>(sender());
    if (serial)
        destroySerialTransport(serial);

    switch (result.error)
    {
        case Serial::Result::ENoError:
            _info() << "Download finished.";
            break;

        case Serial::Result::EAbort:
            _warning() << "Download aborted.";
            break;

        case Serial::Result::EChecksumError:
            _warning() << "Download failed. Checksum error.";
            break;

        case Serial::Result::EConnectionError:
            _warning() << "Download failed. Connection error.";
            break;
    }

    _info() << result.transferedLength << "of" << result.dataLength
               << "Byte(s) downloaded";

    auto outFileName = ui->outputFileNameEdit->text();

    if (!data.isEmpty() && !outFileName.isEmpty())
    {
        _info() << "Saving data to" << outFileName;

        QFile f(outFileName);
        if (!f.open(QIODevice::WriteOnly))
        {
            _warning() << "Failed to open output file";
            return;
        }
        f.write(data);
        f.flush();
        f.close();
    }

    ui->abortButton->setEnabled(false);
    ui->startButton->setEnabled(true);
    ui->transferProgressBar->hide();
}

//--------------------------------------------------------------------
void MainWindow::serialTransferProgress(int dataLength, int transmittedLength)
{
    ui->transferProgressBar->setMaximum(dataLength);
    ui->transferProgressBar->setValue(transmittedLength);
}
