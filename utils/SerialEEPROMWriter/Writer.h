#ifndef WRITER_H
#define WRITER_H

#include <QObject>
#include <QQueue>
#include <QFuture>
#include <QTimer>

#include "qextserialport.h"

typedef struct
{
    int id;
    quint32 baseAddress;
    QByteArray data;
    quint16 checksum;
} WriterQueueItem;

class Writer : public QObject
{
    Q_OBJECT

public:
    explicit Writer(QObject *parent = 0);
    ~Writer();

    int addTask(const quint32 baseAddress, const QByteArray data);
    void processTasks();
    QList<int> taskIds() const;
    bool removeTask(int id);
    void removeAllTasks();
    bool isProcessing() const;

private:
    QQueue<WriterQueueItem> m_taskQueue;
    unsigned int m_idCounter;
    QextSerialPort *m_serial;
    QFuture<void> m_writeFuture;
    bool m_isAbortRequested;
    QByteArray m_serialResponse;
    QTimer *m_serialTimer;
    int m_serialTimerTicks;

    void log(const QString message);
    bool setupSerialPort();
    void serialWrite();

signals:
    void logMessage(const QString message);
    void taskFinished(const int id, const int errorCode);
    void processingStarted();
    void processingFinished();
    void progress(int percent);

public slots:
    void closeSerialPort();

private slots:
    void serialReadyRead();
    void serialBytesWritten(qint64 count);
    void serialTimerTick();
//    void closeSerialPort();

public slots:
    void abortProcessing();

};

#endif // WRITER_H
