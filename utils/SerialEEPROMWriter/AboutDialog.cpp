#include "AboutDialog.h"
#include "ui_AboutDialog.h"

#include <QDesktopServices>
#include <QUrl>

AboutDialog::AboutDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::AboutDialog)
{
    setWindowFlags(Qt::Window | Qt::WindowTitleHint);

    ui->setupUi(this);

#ifdef APP_VERSION
    QString text = ui->label_2->text();
    text.replace("%APP_VERSION%", QString(APP_VERSION));
    ui->label_2->setText(text);
#endif

    connect(ui->label_2, SIGNAL(linkActivated(QString)),
            this, SLOT(linkActivated(QString)));
}

AboutDialog::~AboutDialog()
{
    delete ui;
}

void AboutDialog::linkActivated(QString url)
{
    QDesktopServices::openUrl(url);
}
