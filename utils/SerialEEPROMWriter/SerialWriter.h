#ifndef SERIALWRITER_H
#define SERIALWRITER_H

#include <QObject>
#include <QQueue>

#include "SerialWriter_p.h"

class SerialWriter : public QObject
{
    Q_OBJECT

public:
    explicit SerialWriter(QObject *parent = 0);

    int addTask(const quint32 baseAddress, const QByteArray data);
    void processTasks();

private:
    QQueue<QueueItem> m_taskQueue;
    unsigned int m_idCounter;
    DataWriter *m_dataWriter;

    quint16 calculateChecksum(const QByteArray data) const;
    void log(const QString message);

signals:
    void logMessage(const QString message);
    void taskFinished(const int id, const int errorCode);
    void processingStarted();
    void processingFinished();
    void progress(int percent);

private slots:
    void requeueTasks();

public slots:
    void abortProcessing();
};

#endif // SERIALWRITER_H
