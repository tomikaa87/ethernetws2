#ifndef WRITERSKELETON_H
#define WRITERSKELETON_H

#include <QObject>
#include <QObject>
#include <QQueue>
#include <QFuture>
#include <QTimer>

typedef struct
{
    int id;
    quint32 baseAddress;
    QByteArray data;
    quint16 checksum;
} WriterQueueItem;

class Writer : public QObject
{
    Q_OBJECT

public:
    explicit Writer(QObject *parent = 0);
    ~Writer();

    int addTask(const quint32 baseAddress, const QByteArray data);
    void processTasks();
    QList<int> taskIds() const;
    bool removeTask(int id);
    void removeAllTasks();
    bool isProcessing() const;

signals:
    void logMessage(const QString message);
    void taskFinished(const int id, const int errorCode);
    void processingStarted();
    void processingFinished();
    void progress(int percent);

public slots:
    void abortProcessing();

};

#endif // WRITERSKELETON_H
