#include "Serial.h"

#include <QDebug>

Serial::Serial(QObject *parent) :
    QObject(parent)
{
    m_serial = new QSerialPort(this);
    connect(m_serial, SIGNAL(readyRead()), this, SLOT(serialReadyRead()));

    qDebug() << "Initialized";

    QMetaObject::invokeMethod(this, "openPort", Qt::QueuedConnection);
}

void Serial::openPort()
{
    qDebug() << "Opening serial port";

    if (m_serial->isOpen())
        return;

    m_serial->setPortName("COM8");
    if (!m_serial->open(QIODevice::ReadWrite))
    {
        qDebug() << "Serial port open error:" << m_serial->errorString();
        return;
    }

    if (!m_serial->setBaudRate(9600) ||
        !m_serial->setDataBits(QSerialPort::Data8) ||
        !m_serial->setParity(QSerialPort::NoParity) ||
        !m_serial->setFlowControl(QSerialPort::NoFlowControl) ||
        !m_serial->setStopBits(QSerialPort::OneStop))
        return;
}

void Serial::serialReadyRead()
{
    QByteArray data = m_serial->readAll();
    qDebug() << "Received data:" << data.toHex().toUpper();

    unsigned address;
    QByteArray parsed;
    if (parseDataFrame(data, address, parsed))
    {
        qDebug() << "Valid frame received. Address:" << QString::number(address, 16).toUpper()
                    << "Data:" << parsed.toHex().toUpper();

        // ACK: Send back a valid frame without data
        m_serial->write(makeDataFrame(0));
    }

    // TODO nACK ?
}

/*
 * Data frame structure:
 *
 * FrameCRC : 2 bytes
 * Address  : 4 bytes
 * DataLen  : 1 byte
 * Data     : [DataLen] number of data bytes
 */
QByteArray Serial::makeDataFrame(unsigned address, const QByteArray &data)
{
    if (data.length() > 255)
        return QByteArray();

    QByteArray frame;

    // Address
    frame.append(static_cast<quint8>((address >> 24) & 0xFF));
    frame.append(static_cast<quint8>((address >> 16) & 0xFF));
    frame.append(static_cast<quint8>((address >> 8) & 0xFF));
    frame.append(static_cast<quint8>(address & 0xFF));

    // DataLen
    frame.append(static_cast<quint8>(data.length() & 0xFF));

    // Data
    frame.append(data);

    // Frame CRC
    quint16 checksum = qChecksum(frame.constData(), frame.length());
    frame.prepend(static_cast<quint8>(checksum & 0xFF));        // LSB
    frame.prepend(static_cast<quint8>((checksum >> 8) & 0xFF)); // MSB

    qDebug() << "Data frame:" << frame.toHex().toUpper();

    return frame;
}

//--------------------------------------------------------------------------------------------------
bool Serial::parseDataFrame(const QByteArray &frame, unsigned &address, QByteArray &data)
{
    // Frame is smaller than a valid empty frame
    if (frame.length() < 7)
        return false;

    quint16 frameChecksum = (static_cast<quint8>(frame[0]) << 8) + static_cast<quint8>(frame[1]);
    QByteArray truncated = frame.right(frame.length() - 2);
    quint16 calcChecksum = qChecksum(truncated.constData(), truncated.length());
    if (frameChecksum != calcChecksum)
    {
        qDebug() << "Frame checksum error";
        return false;
    }

    address = (static_cast<quint8>(frame[2]) << 24) +
            (static_cast<quint8>(frame[3]) << 16) +
            (static_cast<quint8>(frame[4]) << 8) +
            static_cast<quint8>(frame[5]);

    int length = static_cast<quint8>(frame[6]);

    data = frame.right(frame.length() - 7);
    if (data.length() != length)
    {
        qDebug() << "Data length mismatch:" << data.length() << "(expected:" << length << ")";
        return false;
    }

    qDebug() << "Data frame parsed. Address:" << QString("%1").arg(address, 8, 16, QChar('0')).toUpper()
                << "Data:" << data.toHex().toUpper();

    return true;
}
