#-------------------------------------------------
#
# Project created by QtCreator 2014-01-04T22:04:27
#
#-------------------------------------------------

QT       += core serialport

QT       -= gui

TARGET = SerialEEPROMWriterTarget
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp \
    Serial.cpp

HEADERS += \
    Serial.h
