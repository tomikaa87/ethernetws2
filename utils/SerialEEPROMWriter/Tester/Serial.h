#ifndef SERIAL_H
#define SERIAL_H

#include <QObject>
#include <QtSerialPort/QSerialPort>

class Serial : public QObject
{
    Q_OBJECT

public:
    explicit Serial(QObject *parent = 0);


private:
    QSerialPort *m_serial;

    QByteArray makeDataFrame(unsigned address, const QByteArray &data = QByteArray());
    bool parseDataFrame(const QByteArray &frame, unsigned &address, QByteArray &data);

private slots:
    void openPort();
    void serialReadyRead();
};

#endif // SERIAL_H
