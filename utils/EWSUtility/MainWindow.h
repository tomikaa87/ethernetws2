#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "Configuration.h"
#include "JobManager.h"
#include "modules/FontImageConverter.h"
#include "AboutDialog.h"

#include <QMainWindow>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(Configuration *config, JobManager *jobManager,
                        QWidget *parent = 0);
    ~MainWindow();

private:
    Ui::MainWindow *ui;
    Configuration *m_config;
    JobManager *m_jobManager;
    FontImageConverter *m_fic;
    AboutDialog *m_aboutDialog;

    void setupJobsTableWidget();
    void loadJobsToListWidget();

private slots:
    void removeSelectedJobs();
    void removeAllJobs();

    void fontImageConverter_browseClicked();
    void fontImageConverter_zoomClicked();

    void showAboutDialog();
};

#endif // MAINWINDOW_H
