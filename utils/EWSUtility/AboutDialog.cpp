#include "AboutDialog.h"
#include "ui_AboutDialog.h"
#include "Version.h"

#include <QDesktopServices>
#include <QUrl>

AboutDialog::AboutDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::AboutDialog)
{
    setWindowFlags(Qt::Dialog | Qt::WindowCloseButtonHint |
                   Qt::WindowTitleHint);
    ui->setupUi(this);

    ui->versionLabel->setText(
                tr("<p align=\"center\">Version: %1</p>").arg(appVersion()));
    layout()->setSizeConstraint( QLayout::SetFixedSize );
}

AboutDialog::~AboutDialog()
{
    delete ui;
}

void AboutDialog::openUrl(const QString &url)
{
    QDesktopServices::openUrl(QUrl(url));
}
