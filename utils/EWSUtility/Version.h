#ifndef VERSION_H
#define VERSION_H

#define APP_REVISION    "$Rev: 169 $"
#define APP_VERSION     "1.0"

#include <QRegExp>
#include <QString>

inline QString appRevision() {
    QRegExp re(".*Rev\\:\\s?(\\d+).*");
    if (re.exactMatch(APP_REVISION)) {
        return re.cap(1);
    } else
        return "-UNKNOWN-";
}

inline QString appVersion() {
    return QString(APP_VERSION) + "-r" + appRevision();
}

#endif // VERSION_H
