#include "JobManager.h"
#include "Debug.h"

#include <QFile>
#include <QStringList>

/*** Constructor / destructor *************************************************/

JobManager::JobManager(Configuration *config, QObject *parent) :
    QObject(parent), m_config(config), m_jobId(0)
{
    // Load job queue
    QStringList jobs = m_config->jobs();
    foreach (QString job, jobs) {
        Job j = job;
        j.id = newJobId();

        if (j.id % 3 == 0)
            setJobFlags(j, JobFinished);

        _debug("Loaded:" << j.toString());
        m_jobQueue.enqueue(j);
    }
    _debug(m_jobQueue.count() << "job(s) loaded");
}

JobManager::~JobManager()
{
    // Save job queue
    _debug("Saving" << m_jobQueue.count() << "job(s)");
    QStringList jobs;
    foreach (Job job, m_jobQueue) {
        jobs << job.toString();
    }
    m_config->saveJobs(jobs);
}

/*** API function implementations *********************************************/

int JobManager::addWriteJob(const QString &sourceFileName,
                            const int eepromAddress, const JobFlags flags)
{
    if (!checkFlags(flags))
        return 0;

//    if (!checkSourceFile(sourceFileName))
//        return 0;

    // Obtain source file size
    QFile f(sourceFileName);
    if (!f.open(QIODevice::ReadOnly)) {
        m_lastError = "Couldn't open source file";
        return 0;
    }
    int length = f.size();
    _debug("File length:" << length);
    f.close();

    if (!checkEepromAddress(eepromAddress, length))
        return 0;

    Job job;
    job.id = newJobId();
    job.sourceFileName = sourceFileName;
    job.eepromAddress = eepromAddress;
    job.length = length;
    job.flags = flags;
    job.destinationFileName = "";
    job.type = WriteJob;
    m_jobQueue.enqueue(job);

    _debug("Job added, ID:" << job.id);

    m_lastError = tr("No error");

    return job.id;
}

int JobManager::addReadJob(const QString &destinationFileName,
                           const int eepromAddress, const int length,
                           const JobFlags flags)
{
    if (!checkFlags(flags))
        return 0;

    if (destinationFileName == "") {
        m_lastError = tr("Destination file name is invalid");
        return 0;
    }

    if (!checkEepromAddress(eepromAddress, length))
        return 0;

    Job job;
    job.id = newJobId();
    job.destinationFileName = destinationFileName;
    job.eepromAddress = eepromAddress;
    job.length = length;
    job.flags = flags;
    job.sourceFileName = "";
    job.type = ReadJob;
    m_jobQueue.enqueue(job);

    _debug("Job added, ID:" << job.id);

    m_lastError = tr("No error");

    return job.id;
}

int JobManager::addConvertImageJob(const QString &sourceFileName,
                                   const QString &destinationFileName,
                                   const JobFlags flags)
{
    if (!checkFlags(flags))
        return 0;

    if (destinationFileName == "") {
        m_lastError = tr("Destination file name is invalid");
        return 0;
    }

    // Obtain source file size
    QFile f(sourceFileName);
    if (!f.open(QIODevice::ReadOnly)) {
        m_lastError = "Couldn't open source file";
        return 0;
    }
    f.close();

    Job job;
    job.id = newJobId();
    job.destinationFileName = destinationFileName;
    job.sourceFileName = sourceFileName;
    job.eepromAddress = 0;
    job.length = 0;
    job.flags = flags;
    job.type = ConvertImageJob;
    m_jobQueue.enqueue(job);

    _debug("Job added, ID:" << job.id);

    m_lastError = tr("No error");

    return job.id;
}

QList<JobManager::Job> JobManager::jobs() const
{
    QList<Job> jobList;
    foreach (Job job, m_jobQueue) {
        jobList.append(job);
    }
    return jobList;
}

JobManager::Job JobManager::jobById(const int id) const
{
    foreach (Job j, m_jobQueue) {
        if (j.id == id)
            return j;
    }

    return Job();
}

bool JobManager::removeJobById(const int id)
{
    Job jobToRemove;
    foreach (Job j, m_jobQueue) {
        if (j.id == id) {
            jobToRemove = j;
            break;
        }
    }
    if (jobToRemove.id > 0) {
        m_jobQueue.removeAll(jobToRemove);
        m_lastError = tr("No error");
        return true;
    } else {
        m_lastError = tr("No such job with the given ID");
        return false;
    }
}

void JobManager::removeAllJobs()
{
    m_jobQueue.clear();
}

/*** Private methods **********************************************************/

int JobManager::newJobId()
{
    return ++m_jobId;
}

bool JobManager::checkEepromAddress(const int address, const int length)
{
    if (address < m_config->eepromMinAddress()) {
        m_lastError = tr("EEPROM address is less than the minimum allowed");
        return false;
    }

    if (address > m_config->eepromMaxAddress()) {
        m_lastError = tr("EEPROM address is greater than the maximum allowed");
        return false;
    }

    if (address + length - 1 > m_config->eepromMaxAddress()) {
        m_lastError = tr("Data too long and won't fit in the EEPROM");
        return false;
    }

    m_lastError = tr("No error");

    return true;
}

//bool JobManager::checkSourceFile(const QString &fileName)
//{
//    QFile f(fileName);
//    if (!f.open(QIODevice::ReadOnly)) {
//        m_lastError = "Couldn't open source file";
//        return false;
//    }
//    if (f.size() == 0) {
//        m_lastError = "Null size file";
//        return false;
//    }
//    f.close();

//    m_lastError = tr("No error");

//    return true;
//}

bool JobManager::checkFlags(const JobFlags flags)
{
    if (flags & JobFinished) {
        m_lastError = tr("JobFinished flag is not allowed to be set manually");
        return false;
    }

    m_lastError = tr("No error");

    return true;
}

void JobManager::setJobFlags(Job &job, const JobFlags flags)
{
    job.flags |= flags;
}

void JobManager::clearJobFlags(Job &job, const JobFlags flags)
{
    job.flags &= 0xFFFFFFFF - flags;
}

bool JobManager::isJobFlagSet(const Job &job, const JobFlags flag)
{
    return (job.flags & flag) > 0;
}
