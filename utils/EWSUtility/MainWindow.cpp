#include "MainWindow.h"
#include "ui_MainWindow.h"
#include "Version.h"

#include <QMessageBox>
#include <QFileDialog>
#include <QFileInfo>

const QString FileDialogImageFilters = "Images (*png *.bmp *.jpg *.xpm)";

MainWindow::MainWindow(Configuration *config, JobManager *jobManager,
                       QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow), m_config(config), m_jobManager(jobManager)
{
    ui->setupUi(this);

    loadJobsToListWidget();
    setupJobsTableWidget();

    m_fic = new FontImageConverter(this);

    m_aboutDialog = new AboutDialog(this);

    _debug("Version:" << appVersion());
}

MainWindow::~MainWindow()
{
    delete ui;
}

/*** Private methods **********************************************************/

void MainWindow::setupJobsTableWidget()
{
    ui->jobsTableWidget->setColumnWidth(1, 150);
    ui->jobsTableWidget->setColumnWidth(0, 350);
    ui->jobsTableWidget->verticalHeader()->hide();
}

void MainWindow::loadJobsToListWidget()
{
    ui->jobListWidget->clear();
    ui->jobsTableWidget->clearContents();

    foreach (JobManager::Job j, m_jobManager->jobs()) {
        QString itemText = j.description();
        //QListWidgetItem *item = new QListWidgetItem(itemText, ui->jobListWidget);

        // Create table widget item job description
        QTableWidgetItem *descItem = new QTableWidgetItem();
        switch (j.type) {
        case JobManager::ReadJob:
            descItem->setIcon(QIcon(":/icons/ReadData"));
            break;
        case JobManager::WriteJob:
            descItem->setIcon(QIcon(":/icons/WriteData"));
            break;
        case JobManager::ConvertImageJob:
            descItem->setIcon(QIcon(":/icons/ConvertImage"));
        default:
            break;
        }
        descItem->setText(itemText);

        // Create table widget item for job description
        //QTableWidgetItem *progressItem = new QTableWidgetItem("n/a %");
        int row = ui->jobsTableWidget->rowCount();
        ui->jobsTableWidget->insertRow(row);
        ui->jobsTableWidget->setItem(row, 0, descItem);



//        if (j.flags & JobManager::JobFinished) {
//            item->setTextColor(Qt::gray);
//        }

//        item->setData(Qt::UserRole, j.id);
//        ui->jobListWidget->addItem(item);
    }
}

/*** Private slots ************************************************************/

void MainWindow::removeSelectedJobs()
{
    if (ui->jobListWidget->count() > 0 &&
            ui->jobListWidget->selectedItems().count() > 0) {
        foreach (QListWidgetItem *item, ui->jobListWidget->selectedItems()) {
            _debug("Removing job:" << item->data(Qt::UserRole).toInt());
            if (!m_jobManager->removeJobById(item->data(Qt::UserRole).toInt())) {
                QMessageBox::warning(this, tr("Remove job"),
                                     tr("Couldn't remove job (ID: %1). Error: %2.")
                                     .arg(item->data(Qt::UserRole).toInt())
                                     .arg(m_jobManager->errorString()));
            }
        }
        loadJobsToListWidget();
    } else {
        QMessageBox::warning(this, tr("Remove job"),
                             tr("Sorry, I can't do that. :("));
    }
}

void MainWindow::removeAllJobs()
{
    if (m_jobManager->jobs().count()) {
        if (QMessageBox::question(this, tr("Remove all jobs"),
                          tr("Do you really want to remove all jobs?"),
                          QMessageBox::Yes, QMessageBox::No) == QMessageBox::Yes) {
            m_jobManager->removeAllJobs();
            loadJobsToListWidget();
        }
    } else {
        QMessageBox::warning(this, tr("Remove all jobs"),
                             tr("Sorry, I can't do that. :("));
    }
}

/*** Font Image Converter slots ***********************************************/

void MainWindow::fontImageConverter_browseClicked()
{
    // Browse image file
    QString fileName =
            QFileDialog::getOpenFileName(this, tr("Select font image"),
                                         ".", FileDialogImageFilters);
    if (fileName.isEmpty())
        return;

    // Open attributes file for the font
    QFileInfo fi(fileName);
    QString attrFileName = fi.absolutePath() + "/" + fi.baseName() + ".attr";
    fi.setFile(attrFileName);
    if (!fi.exists()) {
        QMessageBox::warning(this, tr("Font attributes file error"),
                             tr("Couldn't open attributes file for font"));
        return;
    }

    _debug("Font image file:" << fileName);
    _debug("Attributes file:" << attrFileName);

    ui->fontImageNameEdit->setText(fileName);
    m_fic->setAttributesFileName(attrFileName);
    m_fic->setFileName(fileName);

    // Generate preview
    ui->fontImagePreviewLabel->setPixmap(m_fic->previewImage());
}

void MainWindow::fontImageConverter_zoomClicked()
{
    QPushButton *b = qobject_cast<QPushButton *>(sender());
    if (!b)
        return;

    if (m_fic->fileName().isEmpty())
        return;

    if (b == ui->fontImagePreview1x) {
        ui->fontImagePreviewLabel->setPixmap(m_fic->previewImage());
    } else if (b == ui->fontImagePreview2x) {
        ui->fontImagePreviewLabel->setPixmap(m_fic->previewImage(2.0));
    } else if (b == ui->fontImagePreview4x) {
        ui->fontImagePreviewLabel->setPixmap(m_fic->previewImage(4.0));
    }
}

/*** Other slots **************************************************************/

void MainWindow::showAboutDialog()
{
    m_aboutDialog->show();
}
