#ifndef CONFIGURATION_H
#define CONFIGURATION_H

#include <QObject>
#include <QStringList>

class QSettings;

class Configuration : public QObject
{
    Q_OBJECT

public:
    explicit Configuration(QObject *parent = 0);

    QByteArray mainWindowGeometry() const;
    void setMainWindowGeometry(const QByteArray &geometry);

    int eepromMinAddress() const;
    void setEepromMinAddress(const int address);

    int eepromMaxAddress() const;
    void setEepromMaxAddress(const int address);

    int eepromSize() const;
    void setEepromSize(const int size);

    QString serialPort() const;
    void setSerialPort(const QString &port);

    int serialBaud() const;
    void setSerialBaud(const int baud);

    QStringList jobs() const;
    void saveJobs(const QStringList &jobs);

private:
    QSettings *m_settings;
};

#endif // CONFIGURATION_H
