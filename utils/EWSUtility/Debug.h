#ifndef DEBUG_H
#define DEBUG_H

#include <QDebug>

#define _debug(msg) qDebug() << "[D][" << __PRETTY_FUNCTION__ << "at" << __LINE__ << "]" << msg
#define _warning(msg) qWarning() << "[W][" << __PRETTY_FUNCTION__ << "at" << __LINE__ << "]" << msg

#endif // DEBUG_H
