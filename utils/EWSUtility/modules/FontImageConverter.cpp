#include "FontImageConverter.h"
#include "Debug.h"

#include <QFile>

FontImageConverter::FontImageConverter(QObject *parent) :
    QObject(parent)
{
}

/*** API methods **************************************************************/

void FontImageConverter::setFileName(const QString &fileName)
{
    m_fileName = fileName;
}

QString FontImageConverter::fileName() const
{
    return m_fileName;
}

void FontImageConverter::setAttributesFileName(const QString &fileName)
{
    m_attrFileName = fileName;
}

QString FontImageConverter::attributesFileName() const
{
    return m_attrFileName;
}

QPixmap FontImageConverter::previewImage(float scale) const
{
    if (m_fileName.isEmpty()) {
        _warning("Cannot generate preview image, invalid file name");
        return QPixmap();
    }

    QPixmap p(m_fileName);
    if (p.isNull()) {
        _warning("Cannot generate preview image, format error");
        return QPixmap();
    }

    _debug("Preview image loaded, size =" << p.size() <<
           " colors =" << p.colorCount() << " scale =" << scale);

    if (scale == 1.0)
        return p;
    else
        return p.scaled(QSize(p.width() * scale, p.height() * scale),
                        Qt::KeepAspectRatio, Qt::FastTransformation);
}
