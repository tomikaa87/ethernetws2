#ifndef FONTIMAGECONVERTER_H
#define FONTIMAGECONVERTER_H

#include <QObject>
#include <QPixmap>

class FontImageConverter : public QObject
{
    Q_OBJECT

public:
    explicit FontImageConverter(QObject *parent = 0);

    void setFileName(const QString &fileName);
    QString fileName() const;

    void setAttributesFileName(const QString &fileName);
    QString attributesFileName() const;

    QPixmap previewImage(float scale = 1.0) const;

private:
    QString m_fileName;
    QString m_attrFileName;
};

#endif // FONTIMAGECONVERTER_H
