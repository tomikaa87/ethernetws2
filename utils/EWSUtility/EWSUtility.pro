#-------------------------------------------------
#
# Project created by QtCreator 2012-02-15T22:03:30
#
#-------------------------------------------------

QT       += core gui

TARGET = EWSUtility
TEMPLATE = app


SOURCES += main.cpp\
        MainWindow.cpp \
    JobManager.cpp \
    Configuration.cpp \
    modules/FontImageConverter.cpp \
    modules/ImageConverter.cpp \
    AboutDialog.cpp

HEADERS  += MainWindow.h \
    JobManager.h \
    Configuration.h \
    Debug.h \
    modules/FontImageConverter.h \
    modules/ImageConverter.h \
    AboutDialog.h \
    Version.h

FORMS    += MainWindow.ui \
    AboutDialog.ui

RESOURCES += \
    gfx.qrc

RC_FILE = app.rc
