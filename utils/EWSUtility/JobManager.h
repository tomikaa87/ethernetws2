#ifndef JOBMANAGER_H
#define JOBMANAGER_H

#include "Configuration.h"
#include "Debug.h"

#include <QObject>
#include <QQueue>
#include <QRegExp>

class JobManager : public QObject
{
    Q_OBJECT

public:

    enum JobType {
        WriteJob,
        ReadJob,
        ConvertImageJob
    };

    enum JobFlags {
        NoFlags = 0,
        ConvertImageBeforeWrite = 0x01,
        JobFinished = 0x80000000
    };

    struct Job {
        int id;
        JobType type;
        unsigned int flags;
        QString sourceFileName;
        QString destinationFileName;
        int eepromAddress;
        int length;

        bool operator ==(const Job &other) {
            if (destinationFileName == other.destinationFileName &&
                    eepromAddress == other.eepromAddress &&
                    flags == other.flags &&
                    length == other.length &&
                    sourceFileName == other.sourceFileName &&
                    type == other.type &&
                    id == other.id) {
                return true;
            }

            return false;
        }

        QString toString() {
            return QString("<job type='%1' flags='%2' src='%3' dst='%4' base='%5' len='%6'>")
                    .arg((int)type).arg(flags).arg(sourceFileName)
                    .arg(destinationFileName).arg(eepromAddress).arg(length);
        }

        QString description() {
            switch (type) {
            case WriteJob:
                return tr("Job %1 [%2 -> EEPROM(@%3, %4)]")
                        .arg(id).arg(sourceFileName).arg(eepromAddress)
                        .arg(length);
            case ReadJob:
                return tr("Job %1 [EEPROM(@%2, %3) -> %4]")
                        .arg(id).arg(eepromAddress).arg(length)
                        .arg(destinationFileName);
            case ConvertImageJob:
                return tr("Job %1 [Convert %2 -> %3]")
                        .arg(id).arg(sourceFileName).arg(destinationFileName);
            }

            return tr("Job %1 [UNKNOWN TYPE]").arg(id);
        }

        Job () : id(0) { }

        Job (const QString &str) {
            QRegExp re;
            re.setPattern("<job type='(\\d+)' flags='(\\d+)' src='(.*)' dst='(.*)' base='(\\d+)' len='(\\d+)'>");
            if (re.exactMatch(str)) {
                type = static_cast<JobType>(re.cap(1).toInt());
                flags = static_cast<JobFlags>(re.cap(2).toInt());
                sourceFileName = re.cap(3);
                destinationFileName = re.cap(4);
                eepromAddress = re.cap(5).toInt();
                length = re.cap(6).toInt();
                _debug("fromString OK");
            }
        }
    };

    explicit JobManager(Configuration *config, QObject *parent = 0);
    ~JobManager();

    int addWriteJob(const QString &sourceFileName, const int eepromAddress,
                    const JobFlags flags = NoFlags);
    int addReadJob(const QString &destinationFileName, const int eepromAddress,
                   const int length, const JobFlags flags = NoFlags);
    int addConvertImageJob(const QString &sourceFileName,
                           const QString &destinationFileName,
                           const JobFlags flags = NoFlags);

    QList<Job> jobs() const;
    Job jobById(const int id) const;
    bool removeJobById(const int id);
    void removeAllJobs();

    QString errorString() const {
        return m_lastError;
    }

signals:
    void jobFinished(const int id);

public slots:

private:
    Configuration *m_config;
    QQueue<Job> m_jobQueue;
    int m_jobId;
    QString m_lastError;

    int newJobId();

    bool checkEepromAddress(const int address, const int length);
//    bool checkSourceFile(const QString &fileName);
    bool checkFlags(const JobFlags flags);

    void setJobFlags(Job &job, const JobFlags flags);
    void clearJobFlags(Job &job, const JobFlags flags);
    bool isJobFlagSet(const Job &job, const JobFlags flag);
};

#endif // JOBMANAGER_H
