#include "Configuration.h"
#include "Debug.h"

#include <QSettings>

/*** Configuration key constants **********************************************/

static const QString CKMainWindowGeometry = "MainWindow/Geometry";

static const QString CKEepromMinAddress = "EEPROM/MinAddress";
static const QString CKEepromMaxAddress = "EEPROM/MaxAddress";
static const QString CKEepromSize = "EEPROM/Size";

static const QString CKSerialPort = "Serial/Port";
static const QString CKSerialBaud = "Serial/Baud";

static const QString CKJobList = "Jobs/JobList";

/*** Constructor / destructor *************************************************/

Configuration::Configuration(QObject *parent) :
    QObject(parent)
{
    m_settings = new QSettings("ewsutility.conf", QSettings::IniFormat, this);
}

/*** API function implementations *********************************************/

QByteArray Configuration::mainWindowGeometry() const
{
    return m_settings->value(CKMainWindowGeometry).toByteArray();
}

void Configuration::setMainWindowGeometry(const QByteArray &geometry)
{
    m_settings->setValue(CKMainWindowGeometry, geometry);
}

int Configuration::eepromMinAddress() const
{
    bool ok;
    int value = m_settings->value(CKEepromMinAddress, 0).toInt(&ok);

    if (!ok)
        value = 0;

    return value;
}

void Configuration::setEepromMinAddress(const int address)
{
    m_settings->setValue(CKEepromMinAddress, address);
}

int Configuration::eepromMaxAddress() const
{
    bool ok;
    int value = m_settings->value(CKEepromMaxAddress, 0).toInt(&ok);

    if (!ok)
        value = 0;

    return value;
}

void Configuration::setEepromMaxAddress(const int address)
{
    m_settings->setValue(CKEepromMaxAddress, address);
}

int Configuration::eepromSize() const
{
    bool ok;
    int value = m_settings->value(CKEepromSize, 0).toInt(&ok);

    if (!ok)
        value = 0;

    return value;
}

void Configuration::setEepromSize(const int size)
{
    m_settings->setValue(CKEepromSize, size);
}

QString Configuration::serialPort() const
{
    return m_settings->value(CKSerialPort).toString();
}

void Configuration::setSerialPort(const QString &port)
{
    m_settings->setValue(CKSerialPort, port);
}

int Configuration::serialBaud() const
{
    bool ok;
    int value = m_settings->value(CKSerialBaud, 0).toInt(&ok);

    if (!ok)
        value = 9600;

    return value;
}

void Configuration::setSerialBaud(const int baud)
{
    m_settings->setValue(CKSerialBaud, baud);
}

QStringList Configuration::jobs() const
{
    return m_settings->value(CKJobList).toStringList();
}

void Configuration::saveJobs(const QStringList &jobs)
{
    m_settings->setValue(CKJobList, jobs);
}
