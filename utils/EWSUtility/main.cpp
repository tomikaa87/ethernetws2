#include <QtGui/QApplication>

#include "MainWindow.h"
#include "Configuration.h"
#include "JobManager.h"
#include "Debug.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    Configuration config;
    config.setEepromMaxAddress(131071);
    config.setEepromMinAddress(0);

    JobManager jobManager(&config);
    jobManager.addWriteJob("test", 0);
    jobManager.addReadJob("readfile", 10, 12);
    jobManager.addConvertImageJob("test", "image.bin");

    MainWindow w(&config, &jobManager);
    w.show();

    return a.exec();
}
