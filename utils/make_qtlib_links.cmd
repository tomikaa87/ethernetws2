@echo off

if %1 == "" goto error
if not exist %1 goto error

set parentdir=%CD%
cd QtLibs
for %%i in (*.dll) do (
	echo Creating link of %CD%\%%i to %parentdir%\%1\%%i
	mklink %parentdir%\%1\%%i %CD%\%%i
)
cd ..

goto exit

:error
echo Destination directory not exists

:exit