#include <QApplication>
#include <QTranslator>

#include "MainWindow.h"
#include "Debug.h"
#include "Settings.h"
#include "ExternalEeprom.h"
#include "ExternalSram.h"
#include "GraphicLcd.h"
#include "LCDUI.h"

void myMessageOutput(QtMsgType, const QMessageLogContext &, const QString &msg) {
    fprintf(stderr, "%s\n", msg.toLocal8Bit().constData());
    fflush(stderr);
}

int main(int argc, char *argv[])
{
    qInstallMessageHandler(myMessageOutput);

    QApplication a(argc, argv);

    // Get system locale
    QString locale = QLocale::system().name();
    _debug("System locale: " << locale);

    // Translate the GUI
    QTranslator translator;
    translator.load(QString("ewss_%1").arg(locale));
    a.installTranslator(&translator);

    // Initialize settings provider
    Settings settings;

    // Initialize external EEPROM
    ExternalEeprom eeprom(&settings);

    // Initialize external SRAM
    ExternalSram sram;

    // Initialize graphic LCD
    GraphicLcd glcd;

    // Initialize LCDUI
    LCDUI lcdui(&glcd);

    // Initialize main window
    MainWindow w(&glcd, &settings, &lcdui, &eeprom);

    w.show();
    
    return a.exec();
}
