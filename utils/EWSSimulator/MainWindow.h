#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

class GraphicLcd;
class ExternalEeprom;
class EepromDataTransferDialog;

namespace Ui {
    class MainWindow;
}

class Settings;
class LCDUI;

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(GraphicLcd *lcd, Settings *settings, LCDUI *lcdui,
                        ExternalEeprom *eeprom, QWidget *parent = 0);
    ~MainWindow();

protected:
    bool eventFilter(QObject *, QEvent *);

private:
    Ui::MainWindow *ui;
    Settings *m_settings;
    GraphicLcd *m_lcd;
    LCDUI *m_lcdui;
    ExternalEeprom *m_eeprom;
    EepromDataTransferDialog *m_eepromDataTransferDialog;

private slots:
    void keypadButtonClicked();
};

#endif // MAINWINDOW_H
