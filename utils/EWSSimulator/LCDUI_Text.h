#ifndef LCDUI_TEXT_H
#define LCDUI_TEXT_H

#include <QObject>

class GraphicLcd;

class LCDUI_Text : public QObject
{
    Q_OBJECT

public:
    explicit LCDUI_Text(GraphicLcd *glcd, QObject *parent = 0);

    void drawText(int x, int y, const char *text, int invert, int right_just = 0);

private:
    GraphicLcd *m_glcd;

};

#endif // LCDUI_TEXT_H
