#include "LCDUI_Menu.h"
#include "LCDUI_Text.h"
#include "GraphicLcd.h"
#include "Debug.h"

static MenuItem DisplayGroupItem = {" Display ", ITEM_SEPARATOR, 0, 0, 0};
static MenuItem DisplayBacklightNormalLevel = {"Backlight normal level", ITEM_PROGRESS, 100, 0, 255};
static MenuItem DisplayBacklightDimLevel = {"Backlight dim level", ITEM_PROGRESS, 20, 0, 255};
static MenuItem DisplayBacklightAutoControl = {"Automatic backlight control", ITEM_TOGGLE, 1, 0, 1};
static MenuItem DisplayBacklightNormalLevelStart = {"Normal level start", ITEM_TIME, 8, 0, 0};
static MenuItem DisplayBacklightDimLevelStart = {"Dim level start", ITEM_TIME, 23, 0, 0};

static MenuItem WeatherDataGroup = {" Weather data ", ITEM_SEPARATOR, 0, 0, 0};
static MenuItem WeatherDataUpdateInterval = {"Update interval (min)", ITEM_INT, 1, 1, 60};

static MenuItem NetworkGroup = {" Network ", ITEM_SEPARATOR, 0, 0, 0};
static MenuItem NetworkUseDHCP = {"Use DHCP", ITEM_TOGGLE, 1, 0, 1};
static MenuItem NetworkStaticIP = {"Static IP address", ITEM_IPADDR, 0x1e01a8c0, 0, 0};
static MenuItem NetworkSubnetMask = {"Subnet mask", ITEM_IPADDR, 0x00ffffff, 0, 0};
static MenuItem NetworkDefaultGatewy = {"Default gateway", ITEM_IPADDR, 0x0101a8c0, 0, 0};
static MenuItem NetworkUseHTTPProxy = {"Use HTTP proxy", ITEM_TOGGLE, 0, 0, 1};
static MenuItem NetworkProxyIP = {"Proxy IP address", ITEM_IPADDR, 0x00000000, 0, 0};
static MenuItem NetworkProxyPort = {"Proxy port", ITEM_INT, 8080, 0, 65535};

static MenuItem SettingsStorageGroup = {" Save/Load ", ITEM_SEPARATOR, 0, 0, 0};
static MenuItem SettingsStorageSave = {"Save settings", ITEM_SIMPLE, 0, 0, 0};
static MenuItem SettingsStorageLoad = {"Load settings", ITEM_SIMPLE, 0, 0, 0};
static MenuItem SettingsStorageRestoreDefaults = {"Restore defaults", ITEM_SIMPLE, 0, 0, 0};

static MenuItem *MenuItems[] = {
    &DisplayGroupItem,
    &DisplayBacklightNormalLevel,
    &DisplayBacklightDimLevel,
    &DisplayBacklightAutoControl,
    &DisplayBacklightNormalLevelStart,
    &DisplayBacklightDimLevelStart,
    &WeatherDataGroup,
    &WeatherDataUpdateInterval,
    &NetworkGroup,
    &NetworkUseDHCP,
    &NetworkStaticIP,
    &NetworkSubnetMask,
    &NetworkDefaultGatewy,
    &NetworkUseHTTPProxy,
    &NetworkProxyIP,
    &NetworkProxyPort,
    &SettingsStorageGroup,
    &SettingsStorageSave,
    &SettingsStorageLoad,
    &SettingsStorageRestoreDefaults
};

LCDUI_Menu::LCDUI_Menu(GraphicLcd *glcd, LCDUI_Text *text, QObject *parent) :
    QObject(parent), m_glcd(glcd), m_text(text)
{
    m_menu._lcd = m_glcd;
    m_menu._text = m_text;
    MenuInit(&m_menu, "Weather Station Settings", MenuItems, 20);
    MenuDraw(&m_menu);
}

void LCDUI_Menu::draw()
{
    MenuDraw(&m_menu);
}

void LCDUI_Menu::keyPress(KeyCode key)
{
    MenuKeyPress(&m_menu, key);
}
