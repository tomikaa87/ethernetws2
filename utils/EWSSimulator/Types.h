#ifndef TYPES_H
#define TYPES_H

#include <QtGlobal>

typedef qint8 int8_t;
typedef quint8 uint8_t;

typedef qint16 int16_t;
typedef quint16 uint16_t;

typedef qint32 int24_t;
typedef quint32 uint24_t;

typedef qint32 int32_t;
typedef quint32 uint32_t;

#ifndef __PACKED
    #define __PACKED
#endif

// Advanced 8 bit integer type with bit fields and direct access to nibbles
typedef union
{
    struct __PACKED {
        unsigned b1 : 1;
        unsigned b2 : 1;
        unsigned b3 : 1;
        unsigned b4 : 1;
        unsigned b5 : 1;
        unsigned b6 : 1;
        unsigned b7 : 1;
        unsigned b8 : 1;
    };
    struct __PACKED {
        unsigned hi : 4;
        unsigned lo : 4;
    };
    int8_t si;
    uint8_t ui;
} int8_val_t;

// Advanced 16-bit integer type
typedef union
{
    struct __PACKED {
        int8_val_t lb;
        int8_val_t hb;
    } val;
    struct {
        uint8_t lb;
        uint8_t hb;
    };
    uint8_t bytes[2];
    int8_val_t val_bytes[2];
    int16_t si;
    uint16_t ui;
} int16_val_t;

// Advanced 24-bit integer type
typedef union
{
    struct __PACKED {
        int8_val_t lb;
        int8_val_t hb;
        int8_val_t ub;
    } val;
    struct __PACKED {
        uint8_t lb;
        uint8_t hb;
        uint8_t ub;
    };
    uint8_t bytes[3];
    int8_val_t val_bytes[3];
    int24_t si;
    uint24_t ui;
} int24_adv;

// Advanced 32-bit integer type
typedef union
{
    struct __PACKED {
        int8_val_t lb;
        int8_val_t hb;
        int8_val_t ub;
        int8_val_t mb;
    } val_b;
    struct __PACKED {
        int16_val_t lw;
        int16_val_t hw;
    } val_w;
    struct __PACKED {
        uint8_t lb;
        uint8_t hb;
        uint8_t ub;
        uint8_t mb;
    };
    struct {
        uint16_t lw;
        uint16_t hw;
    };
    uint8_t bytes[4];
    int8_val_t val_bytes[4];
    int16_t words[2];
    int16_val_t val_words[2];
    int32_t si;
    uint32_t ui;
} int32_val_t;

#endif // TYPES_H
