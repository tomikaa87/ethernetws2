#ifndef MENU_H
#define MENU_H

#include <stdint.h>
#include "Keypad.h"

class GraphicLcd;
class LCDUI_Text;

// C-style API

typedef enum
{
    ITEM_SIMPLE,
    ITEM_INT,
    ITEM_PROGRESS,
    ITEM_IPADDR,
    ITEM_TIME,
    ITEM_TOGGLE,
    ITEM_SEPARATOR
} MenuItemType;

typedef struct
{
    const char *Text;
    MenuItemType Type;
    int32_t Value;
    int32_t Min;
    int32_t Max;
//    uint8_t GroupID;
} MenuItem;

typedef struct
{
    const char *Title;
    MenuItem **Items;
    uint8_t ItemCount;

    uint8_t _currentItemY;
    uint8_t _selectionIndex;
    uint8_t _indexOffset;
    int8_t _subFieldIndex;
    int32_t _currentFieldOriginalValue;

    GraphicLcd *_lcd;
    LCDUI_Text *_text;
} Menu;

void MenuInit(Menu *menu, const char *title, MenuItem **items, uint8_t itemCount);
void MenuReset(Menu *menu);
void MenuItemInit(MenuItem *item, const char *text, MenuItemType type, uint32_t value, uint32_t min, uint32_t max);
void MenuDraw(Menu *menu);
void MenuKeyPress(Menu *menu, KeyCode key);

// C-style API

#endif // MENU_H
