#include "EepromDataTransferDialog.h"
#include "ui_EepromDataTransferDialog.h"
#include "Debug.h"
#include "ExternalEeprom.h"

#include <QFileDialog>
#include <QFile>
#include <QMessageBox>
#include <QImage>

EepromDataTransferDialog::EepromDataTransferDialog(ExternalEeprom *eeprom,
                                                   QWidget *parent) :
    QDialog(parent), ui(new Ui::EepromDataTransferDialog),
    m_eeprom(eeprom)
{
    ui->setupUi(this);

    connect(ui->browseSourceFileButton, SIGNAL(clicked()),
            this, SLOT(browseSourceFileButtonClicked()));
    connect(ui->browseDestFileButton, SIGNAL(clicked()),
            this, SLOT(browseDestFileButtonClicked()));
    connect(ui->writeButton, SIGNAL(clicked()),
            this, SLOT(writeButtonClicked()));
    connect(ui->readButton, SIGNAL(clicked()),
            this, SLOT(readButtonClicked()));
}

EepromDataTransferDialog::~EepromDataTransferDialog()
{
    _debug("Closing files");
    if (m_srcFile.isOpen())
        m_srcFile.close();
    if (m_dstFile.isOpen())
        m_dstFile.close();

    delete ui;
}

/*** Private methods **********************************************************/

/**
 * This method parses the entered EEPROM base address.
 */
int EepromDataTransferDialog::parseBaseAddress()
{
    QString text = ui->baseAddressEdit->text();

    // Check if the string is empty
    if (text.isEmpty()) {
        QMessageBox::warning(this, tr("Base address error"),
                             tr("You must enter the EEPROM base address."));
        return -1;
    }

    // Try to convert as a decimal number
    bool ok;
    int address = text.toInt(&ok);
    if (!ok) {
        _debug("Address is not decimal");

        // Try to convert as a hexadecimal number
        if (text.indexOf("0x", Qt::CaseInsensitive) != 0) {
            QMessageBox::warning(this, tr("Base address error"),
                                 tr("You must enter hexadecimal numbers with "
                                    "the 0x prefix."));
            return -1;
        }
        // 0x prefix found, try to convert the hexadecimal number
        text = text.remove("0x");
        address = text.toInt(&ok, 16);
        if (!ok) {
            QMessageBox::warning(this, tr("Base address error"),
                                 tr("You have entered an invalid "
                                    "hexadecimal base address."));
            return -1;
        }
        // Check address
        if (address < 0) {
            QMessageBox::warning(this, tr("Base address error"),
                                 tr("You have entered an invalid "
                                    "EEPROM base address."));
            return -1;
        }

        _debug("Hexadecimal base address: " << QString::number(address, 16));

        return address;
    }

    // Check address
    if (address < 0) {
        QMessageBox::warning(this, tr("Base address error"),
                             tr("You have entered an invalid "
                                "EEPROM base address."));
        return -1;
    }

    _debug("Decimal base address:" << address);

    return address;
}

/*** Private slots ************************************************************/

void EepromDataTransferDialog::browseSourceFileButtonClicked()
{
    // Select source file
    QString fileName =
            QFileDialog::getOpenFileName(this, tr("Select source file"));
    if (fileName.isEmpty())
        return;

    _debug("Selected source file: " << fileName);

    // Check previous source file
    if (m_srcFile.isOpen()) {
        m_srcFile.close();
        _debug("Previous source file closed");
    }

    // Open source file
    m_srcFile.setFileName(fileName);
    if (!m_srcFile.open(QIODevice::ReadOnly)) {
        QMessageBox::warning(this, tr("Source file error"),
                             tr("Source file couldn't be opened. Error: %1")
                             .arg(m_srcFile.errorString()));
        return;
    }

    // Update file size label
    ui->sizeLabel->setText(tr("%1 Bytes").arg(m_srcFile.size()));

    // Update file path edit
    ui->sourceFileEdit->setText(m_srcFile.fileName());
}

void EepromDataTransferDialog::browseDestFileButtonClicked()
{
    // Select destination file
    QString fileName =
            QFileDialog::getSaveFileName(this, tr("Select destination file"));
    if (fileName.isEmpty())
        return;

    _debug("Selected destination file: " << fileName);

    // Check previous destination file
    if (m_dstFile.isOpen()) {
        m_dstFile.close();
        _debug("Previous destination file closed");
    }

    // Open source file
    m_dstFile.setFileName(fileName);
    if (!m_dstFile.open(QIODevice::WriteOnly)) {
        QMessageBox::warning(this, tr("Destination file error"),
                tr("Destination file couldn't be opened for writing. Error: %1")
                .arg(m_dstFile.errorString()));
        return;
    }

    // Update file path edit
    ui->destFileEdit->setText(m_dstFile.fileName());
}

void EepromDataTransferDialog::writeButtonClicked()
{
    // Read EEPROM base address
    int address = parseBaseAddress();
    if (address < 0) {
        ui->baseAddressEdit->setFocus();
        return;
    }

    // Read file
    QByteArray data = m_srcFile.readAll();
    m_srcFile.close();

    int written;

    if (ui->rawDataRadio->isChecked()) {
        _debug("Uploading in raw data mode");

        // Check file size
        if (data.size() > m_eeprom->size() - address) {
            QMessageBox::warning(this, tr("Upload warning"),
                                 tr("Source file won't fit in the EEPROM."));
        }

        // Write data
        written = m_eeprom->writeArray(address, data.data(), data.length());

        // Check written amount
        if (written != data.size()) {
            QMessageBox::warning(this, tr("Upload warning"),
                                 tr("Written bytes: %1").arg(written));
        } else {
            QMessageBox::information(this, tr("Upload finished"),
                                 tr("Written bytes: %1").arg(written));
        }
    } else {
        _debug("Uploading in image mode");

        QImage image;
        image.loadFromData(data);

        // Check if the image is valid
        if (image.isNull())
        {
            QMessageBox::critical(this, tr("Bitmap error"),
                                  tr("Input bitmap format is invalid."));
            return;
        }

        _debug("Image: size=" << image.width() << "x" << image.height() <<
               ", colors=" << image.colorCount());

        /*if (image.colorCount() != 2)
        {
            QMessageBox::critical(this, tr("Bitmap error"),
                                  tr("Input bitmap must be monochrome."));
            return;
        }*/

        // Convert monochrome image into a bit stream
        QByteArray imageData;
        quint8 byte = 0;
        quint64 count = 0;
        for (int y = 0; y < image.height(); y++)
        {
            for (int x = 0; x < image.width(); x++)
            {
                if (image.pixel(x, y) == QColor(Qt::black).rgb())
                {
                    //qDebug() << "Pixel (" << x << ";" << y << "):" << 1;
                    byte |= 1 << (7 - (count % 8));
                    //qDebug() << "Byte: " << QString("%1").arg(byte, 8, 2, QChar('0'));
                }
                else
                {
                    //qDebug() << "Pixel (" << x << ";" << y << "):" << 0;
                    //qDebug() << "Byte: " << QString("%1").arg(byte, 8, 2, QChar('0'));
                }

                count++;

                if (count % 8 == 0 && count > 0)
                {
                    imageData.append(byte);
                    byte = 0;
                }
            }
        }
        if (count > 0) {
            imageData.append(byte);
        }

        _debug("Encoded bitmap size: " << imageData.length());

        // Check image data size
        if (imageData.size() > m_eeprom->size() - address) {
            QMessageBox::warning(this, tr("Upload warning"),
                                tr("Converted image won't fit in the EEPROM."));
        }

//        QFile f("image.bin");
//        if (f.open(QIODevice::WriteOnly)) {
//            f.write(imageData);
//            f.flush();
//            f.close();
//            log(tr("Image data dumped to image.bin"));
//        }

        // Write data
        written = m_eeprom->writeArray(address, imageData.data(),
                                       imageData.length());

        // Check written amount
        if (written != imageData.size()) {
            QMessageBox::warning(this, tr("Upload warning"),
                                 tr("Written bytes: %1").arg(written));
        } else {
            QMessageBox::information(this, tr("Upload finished"),
                                 tr("Written bytes: %1").arg(written));
        }
    }
}

void EepromDataTransferDialog::readButtonClicked()
{
    // Read EEPROM base address
    int address = parseBaseAddress();
    if (address < 0) {
        ui->baseAddressEdit->setFocus();
        return;
    }
}
