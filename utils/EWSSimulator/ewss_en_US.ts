<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="en_US">
<context>
    <name>EepromDataTransferDialog</name>
    <message>
        <location filename="EepromDataTransferDialog.ui" line="14"/>
        <source>EEPROM Data Transfer</source>
        <translation></translation>
    </message>
    <message>
        <location filename="EepromDataTransferDialog.ui" line="20"/>
        <source>Write data</source>
        <translation></translation>
    </message>
    <message>
        <location filename="EepromDataTransferDialog.ui" line="26"/>
        <source>Source:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="EepromDataTransferDialog.ui" line="33"/>
        <source>Source file path.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="EepromDataTransferDialog.ui" line="40"/>
        <source>Opens a dialog where you can choose the source file.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="EepromDataTransferDialog.ui" line="43"/>
        <location filename="EepromDataTransferDialog.ui" line="182"/>
        <source>...</source>
        <translation></translation>
    </message>
    <message>
        <location filename="EepromDataTransferDialog.ui" line="52"/>
        <location filename="EepromDataTransferDialog.ui" line="216"/>
        <source>Size:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="EepromDataTransferDialog.ui" line="59"/>
        <source>Size of the source file.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="EepromDataTransferDialog.ui" line="62"/>
        <source>N/A</source>
        <translation></translation>
    </message>
    <message>
        <location filename="EepromDataTransferDialog.ui" line="86"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt;Select &lt;/span&gt;&lt;span style=&quot; font-size:8pt; font-weight:600;&quot;&gt;Raw data&lt;/span&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt; if you want to write exactly the same data contained in the source file.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="EepromDataTransferDialog.ui" line="93"/>
        <source>Raw data</source>
        <translation></translation>
    </message>
    <message>
        <location filename="EepromDataTransferDialog.ui" line="100"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt;Select &lt;/span&gt;&lt;span style=&quot; font-size:8pt; font-weight:600;&quot;&gt;Image&lt;/span&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt; if you want the source file to be converted from a BMP, JPG or PNG to a series of bytes that can be used to draw an image on the screen.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="EepromDataTransferDialog.ui" line="107"/>
        <source>Image</source>
        <translation></translation>
    </message>
    <message>
        <location filename="EepromDataTransferDialog.ui" line="144"/>
        <source>Starts the write operation.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="EepromDataTransferDialog.ui" line="147"/>
        <source>Write</source>
        <translation></translation>
    </message>
    <message>
        <location filename="EepromDataTransferDialog.ui" line="159"/>
        <source>Read data</source>
        <translation></translation>
    </message>
    <message>
        <location filename="EepromDataTransferDialog.ui" line="165"/>
        <source>Destination:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="EepromDataTransferDialog.ui" line="172"/>
        <source>Destination file path.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="EepromDataTransferDialog.ui" line="179"/>
        <source>Opens a dialog where you can choose the destination file.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="EepromDataTransferDialog.ui" line="204"/>
        <source>Starts the read operation.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="EepromDataTransferDialog.ui" line="207"/>
        <source>Read</source>
        <translation></translation>
    </message>
    <message>
        <location filename="EepromDataTransferDialog.ui" line="223"/>
        <source>Enter the number of bytes you want to read from the EEPROM.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="EepromDataTransferDialog.ui" line="230"/>
        <location filename="EepromDataTransferDialog.ui" line="256"/>
        <source>Address can be entered in hexadecimal and decimal format. Hexadecimal format must be started with 0x followed by hexadecimal numbers.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="EepromDataTransferDialog.ui" line="233"/>
        <source>(hex, dec)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="EepromDataTransferDialog.ui" line="243"/>
        <source>EEPROM address</source>
        <translation></translation>
    </message>
    <message>
        <location filename="EepromDataTransferDialog.ui" line="249"/>
        <source>Base address of the EEPROM.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="EepromDataTransferDialog.ui" line="259"/>
        <source>(hex: 0xHEX, dec)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="EepromDataTransferDialog.cpp" line="35"/>
        <source>Select source file</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="MainWindow.ui" line="14"/>
        <source>Ethernet Weather Station 2 Simulator</source>
        <translation></translation>
    </message>
    <message>
        <location filename="MainWindow.ui" line="85"/>
        <source>F1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="MainWindow.ui" line="110"/>
        <source>Left</source>
        <translation></translation>
    </message>
    <message>
        <location filename="MainWindow.ui" line="135"/>
        <source>Menu</source>
        <translation></translation>
    </message>
    <message>
        <location filename="MainWindow.ui" line="160"/>
        <source>F2</source>
        <translation></translation>
    </message>
    <message>
        <location filename="MainWindow.ui" line="185"/>
        <source>Up</source>
        <translation></translation>
    </message>
    <message>
        <location filename="MainWindow.ui" line="210"/>
        <source>Down</source>
        <translation></translation>
    </message>
    <message>
        <location filename="MainWindow.ui" line="235"/>
        <source>Right</source>
        <translation></translation>
    </message>
    <message>
        <location filename="MainWindow.ui" line="260"/>
        <source>Enter</source>
        <translation></translation>
    </message>
    <message>
        <location filename="MainWindow.ui" line="285"/>
        <source>Esc</source>
        <translation></translation>
    </message>
    <message>
        <location filename="MainWindow.ui" line="305"/>
        <source>EEPROM</source>
        <translation></translation>
    </message>
    <message>
        <location filename="MainWindow.ui" line="313"/>
        <location filename="MainWindow.ui" line="323"/>
        <source>Transfer data</source>
        <translation></translation>
    </message>
    <message>
        <location filename="MainWindow.ui" line="318"/>
        <source>Download data</source>
        <translation></translation>
    </message>
</context>
</TS>
