#ifndef EEPROMDATATRANSFERDIALOG_H
#define EEPROMDATATRANSFERDIALOG_H

#include <QDialog>
#include <QFile>

class ExternalEeprom;

namespace Ui {
class EepromDataTransferDialog;
}

class EepromDataTransferDialog : public QDialog
{
    Q_OBJECT
    
public:
    explicit EepromDataTransferDialog(ExternalEeprom *eeprom,
                                      QWidget *parent = 0);
    ~EepromDataTransferDialog();
    
private:
    Ui::EepromDataTransferDialog *ui;
    ExternalEeprom *m_eeprom;
    QFile m_srcFile;
    QFile m_dstFile;

    int parseBaseAddress();

private slots:
    void browseSourceFileButtonClicked();
    void browseDestFileButtonClicked();
    void writeButtonClicked();
    void readButtonClicked();
};

#endif // EEPROMDATATRANSFERDIALOG_H
