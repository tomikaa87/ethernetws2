#include "MainWindow.h"
#include "ui_MainWindow.h"
#include "EepromDataTransferDialog.h"
#include "Settings.h"
#include "GraphicLcd.h"
#include "ExternalEeprom.h"
#include "LCDUI.h"

#include <QKeyEvent>

MainWindow::MainWindow(GraphicLcd *lcd, Settings *settings, LCDUI *lcdui,
                       ExternalEeprom *eeprom, QWidget *parent) :
    QMainWindow(parent), ui(new Ui::MainWindow),
    m_settings(settings), m_lcd(lcd), m_lcdui(lcdui), m_eeprom(eeprom)
{
    ui->setupUi(this);

    if (m_lcd) {
        m_lcd->setScreen(ui->lcdScreen);
        m_lcd->writeBuf(false);
    }

    // Initialize EEPROM data transfer dialog
    m_eepromDataTransferDialog = new EepromDataTransferDialog(m_eeprom, this);
    connect(ui->eepromTransferDataAction, SIGNAL(triggered()),
            m_eepromDataTransferDialog, SLOT(show()));

    connect(ui->plusButton, SIGNAL(clicked()),
            this, SLOT(keypadButtonClicked()));
    connect(ui->minusButton, SIGNAL(clicked()),
            this, SLOT(keypadButtonClicked()));
    connect(ui->upButton, SIGNAL(clicked()),
            this, SLOT(keypadButtonClicked()));
    connect(ui->downButton, SIGNAL(clicked()),
            this, SLOT(keypadButtonClicked()));
    connect(ui->leftButton, SIGNAL(clicked()),
            this, SLOT(keypadButtonClicked()));
    connect(ui->rightButton, SIGNAL(clicked()),
            this, SLOT(keypadButtonClicked()));
    connect(ui->escButton, SIGNAL(clicked()),
            this, SLOT(keypadButtonClicked()));
    connect(ui->menuButton, SIGNAL(clicked()),
            this, SLOT(keypadButtonClicked()));
    connect(ui->enterButton, SIGNAL(clicked()),
            this, SLOT(keypadButtonClicked()));

    restoreGeometry(m_settings->loadGeometry());

    ui->lcdScreen->installEventFilter(this);
}

MainWindow::~MainWindow()
{
    m_settings->saveGeometry(saveGeometry());

    delete ui;
}

bool MainWindow::eventFilter(QObject *object, QEvent *event)
{
    if (event->type() == QEvent::KeyPress)
    {
        switch (static_cast<QKeyEvent *>(event)->key())
        {
            case Qt::Key_Up: m_lcdui->keyPress(KEY_UP); break;
            case Qt::Key_Down: m_lcdui->keyPress(KEY_DOWN); break;
            case Qt::Key_Left: m_lcdui->keyPress(KEY_LEFT); break;
            case Qt::Key_Right: m_lcdui->keyPress(KEY_RIGHT); break;
            case Qt::Key_Escape: m_lcdui->keyPress(KEY_ESC); break;
            case Qt::Key_Menu: m_lcdui->keyPress(KEY_MENU); break;
            case Qt::Key_Plus: m_lcdui->keyPress(KEY_PLUS); break;
            case Qt::Key_Minus: m_lcdui->keyPress(KEY_MINUS); break;
            case Qt::Key_Enter:
            case Qt::Key_Return:
                m_lcdui->keyPress(KEY_ENTER);
                break;
            default: break;
        }
    }

    return QObject::eventFilter(object, event);
}

/*** Public methods ***********************************************************/


/*** Button click handlers ****************************************************/

void MainWindow::keypadButtonClicked()
{
    QPushButton *b = qobject_cast<QPushButton *>(sender());
    if (!b) {
        return;
    }

    if (b ==  ui->plusButton) {
        m_lcdui->keyPress(KEY_PLUS);
    }
    else if (b == ui->minusButton) {
        m_lcdui->keyPress(KEY_MINUS);
    }
    else if (b == ui->upButton) {
        m_lcdui->keyPress(KEY_UP);
    }
    else if (b == ui->downButton) {
        m_lcdui->keyPress(KEY_DOWN);
    }
    else if (b == ui->leftButton) {
        m_lcdui->keyPress(KEY_LEFT);
    }
    else if (b == ui->rightButton) {
        m_lcdui->keyPress(KEY_RIGHT);
    }
    else if (b == ui->menuButton) {
        m_lcdui->keyPress(KEY_MENU);
    }
    else if (b == ui->escButton) {
        m_lcdui->keyPress(KEY_ESC);
    }
    else if (b == ui->enterButton) {
        m_lcdui->keyPress(KEY_ENTER);
    }
}
