#include "ExternalSram.h"
#include "Debug.h"

#define EXT_SRAM_SIZE_BYTES     32768
#define EXT_SRAM_MAX_ADDRESS    EXT_SRAM_SIZE_BYTES - 1
#define EXT_SRAM_PAGE_SIZE      32
#define EXT_SRAM_MAX_PAGE       (EXT_SRAM_SIZE_BYTES / EXT_SRAM_PAGE_SIZE - 1)

static const int SramSizeBytes = 32768;
static const int SramMaxAddress = SramSizeBytes - 1;
static const int SramPageSize = 32;
static const int SramMaxPage = SramSizeBytes / SramPageSize - 1;

ExternalSram::ExternalSram(QObject *parent) :
    QObject(parent)
{
    m_data.fill(0, SramSizeBytes);

    _debug("External SRAM initialized (size=" << SramSizeBytes <<
           ", maxAddress=" << SramMaxAddress << ", page=" <<
           SramPageSize << ", maxPage=" << SramMaxPage << ")");
}

/*** Public methods ***********************************************************/

int ExternalSram::size() const
{
    return SramSizeBytes;
}

int ExternalSram::maxAddress() const
{
    return SramMaxAddress;
}

int ExternalSram::pageSize() const
{
    return SramPageSize;
}

int ExternalSram::maxPage() const
{
    return SramMaxPage;
}

int ExternalSram::writeByte(int address, quint8 byte)
{
    if (address > SramMaxAddress)
        return 0;

    m_data[address] = byte;
    return 1;
}

int ExternalSram::readByte(int address)
{
    if (address > SramMaxAddress)
        return 0;

    return m_data[address];
}

int ExternalSram::writeArray(int address, quint8 *buf, int bufSize)
{
    if (address > SramMaxAddress)
        return 0;

    int bufptr = 0;
    while (address <= SramMaxAddress && bufptr < bufSize) {
        m_data[address] = buf[bufptr];
        address++;
        bufptr++;
    }

    return bufptr;
}

int ExternalSram::readArray(int address, quint8 *buf, int bufSize)
{
    if (address > SramMaxAddress)
        return 0;

    int bufptr = 0;
    while (address <= SramMaxAddress && bufptr < bufSize) {
        buf[bufptr] = m_data[address];
        address++;
        bufptr++;
    }

    return bufptr;
}
