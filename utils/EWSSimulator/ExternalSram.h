#ifndef EXTERNALSRAM_H
#define EXTERNALSRAM_H

#include <QObject>


class ExternalSram : public QObject
{
    Q_OBJECT

public:
    explicit ExternalSram(QObject *parent = 0);

    int size() const;
    int maxAddress() const;
    int pageSize() const;
    int maxPage() const;

    int writeByte(int address, quint8 byte);
    int readByte(int address);

    int writePage(int page, quint8 offset, quint8 *buf, int bufSize);
    int readPage(int page, quint8 offset, quint8 *buf, int bufSize);

    int writeArray(int address, quint8 *buf, int bufSize);
    int readArray(int address, quint8 *buf, int bufSize);

private:
    QByteArray m_data;
};

#endif // EXTERNALSRAM_H
