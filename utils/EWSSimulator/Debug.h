#ifndef DEBUG_H
#define DEBUG_H

#include <QDebug>

#define _debug(x) qDebug().nospace() << "[D]" << "[" << __FILE__ << "@" << \
    __LINE__ << ": " << __FUNCTION__ << "] " << x

#define _info(x) qWarning().nospace() << "[I]" << "[" << __FILE__ << "@" << \
    __LINE__ << ": " << __FUNCTION__ << "] " << x

#define _warning(x) qWarning().nospace() << "[W]" << "[" << __FILE__ << "@" << \
    __LINE__ << ": " << __FUNCTION__ << "] " << x

#define _error(x) qWarning().nospace() << "[E]" << "[" << __FILE__ << "@" << \
    __LINE__ << ": " << __FUNCTION__ << "] " << x

#define _fatal(x) {\
    qWarning().nospace() << "[F]" << "[" << __FILE__ << "@" << \
    __LINE__ << ": " << __FUNCTION__ << "] " << x;\
    exit(1);\
}

#endif // DEBUG_H
