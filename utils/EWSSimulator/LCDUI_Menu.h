#ifndef LCDUI_MENU_H
#define LCDUI_MENU_H

#include <QObject>
#include "Keypad.h"
#include "Types.h"

#include "menu.h"

class GraphicLcd;
class LCDUI_Text;

class LCDUI_Menu : public QObject
{
    Q_OBJECT

public:
    explicit LCDUI_Menu(GraphicLcd *glcd, LCDUI_Text *text,
                        QObject *parent = 0);

    void draw();
    void keyPress(KeyCode key);
    
private:
    GraphicLcd *m_glcd;
    LCDUI_Text *m_text;
    Menu m_menu;
};

#endif // LCDUI_MENU_H
