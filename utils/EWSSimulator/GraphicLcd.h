#ifndef GRAPHICLCD_H
#define GRAPHICLCD_H

#include <QObject>
#include <QLabel>
#include <stdint.h>

#define GLCD_COLOR_BLACK true
#define GLCD_COLOR_WHITE false

#define GLCD_RESOLUTION_X 128
#define GLCD_RESOLUTION_Y 128

class GraphicLcd : public QObject
{
    Q_OBJECT

public:
    explicit GraphicLcd(QLabel *screen = 0, QObject *parent = 0);

    void setScreen(QLabel *screen);

    void setPixel(quint8 x, quint8 y, bool color);
    bool getPixel(quint8 x, quint8 y);

    void drawRect(quint8 x1, quint8 y1, quint8 x2, quint8 y2, bool fill, bool color);
    void drawImage(char *image, quint8 w, quint8 h, quint8 x, quint8 y, bool invert);
    void drawBar(quint8 value, quint8 max, quint8 x, quint8 y, quint8 w, quint8 h, bool invert);
    void drawLine(quint8 x1, quint8 y1, quint8 x2, quint8 y2, bool color);
    void drawInverseRect(quint8 x1, quint8 y1, quint8 x2, quint8 y2);

    void clearBuf();
    void writeBuf(bool invert);
    void setBufByte(char byte, quint16 addr);
    void setBufData(int x, int y, uint8_t *data, int len, int flags);

    enum {
        GlcdInvert = 128
    } GlcdFlags;

private:
    QLabel *m_screen;
    QPixmap *m_image;
    quint8 m_buffer[GLCD_RESOLUTION_X * GLCD_RESOLUTION_Y / 8];

    quint16 addressOf(int x, int y) const;

    void updateScreen();
    QColor lcdColor(bool color);
    QPen lcdPen(QColor color);
};

#endif // GRAPHICLCD_H
