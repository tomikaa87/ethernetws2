#include "GraphicLcd.h"
#include "Debug.h"

#include <QPainter>

GraphicLcd::GraphicLcd(QLabel *screen, QObject *parent) :
    QObject(parent), m_screen(screen)
{
    m_image = new QPixmap(128, 128);

    clearBuf();;
}

/*** Public methods ***********************************************************/

void GraphicLcd::setScreen(QLabel *screen)
{
    m_screen = screen;
    _debug("New screen: " << screen);
}

void GraphicLcd::setPixel(quint8 x, quint8 y, bool color)
{
    /*QPainter p(m_image);

    p.setPen(QPen(lcdColor(color)));
    p.drawPoint(x, y);
    p.end();

    updateScreen();*/

    if (x > GLCD_RESOLUTION_X || y > GLCD_RESOLUTION_Y)
        return;

    quint16 addr = addressOf(x, y);
    quint8 data = m_buffer[addr];

    if (color)
        data |= (1 << (7 - (x % 8)));
    else
        data &= (0xFF - (1 << (7 - (x % 8))));

    m_buffer[addr] = data;
}

bool GraphicLcd::getPixel(quint8 x, quint8 y)
{
    /*QImage img = m_image->toImage();

    if (img.pixel(x, y) == Qt::black)
        return true;

    return false;*/

    if (x > GLCD_RESOLUTION_X || y > GLCD_RESOLUTION_Y)
        return false;

    quint16 addr = addressOf(x, y);
    quint8 data = m_buffer[addr];

    return (data & (1 << (7 - (x % 8)))) > 0 ? true : false;
}

void GraphicLcd::drawRect(quint8 x1, quint8 y1, quint8 x2, quint8 y2, bool fill, bool color)
{
    if (fill)
    {
        for (int i = x1; i <= x2; i++)
            for (int j = y1; j <= y2; j++)
                setPixel(i, j, color);
    }
    else
    {
        for (int i = x1; i <= x2; i++)
        {
            setPixel(i, y1, color);
            setPixel(i, y2, color);
        }
        for (int i = y1; i <= y2; i++)
        {
            setPixel(x1, i, color);
            setPixel(x2, i, color);
        }
    }
}

void GraphicLcd::drawImage(char *image, quint8 w, quint8 h, quint8 x, quint8 y,
                           bool invert)
{
    for (quint8 i = 0; i < h; i++)
            for (quint8 j = 0; j < w; j++)
            {
                quint8 k = j + i * w;
                setPixel(j + x, i + y, invert ? !image[k] : image[k]);
            }
}

void GraphicLcd::drawBar(quint8 value, quint8 max, quint8 x, quint8 y, quint8 w, quint8 h,
                         bool invert)
{
    quint8 length = (quint8) ((float) value / (float) max * (float) w);

    drawRect(x, y, length + x - 1, h + y - 1, 1, !invert);

    if (length < w - 1)
        drawRect(x + length, y, x + w - 1, h + y - 1, 1, invert);
}

void GraphicLcd::drawLine(quint8 x1, quint8 y1, quint8 x2, quint8 y2, bool color)
{
    unsigned char steep;
    unsigned char tmp, deltaX, deltaY, x, y;
    signed char yStep, error;

    steep = abs(y2 - y1) > abs(x2 - x1);

    if (steep)
    {
        tmp = x1;
        x1 = y1;
        y1 = tmp;

        tmp = x2;
        x2 = y2;
        y2 = tmp;
    }

    if (x1 > x2)
    {
        tmp = x1;
        x1 = x2;
        x2 = tmp;

        tmp = y1;
        y1 = y2;
        y2 = tmp;
    }

    deltaX = x2 - x1;
    deltaY = abs(y2 - y1);
    error = deltaX / 2;
    y = y1;

    if (y1 < y2)
        yStep = 1;
    else
        yStep = -1;

    for (x = x1; x <= x2; x++)
    {
        if (steep)
            setPixel(y, x, color);
        else
            setPixel(x, y, color);

        error = error - deltaY;
        if (error < 0)
        {
            y = y + yStep;
            error = error + deltaX;
        }
    }
}

void GraphicLcd::drawInverseRect(quint8 x1, quint8 y1, quint8 x2, quint8 y2)
{
    for (int y = y1; y <= y2; y++)
        for (int x = x1; x <= x2; x++)
        {
            setPixel(x, y, !getPixel(x, y));
        }
}

void GraphicLcd::clearBuf()
{
    memset(m_buffer, 0, GLCD_RESOLUTION_X * GLCD_RESOLUTION_Y / 8);
}

void GraphicLcd::writeBuf(bool invert)
{
    if (invert)
    {
        m_image->fill(Qt::black);

        QPainter p(m_image);

        p.setPen(lcdPen(Qt::green));

        for (int y = 0; y < GLCD_RESOLUTION_Y; y++) {
            for (int x = 0; x < GLCD_RESOLUTION_X; x++) {
                if (getPixel(x, y)) {
                    p.drawPoint(x, y);
                }
            }
        }

        p.end();

        if (m_screen)
            m_screen->setPixmap(m_image->scaled(512, 512, Qt::KeepAspectRatio));
        else
            _warning("LCD screen canvas is null");
    }
    else
        updateScreen();
}

void GraphicLcd::setBufByte(char byte, quint16 addr)
{
    if (addr > sizeof (m_buffer))
        return;

    m_buffer[addr] = byte;
}

void GraphicLcd::setBufData(int x, int y, uint8_t *data, int len, int flags)
{
    if (x >= GLCD_RESOLUTION_X || y > GLCD_RESOLUTION_Y)
        return;

    const int glcd_graph_area = 0x10;

    int offset = x % 8;
    int addr = (((uint16_t) y) * glcd_graph_area) +
            (uint16_t) x / (uint16_t) 8;
    int last_addr = (((uint16_t) y + 1) * glcd_graph_area);
    quint8 mask_bits;

    if (flags & GlcdInvert) {
        int i;
        for (i = 0; i < len; i++) {
            uint8_t d = ~data[i];
            data[i] = d;
        }
    }

    mask_bits = flags & 7;

    if (offset == 0) {
        while (addr < last_addr && len--) {
            quint8 b = m_buffer[addr];
            if (len == 0 && mask_bits > 0) {
                uint8_t mask = (0xFF >> (8 - mask_bits));
                b &= mask;
                b |= *data & ~mask;
            } else {
                b = *data++;
            }
            m_buffer[addr++] = b;
        }
    } else {
        int first_fragment = 1;
        while (addr < last_addr) {
            quint8 b = m_buffer[addr];
            if (first_fragment) {
                if (len == 1 && mask_bits > offset) {
                    uint8_t mask = (0xFF >> (8 - mask_bits + offset));
                    b &= (0xFF << (8 - offset)) + mask;
                    b |= (*data >> offset) & ~mask;
                    m_buffer[addr] = b;
                    break;
                } else {
                    b &= (0xFF << (8 - offset));
                    b |= (*data >> offset);
                    m_buffer[addr] = b;
                    addr++;
                }
                first_fragment = 0;
            } else {
                if (len == 1 && mask_bits > 0) {
                    quint8 mask = 0xff << (8 - offset + mask_bits);
                    b &= ~mask;
                    b |= (*data << (8 - offset)) & mask;
                    m_buffer[addr] = b;
                } else {
                    b &= (0xFF >> offset);
                    b |= (*data << (8 - offset));
                    m_buffer[addr] = b;
                }

                if (--len == 0)
                    break;
                data++;
                first_fragment = 1;
            }
        }
    }
}

/*** Private methods **********************************************************/

quint16 GraphicLcd::addressOf(int x, int y) const
{
    //qDebug() << x << y << y * GLCD_RESOLUTION_X / 8 + x / 8;
    return (y * GLCD_RESOLUTION_X / 8 + x / 8);
}

void GraphicLcd::updateScreen()
{
    m_image->fill(Qt::green);

    QPainter p(m_image);

    p.setPen(lcdPen(Qt::black));

    for (int y = 0; y < GLCD_RESOLUTION_Y; y++) {
        for (int x = 0; x < GLCD_RESOLUTION_X; x++) {
            if (getPixel(x, y)) {
                p.drawPoint(x, y);
            }
        }
    }

    p.end();

    if (m_screen)
        m_screen->setPixmap(m_image->scaled(512, 512, Qt::KeepAspectRatio));
    else
        _warning("LCD screen canvas is null");
}

QColor GraphicLcd::lcdColor(bool color)
{
    if (color)
        return Qt::black;
    else
        return Qt::green;
}

QPen GraphicLcd::lcdPen(QColor color)
{
    return QPen(QBrush(color, Qt::SolidPattern), 1.0, Qt::SolidLine, Qt::SquareCap, Qt::MiterJoin);
}
