#include "Settings.h"
#include "Debug.h"

static const QString CKEepromData = "Storage/EEPROMData";
static const QString CKWindowGeometry = "Window/Geometry";

Settings::Settings(QObject *parent) :
    QObject(parent)
{
    m_settings = new QSettings("settings.ini", QSettings::IniFormat, this);
}

Settings::~Settings()
{
    _info("Saving settings");
    m_settings->sync();
}

/*** Public methods ***********************************************************/

QByteArray Settings::loadGeometry()
{
    return m_settings->value(CKWindowGeometry).toByteArray();
}

void Settings::saveGeometry(QByteArray geometry)
{
    m_settings->setValue(CKWindowGeometry, geometry);
}

QByteArray Settings::loadEepromData()
{
    return m_settings->value(CKEepromData).toByteArray();
}

void Settings::saveEepromData(QByteArray data)
{
    m_settings->setValue(CKEepromData, data);
    m_settings->sync();
}
