#-------------------------------------------------
#
# Project created by QtCreator 2012-01-16T19:48:08
#
#-------------------------------------------------

QT       += core gui widgets

TARGET = EWSSimulator
TEMPLATE = app


SOURCES += main.cpp\
        MainWindow.cpp \
    GraphicLcd.cpp \
    LCDUI.cpp \
    LCDUI_Menu.cpp \
    LCDUI_Text.cpp \
    ExternalEeprom.cpp \
    ExternalSram.cpp \
    Settings.cpp \
    EepromDataTransferDialog.cpp \
    menu.cpp

HEADERS  += MainWindow.h \
    GraphicLcd.h \
    LCDUI.h \
    LCDUI_Menu.h \
    LCDUI_Text.h \
    ExternalEeprom.h \
    ExternalSram.h \
    Settings.h \
    Debug.h \
    EepromDataTransferDialog.h \
    Types.h \
    Keypad.h \
    menu.h

FORMS    += MainWindow.ui \
    EepromDataTransferDialog.ui

TRANSLATIONS = ewss_en_US.ts \
    ewss_hu_HU.ts
