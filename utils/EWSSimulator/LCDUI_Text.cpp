#include "LCDUI_Text.h"
#include "GraphicLcd.h"
#include "Types.h"
#include "Debug.h"

static const uint8_t DefFontAttr[98] = {
    0x02, 0x02, 0x04, 0x06, 0x06, 0x08, 0x05, 0x02,
    0x03, 0x03, 0x06, 0x06, 0x03, 0x05, 0x02, 0x06,
    0x05, 0x03, 0x05, 0x05, 0x05, 0x05, 0x05, 0x05,
    0x05, 0x05, 0x03, 0x03, 0x04, 0x04, 0x04, 0x05,
    0x09, 0x05, 0x05, 0x05, 0x05, 0x05, 0x05, 0x05,
    0x05, 0x02, 0x04, 0x05, 0x04, 0x06, 0x06, 0x05,
    0x05, 0x05, 0x05, 0x05, 0x06, 0x05, 0x06, 0x06,
    0x05, 0x06, 0x05, 0x03, 0x06, 0x03, 0x04, 0x04,
    0x03, 0x05, 0x05, 0x05, 0x05, 0x05, 0x04, 0x05,
    0x05, 0x02, 0x03, 0x05, 0x02, 0x08, 0x05, 0x05,
    0x05, 0x05, 0x04, 0x05, 0x04, 0x05, 0x06, 0x06,
    0x05, 0x05, 0x04, 0x04, 0x02, 0x04, 0x05, 0x03,
    0x00, 0x0A
};
static const uint8_t DefFontData[961] = {
    0x00, 0x00, 0x00, 0x00, 0x20, 0x00, 0x00, 0x00,
    0x40, 0x80, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x3C, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0xC0, 0x00, 0xC0, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x20, 0x80, 0x80, 0x00,
    0x00, 0x80, 0xA0, 0x50, 0x78, 0x44, 0x60, 0x80,
    0x80, 0x40, 0x00, 0x00, 0x00, 0x00, 0x00, 0x08,
    0x60, 0xC0, 0xE0, 0xE0, 0x20, 0xF0, 0x60, 0xF0,
    0x60, 0x60, 0x00, 0x00, 0x00, 0x00, 0x00, 0xE0,
    0x42, 0x00, 0x60, 0xE0, 0x70, 0xE0, 0xF0, 0xF0,
    0x70, 0x90, 0x80, 0x20, 0x90, 0x80, 0x88, 0x88,
    0x60, 0xE0, 0x60, 0xE0, 0x70, 0xF8, 0x90, 0x88,
    0xA8, 0x90, 0x88, 0xF0, 0x80, 0x80, 0x40, 0x40,
    0x00, 0x80, 0x00, 0x80, 0x00, 0x10, 0x00, 0x20,
    0x00, 0x80, 0x80, 0x40, 0x80, 0x80, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x40, 0x80, 0x40, 0x50,
    0x00, 0x80, 0xA0, 0x50, 0xA0, 0xA8, 0x80, 0x80,
    0x80, 0x40, 0x20, 0x20, 0x00, 0x00, 0x00, 0x10,
    0x90, 0x40, 0x10, 0x10, 0x60, 0x80, 0x80, 0x10,
    0x90, 0x90, 0x00, 0x00, 0x20, 0x00, 0x80, 0x10,
    0x99, 0x00, 0x90, 0x90, 0x80, 0x90, 0x80, 0x80,
    0x80, 0x90, 0x80, 0x20, 0xA0, 0x80, 0xD8, 0xC8,
    0x90, 0x90, 0x90, 0x90, 0x80, 0x20, 0x90, 0x88,
    0xA8, 0x90, 0x50, 0x10, 0x80, 0x40, 0x40, 0xA0,
    0x00, 0x40, 0x00, 0x80, 0x00, 0x10, 0x00, 0x40,
    0x00, 0x80, 0x00, 0x00, 0x80, 0x80, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x40, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x40, 0x80, 0x40, 0xA0,
    0x00, 0x80, 0x00, 0xF8, 0xA0, 0x48, 0x80, 0x00,
    0x80, 0x40, 0xA8, 0x20, 0x00, 0x00, 0x00, 0x10,
    0x90, 0x40, 0x10, 0x10, 0xA0, 0x80, 0x80, 0x20,
    0x90, 0x90, 0x80, 0x40, 0x40, 0xE0, 0x40, 0x10,
    0x85, 0x00, 0x90, 0x90, 0x80, 0x90, 0x80, 0x80,
    0x80, 0x90, 0x80, 0x20, 0xA0, 0x80, 0xA8, 0xA8,
    0x90, 0x90, 0x90, 0x90, 0x80, 0x20, 0x90, 0x50,
    0xA8, 0x90, 0x50, 0x20, 0x80, 0x40, 0x40, 0x00,
    0x00, 0x00, 0x60, 0xE0, 0x70, 0x70, 0x60, 0xE0,
    0x70, 0xE0, 0x80, 0x40, 0x90, 0x80, 0xEC, 0xE0,
    0x60, 0xE0, 0x70, 0xE0, 0x70, 0xE0, 0x90, 0x88,
    0xA8, 0x90, 0x90, 0xE0, 0x40, 0x80, 0x40, 0x00,
    0x00, 0x80, 0x00, 0x50, 0x70, 0x10, 0x50, 0x00,
    0x80, 0x40, 0x70, 0xF8, 0x00, 0x00, 0x00, 0x20,
    0x90, 0x40, 0x60, 0x60, 0xA0, 0xE0, 0xE0, 0x20,
    0x60, 0x70, 0x00, 0x00, 0x80, 0x00, 0x20, 0x20,
    0x9D, 0x00, 0xF0, 0xE0, 0x80, 0x90, 0xE0, 0xE0,
    0xB0, 0xF0, 0x80, 0x20, 0xC0, 0x80, 0x88, 0xA8,
    0x90, 0xE0, 0x90, 0xE0, 0x60, 0x20, 0x90, 0x50,
    0xA8, 0x60, 0x20, 0x40, 0x80, 0x20, 0x40, 0x00,
    0x00, 0x00, 0x10, 0x90, 0x80, 0x90, 0x90, 0x40,
    0x90, 0x90, 0x80, 0x40, 0xA0, 0x80, 0x92, 0x90,
    0x90, 0x90, 0x90, 0x80, 0x80, 0x40, 0x90, 0x88,
    0xA8, 0x90, 0x90, 0x20, 0x80, 0x80, 0x20, 0x00,
    0x00, 0x80, 0x00, 0xF8, 0x28, 0x24, 0xA0, 0x00,
    0x80, 0x40, 0xA8, 0x20, 0x00, 0xE0, 0x00, 0x40,
    0x90, 0x40, 0x80, 0x10, 0xF0, 0x10, 0x90, 0x20,
    0x90, 0x10, 0x00, 0x00, 0x40, 0xE0, 0x40, 0x40,
    0xA5, 0x00, 0x90, 0x90, 0x80, 0x90, 0x80, 0x80,
    0x90, 0x90, 0x80, 0x20, 0xA0, 0x80, 0x88, 0xA8,
    0x90, 0x80, 0x90, 0x90, 0x10, 0x20, 0x90, 0x50,
    0xA8, 0x90, 0x20, 0x40, 0x80, 0x10, 0x40, 0x00,
    0x00, 0x00, 0x70, 0x90, 0x80, 0x90, 0xF0, 0x40,
    0x90, 0x90, 0x80, 0x40, 0xC0, 0x80, 0x92, 0x90,
    0x90, 0x90, 0x90, 0x80, 0x60, 0x40, 0x90, 0x88,
    0xA8, 0x60, 0x90, 0x40, 0x40, 0x80, 0x40, 0x00,
    0x00, 0x00, 0x00, 0x50, 0x28, 0x2A, 0xA0, 0x00,
    0x80, 0x40, 0x20, 0x20, 0x00, 0x00, 0x00, 0x40,
    0x90, 0x40, 0x80, 0x10, 0x20, 0x10, 0x90, 0x40,
    0x90, 0x10, 0x00, 0x00, 0x20, 0x00, 0x80, 0x00,
    0x9D, 0x00, 0x90, 0x90, 0x80, 0x90, 0x80, 0x80,
    0x90, 0x90, 0x80, 0x20, 0xA0, 0x80, 0x88, 0x98,
    0x90, 0x80, 0x90, 0x90, 0x10, 0x20, 0x90, 0x20,
    0xA8, 0x90, 0x20, 0x80, 0x80, 0x10, 0x40, 0x00,
    0x00, 0x00, 0x90, 0x90, 0x80, 0x90, 0x80, 0x40,
    0x90, 0x90, 0x80, 0x40, 0xA0, 0x80, 0x92, 0x90,
    0x90, 0x90, 0x90, 0x80, 0x10, 0x40, 0x90, 0x50,
    0xA8, 0x90, 0x90, 0x80, 0x40, 0x80, 0x40, 0x00,
    0x00, 0x80, 0x00, 0x50, 0xF0, 0x44, 0x50, 0x00,
    0x80, 0x40, 0x00, 0x00, 0x40, 0x00, 0x80, 0x80,
    0x60, 0x40, 0xF0, 0xE0, 0x20, 0xE0, 0x60, 0x40,
    0x60, 0x60, 0x80, 0x40, 0x00, 0x00, 0x00, 0x40,
    0x42, 0x00, 0x90, 0xE0, 0x70, 0xE0, 0xF0, 0x80,
    0x70, 0x90, 0x80, 0xC0, 0x90, 0xE0, 0x88, 0x88,
    0x60, 0x80, 0x60, 0x90, 0xE0, 0x20, 0x60, 0x20,
    0x50, 0x90, 0x20, 0xF0, 0x80, 0x08, 0x40, 0x00,
    0xE0, 0x00, 0x70, 0xE0, 0x70, 0x70, 0x60, 0x40,
    0x70, 0x90, 0x80, 0x40, 0x90, 0x80, 0x92, 0x90,
    0x60, 0xE0, 0x70, 0x80, 0xE0, 0x60, 0x70, 0x20,
    0x50, 0x90, 0x70, 0xE0, 0x40, 0x80, 0x40, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x20, 0x00, 0x00, 0x00,
    0x40, 0x80, 0x00, 0x00, 0x80, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x80, 0x00, 0x00, 0x00, 0x00,
    0x38, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x10, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0xC0, 0x00, 0xC0, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x10, 0x00, 0x00, 0x40, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x80, 0x10, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x10, 0x00, 0x20, 0x80, 0x80, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x60, 0x00, 0x00, 0x80, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x80, 0x10, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x60, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00
};

LCDUI_Text::LCDUI_Text(GraphicLcd *glcd, QObject *parent) :
    QObject(parent), m_glcd(glcd)
{
}

// TODO right justified test should be supported
void LCDUI_Text::drawText(int x, int y, const char *text, int invert,
                          int right_just)
{
    int f_img_h, f_img_bpl, flags;
    int16_val_t f_img_w;

    // Load font attributes
    f_img_w.hb = DefFontAttr[95];
    f_img_w.lb = DefFontAttr[96];
    f_img_h = DefFontAttr[97];

    // Calculate bytes/line
    f_img_bpl = f_img_w.ui / 8;

    // Set drawing flags
    flags = invert ? GraphicLcd::GlcdInvert : 0;

    if (right_just) {
        // Calculate total width of the text
        const char *s = text;
        while (*s) {
            unsigned char c_idx = *s++ - 32;
            if (c_idx > 94)
                continue;
            x -= DefFontAttr[c_idx];
        }
    }

    // Draw text char-by-char
    while (*text) {
        uint8_t c_idx, c_width, c_width_bytes, c_height, y_tmp, mask_bits;
        uint24_t c_addr;

        // Only draw characters with valid code
        c_idx = *text++ - 32;
        if (c_idx > 94)
            continue;

        // Read character dimensions
        c_width = DefFontAttr[c_idx];
        c_height = f_img_h;
        if (c_width == 0) {
            continue;
        }
        c_width_bytes = c_width > 8 ? 2 : 1;

        // Avoid drawing unnecessary pixels by masking
        mask_bits = c_width_bytes == 1 ? 8 - c_width : 16 - c_width;
        flags &= ~0x07;
        flags |= mask_bits;

        // Calculate character begning address
        c_addr = c_idx;
        if (c_idx > 32) {
            // @ is 16 bits width
            c_addr++;
        }

        // Draw character line-by-line
        y_tmp = y;
        if (!invert) {
            while (c_height--) {
                // Draw the line
                m_glcd->setBufData(x, y_tmp++, (uint8_t *)&DefFontData[c_addr],
                                   c_width_bytes, flags);
                c_addr += f_img_bpl;
            }
        } else {
            uint8_t data[2];
            while (c_height--) {
                // Draw the line
                // TODO glcd_set_buf_data is unable to invert constant data
                memcpy(data, &DefFontData[c_addr], c_width_bytes);
                m_glcd->setBufData(x, y_tmp++, data, c_width_bytes, flags);
                c_addr += f_img_bpl;
            }
        }
        x += c_width;
    }
}
