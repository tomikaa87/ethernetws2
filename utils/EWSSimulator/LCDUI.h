#ifndef LCDUI_H
#define LCDUI_H

#include <QObject>
#include "Keypad.h"

class GraphicLcd;
class LCDUI_Text;
class LCDUI_Menu;

class LCDUI : public QObject
{
    Q_OBJECT

public:
    explicit LCDUI(GraphicLcd *lcd, QObject *parent = 0);

    void keyPress(KeyCode key);

    void drawDemo();

private:
    GraphicLcd *m_lcd;
    LCDUI_Text *m_text;
    LCDUI_Menu *m_menu;

private slots:
    void test();
};

#endif // LCDUI_H
