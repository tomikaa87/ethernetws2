#include "ExternalEeprom.h"
#include "Settings.h"
#include "Debug.h"

static const int EepromSizeBytes = 131072;
static const int EepromMaxAddress = EepromSizeBytes - 1;
static const int EepromPageSize = 256;

ExternalEeprom::ExternalEeprom(Settings *settings, QObject *parent) :
    QObject(parent), m_settings(settings)
{
    m_data = new char [EepromSizeBytes];

    memset(m_data, 0xFF, EepromSizeBytes);
    QByteArray data = m_settings->loadEepromData();
    memcpy(m_data, data.data(), data.length());

    if (data.length() > EepromSizeBytes)
        data.truncate(EepromSizeBytes);
    if (data.length() == 0) {
        // Initial EEPROM data
        data.fill(0xFF, EepromSizeBytes);
    }

    _debug("External EEPROM initialized (size=" << EepromSizeBytes <<
           ", maxAddress=" << EepromMaxAddress << ", page="
           << EepromPageSize << ")");
}

ExternalEeprom::~ExternalEeprom()
{
    m_settings->saveEepromData(QByteArray(m_data, EepromSizeBytes));
    delete [] m_data;
}

/*** Public methods ***********************************************************/

int ExternalEeprom::size() const
{
    return EepromSizeBytes;
}

int ExternalEeprom::maxAddress() const
{
    return EepromMaxAddress;
}

int ExternalEeprom::pageSize() const
{
    return EepromPageSize;
}

int ExternalEeprom::writeByte(int address, char byte)
{
    if (address > EepromMaxAddress)
        return 0;

    m_data[address] = byte;
    return 1;
}

int ExternalEeprom::readByte(int address)
{
    if (address > EepromMaxAddress)
        return 0;

    return m_data[address];
}

int ExternalEeprom::writeArray(int address, char *buf, int bufSize)
{
    if (address > EepromMaxAddress)
        return 0;

    int bufptr = 0;
    while (address <= EepromMaxAddress && bufptr < bufSize) {
        m_data[address] = buf[bufptr];
        address++;
        bufptr++;
    }

    return bufptr;
}

int ExternalEeprom::readArray(int address, char *buf, int bufSize)
{
    if (address > EepromMaxAddress)
        return 0;

    int bufptr = 0;
    while (address <= EepromMaxAddress && bufptr < bufSize) {
        buf[bufptr] = m_data[address];
        address++;
        bufptr++;
    }

    return bufptr;
}
