#include "LCDUI.h"
#include "GraphicLcd.h"
#include "LCDUI_Text.h"
#include "LCDUI_Menu.h"
#include "Debug.h"

#include <QTimer>

LCDUI::LCDUI(GraphicLcd *lcd, QObject *parent) :
    QObject(parent), m_lcd(lcd)
{
    m_text = new LCDUI_Text(lcd, this);
    m_menu = new LCDUI_Menu(lcd, m_text, this);

//    QTimer::singleShot(500, this, SLOT(test()));
}

void LCDUI::keyPress(KeyCode key)
{
    _debug("key:" << key);
    switch (key) {
        case KEY_MENU:
            m_menu->draw();
        default:
            m_menu->keyPress(key);
    }
}

void LCDUI::drawDemo()
{
    m_text->drawText(1, 1, "TFZUASFASydfds", 0);
    m_lcd->writeBuf(false);
}

void LCDUI::test()
{
    //m_lcd->drawRect(0, 0, 128, 128, 1, 1);
    m_text->drawText(43, 0, "12", 1, 1);
    m_lcd->writeBuf(0);
}
