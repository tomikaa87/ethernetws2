#ifndef KEYPAD_H
#define KEYPAD_H

enum KeyCode {
    KEY_UP,
    KEY_DOWN,
    KEY_LEFT,
    KEY_RIGHT,
    KEY_ENTER,
    KEY_ESC,
    KEY_MENU,
    KEY_PLUS,
    KEY_MINUS
};

#endif // KEYPAD_H
