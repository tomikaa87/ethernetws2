#ifndef EXTERNALEEPROM_H
#define EXTERNALEEPROM_H

#include <QObject>

class Settings;

class ExternalEeprom : public QObject
{
    Q_OBJECT

public:
    explicit ExternalEeprom(Settings *settings, QObject *parent = 0);
    ~ExternalEeprom();

    int size() const;
    int maxAddress() const;
    int pageSize() const;

    int writeByte(int address, char byte);
    int readByte(int address);

    int writeArray(int address, char *buf, int bufSize);
    int readArray(int address, char *buf, int bufSize);

private:
    Settings *m_settings;
    char *m_data;
};

#endif // EXTERNALEEPROM_H
