#include "GraphicLcd.h"
#include "LCDUI_Text.h"
#include "Debug.h"

#include "menu.h"

#define DISPLAY_MAX_X                       127
#define DISPLAY_MAX_Y                       127

#define MENU_X                              0
#define MENU_Y                              0
#define MENU_X2                             DISPLAY_MAX_X
#define MENU_Y2                             DISPLAY_MAX_Y

///////////////////////////////////////////////////////////////////////////////

#define MENU_TITLE_X                        MENU_X + 2
#define MENU_TITLE_Y                        MENU_Y + 1

#define MENU_FRAME_X                        MENU_X
#define MENU_FRAME_Y                        MENU_TITLE_Y + 11
#define MENU_FRAME_X2                       MENU_X2
#define MENU_FRAME_Y2                       MENU_Y2

#define MENU_ITEM_AREA_X                    MENU_X + 2
#define MENU_ITEM_AREA_Y                    MENU_FRAME_Y + 2
#define MENU_ITEM_AREA_X2                   MENU_X2 - 2
#define MENU_ITEM_AREA_Y2                   MENU_Y2 - 2

#define MENU_ITEM_SPACING                   1
#define MENU_ITEM_BASE_HEIGHT               10
#define MENU_ITEM_HEIGHT                    23
#define MENU_ITEM_TEXT_X_OFFSET             2
#define MENU_ITEM_TEXT_Y_OFFSET             1
#define MENU_ITEM_FIELD_TEXT_X_OFFSET       2
#define MENU_ITEM_FIELD_TEXT_Y_OFFSET       2
#define MENU_ITEM_INT_FIELD_X_OFFSET        0
#define MENU_ITEM_INT_FIELD_Y_OFFSET        2
#define MENU_ITEM_PROGRESS_FIELD_X_OFFSET   18
#define MENU_ITEM_PROGRESS_FIELD_Y_OFFSET   2
#define MENU_ITEM_PROGRESS_FIELD_WIDTH      MENU_ITEM_AREA_X2 - MENU_ITEM_AREA_X - MENU_ITEM_PROGRESS_FIELD_X_OFFSET - 32
#define MENU_ITEM_PROGRESS_FIELD_HEIGHT     8
#define MENU_ITEM_SEPARATOR_TEXT_X_OFFSET   10
#define MENU_ITEM_SEPARATOR_Y_OFFSET        MENU_ITEM_BASE_HEIGHT / 2

///////////////////////////////////////////////////////////////////////////////

uint8_t ItemHeight(MenuItemType itemType, uint8_t selected);
uint8_t YCoordOfSelectedItem(Menu *menu);
void DrawItem(Menu *menu, uint8_t y, uint8_t index, uint8_t selected);
void DrawItemIntegerField(Menu *menu, MenuItem *item, uint8_t y);
void DrawItemProgressField(Menu *menu, MenuItem *item, uint8_t y);
void DrawItemIPAddressField(Menu *menu, MenuItem *item, uint8_t y);
void DrawItemTimeField(Menu *menu, MenuItem *item, uint8_t y);
void DrawItemToggleField(Menu *menu, MenuItem *item, uint8_t y);
void SelectedItemPlus(Menu *menu);
void SelectedItemMinus(Menu *menu);
void SelectedItemLeft(Menu *menu);
void SelectedItemRight(Menu *menu);
void SelectedItemRestoreOriginal(Menu *menu);
//uint8_t GroupOfItemHasOtherItems(Menu *menu)

int IntDiv(int val, int div);
uint8_t GetOctet(int32_t value, uint8_t nth);
void SetOctet(uint8_t nth, uint8_t octet, int32_t *value);

///////////////////////////////////////////////////////////////////////////////

void MenuInit(Menu *menu, const char *title, MenuItem **items, uint8_t itemCount)
{
    menu->Title = title;
    menu->Items = items;
    menu->ItemCount = itemCount;

    MenuReset(menu);
}

void MenuReset(Menu *menu)
{
    menu->_currentItemY = 0;
    menu->_indexOffset = 0;
    menu->_selectionIndex = 0;
    menu->_subFieldIndex = -1;

    while (menu->Items[menu->_selectionIndex]->Type == ITEM_SEPARATOR)
        menu->_selectionIndex++;
}

void MenuItemInit(MenuItem *item, const char *text, MenuItemType type, uint32_t value, uint32_t min, uint32_t max)
{
    item->Text = text;
    item->Type = type;
    item->Value = value;
    item->Max = max;
    item->Min = min;
}

void MenuDraw(Menu *menu)
{
    menu->_lcd->clearBuf();

    // Draw frame
    menu->_lcd->drawRect(MENU_X, MENU_Y, MENU_X2, MENU_FRAME_Y, false, 1);
    menu->_lcd->drawRect(MENU_FRAME_X, MENU_FRAME_Y, MENU_FRAME_X2, MENU_FRAME_Y2, false, 1);

    // Draw title text
    menu->_text->drawText(MENU_TITLE_X, MENU_TITLE_Y, menu->Title, 0);

    // Draw (visible) menu items
    uint8_t y = MENU_ITEM_AREA_Y;
    for (uint8_t i = menu->_indexOffset; i < menu->ItemCount; ++i)
    {
        uint8_t selected = 0;
        if (i == menu->_selectionIndex)
            selected = 1;

        uint8_t itemHeight = ItemHeight(menu->Items[i]->Type, selected);
        if (y + itemHeight > MENU_ITEM_AREA_Y2)
            break;

        if (selected)
            menu->_currentItemY = y;

        DrawItem(menu, y, i, selected);
        y += itemHeight;
    }

    menu->_lcd->writeBuf(false);
}

void MenuKeyPress(Menu *menu, KeyCode key)
{
    switch (key)
    {
        case KEY_UP:
            if (menu->_selectionIndex > 0)
            {
                while (menu->_selectionIndex > 0)
                {
                    menu->_selectionIndex--;
                    if (menu->Items[menu->_selectionIndex]->Type != ITEM_SEPARATOR)
                        break;
                    else if (menu->_selectionIndex == 0)
                    {
                        menu->_selectionIndex++;
                        menu->_indexOffset = 0;
                        break;
                    }
                }

                menu->_currentFieldOriginalValue = menu->Items[menu->_selectionIndex]->Value;
                menu->_subFieldIndex = -1;
                if (menu->_selectionIndex < menu->_indexOffset)
                    menu->_indexOffset = menu->_selectionIndex;
            }
            break;

        case KEY_DOWN:
            if (menu->_selectionIndex < menu->ItemCount - 1)
            {
                while (menu->_selectionIndex < menu->ItemCount - 1)
                {
                    menu->_selectionIndex++;
                    if (menu->Items[menu->_selectionIndex]->Type != ITEM_SEPARATOR)
                        break;
                    else if (menu->_selectionIndex == menu->ItemCount - 1)
                    {
                        while (YCoordOfSelectedItem(menu) > (MENU_ITEM_AREA_Y2 - MENU_ITEM_HEIGHT))
                            menu->_indexOffset++;
                        menu->_selectionIndex--;
                        break;
                    }
                }

                menu->_currentFieldOriginalValue = menu->Items[menu->_selectionIndex]->Value;
                menu->_subFieldIndex = -1;
                while (YCoordOfSelectedItem(menu) > MENU_ITEM_AREA_Y2)
                    menu->_indexOffset++;
            }
            break;

        case KEY_PLUS:
            SelectedItemPlus(menu);
            break;

        case KEY_MINUS:
            SelectedItemMinus(menu);
            break;

        case KEY_LEFT:
            SelectedItemLeft(menu);
            break;

        case KEY_RIGHT:
            SelectedItemRight(menu);
            break;

        case KEY_ESC:
            SelectedItemRestoreOriginal(menu);
            break;

        default:
            return;
    }

    MenuDraw(menu);
}

///////////////////////////////////////////////////////////////////////////////

uint8_t ItemHeight(MenuItemType itemType, uint8_t selected)
{
    uint8_t height = MENU_ITEM_BASE_HEIGHT;
    if (itemType != ITEM_SIMPLE && itemType != ITEM_SEPARATOR && selected)
        height = MENU_ITEM_HEIGHT;
    height += MENU_ITEM_SPACING;
    return height;
}

uint8_t YCoordOfSelectedItem(Menu *menu)
{
    uint8_t y = MENU_ITEM_AREA_Y;
    for (uint8_t i = menu->_indexOffset; i < menu->ItemCount; ++i)
    {
        uint8_t selected = menu->_selectionIndex == i;
        y += ItemHeight(menu->Items[i]->Type, selected);
        if (selected)
            break;
    }
    return y;
}

void DrawItem(Menu *menu, uint8_t y, uint8_t index, uint8_t selected)
{
    MenuItem *item = menu->Items[index];

    // Draw selection rect and data field
    if (selected)
    {
        if (item->Type != ITEM_SEPARATOR)
        {
            menu->_lcd->drawRect(MENU_ITEM_AREA_X, y, MENU_ITEM_AREA_X2, y + MENU_ITEM_BASE_HEIGHT, true, 1);
            if (item->Type != ITEM_SIMPLE)
                menu->_lcd->drawRect(MENU_ITEM_AREA_X, y + MENU_ITEM_BASE_HEIGHT, MENU_ITEM_AREA_X2, y + MENU_ITEM_HEIGHT - MENU_ITEM_SPACING, false, 1);

            // Usable keys notes
            switch (item->Type)
            {
                case ITEM_INT:
                case ITEM_PROGRESS:
                case ITEM_TOGGLE:
                    menu->_text->drawText(MENU_ITEM_AREA_X + MENU_ITEM_FIELD_TEXT_X_OFFSET, y + MENU_ITEM_BASE_HEIGHT + MENU_ITEM_FIELD_TEXT_Y_OFFSET, "+-", 0, 0);
                    break;

                case ITEM_IPADDR:
                case ITEM_TIME:
                    menu->_text->drawText(MENU_ITEM_AREA_X + MENU_ITEM_FIELD_TEXT_X_OFFSET, y + MENU_ITEM_BASE_HEIGHT + MENU_ITEM_FIELD_TEXT_Y_OFFSET, "+-<>", 0, 0);
                    break;

                default:
                    break;
            }
        }


        // Field value
        switch (item->Type)
        {
            case ITEM_INT:
                DrawItemIntegerField(menu, item, y);
                break;
            case ITEM_PROGRESS:
                DrawItemProgressField(menu, item, y);
                break;
            case ITEM_IPADDR:
                DrawItemIPAddressField(menu, item, y);
                break;
            case ITEM_TIME:
                DrawItemTimeField(menu, item, y);
                break;
            case ITEM_TOGGLE:
                DrawItemToggleField(menu, item, y);
                break;
            default:
                break;
        }

        menu->_currentItemY = y;
    }

    if (item->Type == ITEM_SEPARATOR)
    {
        menu->_lcd->drawLine(MENU_ITEM_AREA_X, y + MENU_ITEM_SEPARATOR_Y_OFFSET, MENU_ITEM_AREA_X2, y + MENU_ITEM_SEPARATOR_Y_OFFSET, 1);
        menu->_text->drawText(MENU_ITEM_AREA_X + MENU_ITEM_SEPARATOR_TEXT_X_OFFSET, y, item->Text, 0);
    }
    else
        menu->_text->drawText(MENU_ITEM_AREA_X + MENU_ITEM_TEXT_X_OFFSET, y, item->Text, selected);
}

// Menu is unnecessary here
void DrawItemIntegerField(Menu* menu, MenuItem *item, uint8_t y)
{
    char text[6] = "";
    snprintf(text, 6, "%d", item->Value);
    menu->_text->drawText(MENU_ITEM_AREA_X2 - MENU_ITEM_INT_FIELD_X_OFFSET, y + MENU_ITEM_BASE_HEIGHT + MENU_ITEM_INT_FIELD_Y_OFFSET, text, 0, 1);
}

void DrawItemProgressField(Menu *menu, MenuItem *item, uint8_t y)
{
    DrawItemIntegerField(menu, item, y);

    // Draw the progress bar
    menu->_lcd->drawRect(MENU_ITEM_PROGRESS_FIELD_X_OFFSET,
                         y + MENU_ITEM_BASE_HEIGHT + MENU_ITEM_PROGRESS_FIELD_Y_OFFSET,
                         MENU_ITEM_PROGRESS_FIELD_X_OFFSET + MENU_ITEM_PROGRESS_FIELD_WIDTH,
                         y + MENU_ITEM_BASE_HEIGHT + MENU_ITEM_PROGRESS_FIELD_Y_OFFSET + MENU_ITEM_PROGRESS_FIELD_HEIGHT,
                         false, 1);
    int w = IntDiv(IntDiv((item->Value - item->Min) * 100, item->Max - item->Min) * (MENU_ITEM_PROGRESS_FIELD_WIDTH - 3), 100);
    if (w > 0) {
        menu->_lcd->drawRect(MENU_ITEM_PROGRESS_FIELD_X_OFFSET + 2,
                             y + MENU_ITEM_BASE_HEIGHT + MENU_ITEM_PROGRESS_FIELD_Y_OFFSET + 2,
                             MENU_ITEM_PROGRESS_FIELD_X_OFFSET + 1 + w,
                             y + MENU_ITEM_BASE_HEIGHT + MENU_ITEM_PROGRESS_FIELD_Y_OFFSET + MENU_ITEM_PROGRESS_FIELD_HEIGHT - 2,
                             true, 1);
    }
}

void DrawItemIPAddressField(Menu *menu, MenuItem *item, uint8_t y)
{
    int x = MENU_ITEM_AREA_X2 - MENU_ITEM_INT_FIELD_X_OFFSET;
    for (int8_t i = 3; i >= 0; --i)
    {
        char buf[4];
        uint8_t octet = (item->Value >> (8 * i)) & 0xff;
        snprintf(buf, sizeof (buf), "%03d", octet);
        menu->_text->drawText(x, y + MENU_ITEM_BASE_HEIGHT + MENU_ITEM_INT_FIELD_Y_OFFSET, buf, menu->_subFieldIndex == i, 1);
        x -= 17;
        if (i > 0)
            menu->_text->drawText(x, y + MENU_ITEM_BASE_HEIGHT + MENU_ITEM_INT_FIELD_Y_OFFSET, ".", 0);
    }
}

void DrawItemTimeField(Menu *menu, MenuItem *item, uint8_t y)
{
    int x = MENU_ITEM_AREA_X2 - MENU_ITEM_INT_FIELD_X_OFFSET;
    char buf[3];
    snprintf(buf, sizeof (buf), "%02d", (uint8_t)((item->Value >> 8) & 0xff));
    menu->_text->drawText(x, y + MENU_ITEM_BASE_HEIGHT + MENU_ITEM_INT_FIELD_Y_OFFSET, buf, menu->_subFieldIndex == 1, 1);
    snprintf(buf, sizeof (buf), "%d", (uint8_t)(item->Value & 0xff));
    x -= 13;
    menu->_text->drawText(x, y + MENU_ITEM_BASE_HEIGHT + MENU_ITEM_INT_FIELD_Y_OFFSET, buf, menu->_subFieldIndex == 0, 1);
    menu->_text->drawText(x, y + MENU_ITEM_BASE_HEIGHT + MENU_ITEM_INT_FIELD_Y_OFFSET, ":", 0, 0);
}

void DrawItemToggleField(Menu *menu, MenuItem *item, uint8_t y)
{
    menu->_text->drawText(MENU_ITEM_AREA_X2 - MENU_ITEM_INT_FIELD_X_OFFSET,
                          y + MENU_ITEM_BASE_HEIGHT + MENU_ITEM_INT_FIELD_Y_OFFSET,
                          item->Value ? "Enabled" : "Disabled", 0, 1);
}

void SelectedItemPlus(Menu *menu)
{
    MenuItem *item = menu->Items[menu->_selectionIndex];
    uint8_t update = 0;

    if (item->Type == ITEM_INT || item->Type == ITEM_PROGRESS)
    {
        if (item->Value < item->Max)
        {
            item->Value++;
            update = 1;
        }
    }
    else if (item->Type == ITEM_IPADDR && menu->_subFieldIndex >= 0)
    {
        uint8_t octet = GetOctet(item->Value, menu->_subFieldIndex);
        ++octet;
        SetOctet(menu->_subFieldIndex, octet, &item->Value);
        update = 1;
    }
    else if (item->Type == ITEM_TIME && menu->_subFieldIndex >= 0)
    {
        uint8_t octet = GetOctet(item->Value, menu->_subFieldIndex);
        ++octet;
        if (menu->_subFieldIndex == 0)
            octet %= 24;
        else
            octet %= 60;
        SetOctet(menu->_subFieldIndex, octet, &item->Value);
        update = 1;
    }
    else if (item->Type == ITEM_TOGGLE)
    {
        ++item->Value %= 2;
        update = 1;
    }

    if (update)
        DrawItem(menu, menu->_currentItemY, menu->_selectionIndex, 1);
}

void SelectedItemMinus(Menu *menu)
{
    MenuItem *item = menu->Items[menu->_selectionIndex];
    uint8_t update = 0;

    if (item->Type == ITEM_INT || item->Type == ITEM_PROGRESS)
    {
        if (item->Value > item->Min)
        {
            item->Value--;
            update = 1;
        }
    }
    else if (item->Type == ITEM_IPADDR && menu->_subFieldIndex >= 0)
    {
        uint8_t octet = GetOctet(item->Value, menu->_subFieldIndex);
        --octet;
        SetOctet(menu->_subFieldIndex, octet, &item->Value);
        update = 1;
    }
    else if (item->Type == ITEM_TIME && menu->_subFieldIndex >= 0)
    {
        uint8_t octet = GetOctet(item->Value, menu->_subFieldIndex);
        --octet;
        if (menu->_subFieldIndex == 0)
        {
            if (octet > 23)
                octet = 23;
            octet %= 24;
        }
        else
        {
            if (octet > 59)
                octet = 59;
            octet %= 60;
        }
        SetOctet(menu->_subFieldIndex, octet, &item->Value);
        update = 1;
    }
    else if (item->Type == ITEM_TOGGLE)
    {
        --item->Value %= 2;
        update = 1;
    }

    if (update)
        DrawItem(menu, menu->_currentItemY, menu->_selectionIndex, 1);
}

int IntDiv(int val, int div)
{
    _debug("val = " << val);
    int count = 0;
    while (val >= div) {
        count++;
        val -= div;
    }
    return count;
}

void SelectedItemLeft(Menu *menu)
{
    MenuItem *item = menu->Items[menu->_selectionIndex];
    uint8_t update = 0;

    --menu->_subFieldIndex;

    switch (item->Type)
    {
        case ITEM_IPADDR:
            if (menu->_subFieldIndex < 0)
                menu->_subFieldIndex = 3;
            menu->_subFieldIndex %= 4;
            break;
        case ITEM_TIME:
            if (menu->_subFieldIndex < 0)
                menu->_subFieldIndex = 1;
            menu->_subFieldIndex %= 2;
            break;
        default:
            break;
    }

    if (update)
        DrawItem(menu, menu->_currentItemY, menu->_selectionIndex, 1);
}

void SelectedItemRight(Menu *menu)
{
    MenuItem *item = menu->Items[menu->_selectionIndex];
    uint8_t update = 0;

    switch (item->Type)
    {
        case ITEM_IPADDR:
            ++menu->_subFieldIndex %= 4;
            break;
        case ITEM_TIME:
            ++menu->_subFieldIndex %= 2;
            break;
        default:
            break;
    }

    if (update)
        DrawItem(menu, menu->_currentItemY, menu->_selectionIndex, 1);
}

void SelectedItemRestoreOriginal(Menu *menu)
{
    MenuItem *item = menu->Items[menu->_selectionIndex];
    item->Value = menu->_currentFieldOriginalValue;
    DrawItem(menu, menu->_currentItemY, menu->_selectionIndex, 1);
}

uint8_t GetOctet(int32_t value, uint8_t nth)
{
    if (nth > 3)
        return 0;

    return (value >> (8 * nth)) & 0xff;
}

void SetOctet(uint8_t nth, uint8_t octet, int32_t *value)
{
    if (nth > 3)
        return;

    *value &= 0xffffffff - (0xff << (8 * nth));
    *value |= (uint32_t)octet << (8 * nth);
}
