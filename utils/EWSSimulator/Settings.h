#ifndef SETTINGS_H
#define SETTINGS_H

#include <QObject>
#include <QSettings>

class Settings : public QObject
{
    Q_OBJECT

public:
    explicit Settings(QObject *parent = 0);
    ~Settings();

    QByteArray loadGeometry();
    void saveGeometry(QByteArray geometry);

    QByteArray loadEepromData();
    void saveEepromData(QByteArray data);

private:
    QSettings *m_settings;
};

#endif // SETTINGS_H
