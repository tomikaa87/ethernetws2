#include <htc.h>
#include <stdio.h>
#include <string.h>

#include <CRC16.h>

#define printf (void)
#ifndef printf
#include <stdio.h>
#endif

/*** PIC configuration ********************************************************/

__CONFIG(1, HSPLL);
__CONFIG(2, BOREN & BORV28 & WDTDIS);
__CONFIG(3, PBDIGITAL);
__CONFIG(4, LVPDIS);

/*** PIC routines *************************************************************/

void PIC_init()
{
    // Interrupts
    IPEN = 1;                           // Enable interrupt priorities
    GIEH = 1;                           // Global interrupt enable (high)
    GIEL = 1;                           // Global interrupt enable (low)
    RCIP = 1;                           // USART RX interrupt = high priority
    RCIE = 1;
}    

/*** UART routines ************************************************************/

#define USART_BUF_SIZE  64

unsigned char usart_buf[USART_BUF_SIZE];
unsigned char usart_buf_next_in = 0;
unsigned char usart_buf_next_out = 0;

#define UART_is_buf_byte_ready (usart_buf_next_in != usart_buf_next_out)

/**
 * Initializes the USART module.
 */
void UART_init()
{
    // Async, 8 bit, 115200 (@32 MHz) bps
    TX9 = 0;
    SYNC = 0;
    BRGH = 1;
    SPBRG = 21;
    SPBRGH = 0;
    SPEN = 1;
    CREN = 1;
    SREN = 0;
    TXEN = 1;
    TRISC7 = 1;
    TRISC6 = 0;
    
    printf("UART initialization finished\r\n");
}

/**
 * Sends the given @p byte through the USART port.
 */
void putch(unsigned char byte)
{
    // Wait for TX to become ready
    while (!TRMT)
        continue;

    // Write the byte
    TXREG = byte;
}

/**
 * Reads a byte from the USART port.
 *
 * @return The read byte.
 */
unsigned char getch(void)
{
    // Wait for incoming data
    while (!RCIF)
        continue;

    // Read it out from the buffer
    return RCREG;
}

unsigned char getbufch()
{
    unsigned char c;
    
    // Wait untit a byte is ready to read from the USART buffer
    while (!UART_is_buf_byte_ready)
        continue;       
    
    c = usart_buf[usart_buf_next_out];
    usart_buf_next_out = (usart_buf_next_out + 1) % USART_BUF_SIZE;
    
    return c;
}

/*** ISRs *********************************************************************/

/**
 * (Normal priority) Interrupt Service Routine
 */
void interrupt isr()
{
    if (RCIE && RCIF)
    {
        unsigned char c;
        
        c = RCREG;
        usart_buf[usart_buf_next_in] = c;
        c = usart_buf_next_in;
        usart_buf_next_in = (usart_buf_next_in + 1) % USART_BUF_SIZE;
        if (usart_buf_next_in == usart_buf_next_out)
            usart_buf_next_in = c;
    }
}

/*** EEPROM simulator routines ************************************************/

unsigned char eepromData[256];

void EXT_EEPROM_init()
{
    printf("External EEPROM initialized\r\n");   
}    

void EXT_EEPROM_writeByte(unsigned long address, unsigned char byte)
{
    printf("EEPROM write: 0x%02X -> 0x%08lX(0x%02X)\r\n", byte, address,
        (unsigned char)(address & 0xFF));
    eepromData[(unsigned char)(address & 0xFF)] = byte;
}    

unsigned char EXT_EEPROM_readByte(unsigned long address)
{
    printf("EEPROM read: 0x%02X <- 0x%08lX(0x%02X)\r\n", 
        eepromData[(unsigned char)(address & 0xFF)], address,
        (unsigned char)(address & 0xFF));
    return eepromData[(unsigned char)(address & 0xFF)];
}    

/*** SerialReader routines ****************************************************/

enum
{
    SR_INIT,
    SR_FRAME,
    SR_HEADER,
    SR_DATA
} SR_state = SR_INIT;

void SR_init()
{
    printf("SerialReader initializing...\r\n");
    
    SR_state = SR_INIT;
}

    static unsigned char headerData[7], tmp;
    static unsigned char headerDataPtr, dataPtr;
    static unsigned long address;
    static unsigned int checksum, interCrc;

void SR_yield(char c)
{
    unsigned char i;
    
    printf("SR_yield: '%c' (0x%02X)\r\n", c, c);
    
    switch (SR_state)
    {
        case SR_INIT:
        {
            if (c == 0xFA)
            {
                printf("Data frame MSB detected\r\n");
                SR_state = SR_FRAME;   
            }
            
            break;       
        }       
        
        case SR_FRAME:
        {
            if (c == 0xFB)
            {
                printf("Data frame LSB detected\r\n");   
                memset(headerData, 0, sizeof (headerData));
                headerDataPtr = 0;               
                SR_state = SR_HEADER;
            }     
            else
                SR_state = SR_INIT;  
            
            break;
        }    
        
        case SR_HEADER:
        {
            headerData[headerDataPtr++] = c;
            if (headerDataPtr == sizeof (headerData))
            {
                printf("Header received, receiving data\r\n");
                
                address = headerData[0];
                for (i = 1; i <= 3; i++)
                {
                    address <<= 8;
                    address |= headerData[i];
                }    
                /*address <<= 8;
                address |= headerData[1];
                address <<= 8;
                address |= headerData[2];
                address <<= 8;
                address |= headerData[3];*/
                printf("Base address: 0x%08lX\r\n", address);
                
                checksum = headerData[5] << 8;
                checksum |= headerData[6];
                printf("Data checksum: %04X\r\n", checksum);
                
                CRC16_reset();
                dataPtr = 0;
                SR_state = SR_DATA;
            }    
            
            break;
        }    
        
        case SR_DATA:
        {            
            EXT_EEPROM_writeByte(address + dataPtr++, c);
            if (dataPtr == headerData[4])
            {
                //printf("Data received, checksum: %04X\r\n", CRC16_value());
                printf("Verifying data...\r\n");
                CRC16_reset();
                
                putch(0xFA);
                putch(0xFB);

                for (dataPtr = 0; dataPtr < headerData[4]; dataPtr++)
                    CRC16_byte(EXT_EEPROM_readByte(address + dataPtr));                 
                
                putch((CRC16_value() >> 8) & 0xFF);
                putch(CRC16_value() & 0xFF);
                if (CRC16_value() != checksum)
                {
                    printf("Checksum error, calculated: %04X\r\n", CRC16_value());                    
                    putch(0);
                }    
                else
                {
                    printf("Data verified successfully\r\n");
                    putch(1);
                }    
                SR_state = SR_INIT;   
            }    
            
            break;
        }    
    }    
}        

/*** Main *********************************************************************/

void main()
{
    PIC_init();
    UART_init();
    EXT_EEPROM_init();
    SR_init();
    
    printf("System init finished\r\n");
    
	while (1)
	{
        if (UART_is_buf_byte_ready)
        {
            while (UART_is_buf_byte_ready)
                SR_yield(getbufch());
        }    
    }   	
}
