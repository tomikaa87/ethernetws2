/**
 * Ethernet Weather Station v2 Firmware
 * Useful test functions
 *
 * @author Tamas Karpati <tomikaa87>
 * @date 2011-02-11
 * @file LCDUI_NumberDialog.h
 */

#include "Types.h"
#include "Debug.h"

void typesTest()
{
    int8_adv i8a;
    int16_adv i16a;

    i8a.ui = 0xAB;
    _debug("Testing int8_adv with value 0xAB (10101011)\r\n");

    if (i8a.ui != 171)
        _debug("i8a.ui failed: %X\r\n", i8a.ui);
    if (i8a.i != -85)
        _debug("i8a.i failed: %d\r\n", i8a.i);
    if (i8a.loNibble != 0xB)
        _debug("i8a.loNibble failed: %X\r\n", i8a.loNibble);
    if (i8a.hiNibble != 0xA)
        _debug("i8a.hiNibble failed: %X\r\n", i8a.hiNibble);
    if (i8a.bit1 != 1 || i8a.bit2 != 1 || i8a.bit3 != 0 || i8a.bit4 != 1 ||
            i8a.bit5 != 0 || i8a.bit6 != 1 || i8a.bit7 != 0 || i8a.bit8 != 1)
        _debug("i8a bit test failed\r\n");

    i16a.ui = 0xABCD;
    _debug("Testing int16_adv with value 0xABCD (10101011 11001101)\r\n");

    if (i16a.ui != 43981)
        _debug("i16a.ui failed: %X\r\n", i16a.ui);
    if (i16a.i != -21555)
        _debug("i16a.i failed: %d\r\n", i16a.i);
    if (i16a.lo != 0xCD)
        _debug("i16a.lo failed: %X\r\n", i16a.lo);
    if (i16a.hi != 0xAB)
        _debug("i16a.hi failed: %X\r\n", i16a.hi);
    i8a.ui = 0xCD;
    if (i16a.lo_adv.ui != i8a.ui)
        _debug("i16a.lo_adv failed: %X\r\n", i16a.lo_adv.ui);
    if (i16a.bytes_adv[0].i != i8a.i)
        _debug("i16a.bytes_adv[0] failed: %X\r\n", i16a.bytes_adv[0].ui);
    i8a.ui = 0xAB;
    if (i16a.hi_adv.ui != i8a.ui)
        _debug("i16a.hi_adv failed: %X\r\n", i16a.hi_adv.ui);
    if (i16a.bytes_adv[1].i != i8a.i)
        _debug("i16a.bytes_adv[1] failed: %X\r\n", i16a.bytes_adv[1].ui);
}