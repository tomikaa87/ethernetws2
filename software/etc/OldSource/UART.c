/**
 * Ethernet Weather Station v2 Firmware
 * UART communication routines
 *
 * @author Tamas Karpati <tomikaa87>
 * @date 2011-09-07
 * @file UART.c
 */

#include "UART.h"
#include "TCPIPStack/TCPIP.h"
#include "Debug.h"
#include "25LC1024.h"
#include "CRC16.h"
#include "graphlcd.h"

#include <htc.h>
#include <string.h>
#include <ctype.h>

/*** Golbal variables *********************************************************/

extern ExtSensorData g_extSensorData;

/*** Constant definitions *****************************************************/

#define WRITER_FRAME_FIRST      0xFA
#define WRITER_FRAME_SECOND     0xFB

/*** Global variables *********************************************************/

/**
 * Initializes the USART module.
 */
void serial_init()
{
    uint8 i;

    // Async, 8 bit, 115200 (@32 MHz) bps
    TX9 = 0;
    SYNC = 0;
    BRGH = 1;
    SPBRG = 42;
    SPBRGH = 0;
    SPEN = 1;
    CREN = 1;
    SREN = 0;
    TXEN = 1;
    TRISC7 = 1;
    TRISC6 = 0;

    for (i = 0; i < 4; i++)
    {
        putch('\r');
        putch('\n');
    }
}

/**
 * Sends the given @p byte through the USART port.
 */
void putch(uint8 byte)
{
    // Wait for TX to become ready
    while (!TRMT)
        continue;

    // Write the byte
    TXREG = byte;

    // Wait for the byte to be transmitted
    while (!TRMT)
        continue;
}

/**
 * Reads a byte from the USART port.
 *
 * @return The read byte.
 */
uint8 getch(void)
{
    // Wait for incoming data
    while (!RCIF)
        continue;

    // Read it out from the buffer
    return RCREG;
}

uint8 getbufch()
{
    uint8 c;

    // Wait untit a byte is ready to read from the USART buffer
    while (!UART_isBufByteReady)
        continue;

    c = g_uartBuffer[g_uartBufferNextOut];
    g_uartBufferNextOut = (g_uartBufferNextOut + 1) % USART_BUF_SIZE;

    return c;
}

void putbufch(uint8 c)
{
    g_uartBuffer[g_uartBufferNextIn] = c;
    c = g_uartBufferNextIn;
    g_uartBufferNextIn = (g_uartBufferNextIn + 1) % USART_BUF_SIZE;
    if (g_uartBufferNextIn == g_uartBufferNextOut)
        g_uartBufferNextIn = c;
}

enum
{
    S_INIT,
    S_FRAME_BEGIN,
    S_READ_LENGTH,
    S_READ_BASE_ADDR,
    S_READ_DATA,
    S_READ_CRC,

    S_TX_FRAME_BEGIN,
    S_TX_READ_BASE_ADDR,
    S_TX_READ_LENGTH,

    S_SENSOR_FRAME_BEGIN,
    S_SENSOR_READ_DATA
} g_taskState = S_INIT;

/**
 * Does all the tasks needed for UART communication.
 */
void serial_task()
{
    char c;
    static uint8 buf[128], bufPtr;
    static uint8 data[32], dataPtr;
    static uint32 baseAddress;
    static uint16 checksum;
    static uint8 mode;
    static int32 length, i;

    if (!UART_isBufByteReady)
        return;

    c = getbufch();
    //_debug("c = %02X (%c)\r\n", c, c);

    if (c == '*')
    {
        //_debug("resetting parser\r\n");
        g_taskState = S_INIT;
        mode = 0;
        return;
    }

    /*
     Packet format: ;;LENGTH_HEX:BASE_ADDR_HEX:DATA_HEX:CRC_HEX;
     Transmit request packet format: &;BASE_ADDR_HEX:LENGTH_HEX;
     Sensor packet format: @;BATT_V:TEMP:RH;
     */

    switch (g_taskState)
    {
        // Wait for the first byte of a packet
        case S_INIT:
            // EEPROM data packet begining
            if (c == ';')
                g_taskState = S_FRAME_BEGIN;
            // GLCD data packet begining
            else if (c == ':') {
                mode = 1;
                g_taskState = S_FRAME_BEGIN;
            // Sensor data packet begining
            } else if (c == '@') {
                g_taskState = S_SENSOR_FRAME_BEGIN;
            // EEPROM data read packet begining
            } else if (c == '&') {
                g_taskState = S_TX_FRAME_BEGIN;
            }
            break;

        case S_FRAME_BEGIN:
            if (c == ';')
            {
                g_taskState = S_READ_LENGTH;
                bufPtr = 0;
                memset(buf, 0, sizeof (buf));
            }
            else
            {
                g_taskState = S_INIT;
            }
            break;

        case S_READ_LENGTH:
            if (c == ':')
            {
                // Try to convert read value to an integer
                length = strtol(buf, NULL, 16);
                if (length == 0 || length > sizeof (data))
                {
                    // Send error message that data is too long and reset
                    printf(";;1:Invalid length (min 0, max %u);\r\n",
                            sizeof (data));
                    g_taskState = S_INIT;
                    break;
                }

                _debug("Data length (header): %u\r\n", length);

                memset(buf, 0, sizeof (buf));
                bufPtr = 0;
                
                g_taskState = S_READ_BASE_ADDR;
                break;
            }
            else
            {
                if (isxdigit(c))
                    buf[bufPtr++] = c;
                else
                {
                    // Invalid character received, send error message and reset
                    printf(";;2:Invalid hex digit;\r\n");
                    g_taskState = S_INIT;
                }
            }
            break;

        case S_READ_BASE_ADDR:
            if (c == ':')
            {
                if (bufPtr == 0 || bufPtr > 6)
                {
                    printf(";;3:Invalid base address;\r\n");
                    g_taskState = S_INIT;
                    break;
                }

                baseAddress = strtol(buf, NULL, 16);

                _debug("base: %06lX\r\n", baseAddress);

                memset(buf, 0, sizeof (buf));
                bufPtr = 0;

                g_taskState = S_READ_DATA;
            }
            else
            {
                if (isxdigit(c))
                    buf[bufPtr++] = c;
                else
                {
                    // Invalid character received, send error message and reset
                    printf(";;2:Invalid hex digit;\r\n");
                    g_taskState = S_INIT;
                }
            }
            break;

        case S_READ_DATA:
            if (c == ':')
            {
                // Check received byte count, if not dividable by 2,
                // or zero, send error message and reset
                if (bufPtr % 2 != 0 || bufPtr == 0)
                {
                    printf(";;4:Invalid data length (%u);\r\n",
                            bufPtr);
                    g_taskState = S_INIT;
                    break;
                }

                checksum_reset();

                i = 0;
                memset(data, 0, sizeof (data));
                dataPtr = 0;
                
                while (i < bufPtr)
                {
                    // Read a hexadecimal byte from the buffer
                    char hexByte[3];
                    uint8 byte;
                    hexByte[0] = buf[i++];
                    hexByte[1] = buf[i++];
                    hexByte[2] = 0;

                    // Try to convert the byte to integer
                    byte = strtol(hexByte, NULL, 16);
                    
                    checksum_byte(byte);
                    data[dataPtr++] = byte;
                }

                checksum = checksum_value();

                _debug("Calculated CRC: %04X\r\n", checksum);

                memset(buf, 0, sizeof (buf));
                bufPtr = 0;

                g_taskState = S_READ_CRC;
                break;
            }
            else
            {
                if (isxdigit(c))
                    buf[bufPtr++] = c;
                else
                {
                    // Invalid character received, send error message and reset
                    printf(";;2:Invalid hex digit;\r\n");
                    g_taskState = S_INIT;
                }
            }
            break;

        case S_READ_CRC:
            if (c == ';')
            {
                if (bufPtr == 0 || bufPtr > 4)
                {
                    printf(";;5:Invalid checksum;\r\n");
                    g_taskState = S_INIT;
                    break;
                }

                if (checksum != strtol(buf, NULL, 16))
                {
                    printf(";;6;Checksum mismatch;\r\n");
                    g_taskState = S_INIT;
                    break;
                }

                if (mode == 0)
                {
                    // This mode writes received data into the external EEPROM

                    // Write data to external EEPROM
                    if (ext_eeprom_write_array(baseAddress, data, dataPtr) != dataPtr)
                    {
                        printf(";;7:Data write error;\r\n");
                        g_taskState = S_INIT;
                        break;
                    }

                    // Verify written data
                    if (ext_eeprom_read_array(baseAddress, data, dataPtr) != dataPtr)
                    {
                        printf(";;8:Data verify error;\r\n");
                        g_taskState = S_INIT;
                        break;
                    }

                    // Verify written data checksum
                    checksum_reset();
                    for (i = 0; i < dataPtr; i++)
                        checksum_byte(data[i]);
                    if (checksum_value() != checksum)
                    {
                        printf(";;9:Data checksum error;\r\n");
                    }

                }
                else
                {
                    // This mode writes received data directly into the GLCD buffer
                    
                    for (i = 0; i < dataPtr; i++)
                        glcd_set_buf_byte(baseAddress + i, data[i]);
                    if (baseAddress + i >= 2048)
                        glcd_write_buf(0);
                }

                printf(";;0:OK;\r\n");

                g_taskState = S_INIT;
            }
            else
            {
                if (isxdigit(c))
                    buf[bufPtr++] = c;
                else
                {
                    // Invalid character received, send error message and reset
                    printf(";;2:Invalid hex digit;\r\n");
                    g_taskState = S_INIT;
                }
            }
            break;

        /*** EEPROM read data packet reader ***/

        // Probably an EEPROM read data packet received
        case S_TX_FRAME_BEGIN:
            if (c == ';') {
                // Initialize the buffer and go on to the next state
                g_taskState = S_TX_READ_BASE_ADDR;
                bufPtr = 0;
                memset(buf, 0, sizeof (buf));
            } else {
                // Packet invalid
                g_taskState = S_INIT;
            }
            break;

        case S_TX_READ_BASE_ADDR:
            if (c == ':') {
                if (bufPtr == 0 || bufPtr > 6) {
                    printf(";;3:Invalid base address;\r\n");
                    g_taskState = S_INIT;
                    break;
                }

                baseAddress = strtol(buf, NULL, 16);

                _debug("base: %06lX\r\n", baseAddress);

                memset(buf, 0, sizeof (buf));
                bufPtr = 0;

                g_taskState = S_TX_READ_LENGTH;
            } else {
                if (isxdigit(c))
                    buf[bufPtr++] = c;
                else {
                    // Invalid character received, send error message and reset
                    printf(";;2:Invalid hex digit;\r\n");
                    g_taskState = S_INIT;
                }
            }
            break;

        case S_TX_READ_LENGTH:
            if (c == ';') {
                if (bufPtr == 0 || bufPtr > 6) {
                    printf(";;4:Invalid data length (%u);\r\n", bufPtr);
                    g_taskState = S_INIT;
                    break;
                }
                
                // Try to convert read value to an integer
                length = strtol(buf, NULL, 16);
                if (length == 0 ||
                        length > EXT_EEPROM_SIZE_BYTES - baseAddress) {
                    // Send error message that data is too long and reset
                    printf(";;1:Invalid length (min 0, max %u);\r\n",
                            sizeof (data));
                    g_taskState = S_INIT;
                    break;
                }

                _debug("Data length (header): %u\r\n", length);

                // Send data packet
                printf(";;&:");
                for (i = baseAddress; length > 0; length--) {
                    printf("%02X", ext_eeprom_read_byte(i));
                    i++;
                }
                printf(";\r\n");

                g_taskState = S_INIT;
                break;
            } else {
                if (isxdigit(c))
                    buf[bufPtr++] = c;
                else {
                    // Invalid character received, send error message and reset
                    printf(";;2:Invalid hex digit;\r\n");
                    g_taskState = S_INIT;
                }
            }
            break;

        /*** External sensor data reader ***/

        case S_SENSOR_FRAME_BEGIN:
            if (c == ';') {
                g_taskState = S_SENSOR_READ_DATA;
                bufPtr = 0;
                memset(buf, 0, sizeof (buf));
            } else {
                g_taskState = S_INIT;
            }
            break;

        // *@;[TEMP1_OUT]:[TEMP2_OUT]:[RH_OUT]:[PRES_OUT]:[VDD_OUT]:[ERR_OUT]:[TEMP_IN]:[RH_IN]:[VDD_IN]:[ERR_IN];
        case S_SENSOR_READ_DATA: {
            if (c == ';') {
                uint8 fieldIndex;
                char *token;

                buf[bufPtr] = 0;

                // Process received string
                token = strtok(buf, ":");
                if (token == NULL) {
                    printf("ExtSensorPacketError\r\n");
                } else {
                    fieldIndex = 0;
                    while (token != NULL) {
                        long value = strtol(token, NULL, 16);
                        if (fieldIndex == 0)
                            g_extSensorData.tempOut1 = value;
                        else if (fieldIndex == 1)
                            g_extSensorData.tempOut2 = value;
                        else if (fieldIndex == 2)
                            g_extSensorData.rhOut = value;
                        else if (fieldIndex == 3)
                            g_extSensorData.presOut = value;
                        else if (fieldIndex == 4)
                            g_extSensorData.presOutSeaLevel = value;
                        else if (fieldIndex == 5)
                            g_extSensorData.vddOut = value;
                        else if (fieldIndex == 6)
                            g_extSensorData.errOut = value;
                        else if (fieldIndex == 7)
                            g_extSensorData.tempIn = value;
                        else if (fieldIndex == 8)
                            g_extSensorData.rhIn = value;
                        else if (fieldIndex == 9)
                            g_extSensorData.vddIn = value;
                        else if (fieldIndex == 10)
                            g_extSensorData.errIn = value;

                        token = strtok(NULL, ":");
                        fieldIndex++;
                    }

                    if (fieldIndex == 11)
                        g_extSensorData.sensorDataUpdatedFlag = 1;
                }

                g_taskState = S_INIT;
            } else {
                if (isxdigit(c) || c == ':') {
                    buf[bufPtr++] = c;
                } else {
                    // Invalid character received, send error message and reset
                    printf(";;2:Invalid hex digit;\r\n");
                    g_taskState = S_INIT;
                }
            }

            break;
        }
    }
}
