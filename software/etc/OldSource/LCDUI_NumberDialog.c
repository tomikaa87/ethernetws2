/**
 * Ethernet Weather Station v2 Firmware
 * LCD User Interface - Number input dialog
 *
 * @author Tamas Karpati <tomikaa87>
 * @date 2011-02-11
 * @file LCDUI_NUMBERDIALOG.c
 */

#include "LCDUI_NUMBERDIALOG.h"
#include "drivers/graphlcd.h"
#include "LCDUI_Text.h"
#include "Debug.h"

/*** Private functions ********************************************************/

void draw_digits(LCDUI_NUMBERDIALOG *dialog)
{
    int8 digit, count;
    char str[2];

    _debug("ND::drawDigits\r\n");

    digit = 9;
    count = 0;
    str[1] = 0;
    while (digit >= 0 && count < dialog->length) {
        str[0] = dialog->digits[digit];
        lcdui_text_draw(100 - count * 6, 30, str,
                LCDUI_TEXT_FONT_LCD_LARGE, 0, 0);
        count++;
    }
}

void update(LCDUI_NUMBERDIALOG *dialog)
{
    _debug("ND::update\r\n");
    draw_digits(dialog);
}

/*** API functions ************************************************************/

void lcdui_numdlg_init(LCDUI_NUMBERDIALOG *dialog,
        int32 initValue, int8 digits, const char *title)
{
    int32 value;
    int8 digit;

    if (digits > 10)
        return;

    dialog->title = title;
    dialog->digitIndex = 0;
    dialog->length = digits;
    if (initValue < 0) {
        dialog->isNegative = 1;
        value = -1 * initValue;
    } else {
        dialog->isNegative = 0;
        value = initValue;
    }

    // Decompose the value to individual digits
    digit = 9;
    while (value > 0 && digit >= 0) {
        dialog->digits[digit] = value % 10;
        digit--;
        value /= 10;
    }
    // Fill the remaining space with zeroes
    while (digit >= 0) {
        dialog->digits[digit] = 0;
        digit--;
    }
}

void lcdui_numdlg_draw(LCDUI_NUMBERDIALOG *dialog)
{
    // Draw the dialog's frame
    //GLCD_drawRect(9, 21, 121, 109, GLCD_NO_FILL, GLCD_COLOR_BLACK);
    glcd_draw_rect(7, 19, 122, 110, GLCD_FILL, GLCD_COLOR_WHITE);
    glcd_draw_rect(8, 20, 120, 108, GLCD_NO_FILL, GLCD_COLOR_BLACK);
    glcd_draw_line(9, 109, 121, 109, GLCD_COLOR_BLACK);
    glcd_draw_line(121, 21, 121, 109, GLCD_COLOR_BLACK);

    lcdui_text_draw(10, 22, dialog->title, LCDUI_TEXT_FONT_PF_TEMPESTA,
            0, 0);

    update(dialog);

    glcd_write_buf(0);
}

void lcdui_numdlg_up(LCDUI_NUMBERDIALOG *dialog)
{

}

void lcdui_numdlg_down(LCDUI_NUMBERDIALOG *dialog)
{

}

void lcdui_numdlg_left(LCDUI_NUMBERDIALOG *dialog)
{

}

void lcdui_numdlg_right(LCDUI_NUMBERDIALOG *dialog)
{

}