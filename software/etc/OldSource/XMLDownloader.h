/**
 * Ethernet Weather Station v2 Firmware
 * Weather data downloader module
 *
 * @author Tamas Karpati <tomikaa87>
 * @date 2011-09-08
 * @file WeatherXMLDownloader.h
 */

#ifndef XMLDOWNLOADER_H
#define XMLDOWNLOADER_H

typedef enum
{
    XDL_NO_ERROR,
    XDL_SOCKET_INVALID,
    XDL_NO_BUFFER_SPACE,
    XDL_REQUEST_SEND_FAILED,
    XDL_DOWNLOAD_FAILED,
    XDL_TIMEOUT
} xmldl_error_t;

typedef enum
{
    XDL_MODE_WEATHER,
    XDL_MODE_LOCATION
} xmldl_mode_t;

void xmldl_reset();
void xmldl_start(xmldl_mode_t mode);
void xmldl_task();

unsigned char xmldl_is_finished();
xmldl_error_t xmldl_error();

#endif // WEATHERXMLDOWNLOADER_H