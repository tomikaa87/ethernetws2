/**
 * Weather Station Ethernet Board
 * Simple and small XML parser
 * It parses the XML file downloaded from accuweather.com
 *
 * @author: Tamas Karpati <tomikaa87>
 * @date: 2010-09-12
 * @file WeatherXMLParser.h
 */

#ifndef WEATHERXML_H
#define WEATHERXML_H

/*
 * Data structure to store forecast weather data
 */
typedef struct
{
    char DayCode[4];
    char Date[11];
    
    // Daytime forecast
    unsigned char DayTextShort[50];
    //unsigned char DayTextLong[80];
    unsigned char DayIcon;
    signed char DayHiTemp;
    signed char DayLoTemp;
    signed char DayRFHi;
    signed char DayRFLo;
    unsigned char DayWindSpeed;
    unsigned char DayWindGusts;
    unsigned char DayWindDir[4];
    unsigned char DayMaxUV;
    float DayRainAmount;
    float DaySnowAmount;
    float DayPrecipAmount;
    unsigned char DayTStormProb;
    
    // Nighttime forecast
    unsigned char NightTextShort[50];
    //unsigned char NightTextLong[80];
    unsigned char NightIcon;
    signed char NightHiTemp;
    signed char NightLoTemp;
    signed char NightRFHi;
    signed char NightRFLo;
    unsigned char NightWindSpeed;
    unsigned char NightWindGusts;
    unsigned char NightWindDir[4];
    float NightRainAmount;
    float NightSnowAmount;
    float NightPrecipAmount;
    unsigned char NightTStormProb;
}
ForecastWeatherData;    

/*
 * Data structure to store weather data
 */
typedef struct
{
    unsigned int CurrentPressure;
    char CurrentPressureTend[12];
    signed char CurrentTemperature;
    signed char CurrentRealFeel;
    float CurrentHumidity;
    char CurrentText[50];
    unsigned char CurrentIcon;
    unsigned char CurrentWindGusts;
    unsigned char CurrentWindSpeed;
    char CurrentWindDir[4];
    unsigned char CurrentVisibility;
    float CurrentPrecipation;
    unsigned char CurrentUVIndex;
}
WeatherData;

//void initWeatherData(WeatherData *w_data);
void WXP_init();
void WXP_reset();
//void parseLine(char *line, WeatherData *w_data);
unsigned char WXP_parseLine(char *line);
WeatherData *WXP_getWeatherData();
ForecastWeatherData *WXP_getForecastData(unsigned char day);

#endif // WEATHERXML_H
