#include "CRC16.h"

#define CHECKSUM_POLY                   0x1021

static unsigned int _checksum_value;

void checksum_reset()
{
    _checksum_value = 0;
}

unsigned int checksum_value()
{
    return _checksum_value;
}

void checksum_byte(unsigned char ch)
{
    unsigned char i;

    _checksum_value ^= ch;
    i = 8;

    do
    {
        if (_checksum_value & 0x8000)
            _checksum_value <<= 1;
        else
        {
            _checksum_value <<= 1;
            _checksum_value ^= CHECKSUM_POLY;
        }
    }    while (--i);
}
