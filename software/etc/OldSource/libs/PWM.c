/**
 * Ethernet Weather Station v2 Firmware
 * Pulse Width Modulation peripheral library
 *
 * @author Tamas Karpati <tomikaa87>
 * @date 2011-11-08
 * @file PWM.C
 */

#include "PWM.h"

#include <htc.h>

/**
 * Initializes the PIC's PWM module and Timer2 to be used by PWM.
 */
void pwm_init()
{
    TRISC2 = 0;
    PR2 = 0b01111100;
    T2CON = 0b00000111;
    CCPR1L = 0b11111111;
    CCP1CON = 0b00111100;
}

/**
 * Changes the PWM duty cycle to \p duty.
 */
void pwm_set_dc(unsigned int duty)
{
    // Write 8 MSbits of duty to CCPR1L
    CCPR1L = (duty >> 2) & 0xFF;
    // Write 2 LSBits of duty to DC1B0 and DC1B1
    DC1B0 = (duty & 1) ? 1 : 0;
    DC1B1 = (duty & 2) ? 1 : 0;
}
