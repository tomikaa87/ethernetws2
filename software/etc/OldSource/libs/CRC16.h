/**
 * Ethernet Weather Station v2 Firmware
 * CRC16 calculation routines
 *
 * @author Tamas Karpati <tomikaa87>
 * @date 2011-09-08
 * @file crc16.h
 */

#ifndef CRC16_H
#define CRC16_H

void checksum_reset();
unsigned int checksum_value();
void checksum_byte(unsigned char ch);

#endif // CRC16_H
