/**
 * Ethernet Weather Station v2 Firmware
 * Pulse Width Modulation peripheral library
 *
 * @author Tamas Karpati <tomikaa87>
 * @date 2011-11-08
 * @file PWM.h
 */

#ifndef __PWM_H
#define __PWM_H

#define PWM_MAX_DUTY_CYCLE      800

void pwm_init();
void pwm_set_dc(unsigned int duty);

#endif // __PWM_H
