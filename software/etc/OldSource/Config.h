/**
 * Ethernet Weather Station v2 Firmware
 * Configuration storage
 *
 * @author Tamas Karpati <tomikaa87>
 * @date 2011-09-08
 * @file Config.h
 */

#ifndef CONFIG_H
#define CONFIG_H

#include "HardwareProfile.h"
#include "Types.h"

// Check if EEPROM address of configuration data is defined
#ifndef CONFIG_DATA_EEPROM_ADDR
#   define CONFIG_DATA_EEPROM_ADDR     0x00
#   warning "CONFIG_DATA_EEPROM_ADDR undefined. Using default settings EEPROM address (0x00)."
#endif

typedef struct
{
    uint8 flags;                    // Flags for enabling services etc.

    uint32 ip;                       // IP address of the device
    uint32 subnetMask;               // Subnet mask
    uint32 defaultGateway;           // Default gateway
    uint32 dnsIp;                    // Primary DNS IP
    uint32 httpProxyIp;              // HTTP proxy IP address
    uint16 httpProxyPort;             // HTTP proxy port

    uint8 weatherUpdateInterval;    // Time in minutes between two updates
                                            // Set it to 0 to disable auto update
    char weatherLocation[30];                                            
} config_t;

typedef enum
{
    CONFIG_DHCP_ENABLED = 1,
    CONFIG_PROXY_ENABLED = 2,
    CONFIG_NTP_ENABLED = 4
} config_flag_t;

/*** API **********************************************************************/

config_t config;

#define CONFIG_SETF(_flag) config.flags |= _flag
#define CONFIG_CLRF(_flag) config.flags &= ~_flag
#define CONFIG_ISSETF(_flag) config.flags & _flag

uint16 config_calc_checksum();
uint8 config_is_valid();
void config_load_defaults();
void config_load();
void config_save();

void config_load_stack_settings();

#endif // CONFIG_H
