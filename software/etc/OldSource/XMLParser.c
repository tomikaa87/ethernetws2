/**
 * Ethernet Weather Station 2 Firmware
 * Weather data XML parser module
 *
 * @author Tamas Karpati
 * @date 2012-07-07
 */

#include "XMLParser.h"
#include "SRAM_23K.h"
#include "Types.h"

#include <stdio.h>

#define F_TAG_OPEN              1
#define F_QUOTE_OPEN            2

#define RESETFLAGS(x)           (x = 0)
#define SETFLAG(x, flag)        (x |= flag)
#define CLRFLAG(x, flag)        (x &= ~flag)
#define ISSETF(x, flag)         (x & flag ? 1 : 0)

/**
 * @todo optimize RAM operations
 */
void parse_tag(uint16 start, uint16 end)
{
    uint16 i, bufIdx;
    char buf[64];
    uint8 isClosingTag;

    if (start > end)
        return;

    // Print tag
    printf("[xmlp] tag from %u to %u\r\n", start, end);
    printf("[xmlp] parsing tag: ");
    for (i = start; i <= end; i++) {
        putch(ext_sram_read_byte(i));
    }
    printf("\r\n");

    // Trim '<' and '>'
    if (ext_sram_read_byte(start) == '<')
        start++;
    if (ext_sram_read_byte(end) == '>')
        end--;

    // One line tag? Then trim '/' from the end.
    if (ext_sram_read_byte(end) == '/')
        end--;

    // Print trimmed tag
    printf("[xmlp] trimmed tag: ");
    for (i = start; i <= end; i++) {
        putch(ext_sram_read_byte(i));
    }
    printf("\r\n");

    // Trim leading whitespace characters
    while (start <= end) {
        char c = ext_sram_read_byte(start);

        if (c != ' ' && c != '\t')
            break;
        else
            start++;
    }

    // Is this tag a closing one?
    isClosingTag = 0;
    if (ext_sram_read_byte(start) == '/') {
        isClosingTag = 1;
        start++;
    }

    if (isClosingTag)
        printf("[xmlp] closing tag\r\n");

    // Read tag name
    bufIdx = 0;
    while (start <= end && bufIdx < sizeof (buf) - 1) {
        char c = ext_sram_read_byte(start);
        if (c == ' ')
            break;
        buf[bufIdx++] = c;
        start++;
    }

    buf[bufIdx] = '\0';

    printf("[xmlp] tag name: %s\r\n", buf);

    /** @todo process the tag */
    //processTag(buf, isClosingTag);

    // If no attributes found, do things with the tag and return
    if (start > end) {
        printf("[xmlp] end of tag\r\n");
        return;
    }

    // Read all attributes
    while (start <= end) {
        char c = ext_sram_read_byte(start);
        uint8 hasValue;

        // Skip leading spaces
        if ((c == ' ' || c == '\t') && start <= end) {
            start++;
            continue;
        }

        // End of the tag
        if (c == '/' || c == '>') {
            break;
        }

        // Read attribute name
        bufIdx = 0;
        hasValue = 0;
        while (start <= end) {
            c = ext_sram_read_byte(start++);
            
            if (c == ' ' && !hasValue) {
                // Value-less attribute found
                buf[bufIdx] = 0;
                
                printf("VL-ATTR: %s\r\n", buf);

                break;
            } else if (c == '=') {
                // Attribute name
                buf[bufIdx] = 0;
                bufIdx = 0;

                printf("ATTR: %s\r\n", buf);

                hasValue = 1;
                break;
            } else {
                if (bufIdx < sizeof (buf) - 1)
                    buf[bufIdx++] = c;
            }
        }

        // Read attribute value
        if (hasValue) {

            // Skip first quote character
            if (ext_sram_read_byte(start) == '"')
                start++;

            while (start <= end) {
                c = ext_sram_read_byte(start++);

                if (c == '"') {
                    // Attribute value
                    buf[bufIdx] = 0;
                    bufIdx = 0;

                    printf("VALUE: %s\r\n", buf);
                    
                    break;
                } else if (bufIdx < sizeof (buf) - 1)
                    buf[bufIdx++] = c;
            }
        }
    }
}

void xmlp_parse(unsigned int dataLen)
{
    unsigned char flags;
    unsigned int tagStart, tagEnd, i;
    union {
        struct {
            char c;
            char prev;
        };
        unsigned char buf[2];
    } c;

    RESETFLAGS(flags);

    printf("\r\n\r\n[xmlp] parsing weather data\r\n");

    for (i = RAM_LOC_WDATA_XML_BASE; i < dataLen + RAM_LOC_WDATA_XML_BASE; i++) {
        ext_sram_read_array(i, c.buf, sizeof (c.buf));

//        char c = data.at(i);                    // Current character
//        char pc = i > 0 ? data.at(i - 1) : c;   // Previous character

        //qDebug() << "c =" << c << "; pc =" << pc;

        // Find a tag opening
        if (c.c == '<' && !ISSETF(flags, F_TAG_OPEN) && !ISSETF(flags, F_QUOTE_OPEN)) {
            tagStart = i;
            //tagOpen = true;
            SETFLAG(flags, F_TAG_OPEN);

            printf("[xmlp] tag start at %u\r\n", i);
        }

        // If a quote was found
        if (c.c == '"' && ISSETF(flags, F_TAG_OPEN) && !ISSETF(flags, F_QUOTE_OPEN)) {
            //quoteOpen = true;
            SETFLAG(flags, F_QUOTE_OPEN);

            printf("[xmlp] quote start at %u\r\n", i);

            continue;
        }
        if (c.c == '"' && ISSETF(flags, F_QUOTE_OPEN) && c.prev != '\\') {
            //quoteOpen = false;
            CLRFLAG(flags, F_QUOTE_OPEN);

            printf("[xmlp] quote end at %u\r\n", i);
        }

        // Find end of the tag
        if (c.c == '>' && ISSETF(flags, F_TAG_OPEN) && !ISSETF(flags, F_QUOTE_OPEN)) {
            //tagOpen = false;
            CLRFLAG(flags, F_TAG_OPEN);
            tagEnd = i;

            printf("[xmlp] tag end at %u\r\n", i);
            parse_tag(tagStart, tagEnd);
        }
    }
}


///*
// * parser.c
// *
// *  Created on: Nov 4, 2011
// *      Author: tomikaa
// */
//
//#include "XMLParser.h"
//#include "WeatherData.h"
//
//#include <stdio.h>
//#include <stdlib.h>
//#include <string.h>
//
///*** DEBUG MACROS *************************************************************/
//
////#define _debug printf
////#define _debugf printf
//
///*** GLOBAL VARIABLES *********************************************************/
//
//typedef enum {
//    S_INIT = 0,
//    S_WEATHER_DATA,
//    S_LOCATION_DATA,
//    S_W_CURRENT_CONDITIONS,
//    S_W_FORECAST,
//    S_W_FORECAST_DAY,
//    S_W_FORECAST_CONDITIONS
//
//} xml_parser_state_t;
//
//static xml_parser_state_t state;
//
///*** Temporary variables ******************************************************/
//
//unsigned char fcast_daytime;
//char buf[128];
//WDATA_Context wctx;
//
///*** Utility functions ********************************************************/
//
//void _clear_buf()
//{
//    memset(buf, 0, sizeof (buf));
//}
//
//unsigned char _parse_value(char *line, char *value)
//{
//    unsigned char begin_pos, length;
//    unsigned char len;
//    char *str;
//
//    printf("_parse_value(%s, %s)\r\n", line, value);
//
//    len = strlen(line);
//    str = line;
//
//    // Find end of opening tag
//    begin_pos = 0;
//    while (*str && *str != '>') {
//        begin_pos++;
//        str++;
//    }
//    begin_pos++;
//    str++;
//    if (begin_pos >= len) {
//        //_debug("opening tag end not found\r\n");
//        return 0;
//    }
//
//    // Find begin of closing tag
//    length = 0;
//    while (*str && *str != '<') {
//        length++;
//        str++;
//    }
//    if (length >= len - begin_pos) {
//        //_debug("closing tag begin not found\r\n");
//        return 0;
//    }
//
//    //_debugf("value found at %d, length: %d", begin_pos, length);
//
//    strncpy(value, &line[begin_pos], length);
//
//    //_debugf("value: %s", buf);
//
//    return length;
//}
//
//unsigned char _parse_attribute(char *line, const char *attribute, char *value)
//{
//    char *str;
//    unsigned char len;
//    unsigned char begin_pos, length;
//
//    str = strstr(line, attribute);
//    len = strlen(line);
//
//    if (!str) {
//        //_debugf("attribute not found: \"%s\"\r\n", attribute);
//        return 0;
//    }
//
//    //_debugf("attribute position: %d\r\n", len - strlen(str));
//
//    // Find the begining of the value
//    begin_pos = len - strlen(str);
//    while (*str && *str != '"') {
//        begin_pos++;
//        str++;
//    }
//    begin_pos++;
//    str++;
//
//    // Find the end of the value
//    length = 0;
//    while (*str && *str != '"') {
//        length++;
//        str++;
//    }
//
//    //_debugf("value found at %d, length: %d\r\n", begin_pos, length);
//
//    strncpy(value, &line[begin_pos], length);
//
//    //_debugf("value: %s\r\n", value);
//
//    return length;
//}
//
///*** API implementation *******************************************************/
//
//void XMLP_init()
//{
//    //_debug("initializing\r\n");
//
//    //_debug("resetting parser state\r\n");
//    state = S_INIT;
//
//    //_debug("clearing buffers\r\n");
//    _clear_buf();
//
//    //_debug("init finished\r\n");
//
//    //_debug("WDATA_C_BASE: %u\r\n", RAM_LOC_WDATA_BASE);
//    //_debug("WDATA_C_SIZE: %u\r\n", WDATA_C_SIZE);
//    //_debug("WDATA_F_BASE: %u\r\n", WDATA_F_BASE_ADDR);
//    //_debug("WDATA_F_SIZE: %u\r\n", WDATA_F_SIZE);
//}
//
//void XMLP_parseLine(char *line)
//{
//    switch (state) {
//            // Initial state
//        case S_INIT:
//            // Begining of ADC Database
//            if (strstr(line, "<adc_")) {
//                //_debug("accuweather database found\r\n");
//                state = S_WEATHER_DATA;
//            }
//            break;
//
//        case S_WEATHER_DATA:
//            // Begining of current weather data
//            if (strstr(line, "<curr")) {
//                //_debug("currentconditions tag found\r\n");
//                state = S_W_CURRENT_CONDITIONS;
//                wctx.type = WDATA_CURRENT;
//            }
//            // Begining of forecast data
//            else if (strstr(line, "<fore")) {
//                //_debug("forecast tag found\r\n");
//                state = S_W_FORECAST;
//                wctx.type = WDATA_FORECAST;
//            }
//            // Location data: city list
//            else if (strstr(line, "<cityl")) {
//                //_debug("location data found\r\n");
//                state = S_LOCATION_DATA;
//            }
//            // End of ADC Database
//            else if (strstr(line, "</adc_")) {
//                //_debug("end of accuweather database\r\n");
//                state = S_INIT;
//            }
//            break;
//
//        case S_LOCATION_DATA:
//            // Begining of location data
//            if (strstr(line, "<loc")) {
//                if (_parse_attribute(line, "city", buf)) {
//                    //_debug("city: %s  ", buf);
//                    memset(buf, 0, sizeof (buf));
//                    if (_parse_attribute(line, "location=", buf)) {
//                        //_debug("loc: %s\r\n", buf);
//                    }
//                }
//            }
//            // End of city list
//            else if (strstr(line, "</cityl")) {
//                //_debug("end of city list\r\n");
//                state = S_INIT;
//            }
//            break;
//
//        case S_W_CURRENT_CONDITIONS:
//            _clear_buf();
//            //_debugf("parsing line: \"%s\"\r\n", line);
//            // Pressure
//            if (strstr(line, "<pres")) {
//                // Pressure value
//                if (_parse_value(line, buf)) {
//                    wctx.wu.c_pressure = (uint16)(atof(buf)) * 100;
//                    wctx.wdata = WDATA_C_PRESSURE;
//                    wctx.size = WDATA_C_PRESSURE_SIZE;
//                    WDATA_store(&wctx);
//                }
//                // Pressure tendency
//                if (_parse_attribute(line, "state", buf)) {
//                    wctx.wu.c_pressureTend = -2;
//                    if (strstr(buf, "Inc"))
//                        wctx.wu.c_pressureTend = 1;
//                    else if (strstr(buf, "Ste"))
//                        wctx.wu.c_pressureTend = 0;
//                    else if (strstr(buf, "Dec"))
//                        wctx.wu.c_pressureTend = -1;
//                    wctx.wdata = WDATA_C_PRESSURE_TEND;
//                    wctx.size = WDATA_C_PRESSURE_TEND_SIZE;
//                    WDATA_store(&wctx);
//                }
//            }
//            // Temperature
//            else if (strstr(line, "<te")) {
//                if (_parse_value(line, buf)) {
//                    wctx.wu.c_temperature = atoi(buf);
//                    wctx.wdata = WDATA_C_TEMPERATURE;
//                    wctx.size = WDATA_C_TEMPERATURE_SIZE;
//                    WDATA_store(&wctx);
//                }
//            }
//            // RealFeel temperature
//            else if (strstr(line, "<re")) {
//                if (_parse_value(line, buf)) {
//                    wctx.wu.c_realFeel = atoi(buf);
//                    wctx.wdata = WDATA_C_REALFEEL;
//                    wctx.size = WDATA_C_REALFEEL_SIZE;
//                    WDATA_store(&wctx);
//                }
//            }
//            // Humidity
//            else if (strstr(line, "<hu")) {
//                if (_parse_value(line, buf)) {
//                    buf[strlen(buf) - 1] = 0; // Remove '%' from the end
//                    wctx.wu.c_humidity = atoi(buf);
//                    wctx.wdata = WDATA_C_HUMIDITY;
//                    wctx.size = WDATA_C_HUMIDITY_SIZE;
//                    WDATA_store(&wctx);
//                }
//            }
//            // Weather text
//            else if (strstr(line, "<weathert")) {
//                if (_parse_value(line, buf)) {
//                    //_debugf("weather text: %s\r\n", buf);
//                    wctx.io = WDATA_BUF;
//                    wctx.wdata = WDATA_C_TEXT;
//                    wctx.size = strlen(buf);
//                    wctx.buf = buf;
//                    WDATA_store(&wctx);
//                }
//            }
//            // Weather icon
//            else if (strstr(line, "<weatheri")) {
//                if (_parse_value(line, buf)) {
//                    wctx.wu.c_icon = atoi(buf);
//                    wctx.wdata = WDATA_C_ICON;
//                    wctx.size = WDATA_C_ICON_SIZE;
//                    WDATA_store(&wctx);
//                }
//            }
//            // Wind gusts
//            else if (strstr(line, "<windg")) {
//                if (_parse_value(line, buf)) {
//                    wctx.wu.c_windGusts = atoi(buf);
//                    wctx.wdata = WDATA_C_WINDGUSTS;
//                    wctx.size = WDATA_C_WINDGUSTS_SIZE;
//                    WDATA_store(&wctx);
//                }
//            }
//            // Wind speed
//            else if (strstr(line, "<winds")) {
//                if (_parse_value(line, buf)) {
//                    wctx.wu.c_windSpeed = atoi(buf);
//                    wctx.wdata = WDATA_C_WINDSPEED;
//                    wctx.size = WDATA_C_WINDSPEED_SIZE;
//                    WDATA_store(&wctx);
//                }
//            }
//            // Wind direction
//            else if (strstr(line, "<windd")) {
//                if (_parse_value(line, buf)) {
//                    wctx.io = WDATA_BUF;
//                    wctx.wdata = WDATA_C_WINDDIR;
//                    wctx.size = strlen(buf);
//                    wctx.buf = buf;
//                    WDATA_store(&wctx);
//                }
//            }
//            // Visibility
//            else if (strstr(line, "<vi")) {
//                if (_parse_value(line, buf)) {
//                    wctx.wu.c_visibility = atoi(buf);
//                    wctx.wdata = WDATA_C_VISIBILITY;
//                    wctx.size = WDATA_C_VISIBILITY_SIZE;
//                    WDATA_store(&wctx);
//                }
//            }
//            // Precipitation
//            else if (strstr(line, "<prec")) {
//                if (_parse_value(line, buf)) {
//                    wctx.wu.c_precip = (uint16)(atof(buf) * 100);
//                    wctx.wdata = WDATA_C_PRECIP;
//                    wctx.size = WDATA_C_PRECIP_SIZE;
//                    WDATA_store(&wctx);
//                }
//            }
//            // UV index
//            else if (strstr(line, "<uv")) {
//                if (_parse_attribute(line, "index", buf)) {
//                    wctx.wu.c_uv = atoi(buf);
//                    wctx.wdata = WDATA_C_UV;
//                    wctx.size = WDATA_C_UV_SIZE;
//                    WDATA_store(&wctx);
//                }
//            }
//            // End of current conditions
//            else if (strstr(line, "</curr"))
//                state = S_WEATHER_DATA;
//            break;
//
//        case S_W_FORECAST:
//            //_debugf("parsing line: \"%s\"\r\n", line);
//            // Begining of a forecast day
//            if (strstr(line, "<day")) {
//                if (_parse_attribute(line, "number", buf)) {
//                    //_debugf("## forecast day: %s\r\n", buf);
//                    // Set weather data forecast day;
//                    wctx.fcDay = atoi(buf);
//                    state = S_W_FORECAST_DAY;
//                }
//            }
//            // End of forecast data
//            else if (strstr(line, "</fore")) {
//                //_debug("$$ end of forecast\r\n");
//                state = S_WEATHER_DATA;
//            }
//            break;
//
//        case S_W_FORECAST_DAY:
//            //_debugf("parsing line: \"%s\"\r\n", line);
//            // Begining of forecast day time
//            if (strstr(line, "<dayt")) {
//                //_debug("** forecast day time\r\n");
//                fcast_daytime = 1;
//                state = S_W_FORECAST_CONDITIONS;
//            }
//            // Begining of forecast night time
//            else if (strstr(line, "<nigh")) {
//                //_debug("** forecast night time\r\n");
//                fcast_daytime = 0;
//                state = S_W_FORECAST_CONDITIONS;
//            }
//            // Forecast day date
//            else if (strstr(line, "<obs")) {
//                if (_parse_value(line, buf)) {
//                    //_debugf("forecast date: %s\r\n", buf);
//                }
//            }
//            // End of forecast day
//            else if (strstr(line, "</day>")) {
//                //_debug("## end of forecast day\r\n");
//                state = S_W_FORECAST;
//            }
//            break;
//
//        case S_W_FORECAST_CONDITIONS:
//            //_debugf("parsing line: \"%s\"\r\n", line);
//            if (fcast_daytime) {
//                // End of forecast day time
//                if (strstr(line, "</dayt")) {
//                    //_debug("** end of foreacst day time\r\n");
//                    state = S_W_FORECAST_DAY;
//                }
//            } else {
//                // End of forecast night time
//                if (strstr(line, "</nigh")) {
//                    //_debug("** end of forecast night time\r\n");
//                    state = S_W_FORECAST_DAY;
//                }
//            }
//
//            _clear_buf();
//
//            // Short text
//            if (strstr(line, "<txts")) {
//                if (_parse_value(line, buf)) {
//                    //_debugf("short forecast: %s\r\n", buf);
//                    wctx.buf = buf;
//                    wctx.wdata = fcast_daytime ? WDATA_F_D_TEXT : WDATA_F_N_TEXT;
//                    wctx.size = strlen(buf);
//                    wctx.io = WDATA_BUF;
//                    WDATA_store(&wctx);
//                }
//            }
//            // Weather icon
//            else if (strstr(line, "<we")) {
//                if (_parse_value(line, buf)) {
//                    //_debugf("icon: %s\r\n", buf);
//                    wctx.wu.f_icon = atoi(buf);
//                    wctx.wdata = fcast_daytime ? WDATA_F_D_ICON : WDATA_F_N_ICON;
//                    wctx.size = WDATA_F_D_ICON_SIZE;
//                    WDATA_store(&wctx);
//                }
//            }
//            // High temperature
//            else if (strstr(line, "<hi")) {
//                if (_parse_value(line, buf)) {
//                    //_debugf("high: %s F\r\n", buf);
//                    wctx.wu.f_hiTemp = atoi(buf);
//                    wctx.wdata = fcast_daytime ? WDATA_F_D_HI_TEMP :
//                        WDATA_F_N_HI_TEMP;
//                    wctx.size = WDATA_F_D_HI_TEMP_SIZE;
//                    WDATA_store(&wctx);
//                }
//            }
//            // Low temperature
//            else if (strstr(line, "<lo")) {
//                if (_parse_value(line, buf)) {
//                    //_debugf("low: %s F\r\n", buf);
//                    wctx.wu.f_loTemp = atoi(buf);
//                    wctx.wdata = fcast_daytime ? WDATA_F_D_LO_TEMP :
//                        WDATA_F_N_LO_TEMP;
//                    wctx.size = WDATA_F_D_LO_TEMP_SIZE;
//                    WDATA_store(&wctx);
//                }
//            }
//            // RealFeel high
//            else if (strstr(line, "<realfeelh")) {
//                if (_parse_value(line, buf)) {
//                    //_debugf("RF hi: %s F\r\n", buf);
//                    wctx.wu.f_realFeelHi = atoi(buf);
//                    wctx.wdata = fcast_daytime ? WDATA_F_D_REALFEEL_HI :
//                        WDATA_F_N_REALFEEL_HI;
//                    wctx.size = WDATA_F_D_REALFEEL_HI_SIZE;
//                    WDATA_store(&wctx);
//                }
//            }
//            // RealFeel low
//            else if (strstr(line, "<realfeell")) {
//                if (_parse_value(line, buf)) {
//                    //_debugf("RF lo: %s F\r\n", buf);
//                    wctx.wu.f_realFeelLo = atoi(buf);
//                    wctx.wdata = fcast_daytime ? WDATA_F_D_REALFEEL_LO :
//                        WDATA_F_N_REALFEEL_LO;
//                    wctx.size = WDATA_F_D_REALFEEL_LO_SIZE;
//                    WDATA_store(&wctx);
//                }
//            }
//            // Wind speed
//            else if (strstr(line, "<winds")) {
//                if (_parse_value(line, buf)) {
//                    //_debugf("wind speed: %s\r\n", buf);
//                    wctx.wu.f_windSpeed = atoi(buf);
//                    wctx.wdata = fcast_daytime ? WDATA_F_D_WINDSPEED :
//                        WDATA_F_N_WINDSPEED;
//                    wctx.size = WDATA_F_D_WINDSPEED_SIZE;
//                    WDATA_store(&wctx);
//                }
//            }
//            // Wind gust
//            else if (strstr(line, "<windg")) {
//                if (_parse_value(line, buf)) {
//                    //_debugf("wind gust: %s\r\n", buf);
//                    wctx.wu.f_windGusts = atoi(buf);
//                    wctx.wdata = fcast_daytime ? WDATA_F_D_WINDGUSTS :
//                        WDATA_F_N_WINDGUSTS;
//                    wctx.size = WDATA_F_D_WINDGUSTS_SIZE;
//                    WDATA_store(&wctx);
//                }
//            }
//            // Wind direction
//            else if (strstr(line, "<windd")) {
//                if (_parse_value(line, buf)) {
//                    //_debugf("wind dir: %s\r\n", buf);
//                    wctx.buf = buf;
//                    wctx.wdata = fcast_daytime ? WDATA_F_D_WINDDIR :
//                        WDATA_F_N_WINDGUSTS;
//                    wctx.size = WDATA_F_D_WINDDIR_SIZE;
//                    WDATA_store(&wctx);
//                }
//            }
//            // Max UV
//            else if (strstr(line, "<maxu")) {
//                if (_parse_value(line, buf)) {
//                    //_debugf("max uv: %s\r\n", buf);
//                    wctx.wu.f_maxUv = atoi(buf);
//                    wctx.wdata = WDATA_F_D_MAXUV;
//                    wctx.size = WDATA_F_D_MAXUV_SIZE;
//                    WDATA_store(&wctx);
//                }
//            }
//            // Rain amount
//            else if (strstr(line, "<rai")) {
//                if (_parse_value(line, buf)) {
//                    //_debugf("rain: %s\r\n", buf);
//                    wctx.wu.f_rain = (uint16)(atof(buf) * 100);
//                    wctx.wdata = fcast_daytime ? WDATA_F_D_RAIN :
//                        WDATA_F_N_RAIN;
//                    wctx.size = WDATA_F_D_RAIN_SIZE;
//                    WDATA_store(&wctx);
//                }
//            }
//            // Snow amount
//            else if (strstr(line, "<sno")) {
//                if (_parse_value(line, buf)) {
//                    //_debugf("snow: %s\r\n", buf);
//                    wctx.wu.f_snow = (uint16)(atof(buf) * 100);
//                    wctx.wdata = fcast_daytime ? WDATA_F_D_SNOW :
//                        WDATA_F_N_SNOW;
//                    wctx.size = WDATA_F_D_SNOW_SIZE;
//                    WDATA_store(&wctx);
//                }
//            }
//            // Precipitation amount
//            else if (strstr(line, "<pre")) {
//                if (_parse_value(line, buf)) {
//                    //_debugf("precip: %s\r\n", buf);
//                    wctx.wu.f_precip = (uint16)(atof(buf) * 100);
//                    wctx.wdata = fcast_daytime ? WDATA_F_D_PRECIP :
//                        WDATA_F_N_PRECIP;
//                    wctx.size = WDATA_F_D_MAXUV_SIZE;
//                    WDATA_store(&wctx);
//                }
//            }
//            // Thunderstorm probability
//            else if (strstr(line, "<tst")) {
//                if (_parse_value(line, buf)) {
//                    //_debugf("storm: %s\r\n", buf);
//                    wctx.wu.f_tstorm = atoi(buf);
//                    wctx.wdata = fcast_daytime ? WDATA_F_D_TSTORM :
//                        WDATA_F_N_TSTORM;
//                    wctx.size = WDATA_F_D_TSTORM_SIZE;
//                    WDATA_store(&wctx);
//                }
//            }
//            break;
//    }
//}
