/**
 * Ethernet Weather Station v2 Firmware
 * Weather data storage module
 *
 * @author Tamas Karpati <tomikaa87>
 * @date 2012-06-15
 * @file WeatherData.h
 */

#ifndef WEATHERDATA_H
#define	WEATHERDATA_H

#include "HardwareProfile.h"
#include "Types.h"

// Configuration check
#ifndef RAM_LOC_WDATA_BASE
    #error Weather data RAM base address (RAM_LOC_WDATA_BASE) undefined
#endif

// Current pressure (in * 100)
#define WDATA_C_PRESSURE_SIZE (sizeof (uint16))
#define WDATA_C_PRESSURE (RAM_LOC_WDATA_BASE)

// Current pressure tendency
#define WDATA_C_PRESSURE_TEND_SIZE (sizeof (int8))
#define WDATA_C_PRESSURE_TEND (WDATA_C_PRESSURE + WDATA_C_PRESSURE_SIZE)

// Current temperature (F)
#define WDATA_C_TEMPERATURE_SIZE (sizeof (int8))
#define WDATA_C_TEMPERATURE (WDATA_C_PRESSURE_TEND + WDATA_C_PRESSURE_TEND_SIZE)

// Current RealFeel temperature (F)
#define WDATA_C_REALFEEL_SIZE (sizeof (int8))
#define WDATA_C_REALFEEL (WDATA_C_TEMPERATURE + WDATA_C_TEMPERATURE_SIZE)

// Current humidity (%)
#define WDATA_C_HUMIDITY_SIZE (sizeof (uint8))
#define WDATA_C_HUMIDITY (WDATA_C_REALFEEL + WDATA_C_REALFEEL_SIZE)

// Current weather icon
#define WDATA_C_ICON_SIZE (sizeof (uint8))
#define WDATA_C_ICON (WDATA_C_HUMIDITY + WDATA_C_HUMIDITY_SIZE)

// Current wind gusts speed (mph)
#define WDATA_C_WINDGUSTS_SIZE (sizeof (uint8))
#define WDATA_C_WINDGUSTS (WDATA_C_ICON + WDATA_C_ICON_SIZE)

// Current wind speed (mph)
#define WDATA_C_WINDSPEED_SIZE (sizeof (uint8))
#define WDATA_C_WINDSPEED (WDATA_C_WINDGUSTS + WDATA_C_WINDGUSTS_SIZE)

// Current wind direction (string)
#define WDATA_C_WINDDIR_SIZE (sizeof (char) * 4)
#define WDATA_C_WINDDIR (WDATA_C_WINDSPEED + WDATA_C_WINDSPEED_SIZE)

// Current visibility (miles)
#define WDATA_C_VISIBILITY_SIZE (sizeof (uint8))
#define WDATA_C_VISIBILITY (WDATA_C_WINDDIR + WDATA_C_WINDDIR_SIZE)

// Current precipitation (in * 100)
#define WDATA_C_PRECIP_SIZE (sizeof (uint16))
#define WDATA_C_PRECIP (WDATA_C_VISIBILITY + WDATA_C_VISIBILITY_SIZE)

// Current weather text (string)
#define WDATA_C_TEXT_SIZE (sizeof (char) * 50)
#define WDATA_C_TEXT (WDATA_C_PRECIP + WDATA_C_PRECIP_SIZE)

// Current weather UV index
#define WDATA_C_UV_SIZE (sizeof (uint8))
#define WDATA_C_UV (WDATA_C_TEXT + WDATA_C_TEXT_SIZE)

// Current weather data RAM space
#define WDATA_C_SIZE (WDATA_C_PRESSURE_SIZE + WDATA_C_TEMPERATURE_SIZE + \
    WDATA_C_REALFEEL_SIZE + WDATA_C_HUMIDITY_SIZE + WDATA_C_ICON_SIZE + \
    WDATA_C_WINDGUSTS_SIZE + WDATA_C_WINDSPEED_SIZE + WDATA_C_VISIBILITY_SIZE + \
    WDATA_C_PRECIP_SIZE + WDATA_C_TEXT_SIZE + WDATA_C_PRESSURE_TEND_SIZE + \
    WDATA_C_UV_SIZE)

// Forecast data base address
#define WDATA_F_BASE_ADDR (RAM_LOC_WDATA_BASE + WDATA_C_SIZE)

// Forecast addresses are relative!
// Forecast daytime short text
#define WDATA_F_D_TEXT_SIZE (sizeof (char) * 50)
#define WDATA_F_D_TEXT 0

// Forecast daytime icon
#define WDATA_F_D_ICON_SIZE (sizeof (uint8))
#define WDATA_F_D_ICON (WDATA_F_D_TEXT + WDATA_F_D_TEXT_SIZE)

// Forecast daytime high temperature
#define WDATA_F_D_HI_TEMP_SIZE (sizeof (int8))
#define WDATA_F_D_HI_TEMP (WDATA_F_D_ICON + WDATA_F_D_ICON_SIZE)

// Forecast daytime low temperature
#define WDATA_F_D_LO_TEMP_SIZE (sizeof (int8))
#define WDATA_F_D_LO_TEMP (WDATA_F_D_HI_TEMP + WDATA_F_D_HI_TEMP_SIZE)

// Forecast daytime RealFeel high
#define WDATA_F_D_REALFEEL_HI_SIZE (sizeof (int8))
#define WDATA_F_D_REALFEEL_HI (WDATA_F_D_LO_TEMP + WDATA_F_D_LO_TEMP_SIZE)

// Forecast daytime RealFeel low
#define WDATA_F_D_REALFEEL_LO_SIZE (sizeof (int8))
#define WDATA_F_D_REALFEEL_LO (WDATA_F_D_REALFEEL_HI + WDATA_F_D_REALFEEL_HI_SIZE)

// Forecast daytime wind speed
#define WDATA_F_D_WINDSPEED_SIZE (sizeof (uint8))
#define WDATA_F_D_WINDSPEED (WDATA_F_D_REALFEEL_LO + WDATA_F_D_REALFEEL_LO_SIZE)

// Forecast daytime wind direction
#define WDATA_F_D_WINDDIR_SIZE (sizeof (char) * 4)
#define WDATA_F_D_WINDDIR (WDATA_F_D_WINDSPEED + WDATA_F_D_WINDSPEED_SIZE)

// Forecast daytime wind gusts
#define WDATA_F_D_WINDGUSTS_SIZE (sizeof (uint8))
#define WDATA_F_D_WINDGUSTS (WDATA_F_D_WINDDIR + WDATA_F_D_WINDDIR_SIZE)

// Forecast daytime max UV
#define WDATA_F_D_MAXUV_SIZE (sizeof (uint8))
#define WDATA_F_D_MAXUV (WDATA_F_D_WINDGUSTS + WDATA_F_D_WINDGUSTS_SIZE)

// Forecast daytime rain amount
#define WDATA_F_D_RAIN_SIZE (sizeof (uint16))
#define WDATA_F_D_RAIN (WDATA_F_D_MAXUV + WDATA_F_D_MAXUV_SIZE)

// Forecast daytime snow amount
#define WDATA_F_D_SNOW_SIZE (sizeof (uint16))
#define WDATA_F_D_SNOW (WDATA_F_D_RAIN + WDATA_F_D_RAIN_SIZE)

// Forecast daytime precipitation amount
#define WDATA_F_D_PRECIP_SIZE (sizeof (uint16))
#define WDATA_F_D_PRECIP (WDATA_F_D_SNOW + WDATA_F_D_SNOW_SIZE)

// Forecast daytime thunderstorm probability
#define WDATA_F_D_TSTORM_SIZE (sizeof (uint8))
#define WDATA_F_D_TSTORM (WDATA_F_D_PRECIP + WDATA_F_D_PRECIP_SIZE)

// Forecast nighttime short text
#define WDATA_F_N_TEXT_SIZE (sizeof (char) * 50)
#define WDATA_F_N_TEXT (WDATA_F_D_TSTORM + WDATA_F_D_TSTORM_SIZE)

// Forecast nighttime icon
#define WDATA_F_N_ICON_SIZE (sizeof (uint8))
#define WDATA_F_N_ICON (WDATA_F_N_TEXT + WDATA_F_N_TEXT_SIZE)

// Forecast nighttime high temperature
#define WDATA_F_N_HI_TEMP_SIZE (sizeof (int8))
#define WDATA_F_N_HI_TEMP (WDATA_F_N_ICON + WDATA_F_N_ICON_SIZE)

// Forecast nighttime low temperature
#define WDATA_F_N_LO_TEMP_SIZE (sizeof (int8))
#define WDATA_F_N_LO_TEMP (WDATA_F_N_HI_TEMP + WDATA_F_N_HI_TEMP_SIZE)

// Forecast nighttime RealFeel high
#define WDATA_F_N_REALFEEL_HI_SIZE (sizeof (int8))
#define WDATA_F_N_REALFEEL_HI (WDATA_F_N_LO_TEMP + WDATA_F_N_LO_TEMP_SIZE)

// Forecast nighttime RealFeel low
#define WDATA_F_N_REALFEEL_LO_SIZE (sizeof (int8))
#define WDATA_F_N_REALFEEL_LO (WDATA_F_N_REALFEEL_HI + WDATA_F_N_REALFEEL_HI_SIZE)

// Forecast nighttime wind speed
#define WDATA_F_N_WINDSPEED_SIZE (sizeof (uint8))
#define WDATA_F_N_WINDSPEED (WDATA_F_N_REALFEEL_LO + WDATA_F_N_REALFEEL_LO_SIZE)

// Forecast nighttime wind direction
#define WDATA_F_N_WINDDIR_SIZE (sizeof (char) * 4)
#define WDATA_F_N_WINDDIR (WDATA_F_N_WINDSPEED + WDATA_F_N_WINDSPEED_SIZE)

// Forecast nighttime wind gusts
#define WDATA_F_N_WINDGUSTS_SIZE (sizeof (uint8))
#define WDATA_F_N_WINDGUSTS (WDATA_F_N_WINDDIR + WDATA_F_N_WINDDIR_SIZE)

// Forecast nighttime rain amount
#define WDATA_F_N_RAIN_SIZE (sizeof (uint16))
#define WDATA_F_N_RAIN (WDATA_F_N_WINDGUSTS + WDATA_F_N_WINDGUSTS_SIZE)

// Forecast nighttime snow amount
#define WDATA_F_N_SNOW_SIZE (sizeof (uint16))
#define WDATA_F_N_SNOW (WDATA_F_N_RAIN + WDATA_F_N_RAIN_SIZE)

// Forecast nighttime precipitation amount
#define WDATA_F_N_PRECIP_SIZE (sizeof (uint16))
#define WDATA_F_N_PRECIP (WDATA_F_N_SNOW + WDATA_F_N_SNOW_SIZE)

// Forecast nighttime thunderstorm probability
#define WDATA_F_N_TSTORM_SIZE (sizeof (uint8))
#define WDATA_F_N_TSTORM (WDATA_F_N_PRECIP + WDATA_F_N_PRECIP_SIZE)

// One forecast day size in the RAM
#define WDATA_F_SIZE (WDATA_F_D_TEXT_SIZE + WDATA_F_D_ICON_SIZE + \
    WDATA_F_D_HI_TEMP_SIZE + WDATA_F_D_LO_TEMP_SIZE + WDATA_F_D_REALFEEL_HI_SIZE + \
    WDATA_F_D_REALFEEL_LO_SIZE + WDATA_F_D_WINDSPEED_SIZE + WDATA_F_D_WINDDIR_SIZE + \
    WDATA_F_D_WINDGUSTS_SIZE + WDATA_F_D_MAXUV_SIZE + WDATA_F_D_RAIN_SIZE + \
    WDATA_F_D_SNOW_SIZE + WDATA_F_D_PRECIP_SIZE + WDATA_F_D_TSTORM_SIZE + \
    WDATA_F_N_TEXT_SIZE + WDATA_F_N_ICON_SIZE + WDATA_F_N_HI_TEMP_SIZE + \
    WDATA_F_N_LO_TEMP_SIZE + WDATA_F_N_REALFEEL_HI_SIZE + WDATA_F_N_REALFEEL_LO_SIZE + \
    WDATA_F_N_WINDSPEED_SIZE + WDATA_F_N_WINDDIR_SIZE + WDATA_F_N_WINDGUSTS_SIZE + \
    WDATA_F_N_RAIN_SIZE + WDATA_F_N_SNOW_SIZE + WDATA_F_N_PRECIP_SIZE + \
    WDATA_F_N_TSTORM_SIZE)

// Union data type for convenience
typedef union {
    uint8 data[2];

    // Current weather
    uint16 c_pressure;
    int8 c_pressureTend;
    int8 c_temperature;
    int8 c_realFeel;
    uint8 c_humidity;
    uint8 c_icon;
    uint8 c_windGusts;
    uint8 c_windSpeed;
    uint8 c_visibility;
    uint16 c_precip;
    uint8 c_uv;

    // Forecast weather (day any night)
    uint8 f_icon;
    int8 f_hiTemp;
    int8 f_loTemp;
    int8 f_realFeelLo;
    int8 f_realFeelHi;
    uint8 f_windSpeed;
    uint8 f_windGusts;
    uint8 f_maxUv;
    uint16 f_rain;
    uint16 f_snow;
    uint16 f_precip;
    uint16 f_tstorm;
} wdata_t;

typedef enum {
    WDATA_NUM,      // Default IO Type
    WDATA_BUF
} wdata_io_type_t;

typedef enum {
    WDATA_CURRENT,
    WDATA_FORECAST
} wdata_type_t;

typedef struct {
    wdata_t wu;
    wdata_io_type_t io;
    wdata_type_t type;
    uint8 fcDay;
    uint16 wdata;
    uint8 size;
    char *buf;
} wdata_ctx_t;

//void WDATA_getNumericData(uint16 address, uint8 size, uint8 fcastDay,
//        WDATA_Union *wu);
//void WDATA_getStringData(uint16 address, uint8 size, uint8 fcastDay, char *buf);
//void WDATA_setNumericData(uint16 address, uint8 size, uint8 fcastDay,
//        WDATA_Union *wu);
//void WDATA_setStringData(uint16 address, uint8 size, uint8 fcastDay, char *buf);

void wdata_write(wdata_ctx_t *ctx);
void wdata_read(wdata_ctx_t *ctx);

#endif	/* WEATHERDATA_H */

