/**
 * Ethernet Weather Station v2 Firmware
 * Simple Network Time Protocol routines
 *
 * @author Tamas Karpati <tomikaa87>
 * @date 2011-10-07
 * @file SNTP.h
 */

#ifndef NTP_H
#define NTP_H

void ntp_task();

#endif // NTP_H