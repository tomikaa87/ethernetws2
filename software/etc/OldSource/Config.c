/**
 * Ethernet Weather Station v2 Firmware
 * Configuration storage
 *
 * @author Tamas Karpati <tomikaa87>
 * @date 2011-09-08
 * @file Config.c
 */

#include "Config.h"
#include "CRC16.h"
#include "HardwareProfile.h"
#include "Debug.h"
#include "TCPIPStack/TCPIP.h"

#include <htc.h>

/**
 * Writes the given @p data byte to the EEPROM at the given @p addr address.
 */
void eeprom_write_byte(uint16 addr, uint8 data)
{
    EEADRH = (addr >> 8) & 0x03;
    EEADR = (addr & 0x0FF);
    EEDATA = data;
    EEPGD = 0;
    CFGS = 0;
    WREN = 1;
    GIE = 0;
    EECON2 = 0x55;
    EECON2 = 0x0AA;
    EECON1bits.WR = 1;
    while (EECON1bits.WR);
    GIE = 1;
}

/**
 * Reads a byte from the EEPROM located at the given @p addr address.
 */
uint8 eeprom_read_byte(uint16 addr)
{
    EEADRH = (addr >> 8) & 0x03;
    EEADR = (addr & 0x0FF);
    CFGS = 0;
    EEPGD = 0;
    EECON1bits.RD = 1;

    return EEDATA;
}

/**
 * Calculates the CRC16 sum of the current configuration data stored in the RAM.
 */
uint16 config_calc_checksum()
{
    uint8 *configData;
    uint16 i;

    configData = (uint8 *) &config;

    // Calculate CRC16 value for configuration data
    checksum_reset();
    for (i = 0; i < sizeof (config_t); i++)
        checksum_byte(configData[i]);

    return checksum_value();
}

/**
 * Verifies the stored configuration data by checking the stored CRC16 sum.
 * @return 1 if the configuration is correct, 0 otherwise
 */
uint8 config_is_valid()
{
    uint16 storedCrc, i;

    // Read stored CRC16
    storedCrc = (eeprom_read_byte(CONFIG_DATA_EEPROM_ADDR +
            sizeof (config_t)) << 8);
    storedCrc += eeprom_read_byte(CONFIG_DATA_EEPROM_ADDR +
            sizeof (config_t) + 1);

    // Calculate the CRC16 sum of the configuration stored in the EEPROM
    checksum_reset();
    for (i = CONFIG_DATA_EEPROM_ADDR;
            i < CONFIG_DATA_EEPROM_ADDR + sizeof (config_t); i++)
    {
        checksum_byte(eeprom_read_byte(i));
    }

    _debug("Original CRC: %04X, calculated CRC: %04X\r\n", storedCrc, checksum_value());

    return (storedCrc == checksum_value());
}

/**
 * Loads the default configuration values.
 */
void config_load_defaults()
{
    config.flags = CONFIG_DHCP_ENABLED;

    config.ip = 0ul;
    config.subnetMask = 0ul;
    config.defaultGateway = 0ul;
    config.dnsIp = 0ul;
    config.httpProxyIp = 0ul;
    config.httpProxyPort = 0ul;

    config.weatherUpdateInterval = 0;
}

/**
 * Loads stored configuration from the EEPROM.
 */
void config_load()
{
    uint8 *configData;
    uint16 i, dataIndex;

    configData = (uint8 *) &config;
    dataIndex = 0;

    for (i = CONFIG_DATA_EEPROM_ADDR;
            i < CONFIG_DATA_EEPROM_ADDR + sizeof (config_t); i++)
    {
        configData[dataIndex++] = eeprom_read_byte(i);
    }
}

/**
 * Saves current configuration to the EEPROM.
 */
void config_save()
{
    uint8 *configData;
    uint16 i, dataIndex, calculatedCrc;

    configData = (uint8 *) &config;
    dataIndex = 0;

    checksum_reset();

    // Save configuration data to the EEPROM and calculate CRC16 sum
    for (i = CONFIG_DATA_EEPROM_ADDR;
            i < CONFIG_DATA_EEPROM_ADDR + sizeof (config_t); i++)
    {
        checksum_byte(configData[dataIndex]);

        if (eeprom_read_byte(i) != configData[dataIndex])
            eeprom_write_byte(i, configData[dataIndex]);

        dataIndex++;
    }

    // Store CRC16
    calculatedCrc = checksum_value();
    eeprom_write_byte(CONFIG_DATA_EEPROM_ADDR + sizeof (config_t),
            calculatedCrc >> 8);
    eeprom_write_byte(CONFIG_DATA_EEPROM_ADDR + sizeof (config_t) + 1,
            calculatedCrc & 0xFF);

}

/**
 * Configuers the TCP/IP Stack using the current configuration.
 */
void config_load_stack_settings()
{
    // Load MAC address
    AppConfig.MyMACAddr.v[0] = MY_DEFAULT_MAC_BYTE1;
    AppConfig.MyMACAddr.v[1] = MY_DEFAULT_MAC_BYTE2;
    AppConfig.MyMACAddr.v[2] = MY_DEFAULT_MAC_BYTE3;
    AppConfig.MyMACAddr.v[3] = MY_DEFAULT_MAC_BYTE4;
    AppConfig.MyMACAddr.v[4] = MY_DEFAULT_MAC_BYTE5;
    AppConfig.MyMACAddr.v[5] = MY_DEFAULT_MAC_BYTE6;

    if (CONFIG_ISSETF(CONFIG_DHCP_ENABLED))
    {
        _debug("DHCP enabled.\r\n");

        AppConfig.DefaultIPAddr.Val = 0ul;
        AppConfig.MyMask.Val = 0ul;
        AppConfig.MyGateway.Val = 0ul;
        AppConfig.DefaultIPAddr.Val = 0ul;
        AppConfig.DefaultMask.Val = 0ul;
        AppConfig.PrimaryDNSServer.Val = 0ul;
        AppConfig.SecondaryDNSServer.Val = 0ul;

        AppConfig.Flags.bIsDHCPEnabled = 1;
        DHCPEnable(0);
    }
    else
    {
        _debug("DHCP disabled, using static IP settings.\r\n");

        AppConfig.Flags.bIsDHCPEnabled = 0;
        DHCPDisable(0);

        AppConfig.MyIPAddr.v[0] = (config.ip >> 24) & 0xFF;
        AppConfig.MyIPAddr.v[1] = (config.ip >> 16) & 0xFF;
        AppConfig.MyIPAddr.v[2] = (config.ip >> 8) & 0xFF;
        AppConfig.MyIPAddr.v[3] = (config.ip) & 0xFF;

        AppConfig.MyMask.v[0] = (config.subnetMask >> 24) & 0xFF;
        AppConfig.MyMask.v[1] = (config.subnetMask >> 16) & 0xFF;
        AppConfig.MyMask.v[2] = (config.subnetMask >> 8) & 0xFF;
        AppConfig.MyMask.v[3] = (config.subnetMask) & 0xFF;

        AppConfig.MyGateway.v[0] = (config.defaultGateway >> 24) & 0xFF;
        AppConfig.MyGateway.v[1] = (config.defaultGateway >> 16) & 0xFF;
        AppConfig.MyGateway.v[2] = (config.defaultGateway >> 8) & 0xFF;
        AppConfig.MyGateway.v[3] = (config.defaultGateway) & 0xFF;

        AppConfig.DefaultIPAddr.v[0] = (config.ip >> 24) & 0xFF;
        AppConfig.DefaultIPAddr.v[1] = (config.ip >> 16) & 0xFF;
        AppConfig.DefaultIPAddr.v[2] = (config.ip >> 8) & 0xFF;
        AppConfig.DefaultIPAddr.v[3] = (config.ip) & 0xFF;

        AppConfig.DefaultMask.v[0] = (config.subnetMask >> 24) & 0xFF;
        AppConfig.DefaultMask.v[1] = (config.subnetMask >> 16) & 0xFF;
        AppConfig.DefaultMask.v[2] = (config.subnetMask >> 8) & 0xFF;
        AppConfig.DefaultMask.v[3] = (config.subnetMask) & 0xFF;

        AppConfig.PrimaryDNSServer.v[0] = (config.dnsIp >> 24) & 0xFF;
        AppConfig.PrimaryDNSServer.v[1] = (config.dnsIp >> 16) & 0xFF;
        AppConfig.PrimaryDNSServer.v[2] = (config.dnsIp >> 8) & 0xFF;
        AppConfig.PrimaryDNSServer.v[3] = (config.dnsIp) & 0xFF;
    }

}
