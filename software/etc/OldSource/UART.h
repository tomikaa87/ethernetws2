/**
 * Ethernet Weather Station v2 Firmware
 * UART communication routines
 *
 * @author Tamas Karpati <tomikaa87>
 * @date 2011-09-07
 * @file usart.h
 */

#ifndef UART_H
#define UART_H

#include "Types.h"

#define USART_BUF_SIZE  128

uint8 g_uartBuffer[USART_BUF_SIZE];
uint8 g_uartBufferNextIn = 0;
uint8 g_uartBufferNextOut = 0;

#define UART_isBufByteReady (g_uartBufferNextIn != g_uartBufferNextOut)

void serial_init();

void putch(uint8 byte);
uint8 getch(void);

uint8 getbufch();
void putbufch(uint8 c);

void serial_task();

#endif // UART_H
