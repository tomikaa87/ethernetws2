/**
 * Ethernet Weather Station v2 Firmware
 * Main source file
 *
 * @author Tamas Karpati <tomikaa87>
 * @date 2011-10-05
 * @file main.c
 */

#define THIS_IS_STACK_APPLICATION

#include <htc.h>
#include <stdio.h>

#include "TCPIPStack/TCPIP.h"
#include "Interrupt.h"
#include "Config.h"
#include "Debug.h"
#include "UART.h"
#include "NTP.h"
#include "25LC1024.h"
#include "SRAM_23K.h"
#include "LCDUI.h"
#include "graphlcd.h"
#include "Types.h"
#include "PIC.h"
#include "Utils.h"

#include "LCDUI_Text.h"

#include <time.h>

#include "WeatherData.h"
#include "t6963c.h"
#include "XMLDownloader.h"

/*** PIC Configuration ********************************************************/

__CONFIG(1, IESOEN & HSPLL);
__CONFIG(2, BOREN & BORV28 & WDTDIS);
__CONFIG(3, PBANDIS);
__CONFIG(4, LVPDIS);
__CONFIG(5, UNPROTECT);
__CONFIG(6, WRTEN);
__CONFIG(7, UNPROTECT);

/*** Global variables *********************************************************/

APP_CONFIG AppConfig;
extern config_t config;

DWORD g_epochTime = 0ul;
DWORD g_uptimeSeconds = 0ul;
DWORD g_epochOffset = 7200ul;

ExtSensorData g_extSensorData;// = { -1, -1, -1, -1, 0 };

/*** Routines *****************************************************************/

/*
unsigned int rnd = 0x34;

unsigned int random()
{
    unsigned char msb;

    msb = ((rnd >> 0) ^ (rnd >> 2) ^ (rnd >> 3) ^ (rnd >> 5)) & 1;
    rnd = (rnd >> 1) | (msb << 15);

    return rnd;
}
 */

//void main()
//{
//    PIC_setup();
//    UART_init();
//    EXT_EEPROM_init();
//    EXT_SRAM_init();
//    LCDUI_init();
//    GLCD_clear();
//    GLCD_clearBuf();
//
//    while (1) {
//        LCDUI_task();
//    }
//}

//char str[30];
//struct tm *ptm;
//WDATA_Union wu;

void main()
{
//    char str[30];

    PIC_setup();
    serial_init();
    ext_eeprom_init();
    ext_sram_init();
    //LCDUI_init();

    // Check stored configuration data and load it
    if (!config_is_valid())
    {
        _debug("Configuration data is corrupted. Loading defaults.\r\n");

//        LCDUI_drawFullScreenImageFromEeprom(LCDUI_BITMAP_CONFIG_WARNING);
//        DelayMs(5000);

        config_load_defaults();
        config_save();

        if (!config_is_valid())
            _debug("Error: Configuration data cannot be stored.\r\n");
    }
    config_load();

    CONFIG_CLRF(CONFIG_DHCP_ENABLED);
    config.ip = 0xC0A88902;     // 192.168.137.2
    config.dnsIp = 0xC0A88901;
    config.defaultGateway = 0xC0A88901;
    config.subnetMask = 0xFFFFFF00;

    config_load_stack_settings();

//    // DEVEL
//    if (!CONFIG_isFlagSet(CONFIG_NTP_ENABLED))
//    {
//        CONFIG_setFlag(CONFIG_NTP_ENABLED);
//        CONFIG_saveToEeprom();
//        _debug("NTP enabled.\r\n");
//    }

    _debug("Initializing TCP/IP stack...\r\n");
    StackInit();
    _debug("Initializing Tick subsystem...\r\n");
    TickInit();

//    GLCD_clearBuf();
//    GLCD_writeBuf(0);

    xmldl_reset();
    xmldl_start(XDL_MODE_WEATHER);

    _debug("System initialization finished.\r\n");

//    g_extSensorData.sensorDataUpdatedFlag = 0;

    while (1)
    {
        // Do stack related tasks
        StackTask();
        StackApplications();
//        NTP_task();

//        UART_task();
//        LCDUI_task();

        //if (CONFIG_isFlagSet(CONFIG_DHCP_ENABLED) || DHCPIsBound(0))
            xmldl_task();

//        if (g_extSensorData.sensorDataUpdatedFlag) {
//
//
//            g_extSensorData.sensorDataUpdatedFlag = 0;
//
//            _debug("External sensor data received\r\n");
//
//            GLCD_clearBuf();
//
//            // Outdoor temperature
//            sprintf(str, "Tout: %d.%02d C", g_extSensorData.tempOut1 / 100,
//                    abs(g_extSensorData.tempOut1 % 100));
//            LCDUI_TEXT_drawText(0, 0, str, LCDUI_TEXT_FONT_PF_TEMPESTA, 0, 0);
//
//            // Outdoor RH
//            sprintf(str, "RHout: %d.%02d %%", g_extSensorData.rhOut / 100,
//                    g_extSensorData.rhOut % 100);
//            LCDUI_TEXT_drawText(0, 10, str, LCDUI_TEXT_FONT_PF_TEMPESTA, 0, 0);
//
//            // Outdoor pressure
//            g_extSensorData.presOutSeaLevel = calcRelPres(
//                    g_extSensorData.presOut, 15812);
//            sprintf(str, "Pres: %lu.%02lu (%lu.%02lu) hPa",
//                    g_extSensorData.presOutSeaLevel / 100,
//                    g_extSensorData.presOutSeaLevel % 100,
//                    g_extSensorData.presOut / 100,
//                    g_extSensorData.presOut % 100);
//            LCDUI_TEXT_drawText(0, 20, str, LCDUI_TEXT_FONT_PF_TEMPESTA, 0, 0);
//
//            // Outdoor battery voltage
//            sprintf(str, "VDDout: %d.%03d Volts", g_extSensorData.vddOut / 1000,
//                    g_extSensorData.vddOut % 1000);
//            LCDUI_TEXT_drawText(0, 30, str, LCDUI_TEXT_FONT_PF_TEMPESTA, 0, 0);
//
//            // Indoor temperature
//            sprintf(str, "Tin: %d C", g_extSensorData.tempIn);
//            LCDUI_TEXT_drawText(0, 40, str, LCDUI_TEXT_FONT_PF_TEMPESTA, 0, 0);
//
//            // Indoor RH
//            sprintf(str, "RHin: %d %%", g_extSensorData.rhIn);
//            LCDUI_TEXT_drawText(0, 50, str, LCDUI_TEXT_FONT_PF_TEMPESTA, 0, 0);
//
//            // Weather Station VDD
//            sprintf(str, "VDD: %d.%03d Volts", g_extSensorData.vddIn / 1000,
//                    g_extSensorData.vddIn % 1000);
//            LCDUI_TEXT_drawText(0, 60, str, LCDUI_TEXT_FONT_PF_TEMPESTA, 0, 0);
//
//            // Sensor sync time
//            ptm = gmtime((time_t *)&g_epochTime);
//            //sprintf(str, "Synced at %lu", g_epochTime);
//            sprintf(str, "Synced at %d:%02d", ptm->tm_hour, ptm->tm_min);
//            LCDUI_TEXT_drawText(0, 110, str, LCDUI_TEXT_FONT_PF_TEMPESTA, 0, 0);
//
//            GLCD_writeBuf(0);
//        }
    }
}

//void main_()
//{
//    // Initialization
//    setupPic();
//    UART_init();
//    printf("*****************************************************\r\n" \
//           "*** Ethernet Weather Station 2  (C) ToMikaa, 2011 ***\r\n" \
//           "*** ********************************************* ***\r\n" \
//           "*** Firmware version: 1.0                         ***\r\n" \
//           "*****************************************************\r\n\r\n");
//    EXT_EEPROM_init();
//    EXT_SRAM_init();
//    LCDUI_init();
//
//    // Check stored configuration data and load it
//    if (!CONFIG_isConfigured())
//    {
//        _debug("Configuration data is corrupted. Loading defaults.\r\n");
//
//        LCDUI_drawFullScreenImageFromEeprom(LCDUI_BITMAP_CONFIG_WARNING);
//        DelayMs(5000);
//
//        CONFIG_loadDefaults();
//        CONFIG_saveToEeprom();
//
//        if (!CONFIG_isConfigured())
//            _debug("Error: Configuration data cannot be stored.\r\n");
//    }
//    CONFIG_loadFromEeprom();
//    CONFIG_loadTcpStackSettings();
//
//    // DEVEL
//    if (!CONFIG_isFlagSet(CONFIG_NTP_ENABLED))
//    {
//        CONFIG_setFlag(CONFIG_NTP_ENABLED);
//        CONFIG_saveToEeprom();
//        _debug("NTP enabled.\r\n");
//    }
//
//    _debug("Initializing TCP/IP stack...\r\n");
//    StackInit();
//    _debug("Initializing Tick subsystem...\r\n");
//    TickInit();
//
//    _debug("System initialization finished.\r\n");
//
//    _debug("Initialization took %d second(s)\r\n", g_epochTime);
//
//    GLCD_clear();
//    GLCD_clearBuf();
//
//    //LCDUI_TEXT_drawText(1, 1, "This is a test", LCDUI_TEXT_FONT_MICRO);
//
//    // Main loop
//    while (1)
//    {
//        // Do stack related tasks
//        StackTask();
//        StackApplications();
//
//        // Weather data downloader task
//        /*if (DHCPIsBound(0))
//            WXDL_task();*/
//
//        // Handle serial port events
//        UART_task();
//
//        // Do LCDUI related tasks
//        LCDUI_task();
//
//        // Do Network Time Protocol related tasks
//        NTP_task();
//    }
//}
