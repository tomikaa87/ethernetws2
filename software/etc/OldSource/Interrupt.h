/**
 * Ethernet Weather Station v2 Firmware
 * Interrupt Service Routines file
 *
 * @author Tamas Karpati <tomikaa87>
 * @date 2011-10-05
 * @file interrupt.h
 */

#ifndef INTERRUPT_H
#define INTERRUPT_H

void interrupt isr();
void interrupt low_priority low_isr();

#endif // INTERRUPT_H
