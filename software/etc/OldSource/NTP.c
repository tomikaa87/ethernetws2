/**
 * Ethernet Weather Station v2 Firmware
 * Simple Network Time Protocol routines
 *
 * @author Tamas Karpati <tomikaa87>
 * @date 2011-10-07
 * @file NTP.c
 */

#include "NTP.h"
#include "TCPIPStack/TCPIP.h"
#include "Debug.h"

extern DWORD g_epochTime;
extern DWORD g_epochOffset;
DWORD g_lastSntpSyncTime = 0ul;

/**
 * Runs all tasks needed to synchronize with a time server.
 */
void ntp_task()
{
    // If we have not synchornized yet or the last synchronization occured
    // a given time ago, synchronize the time.
    if (g_epochTime < 1000000ul ||
            SNTPGetUTCSeconds() - g_lastSntpSyncTime >= 30)
    {
        g_epochTime = SNTPGetUTCSeconds() + g_epochOffset;
        g_lastSntpSyncTime = g_epochTime;
        _debug("Time sync: %lu\r\n", g_epochTime);
    }
}
