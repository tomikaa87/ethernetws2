/* 
 * File:   Utils.h
 * Author: ToMikaa
 *
 * Created on 2012. j�nius 20., 14:18
 */

#ifndef UTILS_H
#define	UTILS_H

#include "Types.h"

uint32 utils_calc_rel_pres(uint32 absPres, uint16 alt);

#endif	/* UTILS_H */

