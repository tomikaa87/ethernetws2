/**
 * Ethernet Weather Station v2 Firmware
 * LCD User Interface - Number input dialog
 *
 * @author Tamas Karpati <tomikaa87>
 * @date 2011-02-11
 * @file LCDUI_NumberDialog.h
 */

#ifndef _LCDUI_NUMBERDIALOG_H
#define _LCDUI_NUMBERDIALOG_H

#include "Types.h"

typedef struct {
    int8 digitIndex;
    int8 length;
    int8 digits[10];
    int8 isNegative;
    const char *title;
} LCDUI_NUMBERDIALOG;

void lcdui_numdlg_init(LCDUI_NUMBERDIALOG *dialog,
        int32 initValue, int8 digits, const char *title);

void lcdui_numdlg_draw(LCDUI_NUMBERDIALOG *dialog);
void lcdui_numdlg_up(LCDUI_NUMBERDIALOG *dialog);
void lcdui_numdlg_down(LCDUI_NUMBERDIALOG *dialog);
void lcdui_numdlg_left(LCDUI_NUMBERDIALOG *dialog);
void lcdui_numdlg_right(LCDUI_NUMBERDIALOG *dialog);

#endif // _LCDUI_NUMBERDIALOG_H