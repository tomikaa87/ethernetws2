/**
 * Ethernet Weather Station v2 Firmware
 */

#include "PIC.h"

#include <htc.h>

void PIC_setup()
{
    // Wait for external oscillator to stabilize (IESO)
    while (!OSTS)
        continue;

    // Peripherals
    ADCON1 = 0x0F;      // Disable analog ports
    CVRCON = 0;         // Disable voltage reference
    CMCON = 0x07;       // Disable comparator

    // Disable ECAN module
    CANCON = 0b00100000;
    TRISB3 = 0;
    LATB3 = 1;
    while (CANSTAT & 0x80)
        continue;

    // Interrupts
    IPEN = 1;           // Enable interrupt priorities
    GIEH = 1;           // Global interrupt enable (high)
    GIEL = 1;           // Global interrupt enable (low)
    RCIP = 1;           // USART RX interrupt = high priority
    RCIE = 1;

    // Setup Timer1 as RTC
    T1CON = 0b11001010; // 16 bit, Timer1 OSC, 1:1, OSC on, SYNC, ext. OSC
    T1CON = 0x0F;
    TMR1H = 0x80;       // Preload Timer1 for 1 sec interrupt
    TMR1L = 0;
    TMR1IP = 0;         // Low priority interrupt
    TMR1IF = 0;         // Clear interrupt flag
    TMR1IE = 1;         // Enable interrupt
    TMR1ON = 1;         // Timer1 ON
}