#include "Utils.h"
#include <math.h>

/**
 * Calculates the relative pressure from absolute pressure and altitude.
 *
 * @param absPres Absolute pressure in Pa
 * @param alt Altitude in centimeters
 * @return Relative pressure in Pa
 */
uint32 utils_calc_rel_pres(uint32 absPres, uint16 alt)
{
    double ap, rp, a;
    uint32 rpui;

    ap = absPres / 100.0;
    a = alt / 100.0;
    rp = ap / pow(1 - a / 44330, 5.255);
    rpui = (uint32)(rp * 100.0);

    return rpui;
}