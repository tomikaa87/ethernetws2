#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Include project Makefile
include Makefile
# Include makefile containing local settings
ifeq "$(wildcard nbproject/Makefile-local-default.mk)" "nbproject/Makefile-local-default.mk"
include nbproject/Makefile-local-default.mk
endif

# Environment
MKDIR=gnumkdir -p
RM=rm -f 
MV=mv 
CP=cp 

# Macros
CND_CONF=default
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
IMAGE_TYPE=debug
OUTPUT_SUFFIX=cof
DEBUGGABLE_SUFFIX=cof
FINAL_IMAGE=dist/${CND_CONF}/${IMAGE_TYPE}/ethernetws2.${IMAGE_TYPE}.${OUTPUT_SUFFIX}
else
IMAGE_TYPE=production
OUTPUT_SUFFIX=hex
DEBUGGABLE_SUFFIX=cof
FINAL_IMAGE=dist/${CND_CONF}/${IMAGE_TYPE}/ethernetws2.${IMAGE_TYPE}.${OUTPUT_SUFFIX}
endif

# Object Directory
OBJECTDIR=build/${CND_CONF}/${IMAGE_TYPE}

# Distribution Directory
DISTDIR=dist/${CND_CONF}/${IMAGE_TYPE}

# Object Files Quoted if spaced
OBJECTFILES_QUOTED_IF_SPACED=${OBJECTDIR}/drivers/25LC1024.p1 ${OBJECTDIR}/drivers/KeyBrd.p1 ${OBJECTDIR}/drivers/SRAM_23K.p1 ${OBJECTDIR}/drivers/graphlcd.p1 ${OBJECTDIR}/drivers/pcd8544.p1 ${OBJECTDIR}/drivers/t6963c.p1 ${OBJECTDIR}/libs/CRC16.p1 ${OBJECTDIR}/libs/PWM.p1 ${OBJECTDIR}/TCPIPStack/ARP.p1 ${OBJECTDIR}/TCPIPStack/BigInt.p1 ${OBJECTDIR}/TCPIPStack/DHCP.p1 ${OBJECTDIR}/TCPIPStack/DNS.p1 ${OBJECTDIR}/TCPIPStack/Delay.p1 ${OBJECTDIR}/TCPIPStack/ENC28J60.p1 ${OBJECTDIR}/TCPIPStack/Hashes.p1 ${OBJECTDIR}/TCPIPStack/Helpers.p1 ${OBJECTDIR}/TCPIPStack/ICMP.p1 ${OBJECTDIR}/TCPIPStack/IP.p1 ${OBJECTDIR}/TCPIPStack/SNTP.p1 ${OBJECTDIR}/TCPIPStack/StackTsk.p1 ${OBJECTDIR}/TCPIPStack/TCP.p1 ${OBJECTDIR}/TCPIPStack/Tick.p1 ${OBJECTDIR}/TCPIPStack/UDP.p1 ${OBJECTDIR}/Config.p1 ${OBJECTDIR}/Interrupt.p1 ${OBJECTDIR}/Main.p1 ${OBJECTDIR}/NTP.p1 ${OBJECTDIR}/UART.p1 ${OBJECTDIR}/WeatherXMLDownloader.p1 ${OBJECTDIR}/WeatherXMLParser.p1 ${OBJECTDIR}/XMLDownloader.p1 ${OBJECTDIR}/XMLParser.p1 ${OBJECTDIR}/LCDUI.p1 ${OBJECTDIR}/LCDUI_Menu.p1 ${OBJECTDIR}/LCDUI_Text.p1 ${OBJECTDIR}/LCDUI_NumberDialog.p1 ${OBJECTDIR}/PIC.p1
POSSIBLE_DEPFILES=${OBJECTDIR}/drivers/25LC1024.p1.d ${OBJECTDIR}/drivers/KeyBrd.p1.d ${OBJECTDIR}/drivers/SRAM_23K.p1.d ${OBJECTDIR}/drivers/graphlcd.p1.d ${OBJECTDIR}/drivers/pcd8544.p1.d ${OBJECTDIR}/drivers/t6963c.p1.d ${OBJECTDIR}/libs/CRC16.p1.d ${OBJECTDIR}/libs/PWM.p1.d ${OBJECTDIR}/TCPIPStack/ARP.p1.d ${OBJECTDIR}/TCPIPStack/BigInt.p1.d ${OBJECTDIR}/TCPIPStack/DHCP.p1.d ${OBJECTDIR}/TCPIPStack/DNS.p1.d ${OBJECTDIR}/TCPIPStack/Delay.p1.d ${OBJECTDIR}/TCPIPStack/ENC28J60.p1.d ${OBJECTDIR}/TCPIPStack/Hashes.p1.d ${OBJECTDIR}/TCPIPStack/Helpers.p1.d ${OBJECTDIR}/TCPIPStack/ICMP.p1.d ${OBJECTDIR}/TCPIPStack/IP.p1.d ${OBJECTDIR}/TCPIPStack/SNTP.p1.d ${OBJECTDIR}/TCPIPStack/StackTsk.p1.d ${OBJECTDIR}/TCPIPStack/TCP.p1.d ${OBJECTDIR}/TCPIPStack/Tick.p1.d ${OBJECTDIR}/TCPIPStack/UDP.p1.d ${OBJECTDIR}/Config.p1.d ${OBJECTDIR}/Interrupt.p1.d ${OBJECTDIR}/Main.p1.d ${OBJECTDIR}/NTP.p1.d ${OBJECTDIR}/UART.p1.d ${OBJECTDIR}/WeatherXMLDownloader.p1.d ${OBJECTDIR}/WeatherXMLParser.p1.d ${OBJECTDIR}/XMLDownloader.p1.d ${OBJECTDIR}/XMLParser.p1.d ${OBJECTDIR}/LCDUI.p1.d ${OBJECTDIR}/LCDUI_Menu.p1.d ${OBJECTDIR}/LCDUI_Text.p1.d ${OBJECTDIR}/LCDUI_NumberDialog.p1.d ${OBJECTDIR}/PIC.p1.d

# Object Files
OBJECTFILES=${OBJECTDIR}/drivers/25LC1024.p1 ${OBJECTDIR}/drivers/KeyBrd.p1 ${OBJECTDIR}/drivers/SRAM_23K.p1 ${OBJECTDIR}/drivers/graphlcd.p1 ${OBJECTDIR}/drivers/pcd8544.p1 ${OBJECTDIR}/drivers/t6963c.p1 ${OBJECTDIR}/libs/CRC16.p1 ${OBJECTDIR}/libs/PWM.p1 ${OBJECTDIR}/TCPIPStack/ARP.p1 ${OBJECTDIR}/TCPIPStack/BigInt.p1 ${OBJECTDIR}/TCPIPStack/DHCP.p1 ${OBJECTDIR}/TCPIPStack/DNS.p1 ${OBJECTDIR}/TCPIPStack/Delay.p1 ${OBJECTDIR}/TCPIPStack/ENC28J60.p1 ${OBJECTDIR}/TCPIPStack/Hashes.p1 ${OBJECTDIR}/TCPIPStack/Helpers.p1 ${OBJECTDIR}/TCPIPStack/ICMP.p1 ${OBJECTDIR}/TCPIPStack/IP.p1 ${OBJECTDIR}/TCPIPStack/SNTP.p1 ${OBJECTDIR}/TCPIPStack/StackTsk.p1 ${OBJECTDIR}/TCPIPStack/TCP.p1 ${OBJECTDIR}/TCPIPStack/Tick.p1 ${OBJECTDIR}/TCPIPStack/UDP.p1 ${OBJECTDIR}/Config.p1 ${OBJECTDIR}/Interrupt.p1 ${OBJECTDIR}/Main.p1 ${OBJECTDIR}/NTP.p1 ${OBJECTDIR}/UART.p1 ${OBJECTDIR}/WeatherXMLDownloader.p1 ${OBJECTDIR}/WeatherXMLParser.p1 ${OBJECTDIR}/XMLDownloader.p1 ${OBJECTDIR}/XMLParser.p1 ${OBJECTDIR}/LCDUI.p1 ${OBJECTDIR}/LCDUI_Menu.p1 ${OBJECTDIR}/LCDUI_Text.p1 ${OBJECTDIR}/LCDUI_NumberDialog.p1 ${OBJECTDIR}/PIC.p1


CFLAGS=
ASFLAGS=
LDLIBSOPTIONS=

############# Tool locations ##########################################
# If you copy a project from one host to another, the path where the  #
# compiler is installed may be different.                             #
# If you open this project with MPLAB X in the new host, this         #
# makefile will be regenerated and the paths will be corrected.       #
#######################################################################
# fixDeps replaces a bunch of sed/cat/printf statements that slow down the build
FIXDEPS=fixDeps

.build-conf:  ${BUILD_SUBPROJECTS}
	${MAKE}  -f nbproject/Makefile-default.mk dist/${CND_CONF}/${IMAGE_TYPE}/ethernetws2.${IMAGE_TYPE}.${OUTPUT_SUFFIX}

MP_PROCESSOR_OPTION=18F4685
# ------------------------------------------------------------------------------------
# Rules for buildStep: assemble
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
else
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: compile
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
${OBJECTDIR}/drivers/25LC1024.p1: drivers/25LC1024.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/drivers 
	${MP_CC} --pass1 drivers/25LC1024.c $(MP_EXTRA_CC_PRE) -q --chip=$(MP_PROCESSOR_OPTION) -P  --outdir=${OBJECTDIR}/drivers -N31 -I"." -I"TCPIPStack" -I"libs" -I"drivers" --warn=0 --runtime=default,+clear,+init,-keep,-download,+stackwarn,-config,+clib,-plib --opt=default,+asm,-asmfile,-speed,+space,9 -D__DEBUG --debugger=pickit2 --cp=16 -Blarge --double=24  --mode=pro -g --asmlist "--errformat=%%f:%%l: error: %%s" "--msgformat=%%f:%%l: advisory: %%s" "--warnformat=%%f:%%l warning: %%s"  
	@${MP_CC} --scandep  drivers/25LC1024.c $(MP_EXTRA_CC_PRE) -q --chip=$(MP_PROCESSOR_OPTION) -P  --outdir=${OBJECTDIR}/drivers -N31 -I"." -I"TCPIPStack" -I"libs" -I"drivers" --warn=0 --runtime=default,+clear,+init,-keep,-download,+stackwarn,-config,+clib,-plib --opt=default,+asm,-asmfile,-speed,+space,9 -D__DEBUG --debugger=pickit2 --cp=16 -Blarge --double=24  --mode=pro -g --asmlist "--errformat=%%f:%%l: error: %%s" "--msgformat=%%f:%%l: advisory: %%s" "--warnformat=%%f:%%l warning: %%s"  
	@echo ${OBJECTDIR}/drivers/25LC1024.p1: > ${OBJECTDIR}/drivers/25LC1024.p1.d
	@cat ${OBJECTDIR}/drivers/25LC1024.dep >> ${OBJECTDIR}/drivers/25LC1024.p1.d
	@${FIXDEPS} "${OBJECTDIR}/drivers/25LC1024.p1.d" $(SILENT) -ht
	
${OBJECTDIR}/drivers/KeyBrd.p1: drivers/KeyBrd.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/drivers 
	${MP_CC} --pass1 drivers/KeyBrd.c $(MP_EXTRA_CC_PRE) -q --chip=$(MP_PROCESSOR_OPTION) -P  --outdir=${OBJECTDIR}/drivers -N31 -I"." -I"TCPIPStack" -I"libs" -I"drivers" --warn=0 --runtime=default,+clear,+init,-keep,-download,+stackwarn,-config,+clib,-plib --opt=default,+asm,-asmfile,-speed,+space,9 -D__DEBUG --debugger=pickit2 --cp=16 -Blarge --double=24  --mode=pro -g --asmlist "--errformat=%%f:%%l: error: %%s" "--msgformat=%%f:%%l: advisory: %%s" "--warnformat=%%f:%%l warning: %%s"  
	@${MP_CC} --scandep  drivers/KeyBrd.c $(MP_EXTRA_CC_PRE) -q --chip=$(MP_PROCESSOR_OPTION) -P  --outdir=${OBJECTDIR}/drivers -N31 -I"." -I"TCPIPStack" -I"libs" -I"drivers" --warn=0 --runtime=default,+clear,+init,-keep,-download,+stackwarn,-config,+clib,-plib --opt=default,+asm,-asmfile,-speed,+space,9 -D__DEBUG --debugger=pickit2 --cp=16 -Blarge --double=24  --mode=pro -g --asmlist "--errformat=%%f:%%l: error: %%s" "--msgformat=%%f:%%l: advisory: %%s" "--warnformat=%%f:%%l warning: %%s"  
	@echo ${OBJECTDIR}/drivers/KeyBrd.p1: > ${OBJECTDIR}/drivers/KeyBrd.p1.d
	@cat ${OBJECTDIR}/drivers/KeyBrd.dep >> ${OBJECTDIR}/drivers/KeyBrd.p1.d
	@${FIXDEPS} "${OBJECTDIR}/drivers/KeyBrd.p1.d" $(SILENT) -ht
	
${OBJECTDIR}/drivers/SRAM_23K.p1: drivers/SRAM_23K.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/drivers 
	${MP_CC} --pass1 drivers/SRAM_23K.c $(MP_EXTRA_CC_PRE) -q --chip=$(MP_PROCESSOR_OPTION) -P  --outdir=${OBJECTDIR}/drivers -N31 -I"." -I"TCPIPStack" -I"libs" -I"drivers" --warn=0 --runtime=default,+clear,+init,-keep,-download,+stackwarn,-config,+clib,-plib --opt=default,+asm,-asmfile,-speed,+space,9 -D__DEBUG --debugger=pickit2 --cp=16 -Blarge --double=24  --mode=pro -g --asmlist "--errformat=%%f:%%l: error: %%s" "--msgformat=%%f:%%l: advisory: %%s" "--warnformat=%%f:%%l warning: %%s"  
	@${MP_CC} --scandep  drivers/SRAM_23K.c $(MP_EXTRA_CC_PRE) -q --chip=$(MP_PROCESSOR_OPTION) -P  --outdir=${OBJECTDIR}/drivers -N31 -I"." -I"TCPIPStack" -I"libs" -I"drivers" --warn=0 --runtime=default,+clear,+init,-keep,-download,+stackwarn,-config,+clib,-plib --opt=default,+asm,-asmfile,-speed,+space,9 -D__DEBUG --debugger=pickit2 --cp=16 -Blarge --double=24  --mode=pro -g --asmlist "--errformat=%%f:%%l: error: %%s" "--msgformat=%%f:%%l: advisory: %%s" "--warnformat=%%f:%%l warning: %%s"  
	@echo ${OBJECTDIR}/drivers/SRAM_23K.p1: > ${OBJECTDIR}/drivers/SRAM_23K.p1.d
	@cat ${OBJECTDIR}/drivers/SRAM_23K.dep >> ${OBJECTDIR}/drivers/SRAM_23K.p1.d
	@${FIXDEPS} "${OBJECTDIR}/drivers/SRAM_23K.p1.d" $(SILENT) -ht
	
${OBJECTDIR}/drivers/graphlcd.p1: drivers/graphlcd.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/drivers 
	${MP_CC} --pass1 drivers/graphlcd.c $(MP_EXTRA_CC_PRE) -q --chip=$(MP_PROCESSOR_OPTION) -P  --outdir=${OBJECTDIR}/drivers -N31 -I"." -I"TCPIPStack" -I"libs" -I"drivers" --warn=0 --runtime=default,+clear,+init,-keep,-download,+stackwarn,-config,+clib,-plib --opt=default,+asm,-asmfile,-speed,+space,9 -D__DEBUG --debugger=pickit2 --cp=16 -Blarge --double=24  --mode=pro -g --asmlist "--errformat=%%f:%%l: error: %%s" "--msgformat=%%f:%%l: advisory: %%s" "--warnformat=%%f:%%l warning: %%s"  
	@${MP_CC} --scandep  drivers/graphlcd.c $(MP_EXTRA_CC_PRE) -q --chip=$(MP_PROCESSOR_OPTION) -P  --outdir=${OBJECTDIR}/drivers -N31 -I"." -I"TCPIPStack" -I"libs" -I"drivers" --warn=0 --runtime=default,+clear,+init,-keep,-download,+stackwarn,-config,+clib,-plib --opt=default,+asm,-asmfile,-speed,+space,9 -D__DEBUG --debugger=pickit2 --cp=16 -Blarge --double=24  --mode=pro -g --asmlist "--errformat=%%f:%%l: error: %%s" "--msgformat=%%f:%%l: advisory: %%s" "--warnformat=%%f:%%l warning: %%s"  
	@echo ${OBJECTDIR}/drivers/graphlcd.p1: > ${OBJECTDIR}/drivers/graphlcd.p1.d
	@cat ${OBJECTDIR}/drivers/graphlcd.dep >> ${OBJECTDIR}/drivers/graphlcd.p1.d
	@${FIXDEPS} "${OBJECTDIR}/drivers/graphlcd.p1.d" $(SILENT) -ht
	
${OBJECTDIR}/drivers/pcd8544.p1: drivers/pcd8544.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/drivers 
	${MP_CC} --pass1 drivers/pcd8544.c $(MP_EXTRA_CC_PRE) -q --chip=$(MP_PROCESSOR_OPTION) -P  --outdir=${OBJECTDIR}/drivers -N31 -I"." -I"TCPIPStack" -I"libs" -I"drivers" --warn=0 --runtime=default,+clear,+init,-keep,-download,+stackwarn,-config,+clib,-plib --opt=default,+asm,-asmfile,-speed,+space,9 -D__DEBUG --debugger=pickit2 --cp=16 -Blarge --double=24  --mode=pro -g --asmlist "--errformat=%%f:%%l: error: %%s" "--msgformat=%%f:%%l: advisory: %%s" "--warnformat=%%f:%%l warning: %%s"  
	@${MP_CC} --scandep  drivers/pcd8544.c $(MP_EXTRA_CC_PRE) -q --chip=$(MP_PROCESSOR_OPTION) -P  --outdir=${OBJECTDIR}/drivers -N31 -I"." -I"TCPIPStack" -I"libs" -I"drivers" --warn=0 --runtime=default,+clear,+init,-keep,-download,+stackwarn,-config,+clib,-plib --opt=default,+asm,-asmfile,-speed,+space,9 -D__DEBUG --debugger=pickit2 --cp=16 -Blarge --double=24  --mode=pro -g --asmlist "--errformat=%%f:%%l: error: %%s" "--msgformat=%%f:%%l: advisory: %%s" "--warnformat=%%f:%%l warning: %%s"  
	@echo ${OBJECTDIR}/drivers/pcd8544.p1: > ${OBJECTDIR}/drivers/pcd8544.p1.d
	@cat ${OBJECTDIR}/drivers/pcd8544.dep >> ${OBJECTDIR}/drivers/pcd8544.p1.d
	@${FIXDEPS} "${OBJECTDIR}/drivers/pcd8544.p1.d" $(SILENT) -ht
	
${OBJECTDIR}/drivers/t6963c.p1: drivers/t6963c.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/drivers 
	${MP_CC} --pass1 drivers/t6963c.c $(MP_EXTRA_CC_PRE) -q --chip=$(MP_PROCESSOR_OPTION) -P  --outdir=${OBJECTDIR}/drivers -N31 -I"." -I"TCPIPStack" -I"libs" -I"drivers" --warn=0 --runtime=default,+clear,+init,-keep,-download,+stackwarn,-config,+clib,-plib --opt=default,+asm,-asmfile,-speed,+space,9 -D__DEBUG --debugger=pickit2 --cp=16 -Blarge --double=24  --mode=pro -g --asmlist "--errformat=%%f:%%l: error: %%s" "--msgformat=%%f:%%l: advisory: %%s" "--warnformat=%%f:%%l warning: %%s"  
	@${MP_CC} --scandep  drivers/t6963c.c $(MP_EXTRA_CC_PRE) -q --chip=$(MP_PROCESSOR_OPTION) -P  --outdir=${OBJECTDIR}/drivers -N31 -I"." -I"TCPIPStack" -I"libs" -I"drivers" --warn=0 --runtime=default,+clear,+init,-keep,-download,+stackwarn,-config,+clib,-plib --opt=default,+asm,-asmfile,-speed,+space,9 -D__DEBUG --debugger=pickit2 --cp=16 -Blarge --double=24  --mode=pro -g --asmlist "--errformat=%%f:%%l: error: %%s" "--msgformat=%%f:%%l: advisory: %%s" "--warnformat=%%f:%%l warning: %%s"  
	@echo ${OBJECTDIR}/drivers/t6963c.p1: > ${OBJECTDIR}/drivers/t6963c.p1.d
	@cat ${OBJECTDIR}/drivers/t6963c.dep >> ${OBJECTDIR}/drivers/t6963c.p1.d
	@${FIXDEPS} "${OBJECTDIR}/drivers/t6963c.p1.d" $(SILENT) -ht
	
${OBJECTDIR}/libs/CRC16.p1: libs/CRC16.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/libs 
	${MP_CC} --pass1 libs/CRC16.c $(MP_EXTRA_CC_PRE) -q --chip=$(MP_PROCESSOR_OPTION) -P  --outdir=${OBJECTDIR}/libs -N31 -I"." -I"TCPIPStack" -I"libs" -I"drivers" --warn=0 --runtime=default,+clear,+init,-keep,-download,+stackwarn,-config,+clib,-plib --opt=default,+asm,-asmfile,-speed,+space,9 -D__DEBUG --debugger=pickit2 --cp=16 -Blarge --double=24  --mode=pro -g --asmlist "--errformat=%%f:%%l: error: %%s" "--msgformat=%%f:%%l: advisory: %%s" "--warnformat=%%f:%%l warning: %%s"  
	@${MP_CC} --scandep  libs/CRC16.c $(MP_EXTRA_CC_PRE) -q --chip=$(MP_PROCESSOR_OPTION) -P  --outdir=${OBJECTDIR}/libs -N31 -I"." -I"TCPIPStack" -I"libs" -I"drivers" --warn=0 --runtime=default,+clear,+init,-keep,-download,+stackwarn,-config,+clib,-plib --opt=default,+asm,-asmfile,-speed,+space,9 -D__DEBUG --debugger=pickit2 --cp=16 -Blarge --double=24  --mode=pro -g --asmlist "--errformat=%%f:%%l: error: %%s" "--msgformat=%%f:%%l: advisory: %%s" "--warnformat=%%f:%%l warning: %%s"  
	@echo ${OBJECTDIR}/libs/CRC16.p1: > ${OBJECTDIR}/libs/CRC16.p1.d
	@cat ${OBJECTDIR}/libs/CRC16.dep >> ${OBJECTDIR}/libs/CRC16.p1.d
	@${FIXDEPS} "${OBJECTDIR}/libs/CRC16.p1.d" $(SILENT) -ht
	
${OBJECTDIR}/libs/PWM.p1: libs/PWM.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/libs 
	${MP_CC} --pass1 libs/PWM.c $(MP_EXTRA_CC_PRE) -q --chip=$(MP_PROCESSOR_OPTION) -P  --outdir=${OBJECTDIR}/libs -N31 -I"." -I"TCPIPStack" -I"libs" -I"drivers" --warn=0 --runtime=default,+clear,+init,-keep,-download,+stackwarn,-config,+clib,-plib --opt=default,+asm,-asmfile,-speed,+space,9 -D__DEBUG --debugger=pickit2 --cp=16 -Blarge --double=24  --mode=pro -g --asmlist "--errformat=%%f:%%l: error: %%s" "--msgformat=%%f:%%l: advisory: %%s" "--warnformat=%%f:%%l warning: %%s"  
	@${MP_CC} --scandep  libs/PWM.c $(MP_EXTRA_CC_PRE) -q --chip=$(MP_PROCESSOR_OPTION) -P  --outdir=${OBJECTDIR}/libs -N31 -I"." -I"TCPIPStack" -I"libs" -I"drivers" --warn=0 --runtime=default,+clear,+init,-keep,-download,+stackwarn,-config,+clib,-plib --opt=default,+asm,-asmfile,-speed,+space,9 -D__DEBUG --debugger=pickit2 --cp=16 -Blarge --double=24  --mode=pro -g --asmlist "--errformat=%%f:%%l: error: %%s" "--msgformat=%%f:%%l: advisory: %%s" "--warnformat=%%f:%%l warning: %%s"  
	@echo ${OBJECTDIR}/libs/PWM.p1: > ${OBJECTDIR}/libs/PWM.p1.d
	@cat ${OBJECTDIR}/libs/PWM.dep >> ${OBJECTDIR}/libs/PWM.p1.d
	@${FIXDEPS} "${OBJECTDIR}/libs/PWM.p1.d" $(SILENT) -ht
	
${OBJECTDIR}/TCPIPStack/ARP.p1: TCPIPStack/ARP.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/TCPIPStack 
	${MP_CC} --pass1 TCPIPStack/ARP.c $(MP_EXTRA_CC_PRE) -q --chip=$(MP_PROCESSOR_OPTION) -P  --outdir=${OBJECTDIR}/TCPIPStack -N31 -I"." -I"TCPIPStack" -I"libs" -I"drivers" --warn=0 --runtime=default,+clear,+init,-keep,-download,+stackwarn,-config,+clib,-plib --opt=default,+asm,-asmfile,-speed,+space,9 -D__DEBUG --debugger=pickit2 --cp=16 -Blarge --double=24  --mode=pro -g --asmlist "--errformat=%%f:%%l: error: %%s" "--msgformat=%%f:%%l: advisory: %%s" "--warnformat=%%f:%%l warning: %%s"  
	@${MP_CC} --scandep  TCPIPStack/ARP.c $(MP_EXTRA_CC_PRE) -q --chip=$(MP_PROCESSOR_OPTION) -P  --outdir=${OBJECTDIR}/TCPIPStack -N31 -I"." -I"TCPIPStack" -I"libs" -I"drivers" --warn=0 --runtime=default,+clear,+init,-keep,-download,+stackwarn,-config,+clib,-plib --opt=default,+asm,-asmfile,-speed,+space,9 -D__DEBUG --debugger=pickit2 --cp=16 -Blarge --double=24  --mode=pro -g --asmlist "--errformat=%%f:%%l: error: %%s" "--msgformat=%%f:%%l: advisory: %%s" "--warnformat=%%f:%%l warning: %%s"  
	@echo ${OBJECTDIR}/TCPIPStack/ARP.p1: > ${OBJECTDIR}/TCPIPStack/ARP.p1.d
	@cat ${OBJECTDIR}/TCPIPStack/ARP.dep >> ${OBJECTDIR}/TCPIPStack/ARP.p1.d
	@${FIXDEPS} "${OBJECTDIR}/TCPIPStack/ARP.p1.d" $(SILENT) -ht
	
${OBJECTDIR}/TCPIPStack/BigInt.p1: TCPIPStack/BigInt.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/TCPIPStack 
	${MP_CC} --pass1 TCPIPStack/BigInt.c $(MP_EXTRA_CC_PRE) -q --chip=$(MP_PROCESSOR_OPTION) -P  --outdir=${OBJECTDIR}/TCPIPStack -N31 -I"." -I"TCPIPStack" -I"libs" -I"drivers" --warn=0 --runtime=default,+clear,+init,-keep,-download,+stackwarn,-config,+clib,-plib --opt=default,+asm,-asmfile,-speed,+space,9 -D__DEBUG --debugger=pickit2 --cp=16 -Blarge --double=24  --mode=pro -g --asmlist "--errformat=%%f:%%l: error: %%s" "--msgformat=%%f:%%l: advisory: %%s" "--warnformat=%%f:%%l warning: %%s"  
	@${MP_CC} --scandep  TCPIPStack/BigInt.c $(MP_EXTRA_CC_PRE) -q --chip=$(MP_PROCESSOR_OPTION) -P  --outdir=${OBJECTDIR}/TCPIPStack -N31 -I"." -I"TCPIPStack" -I"libs" -I"drivers" --warn=0 --runtime=default,+clear,+init,-keep,-download,+stackwarn,-config,+clib,-plib --opt=default,+asm,-asmfile,-speed,+space,9 -D__DEBUG --debugger=pickit2 --cp=16 -Blarge --double=24  --mode=pro -g --asmlist "--errformat=%%f:%%l: error: %%s" "--msgformat=%%f:%%l: advisory: %%s" "--warnformat=%%f:%%l warning: %%s"  
	@echo ${OBJECTDIR}/TCPIPStack/BigInt.p1: > ${OBJECTDIR}/TCPIPStack/BigInt.p1.d
	@cat ${OBJECTDIR}/TCPIPStack/BigInt.dep >> ${OBJECTDIR}/TCPIPStack/BigInt.p1.d
	@${FIXDEPS} "${OBJECTDIR}/TCPIPStack/BigInt.p1.d" $(SILENT) -ht
	
${OBJECTDIR}/TCPIPStack/DHCP.p1: TCPIPStack/DHCP.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/TCPIPStack 
	${MP_CC} --pass1 TCPIPStack/DHCP.c $(MP_EXTRA_CC_PRE) -q --chip=$(MP_PROCESSOR_OPTION) -P  --outdir=${OBJECTDIR}/TCPIPStack -N31 -I"." -I"TCPIPStack" -I"libs" -I"drivers" --warn=0 --runtime=default,+clear,+init,-keep,-download,+stackwarn,-config,+clib,-plib --opt=default,+asm,-asmfile,-speed,+space,9 -D__DEBUG --debugger=pickit2 --cp=16 -Blarge --double=24  --mode=pro -g --asmlist "--errformat=%%f:%%l: error: %%s" "--msgformat=%%f:%%l: advisory: %%s" "--warnformat=%%f:%%l warning: %%s"  
	@${MP_CC} --scandep  TCPIPStack/DHCP.c $(MP_EXTRA_CC_PRE) -q --chip=$(MP_PROCESSOR_OPTION) -P  --outdir=${OBJECTDIR}/TCPIPStack -N31 -I"." -I"TCPIPStack" -I"libs" -I"drivers" --warn=0 --runtime=default,+clear,+init,-keep,-download,+stackwarn,-config,+clib,-plib --opt=default,+asm,-asmfile,-speed,+space,9 -D__DEBUG --debugger=pickit2 --cp=16 -Blarge --double=24  --mode=pro -g --asmlist "--errformat=%%f:%%l: error: %%s" "--msgformat=%%f:%%l: advisory: %%s" "--warnformat=%%f:%%l warning: %%s"  
	@echo ${OBJECTDIR}/TCPIPStack/DHCP.p1: > ${OBJECTDIR}/TCPIPStack/DHCP.p1.d
	@cat ${OBJECTDIR}/TCPIPStack/DHCP.dep >> ${OBJECTDIR}/TCPIPStack/DHCP.p1.d
	@${FIXDEPS} "${OBJECTDIR}/TCPIPStack/DHCP.p1.d" $(SILENT) -ht
	
${OBJECTDIR}/TCPIPStack/DNS.p1: TCPIPStack/DNS.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/TCPIPStack 
	${MP_CC} --pass1 TCPIPStack/DNS.c $(MP_EXTRA_CC_PRE) -q --chip=$(MP_PROCESSOR_OPTION) -P  --outdir=${OBJECTDIR}/TCPIPStack -N31 -I"." -I"TCPIPStack" -I"libs" -I"drivers" --warn=0 --runtime=default,+clear,+init,-keep,-download,+stackwarn,-config,+clib,-plib --opt=default,+asm,-asmfile,-speed,+space,9 -D__DEBUG --debugger=pickit2 --cp=16 -Blarge --double=24  --mode=pro -g --asmlist "--errformat=%%f:%%l: error: %%s" "--msgformat=%%f:%%l: advisory: %%s" "--warnformat=%%f:%%l warning: %%s"  
	@${MP_CC} --scandep  TCPIPStack/DNS.c $(MP_EXTRA_CC_PRE) -q --chip=$(MP_PROCESSOR_OPTION) -P  --outdir=${OBJECTDIR}/TCPIPStack -N31 -I"." -I"TCPIPStack" -I"libs" -I"drivers" --warn=0 --runtime=default,+clear,+init,-keep,-download,+stackwarn,-config,+clib,-plib --opt=default,+asm,-asmfile,-speed,+space,9 -D__DEBUG --debugger=pickit2 --cp=16 -Blarge --double=24  --mode=pro -g --asmlist "--errformat=%%f:%%l: error: %%s" "--msgformat=%%f:%%l: advisory: %%s" "--warnformat=%%f:%%l warning: %%s"  
	@echo ${OBJECTDIR}/TCPIPStack/DNS.p1: > ${OBJECTDIR}/TCPIPStack/DNS.p1.d
	@cat ${OBJECTDIR}/TCPIPStack/DNS.dep >> ${OBJECTDIR}/TCPIPStack/DNS.p1.d
	@${FIXDEPS} "${OBJECTDIR}/TCPIPStack/DNS.p1.d" $(SILENT) -ht
	
${OBJECTDIR}/TCPIPStack/Delay.p1: TCPIPStack/Delay.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/TCPIPStack 
	${MP_CC} --pass1 TCPIPStack/Delay.c $(MP_EXTRA_CC_PRE) -q --chip=$(MP_PROCESSOR_OPTION) -P  --outdir=${OBJECTDIR}/TCPIPStack -N31 -I"." -I"TCPIPStack" -I"libs" -I"drivers" --warn=0 --runtime=default,+clear,+init,-keep,-download,+stackwarn,-config,+clib,-plib --opt=default,+asm,-asmfile,-speed,+space,9 -D__DEBUG --debugger=pickit2 --cp=16 -Blarge --double=24  --mode=pro -g --asmlist "--errformat=%%f:%%l: error: %%s" "--msgformat=%%f:%%l: advisory: %%s" "--warnformat=%%f:%%l warning: %%s"  
	@${MP_CC} --scandep  TCPIPStack/Delay.c $(MP_EXTRA_CC_PRE) -q --chip=$(MP_PROCESSOR_OPTION) -P  --outdir=${OBJECTDIR}/TCPIPStack -N31 -I"." -I"TCPIPStack" -I"libs" -I"drivers" --warn=0 --runtime=default,+clear,+init,-keep,-download,+stackwarn,-config,+clib,-plib --opt=default,+asm,-asmfile,-speed,+space,9 -D__DEBUG --debugger=pickit2 --cp=16 -Blarge --double=24  --mode=pro -g --asmlist "--errformat=%%f:%%l: error: %%s" "--msgformat=%%f:%%l: advisory: %%s" "--warnformat=%%f:%%l warning: %%s"  
	@echo ${OBJECTDIR}/TCPIPStack/Delay.p1: > ${OBJECTDIR}/TCPIPStack/Delay.p1.d
	@cat ${OBJECTDIR}/TCPIPStack/Delay.dep >> ${OBJECTDIR}/TCPIPStack/Delay.p1.d
	@${FIXDEPS} "${OBJECTDIR}/TCPIPStack/Delay.p1.d" $(SILENT) -ht
	
${OBJECTDIR}/TCPIPStack/ENC28J60.p1: TCPIPStack/ENC28J60.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/TCPIPStack 
	${MP_CC} --pass1 TCPIPStack/ENC28J60.c $(MP_EXTRA_CC_PRE) -q --chip=$(MP_PROCESSOR_OPTION) -P  --outdir=${OBJECTDIR}/TCPIPStack -N31 -I"." -I"TCPIPStack" -I"libs" -I"drivers" --warn=0 --runtime=default,+clear,+init,-keep,-download,+stackwarn,-config,+clib,-plib --opt=default,+asm,-asmfile,-speed,+space,9 -D__DEBUG --debugger=pickit2 --cp=16 -Blarge --double=24  --mode=pro -g --asmlist "--errformat=%%f:%%l: error: %%s" "--msgformat=%%f:%%l: advisory: %%s" "--warnformat=%%f:%%l warning: %%s"  
	@${MP_CC} --scandep  TCPIPStack/ENC28J60.c $(MP_EXTRA_CC_PRE) -q --chip=$(MP_PROCESSOR_OPTION) -P  --outdir=${OBJECTDIR}/TCPIPStack -N31 -I"." -I"TCPIPStack" -I"libs" -I"drivers" --warn=0 --runtime=default,+clear,+init,-keep,-download,+stackwarn,-config,+clib,-plib --opt=default,+asm,-asmfile,-speed,+space,9 -D__DEBUG --debugger=pickit2 --cp=16 -Blarge --double=24  --mode=pro -g --asmlist "--errformat=%%f:%%l: error: %%s" "--msgformat=%%f:%%l: advisory: %%s" "--warnformat=%%f:%%l warning: %%s"  
	@echo ${OBJECTDIR}/TCPIPStack/ENC28J60.p1: > ${OBJECTDIR}/TCPIPStack/ENC28J60.p1.d
	@cat ${OBJECTDIR}/TCPIPStack/ENC28J60.dep >> ${OBJECTDIR}/TCPIPStack/ENC28J60.p1.d
	@${FIXDEPS} "${OBJECTDIR}/TCPIPStack/ENC28J60.p1.d" $(SILENT) -ht
	
${OBJECTDIR}/TCPIPStack/Hashes.p1: TCPIPStack/Hashes.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/TCPIPStack 
	${MP_CC} --pass1 TCPIPStack/Hashes.c $(MP_EXTRA_CC_PRE) -q --chip=$(MP_PROCESSOR_OPTION) -P  --outdir=${OBJECTDIR}/TCPIPStack -N31 -I"." -I"TCPIPStack" -I"libs" -I"drivers" --warn=0 --runtime=default,+clear,+init,-keep,-download,+stackwarn,-config,+clib,-plib --opt=default,+asm,-asmfile,-speed,+space,9 -D__DEBUG --debugger=pickit2 --cp=16 -Blarge --double=24  --mode=pro -g --asmlist "--errformat=%%f:%%l: error: %%s" "--msgformat=%%f:%%l: advisory: %%s" "--warnformat=%%f:%%l warning: %%s"  
	@${MP_CC} --scandep  TCPIPStack/Hashes.c $(MP_EXTRA_CC_PRE) -q --chip=$(MP_PROCESSOR_OPTION) -P  --outdir=${OBJECTDIR}/TCPIPStack -N31 -I"." -I"TCPIPStack" -I"libs" -I"drivers" --warn=0 --runtime=default,+clear,+init,-keep,-download,+stackwarn,-config,+clib,-plib --opt=default,+asm,-asmfile,-speed,+space,9 -D__DEBUG --debugger=pickit2 --cp=16 -Blarge --double=24  --mode=pro -g --asmlist "--errformat=%%f:%%l: error: %%s" "--msgformat=%%f:%%l: advisory: %%s" "--warnformat=%%f:%%l warning: %%s"  
	@echo ${OBJECTDIR}/TCPIPStack/Hashes.p1: > ${OBJECTDIR}/TCPIPStack/Hashes.p1.d
	@cat ${OBJECTDIR}/TCPIPStack/Hashes.dep >> ${OBJECTDIR}/TCPIPStack/Hashes.p1.d
	@${FIXDEPS} "${OBJECTDIR}/TCPIPStack/Hashes.p1.d" $(SILENT) -ht
	
${OBJECTDIR}/TCPIPStack/Helpers.p1: TCPIPStack/Helpers.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/TCPIPStack 
	${MP_CC} --pass1 TCPIPStack/Helpers.c $(MP_EXTRA_CC_PRE) -q --chip=$(MP_PROCESSOR_OPTION) -P  --outdir=${OBJECTDIR}/TCPIPStack -N31 -I"." -I"TCPIPStack" -I"libs" -I"drivers" --warn=0 --runtime=default,+clear,+init,-keep,-download,+stackwarn,-config,+clib,-plib --opt=default,+asm,-asmfile,-speed,+space,9 -D__DEBUG --debugger=pickit2 --cp=16 -Blarge --double=24  --mode=pro -g --asmlist "--errformat=%%f:%%l: error: %%s" "--msgformat=%%f:%%l: advisory: %%s" "--warnformat=%%f:%%l warning: %%s"  
	@${MP_CC} --scandep  TCPIPStack/Helpers.c $(MP_EXTRA_CC_PRE) -q --chip=$(MP_PROCESSOR_OPTION) -P  --outdir=${OBJECTDIR}/TCPIPStack -N31 -I"." -I"TCPIPStack" -I"libs" -I"drivers" --warn=0 --runtime=default,+clear,+init,-keep,-download,+stackwarn,-config,+clib,-plib --opt=default,+asm,-asmfile,-speed,+space,9 -D__DEBUG --debugger=pickit2 --cp=16 -Blarge --double=24  --mode=pro -g --asmlist "--errformat=%%f:%%l: error: %%s" "--msgformat=%%f:%%l: advisory: %%s" "--warnformat=%%f:%%l warning: %%s"  
	@echo ${OBJECTDIR}/TCPIPStack/Helpers.p1: > ${OBJECTDIR}/TCPIPStack/Helpers.p1.d
	@cat ${OBJECTDIR}/TCPIPStack/Helpers.dep >> ${OBJECTDIR}/TCPIPStack/Helpers.p1.d
	@${FIXDEPS} "${OBJECTDIR}/TCPIPStack/Helpers.p1.d" $(SILENT) -ht
	
${OBJECTDIR}/TCPIPStack/ICMP.p1: TCPIPStack/ICMP.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/TCPIPStack 
	${MP_CC} --pass1 TCPIPStack/ICMP.c $(MP_EXTRA_CC_PRE) -q --chip=$(MP_PROCESSOR_OPTION) -P  --outdir=${OBJECTDIR}/TCPIPStack -N31 -I"." -I"TCPIPStack" -I"libs" -I"drivers" --warn=0 --runtime=default,+clear,+init,-keep,-download,+stackwarn,-config,+clib,-plib --opt=default,+asm,-asmfile,-speed,+space,9 -D__DEBUG --debugger=pickit2 --cp=16 -Blarge --double=24  --mode=pro -g --asmlist "--errformat=%%f:%%l: error: %%s" "--msgformat=%%f:%%l: advisory: %%s" "--warnformat=%%f:%%l warning: %%s"  
	@${MP_CC} --scandep  TCPIPStack/ICMP.c $(MP_EXTRA_CC_PRE) -q --chip=$(MP_PROCESSOR_OPTION) -P  --outdir=${OBJECTDIR}/TCPIPStack -N31 -I"." -I"TCPIPStack" -I"libs" -I"drivers" --warn=0 --runtime=default,+clear,+init,-keep,-download,+stackwarn,-config,+clib,-plib --opt=default,+asm,-asmfile,-speed,+space,9 -D__DEBUG --debugger=pickit2 --cp=16 -Blarge --double=24  --mode=pro -g --asmlist "--errformat=%%f:%%l: error: %%s" "--msgformat=%%f:%%l: advisory: %%s" "--warnformat=%%f:%%l warning: %%s"  
	@echo ${OBJECTDIR}/TCPIPStack/ICMP.p1: > ${OBJECTDIR}/TCPIPStack/ICMP.p1.d
	@cat ${OBJECTDIR}/TCPIPStack/ICMP.dep >> ${OBJECTDIR}/TCPIPStack/ICMP.p1.d
	@${FIXDEPS} "${OBJECTDIR}/TCPIPStack/ICMP.p1.d" $(SILENT) -ht
	
${OBJECTDIR}/TCPIPStack/IP.p1: TCPIPStack/IP.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/TCPIPStack 
	${MP_CC} --pass1 TCPIPStack/IP.c $(MP_EXTRA_CC_PRE) -q --chip=$(MP_PROCESSOR_OPTION) -P  --outdir=${OBJECTDIR}/TCPIPStack -N31 -I"." -I"TCPIPStack" -I"libs" -I"drivers" --warn=0 --runtime=default,+clear,+init,-keep,-download,+stackwarn,-config,+clib,-plib --opt=default,+asm,-asmfile,-speed,+space,9 -D__DEBUG --debugger=pickit2 --cp=16 -Blarge --double=24  --mode=pro -g --asmlist "--errformat=%%f:%%l: error: %%s" "--msgformat=%%f:%%l: advisory: %%s" "--warnformat=%%f:%%l warning: %%s"  
	@${MP_CC} --scandep  TCPIPStack/IP.c $(MP_EXTRA_CC_PRE) -q --chip=$(MP_PROCESSOR_OPTION) -P  --outdir=${OBJECTDIR}/TCPIPStack -N31 -I"." -I"TCPIPStack" -I"libs" -I"drivers" --warn=0 --runtime=default,+clear,+init,-keep,-download,+stackwarn,-config,+clib,-plib --opt=default,+asm,-asmfile,-speed,+space,9 -D__DEBUG --debugger=pickit2 --cp=16 -Blarge --double=24  --mode=pro -g --asmlist "--errformat=%%f:%%l: error: %%s" "--msgformat=%%f:%%l: advisory: %%s" "--warnformat=%%f:%%l warning: %%s"  
	@echo ${OBJECTDIR}/TCPIPStack/IP.p1: > ${OBJECTDIR}/TCPIPStack/IP.p1.d
	@cat ${OBJECTDIR}/TCPIPStack/IP.dep >> ${OBJECTDIR}/TCPIPStack/IP.p1.d
	@${FIXDEPS} "${OBJECTDIR}/TCPIPStack/IP.p1.d" $(SILENT) -ht
	
${OBJECTDIR}/TCPIPStack/SNTP.p1: TCPIPStack/SNTP.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/TCPIPStack 
	${MP_CC} --pass1 TCPIPStack/SNTP.c $(MP_EXTRA_CC_PRE) -q --chip=$(MP_PROCESSOR_OPTION) -P  --outdir=${OBJECTDIR}/TCPIPStack -N31 -I"." -I"TCPIPStack" -I"libs" -I"drivers" --warn=0 --runtime=default,+clear,+init,-keep,-download,+stackwarn,-config,+clib,-plib --opt=default,+asm,-asmfile,-speed,+space,9 -D__DEBUG --debugger=pickit2 --cp=16 -Blarge --double=24  --mode=pro -g --asmlist "--errformat=%%f:%%l: error: %%s" "--msgformat=%%f:%%l: advisory: %%s" "--warnformat=%%f:%%l warning: %%s"  
	@${MP_CC} --scandep  TCPIPStack/SNTP.c $(MP_EXTRA_CC_PRE) -q --chip=$(MP_PROCESSOR_OPTION) -P  --outdir=${OBJECTDIR}/TCPIPStack -N31 -I"." -I"TCPIPStack" -I"libs" -I"drivers" --warn=0 --runtime=default,+clear,+init,-keep,-download,+stackwarn,-config,+clib,-plib --opt=default,+asm,-asmfile,-speed,+space,9 -D__DEBUG --debugger=pickit2 --cp=16 -Blarge --double=24  --mode=pro -g --asmlist "--errformat=%%f:%%l: error: %%s" "--msgformat=%%f:%%l: advisory: %%s" "--warnformat=%%f:%%l warning: %%s"  
	@echo ${OBJECTDIR}/TCPIPStack/SNTP.p1: > ${OBJECTDIR}/TCPIPStack/SNTP.p1.d
	@cat ${OBJECTDIR}/TCPIPStack/SNTP.dep >> ${OBJECTDIR}/TCPIPStack/SNTP.p1.d
	@${FIXDEPS} "${OBJECTDIR}/TCPIPStack/SNTP.p1.d" $(SILENT) -ht
	
${OBJECTDIR}/TCPIPStack/StackTsk.p1: TCPIPStack/StackTsk.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/TCPIPStack 
	${MP_CC} --pass1 TCPIPStack/StackTsk.c $(MP_EXTRA_CC_PRE) -q --chip=$(MP_PROCESSOR_OPTION) -P  --outdir=${OBJECTDIR}/TCPIPStack -N31 -I"." -I"TCPIPStack" -I"libs" -I"drivers" --warn=0 --runtime=default,+clear,+init,-keep,-download,+stackwarn,-config,+clib,-plib --opt=default,+asm,-asmfile,-speed,+space,9 -D__DEBUG --debugger=pickit2 --cp=16 -Blarge --double=24  --mode=pro -g --asmlist "--errformat=%%f:%%l: error: %%s" "--msgformat=%%f:%%l: advisory: %%s" "--warnformat=%%f:%%l warning: %%s"  
	@${MP_CC} --scandep  TCPIPStack/StackTsk.c $(MP_EXTRA_CC_PRE) -q --chip=$(MP_PROCESSOR_OPTION) -P  --outdir=${OBJECTDIR}/TCPIPStack -N31 -I"." -I"TCPIPStack" -I"libs" -I"drivers" --warn=0 --runtime=default,+clear,+init,-keep,-download,+stackwarn,-config,+clib,-plib --opt=default,+asm,-asmfile,-speed,+space,9 -D__DEBUG --debugger=pickit2 --cp=16 -Blarge --double=24  --mode=pro -g --asmlist "--errformat=%%f:%%l: error: %%s" "--msgformat=%%f:%%l: advisory: %%s" "--warnformat=%%f:%%l warning: %%s"  
	@echo ${OBJECTDIR}/TCPIPStack/StackTsk.p1: > ${OBJECTDIR}/TCPIPStack/StackTsk.p1.d
	@cat ${OBJECTDIR}/TCPIPStack/StackTsk.dep >> ${OBJECTDIR}/TCPIPStack/StackTsk.p1.d
	@${FIXDEPS} "${OBJECTDIR}/TCPIPStack/StackTsk.p1.d" $(SILENT) -ht
	
${OBJECTDIR}/TCPIPStack/TCP.p1: TCPIPStack/TCP.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/TCPIPStack 
	${MP_CC} --pass1 TCPIPStack/TCP.c $(MP_EXTRA_CC_PRE) -q --chip=$(MP_PROCESSOR_OPTION) -P  --outdir=${OBJECTDIR}/TCPIPStack -N31 -I"." -I"TCPIPStack" -I"libs" -I"drivers" --warn=0 --runtime=default,+clear,+init,-keep,-download,+stackwarn,-config,+clib,-plib --opt=default,+asm,-asmfile,-speed,+space,9 -D__DEBUG --debugger=pickit2 --cp=16 -Blarge --double=24  --mode=pro -g --asmlist "--errformat=%%f:%%l: error: %%s" "--msgformat=%%f:%%l: advisory: %%s" "--warnformat=%%f:%%l warning: %%s"  
	@${MP_CC} --scandep  TCPIPStack/TCP.c $(MP_EXTRA_CC_PRE) -q --chip=$(MP_PROCESSOR_OPTION) -P  --outdir=${OBJECTDIR}/TCPIPStack -N31 -I"." -I"TCPIPStack" -I"libs" -I"drivers" --warn=0 --runtime=default,+clear,+init,-keep,-download,+stackwarn,-config,+clib,-plib --opt=default,+asm,-asmfile,-speed,+space,9 -D__DEBUG --debugger=pickit2 --cp=16 -Blarge --double=24  --mode=pro -g --asmlist "--errformat=%%f:%%l: error: %%s" "--msgformat=%%f:%%l: advisory: %%s" "--warnformat=%%f:%%l warning: %%s"  
	@echo ${OBJECTDIR}/TCPIPStack/TCP.p1: > ${OBJECTDIR}/TCPIPStack/TCP.p1.d
	@cat ${OBJECTDIR}/TCPIPStack/TCP.dep >> ${OBJECTDIR}/TCPIPStack/TCP.p1.d
	@${FIXDEPS} "${OBJECTDIR}/TCPIPStack/TCP.p1.d" $(SILENT) -ht
	
${OBJECTDIR}/TCPIPStack/Tick.p1: TCPIPStack/Tick.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/TCPIPStack 
	${MP_CC} --pass1 TCPIPStack/Tick.c $(MP_EXTRA_CC_PRE) -q --chip=$(MP_PROCESSOR_OPTION) -P  --outdir=${OBJECTDIR}/TCPIPStack -N31 -I"." -I"TCPIPStack" -I"libs" -I"drivers" --warn=0 --runtime=default,+clear,+init,-keep,-download,+stackwarn,-config,+clib,-plib --opt=default,+asm,-asmfile,-speed,+space,9 -D__DEBUG --debugger=pickit2 --cp=16 -Blarge --double=24  --mode=pro -g --asmlist "--errformat=%%f:%%l: error: %%s" "--msgformat=%%f:%%l: advisory: %%s" "--warnformat=%%f:%%l warning: %%s"  
	@${MP_CC} --scandep  TCPIPStack/Tick.c $(MP_EXTRA_CC_PRE) -q --chip=$(MP_PROCESSOR_OPTION) -P  --outdir=${OBJECTDIR}/TCPIPStack -N31 -I"." -I"TCPIPStack" -I"libs" -I"drivers" --warn=0 --runtime=default,+clear,+init,-keep,-download,+stackwarn,-config,+clib,-plib --opt=default,+asm,-asmfile,-speed,+space,9 -D__DEBUG --debugger=pickit2 --cp=16 -Blarge --double=24  --mode=pro -g --asmlist "--errformat=%%f:%%l: error: %%s" "--msgformat=%%f:%%l: advisory: %%s" "--warnformat=%%f:%%l warning: %%s"  
	@echo ${OBJECTDIR}/TCPIPStack/Tick.p1: > ${OBJECTDIR}/TCPIPStack/Tick.p1.d
	@cat ${OBJECTDIR}/TCPIPStack/Tick.dep >> ${OBJECTDIR}/TCPIPStack/Tick.p1.d
	@${FIXDEPS} "${OBJECTDIR}/TCPIPStack/Tick.p1.d" $(SILENT) -ht
	
${OBJECTDIR}/TCPIPStack/UDP.p1: TCPIPStack/UDP.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/TCPIPStack 
	${MP_CC} --pass1 TCPIPStack/UDP.c $(MP_EXTRA_CC_PRE) -q --chip=$(MP_PROCESSOR_OPTION) -P  --outdir=${OBJECTDIR}/TCPIPStack -N31 -I"." -I"TCPIPStack" -I"libs" -I"drivers" --warn=0 --runtime=default,+clear,+init,-keep,-download,+stackwarn,-config,+clib,-plib --opt=default,+asm,-asmfile,-speed,+space,9 -D__DEBUG --debugger=pickit2 --cp=16 -Blarge --double=24  --mode=pro -g --asmlist "--errformat=%%f:%%l: error: %%s" "--msgformat=%%f:%%l: advisory: %%s" "--warnformat=%%f:%%l warning: %%s"  
	@${MP_CC} --scandep  TCPIPStack/UDP.c $(MP_EXTRA_CC_PRE) -q --chip=$(MP_PROCESSOR_OPTION) -P  --outdir=${OBJECTDIR}/TCPIPStack -N31 -I"." -I"TCPIPStack" -I"libs" -I"drivers" --warn=0 --runtime=default,+clear,+init,-keep,-download,+stackwarn,-config,+clib,-plib --opt=default,+asm,-asmfile,-speed,+space,9 -D__DEBUG --debugger=pickit2 --cp=16 -Blarge --double=24  --mode=pro -g --asmlist "--errformat=%%f:%%l: error: %%s" "--msgformat=%%f:%%l: advisory: %%s" "--warnformat=%%f:%%l warning: %%s"  
	@echo ${OBJECTDIR}/TCPIPStack/UDP.p1: > ${OBJECTDIR}/TCPIPStack/UDP.p1.d
	@cat ${OBJECTDIR}/TCPIPStack/UDP.dep >> ${OBJECTDIR}/TCPIPStack/UDP.p1.d
	@${FIXDEPS} "${OBJECTDIR}/TCPIPStack/UDP.p1.d" $(SILENT) -ht
	
${OBJECTDIR}/Config.p1: Config.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR} 
	${MP_CC} --pass1 Config.c $(MP_EXTRA_CC_PRE) -q --chip=$(MP_PROCESSOR_OPTION) -P  --outdir=${OBJECTDIR} -N31 -I"." -I"TCPIPStack" -I"libs" -I"drivers" --warn=0 --runtime=default,+clear,+init,-keep,-download,+stackwarn,-config,+clib,-plib --opt=default,+asm,-asmfile,-speed,+space,9 -D__DEBUG --debugger=pickit2 --cp=16 -Blarge --double=24  --mode=pro -g --asmlist "--errformat=%%f:%%l: error: %%s" "--msgformat=%%f:%%l: advisory: %%s" "--warnformat=%%f:%%l warning: %%s"  
	@${MP_CC} --scandep  Config.c $(MP_EXTRA_CC_PRE) -q --chip=$(MP_PROCESSOR_OPTION) -P  --outdir=${OBJECTDIR} -N31 -I"." -I"TCPIPStack" -I"libs" -I"drivers" --warn=0 --runtime=default,+clear,+init,-keep,-download,+stackwarn,-config,+clib,-plib --opt=default,+asm,-asmfile,-speed,+space,9 -D__DEBUG --debugger=pickit2 --cp=16 -Blarge --double=24  --mode=pro -g --asmlist "--errformat=%%f:%%l: error: %%s" "--msgformat=%%f:%%l: advisory: %%s" "--warnformat=%%f:%%l warning: %%s"  
	@echo ${OBJECTDIR}/Config.p1: > ${OBJECTDIR}/Config.p1.d
	@cat ${OBJECTDIR}/Config.dep >> ${OBJECTDIR}/Config.p1.d
	@${FIXDEPS} "${OBJECTDIR}/Config.p1.d" $(SILENT) -ht
	
${OBJECTDIR}/Interrupt.p1: Interrupt.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR} 
	${MP_CC} --pass1 Interrupt.c $(MP_EXTRA_CC_PRE) -q --chip=$(MP_PROCESSOR_OPTION) -P  --outdir=${OBJECTDIR} -N31 -I"." -I"TCPIPStack" -I"libs" -I"drivers" --warn=0 --runtime=default,+clear,+init,-keep,-download,+stackwarn,-config,+clib,-plib --opt=default,+asm,-asmfile,-speed,+space,9 -D__DEBUG --debugger=pickit2 --cp=16 -Blarge --double=24  --mode=pro -g --asmlist "--errformat=%%f:%%l: error: %%s" "--msgformat=%%f:%%l: advisory: %%s" "--warnformat=%%f:%%l warning: %%s"  
	@${MP_CC} --scandep  Interrupt.c $(MP_EXTRA_CC_PRE) -q --chip=$(MP_PROCESSOR_OPTION) -P  --outdir=${OBJECTDIR} -N31 -I"." -I"TCPIPStack" -I"libs" -I"drivers" --warn=0 --runtime=default,+clear,+init,-keep,-download,+stackwarn,-config,+clib,-plib --opt=default,+asm,-asmfile,-speed,+space,9 -D__DEBUG --debugger=pickit2 --cp=16 -Blarge --double=24  --mode=pro -g --asmlist "--errformat=%%f:%%l: error: %%s" "--msgformat=%%f:%%l: advisory: %%s" "--warnformat=%%f:%%l warning: %%s"  
	@echo ${OBJECTDIR}/Interrupt.p1: > ${OBJECTDIR}/Interrupt.p1.d
	@cat ${OBJECTDIR}/Interrupt.dep >> ${OBJECTDIR}/Interrupt.p1.d
	@${FIXDEPS} "${OBJECTDIR}/Interrupt.p1.d" $(SILENT) -ht
	
${OBJECTDIR}/Main.p1: Main.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR} 
	${MP_CC} --pass1 Main.c $(MP_EXTRA_CC_PRE) -q --chip=$(MP_PROCESSOR_OPTION) -P  --outdir=${OBJECTDIR} -N31 -I"." -I"TCPIPStack" -I"libs" -I"drivers" --warn=0 --runtime=default,+clear,+init,-keep,-download,+stackwarn,-config,+clib,-plib --opt=default,+asm,-asmfile,-speed,+space,9 -D__DEBUG --debugger=pickit2 --cp=16 -Blarge --double=24  --mode=pro -g --asmlist "--errformat=%%f:%%l: error: %%s" "--msgformat=%%f:%%l: advisory: %%s" "--warnformat=%%f:%%l warning: %%s"  
	@${MP_CC} --scandep  Main.c $(MP_EXTRA_CC_PRE) -q --chip=$(MP_PROCESSOR_OPTION) -P  --outdir=${OBJECTDIR} -N31 -I"." -I"TCPIPStack" -I"libs" -I"drivers" --warn=0 --runtime=default,+clear,+init,-keep,-download,+stackwarn,-config,+clib,-plib --opt=default,+asm,-asmfile,-speed,+space,9 -D__DEBUG --debugger=pickit2 --cp=16 -Blarge --double=24  --mode=pro -g --asmlist "--errformat=%%f:%%l: error: %%s" "--msgformat=%%f:%%l: advisory: %%s" "--warnformat=%%f:%%l warning: %%s"  
	@echo ${OBJECTDIR}/Main.p1: > ${OBJECTDIR}/Main.p1.d
	@cat ${OBJECTDIR}/Main.dep >> ${OBJECTDIR}/Main.p1.d
	@${FIXDEPS} "${OBJECTDIR}/Main.p1.d" $(SILENT) -ht
	
${OBJECTDIR}/NTP.p1: NTP.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR} 
	${MP_CC} --pass1 NTP.c $(MP_EXTRA_CC_PRE) -q --chip=$(MP_PROCESSOR_OPTION) -P  --outdir=${OBJECTDIR} -N31 -I"." -I"TCPIPStack" -I"libs" -I"drivers" --warn=0 --runtime=default,+clear,+init,-keep,-download,+stackwarn,-config,+clib,-plib --opt=default,+asm,-asmfile,-speed,+space,9 -D__DEBUG --debugger=pickit2 --cp=16 -Blarge --double=24  --mode=pro -g --asmlist "--errformat=%%f:%%l: error: %%s" "--msgformat=%%f:%%l: advisory: %%s" "--warnformat=%%f:%%l warning: %%s"  
	@${MP_CC} --scandep  NTP.c $(MP_EXTRA_CC_PRE) -q --chip=$(MP_PROCESSOR_OPTION) -P  --outdir=${OBJECTDIR} -N31 -I"." -I"TCPIPStack" -I"libs" -I"drivers" --warn=0 --runtime=default,+clear,+init,-keep,-download,+stackwarn,-config,+clib,-plib --opt=default,+asm,-asmfile,-speed,+space,9 -D__DEBUG --debugger=pickit2 --cp=16 -Blarge --double=24  --mode=pro -g --asmlist "--errformat=%%f:%%l: error: %%s" "--msgformat=%%f:%%l: advisory: %%s" "--warnformat=%%f:%%l warning: %%s"  
	@echo ${OBJECTDIR}/NTP.p1: > ${OBJECTDIR}/NTP.p1.d
	@cat ${OBJECTDIR}/NTP.dep >> ${OBJECTDIR}/NTP.p1.d
	@${FIXDEPS} "${OBJECTDIR}/NTP.p1.d" $(SILENT) -ht
	
${OBJECTDIR}/UART.p1: UART.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR} 
	${MP_CC} --pass1 UART.c $(MP_EXTRA_CC_PRE) -q --chip=$(MP_PROCESSOR_OPTION) -P  --outdir=${OBJECTDIR} -N31 -I"." -I"TCPIPStack" -I"libs" -I"drivers" --warn=0 --runtime=default,+clear,+init,-keep,-download,+stackwarn,-config,+clib,-plib --opt=default,+asm,-asmfile,-speed,+space,9 -D__DEBUG --debugger=pickit2 --cp=16 -Blarge --double=24  --mode=pro -g --asmlist "--errformat=%%f:%%l: error: %%s" "--msgformat=%%f:%%l: advisory: %%s" "--warnformat=%%f:%%l warning: %%s"  
	@${MP_CC} --scandep  UART.c $(MP_EXTRA_CC_PRE) -q --chip=$(MP_PROCESSOR_OPTION) -P  --outdir=${OBJECTDIR} -N31 -I"." -I"TCPIPStack" -I"libs" -I"drivers" --warn=0 --runtime=default,+clear,+init,-keep,-download,+stackwarn,-config,+clib,-plib --opt=default,+asm,-asmfile,-speed,+space,9 -D__DEBUG --debugger=pickit2 --cp=16 -Blarge --double=24  --mode=pro -g --asmlist "--errformat=%%f:%%l: error: %%s" "--msgformat=%%f:%%l: advisory: %%s" "--warnformat=%%f:%%l warning: %%s"  
	@echo ${OBJECTDIR}/UART.p1: > ${OBJECTDIR}/UART.p1.d
	@cat ${OBJECTDIR}/UART.dep >> ${OBJECTDIR}/UART.p1.d
	@${FIXDEPS} "${OBJECTDIR}/UART.p1.d" $(SILENT) -ht
	
${OBJECTDIR}/WeatherXMLDownloader.p1: WeatherXMLDownloader.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR} 
	${MP_CC} --pass1 WeatherXMLDownloader.c $(MP_EXTRA_CC_PRE) -q --chip=$(MP_PROCESSOR_OPTION) -P  --outdir=${OBJECTDIR} -N31 -I"." -I"TCPIPStack" -I"libs" -I"drivers" --warn=0 --runtime=default,+clear,+init,-keep,-download,+stackwarn,-config,+clib,-plib --opt=default,+asm,-asmfile,-speed,+space,9 -D__DEBUG --debugger=pickit2 --cp=16 -Blarge --double=24  --mode=pro -g --asmlist "--errformat=%%f:%%l: error: %%s" "--msgformat=%%f:%%l: advisory: %%s" "--warnformat=%%f:%%l warning: %%s"  
	@${MP_CC} --scandep  WeatherXMLDownloader.c $(MP_EXTRA_CC_PRE) -q --chip=$(MP_PROCESSOR_OPTION) -P  --outdir=${OBJECTDIR} -N31 -I"." -I"TCPIPStack" -I"libs" -I"drivers" --warn=0 --runtime=default,+clear,+init,-keep,-download,+stackwarn,-config,+clib,-plib --opt=default,+asm,-asmfile,-speed,+space,9 -D__DEBUG --debugger=pickit2 --cp=16 -Blarge --double=24  --mode=pro -g --asmlist "--errformat=%%f:%%l: error: %%s" "--msgformat=%%f:%%l: advisory: %%s" "--warnformat=%%f:%%l warning: %%s"  
	@echo ${OBJECTDIR}/WeatherXMLDownloader.p1: > ${OBJECTDIR}/WeatherXMLDownloader.p1.d
	@cat ${OBJECTDIR}/WeatherXMLDownloader.dep >> ${OBJECTDIR}/WeatherXMLDownloader.p1.d
	@${FIXDEPS} "${OBJECTDIR}/WeatherXMLDownloader.p1.d" $(SILENT) -ht
	
${OBJECTDIR}/WeatherXMLParser.p1: WeatherXMLParser.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR} 
	${MP_CC} --pass1 WeatherXMLParser.c $(MP_EXTRA_CC_PRE) -q --chip=$(MP_PROCESSOR_OPTION) -P  --outdir=${OBJECTDIR} -N31 -I"." -I"TCPIPStack" -I"libs" -I"drivers" --warn=0 --runtime=default,+clear,+init,-keep,-download,+stackwarn,-config,+clib,-plib --opt=default,+asm,-asmfile,-speed,+space,9 -D__DEBUG --debugger=pickit2 --cp=16 -Blarge --double=24  --mode=pro -g --asmlist "--errformat=%%f:%%l: error: %%s" "--msgformat=%%f:%%l: advisory: %%s" "--warnformat=%%f:%%l warning: %%s"  
	@${MP_CC} --scandep  WeatherXMLParser.c $(MP_EXTRA_CC_PRE) -q --chip=$(MP_PROCESSOR_OPTION) -P  --outdir=${OBJECTDIR} -N31 -I"." -I"TCPIPStack" -I"libs" -I"drivers" --warn=0 --runtime=default,+clear,+init,-keep,-download,+stackwarn,-config,+clib,-plib --opt=default,+asm,-asmfile,-speed,+space,9 -D__DEBUG --debugger=pickit2 --cp=16 -Blarge --double=24  --mode=pro -g --asmlist "--errformat=%%f:%%l: error: %%s" "--msgformat=%%f:%%l: advisory: %%s" "--warnformat=%%f:%%l warning: %%s"  
	@echo ${OBJECTDIR}/WeatherXMLParser.p1: > ${OBJECTDIR}/WeatherXMLParser.p1.d
	@cat ${OBJECTDIR}/WeatherXMLParser.dep >> ${OBJECTDIR}/WeatherXMLParser.p1.d
	@${FIXDEPS} "${OBJECTDIR}/WeatherXMLParser.p1.d" $(SILENT) -ht
	
${OBJECTDIR}/XMLDownloader.p1: XMLDownloader.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR} 
	${MP_CC} --pass1 XMLDownloader.c $(MP_EXTRA_CC_PRE) -q --chip=$(MP_PROCESSOR_OPTION) -P  --outdir=${OBJECTDIR} -N31 -I"." -I"TCPIPStack" -I"libs" -I"drivers" --warn=0 --runtime=default,+clear,+init,-keep,-download,+stackwarn,-config,+clib,-plib --opt=default,+asm,-asmfile,-speed,+space,9 -D__DEBUG --debugger=pickit2 --cp=16 -Blarge --double=24  --mode=pro -g --asmlist "--errformat=%%f:%%l: error: %%s" "--msgformat=%%f:%%l: advisory: %%s" "--warnformat=%%f:%%l warning: %%s"  
	@${MP_CC} --scandep  XMLDownloader.c $(MP_EXTRA_CC_PRE) -q --chip=$(MP_PROCESSOR_OPTION) -P  --outdir=${OBJECTDIR} -N31 -I"." -I"TCPIPStack" -I"libs" -I"drivers" --warn=0 --runtime=default,+clear,+init,-keep,-download,+stackwarn,-config,+clib,-plib --opt=default,+asm,-asmfile,-speed,+space,9 -D__DEBUG --debugger=pickit2 --cp=16 -Blarge --double=24  --mode=pro -g --asmlist "--errformat=%%f:%%l: error: %%s" "--msgformat=%%f:%%l: advisory: %%s" "--warnformat=%%f:%%l warning: %%s"  
	@echo ${OBJECTDIR}/XMLDownloader.p1: > ${OBJECTDIR}/XMLDownloader.p1.d
	@cat ${OBJECTDIR}/XMLDownloader.dep >> ${OBJECTDIR}/XMLDownloader.p1.d
	@${FIXDEPS} "${OBJECTDIR}/XMLDownloader.p1.d" $(SILENT) -ht
	
${OBJECTDIR}/XMLParser.p1: XMLParser.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR} 
	${MP_CC} --pass1 XMLParser.c $(MP_EXTRA_CC_PRE) -q --chip=$(MP_PROCESSOR_OPTION) -P  --outdir=${OBJECTDIR} -N31 -I"." -I"TCPIPStack" -I"libs" -I"drivers" --warn=0 --runtime=default,+clear,+init,-keep,-download,+stackwarn,-config,+clib,-plib --opt=default,+asm,-asmfile,-speed,+space,9 -D__DEBUG --debugger=pickit2 --cp=16 -Blarge --double=24  --mode=pro -g --asmlist "--errformat=%%f:%%l: error: %%s" "--msgformat=%%f:%%l: advisory: %%s" "--warnformat=%%f:%%l warning: %%s"  
	@${MP_CC} --scandep  XMLParser.c $(MP_EXTRA_CC_PRE) -q --chip=$(MP_PROCESSOR_OPTION) -P  --outdir=${OBJECTDIR} -N31 -I"." -I"TCPIPStack" -I"libs" -I"drivers" --warn=0 --runtime=default,+clear,+init,-keep,-download,+stackwarn,-config,+clib,-plib --opt=default,+asm,-asmfile,-speed,+space,9 -D__DEBUG --debugger=pickit2 --cp=16 -Blarge --double=24  --mode=pro -g --asmlist "--errformat=%%f:%%l: error: %%s" "--msgformat=%%f:%%l: advisory: %%s" "--warnformat=%%f:%%l warning: %%s"  
	@echo ${OBJECTDIR}/XMLParser.p1: > ${OBJECTDIR}/XMLParser.p1.d
	@cat ${OBJECTDIR}/XMLParser.dep >> ${OBJECTDIR}/XMLParser.p1.d
	@${FIXDEPS} "${OBJECTDIR}/XMLParser.p1.d" $(SILENT) -ht
	
${OBJECTDIR}/LCDUI.p1: LCDUI.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR} 
	${MP_CC} --pass1 LCDUI.c $(MP_EXTRA_CC_PRE) -q --chip=$(MP_PROCESSOR_OPTION) -P  --outdir=${OBJECTDIR} -N31 -I"." -I"TCPIPStack" -I"libs" -I"drivers" --warn=0 --runtime=default,+clear,+init,-keep,-download,+stackwarn,-config,+clib,-plib --opt=default,+asm,-asmfile,-speed,+space,9 -D__DEBUG --debugger=pickit2 --cp=16 -Blarge --double=24  --mode=pro -g --asmlist "--errformat=%%f:%%l: error: %%s" "--msgformat=%%f:%%l: advisory: %%s" "--warnformat=%%f:%%l warning: %%s"  
	@${MP_CC} --scandep  LCDUI.c $(MP_EXTRA_CC_PRE) -q --chip=$(MP_PROCESSOR_OPTION) -P  --outdir=${OBJECTDIR} -N31 -I"." -I"TCPIPStack" -I"libs" -I"drivers" --warn=0 --runtime=default,+clear,+init,-keep,-download,+stackwarn,-config,+clib,-plib --opt=default,+asm,-asmfile,-speed,+space,9 -D__DEBUG --debugger=pickit2 --cp=16 -Blarge --double=24  --mode=pro -g --asmlist "--errformat=%%f:%%l: error: %%s" "--msgformat=%%f:%%l: advisory: %%s" "--warnformat=%%f:%%l warning: %%s"  
	@echo ${OBJECTDIR}/LCDUI.p1: > ${OBJECTDIR}/LCDUI.p1.d
	@cat ${OBJECTDIR}/LCDUI.dep >> ${OBJECTDIR}/LCDUI.p1.d
	@${FIXDEPS} "${OBJECTDIR}/LCDUI.p1.d" $(SILENT) -ht
	
${OBJECTDIR}/LCDUI_Menu.p1: LCDUI_Menu.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR} 
	${MP_CC} --pass1 LCDUI_Menu.c $(MP_EXTRA_CC_PRE) -q --chip=$(MP_PROCESSOR_OPTION) -P  --outdir=${OBJECTDIR} -N31 -I"." -I"TCPIPStack" -I"libs" -I"drivers" --warn=0 --runtime=default,+clear,+init,-keep,-download,+stackwarn,-config,+clib,-plib --opt=default,+asm,-asmfile,-speed,+space,9 -D__DEBUG --debugger=pickit2 --cp=16 -Blarge --double=24  --mode=pro -g --asmlist "--errformat=%%f:%%l: error: %%s" "--msgformat=%%f:%%l: advisory: %%s" "--warnformat=%%f:%%l warning: %%s"  
	@${MP_CC} --scandep  LCDUI_Menu.c $(MP_EXTRA_CC_PRE) -q --chip=$(MP_PROCESSOR_OPTION) -P  --outdir=${OBJECTDIR} -N31 -I"." -I"TCPIPStack" -I"libs" -I"drivers" --warn=0 --runtime=default,+clear,+init,-keep,-download,+stackwarn,-config,+clib,-plib --opt=default,+asm,-asmfile,-speed,+space,9 -D__DEBUG --debugger=pickit2 --cp=16 -Blarge --double=24  --mode=pro -g --asmlist "--errformat=%%f:%%l: error: %%s" "--msgformat=%%f:%%l: advisory: %%s" "--warnformat=%%f:%%l warning: %%s"  
	@echo ${OBJECTDIR}/LCDUI_Menu.p1: > ${OBJECTDIR}/LCDUI_Menu.p1.d
	@cat ${OBJECTDIR}/LCDUI_Menu.dep >> ${OBJECTDIR}/LCDUI_Menu.p1.d
	@${FIXDEPS} "${OBJECTDIR}/LCDUI_Menu.p1.d" $(SILENT) -ht
	
${OBJECTDIR}/LCDUI_Text.p1: LCDUI_Text.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR} 
	${MP_CC} --pass1 LCDUI_Text.c $(MP_EXTRA_CC_PRE) -q --chip=$(MP_PROCESSOR_OPTION) -P  --outdir=${OBJECTDIR} -N31 -I"." -I"TCPIPStack" -I"libs" -I"drivers" --warn=0 --runtime=default,+clear,+init,-keep,-download,+stackwarn,-config,+clib,-plib --opt=default,+asm,-asmfile,-speed,+space,9 -D__DEBUG --debugger=pickit2 --cp=16 -Blarge --double=24  --mode=pro -g --asmlist "--errformat=%%f:%%l: error: %%s" "--msgformat=%%f:%%l: advisory: %%s" "--warnformat=%%f:%%l warning: %%s"  
	@${MP_CC} --scandep  LCDUI_Text.c $(MP_EXTRA_CC_PRE) -q --chip=$(MP_PROCESSOR_OPTION) -P  --outdir=${OBJECTDIR} -N31 -I"." -I"TCPIPStack" -I"libs" -I"drivers" --warn=0 --runtime=default,+clear,+init,-keep,-download,+stackwarn,-config,+clib,-plib --opt=default,+asm,-asmfile,-speed,+space,9 -D__DEBUG --debugger=pickit2 --cp=16 -Blarge --double=24  --mode=pro -g --asmlist "--errformat=%%f:%%l: error: %%s" "--msgformat=%%f:%%l: advisory: %%s" "--warnformat=%%f:%%l warning: %%s"  
	@echo ${OBJECTDIR}/LCDUI_Text.p1: > ${OBJECTDIR}/LCDUI_Text.p1.d
	@cat ${OBJECTDIR}/LCDUI_Text.dep >> ${OBJECTDIR}/LCDUI_Text.p1.d
	@${FIXDEPS} "${OBJECTDIR}/LCDUI_Text.p1.d" $(SILENT) -ht
	
${OBJECTDIR}/LCDUI_NumberDialog.p1: LCDUI_NumberDialog.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR} 
	${MP_CC} --pass1 LCDUI_NumberDialog.c $(MP_EXTRA_CC_PRE) -q --chip=$(MP_PROCESSOR_OPTION) -P  --outdir=${OBJECTDIR} -N31 -I"." -I"TCPIPStack" -I"libs" -I"drivers" --warn=0 --runtime=default,+clear,+init,-keep,-download,+stackwarn,-config,+clib,-plib --opt=default,+asm,-asmfile,-speed,+space,9 -D__DEBUG --debugger=pickit2 --cp=16 -Blarge --double=24  --mode=pro -g --asmlist "--errformat=%%f:%%l: error: %%s" "--msgformat=%%f:%%l: advisory: %%s" "--warnformat=%%f:%%l warning: %%s"  
	@${MP_CC} --scandep  LCDUI_NumberDialog.c $(MP_EXTRA_CC_PRE) -q --chip=$(MP_PROCESSOR_OPTION) -P  --outdir=${OBJECTDIR} -N31 -I"." -I"TCPIPStack" -I"libs" -I"drivers" --warn=0 --runtime=default,+clear,+init,-keep,-download,+stackwarn,-config,+clib,-plib --opt=default,+asm,-asmfile,-speed,+space,9 -D__DEBUG --debugger=pickit2 --cp=16 -Blarge --double=24  --mode=pro -g --asmlist "--errformat=%%f:%%l: error: %%s" "--msgformat=%%f:%%l: advisory: %%s" "--warnformat=%%f:%%l warning: %%s"  
	@echo ${OBJECTDIR}/LCDUI_NumberDialog.p1: > ${OBJECTDIR}/LCDUI_NumberDialog.p1.d
	@cat ${OBJECTDIR}/LCDUI_NumberDialog.dep >> ${OBJECTDIR}/LCDUI_NumberDialog.p1.d
	@${FIXDEPS} "${OBJECTDIR}/LCDUI_NumberDialog.p1.d" $(SILENT) -ht
	
${OBJECTDIR}/PIC.p1: PIC.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR} 
	${MP_CC} --pass1 PIC.c $(MP_EXTRA_CC_PRE) -q --chip=$(MP_PROCESSOR_OPTION) -P  --outdir=${OBJECTDIR} -N31 -I"." -I"TCPIPStack" -I"libs" -I"drivers" --warn=0 --runtime=default,+clear,+init,-keep,-download,+stackwarn,-config,+clib,-plib --opt=default,+asm,-asmfile,-speed,+space,9 -D__DEBUG --debugger=pickit2 --cp=16 -Blarge --double=24  --mode=pro -g --asmlist "--errformat=%%f:%%l: error: %%s" "--msgformat=%%f:%%l: advisory: %%s" "--warnformat=%%f:%%l warning: %%s"  
	@${MP_CC} --scandep  PIC.c $(MP_EXTRA_CC_PRE) -q --chip=$(MP_PROCESSOR_OPTION) -P  --outdir=${OBJECTDIR} -N31 -I"." -I"TCPIPStack" -I"libs" -I"drivers" --warn=0 --runtime=default,+clear,+init,-keep,-download,+stackwarn,-config,+clib,-plib --opt=default,+asm,-asmfile,-speed,+space,9 -D__DEBUG --debugger=pickit2 --cp=16 -Blarge --double=24  --mode=pro -g --asmlist "--errformat=%%f:%%l: error: %%s" "--msgformat=%%f:%%l: advisory: %%s" "--warnformat=%%f:%%l warning: %%s"  
	@echo ${OBJECTDIR}/PIC.p1: > ${OBJECTDIR}/PIC.p1.d
	@cat ${OBJECTDIR}/PIC.dep >> ${OBJECTDIR}/PIC.p1.d
	@${FIXDEPS} "${OBJECTDIR}/PIC.p1.d" $(SILENT) -ht
	
else
${OBJECTDIR}/drivers/25LC1024.p1: drivers/25LC1024.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/drivers 
	${MP_CC} --pass1 drivers/25LC1024.c $(MP_EXTRA_CC_PRE) -q --chip=$(MP_PROCESSOR_OPTION) -P  --outdir=${OBJECTDIR}/drivers -N31 -I"." -I"TCPIPStack" -I"libs" -I"drivers" --warn=0 --runtime=default,+clear,+init,-keep,-download,+stackwarn,-config,+clib,-plib --opt=default,+asm,-asmfile,-speed,+space,9 --cp=16 -Blarge --double=24  --mode=pro -g --asmlist "--errformat=%%f:%%l: error: %%s" "--msgformat=%%f:%%l: advisory: %%s" "--warnformat=%%f:%%l warning: %%s"  
	@${MP_CC} --scandep  drivers/25LC1024.c $(MP_EXTRA_CC_PRE) -q --chip=$(MP_PROCESSOR_OPTION) -P  --outdir=${OBJECTDIR}/drivers -N31 -I"." -I"TCPIPStack" -I"libs" -I"drivers" --warn=0 --runtime=default,+clear,+init,-keep,-download,+stackwarn,-config,+clib,-plib --opt=default,+asm,-asmfile,-speed,+space,9 --cp=16 -Blarge --double=24  --mode=pro -g --asmlist "--errformat=%%f:%%l: error: %%s" "--msgformat=%%f:%%l: advisory: %%s" "--warnformat=%%f:%%l warning: %%s"  
	@echo ${OBJECTDIR}/drivers/25LC1024.p1: > ${OBJECTDIR}/drivers/25LC1024.p1.d
	@cat ${OBJECTDIR}/drivers/25LC1024.dep >> ${OBJECTDIR}/drivers/25LC1024.p1.d
	@${FIXDEPS} "${OBJECTDIR}/drivers/25LC1024.p1.d" $(SILENT) -ht
	
${OBJECTDIR}/drivers/KeyBrd.p1: drivers/KeyBrd.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/drivers 
	${MP_CC} --pass1 drivers/KeyBrd.c $(MP_EXTRA_CC_PRE) -q --chip=$(MP_PROCESSOR_OPTION) -P  --outdir=${OBJECTDIR}/drivers -N31 -I"." -I"TCPIPStack" -I"libs" -I"drivers" --warn=0 --runtime=default,+clear,+init,-keep,-download,+stackwarn,-config,+clib,-plib --opt=default,+asm,-asmfile,-speed,+space,9 --cp=16 -Blarge --double=24  --mode=pro -g --asmlist "--errformat=%%f:%%l: error: %%s" "--msgformat=%%f:%%l: advisory: %%s" "--warnformat=%%f:%%l warning: %%s"  
	@${MP_CC} --scandep  drivers/KeyBrd.c $(MP_EXTRA_CC_PRE) -q --chip=$(MP_PROCESSOR_OPTION) -P  --outdir=${OBJECTDIR}/drivers -N31 -I"." -I"TCPIPStack" -I"libs" -I"drivers" --warn=0 --runtime=default,+clear,+init,-keep,-download,+stackwarn,-config,+clib,-plib --opt=default,+asm,-asmfile,-speed,+space,9 --cp=16 -Blarge --double=24  --mode=pro -g --asmlist "--errformat=%%f:%%l: error: %%s" "--msgformat=%%f:%%l: advisory: %%s" "--warnformat=%%f:%%l warning: %%s"  
	@echo ${OBJECTDIR}/drivers/KeyBrd.p1: > ${OBJECTDIR}/drivers/KeyBrd.p1.d
	@cat ${OBJECTDIR}/drivers/KeyBrd.dep >> ${OBJECTDIR}/drivers/KeyBrd.p1.d
	@${FIXDEPS} "${OBJECTDIR}/drivers/KeyBrd.p1.d" $(SILENT) -ht
	
${OBJECTDIR}/drivers/SRAM_23K.p1: drivers/SRAM_23K.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/drivers 
	${MP_CC} --pass1 drivers/SRAM_23K.c $(MP_EXTRA_CC_PRE) -q --chip=$(MP_PROCESSOR_OPTION) -P  --outdir=${OBJECTDIR}/drivers -N31 -I"." -I"TCPIPStack" -I"libs" -I"drivers" --warn=0 --runtime=default,+clear,+init,-keep,-download,+stackwarn,-config,+clib,-plib --opt=default,+asm,-asmfile,-speed,+space,9 --cp=16 -Blarge --double=24  --mode=pro -g --asmlist "--errformat=%%f:%%l: error: %%s" "--msgformat=%%f:%%l: advisory: %%s" "--warnformat=%%f:%%l warning: %%s"  
	@${MP_CC} --scandep  drivers/SRAM_23K.c $(MP_EXTRA_CC_PRE) -q --chip=$(MP_PROCESSOR_OPTION) -P  --outdir=${OBJECTDIR}/drivers -N31 -I"." -I"TCPIPStack" -I"libs" -I"drivers" --warn=0 --runtime=default,+clear,+init,-keep,-download,+stackwarn,-config,+clib,-plib --opt=default,+asm,-asmfile,-speed,+space,9 --cp=16 -Blarge --double=24  --mode=pro -g --asmlist "--errformat=%%f:%%l: error: %%s" "--msgformat=%%f:%%l: advisory: %%s" "--warnformat=%%f:%%l warning: %%s"  
	@echo ${OBJECTDIR}/drivers/SRAM_23K.p1: > ${OBJECTDIR}/drivers/SRAM_23K.p1.d
	@cat ${OBJECTDIR}/drivers/SRAM_23K.dep >> ${OBJECTDIR}/drivers/SRAM_23K.p1.d
	@${FIXDEPS} "${OBJECTDIR}/drivers/SRAM_23K.p1.d" $(SILENT) -ht
	
${OBJECTDIR}/drivers/graphlcd.p1: drivers/graphlcd.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/drivers 
	${MP_CC} --pass1 drivers/graphlcd.c $(MP_EXTRA_CC_PRE) -q --chip=$(MP_PROCESSOR_OPTION) -P  --outdir=${OBJECTDIR}/drivers -N31 -I"." -I"TCPIPStack" -I"libs" -I"drivers" --warn=0 --runtime=default,+clear,+init,-keep,-download,+stackwarn,-config,+clib,-plib --opt=default,+asm,-asmfile,-speed,+space,9 --cp=16 -Blarge --double=24  --mode=pro -g --asmlist "--errformat=%%f:%%l: error: %%s" "--msgformat=%%f:%%l: advisory: %%s" "--warnformat=%%f:%%l warning: %%s"  
	@${MP_CC} --scandep  drivers/graphlcd.c $(MP_EXTRA_CC_PRE) -q --chip=$(MP_PROCESSOR_OPTION) -P  --outdir=${OBJECTDIR}/drivers -N31 -I"." -I"TCPIPStack" -I"libs" -I"drivers" --warn=0 --runtime=default,+clear,+init,-keep,-download,+stackwarn,-config,+clib,-plib --opt=default,+asm,-asmfile,-speed,+space,9 --cp=16 -Blarge --double=24  --mode=pro -g --asmlist "--errformat=%%f:%%l: error: %%s" "--msgformat=%%f:%%l: advisory: %%s" "--warnformat=%%f:%%l warning: %%s"  
	@echo ${OBJECTDIR}/drivers/graphlcd.p1: > ${OBJECTDIR}/drivers/graphlcd.p1.d
	@cat ${OBJECTDIR}/drivers/graphlcd.dep >> ${OBJECTDIR}/drivers/graphlcd.p1.d
	@${FIXDEPS} "${OBJECTDIR}/drivers/graphlcd.p1.d" $(SILENT) -ht
	
${OBJECTDIR}/drivers/pcd8544.p1: drivers/pcd8544.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/drivers 
	${MP_CC} --pass1 drivers/pcd8544.c $(MP_EXTRA_CC_PRE) -q --chip=$(MP_PROCESSOR_OPTION) -P  --outdir=${OBJECTDIR}/drivers -N31 -I"." -I"TCPIPStack" -I"libs" -I"drivers" --warn=0 --runtime=default,+clear,+init,-keep,-download,+stackwarn,-config,+clib,-plib --opt=default,+asm,-asmfile,-speed,+space,9 --cp=16 -Blarge --double=24  --mode=pro -g --asmlist "--errformat=%%f:%%l: error: %%s" "--msgformat=%%f:%%l: advisory: %%s" "--warnformat=%%f:%%l warning: %%s"  
	@${MP_CC} --scandep  drivers/pcd8544.c $(MP_EXTRA_CC_PRE) -q --chip=$(MP_PROCESSOR_OPTION) -P  --outdir=${OBJECTDIR}/drivers -N31 -I"." -I"TCPIPStack" -I"libs" -I"drivers" --warn=0 --runtime=default,+clear,+init,-keep,-download,+stackwarn,-config,+clib,-plib --opt=default,+asm,-asmfile,-speed,+space,9 --cp=16 -Blarge --double=24  --mode=pro -g --asmlist "--errformat=%%f:%%l: error: %%s" "--msgformat=%%f:%%l: advisory: %%s" "--warnformat=%%f:%%l warning: %%s"  
	@echo ${OBJECTDIR}/drivers/pcd8544.p1: > ${OBJECTDIR}/drivers/pcd8544.p1.d
	@cat ${OBJECTDIR}/drivers/pcd8544.dep >> ${OBJECTDIR}/drivers/pcd8544.p1.d
	@${FIXDEPS} "${OBJECTDIR}/drivers/pcd8544.p1.d" $(SILENT) -ht
	
${OBJECTDIR}/drivers/t6963c.p1: drivers/t6963c.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/drivers 
	${MP_CC} --pass1 drivers/t6963c.c $(MP_EXTRA_CC_PRE) -q --chip=$(MP_PROCESSOR_OPTION) -P  --outdir=${OBJECTDIR}/drivers -N31 -I"." -I"TCPIPStack" -I"libs" -I"drivers" --warn=0 --runtime=default,+clear,+init,-keep,-download,+stackwarn,-config,+clib,-plib --opt=default,+asm,-asmfile,-speed,+space,9 --cp=16 -Blarge --double=24  --mode=pro -g --asmlist "--errformat=%%f:%%l: error: %%s" "--msgformat=%%f:%%l: advisory: %%s" "--warnformat=%%f:%%l warning: %%s"  
	@${MP_CC} --scandep  drivers/t6963c.c $(MP_EXTRA_CC_PRE) -q --chip=$(MP_PROCESSOR_OPTION) -P  --outdir=${OBJECTDIR}/drivers -N31 -I"." -I"TCPIPStack" -I"libs" -I"drivers" --warn=0 --runtime=default,+clear,+init,-keep,-download,+stackwarn,-config,+clib,-plib --opt=default,+asm,-asmfile,-speed,+space,9 --cp=16 -Blarge --double=24  --mode=pro -g --asmlist "--errformat=%%f:%%l: error: %%s" "--msgformat=%%f:%%l: advisory: %%s" "--warnformat=%%f:%%l warning: %%s"  
	@echo ${OBJECTDIR}/drivers/t6963c.p1: > ${OBJECTDIR}/drivers/t6963c.p1.d
	@cat ${OBJECTDIR}/drivers/t6963c.dep >> ${OBJECTDIR}/drivers/t6963c.p1.d
	@${FIXDEPS} "${OBJECTDIR}/drivers/t6963c.p1.d" $(SILENT) -ht
	
${OBJECTDIR}/libs/CRC16.p1: libs/CRC16.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/libs 
	${MP_CC} --pass1 libs/CRC16.c $(MP_EXTRA_CC_PRE) -q --chip=$(MP_PROCESSOR_OPTION) -P  --outdir=${OBJECTDIR}/libs -N31 -I"." -I"TCPIPStack" -I"libs" -I"drivers" --warn=0 --runtime=default,+clear,+init,-keep,-download,+stackwarn,-config,+clib,-plib --opt=default,+asm,-asmfile,-speed,+space,9 --cp=16 -Blarge --double=24  --mode=pro -g --asmlist "--errformat=%%f:%%l: error: %%s" "--msgformat=%%f:%%l: advisory: %%s" "--warnformat=%%f:%%l warning: %%s"  
	@${MP_CC} --scandep  libs/CRC16.c $(MP_EXTRA_CC_PRE) -q --chip=$(MP_PROCESSOR_OPTION) -P  --outdir=${OBJECTDIR}/libs -N31 -I"." -I"TCPIPStack" -I"libs" -I"drivers" --warn=0 --runtime=default,+clear,+init,-keep,-download,+stackwarn,-config,+clib,-plib --opt=default,+asm,-asmfile,-speed,+space,9 --cp=16 -Blarge --double=24  --mode=pro -g --asmlist "--errformat=%%f:%%l: error: %%s" "--msgformat=%%f:%%l: advisory: %%s" "--warnformat=%%f:%%l warning: %%s"  
	@echo ${OBJECTDIR}/libs/CRC16.p1: > ${OBJECTDIR}/libs/CRC16.p1.d
	@cat ${OBJECTDIR}/libs/CRC16.dep >> ${OBJECTDIR}/libs/CRC16.p1.d
	@${FIXDEPS} "${OBJECTDIR}/libs/CRC16.p1.d" $(SILENT) -ht
	
${OBJECTDIR}/libs/PWM.p1: libs/PWM.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/libs 
	${MP_CC} --pass1 libs/PWM.c $(MP_EXTRA_CC_PRE) -q --chip=$(MP_PROCESSOR_OPTION) -P  --outdir=${OBJECTDIR}/libs -N31 -I"." -I"TCPIPStack" -I"libs" -I"drivers" --warn=0 --runtime=default,+clear,+init,-keep,-download,+stackwarn,-config,+clib,-plib --opt=default,+asm,-asmfile,-speed,+space,9 --cp=16 -Blarge --double=24  --mode=pro -g --asmlist "--errformat=%%f:%%l: error: %%s" "--msgformat=%%f:%%l: advisory: %%s" "--warnformat=%%f:%%l warning: %%s"  
	@${MP_CC} --scandep  libs/PWM.c $(MP_EXTRA_CC_PRE) -q --chip=$(MP_PROCESSOR_OPTION) -P  --outdir=${OBJECTDIR}/libs -N31 -I"." -I"TCPIPStack" -I"libs" -I"drivers" --warn=0 --runtime=default,+clear,+init,-keep,-download,+stackwarn,-config,+clib,-plib --opt=default,+asm,-asmfile,-speed,+space,9 --cp=16 -Blarge --double=24  --mode=pro -g --asmlist "--errformat=%%f:%%l: error: %%s" "--msgformat=%%f:%%l: advisory: %%s" "--warnformat=%%f:%%l warning: %%s"  
	@echo ${OBJECTDIR}/libs/PWM.p1: > ${OBJECTDIR}/libs/PWM.p1.d
	@cat ${OBJECTDIR}/libs/PWM.dep >> ${OBJECTDIR}/libs/PWM.p1.d
	@${FIXDEPS} "${OBJECTDIR}/libs/PWM.p1.d" $(SILENT) -ht
	
${OBJECTDIR}/TCPIPStack/ARP.p1: TCPIPStack/ARP.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/TCPIPStack 
	${MP_CC} --pass1 TCPIPStack/ARP.c $(MP_EXTRA_CC_PRE) -q --chip=$(MP_PROCESSOR_OPTION) -P  --outdir=${OBJECTDIR}/TCPIPStack -N31 -I"." -I"TCPIPStack" -I"libs" -I"drivers" --warn=0 --runtime=default,+clear,+init,-keep,-download,+stackwarn,-config,+clib,-plib --opt=default,+asm,-asmfile,-speed,+space,9 --cp=16 -Blarge --double=24  --mode=pro -g --asmlist "--errformat=%%f:%%l: error: %%s" "--msgformat=%%f:%%l: advisory: %%s" "--warnformat=%%f:%%l warning: %%s"  
	@${MP_CC} --scandep  TCPIPStack/ARP.c $(MP_EXTRA_CC_PRE) -q --chip=$(MP_PROCESSOR_OPTION) -P  --outdir=${OBJECTDIR}/TCPIPStack -N31 -I"." -I"TCPIPStack" -I"libs" -I"drivers" --warn=0 --runtime=default,+clear,+init,-keep,-download,+stackwarn,-config,+clib,-plib --opt=default,+asm,-asmfile,-speed,+space,9 --cp=16 -Blarge --double=24  --mode=pro -g --asmlist "--errformat=%%f:%%l: error: %%s" "--msgformat=%%f:%%l: advisory: %%s" "--warnformat=%%f:%%l warning: %%s"  
	@echo ${OBJECTDIR}/TCPIPStack/ARP.p1: > ${OBJECTDIR}/TCPIPStack/ARP.p1.d
	@cat ${OBJECTDIR}/TCPIPStack/ARP.dep >> ${OBJECTDIR}/TCPIPStack/ARP.p1.d
	@${FIXDEPS} "${OBJECTDIR}/TCPIPStack/ARP.p1.d" $(SILENT) -ht
	
${OBJECTDIR}/TCPIPStack/BigInt.p1: TCPIPStack/BigInt.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/TCPIPStack 
	${MP_CC} --pass1 TCPIPStack/BigInt.c $(MP_EXTRA_CC_PRE) -q --chip=$(MP_PROCESSOR_OPTION) -P  --outdir=${OBJECTDIR}/TCPIPStack -N31 -I"." -I"TCPIPStack" -I"libs" -I"drivers" --warn=0 --runtime=default,+clear,+init,-keep,-download,+stackwarn,-config,+clib,-plib --opt=default,+asm,-asmfile,-speed,+space,9 --cp=16 -Blarge --double=24  --mode=pro -g --asmlist "--errformat=%%f:%%l: error: %%s" "--msgformat=%%f:%%l: advisory: %%s" "--warnformat=%%f:%%l warning: %%s"  
	@${MP_CC} --scandep  TCPIPStack/BigInt.c $(MP_EXTRA_CC_PRE) -q --chip=$(MP_PROCESSOR_OPTION) -P  --outdir=${OBJECTDIR}/TCPIPStack -N31 -I"." -I"TCPIPStack" -I"libs" -I"drivers" --warn=0 --runtime=default,+clear,+init,-keep,-download,+stackwarn,-config,+clib,-plib --opt=default,+asm,-asmfile,-speed,+space,9 --cp=16 -Blarge --double=24  --mode=pro -g --asmlist "--errformat=%%f:%%l: error: %%s" "--msgformat=%%f:%%l: advisory: %%s" "--warnformat=%%f:%%l warning: %%s"  
	@echo ${OBJECTDIR}/TCPIPStack/BigInt.p1: > ${OBJECTDIR}/TCPIPStack/BigInt.p1.d
	@cat ${OBJECTDIR}/TCPIPStack/BigInt.dep >> ${OBJECTDIR}/TCPIPStack/BigInt.p1.d
	@${FIXDEPS} "${OBJECTDIR}/TCPIPStack/BigInt.p1.d" $(SILENT) -ht
	
${OBJECTDIR}/TCPIPStack/DHCP.p1: TCPIPStack/DHCP.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/TCPIPStack 
	${MP_CC} --pass1 TCPIPStack/DHCP.c $(MP_EXTRA_CC_PRE) -q --chip=$(MP_PROCESSOR_OPTION) -P  --outdir=${OBJECTDIR}/TCPIPStack -N31 -I"." -I"TCPIPStack" -I"libs" -I"drivers" --warn=0 --runtime=default,+clear,+init,-keep,-download,+stackwarn,-config,+clib,-plib --opt=default,+asm,-asmfile,-speed,+space,9 --cp=16 -Blarge --double=24  --mode=pro -g --asmlist "--errformat=%%f:%%l: error: %%s" "--msgformat=%%f:%%l: advisory: %%s" "--warnformat=%%f:%%l warning: %%s"  
	@${MP_CC} --scandep  TCPIPStack/DHCP.c $(MP_EXTRA_CC_PRE) -q --chip=$(MP_PROCESSOR_OPTION) -P  --outdir=${OBJECTDIR}/TCPIPStack -N31 -I"." -I"TCPIPStack" -I"libs" -I"drivers" --warn=0 --runtime=default,+clear,+init,-keep,-download,+stackwarn,-config,+clib,-plib --opt=default,+asm,-asmfile,-speed,+space,9 --cp=16 -Blarge --double=24  --mode=pro -g --asmlist "--errformat=%%f:%%l: error: %%s" "--msgformat=%%f:%%l: advisory: %%s" "--warnformat=%%f:%%l warning: %%s"  
	@echo ${OBJECTDIR}/TCPIPStack/DHCP.p1: > ${OBJECTDIR}/TCPIPStack/DHCP.p1.d
	@cat ${OBJECTDIR}/TCPIPStack/DHCP.dep >> ${OBJECTDIR}/TCPIPStack/DHCP.p1.d
	@${FIXDEPS} "${OBJECTDIR}/TCPIPStack/DHCP.p1.d" $(SILENT) -ht
	
${OBJECTDIR}/TCPIPStack/DNS.p1: TCPIPStack/DNS.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/TCPIPStack 
	${MP_CC} --pass1 TCPIPStack/DNS.c $(MP_EXTRA_CC_PRE) -q --chip=$(MP_PROCESSOR_OPTION) -P  --outdir=${OBJECTDIR}/TCPIPStack -N31 -I"." -I"TCPIPStack" -I"libs" -I"drivers" --warn=0 --runtime=default,+clear,+init,-keep,-download,+stackwarn,-config,+clib,-plib --opt=default,+asm,-asmfile,-speed,+space,9 --cp=16 -Blarge --double=24  --mode=pro -g --asmlist "--errformat=%%f:%%l: error: %%s" "--msgformat=%%f:%%l: advisory: %%s" "--warnformat=%%f:%%l warning: %%s"  
	@${MP_CC} --scandep  TCPIPStack/DNS.c $(MP_EXTRA_CC_PRE) -q --chip=$(MP_PROCESSOR_OPTION) -P  --outdir=${OBJECTDIR}/TCPIPStack -N31 -I"." -I"TCPIPStack" -I"libs" -I"drivers" --warn=0 --runtime=default,+clear,+init,-keep,-download,+stackwarn,-config,+clib,-plib --opt=default,+asm,-asmfile,-speed,+space,9 --cp=16 -Blarge --double=24  --mode=pro -g --asmlist "--errformat=%%f:%%l: error: %%s" "--msgformat=%%f:%%l: advisory: %%s" "--warnformat=%%f:%%l warning: %%s"  
	@echo ${OBJECTDIR}/TCPIPStack/DNS.p1: > ${OBJECTDIR}/TCPIPStack/DNS.p1.d
	@cat ${OBJECTDIR}/TCPIPStack/DNS.dep >> ${OBJECTDIR}/TCPIPStack/DNS.p1.d
	@${FIXDEPS} "${OBJECTDIR}/TCPIPStack/DNS.p1.d" $(SILENT) -ht
	
${OBJECTDIR}/TCPIPStack/Delay.p1: TCPIPStack/Delay.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/TCPIPStack 
	${MP_CC} --pass1 TCPIPStack/Delay.c $(MP_EXTRA_CC_PRE) -q --chip=$(MP_PROCESSOR_OPTION) -P  --outdir=${OBJECTDIR}/TCPIPStack -N31 -I"." -I"TCPIPStack" -I"libs" -I"drivers" --warn=0 --runtime=default,+clear,+init,-keep,-download,+stackwarn,-config,+clib,-plib --opt=default,+asm,-asmfile,-speed,+space,9 --cp=16 -Blarge --double=24  --mode=pro -g --asmlist "--errformat=%%f:%%l: error: %%s" "--msgformat=%%f:%%l: advisory: %%s" "--warnformat=%%f:%%l warning: %%s"  
	@${MP_CC} --scandep  TCPIPStack/Delay.c $(MP_EXTRA_CC_PRE) -q --chip=$(MP_PROCESSOR_OPTION) -P  --outdir=${OBJECTDIR}/TCPIPStack -N31 -I"." -I"TCPIPStack" -I"libs" -I"drivers" --warn=0 --runtime=default,+clear,+init,-keep,-download,+stackwarn,-config,+clib,-plib --opt=default,+asm,-asmfile,-speed,+space,9 --cp=16 -Blarge --double=24  --mode=pro -g --asmlist "--errformat=%%f:%%l: error: %%s" "--msgformat=%%f:%%l: advisory: %%s" "--warnformat=%%f:%%l warning: %%s"  
	@echo ${OBJECTDIR}/TCPIPStack/Delay.p1: > ${OBJECTDIR}/TCPIPStack/Delay.p1.d
	@cat ${OBJECTDIR}/TCPIPStack/Delay.dep >> ${OBJECTDIR}/TCPIPStack/Delay.p1.d
	@${FIXDEPS} "${OBJECTDIR}/TCPIPStack/Delay.p1.d" $(SILENT) -ht
	
${OBJECTDIR}/TCPIPStack/ENC28J60.p1: TCPIPStack/ENC28J60.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/TCPIPStack 
	${MP_CC} --pass1 TCPIPStack/ENC28J60.c $(MP_EXTRA_CC_PRE) -q --chip=$(MP_PROCESSOR_OPTION) -P  --outdir=${OBJECTDIR}/TCPIPStack -N31 -I"." -I"TCPIPStack" -I"libs" -I"drivers" --warn=0 --runtime=default,+clear,+init,-keep,-download,+stackwarn,-config,+clib,-plib --opt=default,+asm,-asmfile,-speed,+space,9 --cp=16 -Blarge --double=24  --mode=pro -g --asmlist "--errformat=%%f:%%l: error: %%s" "--msgformat=%%f:%%l: advisory: %%s" "--warnformat=%%f:%%l warning: %%s"  
	@${MP_CC} --scandep  TCPIPStack/ENC28J60.c $(MP_EXTRA_CC_PRE) -q --chip=$(MP_PROCESSOR_OPTION) -P  --outdir=${OBJECTDIR}/TCPIPStack -N31 -I"." -I"TCPIPStack" -I"libs" -I"drivers" --warn=0 --runtime=default,+clear,+init,-keep,-download,+stackwarn,-config,+clib,-plib --opt=default,+asm,-asmfile,-speed,+space,9 --cp=16 -Blarge --double=24  --mode=pro -g --asmlist "--errformat=%%f:%%l: error: %%s" "--msgformat=%%f:%%l: advisory: %%s" "--warnformat=%%f:%%l warning: %%s"  
	@echo ${OBJECTDIR}/TCPIPStack/ENC28J60.p1: > ${OBJECTDIR}/TCPIPStack/ENC28J60.p1.d
	@cat ${OBJECTDIR}/TCPIPStack/ENC28J60.dep >> ${OBJECTDIR}/TCPIPStack/ENC28J60.p1.d
	@${FIXDEPS} "${OBJECTDIR}/TCPIPStack/ENC28J60.p1.d" $(SILENT) -ht
	
${OBJECTDIR}/TCPIPStack/Hashes.p1: TCPIPStack/Hashes.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/TCPIPStack 
	${MP_CC} --pass1 TCPIPStack/Hashes.c $(MP_EXTRA_CC_PRE) -q --chip=$(MP_PROCESSOR_OPTION) -P  --outdir=${OBJECTDIR}/TCPIPStack -N31 -I"." -I"TCPIPStack" -I"libs" -I"drivers" --warn=0 --runtime=default,+clear,+init,-keep,-download,+stackwarn,-config,+clib,-plib --opt=default,+asm,-asmfile,-speed,+space,9 --cp=16 -Blarge --double=24  --mode=pro -g --asmlist "--errformat=%%f:%%l: error: %%s" "--msgformat=%%f:%%l: advisory: %%s" "--warnformat=%%f:%%l warning: %%s"  
	@${MP_CC} --scandep  TCPIPStack/Hashes.c $(MP_EXTRA_CC_PRE) -q --chip=$(MP_PROCESSOR_OPTION) -P  --outdir=${OBJECTDIR}/TCPIPStack -N31 -I"." -I"TCPIPStack" -I"libs" -I"drivers" --warn=0 --runtime=default,+clear,+init,-keep,-download,+stackwarn,-config,+clib,-plib --opt=default,+asm,-asmfile,-speed,+space,9 --cp=16 -Blarge --double=24  --mode=pro -g --asmlist "--errformat=%%f:%%l: error: %%s" "--msgformat=%%f:%%l: advisory: %%s" "--warnformat=%%f:%%l warning: %%s"  
	@echo ${OBJECTDIR}/TCPIPStack/Hashes.p1: > ${OBJECTDIR}/TCPIPStack/Hashes.p1.d
	@cat ${OBJECTDIR}/TCPIPStack/Hashes.dep >> ${OBJECTDIR}/TCPIPStack/Hashes.p1.d
	@${FIXDEPS} "${OBJECTDIR}/TCPIPStack/Hashes.p1.d" $(SILENT) -ht
	
${OBJECTDIR}/TCPIPStack/Helpers.p1: TCPIPStack/Helpers.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/TCPIPStack 
	${MP_CC} --pass1 TCPIPStack/Helpers.c $(MP_EXTRA_CC_PRE) -q --chip=$(MP_PROCESSOR_OPTION) -P  --outdir=${OBJECTDIR}/TCPIPStack -N31 -I"." -I"TCPIPStack" -I"libs" -I"drivers" --warn=0 --runtime=default,+clear,+init,-keep,-download,+stackwarn,-config,+clib,-plib --opt=default,+asm,-asmfile,-speed,+space,9 --cp=16 -Blarge --double=24  --mode=pro -g --asmlist "--errformat=%%f:%%l: error: %%s" "--msgformat=%%f:%%l: advisory: %%s" "--warnformat=%%f:%%l warning: %%s"  
	@${MP_CC} --scandep  TCPIPStack/Helpers.c $(MP_EXTRA_CC_PRE) -q --chip=$(MP_PROCESSOR_OPTION) -P  --outdir=${OBJECTDIR}/TCPIPStack -N31 -I"." -I"TCPIPStack" -I"libs" -I"drivers" --warn=0 --runtime=default,+clear,+init,-keep,-download,+stackwarn,-config,+clib,-plib --opt=default,+asm,-asmfile,-speed,+space,9 --cp=16 -Blarge --double=24  --mode=pro -g --asmlist "--errformat=%%f:%%l: error: %%s" "--msgformat=%%f:%%l: advisory: %%s" "--warnformat=%%f:%%l warning: %%s"  
	@echo ${OBJECTDIR}/TCPIPStack/Helpers.p1: > ${OBJECTDIR}/TCPIPStack/Helpers.p1.d
	@cat ${OBJECTDIR}/TCPIPStack/Helpers.dep >> ${OBJECTDIR}/TCPIPStack/Helpers.p1.d
	@${FIXDEPS} "${OBJECTDIR}/TCPIPStack/Helpers.p1.d" $(SILENT) -ht
	
${OBJECTDIR}/TCPIPStack/ICMP.p1: TCPIPStack/ICMP.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/TCPIPStack 
	${MP_CC} --pass1 TCPIPStack/ICMP.c $(MP_EXTRA_CC_PRE) -q --chip=$(MP_PROCESSOR_OPTION) -P  --outdir=${OBJECTDIR}/TCPIPStack -N31 -I"." -I"TCPIPStack" -I"libs" -I"drivers" --warn=0 --runtime=default,+clear,+init,-keep,-download,+stackwarn,-config,+clib,-plib --opt=default,+asm,-asmfile,-speed,+space,9 --cp=16 -Blarge --double=24  --mode=pro -g --asmlist "--errformat=%%f:%%l: error: %%s" "--msgformat=%%f:%%l: advisory: %%s" "--warnformat=%%f:%%l warning: %%s"  
	@${MP_CC} --scandep  TCPIPStack/ICMP.c $(MP_EXTRA_CC_PRE) -q --chip=$(MP_PROCESSOR_OPTION) -P  --outdir=${OBJECTDIR}/TCPIPStack -N31 -I"." -I"TCPIPStack" -I"libs" -I"drivers" --warn=0 --runtime=default,+clear,+init,-keep,-download,+stackwarn,-config,+clib,-plib --opt=default,+asm,-asmfile,-speed,+space,9 --cp=16 -Blarge --double=24  --mode=pro -g --asmlist "--errformat=%%f:%%l: error: %%s" "--msgformat=%%f:%%l: advisory: %%s" "--warnformat=%%f:%%l warning: %%s"  
	@echo ${OBJECTDIR}/TCPIPStack/ICMP.p1: > ${OBJECTDIR}/TCPIPStack/ICMP.p1.d
	@cat ${OBJECTDIR}/TCPIPStack/ICMP.dep >> ${OBJECTDIR}/TCPIPStack/ICMP.p1.d
	@${FIXDEPS} "${OBJECTDIR}/TCPIPStack/ICMP.p1.d" $(SILENT) -ht
	
${OBJECTDIR}/TCPIPStack/IP.p1: TCPIPStack/IP.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/TCPIPStack 
	${MP_CC} --pass1 TCPIPStack/IP.c $(MP_EXTRA_CC_PRE) -q --chip=$(MP_PROCESSOR_OPTION) -P  --outdir=${OBJECTDIR}/TCPIPStack -N31 -I"." -I"TCPIPStack" -I"libs" -I"drivers" --warn=0 --runtime=default,+clear,+init,-keep,-download,+stackwarn,-config,+clib,-plib --opt=default,+asm,-asmfile,-speed,+space,9 --cp=16 -Blarge --double=24  --mode=pro -g --asmlist "--errformat=%%f:%%l: error: %%s" "--msgformat=%%f:%%l: advisory: %%s" "--warnformat=%%f:%%l warning: %%s"  
	@${MP_CC} --scandep  TCPIPStack/IP.c $(MP_EXTRA_CC_PRE) -q --chip=$(MP_PROCESSOR_OPTION) -P  --outdir=${OBJECTDIR}/TCPIPStack -N31 -I"." -I"TCPIPStack" -I"libs" -I"drivers" --warn=0 --runtime=default,+clear,+init,-keep,-download,+stackwarn,-config,+clib,-plib --opt=default,+asm,-asmfile,-speed,+space,9 --cp=16 -Blarge --double=24  --mode=pro -g --asmlist "--errformat=%%f:%%l: error: %%s" "--msgformat=%%f:%%l: advisory: %%s" "--warnformat=%%f:%%l warning: %%s"  
	@echo ${OBJECTDIR}/TCPIPStack/IP.p1: > ${OBJECTDIR}/TCPIPStack/IP.p1.d
	@cat ${OBJECTDIR}/TCPIPStack/IP.dep >> ${OBJECTDIR}/TCPIPStack/IP.p1.d
	@${FIXDEPS} "${OBJECTDIR}/TCPIPStack/IP.p1.d" $(SILENT) -ht
	
${OBJECTDIR}/TCPIPStack/SNTP.p1: TCPIPStack/SNTP.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/TCPIPStack 
	${MP_CC} --pass1 TCPIPStack/SNTP.c $(MP_EXTRA_CC_PRE) -q --chip=$(MP_PROCESSOR_OPTION) -P  --outdir=${OBJECTDIR}/TCPIPStack -N31 -I"." -I"TCPIPStack" -I"libs" -I"drivers" --warn=0 --runtime=default,+clear,+init,-keep,-download,+stackwarn,-config,+clib,-plib --opt=default,+asm,-asmfile,-speed,+space,9 --cp=16 -Blarge --double=24  --mode=pro -g --asmlist "--errformat=%%f:%%l: error: %%s" "--msgformat=%%f:%%l: advisory: %%s" "--warnformat=%%f:%%l warning: %%s"  
	@${MP_CC} --scandep  TCPIPStack/SNTP.c $(MP_EXTRA_CC_PRE) -q --chip=$(MP_PROCESSOR_OPTION) -P  --outdir=${OBJECTDIR}/TCPIPStack -N31 -I"." -I"TCPIPStack" -I"libs" -I"drivers" --warn=0 --runtime=default,+clear,+init,-keep,-download,+stackwarn,-config,+clib,-plib --opt=default,+asm,-asmfile,-speed,+space,9 --cp=16 -Blarge --double=24  --mode=pro -g --asmlist "--errformat=%%f:%%l: error: %%s" "--msgformat=%%f:%%l: advisory: %%s" "--warnformat=%%f:%%l warning: %%s"  
	@echo ${OBJECTDIR}/TCPIPStack/SNTP.p1: > ${OBJECTDIR}/TCPIPStack/SNTP.p1.d
	@cat ${OBJECTDIR}/TCPIPStack/SNTP.dep >> ${OBJECTDIR}/TCPIPStack/SNTP.p1.d
	@${FIXDEPS} "${OBJECTDIR}/TCPIPStack/SNTP.p1.d" $(SILENT) -ht
	
${OBJECTDIR}/TCPIPStack/StackTsk.p1: TCPIPStack/StackTsk.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/TCPIPStack 
	${MP_CC} --pass1 TCPIPStack/StackTsk.c $(MP_EXTRA_CC_PRE) -q --chip=$(MP_PROCESSOR_OPTION) -P  --outdir=${OBJECTDIR}/TCPIPStack -N31 -I"." -I"TCPIPStack" -I"libs" -I"drivers" --warn=0 --runtime=default,+clear,+init,-keep,-download,+stackwarn,-config,+clib,-plib --opt=default,+asm,-asmfile,-speed,+space,9 --cp=16 -Blarge --double=24  --mode=pro -g --asmlist "--errformat=%%f:%%l: error: %%s" "--msgformat=%%f:%%l: advisory: %%s" "--warnformat=%%f:%%l warning: %%s"  
	@${MP_CC} --scandep  TCPIPStack/StackTsk.c $(MP_EXTRA_CC_PRE) -q --chip=$(MP_PROCESSOR_OPTION) -P  --outdir=${OBJECTDIR}/TCPIPStack -N31 -I"." -I"TCPIPStack" -I"libs" -I"drivers" --warn=0 --runtime=default,+clear,+init,-keep,-download,+stackwarn,-config,+clib,-plib --opt=default,+asm,-asmfile,-speed,+space,9 --cp=16 -Blarge --double=24  --mode=pro -g --asmlist "--errformat=%%f:%%l: error: %%s" "--msgformat=%%f:%%l: advisory: %%s" "--warnformat=%%f:%%l warning: %%s"  
	@echo ${OBJECTDIR}/TCPIPStack/StackTsk.p1: > ${OBJECTDIR}/TCPIPStack/StackTsk.p1.d
	@cat ${OBJECTDIR}/TCPIPStack/StackTsk.dep >> ${OBJECTDIR}/TCPIPStack/StackTsk.p1.d
	@${FIXDEPS} "${OBJECTDIR}/TCPIPStack/StackTsk.p1.d" $(SILENT) -ht
	
${OBJECTDIR}/TCPIPStack/TCP.p1: TCPIPStack/TCP.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/TCPIPStack 
	${MP_CC} --pass1 TCPIPStack/TCP.c $(MP_EXTRA_CC_PRE) -q --chip=$(MP_PROCESSOR_OPTION) -P  --outdir=${OBJECTDIR}/TCPIPStack -N31 -I"." -I"TCPIPStack" -I"libs" -I"drivers" --warn=0 --runtime=default,+clear,+init,-keep,-download,+stackwarn,-config,+clib,-plib --opt=default,+asm,-asmfile,-speed,+space,9 --cp=16 -Blarge --double=24  --mode=pro -g --asmlist "--errformat=%%f:%%l: error: %%s" "--msgformat=%%f:%%l: advisory: %%s" "--warnformat=%%f:%%l warning: %%s"  
	@${MP_CC} --scandep  TCPIPStack/TCP.c $(MP_EXTRA_CC_PRE) -q --chip=$(MP_PROCESSOR_OPTION) -P  --outdir=${OBJECTDIR}/TCPIPStack -N31 -I"." -I"TCPIPStack" -I"libs" -I"drivers" --warn=0 --runtime=default,+clear,+init,-keep,-download,+stackwarn,-config,+clib,-plib --opt=default,+asm,-asmfile,-speed,+space,9 --cp=16 -Blarge --double=24  --mode=pro -g --asmlist "--errformat=%%f:%%l: error: %%s" "--msgformat=%%f:%%l: advisory: %%s" "--warnformat=%%f:%%l warning: %%s"  
	@echo ${OBJECTDIR}/TCPIPStack/TCP.p1: > ${OBJECTDIR}/TCPIPStack/TCP.p1.d
	@cat ${OBJECTDIR}/TCPIPStack/TCP.dep >> ${OBJECTDIR}/TCPIPStack/TCP.p1.d
	@${FIXDEPS} "${OBJECTDIR}/TCPIPStack/TCP.p1.d" $(SILENT) -ht
	
${OBJECTDIR}/TCPIPStack/Tick.p1: TCPIPStack/Tick.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/TCPIPStack 
	${MP_CC} --pass1 TCPIPStack/Tick.c $(MP_EXTRA_CC_PRE) -q --chip=$(MP_PROCESSOR_OPTION) -P  --outdir=${OBJECTDIR}/TCPIPStack -N31 -I"." -I"TCPIPStack" -I"libs" -I"drivers" --warn=0 --runtime=default,+clear,+init,-keep,-download,+stackwarn,-config,+clib,-plib --opt=default,+asm,-asmfile,-speed,+space,9 --cp=16 -Blarge --double=24  --mode=pro -g --asmlist "--errformat=%%f:%%l: error: %%s" "--msgformat=%%f:%%l: advisory: %%s" "--warnformat=%%f:%%l warning: %%s"  
	@${MP_CC} --scandep  TCPIPStack/Tick.c $(MP_EXTRA_CC_PRE) -q --chip=$(MP_PROCESSOR_OPTION) -P  --outdir=${OBJECTDIR}/TCPIPStack -N31 -I"." -I"TCPIPStack" -I"libs" -I"drivers" --warn=0 --runtime=default,+clear,+init,-keep,-download,+stackwarn,-config,+clib,-plib --opt=default,+asm,-asmfile,-speed,+space,9 --cp=16 -Blarge --double=24  --mode=pro -g --asmlist "--errformat=%%f:%%l: error: %%s" "--msgformat=%%f:%%l: advisory: %%s" "--warnformat=%%f:%%l warning: %%s"  
	@echo ${OBJECTDIR}/TCPIPStack/Tick.p1: > ${OBJECTDIR}/TCPIPStack/Tick.p1.d
	@cat ${OBJECTDIR}/TCPIPStack/Tick.dep >> ${OBJECTDIR}/TCPIPStack/Tick.p1.d
	@${FIXDEPS} "${OBJECTDIR}/TCPIPStack/Tick.p1.d" $(SILENT) -ht
	
${OBJECTDIR}/TCPIPStack/UDP.p1: TCPIPStack/UDP.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/TCPIPStack 
	${MP_CC} --pass1 TCPIPStack/UDP.c $(MP_EXTRA_CC_PRE) -q --chip=$(MP_PROCESSOR_OPTION) -P  --outdir=${OBJECTDIR}/TCPIPStack -N31 -I"." -I"TCPIPStack" -I"libs" -I"drivers" --warn=0 --runtime=default,+clear,+init,-keep,-download,+stackwarn,-config,+clib,-plib --opt=default,+asm,-asmfile,-speed,+space,9 --cp=16 -Blarge --double=24  --mode=pro -g --asmlist "--errformat=%%f:%%l: error: %%s" "--msgformat=%%f:%%l: advisory: %%s" "--warnformat=%%f:%%l warning: %%s"  
	@${MP_CC} --scandep  TCPIPStack/UDP.c $(MP_EXTRA_CC_PRE) -q --chip=$(MP_PROCESSOR_OPTION) -P  --outdir=${OBJECTDIR}/TCPIPStack -N31 -I"." -I"TCPIPStack" -I"libs" -I"drivers" --warn=0 --runtime=default,+clear,+init,-keep,-download,+stackwarn,-config,+clib,-plib --opt=default,+asm,-asmfile,-speed,+space,9 --cp=16 -Blarge --double=24  --mode=pro -g --asmlist "--errformat=%%f:%%l: error: %%s" "--msgformat=%%f:%%l: advisory: %%s" "--warnformat=%%f:%%l warning: %%s"  
	@echo ${OBJECTDIR}/TCPIPStack/UDP.p1: > ${OBJECTDIR}/TCPIPStack/UDP.p1.d
	@cat ${OBJECTDIR}/TCPIPStack/UDP.dep >> ${OBJECTDIR}/TCPIPStack/UDP.p1.d
	@${FIXDEPS} "${OBJECTDIR}/TCPIPStack/UDP.p1.d" $(SILENT) -ht
	
${OBJECTDIR}/Config.p1: Config.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR} 
	${MP_CC} --pass1 Config.c $(MP_EXTRA_CC_PRE) -q --chip=$(MP_PROCESSOR_OPTION) -P  --outdir=${OBJECTDIR} -N31 -I"." -I"TCPIPStack" -I"libs" -I"drivers" --warn=0 --runtime=default,+clear,+init,-keep,-download,+stackwarn,-config,+clib,-plib --opt=default,+asm,-asmfile,-speed,+space,9 --cp=16 -Blarge --double=24  --mode=pro -g --asmlist "--errformat=%%f:%%l: error: %%s" "--msgformat=%%f:%%l: advisory: %%s" "--warnformat=%%f:%%l warning: %%s"  
	@${MP_CC} --scandep  Config.c $(MP_EXTRA_CC_PRE) -q --chip=$(MP_PROCESSOR_OPTION) -P  --outdir=${OBJECTDIR} -N31 -I"." -I"TCPIPStack" -I"libs" -I"drivers" --warn=0 --runtime=default,+clear,+init,-keep,-download,+stackwarn,-config,+clib,-plib --opt=default,+asm,-asmfile,-speed,+space,9 --cp=16 -Blarge --double=24  --mode=pro -g --asmlist "--errformat=%%f:%%l: error: %%s" "--msgformat=%%f:%%l: advisory: %%s" "--warnformat=%%f:%%l warning: %%s"  
	@echo ${OBJECTDIR}/Config.p1: > ${OBJECTDIR}/Config.p1.d
	@cat ${OBJECTDIR}/Config.dep >> ${OBJECTDIR}/Config.p1.d
	@${FIXDEPS} "${OBJECTDIR}/Config.p1.d" $(SILENT) -ht
	
${OBJECTDIR}/Interrupt.p1: Interrupt.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR} 
	${MP_CC} --pass1 Interrupt.c $(MP_EXTRA_CC_PRE) -q --chip=$(MP_PROCESSOR_OPTION) -P  --outdir=${OBJECTDIR} -N31 -I"." -I"TCPIPStack" -I"libs" -I"drivers" --warn=0 --runtime=default,+clear,+init,-keep,-download,+stackwarn,-config,+clib,-plib --opt=default,+asm,-asmfile,-speed,+space,9 --cp=16 -Blarge --double=24  --mode=pro -g --asmlist "--errformat=%%f:%%l: error: %%s" "--msgformat=%%f:%%l: advisory: %%s" "--warnformat=%%f:%%l warning: %%s"  
	@${MP_CC} --scandep  Interrupt.c $(MP_EXTRA_CC_PRE) -q --chip=$(MP_PROCESSOR_OPTION) -P  --outdir=${OBJECTDIR} -N31 -I"." -I"TCPIPStack" -I"libs" -I"drivers" --warn=0 --runtime=default,+clear,+init,-keep,-download,+stackwarn,-config,+clib,-plib --opt=default,+asm,-asmfile,-speed,+space,9 --cp=16 -Blarge --double=24  --mode=pro -g --asmlist "--errformat=%%f:%%l: error: %%s" "--msgformat=%%f:%%l: advisory: %%s" "--warnformat=%%f:%%l warning: %%s"  
	@echo ${OBJECTDIR}/Interrupt.p1: > ${OBJECTDIR}/Interrupt.p1.d
	@cat ${OBJECTDIR}/Interrupt.dep >> ${OBJECTDIR}/Interrupt.p1.d
	@${FIXDEPS} "${OBJECTDIR}/Interrupt.p1.d" $(SILENT) -ht
	
${OBJECTDIR}/Main.p1: Main.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR} 
	${MP_CC} --pass1 Main.c $(MP_EXTRA_CC_PRE) -q --chip=$(MP_PROCESSOR_OPTION) -P  --outdir=${OBJECTDIR} -N31 -I"." -I"TCPIPStack" -I"libs" -I"drivers" --warn=0 --runtime=default,+clear,+init,-keep,-download,+stackwarn,-config,+clib,-plib --opt=default,+asm,-asmfile,-speed,+space,9 --cp=16 -Blarge --double=24  --mode=pro -g --asmlist "--errformat=%%f:%%l: error: %%s" "--msgformat=%%f:%%l: advisory: %%s" "--warnformat=%%f:%%l warning: %%s"  
	@${MP_CC} --scandep  Main.c $(MP_EXTRA_CC_PRE) -q --chip=$(MP_PROCESSOR_OPTION) -P  --outdir=${OBJECTDIR} -N31 -I"." -I"TCPIPStack" -I"libs" -I"drivers" --warn=0 --runtime=default,+clear,+init,-keep,-download,+stackwarn,-config,+clib,-plib --opt=default,+asm,-asmfile,-speed,+space,9 --cp=16 -Blarge --double=24  --mode=pro -g --asmlist "--errformat=%%f:%%l: error: %%s" "--msgformat=%%f:%%l: advisory: %%s" "--warnformat=%%f:%%l warning: %%s"  
	@echo ${OBJECTDIR}/Main.p1: > ${OBJECTDIR}/Main.p1.d
	@cat ${OBJECTDIR}/Main.dep >> ${OBJECTDIR}/Main.p1.d
	@${FIXDEPS} "${OBJECTDIR}/Main.p1.d" $(SILENT) -ht
	
${OBJECTDIR}/NTP.p1: NTP.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR} 
	${MP_CC} --pass1 NTP.c $(MP_EXTRA_CC_PRE) -q --chip=$(MP_PROCESSOR_OPTION) -P  --outdir=${OBJECTDIR} -N31 -I"." -I"TCPIPStack" -I"libs" -I"drivers" --warn=0 --runtime=default,+clear,+init,-keep,-download,+stackwarn,-config,+clib,-plib --opt=default,+asm,-asmfile,-speed,+space,9 --cp=16 -Blarge --double=24  --mode=pro -g --asmlist "--errformat=%%f:%%l: error: %%s" "--msgformat=%%f:%%l: advisory: %%s" "--warnformat=%%f:%%l warning: %%s"  
	@${MP_CC} --scandep  NTP.c $(MP_EXTRA_CC_PRE) -q --chip=$(MP_PROCESSOR_OPTION) -P  --outdir=${OBJECTDIR} -N31 -I"." -I"TCPIPStack" -I"libs" -I"drivers" --warn=0 --runtime=default,+clear,+init,-keep,-download,+stackwarn,-config,+clib,-plib --opt=default,+asm,-asmfile,-speed,+space,9 --cp=16 -Blarge --double=24  --mode=pro -g --asmlist "--errformat=%%f:%%l: error: %%s" "--msgformat=%%f:%%l: advisory: %%s" "--warnformat=%%f:%%l warning: %%s"  
	@echo ${OBJECTDIR}/NTP.p1: > ${OBJECTDIR}/NTP.p1.d
	@cat ${OBJECTDIR}/NTP.dep >> ${OBJECTDIR}/NTP.p1.d
	@${FIXDEPS} "${OBJECTDIR}/NTP.p1.d" $(SILENT) -ht
	
${OBJECTDIR}/UART.p1: UART.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR} 
	${MP_CC} --pass1 UART.c $(MP_EXTRA_CC_PRE) -q --chip=$(MP_PROCESSOR_OPTION) -P  --outdir=${OBJECTDIR} -N31 -I"." -I"TCPIPStack" -I"libs" -I"drivers" --warn=0 --runtime=default,+clear,+init,-keep,-download,+stackwarn,-config,+clib,-plib --opt=default,+asm,-asmfile,-speed,+space,9 --cp=16 -Blarge --double=24  --mode=pro -g --asmlist "--errformat=%%f:%%l: error: %%s" "--msgformat=%%f:%%l: advisory: %%s" "--warnformat=%%f:%%l warning: %%s"  
	@${MP_CC} --scandep  UART.c $(MP_EXTRA_CC_PRE) -q --chip=$(MP_PROCESSOR_OPTION) -P  --outdir=${OBJECTDIR} -N31 -I"." -I"TCPIPStack" -I"libs" -I"drivers" --warn=0 --runtime=default,+clear,+init,-keep,-download,+stackwarn,-config,+clib,-plib --opt=default,+asm,-asmfile,-speed,+space,9 --cp=16 -Blarge --double=24  --mode=pro -g --asmlist "--errformat=%%f:%%l: error: %%s" "--msgformat=%%f:%%l: advisory: %%s" "--warnformat=%%f:%%l warning: %%s"  
	@echo ${OBJECTDIR}/UART.p1: > ${OBJECTDIR}/UART.p1.d
	@cat ${OBJECTDIR}/UART.dep >> ${OBJECTDIR}/UART.p1.d
	@${FIXDEPS} "${OBJECTDIR}/UART.p1.d" $(SILENT) -ht
	
${OBJECTDIR}/WeatherXMLDownloader.p1: WeatherXMLDownloader.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR} 
	${MP_CC} --pass1 WeatherXMLDownloader.c $(MP_EXTRA_CC_PRE) -q --chip=$(MP_PROCESSOR_OPTION) -P  --outdir=${OBJECTDIR} -N31 -I"." -I"TCPIPStack" -I"libs" -I"drivers" --warn=0 --runtime=default,+clear,+init,-keep,-download,+stackwarn,-config,+clib,-plib --opt=default,+asm,-asmfile,-speed,+space,9 --cp=16 -Blarge --double=24  --mode=pro -g --asmlist "--errformat=%%f:%%l: error: %%s" "--msgformat=%%f:%%l: advisory: %%s" "--warnformat=%%f:%%l warning: %%s"  
	@${MP_CC} --scandep  WeatherXMLDownloader.c $(MP_EXTRA_CC_PRE) -q --chip=$(MP_PROCESSOR_OPTION) -P  --outdir=${OBJECTDIR} -N31 -I"." -I"TCPIPStack" -I"libs" -I"drivers" --warn=0 --runtime=default,+clear,+init,-keep,-download,+stackwarn,-config,+clib,-plib --opt=default,+asm,-asmfile,-speed,+space,9 --cp=16 -Blarge --double=24  --mode=pro -g --asmlist "--errformat=%%f:%%l: error: %%s" "--msgformat=%%f:%%l: advisory: %%s" "--warnformat=%%f:%%l warning: %%s"  
	@echo ${OBJECTDIR}/WeatherXMLDownloader.p1: > ${OBJECTDIR}/WeatherXMLDownloader.p1.d
	@cat ${OBJECTDIR}/WeatherXMLDownloader.dep >> ${OBJECTDIR}/WeatherXMLDownloader.p1.d
	@${FIXDEPS} "${OBJECTDIR}/WeatherXMLDownloader.p1.d" $(SILENT) -ht
	
${OBJECTDIR}/WeatherXMLParser.p1: WeatherXMLParser.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR} 
	${MP_CC} --pass1 WeatherXMLParser.c $(MP_EXTRA_CC_PRE) -q --chip=$(MP_PROCESSOR_OPTION) -P  --outdir=${OBJECTDIR} -N31 -I"." -I"TCPIPStack" -I"libs" -I"drivers" --warn=0 --runtime=default,+clear,+init,-keep,-download,+stackwarn,-config,+clib,-plib --opt=default,+asm,-asmfile,-speed,+space,9 --cp=16 -Blarge --double=24  --mode=pro -g --asmlist "--errformat=%%f:%%l: error: %%s" "--msgformat=%%f:%%l: advisory: %%s" "--warnformat=%%f:%%l warning: %%s"  
	@${MP_CC} --scandep  WeatherXMLParser.c $(MP_EXTRA_CC_PRE) -q --chip=$(MP_PROCESSOR_OPTION) -P  --outdir=${OBJECTDIR} -N31 -I"." -I"TCPIPStack" -I"libs" -I"drivers" --warn=0 --runtime=default,+clear,+init,-keep,-download,+stackwarn,-config,+clib,-plib --opt=default,+asm,-asmfile,-speed,+space,9 --cp=16 -Blarge --double=24  --mode=pro -g --asmlist "--errformat=%%f:%%l: error: %%s" "--msgformat=%%f:%%l: advisory: %%s" "--warnformat=%%f:%%l warning: %%s"  
	@echo ${OBJECTDIR}/WeatherXMLParser.p1: > ${OBJECTDIR}/WeatherXMLParser.p1.d
	@cat ${OBJECTDIR}/WeatherXMLParser.dep >> ${OBJECTDIR}/WeatherXMLParser.p1.d
	@${FIXDEPS} "${OBJECTDIR}/WeatherXMLParser.p1.d" $(SILENT) -ht
	
${OBJECTDIR}/XMLDownloader.p1: XMLDownloader.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR} 
	${MP_CC} --pass1 XMLDownloader.c $(MP_EXTRA_CC_PRE) -q --chip=$(MP_PROCESSOR_OPTION) -P  --outdir=${OBJECTDIR} -N31 -I"." -I"TCPIPStack" -I"libs" -I"drivers" --warn=0 --runtime=default,+clear,+init,-keep,-download,+stackwarn,-config,+clib,-plib --opt=default,+asm,-asmfile,-speed,+space,9 --cp=16 -Blarge --double=24  --mode=pro -g --asmlist "--errformat=%%f:%%l: error: %%s" "--msgformat=%%f:%%l: advisory: %%s" "--warnformat=%%f:%%l warning: %%s"  
	@${MP_CC} --scandep  XMLDownloader.c $(MP_EXTRA_CC_PRE) -q --chip=$(MP_PROCESSOR_OPTION) -P  --outdir=${OBJECTDIR} -N31 -I"." -I"TCPIPStack" -I"libs" -I"drivers" --warn=0 --runtime=default,+clear,+init,-keep,-download,+stackwarn,-config,+clib,-plib --opt=default,+asm,-asmfile,-speed,+space,9 --cp=16 -Blarge --double=24  --mode=pro -g --asmlist "--errformat=%%f:%%l: error: %%s" "--msgformat=%%f:%%l: advisory: %%s" "--warnformat=%%f:%%l warning: %%s"  
	@echo ${OBJECTDIR}/XMLDownloader.p1: > ${OBJECTDIR}/XMLDownloader.p1.d
	@cat ${OBJECTDIR}/XMLDownloader.dep >> ${OBJECTDIR}/XMLDownloader.p1.d
	@${FIXDEPS} "${OBJECTDIR}/XMLDownloader.p1.d" $(SILENT) -ht
	
${OBJECTDIR}/XMLParser.p1: XMLParser.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR} 
	${MP_CC} --pass1 XMLParser.c $(MP_EXTRA_CC_PRE) -q --chip=$(MP_PROCESSOR_OPTION) -P  --outdir=${OBJECTDIR} -N31 -I"." -I"TCPIPStack" -I"libs" -I"drivers" --warn=0 --runtime=default,+clear,+init,-keep,-download,+stackwarn,-config,+clib,-plib --opt=default,+asm,-asmfile,-speed,+space,9 --cp=16 -Blarge --double=24  --mode=pro -g --asmlist "--errformat=%%f:%%l: error: %%s" "--msgformat=%%f:%%l: advisory: %%s" "--warnformat=%%f:%%l warning: %%s"  
	@${MP_CC} --scandep  XMLParser.c $(MP_EXTRA_CC_PRE) -q --chip=$(MP_PROCESSOR_OPTION) -P  --outdir=${OBJECTDIR} -N31 -I"." -I"TCPIPStack" -I"libs" -I"drivers" --warn=0 --runtime=default,+clear,+init,-keep,-download,+stackwarn,-config,+clib,-plib --opt=default,+asm,-asmfile,-speed,+space,9 --cp=16 -Blarge --double=24  --mode=pro -g --asmlist "--errformat=%%f:%%l: error: %%s" "--msgformat=%%f:%%l: advisory: %%s" "--warnformat=%%f:%%l warning: %%s"  
	@echo ${OBJECTDIR}/XMLParser.p1: > ${OBJECTDIR}/XMLParser.p1.d
	@cat ${OBJECTDIR}/XMLParser.dep >> ${OBJECTDIR}/XMLParser.p1.d
	@${FIXDEPS} "${OBJECTDIR}/XMLParser.p1.d" $(SILENT) -ht
	
${OBJECTDIR}/LCDUI.p1: LCDUI.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR} 
	${MP_CC} --pass1 LCDUI.c $(MP_EXTRA_CC_PRE) -q --chip=$(MP_PROCESSOR_OPTION) -P  --outdir=${OBJECTDIR} -N31 -I"." -I"TCPIPStack" -I"libs" -I"drivers" --warn=0 --runtime=default,+clear,+init,-keep,-download,+stackwarn,-config,+clib,-plib --opt=default,+asm,-asmfile,-speed,+space,9 --cp=16 -Blarge --double=24  --mode=pro -g --asmlist "--errformat=%%f:%%l: error: %%s" "--msgformat=%%f:%%l: advisory: %%s" "--warnformat=%%f:%%l warning: %%s"  
	@${MP_CC} --scandep  LCDUI.c $(MP_EXTRA_CC_PRE) -q --chip=$(MP_PROCESSOR_OPTION) -P  --outdir=${OBJECTDIR} -N31 -I"." -I"TCPIPStack" -I"libs" -I"drivers" --warn=0 --runtime=default,+clear,+init,-keep,-download,+stackwarn,-config,+clib,-plib --opt=default,+asm,-asmfile,-speed,+space,9 --cp=16 -Blarge --double=24  --mode=pro -g --asmlist "--errformat=%%f:%%l: error: %%s" "--msgformat=%%f:%%l: advisory: %%s" "--warnformat=%%f:%%l warning: %%s"  
	@echo ${OBJECTDIR}/LCDUI.p1: > ${OBJECTDIR}/LCDUI.p1.d
	@cat ${OBJECTDIR}/LCDUI.dep >> ${OBJECTDIR}/LCDUI.p1.d
	@${FIXDEPS} "${OBJECTDIR}/LCDUI.p1.d" $(SILENT) -ht
	
${OBJECTDIR}/LCDUI_Menu.p1: LCDUI_Menu.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR} 
	${MP_CC} --pass1 LCDUI_Menu.c $(MP_EXTRA_CC_PRE) -q --chip=$(MP_PROCESSOR_OPTION) -P  --outdir=${OBJECTDIR} -N31 -I"." -I"TCPIPStack" -I"libs" -I"drivers" --warn=0 --runtime=default,+clear,+init,-keep,-download,+stackwarn,-config,+clib,-plib --opt=default,+asm,-asmfile,-speed,+space,9 --cp=16 -Blarge --double=24  --mode=pro -g --asmlist "--errformat=%%f:%%l: error: %%s" "--msgformat=%%f:%%l: advisory: %%s" "--warnformat=%%f:%%l warning: %%s"  
	@${MP_CC} --scandep  LCDUI_Menu.c $(MP_EXTRA_CC_PRE) -q --chip=$(MP_PROCESSOR_OPTION) -P  --outdir=${OBJECTDIR} -N31 -I"." -I"TCPIPStack" -I"libs" -I"drivers" --warn=0 --runtime=default,+clear,+init,-keep,-download,+stackwarn,-config,+clib,-plib --opt=default,+asm,-asmfile,-speed,+space,9 --cp=16 -Blarge --double=24  --mode=pro -g --asmlist "--errformat=%%f:%%l: error: %%s" "--msgformat=%%f:%%l: advisory: %%s" "--warnformat=%%f:%%l warning: %%s"  
	@echo ${OBJECTDIR}/LCDUI_Menu.p1: > ${OBJECTDIR}/LCDUI_Menu.p1.d
	@cat ${OBJECTDIR}/LCDUI_Menu.dep >> ${OBJECTDIR}/LCDUI_Menu.p1.d
	@${FIXDEPS} "${OBJECTDIR}/LCDUI_Menu.p1.d" $(SILENT) -ht
	
${OBJECTDIR}/LCDUI_Text.p1: LCDUI_Text.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR} 
	${MP_CC} --pass1 LCDUI_Text.c $(MP_EXTRA_CC_PRE) -q --chip=$(MP_PROCESSOR_OPTION) -P  --outdir=${OBJECTDIR} -N31 -I"." -I"TCPIPStack" -I"libs" -I"drivers" --warn=0 --runtime=default,+clear,+init,-keep,-download,+stackwarn,-config,+clib,-plib --opt=default,+asm,-asmfile,-speed,+space,9 --cp=16 -Blarge --double=24  --mode=pro -g --asmlist "--errformat=%%f:%%l: error: %%s" "--msgformat=%%f:%%l: advisory: %%s" "--warnformat=%%f:%%l warning: %%s"  
	@${MP_CC} --scandep  LCDUI_Text.c $(MP_EXTRA_CC_PRE) -q --chip=$(MP_PROCESSOR_OPTION) -P  --outdir=${OBJECTDIR} -N31 -I"." -I"TCPIPStack" -I"libs" -I"drivers" --warn=0 --runtime=default,+clear,+init,-keep,-download,+stackwarn,-config,+clib,-plib --opt=default,+asm,-asmfile,-speed,+space,9 --cp=16 -Blarge --double=24  --mode=pro -g --asmlist "--errformat=%%f:%%l: error: %%s" "--msgformat=%%f:%%l: advisory: %%s" "--warnformat=%%f:%%l warning: %%s"  
	@echo ${OBJECTDIR}/LCDUI_Text.p1: > ${OBJECTDIR}/LCDUI_Text.p1.d
	@cat ${OBJECTDIR}/LCDUI_Text.dep >> ${OBJECTDIR}/LCDUI_Text.p1.d
	@${FIXDEPS} "${OBJECTDIR}/LCDUI_Text.p1.d" $(SILENT) -ht
	
${OBJECTDIR}/LCDUI_NumberDialog.p1: LCDUI_NumberDialog.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR} 
	${MP_CC} --pass1 LCDUI_NumberDialog.c $(MP_EXTRA_CC_PRE) -q --chip=$(MP_PROCESSOR_OPTION) -P  --outdir=${OBJECTDIR} -N31 -I"." -I"TCPIPStack" -I"libs" -I"drivers" --warn=0 --runtime=default,+clear,+init,-keep,-download,+stackwarn,-config,+clib,-plib --opt=default,+asm,-asmfile,-speed,+space,9 --cp=16 -Blarge --double=24  --mode=pro -g --asmlist "--errformat=%%f:%%l: error: %%s" "--msgformat=%%f:%%l: advisory: %%s" "--warnformat=%%f:%%l warning: %%s"  
	@${MP_CC} --scandep  LCDUI_NumberDialog.c $(MP_EXTRA_CC_PRE) -q --chip=$(MP_PROCESSOR_OPTION) -P  --outdir=${OBJECTDIR} -N31 -I"." -I"TCPIPStack" -I"libs" -I"drivers" --warn=0 --runtime=default,+clear,+init,-keep,-download,+stackwarn,-config,+clib,-plib --opt=default,+asm,-asmfile,-speed,+space,9 --cp=16 -Blarge --double=24  --mode=pro -g --asmlist "--errformat=%%f:%%l: error: %%s" "--msgformat=%%f:%%l: advisory: %%s" "--warnformat=%%f:%%l warning: %%s"  
	@echo ${OBJECTDIR}/LCDUI_NumberDialog.p1: > ${OBJECTDIR}/LCDUI_NumberDialog.p1.d
	@cat ${OBJECTDIR}/LCDUI_NumberDialog.dep >> ${OBJECTDIR}/LCDUI_NumberDialog.p1.d
	@${FIXDEPS} "${OBJECTDIR}/LCDUI_NumberDialog.p1.d" $(SILENT) -ht
	
${OBJECTDIR}/PIC.p1: PIC.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR} 
	${MP_CC} --pass1 PIC.c $(MP_EXTRA_CC_PRE) -q --chip=$(MP_PROCESSOR_OPTION) -P  --outdir=${OBJECTDIR} -N31 -I"." -I"TCPIPStack" -I"libs" -I"drivers" --warn=0 --runtime=default,+clear,+init,-keep,-download,+stackwarn,-config,+clib,-plib --opt=default,+asm,-asmfile,-speed,+space,9 --cp=16 -Blarge --double=24  --mode=pro -g --asmlist "--errformat=%%f:%%l: error: %%s" "--msgformat=%%f:%%l: advisory: %%s" "--warnformat=%%f:%%l warning: %%s"  
	@${MP_CC} --scandep  PIC.c $(MP_EXTRA_CC_PRE) -q --chip=$(MP_PROCESSOR_OPTION) -P  --outdir=${OBJECTDIR} -N31 -I"." -I"TCPIPStack" -I"libs" -I"drivers" --warn=0 --runtime=default,+clear,+init,-keep,-download,+stackwarn,-config,+clib,-plib --opt=default,+asm,-asmfile,-speed,+space,9 --cp=16 -Blarge --double=24  --mode=pro -g --asmlist "--errformat=%%f:%%l: error: %%s" "--msgformat=%%f:%%l: advisory: %%s" "--warnformat=%%f:%%l warning: %%s"  
	@echo ${OBJECTDIR}/PIC.p1: > ${OBJECTDIR}/PIC.p1.d
	@cat ${OBJECTDIR}/PIC.dep >> ${OBJECTDIR}/PIC.p1.d
	@${FIXDEPS} "${OBJECTDIR}/PIC.p1.d" $(SILENT) -ht
	
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: link
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
dist/${CND_CONF}/${IMAGE_TYPE}/ethernetws2.${IMAGE_TYPE}.${OUTPUT_SUFFIX}: ${OBJECTFILES}  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} dist/${CND_CONF}/${IMAGE_TYPE} 
	${MP_LD} $(MP_EXTRA_LD_PRE) -odist/${CND_CONF}/${IMAGE_TYPE}/ethernetws2.${IMAGE_TYPE}.${OUTPUT_SUFFIX}  -mdist/${CND_CONF}/${IMAGE_TYPE}/ethernetws2.${IMAGE_TYPE}.map --summary=default,-psect,-class,+mem,-hex --chip=$(MP_PROCESSOR_OPTION) -P --runtime=default,+clear,+init,-keep,-download,+stackwarn,-config,+clib,-plib --opt=default,+asm,-asmfile,-speed,+space,9 -D__DEBUG --debugger=pickit2 -N31 -I"." -I"TCPIPStack" -I"libs" -I"drivers" --warn=0 --cp=16 -Blarge --double=24  --mode=pro  --output=default,-inhx032 -g --asmlist "--errformat=%%f:%%l: error: %%s" "--msgformat=%%f:%%l: advisory: %%s" "--warnformat=%%f:%%l warning: %%s" ${OBJECTFILES_QUOTED_IF_SPACED}    
	${RM} dist/${CND_CONF}/${IMAGE_TYPE}/ethernetws2.${IMAGE_TYPE}.hex
else
dist/${CND_CONF}/${IMAGE_TYPE}/ethernetws2.${IMAGE_TYPE}.${OUTPUT_SUFFIX}: ${OBJECTFILES}  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} dist/${CND_CONF}/${IMAGE_TYPE} 
	${MP_LD} $(MP_EXTRA_LD_PRE) -odist/${CND_CONF}/${IMAGE_TYPE}/ethernetws2.${IMAGE_TYPE}.${DEBUGGABLE_SUFFIX}  -mdist/${CND_CONF}/${IMAGE_TYPE}/ethernetws2.${IMAGE_TYPE}.map --summary=default,-psect,-class,+mem,-hex --chip=$(MP_PROCESSOR_OPTION) -P --runtime=default,+clear,+init,-keep,-download,+stackwarn,-config,+clib,-plib --opt=default,+asm,-asmfile,-speed,+space,9 -N31 -I"." -I"TCPIPStack" -I"libs" -I"drivers" --warn=0 --cp=16 -Blarge --double=24  --mode=pro  --output=default,-inhx032 -g --asmlist "--errformat=%%f:%%l: error: %%s" "--msgformat=%%f:%%l: advisory: %%s" "--warnformat=%%f:%%l warning: %%s" ${OBJECTFILES_QUOTED_IF_SPACED}    
endif


# Subprojects
.build-subprojects:


# Subprojects
.clean-subprojects:

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r build/default
	${RM} -r dist/default

# Enable dependency checking
.dep.inc: .depcheck-impl

DEPFILES=$(shell mplabwildcard ${POSSIBLE_DEPFILES})
ifneq (${DEPFILES},)
include ${DEPFILES}
endif
