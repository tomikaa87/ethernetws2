#
# Generated - do not edit!
#
# NOCDDL
#
CND_BASEDIR=`pwd`
# 9.63_T420 configuration
CND_ARTIFACT_DIR_9.63_T420=dist/9.63_T420/production
CND_ARTIFACT_NAME_9.63_T420=ethernetws2.production.hex
CND_ARTIFACT_PATH_9.63_T420=dist/9.63_T420/production/ethernetws2.production.hex
CND_PACKAGE_DIR_9.63_T420=${CND_DISTDIR}/9.63_T420/package
CND_PACKAGE_NAME_9.63_T420=ethernetws2.tar
CND_PACKAGE_PATH_9.63_T420=${CND_DISTDIR}/9.63_T420/package/ethernetws2.tar
# XC8_1.00_T420 configuration
CND_ARTIFACT_DIR_XC8_1.00_T420=dist/XC8_1.00_T420/production
CND_ARTIFACT_NAME_XC8_1.00_T420=ethernetws2.production.hex
CND_ARTIFACT_PATH_XC8_1.00_T420=dist/XC8_1.00_T420/production/ethernetws2.production.hex
CND_PACKAGE_DIR_XC8_1.00_T420=${CND_DISTDIR}/XC8_1.00_T420/package
CND_PACKAGE_NAME_XC8_1.00_T420=ethernetws2.tar
CND_PACKAGE_PATH_XC8_1.00_T420=${CND_DISTDIR}/XC8_1.00_T420/package/ethernetws2.tar
# 9.63 configuration
CND_ARTIFACT_DIR_9.63=dist/9.63/production
CND_ARTIFACT_NAME_9.63=ethernetws2.production.hex
CND_ARTIFACT_PATH_9.63=dist/9.63/production/ethernetws2.production.hex
CND_PACKAGE_DIR_9.63=${CND_DISTDIR}/9.63/package
CND_PACKAGE_NAME_9.63=ethernetws2.tar
CND_PACKAGE_PATH_9.63=${CND_DISTDIR}/9.63/package/ethernetws2.tar
