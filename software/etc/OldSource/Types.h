#ifndef _TYPES_H
#define _TYPES_H

typedef signed char int8;
typedef unsigned char uint8;

typedef signed int int16;
typedef unsigned int uint16;

typedef signed short long int24;
typedef unsigned short long uint24;

typedef signed long int32;
typedef unsigned long uint32;

// Advanced 1-byte integer type with bit fields and direct access to nibbles
typedef union
{
    struct {
        unsigned bit1 : 1;
        unsigned bit2 : 1;
        unsigned bit3 : 1;
        unsigned bit4 : 1;
        unsigned bit5 : 1;
        unsigned bit6 : 1;
        unsigned bit7 : 1;
        unsigned bit8 : 1;
    };
    struct {
        unsigned loNibble : 4;
        unsigned hiNibble : 4;
    };
    int8 i;
    uint8 ui;
} int8_adv;

// Advanced 16-bit integer type
typedef union
{
    struct {
        int8_adv lo_adv;
        int8_adv hi_adv;
    };
    struct {
        uint8 lo;
        uint8 hi;
    };
    uint8 bytes[2];
    int8_adv bytes_adv[2];
    int16 i;
    uint16 ui;
} int16_adv;

// Advanced 24-bit integer type
typedef union
{
    struct {
        int8_adv lo_adv;
        int8_adv med_adv;
        int8_adv hi_adv;
    };
    struct {
        uint8 lo;
        uint8 med;
        uint8 hi;
    };
    uint8 bytes[3];
    int8_adv bytes_adv[3];
    int24 i;
    uint24 ui;
} int24_adv;

// Advanced 32-bit integer type
typedef union
{
    struct {
        int8_adv lo_adv;
        int8_adv med1_adv;
        int8_adv med2_adv;
        int8_adv hi_adv;
    };
    struct {
        int16_adv lo_adv16;
        int16_adv hi_adv16;
    };
    struct {
        uint8 lo;
        uint8 med1;
        uint8 med2;
        uint8 hi;
    };
    struct {
        uint16 lo16;
        uint16 hi16;
    };
    uint8 bytes[4];
    int8_adv bytes_adv[4];
    int32 i;
    uint32 ui;
} int32_adv;

typedef struct {
    int16 tempOut1;
    int16 tempOut2;
    uint16 rhOut;
    uint32 presOut;
    uint32 presOutSeaLevel;
    uint16 vddOut;
    uint8 errOut;
    int8 tempIn;
    uint8 rhIn;
    uint16 vddIn;
    uint8 errIn;
    uint8 sensorDataUpdatedFlag;
} ExtSensorData;

#endif // _TYPES_H