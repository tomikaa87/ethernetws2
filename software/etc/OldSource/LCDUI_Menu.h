/**
 * Ethernet Weather Station v2 Firmware
 * LCD User Interface - Menu widget
 *
 * @author Tamas Karpati <tomikaa87>
 * @date 2011-12-25
 * @file LCDUI_Menu.h
 */

#ifndef _LCDUI_MENU_H
#define _LCDUI_MENU_H

#include "Types.h"

typedef struct
{
    const char *title;
    const char **items;
    uint8 count;
    uint8 index;
    uint8 offset;
    uint8 selection;
    uint8 maxItems;
    uint8 y;
    uint8 prevOffset;
    uint8 prevSelection;
} lcdui_menu_t;

void lcdui_menu_init(lcdui_menu_t *menu, const char *title, const char *items[],
        const uint8 numItems, const uint8 maxItems, const uint8 baseY);

void lcdui_menu_show(lcdui_menu_t *menu);
uint8 lcdui_menu_up(lcdui_menu_t *menu);
uint8 lcdui_menu_down(lcdui_menu_t *menu);

#endif // _LCDUI_MENU_H