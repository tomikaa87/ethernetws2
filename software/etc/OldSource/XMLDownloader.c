/**
 * Ethernet Weather Station v2 Firmware
 * Weather and location data downloader module
 *
 * @author Tamas Karpati <tomikaa87>
 * @date 2011-11-04
 * @file WeatherXMLDownloader.c
 */

#include "XMLDownloader.h"
#include "XMLParser.h"
#include "TCPIPStack/TCPIP.h"
#include "Config.h"
#include "Debug.h"
#include "SRAM_23K.h"

enum {
    S_START,
    S_CONNECT,
    S_SEND_REQ,
    S_RECEIVE_RESP,
    S_CLOSE,
    S_DONE
} g_state = S_DONE;

TCP_SOCKET g_socket = INVALID_SOCKET;
unsigned long g_tickCount = 0ul;
unsigned int g_bufferIndex = 0;
unsigned int g_reqLength = 0;
unsigned char g_buffer[256];
unsigned int g_recvBytes;
xmldl_error_t g_lastError = XDL_NO_ERROR;
xmldl_mode_t g_dataMode = XDL_MODE_WEATHER;
unsigned char g_isProxyEnabled = 0;

/*** AccuWeather server constants *********************************************/

// Location search:
// http://forecastfox.accuweather.com/adcbin/forecastfox/locate_city.asp?location=

const BYTE AW_SERVER_HOST[] = "vwidget.accuweather.com";
const WORD AW_SERVER_PORT = 80;

const BYTE AW_WDATA_LOCATION[] = "EUR|HU|HU006|BUDAPEST|";
#define AW_WDATA_LOCATION_SIZE (sizeof (AW_WDATA_LOCATION))

// First part of GET command
const BYTE AW_WDATA_REQ_GET_PART_1[] =
        "GET /widget/vista2/weather_data_v2.asp?location=";
#define AW_WDATA_REQ_GET_PART_1_SIZE (sizeof (AW_WDATA_REQ_GET_PART_1))

// First part of GET command (if proxy enabled)
const BYTE AW_WDATA_REQ_GET_PART_1_PROXY[] =
        "GET http://vwidget.accuweather.com/widget/vista2/weather_data_v2.asp?location=";
#define AW_WDATA_REQ_GET_PART_1_PROXY_SIZE (sizeof (AW_WDATA_REQ_GET_PART_1_PROXY))

// First part of GET command (location request)
const BYTE AW_LDATA_REQ_GET_PART_1[] =
        "GET /adcbin/forecastfox/locate_city.asp?location=";
#define AW_LDATA_REQ_GET_PART_1_SIZE (sizeof (AW_LDATA_REQ_GET_PART_1))

// First part of GET command (if proxy enabled, location request)
const BYTE AW_LDATA_REQ_GET_PART_1_PROXY[] =
        "GET http://forecastfox.accuweather.com/adcbin/forecastfox/locate_city.asp?location=";
#define AW_LDATA_REQ_GET_PART_1_PROXY_SIZE (sizeof (AW_LDATA_REQ_GET_PART_1_PROXY))

// Second part of GET command
const BYTE AW_REQ_GET_PART_2[] =
        "&metric=0 HTTP/1.1\r\n";
#define AW_REQ_GET_PART_2_SIZE (sizeof (AW_REQ_GET_PART_2))

// Necessary HTTP headers
const BYTE AW_REQ_TAIL[] =
        "Host: vwidget.accuweather.com\r\n" \
        "Accept: text/plain\r\n" \
        "Accept-charset: ISO-8859-2\r\n" \
        "Connection: close\r\n\r\n\0";
#define AW_REQ_TAIL_SIZE (sizeof (AW_REQ_TAIL))

/******************************************************************************/

void xmldl_reset()
{
    g_state = S_DONE;
    g_tickCount = 0;
    g_bufferIndex = 0;
    g_reqLength = 0;
    memset(g_buffer, 0, sizeof (g_buffer));
    g_lastError = XDL_NO_ERROR;

    if (TCPIsConnected(g_socket))
        TCPClose(g_socket);
    g_socket = INVALID_SOCKET;
}

void xmldl_start(xmldl_mode_t mode)
{
    if (g_state == S_DONE)
    {
        g_state = S_START;
        g_dataMode = mode;
    }
}

/** @todo error codes (lastError) */
void xmldl_task()
{
    switch (g_state) {
        // Initial state
        case S_START: {
            if (g_isProxyEnabled) {
                // Using HTTP proxy
                // Here we connect to the HTTP proxy
                _debug("[xdl] creating socket (proxy)\r\n");
            } else {
                // Direct connection
                _debug("[xdl] creating socket\r\n");
                // Create TCP client socket
                g_socket = TCPOpen((DWORD)&AW_SERVER_HOST, TCP_OPEN_ROM_HOST,
                        AW_SERVER_PORT, TCP_PURPOSE_DEFAULT);
            }
            // Verify socket
            if (g_socket == INVALID_SOCKET) {
                g_state = S_DONE;
                break;
            }
            _debug("[xdl] socket created, connecting\r\n");
            g_state = S_CONNECT;
            g_tickCount = TickGet();
            break;
        }

        // Connecting to the server
        case S_CONNECT: {
            // Check if socket is connected
            if (!TCPIsConnected(g_socket)) {
                // Check elapsed time
                if (TickGet() - g_tickCount >= 15 * TICKS_PER_SECOND) {
                    // Connection timed out
                    _debug("[xdl] connection timed out\r\n");
                    TCPClose(g_socket);
                    g_socket = INVALID_SOCKET;
                    g_state = S_DONE;
                }
                break;
            }
            // Socket connected, prepare for sending
            g_tickCount = TickGet();
            g_bufferIndex = 0;
            g_state = S_SEND_REQ;
            break;
        }

        // Send HTTP request
        case S_SEND_REQ: {
            // Check if the socket is ready to send
            if (TCPIsPutReady(g_socket) < 125) {
                _debug("[xdl] socket is not ready to send\r\n");
                g_state = S_DONE;
                break;
            }
            // Send request
            switch (g_dataMode) {
                case XDL_MODE_WEATHER: {
                    if (g_isProxyEnabled) {
                        TCPPutROMArray(g_socket, AW_WDATA_REQ_GET_PART_1_PROXY,
                                AW_WDATA_REQ_GET_PART_1_PROXY_SIZE - 1);
                    } else {
                        TCPPutROMArray(g_socket, AW_WDATA_REQ_GET_PART_1,
                                AW_WDATA_REQ_GET_PART_1_SIZE - 1);
                    }
                    TCPPutROMArray(g_socket, AW_WDATA_LOCATION, 
                            AW_WDATA_LOCATION_SIZE - 1);
                    TCPPutROMArray(g_socket, AW_REQ_GET_PART_2,
                            AW_REQ_GET_PART_2_SIZE - 1);
                    TCPPutROMArray(g_socket, AW_REQ_TAIL, 
                            AW_REQ_TAIL_SIZE - 1);

                    break;
                }

                case XDL_MODE_LOCATION:
                {
                    // Location mode implementation goes here
                    /** @todo implement location mode request sending */
                    break;
                }
            }

            _debug("[xdl] request sent, waiting for response\r\n");

            g_bufferIndex = 0;
            g_state = S_RECEIVE_RESP;
            break;
        }

        // Receive server response
        case S_RECEIVE_RESP: {
            unsigned char buf[64];
            unsigned int byteCount;

            // Check if connection is still alive
            if (!TCPIsConnected(g_socket)) {
                _debug("[xdl] connection reset\r\n");
                g_state = S_CLOSE;
//                TCPClose(g_socket);
//                g_socket = INVALID_SOCKET;
//                break;
            } else {
                byteCount = TCPIsGetReady(g_socket);
                if (byteCount == 0)
                    break;
            
                printf("[xdl] receiving %d bytes\r\n", byteCount);
            }

            while (1) {
                byteCount = TCPGetArray(g_socket, buf, sizeof (buf));
                ext_sram_write_array(RAM_LOC_WDATA_XML_BASE + g_recvBytes, buf,
                        byteCount);
                g_recvBytes += byteCount;
                if (byteCount < sizeof (buf))
                    break;
            }

            break;
        }


//        case S_RECEIVE_RESP: {
//            unsigned char c;
//
//            // Check if connection is still alive
//            if (!TCPIsConnected(g_socket)) {
//                _debug("[xdl] connection reset\r\n");
//                g_state = S_DONE;
//                TCPClose(g_socket);
//                g_socket = INVALID_SOCKET;
//                break;
//            }
//
//            // Get number of received bytes
//            g_recvBytes = TCPIsGetReady(g_socket);
//
//            while (g_recvBytes > 0) {
//                c = 0;
//
//                while (c != '\r' && g_recvBytes > 0) {
//                    if (TCPGet(g_socket, &c)) {
//                        if (c >= ' ')
//                            g_buffer[g_bufferIndex++] = c;
//                        g_recvBytes--;
//                    }
//                }
//
//                if (c == '\r') {
//                    g_buffer[g_bufferIndex] = 0;
//
//                    if (strlen(g_buffer) > 0) {
//                        XMLP_parseLine(g_buffer);
//
//                        if (strncmp(g_buffer, "</adc_database>", 15) == 0) {
//                            _debug("[xdl] download finished.\r\n");
//                            g_state = S_CLOSE;
//                            g_lastError = XDL_NO_ERROR;
//                        }
//
//                        g_bufferIndex = 0;
//                        memset(g_buffer, 0, sizeof (g_buffer));
//                    }
//                }
//            }
//
//            break;
//        }

        // Close connection
        case S_CLOSE: {
            unsigned int i;

            TCPDisconnect(g_socket);
            g_socket = INVALID_SOCKET;
            g_state = S_DONE;

            _debug("[xdl] data:\r\n");
            for (i = RAM_LOC_WDATA_XML_BASE;
                    i < RAM_LOC_WDATA_XML_BASE + g_recvBytes; i++) {
                putch(ext_sram_read_byte(i));
            }

            xmlp_parse(g_recvBytes);

            break;
        }

        // Nothing to do
        case S_DONE: {
            break;
        }
    }
}

unsigned char xmldl_is_finished()
{
    return (g_lastError == XDL_NO_ERROR && g_state == S_DONE);
}

xmldl_error_t xmldl_error()
{
    return g_lastError;
}
