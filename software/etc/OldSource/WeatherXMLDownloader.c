/**
 * Ethernet Weather Station v2 Firmware
 * Weather data downloader module
 *
 * @author Tamas Karpati <tomikaa87>
 * @date 2011-09-08
 * @file WeatherXMLDownloader.c
 */

#include "WeatherXMLDownloader.h"
//#include "WeatherXMLParser.h"
#include "Debug.h"
#include "TCPIPStack/TCPIP.h"
#include "XMLParser.h"

#include "SRAM_23K.h"
#include "CRC16.h"

enum
{
    WXDL_START,
    WXDL_CONNECT,
    WXDL_SEND_REQUEST,
    WXDL_RECEIVE,
    WXDL_CLOSE,
    WXDL_DONE
} tcpClientState = WXDL_DONE;

static TCP_SOCKET socket = INVALID_SOCKET;
unsigned long g_tickCount = 0ul;
unsigned int g_bufferIndex = 0;
unsigned int requestLength = 0;
unsigned char g_buffer[256];
unsigned int receivedBytes;
WXDL_Error g_lastError = WXDL_NO_ERROR;

IP_ADDR serverIp;

unsigned int ram_ptr = 0;
unsigned int ram_crc = 0;
unsigned int ram_len = 0;

// Location search:
// http://forecastfox.accuweather.com/adcbin/forecastfox/locate_city.asp?location=

const unsigned char ServerAddress[] = "forecastfox.accuweather.com";
const unsigned int ServerPort = 80;
/*const unsigned char HttpRequest[] =
    "GET /adcbin/forecastfox/weather_data.asp?location=EUR|HU|HU006|BUDAPEST|&metric=0&partner=forecastfox HTTP/1.1\r\n" \
    "Host: forecastfox.accuweather.com\r\n" \
    "Accept: text/html,application/xml\r\n" \
    "Accept-charset: ISO-8859-2\r\n" \
    "Keep-alive: 115\r\n" \
    "Connection: keep-alive\r\n\r\n\0";*/
const unsigned char HttpRequest[] =
        "GET /adcbin/forecastfox/locate_city.asp?location=boston HTTP/1.1\r\n" \
    "Host: forecastfox.accuweather.com\r\n" \
    "Accept: text/html,application/xml\r\n" \
    "Accept-charset: ISO-8859-2\r\n" \
    "Keep-alive: 115\r\n" \
    "Connection: keep-alive\r\n\r\n\0";

void WXDL_reset()
{
    tcpClientState = WXDL_DONE;
    g_tickCount = 0;
    g_bufferIndex = 0;
    requestLength = 0;
    memset(g_buffer, 0, sizeof (g_buffer));
    g_lastError = WXDL_NO_ERROR;

    XMLP_init();

    if (TCPIsConnected(socket))
        TCPClose(socket);
    socket = INVALID_SOCKET;
}

void WXDL_start()
{
    if (tcpClientState == WXDL_DONE)
    {
        tcpClientState = WXDL_START;
        //WXP_reset();
        XMLP_init();
    }
}

void WXDL_task()
{
    switch (tcpClientState)
    {
        case WXDL_START:
        {
            _debug("TCP: connecting to the server.\r\n");

            g_lastError = WXDL_NO_ERROR;

            socket = TCPOpen((DWORD) & ServerAddress[0], TCP_OPEN_ROM_HOST,
                    ServerPort, TCP_PURPOSE_DEFAULT);

            if (socket == INVALID_SOCKET)
            {
                _debug("TCP: cannot create socket.\r\n");

                g_lastError = WXDL_SOCKET_INVALID;
                tcpClientState = WXDL_DONE;
                break;
            }
            else
                _debug("TCP: socket created.\r\n");

            tcpClientState = WXDL_CONNECT;
            g_tickCount = TickGet();
            break;
        }

        case WXDL_CONNECT:
        {
            if (!TCPIsConnected(socket))
            {
                if (TickGet() - g_tickCount > 5 * TICKS_PER_SECOND)
                {
                    _debug("TCP: connection timed out.\r\n");

                    TCPClose(socket);
                    socket = INVALID_SOCKET;
                    g_lastError = WXDL_TIMEOUT;
                    tcpClientState = WXDL_DONE;
                }

                break;
            }

            g_tickCount = TickGet();
            g_bufferIndex = 0;
            requestLength = strlen((char *) HttpRequest);
            tcpClientState = WXDL_SEND_REQUEST;

            break;
        }

        case WXDL_SEND_REQUEST:
        {
            unsigned int attemptCount;

            if (TCPIsPutReady(socket) < 125u)
            {
                _debug("TCP: not enough buffer space available.\r\n");
                g_lastError = WXDL_NO_BUFFER_SPACE;
                tcpClientState = WXDL_DONE;
                break;
            }

            attemptCount = 0;

            while (g_bufferIndex < requestLength && attemptCount < requestLength * 2)
            {
                attemptCount++;

                if (TCPIsPutReady(socket))
                {
                    TCPPut(socket, HttpRequest[g_bufferIndex]);
                    g_bufferIndex++;
                }
            }

            if (attemptCount > requestLength * 2)
            {
                _debug("TCP: too many send attempts.\r\n");
                g_lastError = WXDL_REQUEST_SEND_FAILED;
                tcpClientState = WXDL_DONE;
                break;
            }

            _debug("TCP: HTTP request sent.\r\n");

            tcpClientState = WXDL_RECEIVE;
            g_bufferIndex = 0;

            ram_ptr = 0;
            CRC16_reset();

            break;
        }

        case WXDL_RECEIVE:
        {
            unsigned char c;

            if (!TCPIsConnected(socket))
            {
                _debug("TCP: socket disconnected.\r\n");
                g_lastError = WXDL_DOWNLOAD_FAILED;
                tcpClientState = WXDL_DONE;
                break;
            }

            receivedBytes = TCPIsGetReady(socket);

            while (receivedBytes > 0)
            {
                c = 0;

                while (c != '\r' && receivedBytes > 0)
                {
                    if (TCPGet(socket, &c))
                    {
                        if (c >= ' ')
                            g_buffer[g_bufferIndex++] = c;
                        receivedBytes--;
                    }
                }

                if (c == '\r')
                {
                    g_buffer[g_bufferIndex] = 0;

                    if (strlen(g_buffer) > 0)
                    {
                        //_debug("XML: %s\r\n", buffer);

                        //WXP_parseLine(buffer);
                        XMLP_parseLine(g_buffer);

                        if (strncmp(g_buffer, "</adc_database>", 15) == 0)
                        {
                            _debug("TCP: download finished.\r\n");
                            tcpClientState = WXDL_CLOSE;
                            g_lastError = WXDL_NO_ERROR;

                            /*_debug("Checking RAM CRC... ");
                            ram_crc = CRC16_value();
                            ram_len = ram_ptr;
                            _debug("RAM content length: %u B\r\n", ram_len);
                            CRC16_reset();
                            for (ram_ptr = 0; ram_ptr < ram_len; ram_ptr++)
                                CRC16_byte(EXT_SRAM_readByte(ram_ptr));
                            if (ram_crc != CRC16_value())
                                _debug("FAILED\r\n");
                            else
                                _debug("OK\r\n");
                                
                            _debug("RAM CRC: %04X, content CRC: %04X\r\n",
                                CRC16_value(), ram_crc);
                                
                            _debug("RAM contents:\r\n");
                            for (ram_ptr = 0; ram_ptr < ram_len; ram_ptr++)
                                putch(EXT_SRAM_readByte(ram_ptr));*/
                        }

                        g_bufferIndex = 0;
                        memset(g_buffer, 0, sizeof (g_buffer));
                    }
                }
            }

            break;
        }

        case WXDL_CLOSE:
        {
            TCPDisconnect(socket);
            socket = INVALID_SOCKET;
            g_lastError = WXDL_NO_ERROR;
            tcpClientState = WXDL_DONE;

            break;
        }

        case WXDL_DONE:
        {
            break;
        }
    }
}

unsigned char WXDL_isFinished()
{
    return (g_lastError == WXDL_NO_ERROR && tcpClientState == WXDL_DONE);
}

WXDL_Error WXDL_error()
{
    return g_lastError;
}
