/**
 * Ethernet Weather Station v2 Firmware
 * Weather data storage module
 *
 * @author Tamas Karpati <tomikaa87>
 * @date 2012-06-15
 * @file WeatherData.c
 */

#include "WeatherData.h"
#include "drivers/SRAM_23K.h"

#include <stdio.h>
#define _debug printf

//void WDATA_getNumericData(uint16 address, uint8 size, uint8 fcastDay,
//        WDATA_Union* wu)
//{
//    // Current weather
//    if (fcastDay == 0) {
//        _debug("[wd] %02x%02x[%02u] <- @%04x\r\n", wu->data[0], wu->data[1],
//                size, address);
//        EXT_SRAM_readArray(address, wu->data, size);
//    // Forecast
//    } else {
//        _debug("[wd] %02x%02x[%02u][f:%d] <- @%04x\r\n", wu->data[0], wu->data[1],
//                size, fcastDay, address + WDATA_F_BASE_ADDR +
//                WDATA_F_SIZE * (fcastDay - 1));
//        EXT_SRAM_readArray(address + WDATA_F_BASE_ADDR + WDATA_F_SIZE * (fcastDay - 1),
//                wu->data, size);
//    }
//}
//
//void WDATA_getStringData(uint16 address, uint8 size, uint8 fcastDay, char *buf)
//{
//    // Current weather
//    if (fcastDay == 0) {
//        _debug("[wd] buf [%02u] <- @%04x\r\n", size, address);
//        EXT_SRAM_readArray(address, buf, size);
//    // Forecast
//    } else {
//        _debug("[wd] buf [%02u][f:%d] <- @04x\r\n", size, fcastDay,
//                address + WDATA_F_BASE_ADDR + WDATA_F_SIZE * (fcastDay - 1));
//        EXT_SRAM_readArray(address + WDATA_F_BASE_ADDR + WDATA_F_SIZE * (fcastDay - 1),
//                buf, size);
//    }
//}
//
//void WDATA_setNumericData(uint16 address, uint8 size, uint8 fcastDay,
//        WDATA_Union *wu)
//{
//    // Current weather
//    if (fcastDay == 0) {
//        _debug("[wd] %02x%02x[%02u] -> @%04x\r\n", wu->data[0], wu->data[1],
//                size, address);
//        EXT_SRAM_writeArray(address, wu->data, size);
//    // Forecast
//    } else {
//        _debug("[wd] %02x%02x[%02u][f:%d] -> @%04x\r\n", wu->data[0], wu->data[1],
//                size, fcastDay, address + WDATA_F_BASE_ADDR +
//                WDATA_F_SIZE * (fcastDay - 1));
//        EXT_SRAM_writeArray(address + WDATA_F_BASE_ADDR + WDATA_F_SIZE * (fcastDay - 1),
//                wu->data, size);
//    }
//}
//
//void WDATA_setStringData(uint16 address, uint8 size, uint8 fcastDay, char *buf)
//{
//    // Current weather
//    if (fcastDay == 0) {
//        _debug("[wd] buf [%02u] -> @%04x\r\n", size, address);
//        EXT_SRAM_writeArray(address, buf, size);
//    // Forecast
//    } else {
//        _debug("[wd] buf [%02u][f:%d] -> @04x\r\n", size, fcastDay,
//                address + WDATA_F_BASE_ADDR + WDATA_F_SIZE * (fcastDay - 1));
//        EXT_SRAM_writeArray(address + WDATA_F_BASE_ADDR + WDATA_F_SIZE * (fcastDay - 1),
//                buf, size);
//    }
//}

void wdata_write(wdata_ctx_t *ctx)
{
    switch (ctx->io) {
        // Store numeric data
        case WDATA_NUM:
            switch (ctx->type) {
                // Current weather data
                case WDATA_CURRENT:
                    printf("[wd-s] %02x%02x [%d] -> %04x\r\n",
                            ctx->wu.data[0], ctx->wu.data[1], ctx->size,
                            ctx->wdata);
                    ext_sram_write_array(ctx->wdata, ctx->wu.data, ctx->size);
                    break;

                // Forecast data
                case WDATA_FORECAST:
                    printf("[wd-s] buf [%d] -> %04x\r\n", ctx->size, ctx->wdata);
                    ext_sram_write_array(ctx->wdata + WDATA_F_BASE_ADDR +
                            WDATA_F_SIZE * (ctx->fcDay - 1), ctx->wu.data,
                            ctx->size);
                    break;
            }
            break;

        // Store string data
        case WDATA_BUF:
            switch (ctx->type) {
                // Current weather data
                case WDATA_CURRENT:
                    printf("[wd-s] buf [%d] -> %04x\r\n", ctx->size, ctx->wdata);
                    ext_sram_write_array(ctx->wdata, ctx->buf, ctx->size);
                    break;

                // Forecast data
                case WDATA_FORECAST:
                    printf("[wd-s] fc buf [%d] -> %04x\r\n", ctx->size,
                            ctx->wdata + WDATA_F_BASE_ADDR +
                            WDATA_F_SIZE * (ctx->fcDay - 1));
                    ext_sram_write_array(ctx->wdata + WDATA_F_BASE_ADDR +
                            WDATA_F_SIZE * (ctx->fcDay - 1), ctx->buf,
                            ctx->size);
                    break;
            }
            break;
    }

    // Set IO type to default
    ctx->io = WDATA_NUM;
}

void wdata_read(wdata_ctx_t *ctx)
{
    switch (ctx->io) {
        // Store numeric data
        case WDATA_NUM:
            switch (ctx->type) {
                // Current weather data
                case WDATA_CURRENT:
                    printf("[wd-s] %02x%02x [%d] <- %04x\r\n",
                            ctx->wu.data[0], ctx->wu.data[1], ctx->size,
                            ctx->wdata);
                    ext_sram_read_array(ctx->wdata, ctx->wu.data, ctx->size);
                    break;

                // Forecast data
                case WDATA_FORECAST:
                    printf("[wd-s] buf [%d] <- %04x\r\n", ctx->size, ctx->wdata);
                    ext_sram_read_array(ctx->wdata + WDATA_F_BASE_ADDR +
                            WDATA_F_SIZE * (ctx->fcDay - 1), ctx->wu.data,
                            ctx->size);
                    break;
            }
            break;

        // Store string data
        case WDATA_BUF:
            switch (ctx->type) {
                // Current weather data
                case WDATA_CURRENT:
                    printf("[wd-s] buf [%d] <- %04x\r\n", ctx->size, ctx->wdata);
                    ext_sram_read_array(ctx->wdata, ctx->buf, ctx->size);
                    break;

                // Forecast data
                case WDATA_FORECAST:
                    printf("[wd-s] fc buf [%d] <- %04x\r\n", ctx->size,
                            ctx->wdata + WDATA_F_BASE_ADDR +
                            WDATA_F_SIZE * (ctx->fcDay - 1));
                    ext_sram_read_array(ctx->wdata + WDATA_F_BASE_ADDR +
                            WDATA_F_SIZE * (ctx->fcDay - 1), ctx->buf,
                            ctx->size);
                    break;
            }
            break;
    }

    // Set IO type to default
    ctx->io = WDATA_NUM;
}