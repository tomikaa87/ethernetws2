/**
 * Ethernet Weather Station v2 Firmware
 * Weather data downloader module
 *
 * @author Tamas Karpati <tomikaa87>
 * @date 2011-09-08
 * @file WeatherXMLDownloader.h
 */

#ifndef WEATHERXMLDOWNLOADER_H
#define WEATHERXMLDOWNLOADER_H

typedef enum
{
    WXDL_NO_ERROR,
    WXDL_SOCKET_INVALID,
    WXDL_NO_BUFFER_SPACE,
    WXDL_REQUEST_SEND_FAILED,
    WXDL_DOWNLOAD_FAILED,
    WXDL_TIMEOUT
} WXDL_Error;

void WXDL_reset();
void WXDL_start();
void WXDL_task();

unsigned char WXDL_isFinished();
WXDL_Error WXDL_error();

#endif // WEATHERXMLDOWNLOADER_H