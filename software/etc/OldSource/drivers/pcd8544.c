/******************************************************************************
 * PCD8544 GLCD driver library                                                *
 *                                                                            *
 * \author Tamas Karpati                                                      *
 * \version 1.1.0                                                             *
 * \date 2011-09-28                                                           *
 ******************************************************************************/

#include "graphlcd.h"

#ifdef GLCD_MODULE_PCD8544

/*** GLOBAL DECLARATIONS ******************************************************/

unsigned char scr_buf[GLCD_BUFFER_SIZE];
unsigned int scr_buf_ptr = 0;

/*** PRIVATE FUNCTIONS ********************************************************/

/**
 * Writes the given \p byte to the LCD. The byte is considered as a command is
 * \p is_data is false, otherwise it is considered as a data byte.
 */
void pcd_glcd_write(unsigned char byte, unsigned char is_data)
{
    #ifndef PCD_GLCD_USE_HW_SPI
    unsigned char i;

    PCD_GLCD_DC = is_data > 0 ? 1 : 0;
    PCD_GLCD_SCE = 0;
    for (i = 8; i > 0; i--)
    {
        PCD_GLCD_SCK = 0;
        if (byte >> (i - 1) & 1)
            PCD_GLCD_MOSI = 1;
        else
            PCD_GLCD_MOSI = 0;
        PCD_GLCD_SCK = 1;
    }
    PCD_GLCD_SCE = 1;
    #else
    PCD_GLCD_DC = is_data > 0 ? 1 : 0;
    PCD_GLCD_SCE = 0;
    PCD_GLCD_SSPBUF = byte;
    while (!PCD_GLCD_SSPIF)
        continue;
    PCD_GLCD_SSPIF = 0;
    PCD_GLCD_SCE = 1;
    #endif
}

/**
 * Draws a checkerboard pattern.
 */
void pcd_glcd_test_pattern_1()
{
    unsigned char i, j;

    GLCD_gotoxy(0, 0, 0);

    for (i = 0; i < 42; i++)
    {
        for (j = 0; j < 6; j++)
        {
            pcd_glcd_write(0b10101010, 1);
            pcd_glcd_write(0b01010101, 1);
        }
    }
}

/*** API FUNCTIONS ************************************************************/

/**
 * Initializes the LCD and clears its RAM. This function clears the screen
 * buffer.
 */
void GLCD_init()
{
    unsigned int i;
    unsigned char j;

    PCD_GLCD_SCK_TRIS = 0;
    PCD_GLCD_MOSI_TRIS = 0;
    PCD_GLCD_SCE_TRIS = 0;
    PCD_GLCD_DC_TRIS = 0;
    PCD_GLCD_RST_TRIS = 0;

    PCD_GLCD_SCK = 0;
    PCD_GLCD_MOSI = 0;
    PCD_GLCD_SCE = 0;
    PCD_GLCD_DC = 0;

    #ifdef PCD_GLCD_USE_HW_SPI
        #ifdef PCD_GLCD_SPI_CLK_FOSC_64
    PCD_GLCD_SSPCON1 = 0b00100010;
        #elif defined PCD_GLCD_SPI_CLK_FOSC_16
    PCD_GLCD_SSPCON1 = 0b00100001;
        #elif define PCD_GLCD_SPI_CLK_FOSC_4
    PCD_GLCD_SSPCON1 = 0b00100000;
        #else
            #error PCD8544: Hardware SPI clock frequency undefined.
        #endif
    #endif

    // Reset
    PCD_GLCD_RST = 0;
    for (i = 0; i < 500; i++)
        __delay_us(20);
    PCD_GLCD_RST = 1;
    for (i = 0; i < 500; i++)
        __delay_us(20);

    // Initialize
    pcd_glcd_write(0x21, 0); // Extended command set
    pcd_glcd_write(0xC8, 0); // LCD Vop
    pcd_glcd_write(0x06, 0); // Temp coefficient
    pcd_glcd_write(0x13, 0); // LCD bias
    pcd_glcd_write(0x20, 0); // Standard command set
    pcd_glcd_write(0x09, 0); // Switch on all pixels
    for (i = 0; i < 30000; i++)
        __delay_us(20);
    pcd_glcd_write(0x08, 0);
    pcd_glcd_write(0x0C, 0); // Normal mode

    GLCD_clear();
}

void GLCD_writeBuf(unsigned char invert)
{
    unsigned int i;

    GLCD_gotoxy(0, 0, 0);

    for (i = 0; i < sizeof (scr_buf); i++)
        pcd_glcd_write(invert ? ~scr_buf[i] : scr_buf[i], 1);
}

void GLCD_clear()
{
    unsigned int i;

    for (i = 0; i < sizeof (scr_buf); i++)
    {
        pcd_glcd_write(0, 1);
        scr_buf[i] = 0;
    }

    scr_buf_ptr = 0;
}

void glcd_clear_buf()
{
    unsigned int i;

    for (i = 0; i < sizeof (scr_buf); i++)
        scr_buf[i] = 0;

    scr_buf_ptr = 0;
}

void GLCD_gotoxy(unsigned char x, unsigned char y, unsigned char text)
{
    // PCD8544 doesn't have a text mode
    (void) text;

    pcd_glcd_write(0x40 | y & 0x07, 0);
    pcd_glcd_write(0x80 | x & 0x7F, 0);
}

void GLCD_setPixel(unsigned char x, unsigned char y, unsigned char on)
{
    unsigned int address;

    if (x > GLCD_RESOLUTION_X || y > GLCD_RESOLUTION_Y)
        return;

    address = (unsigned int) y / 8 * GLCD_RESOLUTION_X + (unsigned int) x;

    if (on)
        scr_buf[address] |= (1 << (y % 8));
    else
        scr_buf[address] &= ~(1 << (y % 8));
}

unsigned char glcd_get_pixel(unsigned char x, unsigned char y)
{
    unsigned int address;

    if (x > GLCD_RESOLUTION_X || y > GLCD_RESOLUTION_Y)
        return 255;

    address = (unsigned int) y / 8 * 84 + (unsigned int) x;

    return ((scr_buf[address] & (1 << (y % 8))) > 0 ? 1 : 0);
}

#endif
