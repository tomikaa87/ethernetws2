/**
 * Multi-driver Graphic LCD library
 *
 * \author Tamas Karpati
 * \date 2011-09-29
 * \version 1.0.0
 */

#ifndef GRAPHLCD_H
#define GRAPHLCD_H

#include <htc.h>

#include "HardwareProfile.h"
#include "Types.h"

/*** DRIVER CONFIGURATION *****************************************************/

/*
 * Uncomment the following define corresponding to the type of the LCD module
 */
#define GLCD_MODULE_T6963C
//#define GLCD_MODULE_PCD8544

/*
 * Ucomment the following define to use TEXT mode of T6963C
 */
//#define GLCD_MODULE_T6963C_TEXT_MODE

#ifdef GLCD_MODULE_T6963C_TEXT_MODE
    #define GLCD_MODULE_T6963C_FONT_6x8
#endif

/*
 * Change these values corresponding to the LCD's resolution
 */
#define GLCD_RESOLUTION_X                   128
#define GLCD_RESOLUTION_Y                   128
//#define GLCD_RESOLUTION_X                   84
//#define GLCD_RESOLUTION_Y                   48

/*
 * Uncomment this define to use buffered drawing.
 * Note, that PCD8544 must use a buffer (504 bytes of RAM needed).
 */
//#define GLCD_USE_DRAWING_BUFFER
#define GLCD_USE_SRAM_DRAWING_BUFFER

/*** SANITY CHECK *************************************************************/

#ifdef GLCD_MODULE_T6963C
    #ifdef GLCD_MODULE_PCD8544
        #define GLCD_MULTI_MODULE_ERROR
    #endif
#endif

#ifdef GLCD_MODULE_PCD8544
    #ifdef GLCD_MODULE_T6963C
        #define GLCD_MULTI_MODULE_ERROR
    #endif
#endif

#ifdef GLCD_MULTI_MODULE_ERROR
    #error Only one module can be used at a time.
#endif

#ifdef GLCD_MODULE_PCD8544
    #ifndef GLCD_USE_DRAWING_BUFFER
        #define GLCD_USE_DRAWING_BUFFER
    #endif
#endif

#ifdef GLCD_USE_DRAWING_BUFFER
    #define GLCD_BUFFER_SIZE (GLCD_RESOLUTION_X * GLCD_RESOLUTION_Y / 8)
#endif // GLCD_USE_DRAWING_BUFFER

#ifdef GLCD_USE_SRAM_DRAWING_BUFFER
    #define GLCD_SRAM_BUFFER_SIZE (GLCD_RESOLUTION_X * GLCD_RESOLUTION_Y / 8)
    //#define GLCD_SRAM_BASE_ADDR             0u
    #define GLCD_SRAM_BASE_ADDR     RAM_LOC_GLCD_BUFFER
#endif

/*** HARDWARE CONFIGURATION ***************************************************/

/**
 * Change this value corresponding to PIC's Fosc.
 */
#ifndef _XTAL_FREQ
    #define _XTAL_FREQ                      40000000ul
#endif

#if (defined GLCD_MODULE_T6963C)
    #define GLCD_DATA                       (LATD)
    #define GLCD_DATA_READ                  (PORTD)
    #define GLCD_DATA_TRIS                  (TRISD)
    #define GLCD_WR                         (LATB2)
    #define GLCD_WR_TRIS                    (TRISB2)
    #define GLCD_RD                         (LATB3)
    #define GLCD_RD_TRIS                    (TRISB3)
    #define GLCD_CE                         (LATB4)
    #define GLCD_CE_TRIS                    (TRISB4)
    #define GLCD_CD                         (LATB5)
    #define GLCD_CD_TRIS                    (TRISB5)
    //#define GLCD_RST                        (LATD4)
    //#define GLCD_RST_TRIS                   (TRISD4)
    //#define GLCD_FS                         (LATD5)
    //#define GLCD_FS_TRIS                    (TRISD5)

    #define GLCD_STA0                       (RD0)
    #define GLCD_STA1                       (RD1)
    #define GLCD_STA2                       (RD2)
    #define GLCD_STA3                       (RD3)
    #define GLCD_STA4                       (RD4)
    #define GLCD_STA5                       (RD5)
    #define GLCD_STA6                       (RD6)
    #define GLCD_STA7                       (RD7)
#elif (defined GLCD_MODULE_PCD8544)
    #define PCD_GLCD_USE_HW_SPI

    /**
     * Uncomment the define corresponding to the desired SPI clock frequency.
     * PCD8544 can handle maximum 4 MHz.
     */
    //#define PCD_GLCD_SPI_CLK_FOSC_64
    #define PCD_GLCD_SPI_CLK_FOSC_16
    //#define PCD_GLCD_SPI_CLK_FOSC_4

    #define PCD_GLCD_SCK                    (LATC3)
    #define PCD_GLCD_SCK_TRIS               (TRISC3)
    #define PCD_GLCD_MOSI                   (LATC5)
    #define PCD_GLCD_MOSI_TRIS              (TRISC5)
    #define PCD_GLCD_SCE                    (LATD0)
    #define PCD_GLCD_SCE_TRIS               (TRISD0)
    #define PCD_GLCD_RST                    (LATD1)
    #define PCD_GLCD_RST_TRIS               (TRISD1)
    #define PCD_GLCD_DC                     (LATD2)
    #define PCD_GLCD_DC_TRIS                (TRISD2)
    #define PCD_GLCD_SSPCON1                (SSPCON1)
    #define PCD_GLCD_SSPBUF                 (SSPBUF)
    #define PCD_GLCD_SSPIF                  (SSPIF)
#endif

/*** GLOBAL CONSTANTS *********************************************************/

// Color Constants
#define GLCD_COLOR_BLACK            1
#define GLCD_COLOR_WHITE            0

// Fill constants
#define GLCD_NO_FILL                0
#define GLCD_FILL                   1

/*** API FUNCTIONS ************************************************************/

void glcd_init();

void glcd_write_buf(uint8 invert);
void glcd_write_buf_region(uint8 x1, uint8 y1,
        uint8 x2, uint8 y2, uint8 invert);

void glcd_clear();
void glcd_clear_buf();

void glcd_set_buf_byte(uint16 address, uint8 data);

void glcd_gotoxy(uint8 x, uint8 y, uint8 text);

void glcd_set_pixel(uint8 x, uint8 y, uint8 on);
uint8 glcd_get_pixel(uint8 x, uint8 y);

void glcd_draw_rect(uint8 x1, uint8 y1, uint8 x2, uint8 y2, uint8 fill,
        uint8 color);
void glcd_draw_bitmap(uint8 *image, uint8 width, uint8 height, uint8 x, uint8 y,
        uint8 invert);
void glcd_draw_bar(uint8 value, uint8 max, uint8 x, uint8 y, uint8 width,
        uint8 height, uint8 invert);
void glcd_draw_line(uint8 x1, uint8 y1, uint8 x2, uint8 y2, uint8 color);
void glcd_draw_inv_rect(uint8 x1, uint8 y1, uint8 x2, uint8 y2);

#endif // GRAPHLCD_H
