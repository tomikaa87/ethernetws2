/*
 * Weather Station LCD Board
 * 25LC1024 SPI EEPROM Driver
 *
 * Author: Tamas Karpati
 * Date: 2010-09-21
 */

#include "25LC1024.h"

//#include <stdio.h>

#ifdef HARDWARE_PROFILE_H
    #define _eep_get_bus_ctrl() { EXT_SRAM_CS = 1; ENC_CS_IO = 1; }
#else
    #define _eep_get_bus_ctrl()
#endif

/*** Constant definitions *****************************************************/

#define EXT_EEPROM_CMD_READ             0b00000011
#define EXT_EEPROM_CMD_WRITE            0b00000010
#define EXT_EEPROM_CMD_WREN             0b00000110
#define EXT_EEPROM_CMD_WRDI             0b00000100
#define EXT_EEPROM_CMD_RDSR             0b00000101
#define EXT_EEPROM_CMD_WRSR             0b00000001
#define EXT_EEPROM_CMD_PE               0b01000010
#define EXT_EEPROM_CMD_SE               0b11011000
#define EXT_EEPROM_CMD_CE               0b11000111
#define EXT_EEPROM_CMD_RDID             0b10101011
#define EXT_EEPROM_CMD_DPD              0b10111001

/*** Low level functions ******************************************************/

#define _eep_clear_sspif()   { EXT_EEPROM_SSPIF = 0; }
#define _eep_wait_for_data() { while (!EXT_EEPROM_SSPIF); EXT_EEPROM_SSPIF = 0; }

/**
 * Sends a byte to the EEPROM.
 */
void _eep_write(uint8 byte)
{
#ifdef EXT_EEPROM_USE_HW_SPI
    _eep_clear_sspif();
    EXT_EEPROM_SSPBUF = byte;
    _eep_wait_for_data();
#else
    uint8 i, val;

    for (i = 0; i < 8; i++)
    {
        val = (byte & (1 << (7 - i))) > 0;
        val ? putch('1') : putch('0');
        EXT_EEPROM_SDO = val;
        EXT_EEPROM_SCK = 1;
        EXT_EEPROM_SCK = 0;
    }
#endif
}

/**
 * Ready a data byte from the EEPROM.
 */
uint8 _eep_read()
{
#ifdef EXT_EEPROM_USE_HW_SPI
    _eep_clear_sspif();
    EXT_EEPROM_SSPBUF = 0; // Send dummy byte to generate SCK
    _eep_wait_for_data();
    return EXT_EEPROM_SSPBUF;
#else
    uint8 i, byte;

    byte = 0;
    for (i = 0; i < 8; i++)
    {
        byte |= EXT_EEPROM_SDI << (7 - i);
        EXT_EEPROM_SDI > 0 ? putch('1') : putch('0');
        EXT_EEPROM_SCK = 1;
        EXT_EEPROM_SCK = 0;
    }

    return byte;
#endif
}

void _eep_wren()
{
    EXT_EEPROM_CS = 0;
    _eep_write(EXT_EEPROM_CMD_WREN);
    EXT_EEPROM_CS = 1;
}

void _eep_write_address(uint24 address)
{
    // Write address<23:16> byte
    _eep_write((uint8) ((address >> 16) & 0xFF));
    // Write address<15:8> byte
    _eep_write((uint8) ((address >> 8) & 0xFF));
    // Write address<7:0> byte
    _eep_write((uint8) (address & 0xFF));
}

/**
 * Reads the status register of the EEPROM and returns its value.
 */
uint8 _eep_read_status_reg()
{
    uint8 data;

    EXT_EEPROM_CS = 0;
    _eep_write(EXT_EEPROM_CMD_RDSR);
    data = _eep_read();
    EXT_EEPROM_CS = 1;

    return data;
}

/**
 * Changes the stored value of the EEPROM's status register to the given
 * \p value.
 */
void _eep_write_status_reg(uint8 value)
{
    EXT_EEPROM_CS = 0;
    _eep_write(EXT_EEPROM_CMD_WRSR);
    _eep_write(value);
    EXT_EEPROM_CS = 1;
}

/**
 * Checks if the EEPROM has finished the previous write operation.
 */
uint8 _eep_is_ready()
{
    uint8 data;

    EXT_EEPROM_CS = 0;
    _eep_write(EXT_EEPROM_CMD_RDSR);
    data = _eep_read();
    EXT_EEPROM_CS = 1;

    return !(data & 0x01);
}

/*** API function implementations *********************************************/

/**
 * Initializes the external EEPROM.
 */
void ext_eeprom_init()
{
    EXT_EEPROM_CS = 1;
    EXT_EEPROM_CS_TRIS = 0;

    EXT_EEPROM_SDI_TRIS = 1;
    EXT_EEPROM_SCK_TRIS = 0;
    EXT_EEPROM_SDO_TRIS = 0;

#ifdef EXT_EEPROM_USE_HW_SPI
    #ifndef EEPROM_DONT_MODIFY_SSPCON
    EXT_EEPROM_SSPCON1 = EXT_EEPROM_SSPCON1_VALUE;
    EXT_EEPROM_SSPSTATbits.CKE = 1;
    EXT_EEPROM_SSPSTATbits.SMP = 0;
    #endif
#endif

    // Check write protection and disable it if necessary
    _eep_get_bus_ctrl();
    if (_eep_read_status_reg() & 0b10001100)
        _eep_write_status_reg(0x00);
}

/**
 * Writes the given \p data byte into the EEPROM. The address is specified
 * by \p address.
 *
 * @return The number of written bytes.
 */
uint8 ext_eeprom_write_byte(uint24 address, uint8 byte)
{
    if (address > EXT_EEPROM_MAX_ADDRESS)
        return 0;

    // Get control over the SPI bus
    _eep_get_bus_ctrl();

    // Wait for the chip to complete the previous write
    while (!_eep_is_ready());

    // EEPROM write enable
    _eep_wren();
    // Select the chip
    EXT_EEPROM_CS = 0;
    // Instruction
    _eep_write(EXT_EEPROM_CMD_WRITE);
    // Address
    _eep_write_address(address);
    // Data
    _eep_write(byte);
    // Deselect the chip
    EXT_EEPROM_CS = 1;

    return 1;
}

/**
 *
 *
 * @return The number of written bytes.
 */
uint24 ext_eeprom_write_array(uint24 address, uint8 *buf, uint24 length)
{
    uint24 buf_ptr;

    // Get control over the SPI bus
    _eep_get_bus_ctrl();

    if (address + length > EXT_EEPROM_MAX_ADDRESS + 1)
        return 0;

    //printf("EEP: begining address: %05X\r\n", address);
    //printf("EEP: buffer size: %lu\r\n", buf_size);

    // Wait for the chip to complete the previous write
    while (!_eep_is_ready());

    // EEPROM write enable
    _eep_wren();
    // Select the chip
    EXT_EEPROM_CS = 0;
    // Write instruction
    _eep_write(EXT_EEPROM_CMD_WRITE);
    // Address
    _eep_write_address(address);

    buf_ptr = 0;
    while (buf_ptr < length && (address + buf_ptr) <= EXT_EEPROM_MAX_ADDRESS)
    {
        // Check if the address is the begining of the next page
        if (buf_ptr > 0 && (address + buf_ptr) % EXT_EEPROM_PAGE_SIZE == 0)
        {
            //printf("EEP: commit page #%u\r\n", (address + buf_ptr - 1) / EXT_EEPROM_PAGE_SIZE);

            // Commit changes of the current page before sending the next byte
            EXT_EEPROM_CS = 1;

            // Wait for the chip to complete the previous write
            while (!_eep_is_ready());

            // EEPROM write enable
            _eep_wren();
            // Select the chip
            EXT_EEPROM_CS = 0;
            // Write instruction
            _eep_write(EXT_EEPROM_CMD_WRITE);
            // Address
            _eep_write_address(address + buf_ptr);
        }

        //printf("EEP: write: %02X -> %05X\r\n", buf[buf_ptr], buf_ptr + address);

        _eep_write(buf[buf_ptr++]);
    }

    EXT_EEPROM_CS = 1;

    return buf_ptr;
}

/**
 * Reads a byte from the EEPROM. The address is specified by \p address.
 */
uint8 ext_eeprom_read_byte(uint24 address)
{
    uint8 data;

    // Get control over the SPI bus
    _eep_get_bus_ctrl();

    // Wait for the chip to complete the previous write
    while (!_eep_is_ready());

    // Select the chip
    EXT_EEPROM_CS = 0;
    // Instruction
    _eep_write(EXT_EEPROM_CMD_READ);
    // Address
    _eep_write_address(address);
    // Read data
    data = _eep_read();
    // Deselect the chip
    EXT_EEPROM_CS = 1;

    return data;
}

/**
 * Writes the contents of the given \p buf buffer into the EEPROM. \p buf_size
 * holds the size of the buffer (in bytes). The address of the first byte is
 * held by \p address.
 */
uint24 ext_eeprom_read_array(uint24 address, uint8 *buf, uint24 length)
{
    uint24 bufPtr;

    // Get control over the SPI bus
    _eep_get_bus_ctrl();

    // Wait for the chip to complete the previous write
    while (!_eep_is_ready());

    // Select the chip
    EXT_EEPROM_CS = 0;
    // Instruction
    _eep_write(EXT_EEPROM_CMD_READ);
    // Address
    _eep_write_address(address);

    // Read data bytes
    bufPtr = 0;
    while (bufPtr < length && bufPtr <= EXT_EEPROM_MAX_ADDRESS)
        buf[bufPtr++] = _eep_read();

    EXT_EEPROM_CS = 1;

    return bufPtr;
}
