/**
 * Ethernet Weather Station v2 Firmware
 * External SPI SRAM (23Kxxx) driver
 *
 * @author Tamas Karpati <tomikaa87>
 * @date 2011-10-09
 * @file SRAM_23K.h
 */

#include "SRAM_23K.h"

#ifdef HARDWARE_PROFILE_H
    #define _sram_get_bus_ctrl() { EXT_EEPROM_CS = 1; ENC_CS_IO = 1; }
#else
    #define _sram_get_bus_ctrl()
#endif

/*** Global variables *********************************************************/

static enum
{
    EXT_SRAM_MODE_BYTE = 0b00000000,
    EXT_SRAM_MODE_SEQUENTIAL = 0b01000000,
    EXT_SRAM_MODE_PAGE = 0b10000000
} g_sramMode = EXT_SRAM_MODE_BYTE;

/*** Constant definitions *****************************************************/

#define EXT_SRAM_CMD_READ               0b00000011
#define EXT_SRAM_CMD_WRITE              0b00000010
#define EXT_SRAM_CMD_RDSR               0b00000101
#define EXT_SRAM_CMD_WRSR               0b00000001

/*** Low level functions ******************************************************/

#define _sram_clear_sspif()    { EXT_SRAM_SSPIF = 0; }
#define _sram_wait_for_data()  { while (!EXT_SRAM_SSPIF); EXT_SRAM_SSPIF = 0; }

/**
 * Sends a \p byte through SPI.
 */
void _sram_write_byte(uint8 byte)
{
#ifdef EXT_SRAM_USE_HW_SPI
    _sram_clear_sspif();
    EXT_SRAM_SSPBUF = byte;
    _sram_wait_for_data();
#else
    uint8 i, data;

    for (i = 0; i < 8; i++)
    {
        data = (byte & (1 << (7 - i))) > 0;
        EXT_SRAM_SDO = data;
        EXT_SRAM_SCK = 1;
        EXT_SRAM_SCK = 0;
    }
#endif
}

/**
 * Reads a byte from the SPI bus.
 */
uint8 _sram_read_byte()
{
#ifdef EXT_SRAM_USE_HW_SPI
    _sram_clear_sspif();
    EXT_SRAM_SSPBUF = 0; // Dummy byte to generate SCK
    _sram_wait_for_data();
    return EXT_SRAM_SSPBUF;
#else
    uint8 i, data;

    data = 0;
    for (i = 0; i < 8; i++)
    {
        data |= EXT_SRAM_SDI << (7 - i);
        EXT_SRAM_SCK = 1;
        EXT_SRAM_SCK = 0;
    }

    return data;
#endif
}

/**
 * Sends the given 16 bit \p address to the SRAM.
 */
void _sram_write_address(uint16 address)
{
    // Write address<15:8> byte
    _sram_write_byte((uint8) ((address >> 8) & 0xFF));
    // Write address<7:0> byte
    _sram_write_byte((uint8) (address & 0xFF));
}

/**
 * Reads the Status Register.
 */
uint8 _sram_read_status()
{
    _sram_write_byte(EXT_SRAM_CMD_RDSR);
    return _sram_read_byte();
}

/**
 * Writes the given \p data to the Status Register.
 */
void _sram_write_status(uint8 data)
{
    _sram_write_byte(EXT_SRAM_CMD_WRSR);
    _sram_write_byte(data);
}

/**
 * Changes the data mode (byte, page, sequential).
 */
void _sram_set_mode(uint8 mode)
{
    if (mode != g_sramMode)
    {
        EXT_SRAM_CS = 0;

        _sram_write_status(mode);
        g_sramMode = mode;

        EXT_SRAM_CS = 1;
    }
}

/*** API function implementations *********************************************/

/**
 * Initializes the SPI SRAM and the SPI bus.
 */
void ext_sram_init()
{
    EXT_SRAM_CS = 1;
    EXT_SRAM_CS_TRIS = 0;

    EXT_SRAM_SDI_TRIS = 1;
    EXT_SRAM_SCK_TRIS = 0;
    EXT_SRAM_SDO_TRIS = 0;

#ifdef EXT_SRAM_USE_HW_SPI
    #ifndef EXT_SRAM_DONT_MODIFY_SSPCON
    EXT_SRAM_SSPCON1 = EXT_SRAM_SSPCON1_VALUE;
    EXT_SRAM_SSPSTATbits.CKE = 1;
    EXT_SRAM_SSPSTATbits.SMP = 0;
    #endif
#endif
}

/**
 * Writes the given \p byte to the given \p address. If the address is above
 * the maximum allowed, the function returs 0, otherwise 1.
 */
uint8 ext_sram_write_byte(uint16 address, uint8 byte)
{
#ifdef EXT_SRAM_DO_SANITY_CHECK
    if (address > EXT_SRAM_MAX_ADDRESS)
        return 0;
#endif

    // Get control over the SPI bus
    _sram_get_bus_ctrl();

    _sram_set_mode(EXT_SRAM_MODE_BYTE);

    EXT_SRAM_CS = 0;

    _sram_write_byte(EXT_SRAM_CMD_WRITE);
    _sram_write_address(address);
    _sram_write_byte(byte);

    EXT_SRAM_CS = 1;

    _sram_clear_sspif();

    return 1;
}

/**
 * Reads a byte from the given \p address. If the address is above the
 * maximum allowed, the function returns 0xFF, otherwise the read byte.
 */
uint8 ext_sram_read_byte(uint16 address)
{
    uint8 data;

#ifdef EXT_SRAM_DO_SANITY_CHECK
    if (address > EXT_SRAM_MAX_ADDRESS)
        return 0;
#endif        

    // Get control over the SPI bus
    _sram_get_bus_ctrl();

    _sram_set_mode(EXT_SRAM_MODE_BYTE);

    EXT_SRAM_CS = 0;

    _sram_write_byte(EXT_SRAM_CMD_READ);
    _sram_write_address(address);
    data = _sram_read_byte();

    EXT_SRAM_CS = 1;

    _sram_clear_sspif();

    return data;
}

/**
 * Writes the content of the given \p buf buffer into the given \p page of the
 * SRAM. Page offset can be adjusted by \p offset. The size of the buffer is
 * held by \p bufSize.
 * The function returns the number of written bytes or 0 if there was an error.
 */
uint8 ext_sram_write_page(uint16 page, uint8 offset, uint8 *buf, uint8 length)
{
    uint8 buf_ptr;

#ifdef EXT_SRAM_DO_SANITY_CHECK
    if (page > EXT_SRAM_MAX_PAGE)
        return 0;

    if (offset > (uint8) EXT_SRAM_PAGE_SIZE - 1)
        return 0;

    if (length > (uint8) EXT_SRAM_PAGE_SIZE - offset)
        return 0;
#endif

    // Get control over the SPI bus
    _sram_get_bus_ctrl();

    _sram_set_mode(EXT_SRAM_MODE_PAGE);

    EXT_SRAM_CS = 0;

    _sram_write_byte(EXT_SRAM_CMD_WRITE);
    _sram_write_address(EXT_SRAM_PAGE_SIZE * page + (uint16) offset);

    buf_ptr = 0;
    while (buf_ptr < length)
        _sram_write_byte(buf[buf_ptr++]);

    EXT_SRAM_CS = 1;

    _sram_clear_sspif();

    return buf_ptr;
}

/**
 * Reads the content of the given \p page of the SRAM and copies it to the given
 * \p buf buffer. Page offset can be adjusted by \p offset. The size of the 
 * buffer is held by \p bufSize.
 * The function returns the number of read bytes or 0 if there was an error.
 */
uint8 ext_sram_read_page(uint16 page, uint8 offset, uint8 *buf, uint8 length)
{
    uint8 buf_ptr;

#ifdef EXT_SRAM_DO_SANITY_CHECK
    if (page > EXT_SRAM_MAX_PAGE)
        return 0;

    if (offset > (uint8) EXT_SRAM_PAGE_SIZE - 1)
        return 0;

    if (length > (uint8) EXT_SRAM_PAGE_SIZE - offset)
        return 0;
#endif

    // Get control over the SPI bus
    _sram_get_bus_ctrl();

    EXT_SRAM_CS = 0;

    _sram_write_byte(EXT_SRAM_CMD_READ);
    _sram_write_address(EXT_SRAM_PAGE_SIZE * page + (uint16) offset);

    buf_ptr = 0;
    while (buf_ptr < length)
        buf[buf_ptr++] = _sram_read_byte();

    EXT_SRAM_CS = 1;

    _sram_clear_sspif();

    return buf_ptr;
}

/**
 * Writes the content of the given \p buf buffer to the SRAM. The begining
 * address is held by \p address and buffer size by \p bufSize.
 * The function returns the number of written bytes of 0 if there was an error.
 */
uint16 ext_sram_write_array(uint16 address, uint8 *buf, uint16 length)
{
    uint16 buf_ptr;

#ifdef EXT_SRAM_DO_SANITY_CHECK
    if (address > EXT_SRAM_MAX_ADDRESS)
        return 0;
#endif      

    // Get control over the SPI bus
    _sram_get_bus_ctrl();

    _sram_set_mode(EXT_SRAM_MODE_SEQUENTIAL);

    EXT_SRAM_CS = 0;

    _sram_write_byte(EXT_SRAM_CMD_WRITE);
    _sram_write_address(address);

    buf_ptr = 0;
    while (buf_ptr < length && buf_ptr <= EXT_SRAM_MAX_ADDRESS)
        _sram_write_byte(buf[buf_ptr++]);

    EXT_SRAM_CS = 1;

    _sram_clear_sspif();

    return buf_ptr;
}

/**
 * Reads the contents of the SRAM from the given \p address. Read data is
 * copied into the given \p buf buffer. The buffer size is held by \p bufSize.
 * The function returns the number of read bytes or 0 if there was an error.
 */
uint16 ext_sram_read_array(uint16 address, uint8 *buf, uint16 length)
{
    uint16 buf_ptr;

#ifdef EXT_SRAM_DO_SANITY_CHECK
    if (address > EXT_SRAM_MAX_ADDRESS)
        return 0;
#endif        

    // Get control over the SPI bus
    _sram_get_bus_ctrl();

    _sram_set_mode(EXT_SRAM_MODE_SEQUENTIAL);

    EXT_SRAM_CS = 0;

    _sram_write_byte(EXT_SRAM_CMD_READ);
    _sram_write_address(address);

    buf_ptr = 0;
    while (buf_ptr < length && buf_ptr <= EXT_SRAM_MAX_ADDRESS)
        buf[buf_ptr++] = _sram_read_byte();

    EXT_SRAM_CS = 1;

    _sram_clear_sspif();

    return buf_ptr;
}

/*** Performance and stability tests ******************************************/

#ifdef EXT_SRAM_COMPILE_STRESS_TEST

/**
 * Performs a performance test without checking if the RAM contents
 * are written correctly.
 */
void EXT_SRAM_writePerformanceTest()
{
    uint16 bufPtr;

    _sram_set_mode(EXT_SRAM_MODE_SEQUENTIAL);

    EXT_SRAM_CS = 0;

    _sram_write_byte(EXT_SRAM_CMD_WRITE);
    _sram_write_address(0ul);

    bufPtr = 0;
    while (bufPtr++ <= EXT_SRAM_MAX_ADDRESS)
        _sram_write_byte(0xAB);

    EXT_SRAM_CS = 1;

    _sram_clear_sspif();
}

/**
 * Performs a performance test without checking if the RAM contents
 * are read correctly.
 */
void EXT_SRAM_readPerformanceTest()
{
    uint16 bufPtr;

    _sram_set_mode(EXT_SRAM_MODE_SEQUENTIAL);

    EXT_SRAM_CS = 0;

    _sram_write_byte(EXT_SRAM_CMD_READ);
    _sram_write_address(0ul);

    bufPtr = 0;
    while (bufPtr++ <= EXT_SRAM_MAX_ADDRESS)
        (void)_sram_read_byte();

    EXT_SRAM_CS = 1;

    _sram_clear_sspif();
}

    #define EXT_SRAM_CRC16_POLYNOMIAL 0x1021
static uint16 EXT_SRAM_CRC16_runningValue;
    #define EXT_SRAM_CRC16_reset() (EXT_SRAM_CRC16_runningValue = 0)
    #define EXT_SRAM_CRC16_value() (EXT_SRAM_CRC16_runningValue)

    void EXT_SRAM_CRC16_byte(uint8 ch)
{
    uint8 i;

    EXT_SRAM_CRC16_runningValue ^= ch;
    i = 8;

    do
    {
        if (EXT_SRAM_CRC16_runningValue & 0x8000)
            EXT_SRAM_CRC16_runningValue <<= 1;
        else
        {
            EXT_SRAM_CRC16_runningValue <<= 1;
            EXT_SRAM_CRC16_runningValue ^= EXT_SRAM_CRC16_POLYNOMIAL;
        }
    }    while (--i);
}

uint16 EXT_SRAM_rnd = 0x1024;

uint16 EXT_SRAM_random()
{
    uint8 msb;

    msb = ((EXT_SRAM_rnd >> 0) ^ (EXT_SRAM_rnd >> 2) ^
            (EXT_SRAM_rnd >> 3) ^ (EXT_SRAM_rnd >> 5)) & 1;
    EXT_SRAM_rnd = (EXT_SRAM_rnd >> 1) | (msb << 15);

    return EXT_SRAM_rnd;
}

//#include <stdio.h>

/**
 * Performs a stability test by filling up the RAM with random bytes and
 * checking the CRC of the read data.
 */
uint8 EXT_SRAM_stabilityTest()
{
    uint16 bufPtr, crc;
    uint8 byte;

    _sram_set_mode(EXT_SRAM_MODE_SEQUENTIAL);

    // Write random data and calculate CRC
    EXT_SRAM_CS = 0;

    _sram_write_byte(EXT_SRAM_CMD_WRITE);
    _sram_write_address(0ul);

    bufPtr = 0;
    EXT_SRAM_CRC16_reset();
    EXT_SRAM_rnd = 0x1024;
    while (bufPtr < EXT_SRAM_MAX_ADDRESS)
    {
        byte = (uint8) (EXT_SRAM_random() % 256);
        _sram_write_byte(byte);
        EXT_SRAM_CRC16_byte(byte);
        bufPtr++;
    }

    crc = EXT_SRAM_CRC16_value();

    EXT_SRAM_CS = 1;

    /*asm("nop");
    asm("nop");*/

    // Read stored data and calculate CRC
    EXT_SRAM_CS = 0;

    _sram_write_byte(EXT_SRAM_CMD_READ);
    _sram_write_address(0ul);

    bufPtr = 0;
    EXT_SRAM_CRC16_reset();
    EXT_SRAM_rnd = 0x1024;
    while (bufPtr < EXT_SRAM_MAX_ADDRESS)
    {
        byte = _sram_read_byte();
        EXT_SRAM_CRC16_byte(byte);
        /*if (byte != EXT_SRAM_random() % 256)
            printf("Error @%04X: %u != %u\r\n", bufPtr, byte, EXT_SRAM_rnd % 256);*/
        bufPtr++;
    }

    EXT_SRAM_CS = 1;

    _sram_clear_sspif();

    return (crc == EXT_SRAM_CRC16_value() ? 1 : 0);
}

#endif // EXT_SRAM_COMPILE_STRESS_TEST
