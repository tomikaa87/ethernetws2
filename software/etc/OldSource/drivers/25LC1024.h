/*
 * Weather Station LCD Board
 * 25LC1024 SPI EEPROM Driver
 *
 * Author: Tamas Karpati
 * Date: 2010-09-21
 */

#ifndef _25LC1024_H
#define _25LC1024_H

#include <htc.h>

#include "HardwareProfile.h"
#include "Types.h"

/*** Constant definitions *****************************************************/

#ifndef HARDWARE_PROFILE_H

#define EXT_EEPROM_SIZE_BYTES           131072ul
#define EXT_EEPROM_MAX_ADDRESS          (EXT_EEPROM_SIZE_BYTES - 1ul)
#define EXT_EEPROM_PAGE_SIZE            256

#define EXT_EEPROM_USE_HW_SPI
//#define EEPROM_DONT_MODIFY_SSPCON

#define EXT_EEPROM_CS                   (LATC2)
#define EXT_EEPROM_SCK                  (LATC3)
#define EXT_EEPROM_SDI                  (RC4)
#define EXT_EEPROM_SDO                  (LATC5)
#define EXT_EEPROM_CS_TRIS              (TRISC2)
#define EXT_EEPROM_SCK_TRIS             (TRISC3)
#define EXT_EEPROM_SDI_TRIS             (TRISC4)
#define EXT_EEPROM_SDO_TRIS             (TRISC5)
#define EXT_EEPROM_SSPBUF               (SSPBUF)
#define EXT_EEPROM_SSPIF                (SSPIF)
#define EXT_EEPROM_SSPCON1              (SSPCON1)
#define EXT_EEPROM_SSPSTATbits          (SSPSTATbits)
#define EXT_EEPROM_SSPCON1_VALUE        0x21

#endif

/*** API functions ************************************************************/

void ext_eeprom_init();
uint8 ext_eeprom_write_byte(uint24 address, uint8 byte);
uint24 ext_eeprom_write_array(uint24 address, uint8 *buf, uint24 length);
uint8 ext_eeprom_read_byte(uint24 address);
uint24 ext_eeprom_read_array(uint24 address, uint8 *buf, uint24 length);

#endif // _25LC1024_H
