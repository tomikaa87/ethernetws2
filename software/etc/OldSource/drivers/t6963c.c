/******************************************************************************
 * T6963C GLCD driver library                                                 *
 *                                                                            *
 * \author Tamas Karpati                                                      *
 * \version 1.2.0                                                             *
 *                                                                            *
 * written in CCS-C                                                           *
 * modified for HI-TECH PIC C - 2010-09-21                                    *
 * modified for screen buffering - 2011-09-09                                 *
 * modified for Unified GLCD driver - 2011-09-29                              *
 *                                                                            *
 ******************************************************************************/

#include <htc.h>
#include <stdlib.h>
#include <stdio.h>

#include "graphlcd.h"

#ifdef GLCD_MODULE_T6963C

/*** CONSTANTS ****************************************************************/

// GLCD timing
#define GLCD_COMMAND_TIME           8
#define GLCD_DATA_TIME              8
#define GLCD_READ_TIME              8
#define GLCD_BUSY_CHECK_TIME        8

// GLCD addresses
#define GLCD_GRAPH_HOME_ADDR        0x0000u
#define GLCD_GRAPH_AREA_ADDR        0x0010u
#define GLCD_TEXT_HOME_ADDR         0x0800u
#define GLCD_TEXT_AREA_ADDR         0x0010u
#define GLCD_CGRAM_HOME_ADDR        0x1000u
#define GLCD_GRAPH_HOME_ADDR_6x8    0x0000u
#define GLCD_GRAPH_AREA_ADDR_6x8    0x0016u
#define GLCD_TEXT_HOME_ADDR_6x8     0x0B00u
#define GLCD_TEXT_AREA_ADDR_6x8     0x0016u
#define GLCD_CGRAM_HOME_ADDR_6x8    0x1600u

// GLCD sizes
#define GLCD_MEM_SIZE               2048u
#define GLCD_MEM_SIZE_6x8           2816u

// Register set commands
#define GLCD_CURSOR_PTR_SET         0x21   //Cursor Pointer Set
#define GLCD_OFFSET_REG_SET         0x22   //Offset Register Set
#define GLCD_ADDR_PTR_SET           0x24   //Address Pointer Set

// Control Word Set commands
#define GLCD_TEXT_HOME_SET          0x40   //Text Home Address Set
#define GLCD_TEXT_AREA_SET          0x41   //Text Area Set
#define GLCD_GRAPH_HOME_SET         0x42   //Graphics Home address Set
#define GLCD_GRAPH_AREA_SET         0x43   //Graphics Area Set

// Mode Set commands (OR with CG rom commands)
#define GLCD_OR_MODE                0x80   //OR mode
#define GLCD_XOR_MODE               0x81   //XOR mode
#define GLCD_AND_MODE               0x83   //AND mode
#define GLCD_TEXT_ATTR_MODE         0x84   //Text Attribute mode
#define GLCD_INT_CG_MODE            0x80   //Internal CG ROM mode
#define GLCD_EXT_CG_MODE            0x88   //External CG ROM mode

// Display Mode commands (OR together required bits)
#define GLCD_DISPLAY_OFF            0x90
#define GLCD_BLINK_ON               0x91
#define GLCD_CURSOR_ON              0x92
#define GLCD_TEXT_ON                0x94
#define GLCD_GRAPH_ON               0x98
#define GLCD_TEXT_AND_GRAPH_ON      0x9C

// Cursor Pattern Select
#define GLCD_CURSOR_1LINE           0xA0
#define GLCD_CURSOR_2LINE           0xA1
#define GLCD_CURSOR_3LINE           0xA2
#define GLCD_CURSOR_4LINE           0xA3
#define GLCD_CURSOR_5LINE           0xA4
#define GLCD_CURSOR_6LINE           0xA5
#define GLCD_CURSOR_7LINE           0xA6
#define GLCD_CURSOR_8LINE           0xA7

// Data Auto Read/Write
#define GLCD_DATA_AUTO_WR           0xB0
#define GLCD_DATA_AUTO_RD           0xB1
#define GLCD_AUTO_DATA_RESET        0xB2

// Data Read/Write
#define GLCD_DATA_WR_INC            0xC0   //Data write and increment addr
#define GLCD_DATA_RD_INC            0xC1   //Data read and increment addr
#define GLCD_DATA_WR_DEC            0xC2   //Data write and decrement addr
#define GLCD_DATA_RD_DEC            0xC3   //Data read and decrement addr
#define GLCD_DATA_WR                0xC4   //Data write - no addr change
#define GLCD_DATA_RD                0xC5   //Data read - no addr change

// Screen Peek
#define GLCD_SCREEN_PEEK            0xE0

// Screen Copy
#define GLCD_SCREEN_COPY            0xE8

// Bit Set/Reset (OR with bit number 0-7)
#define GLCD_BIT_RESET              0xF0
#define GLCD_BIT_SET                0xF8

// Font Constants
#define GLCD_FONT_8x8               0
#define GLCD_FONT_6x8               1

#if (defined GLCD_MODULE_T6963C_TEXT_MODE && defined GLCD_MODULE_T6963C_FONT_6x8)
uint8 glcd_font = GLCD_FONT_6x8;
#else
uint8 glcd_font = GLCD_FONT_8x8;
#endif

uint16 glcd_graph_home = GLCD_GRAPH_HOME_ADDR;
uint16 glcd_graph_area = GLCD_GRAPH_AREA_ADDR;
uint16 glcd_text_home = GLCD_TEXT_HOME_ADDR;
uint16 glcd_text_area = GLCD_TEXT_AREA_ADDR;

#ifdef GLCD_USE_DRAWING_BUFFER
static uint8 scr_buf[GLCD_BUFFER_SIZE];
static uint16 scr_buf_ptr = 0;
#endif

/*** Stuff for SRAM buffering *************************************************/

#ifdef GLCD_USE_SRAM_DRAWING_BUFFER
#include "SRAM_23K.h"

//uint8 sram_buf[32];
//uint16 sram_base_ptr;
#endif

/*** Low level routines ********************************************************/

/**
 * Checks if the LCD is doing something or not
 */
void glcd_busy_check()
{
    uint8 sta0 = 0, sta1 = 0;

    GLCD_DATA_TRIS = 0xFF;

    do
    {
        GLCD_CD = 1;
        GLCD_RD = 0;
        GLCD_WR = 1;
        GLCD_CE = 0;
        __delay_us(GLCD_BUSY_CHECK_TIME);
        sta0 = GLCD_STA0;
        sta1 = GLCD_STA1;
        GLCD_CE = 1;
    } while (!sta0 && !sta1);
}

/**
 * Writes the given @p glcd_data byte to the LCD
 */
void glcd_write_data(uint8 glcd_data)
{
    glcd_busy_check();
    GLCD_DATA_TRIS = 0x00;
    GLCD_DATA = glcd_data;
    GLCD_CD = 0;
    GLCD_WR = 0;
    GLCD_RD = 1;
    GLCD_CE = 0;
    __delay_us(GLCD_DATA_TIME);
    GLCD_CE = 1;
}

/**
 * Writes a command to the LCD
 *
 * @param glcd_command The command code
 */
void glcd_write_command(uint8 glcd_command)
{
    glcd_busy_check();
    GLCD_DATA_TRIS = 0x00;
    GLCD_DATA = glcd_command;
    GLCD_CD = 1;
    GLCD_WR = 0;
    GLCD_RD = 1;
    GLCD_CE = 0;
    __delay_us(GLCD_COMMAND_TIME);
    GLCD_CE = 1;
}

/**
 * Writes a command with TWO (2x8 bit) parameters to the LCD
 *
 * @param glcd_data The data parameter for the command (MSB, LSB)
 * @param glcd_command The command code
 */
void glcd_write_command2(uint16 glcd_data, uint8 glcd_command)
{
    glcd_write_data(glcd_data & 0xFF);
    glcd_write_data(glcd_data >> 8);
    glcd_write_command(glcd_command);
}

/**
 * Writes a command with ONE parameter to the LCD
 *
 * @param glcd_msb The data parameter for the command (MSB)
 * @param glcd_command The command code
 */
void glcd_write_command1(uint8 glcd_msb, uint8 glcd_command)
{
    glcd_write_data(glcd_msb);
    glcd_write_command(glcd_command);
}

/**
 * Reads a byte from the LCD
 */
uint8 glcd_read_data()
{
    uint8 data;

    glcd_write_command(GLCD_DATA_RD);
    glcd_busy_check();
    GLCD_CD = 0;
    GLCD_RD = 0;
    GLCD_WR = 1;
    GLCD_CE = 0;
    __delay_us(GLCD_READ_TIME);
    data = GLCD_DATA_READ;
    GLCD_CE = 1;

    return data;
}

/**
 * Reads a data byte from the LCD and increment the LCD's data pointer
 */
uint8 glcd_read_data_inc()
{
    uint8 data;

    glcd_write_command(GLCD_DATA_RD_INC);
    glcd_busy_check();
    GLCD_CD = 0;
    GLCD_RD = 0;
    GLCD_WR = 1;
    GLCD_CE = 0;
    __delay_us(GLCD_READ_TIME);
    data = GLCD_DATA_READ;
    GLCD_CE = 1;

    return data;
}

/**
 * Sets the memory address pointer of the LCD controller (or the screen buffer)
 * to the given @p address.
 */
void t6963c_set_address_ptr(uint16 address)
{
#ifdef GLCD_USE_DRAWING_BUFFER
    if (address >= sizeof (scr_buf))
        return;

    scr_buf_ptr = address;
#else
    glcd_write_command2(address, GLCD_ADDR_PTR_SET);
#endif
}

/**
 * Writes the given @p byte into the LCD controller memory
 * (or the screen buffer).
 */
void t6963c_write_byte(uint8 byte)
{
#ifdef GLCD_USE_DRAWING_BUFFER
    scr_buf[scr_buf_ptr] = byte;
#else
    glcd_write_command1(byte, GLCD_DATA_WR);
#endif
}

/**
 * Writes the given @p byte into the LCD controller memory
 * (or the screen buffer) and increments the memory address pointer.
 */
void glcd_write_byte_inc(uint8 byte)
{
#ifdef GLCD_USE_DRAWING_BUFFER
    scr_buf[scr_buf_ptr++] = byte;
#else
    glcd_write_command1(byte, GLCD_DATA_WR_INC);
#endif
}

/*** High level routines ******************************************************/

/**
 * Initializes the LCD and clears its RAM. If buffering is enabled, this
 * function clears the screen buffer too.
 */
void glcd_init()
{
    uint16 i;
    
    // Setup control line pins as digital output
    GLCD_WR_TRIS = 0;
    GLCD_RD_TRIS = 0;
    GLCD_CE_TRIS = 0;
    GLCD_CD_TRIS = 0;

#ifdef GLCD_RST
    GLCD_RST_TRIS = 0;
    // Reset GLCD controller
    GLCD_RST = 0;
    __delay_ms(5);
    GLCD_RST = 1;
    __delay_ms(5);
#endif

    // Pull down FS pin to select 8*8 font
#ifdef GLCD_FS
    GLCD_FS_TRIS = 0;
    GLCD_FS = glcd_font;
#endif

#if (defined GLCD_MODULE_T6963C_TEXT_MODE && defined GLCD_MODULE_T6963C_FONT_6x8)
    // Adjust addresses for font
    glcd_graph_home = GLCD_GRAPH_HOME_ADDR_6x8;
    glcd_graph_area = GLCD_GRAPH_AREA_ADDR_6x8;
    glcd_text_home = GLCD_TEXT_HOME_ADDR_6x8;
    glcd_text_area = GLCD_TEXT_AREA_ADDR_6x8;
#endif

    // Set graphics home address
    glcd_write_command2(glcd_graph_home, GLCD_GRAPH_HOME_SET);

    // Set graphics area
    glcd_write_command2(glcd_graph_area, GLCD_GRAPH_AREA_SET);

    // Set text home
    glcd_write_command2(glcd_text_home, GLCD_TEXT_HOME_SET);

    // Set text area
    glcd_write_command2(glcd_text_area, GLCD_TEXT_AREA_SET);

    // Set CGRAM offset
    glcd_write_command2(0x0300u, GLCD_OFFSET_REG_SET);

    // Set OR mode
    glcd_write_command(GLCD_XOR_MODE);

    // Set address pointer
    glcd_write_command2(0x0000u, GLCD_ADDR_PTR_SET);

    // Set graphic mode on
    glcd_write_command(GLCD_GRAPH_ON);

    // Clear the whole memory
    for (i = 0; i < 0x1FFFu; i++)
        glcd_write_command1(0x00, GLCD_DATA_WR_INC);

    // Set text mode
    glcd_write_command(GLCD_TEXT_AND_GRAPH_ON);

    // Set address pointer
    glcd_write_command2(0x0000u, GLCD_ADDR_PTR_SET);

#ifdef GLCD_USE_DRAWING_BUFFER
    // Reset screen buffer
    scr_buf_ptr = 0;
    for (i = 0; i < sizeof (scr_buf); i++)
        scr_buf[i] = 0;
#endif

#ifdef GLCD_USE_SRAM_DRAWING_BUFFER
    ext_sram_init();
    
/*
    sram_base_ptr = 0;
    for (i = 0; i < sizeof (sram_buf); i++)
        sram_buf[i] = 0;
*/

    for (i = GLCD_SRAM_BASE_ADDR; i < GLCD_SRAM_BUFFER_SIZE; i++)
        ext_sram_write_byte(i, 0);

    for (i = GLCD_SRAM_BASE_ADDR; i < GLCD_SRAM_BUFFER_SIZE; i++)
        if (ext_sram_read_byte(i) != 0)
        {
            printf("SRAM init error: data mismatch\r\n");
            break;
        }
#endif
}

/**
 * Writes the given @p c character into the LCD controller and increments
 * the RAM pointer.
 */
void t6963c_putc(char c)
{
    glcd_write_command1(c - 0x20, GLCD_DATA_WR_INC);
}

/**
 * Writes a string on the LCD.
 */
void glcd_puts(char *str)
{
    while (*str)
    {
        glcd_write_command1((*str) - 0x20, GLCD_DATA_WR_INC);
        str++;
    }
}

/**
 * Clears the whole memory of the LCD controller.
 */
void glcd_clear()
{
    uint16 i;

    // Set address pointer
    glcd_write_command2(0x0000u, GLCD_ADDR_PTR_SET);

    for (i = 0; i < 0x1FFFu; i++)
        glcd_write_command1(0x00, GLCD_DATA_WR_INC);
}

/**
 * Clears the screen buffer, or the LCD if buffering is disabled.
 */
void glcd_clear_buf()
{
#ifdef GLCD_USE_DRAWING_BUFFER
    uint16 i;

    for (i = 0; i < sizeof (scr_buf); i++)
        scr_buf[i] = 0;
#elif defined GLCD_USE_SRAM_DRAWING_BUFFER
    uint16 i;
    
    for (i = GLCD_SRAM_BASE_ADDR; i < GLCD_SRAM_BUFFER_SIZE; i++)
        ext_sram_write_byte(i, 0);
#else
    glcd_clear();
#endif
}

/**
 * Sets the "cursor" to the given @p x and @p y coordinates. If the @p text
 * parameter is set to 1, the LCD will change the text mode cursor position.
 * Otherwise, if @p text is 0, the graphic memory address pointer will be
 * changed to the position corresponding to the given coordinates.
 */
void glcd_gotoxy(uint8 x, uint8 y, uint8 text)
{
    uint16 location, home, line;

    if (!text)
    {
        home = glcd_graph_home;
        line = glcd_graph_area;
    }
    else
    {
        home = glcd_text_home;
        line = glcd_text_area;
    }

    location = home + (((uint16) y) * line) + x;
    glcd_write_command2(location, GLCD_ADDR_PTR_SET);
}

/**
 * Switches the pixel at the given @p and @p y coordinates according to @p on.
 */
void glcd_set_pixel(uint8 x, uint8 y, uint8 on)
{
    uint16 address;

#ifdef GLCD_USE_SRAM_DRAWING_BUFFER
    uint8 data;

    address = (((uint16) y) * glcd_graph_area) +
            (uint16) x / (uint16) 8 + GLCD_SRAM_BASE_ADDR;

    data = ext_sram_read_byte(address);

    if (on)
        data |= (1 << (7 - (x % 8)));
    else
        data &= (0xFF - (1 << (7 - (x % 8))));

    ext_sram_write_byte(address, data);

#elif (defined GLCD_USE_DRAWING_BUFFER)
    uint16 address;
    
    address = (((uint16) y) * glcd_graph_area) +
            (uint16) x / (uint16) 8;

    if (on)
        scr_buf[address] |= (1 << (7 - (x % 8)));
    else
        scr_buf[address] &= (0xFF - (1 << (7 - (x % 8))));
#else
    address = glcd_graph_home + (((uint16) y) * glcd_graph_area) + x / 8;
    glcd_write_command2(address, GLCD_ADDR_PTR_SET);

    if (on)
        glcd_write_command(GLCD_BIT_SET | (7 - (x % 8)));
    else
        glcd_write_command(GLCD_BIT_RESET | (7 - (x % 8)));
#endif
}

/**
 * Returns with the state of the pixel at the given @p x and @p y coordinates.
 */
uint8 glcd_get_pixel(uint8 x, uint8 y)
{
    uint16 address;

#ifdef GLCD_USE_SRAM_DRAWING_BUFFER
    uint8 data;

    address = (((uint16) y) * glcd_graph_area) + x / 8 +
            GLCD_SRAM_BASE_ADDR;
    data = ext_sram_read_byte(address);

    return (data & (1 << (7 - (x % 8)))) > 0 ? 1 : 0;

#elif (defined GLCD_USE_DRAWING_BUFFER)
    address = (((uint16) y) * glcd_graph_area) + x / 8;

    return (scr_buf[address] & (1 << (7 - (x % 8)))) > 0 ? 1 : 0;
#else
    uint8 data;

    address = glcd_graph_home + (((uint16) y) * glcd_graph_area) + x / 8;
    glcd_write_command2(address, GLCD_ADDR_PTR_SET);

    data = glcd_read_data();

    return (data & (1 << (7 - (x % 8)))) > 0 ? 1 : 0;
#endif
}

/**
 * Writes the contents of the screen buffer to the LCD controller's memory if
 * screen buffering is enabled. Otherwise it does nothing except when the
 * @p invert parameter is set to 1. In this case it inverts the picture stored
 * in the LCD controller's graphic memory.
 */
void glcd_write_buf(uint8 invert)
{
    uint16 i;

#ifdef GLCD_USE_SRAM_DRAWING_BUFFER
    uint8 data;

    glcd_write_command2(0, GLCD_ADDR_PTR_SET);

    for (i = GLCD_SRAM_BASE_ADDR; i < GLCD_SRAM_BUFFER_SIZE; i++)
    {
        data = ext_sram_read_byte(i);
        glcd_write_command1(invert ? ~data : data, GLCD_DATA_WR_INC);
    }
    
#elif (defined GLCD_USE_DRAWING_BUFFER)
    glcd_write_command2(0, GLCD_ADDR_PTR_SET);

    if (invert)
        for (i = 0; i < sizeof (scr_buf); i++)
        {
            glcd_write_command1(~scr_buf[i], GLCD_DATA_WR_INC);
        }
    else
        for (i = 0; i < sizeof (scr_buf); i++)
        {
            glcd_write_command1(scr_buf[i], GLCD_DATA_WR_INC);
        }
#else
    if (!invert)
        return;

    glcd_write_command2(0, GLCD_ADDR_PTR_SET);

    for (i = glcd_graph_home; i < glcd_graph_home + 2048; i++)
        glcd_write_command1(~glcd_read_data(), GLCD_DATA_WR_INC);
#endif
}

void glcd_write_buf_region(uint8 x1, uint8 y1,
        uint8 x2, uint8 y2, uint8 invert)
{
    uint16 i;

#ifdef GLCD_USE_SRAM_DRAWING_BUFFER
    uint8 data;
#endif

#if (defined GLCD_USE_SRAM_DRAWING_BUFFER || defined GLCD_USE_DRAWING_BUFFER)
    uint16 from_addr, to_addr;

    from_addr = (((uint16) y1) * glcd_graph_area) +
            (uint16) x1 / (uint16) 8;
    to_addr = (((uint16) y2) * glcd_graph_area) +
            (uint16) x2 / (uint16) 8;
#endif

#ifdef GLCD_USE_SRAM_DRAWING_BUFFER
    glcd_write_command2(from_addr, GLCD_ADDR_PTR_SET);

    for (i = GLCD_SRAM_BASE_ADDR + from_addr;
            i < GLCD_SRAM_BASE_ADDR + to_addr; i++)
    {
        data = ext_sram_read_byte(i);
        glcd_write_command1(invert ? ~data : data, GLCD_DATA_WR_INC);
    }

#elif (defined GLCD_USE_DRAWING_BUFFER)
    glcd_write_command2(from_addr, GLCD_ADDR_PTR_SET);

    if (invert)
        for (i = fromAdrress; i < to_addr; i++)
        {
            glcd_write_command1(~scr_buf[i], GLCD_DATA_WR_INC);
        }
    else
        for (i = 0; i < sizeof (scr_buf); i++)
        {
            glcd_write_command1(scr_buf[i], GLCD_DATA_WR_INC);
        }
#else
    if (!invert)
        return;

    glcd_write_command2(from_addr, GLCD_ADDR_PTR_SET);

    for (i = from_addr; i < to_addr; i++)
        glcd_write_command1(~glcd_read_data(), GLCD_DATA_WR_INC);
#endif
}

void glcd_set_buf_byte(uint16 address, uint8 data)
{
#ifdef GLCD_USE_SRAM_DRAWING_BUFFER

    ext_sram_write_byte(GLCD_SRAM_BASE_ADDR + address, data);
    
#elif (defined GLCD_USE_DRAWING_BUFFER)
    
    scr_buf[address] = data;
        
#else
    
    return;
    
#endif
}    

#endif
