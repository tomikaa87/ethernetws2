#include "graphlcd3d.h"
#include "graphlcd.h"

#include <stdio.h>
#include <math.h>

#define RADIANS 3.141592/180

glcd3d_static_vertex_t glcd3d_static_vertex(signed int x, signed int y, signed int z)
{
    glcd3d_static_vertex_t v;
    v.x = x;
    v.y = y;
    v.z = z;

    return v;
}

/**
 * Converts a vertex into a 2D point.
 *
 * @param vertex The vertex which coordinates will be translated to 2D coordinates
 * @param zoom_factor Zoom factor
 * @param x_origin Screen coordinate of the x = 0 point
 * @param y_origin Screen coordinate of the y = 0 point
 */
void glcd3d_convert_vertex(glcd3d_vertex_t *vertex,
        unsigned char zoom_factor,
        unsigned int x_origin,
        unsigned int y_origin)
{
    vertex->screen_x = (unsigned int) (((float) zoom_factor * (float) (vertex->v.x)) / (float) (vertex->v.z)) + x_origin;
    vertex->screen_y = (unsigned int) -(((float) zoom_factor * (float) (vertex->v.y)) / (float) (vertex->v.z)) + y_origin;

    printf("glcd3d_convert_vertex():\r\n");
    printf("    zoom = %u\r\n", zoom_factor);
    printf("    v.x = %d\r\n    v.y = %d\r\n    v.z = %d\r\n",
            vertex->v.x, vertex->v.y, vertex->v.z);
    printf("    sx = %u\r\n    sy = %u\r\n", vertex->screen_x,
            vertex->screen_y);
}

void glcd3d_convert_polygon(glcd3d_polygon_t *polygon,
        unsigned char zoom_factor,
        unsigned int x_origin,
        unsigned int y_origin)
{
    unsigned char v_index;

    for (v_index = 0; v_index < polygon->vertex_count; v_index++)
        glcd3d_convert_vertex(&(polygon->vertices[v_index]),
            zoom_factor, x_origin, y_origin);
}

/**
 * Allocates a polygon in the memory.
 * @param vertex_count The number of vertices contained by the polygon.
 */
/*
glcd3d_polygon_t *glcd3d_new_polygon(unsigned char vertex_count,
        unsigned char color)
{
    glcd3d_polygon_t *polygon;
    polygon = (glcd3d_polygon_t *)malloc(sizeof (glcd3d_polygon_t));
    polygon->vertex_count = vertex_count;
    polygon->vertices = (glcd3d_vertex_t *)malloc(sizeof (glcd3d_vertex_t) *
            vertex_count);
    polygon->color = color;

    return polygon;
}
 */

/**
 * Draws the given \p polygon on the screen.
 */
void glcd3d_draw_polygon(glcd3d_polygon_t *polygon)
{
    /*
        unsigned char v_index;

        printf("glcd3d_draw_polygon():\r\n");
        printf("    color = %u\r\n", polygon->color);

        // If the polygon contains only 1 vertex, draw a point
        if (polygon->vertex_count == 1)
        {
            printf("    1 vertex -> set_pixel(%u, %u)\r\n",
                    polygon->vertices[0].screen_x, polygon->vertices[0].screen_y);

            glcd_set_pixel(polygon->vertices[0].screen_x,
                    polygon->vertices[0].screen_y,
                    polygon->color);
        }
        // If the polygon contains two or more vertices, draw the polygon
        else if (polygon->vertex_count >= 2)
        {
            for (v_index = 0; v_index < polygon->vertex_count - 1; v_index++)
            {
                printf("    line: %u;%u -> %u;%u\r\n",
                        polygon->vertices[v_index].screen_x,
                        polygon->vertices[v_index].screen_y,
                        polygon->vertices[v_index + 1].screen_x,
                        polygon->vertices[v_index + 1].screen_y);

                glcd_draw_line(polygon->vertices[v_index].screen_x,
                        polygon->vertices[v_index].screen_y,
                        polygon->vertices[v_index + 1].screen_x,
                        polygon->vertices[v_index + 1].screen_y,
                        polygon->color);
            }

            printf("    line: %u;%u -> %u;%u\r\n",
                    polygon->vertices[polygon->vertex_count - 1].screen_x,
                    polygon->vertices[polygon->vertex_count - 1].screen_y,
                    polygon->vertices[0].screen_x,
                    polygon->vertices[0].screen_y);

            glcd_draw_line(polygon->vertices[polygon->vertex_count - 1].screen_x,
                    polygon->vertices[polygon->vertex_count - 1].screen_y,
                    polygon->vertices[0].screen_x,
                    polygon->vertices[0].screen_y,
                    polygon->color);
        }
     */

    signed int x_diff_1, x_diff_2, y_diff_1, y_diff_2;
    signed int start_offset, length;
    signed int error_1, error_2;
    signed int offset_1, offset_2;
    signed int count_1, count_2;
    signed int x_unit_1, x_unit_2;
    signed int edges;
    unsigned int first_vertex_index;
    unsigned int min, i;
    unsigned int start_v_1, start_v_2;
    unsigned int end_v_1, end_v_2;
    signed int start_v_1_x, start_v_1_y;
    signed int start_v_2_x, start_v_2_y;
    signed int end_v_1_x, end_v_1_y;
    signed int end_v_2_x, end_v_2_y;

    // Calculate number of edges
    edges = polygon->vertex_count;

    // Determine which vertex is at top of the polygon
    first_vertex_index = 0;
    min = polygon->vertices[0].screen_y;
    for (i = 1; i < polygon->vertex_count; i++)
    {
        if (polygon->vertices[i].screen_y < min)
        {
            first_vertex_index = i;
            min = polygon->vertices[i].screen_y;
        }
    }

    // Find starting and ending vertices of first two edges
    start_v_1 = first_vertex_index;
    start_v_2 = first_vertex_index;
    start_v_1_x = polygon->vertices[start_v_1].screen_x;
    start_v_1_y = polygon->vertices[start_v_1].screen_y;
    start_v_2_x = polygon->vertices[start_v_2].screen_x;
    start_v_2_y = polygon->vertices[start_v_2].screen_y;

    end_v_1 = start_v_1 - 1;
    if (end_v_1 < 0)
        end_v_1 = polygon->vertex_count - 1;

    end_v_1_x = polygon->vertices[end_v_1].screen_x;
    end_v_1_y = polygon->vertices[end_v_1].screen_y;

    end_v_2 = start_v_2 + 1;
    if (end_v_2 == polygon->vertex_count)
        end_v_2 = 0;

    end_v_2_x = polygon->vertices[end_v_2].screen_x;
    end_v_2_y = polygon->vertices[end_v_2].screen_y;

    // Start drawing the polygon
    while (edges > 0)
    {
        offset_1 = GLCD_RESOLUTION_X * start_v_1_y + start_v_1_x;
        offset_2 = GLCD_RESOLUTION_X * start_v_2_y + start_v_2_x;
        error_1 = 0;
        error_2 = 0;

        y_diff_1 = end_v_1_y - start_v_1_y;
        if (y_diff_1 < 0)
            y_diff_1 *= -1;

        y_diff_2 = end_v_2_y - start_v_2_y;
        if (y_diff_2 < 0)
            y_diff_2 *= -1;

        x_diff_1 = end_v_1_x - start_v_1_x;
        if (x_diff_1 < 0)
        {
            x_unit_1 = -1;
            x_diff_1 *= -1;
        }
        else
            x_unit_1 = 1;

        x_diff_2 = end_v_2_x - start_v_2_x;
        if (x_diff_2 < 0)
        {
            x_unit_2 = -1;
            x_diff_2 *= -1;
        }
        else
            x_unit_2 = 1;
    }
}

void glcd3d_rotate_vertex(glcd3d_static_vertex_t *orig, glcd3d_static_vertex_t *rotated,
        signed int x_angle, signed int y_angle, signed int z_angle)
{
    signed int x_temp, y_temp, z_temp;
    signed int x_new, y_new, z_new;
    float x_angle_r, y_angle_r, z_angle_r;
    float x_sin, x_cos, y_sin, y_cos, z_sin, z_cos;

    printf("glcd_rotate_vertex():\r\n");

    x_new = orig->x;
    y_new = orig->y;
    z_new = orig->z;

    printf("    original = %d;%d;%d\r\n", x_new, y_new, z_new);

    // Convert angle degree to Radians
    x_angle_r = (float) x_angle * RADIANS;
    y_angle_r = (float) y_angle * RADIANS;
    z_angle_r = (float) z_angle * RADIANS;

    //printf("    angles = %0.2f;%0.2f;%0.2f\r\n", x_angle_r, y_angle_r, z_angle_r);

    x_sin = sin(x_angle_r);
    x_cos = cos(x_angle_r);
    y_sin = sin(y_angle_r);
    y_cos = cos(y_angle_r);
    z_sin = sin(z_angle_r);
    z_cos = cos(z_angle_r);

    // Rotate around Z
    x_temp = x_new * z_cos - y_new * z_sin;
    y_temp = x_new * z_sin + y_new * z_cos;
    x_new = x_temp;
    y_new = y_temp;

    printf("    z_rot = %d;%d;%d\r\n", x_new, y_new, z_new);

    // Rotate around Y
    x_temp = x_new * y_cos - z_new * y_sin;
    z_temp = x_new * y_sin + z_new * y_cos;
    x_new = x_temp;
    z_new = z_temp;

    printf("    y_rot = %d;%d;%d\r\n", x_new, y_new, z_new);

    // Rotate around X
    y_temp = y_new * x_cos - z_new * x_sin;
    z_temp = y_new * x_sin + z_new * x_cos;

    rotated->x = x_new;
    rotated->y = y_temp;
    rotated->z = z_temp;

    printf("    x_rot = %d;%d;%d\r\n", x_new, y_temp, z_temp);

    /*
        int tempx,tempy,tempz,newx,newy,newz;
      newx=sv->x;
      newy=sv->y;
      newz=sv->z;

            // Change from degree to Radians
      float xAngleR = (float)angle_x*RADIANS;
      float yAngleR = (float)angle_y*RADIANS;
      float zAngleR = (float)angle_z*RADIANS;

      float xSin = sin(xAngleR);
      float xCos = cos(xAngleR);

      float ySin = sin(yAngleR);
      float yCos = cos(yAngleR);

      float zSin = sin(zAngleR);
      float zCos = cos(zAngleR);

    // rotate coord around z axis
      tempx=newx*zCos-newy*zSin;
      tempy=newx*zSin+newy*zCos;
      newx=tempx;
      newy=tempy;

      //then y axis
      tempx=newx*yCos-newz*ySin;
      tempz=newx*ySin+newz*yCos;
      newx=tempx;
      newz=tempz;

      //then x axis
      tempy=newy*xCos-newz*xSin;
      tempz=newy*xSin+newz*xCos;

      v->x=newx;
      v->y=tempy;
      v->z=tempz;
     */
}