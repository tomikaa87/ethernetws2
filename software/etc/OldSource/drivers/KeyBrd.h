/**
 * Ethernet Weather Station v2 Firmware
 * Keyboard driver
 *
 * @author Tamas Karpati <tomikaa87>
 * @date 2011-11-11
 * @file KeyBrd.h
 */

#ifndef __KEYBRD_H
#define __KEYBRD_H

typedef enum
{
    BTN_FUNC_1 = 0x11,
    BTN_UP = 0x12,
    BTN_FUNC_2 = 0x13,
    BTN_LEFT = 0x21,
    BTN_ENTER = 0x22,
    BTN_RIGHT = 0x23,
    BTN_MENU = 0x31,
    BTN_DOWN = 0x32,
    BTN_ESC = 0x33,
    BTN_NONE = 0xFF
} key_code_t;

void key_init();
key_code_t key_task();

#endif // __KEYBRD_H
