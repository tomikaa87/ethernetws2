/*
 * Weather Station LCD Board
 * T6963C LCD Display Driver
 *
 * Author: Tamas Karpati
 * Date: 2010-09-21
 */

#ifndef __glcd_H
#define __glcd_H

#include "HardwareProfile.h"
#include "Types.h"

// Color Constants
#define GLCD_COLOR_BLACK            1
#define GLCD_COLOR_WHITE            0

// Font Constants
#define GLCD_FONT_8x8               0
#define GLCD_FONT_6x8               1

void GLCD_putc(char c);
void GLCD_puts(char *str);

void GLCD_setAddressPointer(uint16 address);

void GLCD_writeByte(uint8 byte);
void GLCD_writeByteInc(uint8 byte);

void GLCD_writeBuf(uint8 invert);

void GLCD_clear();
void GLCD_clearBuf();

//void glcd_gotoxy(int x, int y, uint8 text);

void GLCD_setPixel(uint8 x, uint8 y, uint8 on);
uint8 GLCD_getPixel(uint8 x, uint8 y);

//void glcd_draw_rect(uint8 x1, uint8 y1, uint8 x2,
//        uint8 y2, uint8 fill, uint8 color);
//void glcd_draw_image(uint8 *image, uint8 width,
//        uint8 height, uint8 x, uint8 y,
//        uint8 invert);
//void glcd_draw_bar(uint8 value, uint8 max,
//        uint8 x, uint8 y, uint8 width,
//        uint8 height, uint8 invert);
//void glcd_draw_line(uint8 x1, uint8 y1, uint8 x2,
//        uint8 y2, uint8 color);
//void glcd_draw_rect_inv(uint8 x1, uint8 y1, uint8 x2,
//        uint8 y2);

#endif // __glcd_H
