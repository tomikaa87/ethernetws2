/**
 * Ethernet Weather Station v2 Firmware
 * Keyboard driver
 *
 * @author Tamas Karpati <tomikaa87>
 * @date 2011-11-11
 * @file KeyBrd.c
 */
 
#include "KeyBrd.h"
#include "HardwareProfile.h"
#include "Types.h"

void key_init()
{
    KBD_ROW_1_TRIS = 1;
    KBD_ROW_2_TRIS = 1;
    KBD_ROW_3_TRIS = 1;
    KBD_COL_1_TRIS = 0;
    KBD_COL_2_TRIS = 0;
    KBD_COL_3_TRIS = 0;  
    KBD_COL_1 = 0;
    KBD_COL_2 = 0;
    KBD_COL_3 = 0; 
}    
 
key_code_t key_task()
{
    static uint8 pressTimer = 50u;
    // FIXME isPressed unused
    static uint8 isPressed = 0;
    
    if (pressTimer < 50u)
    {
        // Check if a button is pressed
        KBD_COL_1 = 0;
        KBD_COL_2 = 0;
        KBD_COL_3 = 0;
    
        if (KBD_ROW_1 == 0 || KBD_ROW_2 == 0 || KBD_ROW_3 == 0)
            pressTimer = 0;
        else
            pressTimer++;
            
        return BTN_NONE;   
    }    
    
    KBD_COL_1 = 0;
    if (!KBD_ROW_1) 
    {
        pressTimer = 0;
        return BTN_FUNC_1;
    } 
    else if (!KBD_ROW_2) 
    {
        pressTimer = 0;
        return BTN_LEFT;
    } 
    else if (!KBD_ROW_3) 
    {
        pressTimer = 0;
        return BTN_MENU;
    }
    KBD_COL_1 = 1;
    
    KBD_COL_2 = 0;
    if (!KBD_ROW_1) 
    {
        pressTimer = 0;
        return BTN_UP;
    } 
    else if (!KBD_ROW_2) 
    {
        pressTimer = 0;
        return BTN_ENTER;
    } 
    else if (!KBD_ROW_3) 
    {
        pressTimer = 0;
        return BTN_DOWN;
    }
    KBD_COL_2 = 1;
    
    KBD_COL_3 = 0;
    if (!KBD_ROW_1)
    {
        pressTimer = 0;
        return BTN_FUNC_2;
    } 
    else if (!KBD_ROW_2) 
    {
        pressTimer = 0;
        return BTN_RIGHT;
    } 
    else if (!KBD_ROW_3) 
    {
        pressTimer = 0;
        return BTN_ESC;
    }
    KBD_COL_3 = 1;
    
    return BTN_NONE;
}    
