/**
 * Ethernet Weather Station v2 Firmware
 * LCD User Interface - Text writer
 *
 * @author Tamas Karpati <tomikaa87>
 * @date 2011-12-25
 * @file LCDUI_Menu.h
 */

#ifndef _LCDUI_TEXT_H
#define _LCDUI_TEXT_H

#include "Types.h"

typedef enum
{
    LCDUI_TEXT_FONT_MICRO,
    LCDUI_TEXT_FONT_PF_TEMPESTA,
    LCDUI_TEXT_FONT_PF_TEMPESTA_BOLD,
    LCDUI_TEXT_FONT_LCD_LARGE,
    LCDUI_TEXT_FONT_LCD_SMALL,

    LCDUI_TEXT_FONT_UNKNOWN = 255
} lcdui_text_font_t;

void lcdui_text_draw(uint8 x, uint8 y, const char *text,
        const lcdui_text_font_t font, uint8 noStroke, uint8 invert);

#endif // _LCDUI_TEXT_H