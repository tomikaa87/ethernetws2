/**
 * Ethernet Weather Station v2 Firmware
 * Hardware Profile file - stores hardware spcific settings
 *
 * @author Tamas Karpati <tomikaa87>
 * @date 2011-10-05
 * @file HardwareProfile.h
 */

#ifndef HARDWARE_PROFILE_H
#define HARDWARE_PROFILE_H

#include <htc.h>

/*** ENC28J60 I/O pin configuration *******************************************/

#define ENC_IN_SPI1
#define ENC_RST_TRIS        (TRISE2)
#define ENC_RST_IO          (LATE2)
#define ENC_CS_TRIS         (TRISB1)
#define ENC_CS_IO           (LATB1)
#define ENC_SCK_TRIS        (TRISC3)
#define ENC_SDI_TRIS        (TRISC4)
#define ENC_SDO_TRIS        (TRISC5)
#define ENC_SPI_IF          (SSPIF)
#define ENC_SSPBUF          (SSPBUF)
#define ENC_SPISTAT         (SSPSTAT)
#define ENC_SPISTATbits     (SSPSTATbits)
#define ENC_SPICON1         (SSPCON1)
#define ENC_SPICON1bits     (SSPCON1bits)
#define ENC_SPICON2         (SSPCON2)
//#define ENC_SPICON_VALUE    0x20

/*** Clock frequency macros for TCP stack *************************************/

#define GetSystemClock()        (40000000ul)
#define GetInstructionClock()   (GetSystemClock() / 4)
#define GetPeripheralClock()    GetInstructionClock()
#define _XTAL_FREQ              40000000ul

/*** External EEPROM configuration ********************************************/

#define EXT_EEPROM_SIZE_BYTES           131072ul
#define EXT_EEPROM_MAX_ADDRESS          (EXT_EEPROM_SIZE_BYTES - 1ul)
#define EXT_EEPROM_PAGE_SIZE            256

#define EXT_EEPROM_USE_HW_SPI
///#define EEPROM_DONT_MODIFY_SSPCON

#define EXT_EEPROM_CS                   (LATE1)
#define EXT_EEPROM_SCK                  (LATC3)
#define EXT_EEPROM_SDI                  (RC4)
#define EXT_EEPROM_SDO                  (LATC5)
#define EXT_EEPROM_CS_TRIS              (TRISE1)
#define EXT_EEPROM_SCK_TRIS             (TRISC3)
#define EXT_EEPROM_SDI_TRIS             (TRISC4)
#define EXT_EEPROM_SDO_TRIS             (TRISC5)
#define EXT_EEPROM_SSPBUF               (SSPBUF)
#define EXT_EEPROM_SSPIF                (SSPIF)
#define EXT_EEPROM_SSPCON1              (SSPCON1)
#define EXT_EEPROM_SSPSTATbits          (SSPSTATbits)
#define EXT_EEPROM_SSPCON1_VALUE        0x20

/*** External SRAM configuration **********************************************/

#define EXT_SRAM_SIZE_BYTES             32768u
#define EXT_SRAM_PAGE_SIZE              32u

#define EXT_SRAM_MAX_ADDRESS            (EXT_SRAM_SIZE_BYTES - 1u)
#define EXT_SRAM_MAX_PAGE       (EXT_SRAM_SIZE_BYTES / EXT_SRAM_PAGE_SIZE - 1u)

#define EXT_SRAM_USE_HW_SPI
//#define EXT_SRAM_DONT_MODIFY_SSPCON

#define EXT_SRAM_CS                     (LATE0)
#define EXT_SRAM_SCK                    (LATC3)
#define EXT_SRAM_SDI                    (RC4)
#define EXT_SRAM_SDO                    (LATC5)
#define EXT_SRAM_CS_TRIS                (TRISE0)
#define EXT_SRAM_SCK_TRIS               (TRISC3)
#define EXT_SRAM_SDI_TRIS               (TRISC4)
#define EXT_SRAM_SDO_TRIS               (TRISC5)
#define EXT_SRAM_SSPBUF                 (SSPBUF)
#define EXT_SRAM_SSPIF                  (SSPIF)
#define EXT_SRAM_SSPCON1                (SSPCON1)
#define EXT_SRAM_SSPSTATbits            (SSPSTATbits)
#define EXT_SRAM_SSPCON1_VALUE          0x20

/*
 * Comment out the define to disable sanity checks. It gives more speed on
 * slower microcontrollers, but you must check input addresses and buffer
 * sizes before calling any of the write or read functions.
 */
//#define EXT_SRAM_DO_SANITY_CHECK

/*** UART macros for TCP stack ************************************************/

/*#define BusyUART            ()BusyUSART()
#define CloseUART()CloseUSART()
#define ConfigIntUART(a)ConfigIntUSART(a)
#define DataRdyUART()DataRdyUSART()
#define OpenUART(a,b,c)OpenUSART(a,b,c)
#define ReadUART()ReadUSART()
#define WriteUART(a)WriteUSART(a)
#define getsUART(a,b,c)getsUSART(b,a)
#define putsUART(a)putsUSART(a)
#define getcUART()ReadUSART()
#define putcUART(a)WriteUSART(a)
#define putrsUART(a)putrsUSART((far rom char*)a)*/

/*** EEPROM configuration *****************************************************/

#define CONFIG_DATA_EEPROM_ADDR         0x0000u

/*** Keyboard pin configuration ***********************************************/

#define KBD_ROW_1               RA0
#define KBD_ROW_2               RA1
#define KBD_ROW_3               RA2
#define KBD_COL_1               LATA3
#define KBD_COL_2               LATA4
#define KBD_COL_3               LATA5
#define KBD_ROW_1_TRIS          TRISA0
#define KBD_ROW_2_TRIS          TRISA1
#define KBD_ROW_3_TRIS          TRISA2
#define KBD_COL_1_TRIS          TRISA3
#define KBD_COL_2_TRIS          TRISA4
#define KBD_COL_3_TRIS          TRISA5

/*** RAM locations ************************************************************/

#define RAM_LOC_GLCD_BUFFER         0u          // 2048 B

// Weather data locations
#define RAM_LOC_WDATA_BASE          8048u
#define RAM_LOC_WDATA_XML_BASE      EXT_SRAM_SIZE_BYTES - 6000u

/*** EEPROM locations *********************************************************/

// Weather screen image locations
#define EEPROM_LOC_SPLASH_SCR           0ul
#define EEPROM_LOC_CONFIG_ERR_SCR       2048ul
#define EEPROM_LOC_CURRENT_W_SCR        4096ul
#define EEPROM_LOC_FORE_D_SCR           6144ul
#define EEPROM_LOC_FORE_N_SCR           8192ul

// Font data locations
#define EEPROM_LOC_FONT_MICRO_DATA      20480ul
#define EEPROM_LOC_FONT_MICRO_ATTR      20780ul         // 95+3 bytes (offs+w+h)
#define EEPROM_LOC_FONT_PFT_DATA        20880ul
#define EEPROM_LOC_FONT_PFT_ATTR        21480ul
#define EEPROM_LOC_FONT_PFTB_DATA       21580ul
#define EEPROM_LOC_FONT_PFTB_ATTR       22300ul
#define EEPROM_LOC_FONT_LCDL_DATA       22400ul
#define EEPROM_LOC_FONT_LCDL_ATTR       22800ul
#define EEPROM_LOC_FONT_LCDS_DATA       22900ul
#define EEPROM_LOC_FONT_LCDS_ATTR       23100ul


#endif // HARDWARE_PROFILE_H


