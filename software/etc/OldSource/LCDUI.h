/**
 * Ethernet Weather Station v2 Firmware
 * LCD User Interface - main source
 *
 * @author Tamas Karpati <tomikaa87>
 * @date 2011-12-22
 * @file LCDUI.h
 */

#ifndef _LCDUI_H
#define _LCDUI_H

#include "Types.h"

/*** Constant definitions *****************************************************/

#define LCDUI_BITMAP_SPLASH             0
#define LCDUI_BITMAP_CONFIG_WARNING     2048u

/*** API **********************************************************************/

void lcdui_init();
void lcdui_task();

int8 lcdui_is_on_main_screen();

void LCDUI_setLcdBacklight(const uint8 percent);
void lcdui_draw_fs_bitmap_from_eeprom(const uint24 address);

#endif // _LCDUI_H