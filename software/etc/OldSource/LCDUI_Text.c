/**
 * Ethernet Weather Station v2 Firmware
 * LCD User Interface - Text writer
 *
 * @author Tamas Karpati <tomikaa87>
 * @date 2011-12-25
 * @file LCDUI_Menu.h
 */

#include "LCDUI_Text.h"
#include "HardwareProfile.h"
#include "25LC1024.h"
#include "graphlcd.h"

#include "Debug.h"

/**
 * Obtain font data base addresses for the given \p font. Obtained memory
 * addresses will be loaded into the variables pointed by \p dataAddr and
 * \p attrAddr.
 */
void get_font_base_addr(lcdui_text_font_t font, uint24 *data_addr,
        uint24 *attr_addr)
{
    // Load base addresses for the given font
    switch (font)
    {
        case LCDUI_TEXT_FONT_MICRO:
            *data_addr = EEPROM_LOC_FONT_MICRO_DATA;
            *attr_addr = EEPROM_LOC_FONT_MICRO_ATTR;
            break;

        case LCDUI_TEXT_FONT_PF_TEMPESTA:
            *data_addr = EEPROM_LOC_FONT_PFT_DATA;
            *attr_addr = EEPROM_LOC_FONT_PFT_ATTR;
            break;

        case LCDUI_TEXT_FONT_PF_TEMPESTA_BOLD:
            *data_addr = EEPROM_LOC_FONT_PFTB_DATA;
            *attr_addr = EEPROM_LOC_FONT_PFTB_ATTR;
            break;

        case LCDUI_TEXT_FONT_LCD_LARGE:
            *data_addr = EEPROM_LOC_FONT_LCDL_DATA;
            *attr_addr = EEPROM_LOC_FONT_LCDL_ATTR;
            break;

        case LCDUI_TEXT_FONT_LCD_SMALL:
            *data_addr = EEPROM_LOC_FONT_LCDS_DATA;
            *attr_addr = EEPROM_LOC_FONT_LCDS_ATTR;
            break;
    }
}

/**
 * Draws a \p text at the given \p x \p y coordinates with \p font. If
 * \p no_outline is true, only the black (1) pixels of the letters are drawn.
 * If \p invert is true the text will be drawn in inverted color.
 */
void lcdui_text_draw(uint8 x, uint8 y, const char *text,
        const lcdui_text_font_t font, uint8 no_outline,
        uint8 invert)
{
    uint8 i, fontAttr[98], xOffset, fontImageHeight;
    int16_adv fontImageWidth;
    uint24 fontImageAddress, fontAttrAddress;

    _debug("TEXT::drawText(%d,%d,%s)\r\n", x, y, text);

    get_font_base_addr(font, &fontImageAddress, &fontAttrAddress);
    ext_eeprom_read_array(fontAttrAddress, fontAttr, sizeof (fontAttr));
    
    fontImageWidth.hi = fontAttr[95];
    fontImageWidth.lo = fontAttr[96];
    fontImageHeight = fontAttr[97];

    if (y + fontImageHeight > GLCD_RESOLUTION_Y)
        return;

    // Draw text
    i = 0;
    xOffset = x;
    while (text[i])
    {
        uint8 code, j, charX, charY;
        uint16 charOffset;
        
        // Only draw characters with valid code
        if (text[i] < 32 || text[i] > 126)
            continue;

        // Calculate array index of the character
        code = text[i] - 32;

        // If character width is 0, skip this iteration
        if (fontAttr[code] == 0) {
            i++;
            continue;
        }

        // Check if the position is smaller than the screen's width
        if (xOffset + fontAttr[code] > GLCD_RESOLUTION_X)
            break;

        // Calculate character image offset (in pixels)
        charOffset = 0;
        for (j = 0; j < code; j++)
            charOffset += (uint16)fontAttr[j];

        for (charX = 0; charX < fontAttr[j]; charX++)
        {
            for (charY = 0; charY < fontImageHeight; charY++)
            {
                uint32 bitNo = (uint32)charY *
                    (uint32)fontImageWidth.ui +
                    (uint32)charX + (uint32)charOffset;

                uint24 addr = bitNo / 8;
                uint8 byte = ext_eeprom_read_byte(addr + fontImageAddress);
                uint8 pixel;
                bitNo -= addr * 8;

                pixel = byte & (1 << (7 - bitNo));

                // If NoStroke is enabled, only 'BLACK' pixels are drawn.
                // This method avoids drawing padding pixels of a character.
                if (no_outline && !pixel)
                    continue;

                // If inverted mode is enabled, invert all pixels
                if (invert)
                    pixel = !pixel;
                
                glcd_set_pixel(xOffset + charX, y + charY, pixel);
            }
        }

        xOffset += fontAttr[j];
        i++;
    }
}