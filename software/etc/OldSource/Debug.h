/**
 * Ethernet Weather Station v2 Firmware
 * Debug routines
 *
 * @author Tamas Karpati <tomikaa87>
 * @date 2011-09-08
 * @file debuh.h
 */

#ifndef DEBUG_H
#define DEBUG_H

#include <stdio.h>

#define DEBUG_ENABLED

#ifdef DEBUG_ENABLED
#   define _debug printf
#else
#   define _debug (void)
#endif

#endif // DEBUG_H
