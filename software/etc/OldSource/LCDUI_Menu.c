/**
 * Ethernet Weather Station v2 Firmware
 * LCD User Interface - Menu widget
 *
 * @author Tamas Karpati <tomikaa87>
 * @date 2011-12-25
 * @file LCDUI_Menu.c
 */

#include "LCDUI_Menu.h"
#include "LCDUI_Text.h"
#include "graphlcd.h"
#include "Debug.h"

/*** Private functions ********************************************************/

void _drawMenuItem(const uint8 y, const char *text, const uint8 selected)
{
    // Clear the area before drawing
    glcd_draw_rect(2, y, 125, y + 11, 1, GLCD_COLOR_WHITE);

    // Draw the menu item text
    lcdui_text_draw(4, y + 1, text, LCDUI_TEXT_FONT_PF_TEMPESTA, 0, 0);

    if (selected)
    {
        // Draw selection bar
        glcd_draw_inv_rect(2, y, 125, y + 11);
    }
}

void _updateMenu(lcdui_menu_t *menu)
{
    uint8 i;
    
    // Redraw the menu items if necessary
    if (menu->offset != menu->prevOffset)
    {
        // All items need to be redrawn
        for (i = 0; i < menu->maxItems && i < menu->count; i++)
        {
            uint8 y = menu->y + i * 14;
            uint8 itemIndex = menu->offset + i;
            uint8 isSelected = menu->selection == i;
            _drawMenuItem(y, menu->items[itemIndex], isSelected);
        }

        glcd_write_buf(0);
    }
    else
    {
        uint8 fromY, toY;

        if (menu->prevSelection < menu->selection)
        {
            fromY = menu->y + menu->prevSelection * 14;
            toY = menu->y + menu->selection * 14 + 13;
        }
        else
        {
            fromY = menu->y + menu->selection * 14;
            toY = menu->y + menu->prevSelection * 14 + 13;
        }

        for (i = 0; i < menu->maxItems && i < menu->count; i++)
        {
            if (i == menu->prevSelection || i == menu->selection)
            {
                // Only "move" the selection bar
                uint8 y = menu->y + i * 14;
                glcd_draw_inv_rect(2, y, 125, y + 11);
            }
        }

        glcd_write_buf_region(2, fromY, 125, toY, 0);
    }
}

/*** Public functions *********************************************************/

void lcdui_menu_init(lcdui_menu_t *menu, const char *title, const char *items[],
        const uint8 numItems, const uint8 maxItems, const uint8 baseY)
{
    _debug("Initializing menu: %08X (%s)\r\n", (unsigned long)menu,
            title);

    menu->title = title;
    menu->items = items;
    menu->count = numItems;
    menu->y = baseY;
    menu->offset = 0;
    menu->index = 0;
    menu->selection = 0;
    menu->maxItems = maxItems;
}

void lcdui_menu_show(lcdui_menu_t *menu)
{
    uint8 i;

    _debug("Showing menu: %08X (%s)\r\n", (unsigned long)menu,
            menu->title);
    
    glcd_clear_buf();

    lcdui_text_draw(1, 0, menu->title,
            LCDUI_TEXT_FONT_PF_TEMPESTA_BOLD, 0, 0);

    for (i = 0; i < menu->maxItems && i < menu->count; i++)
    {
        uint8 y = menu->y + i * 14;
        uint8 itemIndex = menu->offset + i;
        uint8 isSelected = menu->selection == i;
        _drawMenuItem(y, menu->items[itemIndex], isSelected);
    }

    glcd_write_buf(0);
}

uint8 lcdui_menu_up(lcdui_menu_t *menu)
{
    _debug("Menu up: %08X (%s)\r\n", (unsigned long)menu,
            menu->title);

    if (menu->index == 0)
        return 0;

    menu->prevOffset = menu->offset;
    menu->prevSelection = menu->selection;
    menu->index--;

    if (menu->selection > 0)
    {
        menu->selection--;
    }
    else
    {
        menu->offset--;
    }

    _updateMenu(menu);

    return menu->index;
}

uint8 lcdui_menu_down(lcdui_menu_t *menu)
{
    _debug("Menu down: %08X (%s)\r\n", (unsigned long)menu,
            menu->title);

    // Do nothing if the last item is selected
    if (menu->index == menu->count - 1)
        return menu->index;

    menu->prevOffset = menu->offset;
    menu->prevSelection = menu->selection;
    menu->index++;

    if (menu->selection < menu->maxItems - 1)
    {
        menu->selection++;
    }
    else
    {
        menu->offset++;
    }

    _updateMenu(menu);

    return menu->index;
}