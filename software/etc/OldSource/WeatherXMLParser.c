/**
 * Weather Station Ethernet Board
 * Simple and small XML parser
 * It parses the XML file downloaded from accuweather.com
 *
 * @author: Tamas Karpati <tomikaa87>
 * @date: 2010-09-12
 * @file WeatherXMLParser.c
 */

#include "WeatherXMLParser.h"

#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>

static WeatherData w_data;
static ForecastWeatherData f_data_1;
static ForecastWeatherData f_data_2;
static ForecastWeatherData f_data_3;
static ForecastWeatherData f_data_4;
static ForecastWeatherData f_data_5;
char temp[255];
char string[25];
unsigned int forecast_day;
ForecastWeatherData *f_data;

/*
 * This enum stores the state of the parser
 */
typedef enum
{
    InitialState,
    OpenerTagBegin,
    AttributeFound,
    AttributeValueBegin,
    AttributeValueEnd,
    OpenerTagEnd,
    //ValueFound,
    CloserTagBegin,
    CloserTagEnd,
    Unknown
}
ParserState;

/*
 * The data type helps the parser by
 * storing what type of weather data
 * tag is being parsed
 */
typedef enum
{
    InitialPosition,
    CurrentConditions,
    CurrentPres,
    CurrentPresTend,
    CurrentTemp,
    CurrentRF,
    CurrentHum,
    CurrentTxt,
    CurrentIcn,
    CurrentWG,
    CurrentWS,
    CurrentWD,
    CurrentVis,
    CurrentPrec,
    CurrentUVIdx,
    ForecastDate,
    ForecastDayCode,
    ForecastDay,
    ForecastDayNumber,
    ForecastDayTime,
    ForecastNightTime,
    ForecastDayTxtShort,
    ForecastDayTxtLong,
    ForecastDayIcon,
    ForecastDayHiTemp,
    ForecastDayLoTemp,
    ForecastDayHiRF,
    ForecastDayLoRF,
    ForecastDayWS,
    ForecastDayWD,
    ForecastDayWG,
    ForecastDayMaxUV,
    ForecastDayRain,
    ForecastDaySnow,
    ForecastDayPrecip,
    ForecastDayTStorm,
    ForecastNightTxtShort,
    ForecastNightTxtLong,
    ForecastNightIcon,
    ForecastNightHiTemp,
    ForecastNightLoTemp,
    ForecastNightHiRF,
    ForecastNightLoRF,
    ForecastNightWS,
    ForecastNightWD,
    ForecastNightWG,
    ForecastNightMaxUV,
    ForecastNightRain,
    ForecastNightSnow,
    ForecastNightPrecip,
    ForecastNightTStorm,
    EndOfWeatherData,
    UnknownPosition
}
WeatherXMLPosition;

typedef struct
{
    const char *tag;
    WeatherXMLPosition state;
} wxml_tag_t;

wxml_tag_t wxml_tags[35] = {
    { "currentconditions", CurrentConditions },
    { "/currentconditions", InitialPosition },
    { "/day", InitialPosition },
    { "/daytime", ForecastDay },
    { "/nighttime", ForecastDay },
    { "/adc_database", EndOfWeatherData },

    // Current conditions
    { "temperature", CurrentTemp },
    { "realfeel", CurrentRF },
    { "humidity", CurrentHum },
    { "weathertext", CurrentTxt },
    { "weathericon", CurrentIcn },
    { "windgusts", CurrentWG },
    { "windspeed", CurrentWS },
    { "winddir", CurrentWD },
    { "visibility", CurrentVis },
    { "precip", CurrentPrec },
    { "uvindex", CurrentUVIdx },

    // Forecast
    { "daytime", ForecastDayTime },
    { "nighttime", ForecastNightTime },
    { "daycode", ForecastDayCode },
    { "obsdate", ForecastDate },

    // Forecast daytime
    { "txtshort", ForecastDayTxtShort },
    { "weathericon", ForecastDayIcon },
    { "hightemperature", ForecastDayHiTemp },
    { "lowtemperature", ForecastDayLoTemp },
    { "realfeelhigh", ForecastDayHiRF },
    { "realfeellow", ForecastDayLoRF },
    { "windspeed", ForecastDayWS },
    { "windgust", ForecastDayWG },
    { "winddirection", ForecastDayWD },
    { "maxuv", ForecastDayMaxUV },
    { "rainamount", ForecastDayRain },
    { "snowamount", ForecastDaySnow },
    { "precipamount", ForecastDayPrecip },
    { "tstormprob", ForecastDayTStorm }
};

WeatherXMLPosition wxml_state(char *tag_name, unsigned char fore_night)
{
    unsigned char i;
    WeatherXMLPosition state = UnknownPosition;

    for (i = 0; i < sizeof (wxml_tags); i++)
    {
        if (strcmp(tag_name, wxml_tags[i].tag) == 0)
            state = wxml_tags[i].state;
    }

    if (fore_night)
    {
        switch (state)
        {
            case ForecastDayTxtShort:
                state = ForecastNightTxtShort;
                break;
            case ForecastDayIcon:
                state = ForecastNightIcon;
                break;
            case ForecastDayHiTemp:
                state = ForecastNightHiTemp;
                break;
            case ForecastDayLoTemp:
                state = ForecastNightLoTemp;
                break;
            case ForecastDayHiRF:
                state = ForecastNightHiRF;
                break;
            case ForecastDayLoRF:
                state = ForecastNightLoRF;
                break;
            case ForecastDayWS:
                state = ForecastNightWS;
                break;
            case ForecastDayWG:
                state = ForecastNightWG;
                break;
            case ForecastDayWD:
                state = ForecastNightWD;
                break;
            case ForecastDayMaxUV:
                state = ForecastNightMaxUV;
                break;
            case ForecastDayRain:
                state = ForecastNightRain;
                break;
            case ForecastDaySnow:
                state = ForecastNightSnow;
                break;
            case ForecastDayPrecip:
                state = ForecastNightPrecip;
                break;
            case ForecastDayTStorm:
                state = ForecastNightTStorm;
                break;
        }
    }

    return state;
}

/*
 * Global variables
 */
static WeatherXMLPosition w_pos = InitialPosition;

/*
 * This function is used for initializing
 * a weather data storage variable
 */
void WXP_init(/*WeatherData *w_data*/)
{
    memset(&w_data, 0, sizeof (WeatherData));
    memset(&f_data_1, 0, sizeof (ForecastWeatherData));
    memset(&f_data_2, 0, sizeof (ForecastWeatherData));
    memset(&f_data_3, 0, sizeof (ForecastWeatherData));
    memset(&f_data_4, 0, sizeof (ForecastWeatherData));
    memset(&f_data_5, 0, sizeof (ForecastWeatherData));
}

/*
 * This function resets the parser to its initial state
 */
void WXP_reset()
{
    w_pos = InitialPosition;
}

/*
 * Very simple line-by-line XML parser
 * It understands:
 *   Simple tags: <tag>
 *   Attributes with and without values <tag attr attr2="1">
 *   Values between tags: <tag>value</tag>
 *   Tags without values: <tag/>
 *   Tags without values and with attributes: <tag attr attr2="1"/>
 *
 * The function returns 1 if </adc_database> tag found to notify
 * the caller that is not necessary to call this function again.
 */
unsigned char WXP_parseLine(char *line/*, WeatherData *w_data*/)
{
    unsigned int len;
    ParserState state;
    unsigned int pos;

    if (line == NULL)
        return 0;

    len = strlen(line);

    pos = 0;
    state = InitialState;
    while (pos < len && line[pos] != 0)
    {
        char c = line[pos];
        unsigned int temp_ptr;

        switch (state)
        {
                // The first stage
            case InitialState:
            {
                // We have found a tag opener character
                if (c == '<')
                {
                    state = OpenerTagBegin;

                    // Initialize temp buffer
                    temp_ptr = 0;
                    memset(temp, 0, sizeof (temp));
                }

                break;
            }

                // Process text after tag opener
            case OpenerTagBegin:
            {
                if (c == ' ')
                {
                    // Space found, end of tag name and attribute found
                    //printf("Tag name: \"%s\"\r\n", temp);
                    state = AttributeFound;

                    // <day number=""> tag found
                    strcpy(string, "day");
                    if (strncmp(temp, string, 3) == 0)
                        w_pos = ForecastDay;

                    // Check if we are parsing current conditions data
                    if (w_pos == CurrentConditions)
                    {
                        // <pressure> tag found
                        strcpy(string, "pressure");
                        if (strncmp(temp, string, 8) == 0)
                            w_pos = CurrentPres;
                    }

                    printf("Current pos: %d; Temp: %s\r\n", w_pos, temp);

                    temp_ptr = 0;
                    memset(temp, 0, sizeof (temp));
                }
                else if (c == '>')
                {
                    // '>' found, end of tag, no attributes
                    printf("Tag name: \"%s\"\r\n", temp);
                    /*
                                    // Found <currentconditions> tag
                                    strcpy(str, "currentconditions");
                                    if (strncmp(temp, str, 17) == 0)
                                        w_pos = CurrentConditions;
                  
                                    // Found </currentconditions> tag
                                    strcpy(str, "/currentconditions");
                                    if (strncmp(temp, str, 18) == 0)
                                        w_pos = InitialPosition;
                    
                                    // Found </day> tag
                                    strcpy(str, "/day");
                                    if (strncmp(temp, str, 4) == 0)
                                        w_pos = InitialPosition;
                    
                                    // Found </daytime> tag
                                    strcpy(str, "/daytime");
                                    if (strncmp(temp, str, 8) == 0)
                                        w_pos = ForecastDay;

                                    // Found </nighttime> tag
                                    strcpy(str, "/nighttime");
                                    if (strncmp(temp, str, 10) == 0)
                                        w_pos = ForecastDay;
                    
                                    // Found </adt_database> tag, end of XML
                                    strcpy(str, "/adc_database");
                                    if (strncmp(temp, str, 13) == 0)
                                        w_pos = EndOfWeatherData;
                     */
                    w_pos = wxml_state(temp, (w_pos == ForecastNightTime));

                    /*
                                    // Read data for current conditions
                                    if (w_pos == CurrentConditions)
                                    {
                                        // <temperature> tag found
                                        strcpy(str, "temperature");
                                        if (strncmp(temp, str, 11) == 0)
                                            w_pos = CurrentTemp;
                        
                                        // <realfeel> tag found
                                        strcpy(str, "realfeel");
                                        if (strncmp(temp, str, 8) == 0)
                                            w_pos = CurrentRF;
                        
                                        // <humidity> tag found
                                        strcpy(str, "humidity");
                                        if (strncmp(temp, str, 8) == 0)
                                            w_pos = CurrentHum;
                        
                                        // <weathertext> tag found
                                        strcpy(str, "weathertext");
                                        if (strncmp(temp, str, 11) == 0)
                                            w_pos = CurrentTxt;
                        
                                        // <weathericon> tag found
                                        strcpy(str, "weathericon");
                                        if (strncmp(temp, str, 11) == 0)
                                            w_pos = CurrentIcn;
                        
                                        // <windgusts> tag found
                                        strcpy(str, "windgusts");
                                        if (strncmp(temp, str, 9) == 0)
                                            w_pos = CurrentWG;
                        
                                        // <windspeed> tag found
                                        strcpy(str, "windspeed");
                                        if (strncmp(temp, str, 9) == 0)
                                            w_pos = CurrentWS;
                        
                                        // <winddir> tag found
                                        strcpy(str, "winddir");
                                        if (strncmp(temp, str, 7) == 0)
                                            w_pos = CurrentWD;
                        
                                        // <visibility> tag found
                                        strcpy(str, "visibility");
                                        if (strncmp(temp, str, 10) == 0)
                                            w_pos = CurrentVis;
                        
                                        // <precip> tag found
                                        strcpy(str, "precip");
                                        if (strncmp(temp, str, 6) == 0)
                                            w_pos = CurrentPrec;
                        
                                        // <uvindex> tag found
                                        strcpy(str, "uvindex");
                                        if (strncmp(temp, str, 7) == 0)
                                            w_pos = CurrentUVIdx;
                                    }
                
                                    // Forecast day data
                                    if (w_pos == ForecastDay)
                                    {
                                        // <daytime> tag found
                                        strcpy(str, "daytime");
                                        if (strncmp(temp, str, 7) == 0)
                                            w_pos = ForecastDayTime;
                        
                                        // <nighttime> tag found
                                        strcpy(str, "nighttime");
                                        if (strncmp(temp, str, 9) == 0)
                                            w_pos = ForecastNightTime;
                        
                                        // <daycode> tag found
                                        strcpy(str, "daycode");
                                        if (strncmp(temp, str, 7) == 0)
                                            w_pos = ForecastDayCode;
                    
                                        // <daycode> tag found
                                        strcpy(str, "obsdate");
                                        if (strncmp(temp, str, 7) == 0)
                                            w_pos = ForecastDate;
                            
                                         //printf("xml: forecast day: %s\r\n", w_pos == ForecastDayTime ? "Day" : "Night");
                                    }
                
                                    // Forecast day daytime data
                                    if (w_pos == ForecastDayTime)
                                    {
                                        // <txtshort> tag
                                        strcpy(str, "txtshort");
                                        if (strncmp(temp, str, 8) == 0)
                                            w_pos = ForecastDayTxtShort;
                        
                                        // <txtlong> tag
                                        //strcpy(str, "txtlong");
                                        //if (strncmp(temp, str, 7) == 0)
                                            //w_pos = ForecastDayTxtLong;
                        
                                        // <weathericon> tag
                                        strcpy(str, "weathericon");
                                        if (strncmp(temp, str, 11) == 0)
                                            w_pos = ForecastDayIcon;
                        
                                        // <hightemperature> tag
                                        strcpy(str, "hightemperature");
                                        if (strncmp(temp, str, 15) == 0)
                                            w_pos = ForecastDayHiTemp;
                        
                                        // <lowtemperature> tag
                                        strcpy(str, "lowtemperature");
                                        if (strncmp(temp, str, 14) == 0)
                                            w_pos = ForecastDayLoTemp;
                        
                                        // <realfeelhigh> tag
                                        strcpy(str, "realfeelhigh");
                                        if (strncmp(temp, str, 12) == 0)
                                            w_pos = ForecastDayHiRF;
                        
                                        // <realfeellow> tag
                                        strcpy(str, "realfeellow");
                                        if (strncmp(temp, str, 11) == 0)
                                            w_pos = ForecastDayLoRF;
                        
                                        // <windspeed> tag
                                        strcpy(str, "windspeed");
                                        if (strncmp(temp, str, 9) == 0)
                                            w_pos = ForecastDayWS;
                        
                                        // <windgust> tag
                                        strcpy(str, "windgust");
                                        if (strncmp(temp, str, 8) == 0)
                                            w_pos = ForecastDayWG;
                        
                                        // <winddirection> tag
                                        strcpy(str, "winddirection");
                                        if (strncmp(temp, str, 13) == 0)
                                            w_pos = ForecastDayWD;
                        
                                        // <maxuv> tag
                                        strcpy(str, "maxuv");
                                        if (strncmp(temp, str, 5) == 0)
                                            w_pos = ForecastDayMaxUV;
                        
                                        // <rainamount> tag
                                        strcpy(str, "rainamount");
                                        if (strncmp(temp, str, 10) == 0)
                                            w_pos = ForecastDayRain;
                        
                                        // <snowamount> tag
                                        strcpy(str, "snowamount");
                                        if (strncmp(temp, str, 10) == 0)
                                            w_pos = ForecastDaySnow;
                        
                                        // <precipamount> tag
                                        strcpy(str, "precipamount");
                                        if (strncmp(temp, str, 12) == 0)
                                            w_pos = ForecastDayPrecip;
                        
                                        // <tstormprob> tag
                                        strcpy(str, "tstormprob");
                                        if (strncmp(temp, str, 10) == 0)
                                            w_pos = ForecastDayTStorm;
                                    }
                
                                    // Forecast day nighttime data
                                    if (w_pos == ForecastNightTime)
                                    {
                                        // <txtshort> tag
                                        strcpy(str, "txtshort");
                                        if (strncmp(temp, str, 8) == 0)
                                            w_pos = ForecastNightTxtShort;
                        
                                        // <txtlong> tag
                                        //strcpy(str, "txtlong");
                                        //if (strncmp(temp, str, 7) == 0)
                                            //w_pos = ForecastNightTxtLong;
                        
                                        // <weathericon> tag
                                        strcpy(str, "weathericon");
                                        if (strncmp(temp, str, 11) == 0)
                                            w_pos = ForecastNightIcon;
                        
                                        // <hightemperature> tag
                                        strcpy(str, "hightemperature");
                                        if (strncmp(temp, str, 15) == 0)
                                            w_pos = ForecastNightHiTemp;
                        
                                        // <lowtemperature> tag
                                        strcpy(str, "lowtemperature");
                                        if (strncmp(temp, str, 14) == 0)
                                            w_pos = ForecastNightLoTemp;
                        
                                        // <realfeelhigh> tag
                                        strcpy(str, "realfeelhigh");
                                        if (strncmp(temp, str, 12) == 0)
                                            w_pos = ForecastNightHiRF;
                        
                                        // <realfeellow> tag
                                        strcpy(str, "realfeellow");
                                        if (strncmp(temp, str, 11) == 0)
                                            w_pos = ForecastNightLoRF;
                        
                                        // <windspeed> tag
                                        strcpy(str, "windspeed");
                                        if (strncmp(temp, str, 9) == 0)
                                            w_pos = ForecastNightWS;
                        
                                        // <windgust> tag
                                        strcpy(str, "windgust");
                                        if (strncmp(temp, str, 8) == 0)
                                            w_pos = ForecastNightWG;
                        
                                        // <winddirection> tag
                                        strcpy(str, "winddirection");
                                        if (strncmp(temp, str, 13) == 0)
                                            w_pos = ForecastNightWD;
                        
                                        // <rainamount> tag
                                        strcpy(str, "rainamount");
                                        if (strncmp(temp, str, 10) == 0)
                                            w_pos = ForecastNightRain;
                        
                                        // <snowamount> tag
                                        strcpy(str, "snowamount");
                                        if (strncmp(temp, str, 10) == 0)
                                            w_pos = ForecastNightSnow;
                        
                                        // <precipamount> tag
                                        strcpy(str, "precipamount");
                                        if (strncmp(temp, str, 12) == 0)
                                            w_pos = ForecastNightPrecip;
                        
                                        // <tstormprob> tag
                                        strcpy(str, "tstormprob");
                                        if (strncmp(temp, str, 10) == 0)
                                            w_pos = ForecastNightTStorm;
                                    }
                     */
                    printf("xml: temp = %s\r\n", temp);
                    printf("Current pos: %d; Temp: %s\r\n", w_pos, temp);

                    temp_ptr = 0;
                    memset(temp, 0, sizeof (temp));
                    state = OpenerTagEnd;
                }
                else
                {
                    // Store c in temp buffer
                    temp[temp_ptr] = c;
                    temp_ptr++;
                }

                break;
            }

                // We have found an attribute in the current tag
            case AttributeFound:
            {
                if (c == '=')
                {
                    state = AttributeValueBegin;
                    printf("=Attribute name: \"%s\"\r\n", temp);

                    // <pressure state=""> found
                    strcpy(string, "state");
                    if (strncmp(temp, string, 5) == 0 && w_pos == CurrentPres)
                        w_pos = CurrentPresTend;

                    // <day number=""> found
                    strcpy(string, "number");
                    if (strncmp(temp, string, 6) == 0)
                        w_pos = ForecastDayNumber;

                    temp_ptr = 0;
                    memset(temp, 0, sizeof (temp));

                    // Skip '"'
                    pos++;
                }
                    // End of the current tag
                else if (c == '>')
                {
                    state = OpenerTagEnd;
                    printf(">Attribute name: \"%s\"\r\n", temp);
                    temp_ptr = 0;
                    memset(temp, 0, sizeof (temp));
                }
                else
                {
                    temp[temp_ptr] = c;
                    temp_ptr++;
                }

                break;
            }

                // Read the current attribute value
            case AttributeValueBegin:
            {
                if (c == '"')
                {
                    state = AttributeValueEnd;
                    printf("Attribute value: \"%s\"\r\n", temp);

                    switch (w_pos)
                    {
                            // <pressure state=""> tag, read state value
                        case CurrentPresTend:
                            strcpy(w_data.CurrentPressureTend, temp);
                            w_pos = CurrentPres;
                            break;

                            // <day number=""> tag, read number value
                        case ForecastDayNumber:
                            forecast_day = atoi(temp);

                            switch (forecast_day)
                            {
                                case 1:
                                    f_data = &f_data_1;
                                    break;
                                case 2:
                                    f_data = &f_data_2;
                                    break;
                                case 3:
                                    f_data = &f_data_3;
                                    break;
                                case 4:
                                    f_data = &f_data_4;
                                    break;
                                case 5:
                                    f_data = &f_data_5;
                                    break;
                            }

                            if (forecast_day > 5)
                            {
                                w_pos = InitialState;
                                break;
                            }
                            printf("xml: forecast day = %d\r\n", forecast_day);
                            w_pos = ForecastDay;
                            break;
                    }


                    temp_ptr = 0;
                    memset(temp, 0, sizeof (temp));
                }
                    // There is no value of the attribute, end of tag reached
                else if (c == '>')
                {
                    state = OpenerTagEnd;
                    printf("Attribute value: \"%s\"\r\n", temp);
                    temp_ptr = 0;
                    memset(temp, 0, sizeof (temp));
                }
                else
                {
                    temp[temp_ptr] = c;
                    temp_ptr++;
                }

                break;
            }

            case AttributeValueEnd:
            {
                // We have found a new attribute
                if (c == ' ')
                {
                    state = AttributeFound;
                }
                    // We have reached the end of the tag
                else if (c == '>')
                {
                    state = OpenerTagEnd;
                }
                    // '/' indicates the end of a closer tag
                else if (c == '/')
                {
                    state = CloserTagEnd;
                    // Skip '>' after '/'
                    pos++;
                }

                break;
            }

                // We have reached the end of the opener tag
            case OpenerTagEnd:
            {
                // The begin of a closer tag
                // The value between the opnener and the closer tag is in 'temp'
                if (c == '<')
                {
                    state = CloserTagBegin;
                    printf("Value: \"%s\"\r\n", temp);

                    switch (w_pos)
                    {
                            // Current weather
                        case CurrentPres:
                            // Convert inHg to hPa
                            w_data.CurrentPressure = (unsigned int) (round(atof(temp) * 33.8639));
                            w_pos = CurrentConditions;
                            break;
                        case CurrentTemp:
                            // Convert F to C
                            w_data.CurrentTemperature = (signed char) (round(0.5556 * (atof(temp) - 32.0)));
                            w_pos = CurrentConditions;
                            break;
                        case CurrentRF:
                            // Convert F to C
                            w_data.CurrentRealFeel = (signed char) (round(0.5556 * (atof(temp) - 32.0)));
                            w_pos = CurrentConditions;
                            break;
                        case CurrentHum:
                            w_data.CurrentHumidity = atof(temp);
                            w_pos = CurrentConditions;
                            break;
                        case CurrentTxt:
                            strncpy(w_data.CurrentText, temp, 50);
                            w_pos = CurrentConditions;
                            break;
                        case CurrentIcn:
                            w_data.CurrentIcon = atoi(temp);
                            w_pos = CurrentConditions;
                            break;
                        case CurrentWG:
                            // Convert mph to 10 * km/h
                            w_data.CurrentWindGusts = (unsigned char) (round(1.609344 * atof(temp)) * 10.0);
                            w_pos = CurrentConditions;
                            break;
                        case CurrentWS:
                            // Convert mph to 10 * km/h
                            w_data.CurrentWindSpeed = (unsigned char) (round(1.609344 * atof(temp)) * 10.0);
                            w_pos = CurrentConditions;
                            break;
                        case CurrentWD:
                            strcpy(w_data.CurrentWindDir, temp);
                            w_pos = CurrentConditions;
                            break;
                        case CurrentVis:
                            w_data.CurrentVisibility = atoi(temp);
                            w_pos = CurrentConditions;
                            break;
                        case CurrentPrec:
                            // Convert in to mm
                            w_data.CurrentPrecipation = 25.4 * atof(temp);
                            w_pos = CurrentConditions;
                            break;
                        case CurrentUVIdx:
                            w_data.CurrentUVIndex = atoi(temp);
                            w_pos = CurrentConditions;
                            break;

                            // Forecast date
                        case ForecastDate:
                            strncpy(f_data->Date, temp, 10);
                            w_pos = ForecastDay;
                            break;
                            // Forecast day code
                        case ForecastDayCode:
                            strncpy(f_data->DayCode, temp, 3);
                            w_pos = ForecastDay;
                            break;

                            // Forecast daytime
                        case ForecastDayTxtShort:
                            strncpy(f_data->DayTextShort, temp, 50);
                            w_pos = ForecastDayTime;
                            break;
                            /*case ForecastDayTxtLong:
                                strncpy(f_data->DayTextLong, temp, 80);
                                w_pos = ForecastDayTime;
                                break;*/
                        case ForecastDayIcon:
                            f_data->DayIcon = atoi(temp);
                            w_pos = ForecastDayTime;
                            break;
                        case ForecastDayHiTemp:
                            f_data->DayHiTemp = (signed char) (round(0.5556 * (atof(temp) - 32.0)));
                            w_pos = ForecastDayTime;
                            break;
                        case ForecastDayLoTemp:
                            f_data->DayLoTemp = (signed char) (round(0.5556 * (atof(temp) - 32.0)));
                            w_pos = ForecastDayTime;
                            break;
                        case ForecastDayHiRF:
                            f_data->DayRFHi = (signed char) (round(0.5556 * (atof(temp) - 32.0)));
                            w_pos = ForecastDayTime;
                            break;
                        case ForecastDayLoRF:
                            f_data->DayRFLo = (signed char) (round(0.5556 * (atof(temp) - 32.0)));
                            w_pos = ForecastDayTime;
                            break;
                        case ForecastDayWS:
                            f_data->DayWindSpeed = (unsigned char) (round(1.609344 * atof(temp)) * 10.0);
                            w_pos = ForecastDayTime;
                            break;
                        case ForecastDayWD:
                            strcpy(f_data->DayWindDir, temp);
                            w_pos = ForecastDayTime;
                            break;
                        case ForecastDayWG:
                            f_data->DayWindGusts = (unsigned char) (round(1.609344 * atof(temp)) * 10.0);
                            w_pos = ForecastDayTime;
                            break;
                        case ForecastDayMaxUV:
                            f_data->DayMaxUV = atoi(temp);
                            w_pos = ForecastDayTime;
                            break;
                        case ForecastDayRain:
                            f_data->DayRainAmount = 25.4 * atof(temp);
                            w_pos = ForecastDayTime;
                            break;
                        case ForecastDaySnow:
                            f_data->DaySnowAmount = 25.4 * atof(temp);
                            w_pos = ForecastDayTime;
                            break;
                        case ForecastDayPrecip:
                            f_data->DayPrecipAmount = 25.4 * atof(temp);
                            w_pos = ForecastDayTime;
                            break;
                        case ForecastDayTStorm:
                            f_data->DayTStormProb = atoi(temp);
                            w_pos = ForecastDayTime;
                            break;

                            // Forecast nighttime
                        case ForecastNightTxtShort:
                            strcpy(f_data->NightTextShort, temp);
                            w_pos = ForecastNightTime;
                            break;
                            /*case ForecastNightTxtLong:
                                strcpy(f_data->NightTextLong, temp);
                                w_pos = ForecastNightTime;
                                break;*/
                        case ForecastNightIcon:
                            f_data->NightIcon = atoi(temp);
                            w_pos = ForecastNightTime;
                            break;
                        case ForecastNightHiTemp:
                            f_data->NightHiTemp = (signed char) (round(0.5556 * (atof(temp) - 32.0)));
                            w_pos = ForecastNightTime;
                            break;
                        case ForecastNightLoTemp:
                            f_data->NightLoTemp = (signed char) (round(0.5556 * (atof(temp) - 32.0)));
                            w_pos = ForecastNightTime;
                            break;
                        case ForecastNightHiRF:
                            f_data->NightRFHi = (signed char) (round(0.5556 * (atof(temp) - 32.0)));
                            w_pos = ForecastNightTime;
                            break;
                        case ForecastNightLoRF:
                            f_data->NightRFLo = (signed char) (round(0.5556 * (atof(temp) - 32.0)));
                            w_pos = ForecastNightTime;
                            break;
                        case ForecastNightWS:
                            f_data->NightWindSpeed = (unsigned char) (round(1.609344 * atof(temp)) * 10.0);
                            w_pos = ForecastNightTime;
                            break;
                        case ForecastNightWD:
                            strcpy(f_data->NightWindDir, temp);
                            w_pos = ForecastNightTime;
                            break;
                        case ForecastNightWG:
                            f_data->NightWindGusts = (unsigned char) (round(1.609344 * atof(temp)) * 10.0);
                            w_pos = ForecastNightTime;
                            break;
                        case ForecastNightRain:
                            f_data->NightRainAmount = 25.4 * atof(temp);
                            w_pos = ForecastNightTime;
                            break;
                        case ForecastNightSnow:
                            f_data->NightSnowAmount = 25.4 * atof(temp);
                            w_pos = ForecastNightTime;
                            break;
                        case ForecastNightPrecip:
                            f_data->NightPrecipAmount = 25.4 * atof(temp);
                            w_pos = ForecastNightTime;
                            break;
                        case ForecastNightTStorm:
                            f_data->NightTStormProb = atoi(temp);
                            w_pos = ForecastNightTime;
                            break;
                    }

                    temp_ptr = 0;
                    memset(temp, 0, sizeof (temp));
                }
                else
                {
                    temp[temp_ptr] = c;
                    temp_ptr++;
                }

                break;
            }

                // Don't care about closer tags, we don't need them
            case CloserTagBegin:
            case CloserTagEnd:
                break;

            case Unknown:
                return 0;
        }

        pos++;
    }

    // End of weather data XML reached, return 1
    if (w_pos == EndOfWeatherData)
        return 1;
    else
        return 0;
}

/*
 * Returns with the stored weather data
 */
WeatherData *WXP_getWeatherData()
{
    return &w_data;
}

/*
 * Returns with the forecast data of the given day
 * or NULL of invalid day number is given
 */
ForecastWeatherData *WXP_getForecastData(unsigned char day)
{
    switch (day)
    {
        case 1:
            return &f_data_1;
        case 2:
            return &f_data_2;
        case 3:
            return &f_data_3;
        case 4:
            return &f_data_4;
        case 5:
            return &f_data_5;
        default:
            return NULL;
    }
}
