/**
 * Ethernet Weather Station v2 Firmware
 * LCD User Interface - main source
 *
 * @author Tamas Karpati <tomikaa87>
 * @date 2011-12-22
 * @file LCDUI.c
 */

#include "graphlcd.h"
#include "Debug.h"
#include "LCDUI.h"
#include "PWM.h"
#include "WeatherXMLParser.h"
#include "25LC1024.h"
#include "KeyBrd.h"
#include "LCDUI_Menu.h"
#include "LCDUI_Text.h"
#include "LCDUI_NumberDialog.h"

/*** Constant definitions *****************************************************/

typedef enum {
    SCR_CURRENT_WEATHER = 0,
    SCR_FORECAST_1,
    SCR_FORECAST_2,
    SCR_FORECAST_3,
    SCR_FORECAST_4,
    SCR_FORECAST_5,
    SCR_MAIN_MENU,
    SCR_WEATHER_MENU,
    SCR_DISPLAY_MENU,
    SCR_NETWORK_MENU,
    SCR_TIME_MENU,
    SCR_SENSOR_MENU,
    SCR_NUMBER_DIALOG
} LCDUI_Screen;

/*** Menu constans ************************************************************/

static const char MainMenuTitle[] = "Main menu";
static const char *MainMenuItems[] = {
    "Weather settings", "Display settings", "Network settings",
    "Time and date settings", "Outdoor sensor",
    "Status", "About", "SAVE SETTINGS"
};

static const char WeatherSettingsMenuTitle[] = "Weather settings";
static const char *WeatherSettingsMenuItems[] = {
    "Update interval", "Location"
};

static const char DisplaySettingsMenuTitle[] = "Display settings";
static const char *DisplaySettingsMenuItems[] = {
    "Automatic backlight mode", "Auto BL bright time", "Auto BL dark time",
    "Auto BL bright level", "Auto BL dark level", "Backlight level"
};

static const char NetworkSettingsMenuTitle[] = "Network settings";
static const char *NetworkSettingsMenuItems[] = {
    "Static IP", "IP address", "Subnet mask", "Default gateway",
    "Network proxy", "Proxy IP", "Proxy port"
};

static const char TimeSettingsMenuTitle[] = "Time and date settings";
static const char *TimeSettingsMenuItems[] = {
    "Time", "Date", "Time zone"
};

static const char OutdoorSensorMenuTitle[] = "Outdoor sensor";
static const char *OutdoorSensorMenuItems[] = {
    "Altitude", "Status"
};

/*** Global variables *********************************************************/

uint8 g_currentScreen = SCR_CURRENT_WEATHER;
uint8 g_currentMenu = 255;
lcdui_menu_t g_menus[6];
LCDUI_NUMBERDIALOG g_weatherUpdateIntervalDialog;
LCDUI_Screen g_lastScreen;

/******************************************************************************/

void on_f1_button()
{
    _debug("Resetting device...\r\n");
    asm("reset");
}

void on_up_button()
{
    if (g_currentMenu < sizeof (g_menus))
        lcdui_menu_up(&g_menus[g_currentMenu]);
}

void on_f2_button()
{
    
}

void on_left_button()
{
    
}

void on_enter_button()
{
    // Main menu
    if (g_currentScreen == SCR_MAIN_MENU) {
        uint8 index = g_menus[g_currentMenu].index;

        if (index <= 4) {
            g_currentScreen = SCR_MAIN_MENU + index + 1;
            g_currentMenu = index + 1;
            lcdui_menu_show(&g_menus[g_currentMenu]);
        }
    }
    // Main menu -> Weather menu
    else if (g_currentScreen == SCR_WEATHER_MENU) {
        if (g_menus[g_currentMenu].index == 0) {
            g_lastScreen = g_currentScreen;
            g_currentScreen = SCR_NUMBER_DIALOG;
            lcdui_numdlg_draw(&g_weatherUpdateIntervalDialog);
        }
    }
}

void on_right_button()
{
    
}

void on_menu_button()
{
    if (g_currentScreen == SCR_CURRENT_WEATHER) {
        g_currentScreen = SCR_MAIN_MENU;
        g_currentMenu = 0;
    }

    if (g_currentMenu < sizeof (g_menus))
        lcdui_menu_show(&g_menus[g_currentMenu]);
}

void on_down_button()
{
    if (g_currentMenu < sizeof (g_menus))
        lcdui_menu_down(&g_menus[g_currentMenu]);
}

void on_cancel_button()
{
    if (g_currentScreen == SCR_NUMBER_DIALOG) {
        g_currentScreen = g_lastScreen;
        lcdui_menu_show(&g_menus[g_currentMenu]);
    } else if (g_currentScreen > SCR_MAIN_MENU) {
        g_currentMenu = 0;
        g_currentScreen = SCR_MAIN_MENU;
        lcdui_menu_show(&g_menus[g_currentMenu]);
    }
}

/*** Private functions ********************************************************/

/**
 * Initializes the LCD UI and all necessary peripherals (LCD, PWM ...)
 */
void lcdui_init()
{
    // Initialize GLCD
    _debug("Initializing LCD...\r\n");
    glcd_init();

    // Initialize PWM for LCD backlight
    _debug("Initializing PWM...\r\n");
    pwm_init();
    pwm_set_dc(150);

    // Draw splash screen
    lcdui_draw_fs_bitmap_from_eeprom(LCDUI_BITMAP_SPLASH);

    // Initialize the keyboard
    _debug("Initializing keyboard...\r\n");
    key_init();

    // Initialize all menus
    lcdui_menu_init(&g_menus[0], MainMenuTitle, MainMenuItems, 8, 8, 16);
    lcdui_menu_init(&g_menus[1], WeatherSettingsMenuTitle,
        WeatherSettingsMenuItems, 2, 8, 16);
    lcdui_menu_init(&g_menus[2], DisplaySettingsMenuTitle,
        DisplaySettingsMenuItems, 6, 8, 16);
    lcdui_menu_init(&g_menus[3], NetworkSettingsMenuTitle,
        NetworkSettingsMenuItems, 7, 8, 16);
    lcdui_menu_init(&g_menus[4], TimeSettingsMenuTitle,
        TimeSettingsMenuItems, 3, 8, 16);
    lcdui_menu_init(&g_menus[5], OutdoorSensorMenuTitle,
        OutdoorSensorMenuItems, 2, 8, 16);

    // Initialize number input dialogs
    lcdui_numdlg_init(&g_weatherUpdateIntervalDialog, 10, 2,
        WeatherSettingsMenuItems[0]);
}

/**
 * Executes all necessary tasks
 */
void lcdui_task()
{
    // Handle keyboard events
    switch (key_task())
    {
        case BTN_FUNC_1: on_f1_button(); break;
        case BTN_FUNC_2: on_f2_button(); break;
        case BTN_UP: on_up_button(); break;
        case BTN_LEFT: on_left_button(); break;
        case BTN_RIGHT: on_down_button(); break;
        case BTN_DOWN: on_down_button(); break;
        case BTN_ENTER: on_enter_button(); break;
        case BTN_MENU: on_menu_button(); break;
        case BTN_ESC: on_cancel_button(); break;
        default: break;
    }
}

int8 lcdui_is_on_main_screen()
{
    return (g_currentScreen == SCR_MAIN_MENU);
}

/**
 * Reads an image from the external EEPROM at the given \p address and draws it
 * on the LCD.
 */
void lcdui_draw_fs_bitmap_from_eeprom(const uint24 address)
{
    unsigned long i;
    
    for (i = 0; i < 2048; i++)
        glcd_set_buf_byte(i, ext_eeprom_read_byte(address + i));

    glcd_write_buf(0);
}