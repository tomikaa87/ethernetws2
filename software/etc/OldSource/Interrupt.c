/**
 * Ethernet Weather Station v2 Firmware
 * Interrupt Service Routines file
 *
 * @author Tamas Karpati <tomikaa87>
 * @date 2011-10-05
 * @file interrupt.c
 */

#include "Interrupt.h"
#include "TCPIPStack/Tick.h"
#include "UART.h"

#include <htc.h>

extern DWORD g_epochTime;
extern DWORD g_uptimeSeconds;

/**
 * (Normal priority) Interrupt Service Routine
 */
void interrupt isr()
{
    if (RCIE && RCIF)
    {
        putbufch(RCREG);
    }
}

/**
 * Low priority Interrupt Service Routine
 */
void interrupt low_priority low_isr()
{
    if (TMR0IE && TMR0IF)
    {
        TickUpdate();
        TMR0IF = 0;
    }

    if (TMR1IE && TMR1IF)
    {
        TMR1H |= 0x80; // Preload for 1 sec overflow
        g_epochTime++;
        g_uptimeSeconds++;
        TMR1IF = 0;
    }
}
