#include <htc.h>
#include <stdio.h>

/*__CONFIG(1, OSC_INTIO67);
__CONFIG(2, WDT_OFF);
__CONFIG(3, MCLRE_OFF);
__CONFIG(4, LVP_OFF);*/

__CONFIG(1, IESOEN & RCIO);
__CONFIG(2, BOREN & BORV28 & WDTDIS);
__CONFIG(3, PBANDIS);
__CONFIG(4, LVPDIS);
__CONFIG(5, UNPROTECT);
__CONFIG(6, WRTEN);
__CONFIG(7, UNPROTECT);

#define BUFFER_SIZE         128
unsigned char buffer[BUFFER_SIZE];
unsigned char bufferInIndex = 0;
unsigned char bufferOutIndex = 0;
#define bufferHasData() (bufferInIndex != bufferOutIndex)

unsigned char bufferGetch()
{
    unsigned char c;

    while (!bufferHasData())
        continue;

    c = buffer[bufferOutIndex];
    bufferOutIndex = (bufferOutIndex + 1) % BUFFER_SIZE;

    return c;
}


#define USART_BAUD          1200
#define FOSC                8000000ul
#define USART_NINE_BITS     0
#define USART_HIGH_SPEED    0

#define USART_DIVIDER       ((int)(FOSC / (16ul * USART_BAUD) - 1))
#if USART_HIGH_SPEED == 1
    #define USART_SPEED     0x04
#else
    #define USART_SPEED     0
#endif

#if USART_NINE_BITS == 1
    #define USART_NINE      0x40
#else
    #define USART_NINE      0
#endif

void USART_init()
{
    //SPBRG = USART_DIVIDER;

    BRG16 = 1;
    BRGH = 0;
    SPBRGH = 0x06;
    SPBRG = 0x81;

    TX9 = 0;
    SYNC = 0;
    SPEN = 1;
    CREN = 0;
    SREN = 0;
    TXEN = 1;

    TRISC6 = 0;
    TRISC7 = 1;
}

void putch(unsigned char c)
{
    while (!TRMT)
        continue;
    TXREG = c;
    while (!TRMT)
        continue;
}

unsigned char getch()
{
    while (!RCIF)
        continue;
    return RCREG;
}

#define CRC16_POLYNOMIAL    0x1021

static unsigned int CRC16_runningValue;

void CRC16_reset()
{
    CRC16_runningValue = 0;
}

unsigned int CRC16_value()
{
    return CRC16_runningValue;
}

void CRC16_byte(unsigned char ch)
{
    unsigned char i;

    CRC16_runningValue ^= ch;
    i = 8;

    do
    {
        if (CRC16_runningValue & 0x8000)
            CRC16_runningValue <<= 1;
        else
        {
            CRC16_runningValue <<= 1;
            CRC16_runningValue ^= CRC16_POLYNOMIAL;
        }
    }    while (--i);
}

unsigned char MAN_decode(unsigned char encoded)
{
    unsigned char decoded, pattern, i;

    if (encoded == 0xF0)
        return 0xF0;

//    printf("MAN_decode(%02X)\r\n", encoded);

    decoded = 0;

    for (i = 0; i < 4; i++)
    {
        // First 2 MSB bits
        pattern = encoded & 0xC0;
        // 10 -> 1
        if (pattern == 0x80)
            decoded |= 1;
            // 01 -> 0
        else if (pattern == 0x40)
            decoded |= 0;
            // Invalid code
        else
            return 0xFF;
        decoded <<= 1;
        encoded <<= 2;
    }

    decoded >>= 1;

    return decoded;
}

void interrupt isr()
{
    if (RCIF)
    {
        unsigned char c;

        buffer[bufferInIndex] = RCREG;
        c = bufferInIndex;
        bufferInIndex = (bufferInIndex + 1) % BUFFER_SIZE;
        if (bufferInIndex == bufferOutIndex)
            bufferInIndex = c;
    }
}

/*
 * Packet format
 * [0xFA 0xAF][VOLTAGE][DHT_TEMP][DHT_RH][BMP085_TEMP][BMP085_PRES][ERROR_CODE][CHECKSUM]
 *  __ 2 B __  _ 2 B _  __ 2 B _  _2 B__  __ 2 B ____  __ 2 B ____  __ 1 B ___  __ 2 B _
 *
 * Total size: 15 Bytes
 */

void processPacket(unsigned char c)
{
    static enum {
        S_INIT, S_FRAME, S_DATA, S_CHECKSUM
    } state = S_INIT;
}

//void processPacket(unsigned char c)
//{
//
//    static enum
//    {
//        S_INIT,
//        S_FRAME,
//        S_RH,
//        S_TEMP,
//        S_VDD,
//        S_CHECKSUM
//    } state = S_INIT;
//
//    static union
//    {
//        signed int si;
//        unsigned int ui;
//
//        struct
//        {
//            unsigned char uc1;
//            unsigned char uc2;
//        };
//    } tmp;
//    static unsigned char i;
//    static signed int temperature;
//    static signed int voltage;
//    static unsigned char humidity;
//
////    printf("%02X", c);
//
//    switch (state)
//    {
//        case S_INIT:
//            if (c == 0xFA)
//                state = S_FRAME;
//            break;
//
//        case S_FRAME:
//            if (c == 0xAF)
//            {
//                state = S_RH;
//                i = 0;
//                CRC16_reset();
//            }
//            else
//                state = S_INIT;
//            break;
//
//        case S_RH:
//            //((unsigned char *)&(tmp.ui))[i++] = c;
//            //tmp.uc = c;
//            //i++;
//            CRC16_byte(c);
//            //if (i == 1)
//        {
//            state = S_TEMP;
//            humidity = c;
//            //i = 0;
//        }
//            break;
//
//        case S_TEMP:
//            ((unsigned char *) &(tmp.ui))[i++] = c;
//            CRC16_byte(c);
//            if (i == 2)
//            {
//                state = S_VDD;
//                temperature = tmp.si;
//                i = 0;
//            }
//            break;
//
//        case S_VDD:
//            ((unsigned char *) &(tmp.ui))[i++] = c;
//            CRC16_byte(c);
//            if (i == 2)
//            {
//                state = S_CHECKSUM;
//                voltage = tmp.si;
//                i = 0;
//            }
//            break;
//
//        case S_CHECKSUM:
//            ((unsigned char *) &(tmp.ui))[i++] = c;
//            if (i == 2)
//            {
//                // Speed up UART to 9600 bps
//                SPBRGH = 0;
//                SPBRG = 34;
//                BRGH = 1;
//
//                if (tmp.ui != CRC16_value())
////                                        printf("Checksum error (o: %04X, c: %04X)\r\n",
////                                            tmp.ui, CRC16_value());
//                    break;
//                else
//                {
//                    //                    printf("\r\n");
//                    //                    printf("Temperature: ");
//                    //                    if (temperature >= 0)
//                    //                        putch('+');
//                    //                    else
//                    //                        putch('-');
//                    //                    temperature = temperature >= 0 ? temperature : -temperature;
//                    printf("*@;%04X:%04X:%02X;\r\n", voltage, temperature + 10000,
//                            humidity);
////                    printf("%u %u %u\r\n", temperature + 10000, voltage, humidity);
//                    //                    printf("%d.%02d", (unsigned char)(temperature / 100),
//                    //                        (unsigned char)(temperature % 10));
//                    //                    printf(" C, VDD: %d mV, RH: %u%%\r\n", voltage, humidity);
//                }
//                state = S_INIT;
//
//
//
//                // Slow down UART to 300 bps
//                BRGH = 0;
//                SPBRGH = 0x06;
//                SPBRG = 0x81;
//            }
//            break;
//    }
//}

void main()
{
    OSCCON = 0b01110000;
    //PLLEN = 1;

    GIE = 1;
    PEIE = 1;

    USART_init();
    RCIE = 1;
    SPEN = 1;
    CREN = 1;

    SPBRGH = 0;
    SPBRG = 34;
    BRGH = 1;
    printf("*@;0000:0000:00;\r\n");
    BRGH = 0;
    SPBRGH = 0x06;
    SPBRG = 0x81;

    while (1)
    {
        unsigned char c, byte, recv;
        unsigned char end;

        if (bufferGetch() == 0xF0)
            end = 0;
        else
            end = 1;

        while (!end)
        {
            do
            {
                c = bufferGetch();
            } while (c == 0xF0);

            byte = 0;
            c = MAN_decode(c);
            if (c & 0xF0)
            {
                end = 1;
                continue;
            }
            byte = c << 4;

            c = MAN_decode(bufferGetch());
            if (c & 0xF0)
            {
                end = 1;
                continue;
            }
            byte += c & 0x0F;
            processPacket(byte);
        }
    }
}
