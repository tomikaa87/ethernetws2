/**
 * Wireless outdoor thermometer module software
 * 
 * Data packet format:
 *   _padding___ _frame_type _temp_data_ ___CRC16___ _padding___
 *   [0x00 0xFF] [0xFA 0xAF] [0xXX 0xXX] [0xXX 0xXX] [0x00 0xFF]
 *
 * @author ToMikaa
 * @data 2011-12-02
 * @file main.c
 */

#include <htc.h>
#include <stdio.h>

__CONFIG(FOSC_INTOSCIO & 
         WDTE_OFF & 
         PWRTE_ON & 
         BOREN_NSLEEP & 
         IESO_OFF & 
         MCLRE_OFF &
         FCMEN_OFF &
         CP_ON);

/*** Hardware configuration ***************************************************/

#define _XTAL_FREQ                  2000000ul

#define UART_BITRATE                600
#define UART_TXDATA                 GP0
#define UART_TXTRIS                 TRISIO0

#define ONE_WIRE_PIN                GP2
#define ONE_WIRE_TRIS               TRISIO2

#define DS18B20_RESOLUTION_BITS     12

#define RADIO_POWER_PIN             GP5
#define RADIO_POWER_PIN_TRIS        TRISIO5

#define SENSOR_POWER_PIN            GP4
#define SENSOR_POWER_PIN_TRIS       TRISIO4

/*** Software UART ************************************************************/

#define UART_INIT()         (UART_TXTRIS = 0)
#define UART_SCALER         1000000ul
#define UART_ITIME          (4 * UART_SCALER / _XTAL_FREQ)
#if UART_BITRATE > 1200
    #define	UART_DLY        3   /* cycles per null loop */
    #define	UART_TX_OHEAD   13  /* overhead cycles per loop */
#else
    #define	UART_DLY        9   /* cycles per null loop */
    #define UART_TX_OHEAD   14
#endif
#define	UART_DELAY(ohead) \
    ((UART_SCALER / UART_BITRATE - ohead * UART_ITIME) / (UART_DLY * UART_ITIME))
    
/**
 * Writes the given \p c character to the wireless transmitter.
 */
void putch(char c)
{
    unsigned char	bitno;
#if UART_BITRATE > 1200
    unsigned char	dly;
#else
    unsigned int	dly;
#endif

    UART_INIT();
    
    // Write start bit
    UART_TXDATA = 0;
    
    bitno = 12;
    do 
    {
        // Wait 1 bit time
        dly = UART_DELAY(UART_TX_OHEAD);
        do {} while (--dly);
        
        // Write data bit
        if (c & 1)
            UART_TXDATA = 1;
        if (!(c & 1))
            UART_TXDATA = 0;
            
        c = (c >> 1) | 0x80;
    } 
    while(--bitno);
    
    asm("nop"); 
}    

/*** Delay ********************************************************************/

/*#define dly1u asm("nop")
#define dly2u dly1u;dly1u
#define dly5u dly2u;dly2u;dly1u

unsigned char _dly_cnt;
#define delayUs(x) { \
    _dly_cnt = (unsigned char)(x / 4); \
    asm("nop"); \
    asm("decfsz __dly_cnt, f"); \
    asm("goto $ - 2"); \
} 

void delayBigUs(unsigned int cnt)
{
	unsigned char	i;

	i = (unsigned char)(cnt>>8);
	while(i>=1)
	{
		i--;
		delayUs(253);
		//CLRWDT();
	}
	delayUs((unsigned char)(cnt & 0xFF));
}

void delayBigMs(unsigned int cnt)
{
	unsigned char	i;
	do 
    {
		i = 4;
		do 
        {
			delayUs(250);
			//CLRWDT();
		} 
		while(--i);
	} 
	while(--cnt);
}*/
void delayBigMs(unsigned int cnt)
{
    unsigned char i;
    do
    {
        i = 4;
        do
        {
            __delay_us(250);
        }
        while (--i);
    }
    while (--cnt);                   
}    

/*** 1Wire ********************************************************************/

#define OW_FLOAT()  (ONE_WIRE_TRIS = 1)
#define OW_LOW()    (ONE_WIRE_PIN = 0, ONE_WIRE_TRIS = 0)
#define OW_HIGH()   (ONE_WIRE_PIN = 1, ONE_WIRE_TRIS = 0)

#define OW_DELAY_A()    (__delay_us(8))     // Recommended: 6
#define OW_DELAY_B()    (__delay_us(64))    // 64
#define OW_DELAY_C()    (__delay_us(64))    // 60
#define OW_DELAY_D()    (__delay_us(12))    // 10
#define OW_DELAY_E()    (__delay_us(8))     // 9
#define OW_DELAY_F()    (__delay_us(56))    // 55
#define OW_DELAY_G()    void                // 0
#define OW_DELAY_H()    (__delay_us(200), __delay_us(200), __delay_us(200)) // 480
#define OW_DELAY_I()    (__delay_us(72))    // 70
#define OW_DELAY_J()    (__delay_us(208), __delay_us(208)) // 410

/**
 * Initializes the 1-wire bus
 * 
 * @return 0 if devices is connected, 1 if not connected or 2 on bus error
 */
unsigned char OW_reset()
{
    unsigned char presence, temp;
    
    OW_DELAY_G();
    OW_LOW();
    OW_DELAY_H();
    OW_FLOAT();
    OW_DELAY_I();
    presence = ONE_WIRE_PIN;
    OW_DELAY_J();
    temp = ONE_WIRE_PIN;
   
    return (!temp ? 2 : presence);
}

/**
 * Writes a bit to the 1-wire bus
 */
void OW_writeBit(unsigned char b)
{
    OW_FLOAT();
    
    if (b)
    {
        OW_LOW();
        OW_DELAY_A();
        OW_FLOAT();
        OW_DELAY_B();   
    }    
    else
    {
        OW_LOW();
        OW_DELAY_C();
        OW_FLOAT();
        OW_DELAY_D();   
    }    
} 

/**
 * Reads a bit from the 1-wire bus
 */
unsigned char OW_readBit()
{
    unsigned char data;
    
    OW_FLOAT();
    
    OW_LOW();
    OW_DELAY_A();
    OW_FLOAT();
    OW_DELAY_E();
    data = ONE_WIRE_PIN;
    OW_DELAY_F();
   
    return data;   
} 

/**
 * Writes a data byte to the 1-wire bus
 */
void OW_writeByte(unsigned char b)
{
    unsigned char i = 8;
    
    while (i--)
    {
        OW_writeBit(b & 0x01);
        b >>= 1;   
    }    
}

/**
 * Reads a data byte from the 1-wire bus
 */
unsigned char OW_readByte()
{
    unsigned char data, i;
    
    data = 0;
    for (i = 0; i < 8; i++)
    {
        if (OW_readBit())
            data |= (0x01 << i);  
    }   
    
    return data; 
} 

/*** DS18B20 ******************************************************************/

/**
 * Reads the current temperature value from
 * the DS18X20 1-wire sensor
 *
 * @return The read temperature multiplied by 100
 */
signed int DS1820_readSensor()
{
    unsigned char hi, lo, temp_whole;
    unsigned int temp_i, temp_fraction;
    signed int temp;
    
    if (OW_reset() > 0)
        return -8888;
        
    OW_writeByte(0xCC);
    OW_writeByte(0x44);
    
    delayBigMs(600);
    
    if (OW_reset() > 0)
        return -8888;
        
    OW_writeByte(0xCC);
    OW_writeByte(0xBE);
    lo = OW_readByte();
    hi = OW_readByte();
    temp_i = (hi << 8) + lo;
    
    if (temp_i & 0x8000)
    {
        temp_i = ~temp_i + 1;
    }
    
    temp = 0;
    
    temp_whole = temp_i >> (DS18B20_RESOLUTION_BITS - 8);
    temp += temp_whole * 100;
    
    temp_fraction = temp_i << (4 - (DS18B20_RESOLUTION_BITS - 8));
    temp_fraction &= 0x000F;
    temp_fraction *= 625;
    temp += temp_fraction / 100;
    
    if (temp_i & 0x8000)
    {
        temp *= -1;
    }

    return temp;
} 

/*** CRC16 ********************************************************************/

#define CRC16_POLYNOMIAL    0x1021

static unsigned int CRC16_runningValue;

void CRC16_reset()
{
    CRC16_runningValue = 0;   
}    

unsigned int CRC16_value()
{
    return CRC16_runningValue;
}    

void CRC16_byte(unsigned char ch)
{
    unsigned char i;

    CRC16_runningValue ^= ch;
    i = 8;

    do
    {
        if (CRC16_runningValue & 0x8000)
            CRC16_runningValue <<= 1;
        else
        {
            CRC16_runningValue <<= 1;
            CRC16_runningValue ^= CRC16_POLYNOMIAL;
        }
    }
    while (--i);
}

/*** Peripheral control *******************************************************/

#define RADIO_POWER_ON()    (RADIO_POWER_PIN = 1)
#define RADIO_POWER_OFF()   (RADIO_POWER_PIN = 0)

#define SENSOR_POWER_ON()   (SENSOR_POWER_PIN = 1)
#define SENSOR_POWER_OFF()  (SENSOR_POWER_PIN = 0)

/*** Data packet settings *****************************************************/

#define PACKET_PADDING    2
#define PACKET_COUNT      3

/*** Manchester ***************************************************************/

void MAN_putch(unsigned char c)
{
    unsigned char i, nibble, tmp;
    
    nibble = 0;
    tmp = c;
    for (i = 0; i < 8; i++)
    {
        if (tmp & 0x80)
            nibble |= 2; // 10
        else
            nibble |= 1; // 01
        tmp <<= 1;
        if (i == 3 || i == 7)
        {
            putch(nibble);
            nibble = 0;
        }
        else
            nibble <<= 2;
    }       
}    

void MAN_sendSS()
{
    unsigned char i;
    
    for (i = 0; i < 4; i++)
        putch(0xF0);   
}    

/*** Main *********************************************************************/

#define DHT11_IO_PIN    GP4
#define DHT11_IO_TRIS   TRISIO4

void read_dht11()
{
    unsigned int i;
    
    GP5 = 1;
    TRISIO5 = 0;
    delayBigMs(1000);
    GP5 = 0;
    
    // Send start signal
    DHT11_IO_PIN = 0;
    DHT11_IO_TRIS = 0;
    delayBigMs(18);
    DHT11_IO_TRIS = 1;
    
    __delay_us(80);
    
    // Wait for response
    while (DHT11_IO_PIN);
    
    GP5 = 1;
    
    for (i = 0; i < 40; i++)
    {
           
    }    
    
}    

void main()
{
    OSCCON = 0b01010000;
    ANSEL = 0;
    CMCON0 = 7;
    
    
    
    while (1)
    {
        read_dht11();
    }    
}    

void amain()
{
    unsigned char i;
    union
    {
        signed int si;
        unsigned int ui;
        struct 
        {
            unsigned char uc1;
            unsigned char uc2;       
        };      
    } myWord;    
    unsigned char packet[6], packetPtr;
    
    OSCCON = 0b01010000;
    ANSEL = 0;
    CMCON0 = 7;
    
    RADIO_POWER_PIN_TRIS = 0;
    SENSOR_POWER_PIN_TRIS = 0;
    
    // Configure Watchdog
    PSA = 1;
    WDTCON = 0b00001100;        // 1:2048 post-scaler
    
    //RADIO_POWER_ON();
    //UART_TXTRIS = 0;
    /*while (1)
    {        
        MAN_sendSS();        
        MAN_putch(0b10000010);
        MAN_sendSS();
        delayBigMs(2000);
    } */   
    
    while (1)
    {
        // Disable watchdog
        SWDTEN = 0;
        
        SENSOR_POWER_ON(); 
        myWord.si = DS1820_readSensor();
        SENSOR_POWER_OFF();
        
        // Zero out the buffer
        for (packetPtr = 0; packetPtr < sizeof (packet); packetPtr++)
            packet[packetPtr] = 1;
        
        // Generate packet padding
        packetPtr = 0;
        // Write frame indicator
        packet[packetPtr++] = 0xFA;
        packet[packetPtr++] = 0xAF;
        // Write temperature data
        packet[packetPtr++] = myWord.uc1;
        packet[packetPtr++] = myWord.uc2;
        // Generate CRC16
        CRC16_reset();
        CRC16_byte(myWord.uc1);
        CRC16_byte(myWord.uc2);
        myWord.ui = CRC16_value();
        packet[packetPtr++] = myWord.uc1;
        packet[packetPtr++] = myWord.uc2;
        
        RADIO_POWER_ON();
        for (i = 0; i < PACKET_COUNT; i++)
        {
            MAN_sendSS();
            for (packetPtr = 0; packetPtr < sizeof (packet); packetPtr++)
                MAN_putch(packet[packetPtr]);
            MAN_sendSS();
            //printf("\r\n@@%04d@@\r\n", myWord.si);
        }
        RADIO_POWER_OFF();
        
        // Enable Watchdog
        SWDTEN = 1;
        
        // ZZZzzzz...
        asm("sleep");
    }       
}      