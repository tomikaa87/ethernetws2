/**
 * Ethernet Weather Station 2 Firmware
 * CRC-16 checksum calculation algorithms
 *
 * @author Tamas Karpati <tomikaa87>
 * @date 2012. okt�ber 18.
 */

#ifndef CRC16_H
#define CRC16_H

#include "types.h"

uint16_t CRC16_CalculateBufferChecksum(char *data, uint16_t len);
void CRC16_Reset();
void CRC16_Update(uint8_t byte);
uint16_t CRC16_GetValue();

#endif /* CRC16_H */