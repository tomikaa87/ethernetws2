/**
 * Ethernet Weather Station 2 Firmware
 * Basic and advanced type definitions
 *
 * @author Tamas Karpati <tomikaa87>
 * @date 2012. okt�ber 18.
 */

#ifndef TYPES_H
#define TYPES_H

#include <stdint.h>

#ifndef __PACKED
#define __PACKED
#endif

// Advanced 8 bit integer type with bit fields and direct access to nibbles

typedef union {

    struct __PACKED {
        unsigned b1 : 1;
        unsigned b2 : 1;
        unsigned b3 : 1;
        unsigned b4 : 1;
        unsigned b5 : 1;
        unsigned b6 : 1;
        unsigned b7 : 1;
        unsigned b8 : 1;
    } ;

    struct __PACKED {
        unsigned hi : 4;
        unsigned lo : 4;
    } ;
    int8_t si;
    uint8_t ui;
} DecomposedInt8;

// Advanced 16-bit integer type

typedef union {

    struct __PACKED {
        DecomposedInt8 lb;
        DecomposedInt8 hb;
    } components;

    struct {
        uint8_t lb;
        uint8_t hb;
    } ;
    uint8_t bytes[2];
    DecomposedInt8 decomposed[2];
    int16_t si;
    uint16_t ui;
} DecomposedInt16;

// Advanced 24-bit integer type

typedef union {

    struct __PACKED {
        DecomposedInt8 lb;
        DecomposedInt8 hb;
        DecomposedInt8 ub;
    } components;

    struct __PACKED {
        uint8_t lb;
        uint8_t hb;
        uint8_t ub;
    } ;
    uint8_t bytes[3];
    DecomposedInt8 decomposed[3];
    int24_t si;
    uint24_t ui;
} DecomposedInt24;

// Advanced 32-bit integer type

typedef union {

    struct __PACKED {
        DecomposedInt8 lb;
        DecomposedInt8 hb;
        DecomposedInt8 ub;
        DecomposedInt8 mb;
    } components;

    struct __PACKED {
        DecomposedInt16 lw;
        DecomposedInt16 hw;
    } componentWords;

    struct __PACKED {
        uint8_t lb;
        uint8_t hb;
        uint8_t ub;
        uint8_t mb;
    } ;

    struct {
        uint16_t lw;
        uint16_t hw;
    } ;
    uint8_t bytes[4];
    DecomposedInt8 decomposed[4];
    int16_t words[2];
    DecomposedInt16 decomposedWords[2];
    int32_t si;
    uint32_t ui;
} DecomposedInt32;

#endif // TYPES_H