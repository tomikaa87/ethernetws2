/**
 * Ethernet Weather Station 2 Firmware
 * CRC-16 checksum calculation algorithms
 *
 * @author Tamas Karpati <tomikaa87>
 * @date 2012. okt�ber 18.
 */

#include "crc16.h"

static const uint16_t CRCTable[16] = {
    0x0000, 0x1081, 0x2102, 0x3183,
    0x4204, 0x5285, 0x6306, 0x7387,
    0x8408, 0x9489, 0xa50a, 0xb58b,
    0xc60c, 0xd68d, 0xe70e, 0xf78f
};
static uint16_t g_currentValue;


/**
 * Returns the CRC-16 checksum of the given data array.
 * @param data The data array
 * @param len The length of the data array
 * @return CRC-16 checksum
 */
uint16_t CRC16_CalculateBufferChecksum(char *data, uint16_t len)
{
    uint16_t crc = 0xffff;
    uint8_t c;
    const uint8_t *p = (uint8_t *)data;
    while (len--) {
        c = *p++;
        crc = ((crc >> 4) & 0x0fff) ^ CRCTable[((crc ^ c) & 15)];
        c >>= 4;
        crc = ((crc >> 4) & 0x0fff) ^ CRCTable[((crc ^ c) & 15)];
    }
    return ~crc & 0xffff;
}


/**
 * Resets the online CRC-16 checksum calculator.
 */
void CRC16_Reset()
{
    g_currentValue = 0xffff;
}


/**
 * Adds a byte to the online CRC-16 checksum value.
 * @param byte The data byte
 */
void CRC16_Update(uint8_t byte)
{
    g_currentValue = ((g_currentValue >> 4) & 0x0fff) ^ CRCTable[((g_currentValue ^ byte) & 15)];
    byte >>= 4;
    g_currentValue = ((g_currentValue >> 4) & 0x0fff) ^ CRCTable[((g_currentValue ^ byte) & 15)];
}


/**
 * Returns the result of the online checksum calculator.
 * @return The calculated CRC-16 value
 */
uint16_t CRC16_GetValue()
{
    return ~g_currentValue & 0xffff;
}