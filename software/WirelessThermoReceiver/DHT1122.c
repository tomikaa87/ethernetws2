#include "DHT1122.h"

#include <htc.h>

#include <stdio.h>

/*** MCU clock frequency ******************************************************/

#ifndef _XTAL_FREQ
    #define _XTAL_FREQ                  32000000ul
#endif

/*** Data pin configuration ***************************************************/

#define DHT_DATA_IN                     RA2
#define DHT_DATA_OUT                    LATA2
#define DHT_DATA_TRIS                   TRISA2

// Uncomment this line to enable power management
//#define DHT_USE_POWER_PIN

#ifdef DHT_USE_POWER_PIN
    #define DHT_POWER_PIN               LATA1
    #define DHT_POWER_PIN_TRIS          TRISA1
#endif

//#define DHT_USE_WEAK_PULL_UP

#ifdef DHT_USE_WEAK_PULL_UP
#define DHT_WEAK_PULL_UP                WPUA2
#endif

/*** Sensor type configuration ************************************************/

// Uncomment this line to use this driver with DHT22, instead of DHT11
//#define DHT_SENSOR_DHT22

/*** DON'T TOUCH ANYTHING BELOW THIS LINE *************************************/

#ifdef DHT_SENSOR_DHT22
    #define DHT_INIT_DELAY               1500
#else
    #define DHT_INIT_DELAY               200
#endif

#ifdef DHT_SENSOR_DHT22
    #define DHT_REQ_DELAY                1
#else
    #define DHT_REQ_DELAY                18
#endif

/*** Global variables *********************************************************/

static unsigned char dhtData[5];

/*** Utility functions ********************************************************/

//void delayMs(unsigned int ms)
//{
//    unsigned char i;
//    do {
//        i = 4;
//        do {
//            __delay_us(250);
//        } while (--i);
//    } while (--ms);
//}

/*** API function implementations *********************************************/

char DHT_readSensor()
{
    unsigned char rawData[43], i, bitIndex, errors;
    int bitThreshold, bitMin, bitMax;

    for (i = 0; i < 5; i++)
        dhtData[i] = 0xFF;

    // Power up the sensor
#ifdef DHT_USE_POWER_PIN
    DHT_POWER_PIN_TRIS = 0;
    DHT_POWER_PIN = 1;

     // Wait for the sensor to initialize
    delayMs(DHT_INIT_DELAY);
#elif !defined(DHT_SENSOR_DHT22)
    __delay_ms(DHT_INIT_DELAY);
#endif

#ifdef DHT_USE_WEAK_PULL_UP
    DHT_WEAK_PULL_UP = 1;
#endif

    // Send 'Request data' signal, wake up the sensor
    DHT_DATA_OUT = 0;
    DHT_DATA_TRIS = 0;
    __delay_ms(DHT_REQ_DELAY);
    DHT_DATA_TRIS = 1;

//    // Skip first two edges of response
//    for (i = 0; i < 2; i++) {
//        unsigned char counter;
//        counter = 0;
//        while (DHT_DATA_IN == 0 && counter++ < 254)
//            continue;
//        if (counter >= 254)
//            return DHT_TIMEOUT_ERROR;
//        counter = 0;
//        while (DHT_DATA_IN == 1 && counter++ < 254)
//            continue;
//        if (counter >= 254)
//            return DHT_TIMEOUT_ERROR;
//    }

    // Read 2+40+1 data bits from the sensor
    for (i = 0; i < 43; i++) {
        unsigned char counter;
        // l->h
        counter = 0;
        while (DHT_DATA_IN == 0 && counter++ < 254)
            continue;
//        if (counter >= 254)
//            return DHT_TIMEOUT_ERROR;
        // h->l
        counter = 0;
        while (DHT_DATA_IN == 1 && counter++ < 254)
            continue;
        rawData[i] = counter;
    }

    // Power down the sensor
#ifdef DHT_USE_POWER_PIN
    DHT_POWER_PIN = 0;
#endif

#ifdef DHT_USE_WEAK_PULL_UP
    DHT_WEAK_PULL_UP = 0;
#endif

//    printf("rawData:");
//    for (i = 0; i < 42; i++) {
//        printf(" %u", rawData[i]);
//    }
//    printf("\r\n");

    // Calculate bit threshold (counter value for '1' bits)
    bitMin = 255;
    bitMax = 0;
    errors = 0;
    for (i = 2; i < 42; i++) {
        // Find the smallest value but filter out zeroes
        if (rawData[i] < bitMin && rawData[i] != 0)
            bitMin = rawData[i];
        // Find the largest value but filter out 255s
        if (rawData[i] > bitMax && rawData[i] != 255)
            bitMax = rawData[i];
        if (rawData[i] >= 254)
            errors++;
    }

    if (errors > 3)
        return DHT_TIMEOUT_ERROR;

    // Calculate threshold between logic '0' and '1'
    bitThreshold = (bitMin + bitMax) / 2;
    // Decrease the threshold by 33% to get stable readings
    bitThreshold -= bitThreshold / 3;
//    printf("bitMin = %d, bitMax = %d, bitThreshold = %d\r\n",
//            bitMin, bitMax, bitThreshold);

    if (bitMin == bitMax || bitThreshold == 0)
        return DHT_BIT_THRESHOLD_ERROR;

//    printf("DHT data:");

    unsigned char tempRangeError = 1;

    // Convert raw data
    for (i = 0; i < 5; i++) {
        unsigned char byte = 0;
        for (bitIndex = 0; bitIndex < 8; bitIndex++) {
            if (rawData[i * 8 + bitIndex + 2] > bitThreshold)
                byte |= (1 << (7 - bitIndex));
        }

        if (byte < 255 && tempRangeError)
            tempRangeError = 0;

        dhtData[i] = byte;
//        printf(" %02X", byte);
    }
//    printf("\r\n");

    if (tempRangeError)
        return DHT_TEMP_RANGE_ERROR;

    // Verify checksum
    unsigned int sum = 0;
    for (i = 0; i < 4; i++)
        sum += dhtData[i];
    if ((sum & 0xFF) != dhtData[4])
    {
        // DHT11 sometimes cuts the last bit of the data stream.
        // Try again with changing the last checksum bit.
        sum &= 0xFE;
        if ((sum & 0xFF) != dhtData[4]) {
//            printf("rawData:");
//            for (i = 0; i < 43; i++) {
//                printf(" %u", rawData[i]);
//            }
//            printf("\r\n");
//            printf("bitMin = %d, bitMax = %d, bitThreshold = %d\r\n",
//                bitMin, bitMax, bitThreshold);
            return DHT_CHECKSUM_ERROR;
        }
    }

    return DHT_OK;
}

signed int DHT_relativeHumidityMul10()
{
#ifdef DHT_SENSOR_DHT22
    union {
        struct {
            unsigned char lo;
            unsigned char hi;
        };
        signed int si;
    } word;

    word.lo = dhtData[1];
    word.hi = dhtData[0];

    return word.si;
#else
    return dhtData[0] * 10;
#endif
}

signed int DHT_temperatureMul10()
{
#ifdef DHT_SENSOR_DHT22
    union {
        struct {
            unsigned char lo;
            unsigned char hi;
        };
        signed int si;
    } word;

    word.lo = dhtData[3];
    word.hi = dhtData[2] & 0x7F;
    // Handle sign bit
    if (dhtData[2] & 0x80)
        word.si *= -1;

    return word.si;
#else
    return dhtData[2] * 10;
#endif
}

unsigned char DHT_dataByte(unsigned char index)
{
    return dhtData[index];
}
