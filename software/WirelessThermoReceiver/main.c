/**
 * Wireless Thermometer Receiver unit firmware
 *
 * @author Tamas Karpati
 * @date 2012.03.15
 */

#include "DHT1122.h"

#include <xc.h>
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <math.h>

#define ALTITUDE    115.54

/*** PIC configuration ********************************************************/

// CONFIG1
#pragma config FOSC = INTOSC    // Oscillator Selection (INTOSC oscillator: I/O function on CLKIN pin)
#pragma config WDTE = SWDTEN    // Watchdog Timer Enable (WDT controlled by the SWDTEN bit in the WDTCON register)
#pragma config PWRTE = ON       // Power-up Timer Enable (PWRT enabled)
#pragma config MCLRE = OFF      // MCLR Pin Function Select (MCLR/VPP pin function is digital input)
#pragma config CP = ON          // Flash Program Memory Code Protection (Program memory code protection is enabled)
#pragma config CPD = OFF        // Data Memory Code Protection (Data memory code protection is disabled)
#pragma config BOREN = ON       // Brown-out Reset Enable (Brown-out Reset enabled)
#pragma config CLKOUTEN = OFF   // Clock Out Enable (CLKOUT function is disabled. I/O or oscillator function on the CLKOUT pin)
#pragma config IESO = OFF       // Internal/External Switchover (Internal/External Switchover mode is disabled)
#pragma config FCMEN = OFF      // Fail-Safe Clock Monitor Enable (Fail-Safe Clock Monitor is disabled)

// CONFIG2
#pragma config WRT = ALL        // Flash Memory Self-Write Protection (000h to FFFh write protected, no addresses may be modified by EECON control)
#pragma config PLLEN = OFF      // PLL Enable (4x PLL disabled)
#pragma config STVREN = ON      // Stack Overflow/Underflow Reset Enable (Stack Overflow or Underflow will cause a Reset)
#pragma config BORV = HI        // Brown-out Reset Voltage Selection (Brown-out Reset Voltage (Vbor), high trip point selected.)
#pragma config LVP = OFF        // Low-Voltage Programming Enable (High-voltage on MCLR/VPP must be used for programming)

#ifndef _XTAL_FREQ
#define _XTAL_FREQ                      32000000ul
#endif

#define BATT_ADC_CAL_VOLTAGE        4.3
#define BATT_ADC_CAL_VALUE          485

/*** Global variables *********************************************************/

#define BUFFER_SIZE         50
unsigned char buffer[BUFFER_SIZE];
unsigned char bufferInIndex = 0;
unsigned char bufferOutIndex = 0;
#define bufferHasData() (bufferInIndex != bufferOutIndex)

bit g_isTimeout = 0;

/*** UART routines ************************************************************/

unsigned char bufferGetch()
{
    unsigned char c;

    while (!bufferHasData())
        continue;

    c = buffer[bufferOutIndex];
    bufferOutIndex = (bufferOutIndex + 1) % BUFFER_SIZE;

    return c;
}

//#ifndef _XTAL_FREQ
//    #error "_XTAL_FREQ must be set"
//#endif
//
//#define UART_RADIO_BAUD               600
//#define UART_RADIO_HIGH_SPEED         0
//
//#if (UART_RADIO_HIGH_SPEED == 1)
//    #define UART_RADIO_DIVIDER        16
//#else
//    #define UART_RADIO_DIVIDER        64
//#endif
//#define UART_RADIO_SPBRG (unsigned char)(_XTAL_FREQ / UART_RADIO_BAUD / UART_RADIO_DIVIDER - 1)
//#define UART_RADIO_REAL_BAUD (unsigned long)(_XTAL_FREQ / (UART_RADIO_DIVIDER * (UART_RADIO_SPBRG + 1)))
//#define UART_RADIO_BAUD_ERROR \
//    (signed char)((((float)UART_RADIO_REAL_BAUD - (float)UART_RADIO_BAUD) / (float)UART_RADIO_BAUD) * 100.0)
//
//char Warning_UartRadioErrorTooHigh[UART_RADIO_BAUD_ERROR <= 3];
//
//#define UART_TX_BAUD               57600ul
//#define UART_TX_HIGH_SPEED         1
//
//#if (UART_TX_HIGH_SPEED == 1)
//    #define UART_TX_DIVIDER        16ul
//#else
//    #define UART_TX_DIVIDER        64ul
//#endif
//#define UART_TX_SPBRG (unsigned char)(_XTAL_FREQ / UART_TX_BAUD / UART_TX_DIVIDER - 1)
//#define UART_TX_REAL_BAUD (unsigned long)(_XTAL_FREQ / (UART_TX_DIVIDER * (UART_TX_SPBRG + 1)))
//#define UART_TX_BAUD_ERROR \
//    (signed char)((((float)UART_TX_REAL_BAUD - (float)UART_TX_BAUD) / (float)UART_TX_BAUD) * 100.0)

//char Warning_UartTxErrorTooHigh[UART_TX_BAUD_ERROR <= 3];

/**
 * 600 bps @8 MHz
 */
void UART_switchToRadioSpeed()
{
    BRGH = 1;
    BRG16 = 1;
    SPBRGH = 0x34;
    SPBRG = 0x14;
//    BRGH = UART_RADIO_HIGH_SPEED;
//    SPBRGH = (UART_RADIO_SPBRG >> 8) & 0xFF;
//    SPBRG = UART_RADIO_SPBRG & 0xFF;
}

/**
 * 57600 bps @8 MHz
 */
void UART_switchToTransmitSpeed()
{
    BRGH = 1;
    BRG16 = 1;
    SPBRGH = 0;
    //SPBRG = 0x89; // 57600 bps
    SPBRG = 0x44; // 115200
//    BRGH = UART_TX_HIGH_SPEED;
//    SPBRGH = (UART_TX_SPBRG >> 8) & 0xFF;
//    SPBRG = UART_TX_SPBRG & 0xFF;
}

void USART_init()
{
    TX9 = 0;
    SYNC = 0;
    SREN = 0;
    TXEN = 1;
    
    RCIE = 1;
    SPEN = 1;
    CREN = 1;

    TRISA5 = 1;
    TRISA4 = 0;
}

void putch(unsigned char c)
{
    while (!TRMT)
        continue;
    TXREG = c;
    while (!TRMT)
        continue;
}

unsigned char getch()
{
    while (!RCIF)
        continue;
    return RCREG;
}

/*** CRC checksum routines ****************************************************/

#define CRC16_POLYNOMIAL    0x1021

static unsigned int CRC16_runningValue;

void CRC16_reset()
{
    CRC16_runningValue = 0;
}

unsigned int CRC16_value()
{
    return CRC16_runningValue;
}

void CRC16_byte(unsigned char ch)
{
    unsigned char i;

    CRC16_runningValue ^= ch;
    i = 8;

    do
    {
        if (CRC16_runningValue & 0x8000)
            CRC16_runningValue <<= 1;
        else
        {
            CRC16_runningValue <<= 1;
            CRC16_runningValue ^= CRC16_POLYNOMIAL;
        }
    }    while (--i);
}

/*** Manchester decoder *******************************************************/

unsigned char MAN_decode(unsigned char encoded)
{
    unsigned char decoded, pattern, i;

    if (encoded == 0xF0)
        return 0xF0;

    decoded = 0;

    for (i = 0; i < 4; i++)
    {
        // First 2 MSB bits
        pattern = encoded & 0xC0;
        // 10 -> 1
        if (pattern == 0x80)
            decoded |= 1;
            // 01 -> 0
        else if (pattern == 0x40)
            decoded |= 0;
            // Invalid code
        else
            return 0xFF;
        decoded <<= 1;
        encoded <<= 2;
    }

    decoded >>= 1;

    return decoded;
}

/*** Battery management *******************************************************/

unsigned int measureVddMilliVolts()
{
    union {
        struct {
            unsigned char l;
            unsigned char h;
        };
        unsigned int u;
    } result;

    // Setup Fixed Voltage Reference, 1.024 Volts for ADC
    FVRCON = 0b10000010;
    // Wait until FVR stabilizes the output voltage
    while (!FVRRDY)
        continue;

    // Setup ADC, use FVR as input, VDD as Vref+, read from AN2, ADFM = 1
    ADCON0 = 0b01111101;
    ADCON1 = 0b11110000;

    // Start conversion and wait for it to finish
    GO = 1;
    while (GO)
        continue;

    // Read conversion result
    result.h = ADRESH;
    result.l = ADRESL;

    // Calculate VDD
    float vdd = (BATT_ADC_CAL_VOLTAGE * (float)BATT_ADC_CAL_VALUE) /
            (float)(result.u);
    unsigned int vddMilliVolts = (unsigned int)(vdd * 100.0) * 10;

    // Disable ADC and FVR
    ADON = 0;
    FVRCON = 0;

    return vddMilliVolts;
}

/*** Interrupt service routine ************************************************/

void interrupt isr()
{
    if (RCIF)
    {
        unsigned char c;

        buffer[bufferInIndex] = RCREG;
        c = bufferInIndex;
        bufferInIndex = (bufferInIndex + 1) % BUFFER_SIZE;
        if (bufferInIndex == bufferOutIndex)
            bufferInIndex = c;
    }

    if (TMR1IF)
    {
        TMR1H = 0x3C;
        TMR1L = 0xB0;
        TMR1IF = 0;

        static uint8_t counter = 0;
        if (++counter == 200)
        {
            // Timeout at 10 s
            g_isTimeout = 1;
            counter = 0;
        }
    }
}

/*** Packet processor *********************************************************/

/*
 * Packet format
 * [0xFA 0xAF][VOLTAGE][DHT_TEMP][DHT_RH][BMP085_TEMP][BMP085_PRES][ERROR_CODE][CHECKSUM]
 *  __ 2 B __  _ 2 B _  __ 2 B _  _2 B__  __ 2 B ____  __ 4 B ____  __ 1 B ___  __ 2 B _
 *
 * Total size: 17 Bytes
 */

enum {
        S_INIT, S_FRAME, S_DATA
} state = S_INIT;

void processPacket(unsigned char c)
{
    static unsigned char bufPtr;
    static unsigned char buf[15];

    union {
        struct {
            unsigned char lo;
            unsigned char hi;
        };
        signed int si;
        unsigned int ui;
    } Word;

    union {
        struct {
            unsigned char lo;
            unsigned char mid_lo;
            unsigned char mid_hi;
            unsigned char hi;
        };
        unsigned char data[4];
        unsigned long ul;
        signed long sl;
    } Long;

    switch (state) {
        case S_INIT:
            if (c == 0xFA) {
                state = S_FRAME;
            }
            break;

        case S_FRAME:
            if (c == 0xAF) {
                state = S_DATA;
                bufPtr = 0;
                memset(buf, 0, sizeof (buf));
            } else {
                state = S_INIT;
            }
            break;

        case S_DATA:
            if (c == 0xAF) {
                if (bufPtr > 0 && buf[bufPtr - 1] == 0xFA) {
                    state = S_INIT;
                    break;
                }
            }
            if (bufPtr < sizeof (buf)) {
                buf[bufPtr++] = c;
            } else {
                unsigned int wtTxVdd, wtTxRh, wtTxTempDht, wtTxTempBmp, vdd;
                unsigned long wtTxPres, wtTxPresSLP;
                unsigned char wtTxError, dhtError, errorCode = 0;

                // Calculate checksum
                CRC16_reset();
                for (bufPtr = 0; bufPtr < sizeof (buf) - 2; bufPtr++)
                    CRC16_byte(buf[bufPtr]);

                // Verify checksum
                if (CRC16_value() != (((unsigned int)buf[13] << 8) + buf[14]))
                    errorCode = 10;

                // Read voltage
                Word.hi = buf[0];
                Word.lo = buf[1];
                wtTxVdd = Word.ui;

                // Read temperature (DHT22)
                Word.hi = buf[2];
                Word.lo = buf[3];
                wtTxTempDht = Word.si;

                // Read humidity
                Word.hi = buf[4];
                Word.lo = buf[5];
                wtTxRh = Word.ui;

                // Read temperature (BMP085)
                Word.hi = buf[6];
                Word.lo = buf[7];
                wtTxTempBmp = Word.si;

                // Read pressure
                Long.hi = buf[8];
                Long.mid_hi = buf[9];
                Long.mid_lo = buf[10];
                Long.lo = buf[11];
                wtTxPres = Long.ul;

                // Calculate Sea Level Pressure at the given ALTITUDE
                float p0 = ((float)wtTxPres / pow(1 - ALTITUDE / 44330, 5.255));
                wtTxPresSLP = (unsigned long)p0;

                // Read error code
                wtTxError = buf[12];

                // Read DHT11
                dhtError = DHT_readSensor();
                if (dhtError != 0) {
                    // Try reading DHT11 again
                    __delay_ms(100);
                    dhtError = DHT_readSensor();
                }
                errorCode += dhtError;

                // Measure Vdd
                vdd = measureVddMilliVolts();

                UART_switchToTransmitSpeed();

                // Send packet
                // *@;[ERR_OUT]:[TEMP1_OUT]:[TEMP2_OUT]:[RH_OUT]:[PRES_OUT]:[PRES_SL]:[VDD_OUT]:[ERR_IN]:[TEMP_IN]:[RH_IN]:[VDD_IN];
                printf("*@;%02X:%04X:%04X:%04X:%08lX:%08lX:%04X:%02X:%02X:%04X:%02X;\r\n",
                        wtTxError,
                        wtTxTempBmp * 10,
                        wtTxTempDht,
                        wtTxRh,
                        wtTxPres,
                        wtTxPresSLP,
                        wtTxVdd,
                        errorCode,
                        DHT_dataByte(2),
                        DHT_dataByte(0),
                        vdd);

#if 0
                printf("**********************\r\n");
                printf("WT TX VDD: %u\r\n", wtTxVdd);
                printf("WT TX DHT temp: %d\r\n", wtTxTempDht);
                printf("WT TX DHT RH: %u\r\n", wtTxRh);
                printf("WT TX BMP temp: %d\r\n", wtTxTempBmp);
                printf("WT TX BMP pres: %lu\r\n", wtTxPres);
                printf("WT TX error: %u\r\n", wtTxError);
                printf("WT RX DHT temp: %d\r\n", DHT_temperatureMul10() * 10);
                printf("WT RX DHT RH: %u\r\n", DHT_relativeHumidityMul10() * 10);
                printf("WT RX VDD: %u\r\n", vdd);
                printf("WT RX error: %u\r\n", errorCode);
                printf("***\r\n");

                float altitude = 182.92;
                float p0 = ((float)wtTxPres / pow(1 - altitude / 44330, 5.255));
                unsigned long truePres = (unsigned long)p0;
                
                printf("WT TX true pres: %lu\r\n", truePres);
#endif
                
                UART_switchToRadioSpeed();

                state = S_INIT;
            }
            break;
    }
}

void sendLocalMeasurementResult()
{
    // Read DHT11
    uint8_t errorCode = DHT_readSensor();
    if (errorCode != 0) {
        // Try reading DHT11 again
        __delay_ms(100);
        errorCode = DHT_readSensor();
    }

    // Measure Vdd
    uint16_t vdd = measureVddMilliVolts();

    UART_switchToTransmitSpeed();

    // Send packet
    // *@;[ERR_OUT]:[TEMP1_OUT]:[TEMP2_OUT]:[RH_OUT]:[PRES_OUT]:[PRES_SL]:[VDD_OUT]:[ERR_IN]:[TEMP_IN]:[RH_IN]:[VDD_IN];
    // ERR_OUT = 0xFF indicates that it's only local measurement data
    printf("*@;FF:0000:0000:0000:00000000:00000000:0000:%02X:%02X:%04X:%02X;\r\n",
            errorCode,
            DHT_dataByte(2),
            DHT_dataByte(0),
            vdd);

    UART_switchToRadioSpeed();
}

/*** Main *********************************************************************/

void main()
{
    // Int OSC, 8 Mhz
    OSCCON = 0b11110000;

    ADCON0 = 0;
    ADCON1 = 0;
    ANSELA = 0;

    TXCKSEL = 1;
    RXDTSEL = 1;

    // Prescaler 1:8; TMR1 Preload = 15536; Actual Interrupt Time : 50 ms
    T1CON = 0x31;
    TMR1IF = 0;
    TMR1H = 0x3C;
    TMR1L = 0xB0;
    TMR1IE = 1;

    // Initialization delay
    __delay_ms(200);

    GIE = 1;
    PEIE = 1;

    USART_init();


#if 0
    UART_switchToTransmitSpeed();
    printf("Running DHT driver test... ");
    unsigned char pass, fails = 0;
    for (pass = 0; pass < 20; pass++) {
        unsigned char dhtError = DHT_readSensor();
//        unsigned char counter = 0;
//        while (dhtError == 1 && counter++ < 255) {
//            __delay_ms(10);
//            dhtError = DHT_readSensor();
//        }

        if (dhtError != 0) {
            fails++;
            putch('f');
            putch('0' + dhtError);
        } else {
            putch('.');
        }

        __delay_ms(1000);
    }
    printf(" ... Failures: %u of %u\r\n", fails, pass);

    printf("DHT read result: %5d\r\n", DHT_readSensor());
    printf("DHT temperature: %5dC\r\n", DHT_dataByte(2));
    printf("DHT humidity:    %5d%%\r\n", DHT_dataByte(0));
#endif 

    UART_switchToRadioSpeed();
    g_isTimeout = 1;

    while (1)
    {
        if (g_isTimeout)
        {
            g_isTimeout = 0;
            sendLocalMeasurementResult();
        }

        if (bufferHasData())
        {
            uint8_t c, byte, end;

            if (bufferGetch() == 0xF0)
                end = 0;
            else
                end = 1;

            while (!end)
            {
                do
                {
                    c = bufferGetch();
                } while (c == 0xF0);

                byte = 0;
                c = MAN_decode(c);
                if (c & 0xF0)
                {
                    end = 1;
                    continue;
                }
                byte = c << 4;

                c = MAN_decode(bufferGetch());
                if (c & 0xF0)
                {
                    end = 1;
                    continue;
                }
                byte += c & 0x0F;
                processPacket(byte);
            }
        }
    }
}
