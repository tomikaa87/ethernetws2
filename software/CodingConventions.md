# Project Coding Conventions

## Public/API functions

- The name must be in `PascalCase`

- Prefix the name of function with the name of the component, followed by and underscore character: `Component_`

- Parameter names must be in `camelCase`

- Abbreviations must be written with capital letters: `SRAM`, `EEPROM`, but NOT `Sram` or `Eeprom`

Example:

```c
uint8_t SRAM_ReadByte(uint16_t address);
```

## Private functions

- The name must be in `camelCase`

- Make the function static to make sure it's only accessible in the current compilation unit

- Parameter names must be in `camelCase`

- Abbreviations must be written with capital letters: `SRAM`, `EEPROM`, but NOT `Sram` or `Eeprom`

Example:

```c
static void selectCurrentItem(Menu* menu);
```

## Type aliases, enums, structures, unions

- The name must be in `PascalCase`

- Abbreviations must be written with capital letters: `SRAM`, `EEPROM`

- Don't use the `_t` suffix

- Don't use the `T` prefix

## Named constants

- The name must be in `PascalCase`

- Must be qualified with `static` and `const`

Example:

```c
static const MaximumWidth = 128;
```

## Macros

- The name must be in all capitalized `SNAKE_CASE`

## Global variables

- The name must be in `camelCase`

- Must be prefixed with `g_`

- Abbreviations must be written with capital letters: `SRAM`, `EEPROM`