/**
 * Ethernet Weather Station 2 Firmware
 * Simple Network Time Protocol module
 *
 * @author Tamas Karpati <tomikaa87>
 * @date 2012. november 6.
 */

#ifndef NTP_H
#define NTP_H

#include <stdint.h>
#include <time.h>

extern volatile time_t g_UTCTime;

typedef enum {
    NTP_UPDATE_SUCCESSFUL,
    NTP_UPDATE_FAILED
} NTPUpdateResult;

NTPUpdateResult NTP_Task();

#define NTPGetTime()            g_UTCTime

#endif  /* NTP_H */

