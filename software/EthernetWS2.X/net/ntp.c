/**
 * Ethernet Weather Station 2 Firmware
 * Simple Network Time Protocol module
 *
 * @author Tamas Karpati <tomikaa87>
 * @date 2012. november 6.
 */

#include "ntp.h"
#include "TCPIP.h"

//#include <stdio.h>

//------------------------------------------------------------------------------
// Globals
//------------------------------------------------------------------------------

volatile time_t g_UTCTime = 0;

//------------------------------------------------------------------------------
// API functions
//------------------------------------------------------------------------------


NTPUpdateResult NTP_Task()
{
    DWORD utc = SNTPGetUTCSeconds();
    if (utc > 1388534400ul /*2014-01-01 00:00:00*/) {
        TMR1IE = 0;
        g_UTCTime = utc;
        TMR1H = 0x80;
        TMR1IE = 1;

        return NTP_UPDATE_SUCCESSFUL;
    }
    return NTP_UPDATE_FAILED;
}