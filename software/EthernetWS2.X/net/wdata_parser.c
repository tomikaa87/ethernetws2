/**
 * Ethernet Weather Station 2 Firmware
 * Weather Data Parser module
 *
 * @author Tamas Karpati <tomikaa87>
 * @date 2013-03-29
 */

#include "wdata.h"
#include "wdata_parser.h"
#include "driver/sram_23k.h"
#include "types.h"
#include "hardware_profile.h"

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

typedef enum {
    XML_INIT,

    XML_LD_CITYLIST,
    XML_LD_LOCATION,

    XML_WD_UNITS,
    XML_WD_LOCAL,

    XML_WD_CURRENT,

    XML_WD_FORE,
    XML_WD_FORE_DAY,
    XML_WD_FORE_DAY_DAYTIME,
    XML_WD_FORE_DAY_DAYTIME_TEXT,
    XML_WD_FORE_DAY_NIGHTTIME,
    XML_WD_FORE_DAY_NIGHTTIME_TEXT
} State;
static State g_state = XML_INIT;

typedef struct {
    int start;
    int end;
} Range;
Range g_lastArg;

uint8_t g_dayNumber = 0;


const char *stateToString(State s)
{
    switch (s) {
    case XML_INIT:
        return "XML_INIT";
    case XML_LD_CITYLIST:
        return "XML_LD_CITYLIST";
    case XML_LD_LOCATION:
        return "XML_LD_LOCATION";
    case XML_WD_UNITS:
        return "XML_WD_UNITS";
    case XML_WD_LOCAL:
        return "XML_WD_LOCAL";
    case XML_WD_CURRENT:
        return "XML_WD_CURRENT";
    case XML_WD_FORE:
        return "XML_WD_FORE";
    case XML_WD_FORE_DAY:
        return "XML_WD_FORE_DAY";
    case XML_WD_FORE_DAY_DAYTIME:
        return "XML_WD_FORE_DAY_DAYTIME";
    case XML_WD_FORE_DAY_DAYTIME_TEXT:
        return "XML_WD_FORE_DAY_DAYTIME_TEXT";
    case XML_WD_FORE_DAY_NIGHTTIME:
        return "XML_WD_FORE_DAY_NIGHTTIME";
    case XML_WD_FORE_DAY_NIGHTTIME_TEXT:
        return "XML_WD_FORE_DAY_NIGHTTIME_TEXT";
    }

    return "UNKNOWN";
}


/**
 * Converts a float number string to an integer by removing the radix point.
 * @param s String representation of the float number.
 * @return Integer representation of the float number.
 */
int floatStringToInt(const char *s)
{
    uint8_t i;
    static char num[7];

    i = 0;
    while (*s && i < sizeof (num) - 1) {
        if (*s != '.') {
            num[i++] = *s;
        }
        s++;
    }
    num[i] = 0;

    return atoi(num);
}


/**
 * Process an XML tag's name
 * @param start Start address of the name string
 * @param end End address of the name string
 */
void processTagName(uint16_t start, uint16_t end)
{
    uint16_t i;
    uint8_t j;
    static char buf[6];

    memset(buf, 0, sizeof (buf));
    for (i = start, j = 0; i <= end && j < sizeof (buf) - 1; i++, j++) {
        buf[j] = (char) SRAM_ReadByte(i);
    }

    // Do state transitions based on tags
    switch (g_state) {
    case XML_INIT:
        // location: <citylist>
        if (strncmp(buf, "cit", 3) == 0) {
            g_state = XML_LD_CITYLIST;
        }// weather: <units>
        else if (strncmp(buf, "uni", 3) == 0) {
            g_state = XML_WD_UNITS;
        }// weather: <local>
        else if (strncmp(buf, "loc", 3) == 0) {
            g_state = XML_WD_LOCAL;
        }// weather: <current>
        else if (strncmp(buf, "cur", 3) == 0) {
            g_state = XML_WD_CURRENT;
            g_dayNumber = 0;
        }// weather: <forecast>
        else if (strncmp(buf, "for", 3) == 0) {
            g_state = XML_WD_FORE;
        }
        break;

    case XML_LD_CITYLIST:
        // location: <location>
        if (strncmp(buf, "loc", 3) == 0) {
            g_state = XML_LD_LOCATION;
        }// location: </citylist>
        else if (strncmp(buf, "/ci", 3) == 0) {
            g_state = XML_INIT;
        }
        break;

    case XML_LD_LOCATION:
        // location: </citylist>
        if (strncmp(buf, "/ci", 3) == 0) {
            g_state = XML_INIT;
        }
        break;

    case XML_WD_UNITS:
        // weather: <local>
        if (strncmp(buf, "loc", 3) == 0) {
            g_state = XML_WD_LOCAL;
            break;
        }
        g_state = XML_INIT;
        break;

    case XML_WD_LOCAL:
        // weather: <units>
        if (strncmp(buf, "uni", 3) == 0) {
            g_state = XML_WD_UNITS;
            break;
        }
        g_state = XML_INIT;
        break;

    case XML_WD_CURRENT:
        // current: </currentconditions>
        if (strncmp(buf, "/cu", 3) == 0) {
            g_state = XML_INIT;
        }
        break;

    case XML_WD_FORE:
        // forecast: <day>
        if (strncmp(buf, "day", 3) == 0) {
            g_state = XML_WD_FORE_DAY;
        }// forecast: </forecast>
        else if (strncmp(buf, "/fo", 3) == 0) {
            g_state = XML_INIT;
        }
        break;

    case XML_WD_FORE_DAY:
        // forecast: <daytime>
        if (strncmp(buf, "dayt", 4) == 0) {
            g_state = XML_WD_FORE_DAY_DAYTIME;
        }// forecast: <nighttime>
        else if (strncmp(buf, "nig", 3) == 0) {
            g_state = XML_WD_FORE_DAY_NIGHTTIME;
        }// forecast: </day>
        else if (strncmp(buf, "/da", 3) == 0) {
            g_state = XML_WD_FORE;
        }
        break;

    case XML_WD_FORE_DAY_DAYTIME:
        // daytime: <txtshort>
        if (strncmp(buf, "txts", 4) == 0) {
            g_state = XML_WD_FORE_DAY_DAYTIME_TEXT;
        }// daytime: </daytime>
        else if (strncmp(buf, "/dayt", 5) == 0) {
            g_state = XML_WD_FORE_DAY;
        }
        break;

    case XML_WD_FORE_DAY_DAYTIME_TEXT:
        // daytime: </txtshort>
        if (strncmp(buf, "/tx", 3) == 0) {
            g_state = XML_WD_FORE_DAY_DAYTIME;
        }
        break;

    case XML_WD_FORE_DAY_NIGHTTIME:
        // nighttime: <txtshort>
        if (strncmp(buf, "txts", 4) == 0) {
            g_state = XML_WD_FORE_DAY_NIGHTTIME_TEXT;
        }// nihgttime: </daytime>
        else if (strncmp(buf, "/ni", 3) == 0) {
            g_state = XML_WD_FORE_DAY;
        }
        break;

    case XML_WD_FORE_DAY_NIGHTTIME_TEXT:
        // nighttime: </txtshort>
        if (strncmp(buf, "/tx", 3) == 0) {
            g_state = XML_WD_FORE_DAY_NIGHTTIME;
        }
        break;
    }
}


/**
 * Process the value of an XML attribute.
 * @param start Start address of the value string.
 * @param end End address of the value string.
 */
void processTagValue(uint16_t start, uint16_t end)
{
    uint16_t i;
    uint8_t j;
    static char arg[5], val[50];
    TWDataNumber wd;

    // Load value
    memset(val, 0, sizeof (val));
    for (i = start, j = 0; i <= end && j < sizeof (val) - 1; i++, j++) {
        val[j] = (char) SRAM_ReadByte(i);
    }

    // Load argument
    memset(arg, 0, sizeof (arg));
    for (i = g_lastArg.start, j = 0; i <= g_lastArg.end && j < sizeof (arg) - 1; i++, j++) {
        arg[j] = SRAM_ReadByte(i);
    }

    switch (g_state) {
    case XML_LD_LOCATION:
        // location
        if (strncmp(arg, "loc", 3) == 0) {
            printf("location: ");
            for (i = start; i <= end; i++) {
                putch(SRAM_ReadByte(i));
            }
            printf("\r\n");
        }// city
        else if (strncmp(arg, "cit", 3) == 0) {
            printf("city: ");
            for (i = start; i <= end; i++) {
                putch(SRAM_ReadByte(i));
            }
            printf("\r\n");
        }// state
        else if (strncmp(arg, "sta", 3) == 0) {
            printf("state: ");
            for (i = start; i <= end; i++) {
                putch(SRAM_ReadByte(i));
            }
            printf("\r\n");
        }
        break;

    case XML_WD_LOCAL:
        // gmtdiff
        if (strncmp(arg, "gmt", 3) == 0) {
            printf("local gmtdiff: %s\r\n", val);
            wd.Int8 = atoi(val);
            WData_SetNumber(wd, WD_LOCAL_GMTDIFF, 0);
        }
        break;

    case XML_WD_CURRENT:
        // precipitation
        if (strncmp(arg, "prec", 4) == 0) {
            printf("curr prec: %s\r\n", val);
            // cm -> mm
            wd.UInt16 = (uint16_t)(atof(val) * 10);
            WData_SetNumber(wd, WD_C_PRECIP, 0);
        }// pressure
        else if (strncmp(arg, "pres", 4) == 0) {
            printf("curr pres: %s\r\n", val);
            wd.UInt16 = atoi(val);
            WData_SetNumber(wd, WD_C_PRESSURE, 0);
        }// temperature
        else if (strncmp(arg, "tem", 3) == 0) {
            printf("curr temp: %s\r\n", val);
            wd.Int8 = atoi(val);
            WData_SetNumber(wd, WD_C_TEMPERATURE, 0);
        }// uv index
        else if (strncmp(arg, "uvi", 3) == 0) {
            printf("curr uvi: %s\r\n", val);
            wd.UInt8 = atoi(val);
            WData_SetNumber(wd, WD_C_UV, 0);
        }// visibility
        else if (strncmp(arg, "vis", 3) == 0) {
            printf("curr vis: %s\r\n", val);
            wd.UInt8 = atoi(val);
            WData_SetNumber(wd, WD_C_VISIBILITY, 0);
        }// wind dir
        else if (strncmp(arg, "wdi", 3) == 0) {
            printf("curr wdir: %s\r\n", val);
            WData_SetString(WD_C_WINDDIR, val, 0);
        }// wind speed
        else if (strncmp(arg, "wsp", 3) == 0) {
            printf("curr wspd: %s\r\n", val);
            wd.UInt16 = atoi(val) * 10;
            WData_SetNumber(wd, WD_C_WINDSPEED, 0);
        }// wind gusts
        else if (strncmp(arg, "wgu", 3) == 0) {
            printf("curr wgus: %s\r\n", val);
            wd.UInt16 = atoi(val) * 10;
            WData_SetNumber(wd, WD_C_WINDGUSTS, 0);
        }// icon
        else if (strncmp(arg, "ico", 3) == 0) {
            printf("curr icon: %s\r\n", val);
            wd.UInt8 = atoi(val);
            WData_SetNumber(wd, WD_C_ICON, 0);
        }// humidity
        else if (strncmp(arg, "hum", 3) == 0) {
            printf("curr hum: %s\r\n", val);
            wd.UInt8 = atoi(val);
            WData_SetNumber(wd, WD_C_HUMIDITY, 0);
        }// realfeel
        else if (strncmp(arg, "rft", 3) == 0) {
            printf("curr rft: %s\r\n", val);
            wd.Int8 = atoi(val);
            WData_SetNumber(wd, WD_C_REALFEEL, 0);
        }// pressure tendency
        else if (strncmp(arg, "pte", 3) == 0) {
            printf("curr ptend: %s\r\n", val);
            uint8_t ptend;
            switch (val[0]) {
            case 'D':   // Decreasing
                ptend = -1;
                break;
            case 'S':   // Steady
                ptend = 0;
                break;
            case 'I':   // Increasing
                ptend = 1;
                break;
            default:
                ptend = 127;
            }
            wd.Int8 = ptend;
            WData_SetNumber(wd, WD_C_PRESSURE_TEND, 0);
        } else if (strncmp(arg, "tex", 3) == 0) {
            printf("curr text: %s\r\n", val);
            WData_SetString(WD_C_TEXT, val, 0);
        }
        break;

    case XML_WD_FORE_DAY:
        // day number
        if (strncmp(arg, "num", 3) == 0) {
            printf("fore day: %s\r\n", val);
            g_dayNumber = atoi(val);
        }// date
        else if (strncmp(arg, "dat", 3) == 0) {
            printf("fore day date: %s\r\n", val);
            WData_SetString(WD_F_DATE, val, g_dayNumber);
        }// weekday
        else if (strncmp(arg, "wda", 3) == 0) {
            printf("fore day: %s\r\n", val);
            WData_SetString(WD_F_WEEKDAY, val, g_dayNumber);
        }
        break;

    case XML_WD_FORE_DAY_DAYTIME:
        // precipitation
        if (strncmp(arg, "pre", 3) == 0) {
            printf("fd prec: %s\r\n", val);
            // cm -> mm
            wd.UInt16 = (uint16_t)(atof(val) * 10);
            WData_SetNumber(wd, WD_FD_PRECIP, g_dayNumber);
        }
        // snow
        if (strncmp(arg, "sno", 3) == 0) {
            printf("fd snow: %s\r\n", val);
            // cm -> mm
            wd.UInt16 = (uint16_t)(atof(val) * 10);
            WData_SetNumber(wd, WD_FD_SNOW, g_dayNumber);
        }
        // rain
        if (strncmp(arg, "rai", 3) == 0) {
            printf("fd rain: %s\r\n", val);
            // cm -> mm
            wd.UInt16 = (uint16_t)(atof(val) * 10);
            WData_SetNumber(wd, WD_FD_RAIN, g_dayNumber);
        }
        // max uv
        if (strncmp(arg, "mxu", 3) == 0) {
            printf("fd max uv: %s\r\n", val);
            wd.UInt8 = atoi(val);
            WData_SetNumber(wd, WD_FD_MAXUV, g_dayNumber);
        }// high temperature
        else if (strncmp(arg, "htm", 3) == 0) {
            printf("fd h temp: %s\r\n", val);
            wd.Int8 = atoi(val);
            WData_SetNumber(wd, WD_FD_HI_TEMP, g_dayNumber);
        }// low temperature
        else if (strncmp(arg, "ltm", 3) == 0) {
            printf("fd l temp: %s\r\n", val);
            wd.Int8 = atoi(val);
            WData_SetNumber(wd, WD_FD_LO_TEMP, g_dayNumber);
        }// thunderstorm
        else if (strncmp(arg, "tst", 3) == 0) {
            printf("fd tstrm: %s\r\n", val);
            wd.UInt8 = atoi(val);
            WData_SetNumber(wd, WD_FD_TSTORM, g_dayNumber);
        }// wind dir
        else if (strncmp(arg, "wdi", 3) == 0) {
            printf("fd wdir: %s\r\n", val);
            WData_SetString(WD_FD_WINDDIR, val, g_dayNumber);
        }// wind speed
        else if (strncmp(arg, "wsp", 3) == 0) {
            printf("fd wspd: %s\r\n", val);
            wd.UInt16 = atoi(val) * 10;
            WData_SetNumber(wd, WD_FD_WINDSPEED, g_dayNumber);
        }// wind gusts
        else if (strncmp(arg, "wgu", 3) == 0) {
            printf("fd wgus: %s\r\n", val);
            wd.UInt16 = atoi(val) * 10;
            WData_SetNumber(wd, WD_FD_WINDGUSTS, g_dayNumber);
        }// icon
        else if (strncmp(arg, "ico", 3) == 0) {
            printf("fd icon: %s\r\n", val);
            wd.UInt8 = atoi(val);
            WData_SetNumber(wd, WD_FD_ICON, g_dayNumber);
        }// realfeel low
        else if (strncmp(arg, "rfl", 3) == 0) {
            printf("fd rfl: %s\r\n", val);
            wd.Int8 = atoi(val);
            WData_SetNumber(wd, WD_FD_REALFEEL_LO, g_dayNumber);
        }// realfeel high
        else if (strncmp(arg, "rfh", 3) == 0) {
            printf("fd rfh: %s\r\n", val);
            wd.Int8 = atoi(val);
            WData_SetNumber(wd, WD_FD_REALFEEL_HI, g_dayNumber);
        }
        break;

    case XML_WD_FORE_DAY_NIGHTTIME:
        // precipitation
        if (strncmp(arg, "pre", 3) == 0) {
            printf("fn prec: %s\r\n", val);
            // cm -> mm
            wd.UInt16 = (uint16_t)(atof(val) * 10);
            WData_SetNumber(wd, WD_FN_PRECIP, g_dayNumber);
        }
        // snow
        if (strncmp(arg, "sno", 3) == 0) {
            printf("fn snow: %s\r\n", val);
            // cm -> mm
            wd.UInt16 = (uint16_t)(atof(val) * 10);
            WData_SetNumber(wd, WD_FN_SNOW, g_dayNumber);
        }
        // rain
        if (strncmp(arg, "rai", 3) == 0) {
            printf("fn rain: %s\r\n", val);
            // cm -> mm
            wd.UInt16 = (uint16_t)(atof(val) * 10);
            WData_SetNumber(wd, WD_FN_RAIN, g_dayNumber);
        }// high temperature
        else if (strncmp(arg, "htm", 3) == 0) {
            printf("fn h temp: %s\r\n", val);
            wd.Int8 = atoi(val);
            WData_SetNumber(wd, WD_FN_HI_TEMP, g_dayNumber);
        }// low temperature
        else if (strncmp(arg, "ltm", 3) == 0) {
            printf("fn l temp: %s\r\n", val);
            wd.Int8 = atoi(val);
            WData_SetNumber(wd, WD_FN_LO_TEMP, g_dayNumber);
        }// thunderstorm
        else if (strncmp(arg, "tst", 3) == 0) {
            printf("fn tstrm: %s\r\n", val);
            wd.UInt8 = atoi(val);
            WData_SetNumber(wd, WD_FN_TSTORM, g_dayNumber);
        }// wind dir
        else if (strncmp(arg, "wdi", 3) == 0) {
            printf("fn wdir: %s\r\n", val);
            WData_SetString(WD_FN_WINDDIR, val, g_dayNumber);
        }// wind speed
        else if (strncmp(arg, "wsp", 3) == 0) {
            printf("fn wspd: %s\r\n", val);
            wd.UInt16 = atoi(val) * 10;
            WData_SetNumber(wd, WD_FN_WINDSPEED, g_dayNumber);
        }// wind gusts
        else if (strncmp(arg, "wgu", 3) == 0) {
            printf("fn wgus: %s\r\n", val);
            wd.UInt16 = atoi(val) * 10;
            WData_SetNumber(wd, WD_FN_WINDGUSTS, g_dayNumber);
        }// icon
        else if (strncmp(arg, "ico", 3) == 0) {
            printf("fn icon: %s\r\n", val);
            wd.UInt8 = atoi(val);
            WData_SetNumber(wd, WD_FN_ICON, g_dayNumber);
        }// realfeel low
        else if (strncmp(arg, "rfl", 3) == 0) {
            printf("fn rfl: %s\r\n", val);
            wd.Int8 = atoi(val);
            WData_SetNumber(wd, WD_FN_REALFEEL_LO, g_dayNumber);
        }// realfeel high
        else if (strncmp(arg, "rfh", 3) == 0) {
            printf("fn rfh: %s\r\n", val);
            wd.Int8 = atoi(val);
            WData_SetNumber(wd, WD_FN_REALFEEL_HI, g_dayNumber);
        }
        break;

    default:
        break;
    }
}


/**
 * Process text found between an opening and closing tag.
 * @param start Start address of the text string.
 * @param end End address of the text string.
 */
void processContent(uint16_t start, uint16_t end)
{
    uint16_t i;
    uint8_t j;
    static char txt[50];

    memset(txt, 0, sizeof (txt));
    for (i = start, j = 0; i <= end && j < sizeof (txt) - 1; i++, j++) {
        txt[j] = SRAM_ReadByte(i);
    }

    switch (g_state) {
    case XML_WD_FORE_DAY_DAYTIME_TEXT:
        printf("fore day text: %s\r\n", txt);
        WData_SetString(WD_FD_TEXT, txt, g_dayNumber);
        break;

    case XML_WD_FORE_DAY_NIGHTTIME_TEXT:
        printf("fore night text: %s\r\n", txt);
        WData_SetString(WD_FN_TEXT, txt, g_dayNumber);
        break;

    default:
        break;
    }
}


/**
 * Parses the XML data stored in the external SRAM.
 * @param data_len Length of the XML data.
 */
void WDParser_ParseBufferedData(uint16_t length)
{
    uint16_t i, start, end;

    enum {
        S_INIT,
        S_TAG_OPEN,
        S_TAG_NAME,
        S_TAG_ARG_NAME,
        S_TAG_ARG_VALUE,
        S_TAG_CLOSE,
        S_TAG_TEXT
    } state = S_INIT;

    for (i = RAM_LOC_WDATA_XML_BASE; i < RAM_LOC_WDATA_XML_BASE + length; i++) {
        char c = (char) SRAM_ReadByte(i);

        switch (state) {
        case S_INIT:
            if (c != '<') {
                break;
            }
            state = S_TAG_OPEN;
            break;

        case S_TAG_OPEN:
            start = i;
            state = S_TAG_NAME;
            break;

        case S_TAG_NAME:
            if (c == ' ' || c == '\t') {
                processTagName(start, end);
                state = S_TAG_ARG_NAME;
                start = -1;
                break;
            } else if (c == '>') {
                processTagName(start, end);
                start = -1;
                state = S_TAG_CLOSE;
                break;
            }
            end = i;
            break;

        case S_TAG_ARG_NAME:
            if (start == -1) {
                // Skip leading characters
                if (c == ' ' || c == '\t') {
                    break;
                }
                // End of tag found
                if (c == '/' || c == '>' || c == '?') {
                    state = S_TAG_CLOSE;
                    break;
                }
                start = i;
            } else {
                // Argument value found
                if (c == '=') {
                    g_lastArg.start = start;
                    g_lastArg.end = end;
                    start = -1;
                    state = S_TAG_ARG_VALUE;
                    break;
                } else if (c == ' ') {
                    g_lastArg.start = start;
                    g_lastArg.end = end;
                    start = -1;
                    break;
                }
            }
            end = i;
            break;

        case S_TAG_ARG_VALUE:
            if (start == -1) {
                // Skip first quote mark
                if (c == '"') {
                    break;
                }
                start = i;
            } else {
                // End of value
                if (c == '"') {
                    processTagValue(start, end);
                    start = -1;
                    state = S_TAG_ARG_NAME;
                }
            }
            end = i;
            break;

        case S_TAG_CLOSE:
            if (c == ' ' || c == '\t' || c == '>') {
                break;
            }
            if (c == '<') {
                state = S_TAG_OPEN;
                break;
            }
            state = S_TAG_TEXT;
            start = i;
            break;

        case S_TAG_TEXT:
            if (c == '<') {
                state = S_TAG_OPEN;
                processContent(start, end);
                start = -1;
                break;
            }
            end = i;
            break;
        }
    }
}


void WDParser_Reset()
{
    g_state = XML_INIT;
    g_dayNumber = 0;
}
