//
// Ethernet Weather Station 2
// Weather Data module
//
// Author: Tamas Karpati
// This file was created on 2012-11-06
//

#ifndef WDATA_H
#define WDATA_H

#include "hardware_profile.h"

#include <stdint.h>

extern bit g_hasWeatherData;

// Configuration check
#ifndef RAM_LOC_WDATA_BASE
#error Weather data RAM base address (RAM_LOC_WDATA_BASE) undefined
#endif

#define WD_NUMBER_SIZE          sizeof (float)

typedef union {
    uint8_t UInt8;
    int8_t Int8;
    uint16_t UInt16;
    int16_t Int16;
    float Float;

    uint8_t Data[WD_NUMBER_SIZE];
} TWDataNumber;

#define WD_DAYS_MAX             5
#define WD_WDIR_MAX_LEN         4
#define WD_TEXT_MAX_LEN         51
#define WD_DATE_LEN             11
#define WD_WEEKDAY_LEN          11

#define AddNumVariantSizeTo     sizeof (TWDataNumber) +
#define AddStrVariantSizeTo     1 +

#define AndAddWindDirSize       + WD_WDIR_MAX_LEN
#define AndAddTextSize          + WD_TEXT_MAX_LEN
#define AndAddDateSize          + WD_DATE_LEN
#define AndAddWeekdaySize       + WD_WEEKDAY_LEN

// Local data (time zone etc.)
#define WD_LOCAL_START          RAM_LOC_WDATA_BASE
#define WD_LOCAL_GMTDIFF        WD_LOCAL_START
#define WD_LOCAL_END            (AddNumVariantSizeTo WD_LOCAL_GMTDIFF)
#define WD_LOCAL_SIZE           (WD_LOCAL_END - WD_LOCAL_START)

// Current conditions
#define WD_C_START              WD_LOCAL_END
#define WD_C_PRESSURE           WD_C_START
#define WD_C_PRESSURE_TEND      (AddNumVariantSizeTo WD_C_PRESSURE)
#define WD_C_TEMPERATURE        (AddNumVariantSizeTo WD_C_PRESSURE_TEND)
#define WD_C_REALFEEL           (AddNumVariantSizeTo WD_C_TEMPERATURE)
#define WD_C_HUMIDITY           (AddNumVariantSizeTo WD_C_REALFEEL)
#define WD_C_ICON               (AddNumVariantSizeTo WD_C_HUMIDITY)
#define WD_C_WINDGUSTS          (AddNumVariantSizeTo WD_C_ICON)
#define WD_C_WINDSPEED          (AddNumVariantSizeTo WD_C_WINDGUSTS)
#define WD_C_WINDDIR            (AddNumVariantSizeTo WD_C_WINDSPEED)
#define WD_C_VISIBILITY         (AddStrVariantSizeTo WD_C_WINDDIR AndAddWindDirSize)
#define WD_C_PRECIP             (AddNumVariantSizeTo WD_C_VISIBILITY)
#define WD_C_UV                 (AddNumVariantSizeTo WD_C_PRECIP)
#define WD_C_TEXT               (AddNumVariantSizeTo WD_C_UV)
#define WD_C_END                (AddStrVariantSizeTo WD_C_TEXT AndAddTextSize)
#define WD_C_SIZE               (WD_C_END - WD_C_START)

// Forecast day
#define WD_F_START              WD_C_END
#define WD_F_DATE               WD_F_START
#define WD_F_WEEKDAY            (AddStrVariantSizeTo WD_F_DATE AndAddDateSize)
#define WD_FD_ICON              (AddStrVariantSizeTo WD_F_WEEKDAY AndAddWeekdaySize)
#define WD_FD_HI_TEMP           (AddNumVariantSizeTo WD_FD_ICON)
#define WD_FD_LO_TEMP           (AddNumVariantSizeTo WD_FD_HI_TEMP)
#define WD_FD_REALFEEL_HI       (AddNumVariantSizeTo WD_FD_LO_TEMP)
#define WD_FD_REALFEEL_LO       (AddNumVariantSizeTo WD_FD_REALFEEL_HI)
#define WD_FD_WINDSPEED         (AddNumVariantSizeTo WD_FD_REALFEEL_LO)
#define WD_FD_WINDGUSTS         (AddNumVariantSizeTo WD_FD_WINDSPEED)
#define WD_FD_WINDDIR           (AddNumVariantSizeTo WD_FD_WINDGUSTS)
#define WD_FD_MAXUV             (AddStrVariantSizeTo WD_FD_WINDDIR AndAddWindDirSize)
#define WD_FD_RAIN              (AddNumVariantSizeTo WD_FD_MAXUV)
#define WD_FD_SNOW              (AddNumVariantSizeTo WD_FD_RAIN)
#define WD_FD_PRECIP            (AddNumVariantSizeTo WD_FD_SNOW)
#define WD_FD_TSTORM            (AddNumVariantSizeTo WD_FD_PRECIP)
#define WD_FD_TEXT              (AddNumVariantSizeTo WD_FD_TSTORM)
#define WD_FN_ICON              (AddStrVariantSizeTo WD_FD_TEXT AndAddTextSize)
#define WD_FN_HI_TEMP           (AddNumVariantSizeTo WD_FN_ICON)
#define WD_FN_LO_TEMP           (AddNumVariantSizeTo WD_FN_HI_TEMP)
#define WD_FN_REALFEEL_HI       (AddNumVariantSizeTo WD_FN_LO_TEMP)
#define WD_FN_REALFEEL_LO       (AddNumVariantSizeTo WD_FN_REALFEEL_HI)
#define WD_FN_WINDSPEED         (AddNumVariantSizeTo WD_FN_REALFEEL_LO)
#define WD_FN_WINDGUSTS         (AddNumVariantSizeTo WD_FN_WINDSPEED)
#define WD_FN_WINDDIR           (AddNumVariantSizeTo WD_FN_WINDGUSTS)
#define WD_FN_RAIN              (AddStrVariantSizeTo WD_FN_WINDDIR AndAddWindDirSize)
#define WD_FN_SNOW              (AddNumVariantSizeTo WD_FN_RAIN)
#define WD_FN_PRECIP            (AddNumVariantSizeTo WD_FN_SNOW)
#define WD_FN_TSTORM            (AddNumVariantSizeTo WD_FN_PRECIP)
#define WD_FN_TEXT              (AddNumVariantSizeTo WD_FN_TSTORM)
#define WD_F_END                (AddStrVariantSizeTo WD_FN_TEXT AndAddTextSize)
#define WD_F_SIZE               (WD_F_END - WD_F_START)

#define WD_SIZE                 (WD_LOCAL_SIZE + WD_C_SIZE + 5 * WD_F_SIZE)

TWDataNumber WData_GetNumber(uint16_t address, uint8_t day);
uint8_t WData_GetString(uint16_t address, char *buf, uint8_t bufLen, uint8_t day);

void WData_SetNumber(TWDataNumber number, uint16_t address, uint8_t day);
uint8_t WData_SetString(uint16_t address, char *str, uint8_t day);

#define Clamp(value, min, max)      ((value <= min) ? min : ((value >= max) ? max : value))
#define ClampDefault(value, min, max, def) \
    ((value < min) ? def : ((value > max) ? def : value))

#endif  /* WDATA_H */

