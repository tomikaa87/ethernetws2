/**
 * Ethernet Weather Station 2 Firmware
 * Weather Data Downloader module
 *
 * @author Tamas Karpati <tomikaa87>
 * @date 2012. november 6.
 */

#include "wdata_dloader.h"
#include "wdata_parser.h"
#include "stack/TCPIP.h"
#include "driver/sram_23k.h"
#include "hardware_profile.h"
#include "wdata.h"
#include "config.h"

#include <stdio.h>
//#define //_debug printf

/**
 * Struct for storing context of Weather Data Downloader.
 */
typedef enum {
    S_START,
    S_CONNECT,
    S_SEND_REQ,
    S_RECEIVE_RESP,
    S_CLOSE,
    S_DONE
} State;

static struct {
    State state;
    TCP_SOCKET socket;
    DWORD tick_cnt;
    uint16_t req_len;
    uint16_t recv_bytes;
    uint8_t flags;
    uint8_t success;
} g_context;

//------------------------------------------------------------------------------
// Constants
//------------------------------------------------------------------------------

const BYTE AW_SERVER_HOST[] = "vwidget.accuweather.com";
const WORD AW_SERVER_PORT = 80;

const BYTE AW_WDATA_LOCATION[] = "lid_187423";
#define AW_WDATA_LOCATION_SIZE (sizeof (AW_WDATA_LOCATION))

// First part of GET command
const BYTE AW_WDATA_REQ_GET_PART_1[] =
        "GET /widget/vista2/weather_data_v2.asp?location=";
#define AW_WDATA_REQ_GET_PART_1_SIZE (sizeof (AW_WDATA_REQ_GET_PART_1))

#ifdef ENABLE_NETWORK_CONFIG
// First part of GET command (if proxy enabled)
const BYTE AW_WDATA_REQ_GET_PART_1_PROXY[] =
        "GET http://vwidget.accuweather.com/widget/vista2/weather_data_v2.asp?location=";
#define AW_WDATA_REQ_GET_PART_1_PROXY_SIZE (sizeof (AW_WDATA_REQ_GET_PART_1_PROXY))
#endif

// First part of GET command (location request)
const BYTE AW_LDATA_REQ_GET_PART_1[] =
        "GET /widget/vista1/locate_city.asp?location=";
#define AW_LDATA_REQ_GET_PART_1_SIZE (sizeof (AW_LDATA_REQ_GET_PART_1))

#ifdef ENABLE_NETWORK_CONFIG
// First part of GET command (if proxy enabled, location request)
const BYTE AW_LDATA_REQ_GET_PART_1_PROXY[] =
        "GET http://vwidget.accuweather.com/widget/vista1/locate_city.asp?location=";
#define AW_LDATA_REQ_GET_PART_1_PROXY_SIZE (sizeof (AW_LDATA_REQ_GET_PART_1_PROXY))
#endif

// Second part of GET command
const BYTE AW_REQ_GET_PART_2[] =
        "&metric=1 HTTP/1.1\r\n";
#define AW_REQ_GET_PART_2_SIZE (sizeof (AW_REQ_GET_PART_2))

// Necessary HTTP headers
const BYTE AW_REQ_TAIL[] =
        "Host: vwidget.accuweather.com\r\n" \
        "Accept: text/plain\r\n" \
        "Accept-charset: ISO-8859-2\r\n" \
        "Connection: close\r\n\r\n\0";
#define AW_REQ_TAIL_SIZE (sizeof (AW_REQ_TAIL))

//------------------------------------------------------------------------------
// API functions
//------------------------------------------------------------------------------


void WDDownloader_Reset()
{
    g_context.state = S_DONE;
    g_context.tick_cnt = 0;
    g_context.req_len = 0;
    TCPClose(g_context.socket);
    g_context.socket = INVALID_SOCKET;
    g_context.success = 0;
    g_context.recv_bytes = 0;

    WDParser_Reset();
}

//--------------------------------------------------------------------


void WDDownload_Start(uint8_t flags)
{
    WDDownloader_Reset();
    g_context.state = S_START;
    g_context.flags = flags;
}

//--------------------------------------------------------------------


WDDownloaderResult WDDownloader_Task()
{
    switch (g_context.state) {
        // Initial state
    case S_START:
    {
#ifdef ENABLE_NETWORK_CONFIG
        if (g_context.flags & WDATA_DL_F_USE_PROXY) {
            // Using HTTP proxy
            // Here we connect to the HTTP proxy
            //_debug("[xdl] creating socket (proxy)\r\n");
        } else
#endif
        {
            // Direct connection
            //_debug("[xdl] creating socket\r\n");
            // Create TCP client socket
            g_context.socket = TCPOpen((DWORD) & AW_SERVER_HOST, TCP_OPEN_ROM_HOST,
                                       AW_SERVER_PORT, TCP_PURPOSE_DEFAULT);
        }
        // Verify socket
        if (g_context.socket == INVALID_SOCKET) {
            g_context.state = S_DONE;
            return WDR_ERROR;
        }
        //_debug("[xdl] socket created, connecting\r\n");
        g_context.state = S_CONNECT;
        g_context.tick_cnt = TickGet();

        return WDR_IN_PROGRESS;
    }

        // Connecting to the server
    case S_CONNECT:
    {
        // Check if socket is connected
        if (!TCPIsConnected(g_context.socket)) {
            // Check elapsed time
            if (TickGet() - g_context.tick_cnt >= 15 * TICKS_PER_SECOND) {
                // Connection timed out
                //_debug("[xdl] connection timed out\r\n");
                TCPClose(g_context.socket);
                g_context.socket = INVALID_SOCKET;
                g_context.state = S_DONE;
                return WDR_ERROR;
            }
        } else {
            // Socket connected, prepare for sending
            g_context.tick_cnt = TickGet();
            g_context.state = S_SEND_REQ;
        }

        return WDR_IN_PROGRESS;
    }

        // Send HTTP request
    case S_SEND_REQ:
    {
        // Check if the socket is ready to send
        if (TCPIsPutReady(g_context.socket) < 125) {
            //_debug("[xdl] socket is not ready to send\r\n");
            g_context.state = S_DONE;
            return WDR_ERROR;
        }
        // Send request request
        if (g_context.flags & WDATA_DL_F_LOCATION_MODE) {
#ifdef ENABLE_NETWORK_CONFIG
            // Location data
            if (g_context.flags & WDATA_DL_F_USE_PROXY) {
                TCPPutROMArray(g_context.socket, AW_WDATA_REQ_GET_PART_1_PROXY,
                               AW_LDATA_REQ_GET_PART_1_PROXY_SIZE - 1);
            } else
#endif
            {
                TCPPutROMArray(g_context.socket, AW_WDATA_REQ_GET_PART_1,
                               AW_LDATA_REQ_GET_PART_1_SIZE - 1);
            }
            // TODO location search string
            //                TCPPutROMArray(ctx.socket, AW_WDATA_LOCATION,
            //                        AW_WDATA_LOCATION_SIZE - 1);
            TCPPutROMArray(g_context.socket, AW_REQ_GET_PART_2,
                           AW_REQ_GET_PART_2_SIZE - 1);
            TCPPutROMArray(g_context.socket, AW_REQ_TAIL,
                           AW_REQ_TAIL_SIZE - 1);
        } else {
#ifdef ENABLE_NETWORK_CONFIG
            // Weather data request
            if (g_context.flags & WDATA_DL_F_USE_PROXY) {
                TCPPutROMArray(g_context.socket, AW_WDATA_REQ_GET_PART_1_PROXY,
                               AW_WDATA_REQ_GET_PART_1_PROXY_SIZE - 1);
            } else
#endif
            {
                TCPPutROMArray(g_context.socket, AW_WDATA_REQ_GET_PART_1,
                               AW_WDATA_REQ_GET_PART_1_SIZE - 1);
            }
            TCPPutROMArray(g_context.socket, AW_WDATA_LOCATION,
                           AW_WDATA_LOCATION_SIZE - 1);
            TCPPutROMArray(g_context.socket, AW_REQ_GET_PART_2,
                           AW_REQ_GET_PART_2_SIZE - 1);
            TCPPutROMArray(g_context.socket, AW_REQ_TAIL,
                           AW_REQ_TAIL_SIZE - 1);
        }

        //_debug("[xdl] request sent, waiting for response\r\n");

        //ctx.buf_idx = 0;
        g_context.state = S_RECEIVE_RESP;

        return WDR_IN_PROGRESS;
    }

        // Receive server response
    case S_RECEIVE_RESP:
    {
        unsigned char buf[64];
        unsigned int byteCount;

        // Check if connection is still alive
        if (!TCPIsConnected(g_context.socket)) {
            //_debug("[xdl] connection reset\r\n");
            g_context.state = S_CLOSE;
            //                TCPClose(ctx.socket);
            //                ctx.socket = INVALID_SOCKET;
            //                return WDR_ERROR;
        } else {
            byteCount = TCPIsGetReady(g_context.socket);
            if (byteCount == 0)
                return WDR_IN_PROGRESS;

            //printf("[xdl] receiving %d bytes\r\n", byteCount);
        }

        while (1) {
            byteCount = TCPGetArray(g_context.socket, buf, sizeof (buf));
            SRAM_WriteBuffer(RAM_LOC_WDATA_XML_BASE + g_context.recv_bytes, buf,
                             byteCount);
            g_context.recv_bytes += byteCount;
            if (byteCount < sizeof (buf))
                break;
        }

        return WDR_IN_PROGRESS;
    }

        // Close connection
    case S_CLOSE:
    {
        //            unsigned int i;

        TCPDisconnect(g_context.socket);
        g_context.socket = INVALID_SOCKET;
        g_context.state = S_DONE;

        //            //_debug("[xdl] data:\r\n");
        //            for (i = RAM_LOC_WDATA_XML_BASE;
        //                 i < RAM_LOC_WDATA_XML_BASE + ctx.recv_bytes; i++)
        //            {
        //                putch(ext_sram_read_byte(i));
        //            }

        //_debug("[xdl] download size: %u\r\n", ctx.recv_bytes);

        if (g_context.recv_bytes > 3000) {
            WDParser_ParseBufferedData(g_context.recv_bytes);
            g_context.success = 1;
            g_hasWeatherData = 1;
        }


        return WDR_IN_PROGRESS;
    }

        // Nothing to do
    case S_DONE:
    {
        if (g_context.success)
            return WDR_FINISHED;
        else
            return WDR_ERROR;
    }
    }

    // FIXME check if we can go there
    return WDR_ERROR;
}
