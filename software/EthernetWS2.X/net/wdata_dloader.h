/**
 * Ethernet Weather Station 2 Firmware
 * Weather Data Downloader module
 *
 * @author Tamas Karpati <tomikaa87>
 * @date 2012. november 6.
 */

#ifndef WDATA_DLOADER_H
#define WDATA_DLOADER_H

#include <stdint.h>

//typedef enum
//{
//    XDL_NO_ERROR,
//    XDL_SOCKET_INVALID,
//    XDL_NO_BUFFER_SPACE,
//    XDL_REQUEST_SEND_FAILED,
//    XDL_DOWNLOAD_FAILED,
//    XDL_TIMEOUT
//} wdata_dloader_error_t;

typedef enum {
    WDR_FINISHED = 0,
    WDR_IN_PROGRESS,
    WDR_ERROR
} WDDownloaderResult;

#define WDATA_DL_F_USE_PROXY            1
#define WDATA_DL_F_LOCATION_MODE        2

void WDDownloader_Reset();
void WDDownload_Start(uint8_t flags);
WDDownloaderResult WDDownloader_Task();

//unsigned char wdata_dloader_is_finished();
//wdata_dloader_error_t wdata_dloader_last_error();

#endif  /* WDATA_DLOADER_H */

