/**
 * Ethernet Weather Station 2 Firmware
 * Weather Data Parser module
 *
 * @author Tamas Karpati <tomikaa87>
 * @date 2012. november 6.
 */

#ifndef WDATA_PARSER_H
#define WDATA_PARSER_H

#include <stdint.h>

void WDParser_ParseBufferedData(uint16_t length);
void WDParser_Reset();

#endif  /* WDATA_PARSER_H */

