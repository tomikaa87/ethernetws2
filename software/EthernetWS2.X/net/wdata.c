//
// Ethernet Weather Station 2
// Weather Data module
//
// Author: Tamas Karpati
// This file was created on 2012-11-06
//

#include "wdata.h"
#include "driver/sram_23k.h"

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

//------------------------------------------------------------------------------
// Global variables
//------------------------------------------------------------------------------

bit g_hasWeatherData = 0;

//------------------------------------------------------------------------------
// API functions
//------------------------------------------------------------------------------


TWDataNumber WData_GetNumber(uint16_t address, uint8_t day)
{
    TWDataNumber number = {0, };

    if (day > WD_DAYS_MAX)
        return number;

    if (day > 1)
        address += WD_F_SIZE * (day - 1);

    SRAM_ReadBuffer(address, number.Data, WD_NUMBER_SIZE);

    return number;
}

//--------------------------------------------------------------------


uint8_t WData_GetString(uint16_t address, char *buf, uint8_t bufLen, uint8_t day)
{
    if (day > WD_DAYS_MAX)
        return 0;

    if (day > 1)
        address += WD_F_SIZE * (day - 1);

    uint8_t len = SRAM_ReadByte(address);
    SRAM_ReadBuffer(address + 1, buf, min(bufLen, len));

    return len;
}

//--------------------------------------------------------------------


void WData_SetNumber(TWDataNumber number, uint16_t address, uint8_t day)
{
    if (day > WD_DAYS_MAX)
        return;

    if (day > 1)
        address += WD_F_SIZE * (day - 1);

    SRAM_WriteBuffer(address, number.Data, WD_NUMBER_SIZE);
}

//--------------------------------------------------------------------


uint8_t WData_SetString(uint16_t address, char *str, uint8_t day)
{
    if (day > WD_DAYS_MAX)
        return 0;

    if (day > 1)
        address += WD_F_SIZE * (day - 1);

    uint8_t len = min(strlen(str) + 1, WD_TEXT_MAX_LEN);
    SRAM_WriteByte(address, len);
    SRAM_WriteBuffer(address + 1, (uint8_t *)str, len);

    return len;
}
