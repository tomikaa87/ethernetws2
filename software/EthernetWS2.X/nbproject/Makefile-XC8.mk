#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Include project Makefile
ifeq "${IGNORE_LOCAL}" "TRUE"
# do not include local makefile. User is passing all local related variables already
else
include Makefile
# Include makefile containing local settings
ifeq "$(wildcard nbproject/Makefile-local-XC8.mk)" "nbproject/Makefile-local-XC8.mk"
include nbproject/Makefile-local-XC8.mk
endif
endif

# Environment
MKDIR=gnumkdir -p
RM=rm -f 
MV=mv 
CP=cp 

# Macros
CND_CONF=XC8
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
IMAGE_TYPE=debug
OUTPUT_SUFFIX=elf
DEBUGGABLE_SUFFIX=elf
FINAL_IMAGE=dist/${CND_CONF}/${IMAGE_TYPE}/EthernetWS2.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}
else
IMAGE_TYPE=production
OUTPUT_SUFFIX=hex
DEBUGGABLE_SUFFIX=elf
FINAL_IMAGE=dist/${CND_CONF}/${IMAGE_TYPE}/EthernetWS2.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}
endif

ifeq ($(COMPARE_BUILD), true)
COMPARISON_BUILD=--mafrlcsj
else
COMPARISON_BUILD=
endif

ifdef SUB_IMAGE_ADDRESS

else
SUB_IMAGE_ADDRESS_COMMAND=
endif

# Object Directory
OBJECTDIR=build/${CND_CONF}/${IMAGE_TYPE}

# Distribution Directory
DISTDIR=dist/${CND_CONF}/${IMAGE_TYPE}

# Source Files Quoted if spaced
SOURCEFILES_QUOTED_IF_SPACED=driver/eeprom_25lc.c driver/graphlcd.c driver/sram_23k.c driver/t6963c.c lcdui/ForecastScreen.c lcdui/image.c lcdui/lcdui.c lcdui/text.c lcdui/MainScreen.c lcdui/DisplayController.c lcdui/Menu.c lcdui/MenuScreen.c lcdui/DiagScreen.c ../../software/lib/crc16.c lib/TendencyData.c lib/WeatherDataCollector.c net/ntp.c net/wdata_dloader.c net/wdata_parser.c net/wdata.c sys/interrupt.c sys/keypad.c sys/pic.c sys/sensor.c sys/serial.c sys/settings.c sys/scheduler.c sys/tick.c main.c Utils.c stack/ARP.c stack/BigInt.c stack/Delay.c stack/DHCP.c stack/DNS.c stack/ENC28J60.c stack/Hashes.c stack/Helpers.c stack/ICMP.c stack/IP.c stack/SNTP.c stack/SPIEEPROM.c stack/SPIFlash.c stack/SPIRAM.c stack/StackTsk.c stack/TCP.c stack/Telnet.c stack/Tick.c stack/UDP.c

# Object Files Quoted if spaced
OBJECTFILES_QUOTED_IF_SPACED=${OBJECTDIR}/driver/eeprom_25lc.p1 ${OBJECTDIR}/driver/graphlcd.p1 ${OBJECTDIR}/driver/sram_23k.p1 ${OBJECTDIR}/driver/t6963c.p1 ${OBJECTDIR}/lcdui/ForecastScreen.p1 ${OBJECTDIR}/lcdui/image.p1 ${OBJECTDIR}/lcdui/lcdui.p1 ${OBJECTDIR}/lcdui/text.p1 ${OBJECTDIR}/lcdui/MainScreen.p1 ${OBJECTDIR}/lcdui/DisplayController.p1 ${OBJECTDIR}/lcdui/Menu.p1 ${OBJECTDIR}/lcdui/MenuScreen.p1 ${OBJECTDIR}/lcdui/DiagScreen.p1 ${OBJECTDIR}/_ext/1697446845/crc16.p1 ${OBJECTDIR}/lib/TendencyData.p1 ${OBJECTDIR}/lib/WeatherDataCollector.p1 ${OBJECTDIR}/net/ntp.p1 ${OBJECTDIR}/net/wdata_dloader.p1 ${OBJECTDIR}/net/wdata_parser.p1 ${OBJECTDIR}/net/wdata.p1 ${OBJECTDIR}/sys/interrupt.p1 ${OBJECTDIR}/sys/keypad.p1 ${OBJECTDIR}/sys/pic.p1 ${OBJECTDIR}/sys/sensor.p1 ${OBJECTDIR}/sys/serial.p1 ${OBJECTDIR}/sys/settings.p1 ${OBJECTDIR}/sys/scheduler.p1 ${OBJECTDIR}/sys/tick.p1 ${OBJECTDIR}/main.p1 ${OBJECTDIR}/Utils.p1 ${OBJECTDIR}/stack/ARP.p1 ${OBJECTDIR}/stack/BigInt.p1 ${OBJECTDIR}/stack/Delay.p1 ${OBJECTDIR}/stack/DHCP.p1 ${OBJECTDIR}/stack/DNS.p1 ${OBJECTDIR}/stack/ENC28J60.p1 ${OBJECTDIR}/stack/Hashes.p1 ${OBJECTDIR}/stack/Helpers.p1 ${OBJECTDIR}/stack/ICMP.p1 ${OBJECTDIR}/stack/IP.p1 ${OBJECTDIR}/stack/SNTP.p1 ${OBJECTDIR}/stack/SPIEEPROM.p1 ${OBJECTDIR}/stack/SPIFlash.p1 ${OBJECTDIR}/stack/SPIRAM.p1 ${OBJECTDIR}/stack/StackTsk.p1 ${OBJECTDIR}/stack/TCP.p1 ${OBJECTDIR}/stack/Telnet.p1 ${OBJECTDIR}/stack/Tick.p1 ${OBJECTDIR}/stack/UDP.p1
POSSIBLE_DEPFILES=${OBJECTDIR}/driver/eeprom_25lc.p1.d ${OBJECTDIR}/driver/graphlcd.p1.d ${OBJECTDIR}/driver/sram_23k.p1.d ${OBJECTDIR}/driver/t6963c.p1.d ${OBJECTDIR}/lcdui/ForecastScreen.p1.d ${OBJECTDIR}/lcdui/image.p1.d ${OBJECTDIR}/lcdui/lcdui.p1.d ${OBJECTDIR}/lcdui/text.p1.d ${OBJECTDIR}/lcdui/MainScreen.p1.d ${OBJECTDIR}/lcdui/DisplayController.p1.d ${OBJECTDIR}/lcdui/Menu.p1.d ${OBJECTDIR}/lcdui/MenuScreen.p1.d ${OBJECTDIR}/lcdui/DiagScreen.p1.d ${OBJECTDIR}/_ext/1697446845/crc16.p1.d ${OBJECTDIR}/lib/TendencyData.p1.d ${OBJECTDIR}/lib/WeatherDataCollector.p1.d ${OBJECTDIR}/net/ntp.p1.d ${OBJECTDIR}/net/wdata_dloader.p1.d ${OBJECTDIR}/net/wdata_parser.p1.d ${OBJECTDIR}/net/wdata.p1.d ${OBJECTDIR}/sys/interrupt.p1.d ${OBJECTDIR}/sys/keypad.p1.d ${OBJECTDIR}/sys/pic.p1.d ${OBJECTDIR}/sys/sensor.p1.d ${OBJECTDIR}/sys/serial.p1.d ${OBJECTDIR}/sys/settings.p1.d ${OBJECTDIR}/sys/scheduler.p1.d ${OBJECTDIR}/sys/tick.p1.d ${OBJECTDIR}/main.p1.d ${OBJECTDIR}/Utils.p1.d ${OBJECTDIR}/stack/ARP.p1.d ${OBJECTDIR}/stack/BigInt.p1.d ${OBJECTDIR}/stack/Delay.p1.d ${OBJECTDIR}/stack/DHCP.p1.d ${OBJECTDIR}/stack/DNS.p1.d ${OBJECTDIR}/stack/ENC28J60.p1.d ${OBJECTDIR}/stack/Hashes.p1.d ${OBJECTDIR}/stack/Helpers.p1.d ${OBJECTDIR}/stack/ICMP.p1.d ${OBJECTDIR}/stack/IP.p1.d ${OBJECTDIR}/stack/SNTP.p1.d ${OBJECTDIR}/stack/SPIEEPROM.p1.d ${OBJECTDIR}/stack/SPIFlash.p1.d ${OBJECTDIR}/stack/SPIRAM.p1.d ${OBJECTDIR}/stack/StackTsk.p1.d ${OBJECTDIR}/stack/TCP.p1.d ${OBJECTDIR}/stack/Telnet.p1.d ${OBJECTDIR}/stack/Tick.p1.d ${OBJECTDIR}/stack/UDP.p1.d

# Object Files
OBJECTFILES=${OBJECTDIR}/driver/eeprom_25lc.p1 ${OBJECTDIR}/driver/graphlcd.p1 ${OBJECTDIR}/driver/sram_23k.p1 ${OBJECTDIR}/driver/t6963c.p1 ${OBJECTDIR}/lcdui/ForecastScreen.p1 ${OBJECTDIR}/lcdui/image.p1 ${OBJECTDIR}/lcdui/lcdui.p1 ${OBJECTDIR}/lcdui/text.p1 ${OBJECTDIR}/lcdui/MainScreen.p1 ${OBJECTDIR}/lcdui/DisplayController.p1 ${OBJECTDIR}/lcdui/Menu.p1 ${OBJECTDIR}/lcdui/MenuScreen.p1 ${OBJECTDIR}/lcdui/DiagScreen.p1 ${OBJECTDIR}/_ext/1697446845/crc16.p1 ${OBJECTDIR}/lib/TendencyData.p1 ${OBJECTDIR}/lib/WeatherDataCollector.p1 ${OBJECTDIR}/net/ntp.p1 ${OBJECTDIR}/net/wdata_dloader.p1 ${OBJECTDIR}/net/wdata_parser.p1 ${OBJECTDIR}/net/wdata.p1 ${OBJECTDIR}/sys/interrupt.p1 ${OBJECTDIR}/sys/keypad.p1 ${OBJECTDIR}/sys/pic.p1 ${OBJECTDIR}/sys/sensor.p1 ${OBJECTDIR}/sys/serial.p1 ${OBJECTDIR}/sys/settings.p1 ${OBJECTDIR}/sys/scheduler.p1 ${OBJECTDIR}/sys/tick.p1 ${OBJECTDIR}/main.p1 ${OBJECTDIR}/Utils.p1 ${OBJECTDIR}/stack/ARP.p1 ${OBJECTDIR}/stack/BigInt.p1 ${OBJECTDIR}/stack/Delay.p1 ${OBJECTDIR}/stack/DHCP.p1 ${OBJECTDIR}/stack/DNS.p1 ${OBJECTDIR}/stack/ENC28J60.p1 ${OBJECTDIR}/stack/Hashes.p1 ${OBJECTDIR}/stack/Helpers.p1 ${OBJECTDIR}/stack/ICMP.p1 ${OBJECTDIR}/stack/IP.p1 ${OBJECTDIR}/stack/SNTP.p1 ${OBJECTDIR}/stack/SPIEEPROM.p1 ${OBJECTDIR}/stack/SPIFlash.p1 ${OBJECTDIR}/stack/SPIRAM.p1 ${OBJECTDIR}/stack/StackTsk.p1 ${OBJECTDIR}/stack/TCP.p1 ${OBJECTDIR}/stack/Telnet.p1 ${OBJECTDIR}/stack/Tick.p1 ${OBJECTDIR}/stack/UDP.p1

# Source Files
SOURCEFILES=driver/eeprom_25lc.c driver/graphlcd.c driver/sram_23k.c driver/t6963c.c lcdui/ForecastScreen.c lcdui/image.c lcdui/lcdui.c lcdui/text.c lcdui/MainScreen.c lcdui/DisplayController.c lcdui/Menu.c lcdui/MenuScreen.c lcdui/DiagScreen.c ../../software/lib/crc16.c lib/TendencyData.c lib/WeatherDataCollector.c net/ntp.c net/wdata_dloader.c net/wdata_parser.c net/wdata.c sys/interrupt.c sys/keypad.c sys/pic.c sys/sensor.c sys/serial.c sys/settings.c sys/scheduler.c sys/tick.c main.c Utils.c stack/ARP.c stack/BigInt.c stack/Delay.c stack/DHCP.c stack/DNS.c stack/ENC28J60.c stack/Hashes.c stack/Helpers.c stack/ICMP.c stack/IP.c stack/SNTP.c stack/SPIEEPROM.c stack/SPIFlash.c stack/SPIRAM.c stack/StackTsk.c stack/TCP.c stack/Telnet.c stack/Tick.c stack/UDP.c


CFLAGS=
ASFLAGS=
LDLIBSOPTIONS=

############# Tool locations ##########################################
# If you copy a project from one host to another, the path where the  #
# compiler is installed may be different.                             #
# If you open this project with MPLAB X in the new host, this         #
# makefile will be regenerated and the paths will be corrected.       #
#######################################################################
# fixDeps replaces a bunch of sed/cat/printf statements that slow down the build
FIXDEPS=fixDeps

.build-conf:  ${BUILD_SUBPROJECTS}
ifneq ($(INFORMATION_MESSAGE), )
	@echo $(INFORMATION_MESSAGE)
endif
	${MAKE}  -f nbproject/Makefile-XC8.mk dist/${CND_CONF}/${IMAGE_TYPE}/EthernetWS2.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}

MP_PROCESSOR_OPTION=18F4685
# ------------------------------------------------------------------------------------
# Rules for buildStep: compile
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
${OBJECTDIR}/driver/eeprom_25lc.p1: driver/eeprom_25lc.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/driver" 
	@${RM} ${OBJECTDIR}/driver/eeprom_25lc.p1.d 
	@${RM} ${OBJECTDIR}/driver/eeprom_25lc.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1  --debugger=pickit3  --double=24 --float=24 --emi=wordwrite --opt=+asm,+asmfile,+speed,-space,-debug,-local --addrqual=ignore --mode=pro -P -N255 -I"../lib" -I"stack" -I"." --warn=-3 --asmlist -DXPRJ_XC8=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"     -o${OBJECTDIR}/driver/eeprom_25lc.p1 driver/eeprom_25lc.c 
	@-${MV} ${OBJECTDIR}/driver/eeprom_25lc.d ${OBJECTDIR}/driver/eeprom_25lc.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/driver/eeprom_25lc.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/driver/graphlcd.p1: driver/graphlcd.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/driver" 
	@${RM} ${OBJECTDIR}/driver/graphlcd.p1.d 
	@${RM} ${OBJECTDIR}/driver/graphlcd.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1  --debugger=pickit3  --double=24 --float=24 --emi=wordwrite --opt=+asm,+asmfile,+speed,-space,-debug,-local --addrqual=ignore --mode=pro -P -N255 -I"../lib" -I"stack" -I"." --warn=-3 --asmlist -DXPRJ_XC8=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"     -o${OBJECTDIR}/driver/graphlcd.p1 driver/graphlcd.c 
	@-${MV} ${OBJECTDIR}/driver/graphlcd.d ${OBJECTDIR}/driver/graphlcd.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/driver/graphlcd.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/driver/sram_23k.p1: driver/sram_23k.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/driver" 
	@${RM} ${OBJECTDIR}/driver/sram_23k.p1.d 
	@${RM} ${OBJECTDIR}/driver/sram_23k.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1  --debugger=pickit3  --double=24 --float=24 --emi=wordwrite --opt=+asm,+asmfile,+speed,-space,-debug,-local --addrqual=ignore --mode=pro -P -N255 -I"../lib" -I"stack" -I"." --warn=-3 --asmlist -DXPRJ_XC8=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"     -o${OBJECTDIR}/driver/sram_23k.p1 driver/sram_23k.c 
	@-${MV} ${OBJECTDIR}/driver/sram_23k.d ${OBJECTDIR}/driver/sram_23k.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/driver/sram_23k.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/driver/t6963c.p1: driver/t6963c.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/driver" 
	@${RM} ${OBJECTDIR}/driver/t6963c.p1.d 
	@${RM} ${OBJECTDIR}/driver/t6963c.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1  --debugger=pickit3  --double=24 --float=24 --emi=wordwrite --opt=+asm,+asmfile,+speed,-space,-debug,-local --addrqual=ignore --mode=pro -P -N255 -I"../lib" -I"stack" -I"." --warn=-3 --asmlist -DXPRJ_XC8=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"     -o${OBJECTDIR}/driver/t6963c.p1 driver/t6963c.c 
	@-${MV} ${OBJECTDIR}/driver/t6963c.d ${OBJECTDIR}/driver/t6963c.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/driver/t6963c.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/lcdui/ForecastScreen.p1: lcdui/ForecastScreen.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/lcdui" 
	@${RM} ${OBJECTDIR}/lcdui/ForecastScreen.p1.d 
	@${RM} ${OBJECTDIR}/lcdui/ForecastScreen.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1  --debugger=pickit3  --double=24 --float=24 --emi=wordwrite --opt=+asm,+asmfile,+speed,-space,-debug,-local --addrqual=ignore --mode=pro -P -N255 -I"../lib" -I"stack" -I"." --warn=-3 --asmlist -DXPRJ_XC8=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"     -o${OBJECTDIR}/lcdui/ForecastScreen.p1 lcdui/ForecastScreen.c 
	@-${MV} ${OBJECTDIR}/lcdui/ForecastScreen.d ${OBJECTDIR}/lcdui/ForecastScreen.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/lcdui/ForecastScreen.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/lcdui/image.p1: lcdui/image.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/lcdui" 
	@${RM} ${OBJECTDIR}/lcdui/image.p1.d 
	@${RM} ${OBJECTDIR}/lcdui/image.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1  --debugger=pickit3  --double=24 --float=24 --emi=wordwrite --opt=+asm,+asmfile,+speed,-space,-debug,-local --addrqual=ignore --mode=pro -P -N255 -I"../lib" -I"stack" -I"." --warn=-3 --asmlist -DXPRJ_XC8=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"     -o${OBJECTDIR}/lcdui/image.p1 lcdui/image.c 
	@-${MV} ${OBJECTDIR}/lcdui/image.d ${OBJECTDIR}/lcdui/image.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/lcdui/image.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/lcdui/lcdui.p1: lcdui/lcdui.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/lcdui" 
	@${RM} ${OBJECTDIR}/lcdui/lcdui.p1.d 
	@${RM} ${OBJECTDIR}/lcdui/lcdui.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1  --debugger=pickit3  --double=24 --float=24 --emi=wordwrite --opt=+asm,+asmfile,+speed,-space,-debug,-local --addrqual=ignore --mode=pro -P -N255 -I"../lib" -I"stack" -I"." --warn=-3 --asmlist -DXPRJ_XC8=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"     -o${OBJECTDIR}/lcdui/lcdui.p1 lcdui/lcdui.c 
	@-${MV} ${OBJECTDIR}/lcdui/lcdui.d ${OBJECTDIR}/lcdui/lcdui.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/lcdui/lcdui.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/lcdui/text.p1: lcdui/text.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/lcdui" 
	@${RM} ${OBJECTDIR}/lcdui/text.p1.d 
	@${RM} ${OBJECTDIR}/lcdui/text.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1  --debugger=pickit3  --double=24 --float=24 --emi=wordwrite --opt=+asm,+asmfile,+speed,-space,-debug,-local --addrqual=ignore --mode=pro -P -N255 -I"../lib" -I"stack" -I"." --warn=-3 --asmlist -DXPRJ_XC8=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"     -o${OBJECTDIR}/lcdui/text.p1 lcdui/text.c 
	@-${MV} ${OBJECTDIR}/lcdui/text.d ${OBJECTDIR}/lcdui/text.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/lcdui/text.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/lcdui/MainScreen.p1: lcdui/MainScreen.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/lcdui" 
	@${RM} ${OBJECTDIR}/lcdui/MainScreen.p1.d 
	@${RM} ${OBJECTDIR}/lcdui/MainScreen.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1  --debugger=pickit3  --double=24 --float=24 --emi=wordwrite --opt=+asm,+asmfile,+speed,-space,-debug,-local --addrqual=ignore --mode=pro -P -N255 -I"../lib" -I"stack" -I"." --warn=-3 --asmlist -DXPRJ_XC8=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"     -o${OBJECTDIR}/lcdui/MainScreen.p1 lcdui/MainScreen.c 
	@-${MV} ${OBJECTDIR}/lcdui/MainScreen.d ${OBJECTDIR}/lcdui/MainScreen.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/lcdui/MainScreen.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/lcdui/DisplayController.p1: lcdui/DisplayController.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/lcdui" 
	@${RM} ${OBJECTDIR}/lcdui/DisplayController.p1.d 
	@${RM} ${OBJECTDIR}/lcdui/DisplayController.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1  --debugger=pickit3  --double=24 --float=24 --emi=wordwrite --opt=+asm,+asmfile,+speed,-space,-debug,-local --addrqual=ignore --mode=pro -P -N255 -I"../lib" -I"stack" -I"." --warn=-3 --asmlist -DXPRJ_XC8=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"     -o${OBJECTDIR}/lcdui/DisplayController.p1 lcdui/DisplayController.c 
	@-${MV} ${OBJECTDIR}/lcdui/DisplayController.d ${OBJECTDIR}/lcdui/DisplayController.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/lcdui/DisplayController.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/lcdui/Menu.p1: lcdui/Menu.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/lcdui" 
	@${RM} ${OBJECTDIR}/lcdui/Menu.p1.d 
	@${RM} ${OBJECTDIR}/lcdui/Menu.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1  --debugger=pickit3  --double=24 --float=24 --emi=wordwrite --opt=+asm,+asmfile,+speed,-space,-debug,-local --addrqual=ignore --mode=pro -P -N255 -I"../lib" -I"stack" -I"." --warn=-3 --asmlist -DXPRJ_XC8=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"     -o${OBJECTDIR}/lcdui/Menu.p1 lcdui/Menu.c 
	@-${MV} ${OBJECTDIR}/lcdui/Menu.d ${OBJECTDIR}/lcdui/Menu.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/lcdui/Menu.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/lcdui/MenuScreen.p1: lcdui/MenuScreen.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/lcdui" 
	@${RM} ${OBJECTDIR}/lcdui/MenuScreen.p1.d 
	@${RM} ${OBJECTDIR}/lcdui/MenuScreen.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1  --debugger=pickit3  --double=24 --float=24 --emi=wordwrite --opt=+asm,+asmfile,+speed,-space,-debug,-local --addrqual=ignore --mode=pro -P -N255 -I"../lib" -I"stack" -I"." --warn=-3 --asmlist -DXPRJ_XC8=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"     -o${OBJECTDIR}/lcdui/MenuScreen.p1 lcdui/MenuScreen.c 
	@-${MV} ${OBJECTDIR}/lcdui/MenuScreen.d ${OBJECTDIR}/lcdui/MenuScreen.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/lcdui/MenuScreen.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/lcdui/DiagScreen.p1: lcdui/DiagScreen.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/lcdui" 
	@${RM} ${OBJECTDIR}/lcdui/DiagScreen.p1.d 
	@${RM} ${OBJECTDIR}/lcdui/DiagScreen.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1  --debugger=pickit3  --double=24 --float=24 --emi=wordwrite --opt=+asm,+asmfile,+speed,-space,-debug,-local --addrqual=ignore --mode=pro -P -N255 -I"../lib" -I"stack" -I"." --warn=-3 --asmlist -DXPRJ_XC8=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"     -o${OBJECTDIR}/lcdui/DiagScreen.p1 lcdui/DiagScreen.c 
	@-${MV} ${OBJECTDIR}/lcdui/DiagScreen.d ${OBJECTDIR}/lcdui/DiagScreen.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/lcdui/DiagScreen.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/_ext/1697446845/crc16.p1: ../../software/lib/crc16.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1697446845" 
	@${RM} ${OBJECTDIR}/_ext/1697446845/crc16.p1.d 
	@${RM} ${OBJECTDIR}/_ext/1697446845/crc16.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1  --debugger=pickit3  --double=24 --float=24 --emi=wordwrite --opt=+asm,+asmfile,+speed,-space,-debug,-local --addrqual=ignore --mode=pro -P -N255 -I"../lib" -I"stack" -I"." --warn=-3 --asmlist -DXPRJ_XC8=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"     -o${OBJECTDIR}/_ext/1697446845/crc16.p1 ../../software/lib/crc16.c 
	@-${MV} ${OBJECTDIR}/_ext/1697446845/crc16.d ${OBJECTDIR}/_ext/1697446845/crc16.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/1697446845/crc16.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/lib/TendencyData.p1: lib/TendencyData.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/lib" 
	@${RM} ${OBJECTDIR}/lib/TendencyData.p1.d 
	@${RM} ${OBJECTDIR}/lib/TendencyData.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1  --debugger=pickit3  --double=24 --float=24 --emi=wordwrite --opt=+asm,+asmfile,+speed,-space,-debug,-local --addrqual=ignore --mode=pro -P -N255 -I"../lib" -I"stack" -I"." --warn=-3 --asmlist -DXPRJ_XC8=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"     -o${OBJECTDIR}/lib/TendencyData.p1 lib/TendencyData.c 
	@-${MV} ${OBJECTDIR}/lib/TendencyData.d ${OBJECTDIR}/lib/TendencyData.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/lib/TendencyData.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/lib/WeatherDataCollector.p1: lib/WeatherDataCollector.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/lib" 
	@${RM} ${OBJECTDIR}/lib/WeatherDataCollector.p1.d 
	@${RM} ${OBJECTDIR}/lib/WeatherDataCollector.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1  --debugger=pickit3  --double=24 --float=24 --emi=wordwrite --opt=+asm,+asmfile,+speed,-space,-debug,-local --addrqual=ignore --mode=pro -P -N255 -I"../lib" -I"stack" -I"." --warn=-3 --asmlist -DXPRJ_XC8=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"     -o${OBJECTDIR}/lib/WeatherDataCollector.p1 lib/WeatherDataCollector.c 
	@-${MV} ${OBJECTDIR}/lib/WeatherDataCollector.d ${OBJECTDIR}/lib/WeatherDataCollector.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/lib/WeatherDataCollector.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/net/ntp.p1: net/ntp.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/net" 
	@${RM} ${OBJECTDIR}/net/ntp.p1.d 
	@${RM} ${OBJECTDIR}/net/ntp.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1  --debugger=pickit3  --double=24 --float=24 --emi=wordwrite --opt=+asm,+asmfile,+speed,-space,-debug,-local --addrqual=ignore --mode=pro -P -N255 -I"../lib" -I"stack" -I"." --warn=-3 --asmlist -DXPRJ_XC8=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"     -o${OBJECTDIR}/net/ntp.p1 net/ntp.c 
	@-${MV} ${OBJECTDIR}/net/ntp.d ${OBJECTDIR}/net/ntp.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/net/ntp.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/net/wdata_dloader.p1: net/wdata_dloader.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/net" 
	@${RM} ${OBJECTDIR}/net/wdata_dloader.p1.d 
	@${RM} ${OBJECTDIR}/net/wdata_dloader.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1  --debugger=pickit3  --double=24 --float=24 --emi=wordwrite --opt=+asm,+asmfile,+speed,-space,-debug,-local --addrqual=ignore --mode=pro -P -N255 -I"../lib" -I"stack" -I"." --warn=-3 --asmlist -DXPRJ_XC8=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"     -o${OBJECTDIR}/net/wdata_dloader.p1 net/wdata_dloader.c 
	@-${MV} ${OBJECTDIR}/net/wdata_dloader.d ${OBJECTDIR}/net/wdata_dloader.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/net/wdata_dloader.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/net/wdata_parser.p1: net/wdata_parser.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/net" 
	@${RM} ${OBJECTDIR}/net/wdata_parser.p1.d 
	@${RM} ${OBJECTDIR}/net/wdata_parser.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1  --debugger=pickit3  --double=24 --float=24 --emi=wordwrite --opt=+asm,+asmfile,+speed,-space,-debug,-local --addrqual=ignore --mode=pro -P -N255 -I"../lib" -I"stack" -I"." --warn=-3 --asmlist -DXPRJ_XC8=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"     -o${OBJECTDIR}/net/wdata_parser.p1 net/wdata_parser.c 
	@-${MV} ${OBJECTDIR}/net/wdata_parser.d ${OBJECTDIR}/net/wdata_parser.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/net/wdata_parser.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/net/wdata.p1: net/wdata.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/net" 
	@${RM} ${OBJECTDIR}/net/wdata.p1.d 
	@${RM} ${OBJECTDIR}/net/wdata.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1  --debugger=pickit3  --double=24 --float=24 --emi=wordwrite --opt=+asm,+asmfile,+speed,-space,-debug,-local --addrqual=ignore --mode=pro -P -N255 -I"../lib" -I"stack" -I"." --warn=-3 --asmlist -DXPRJ_XC8=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"     -o${OBJECTDIR}/net/wdata.p1 net/wdata.c 
	@-${MV} ${OBJECTDIR}/net/wdata.d ${OBJECTDIR}/net/wdata.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/net/wdata.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sys/interrupt.p1: sys/interrupt.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/sys" 
	@${RM} ${OBJECTDIR}/sys/interrupt.p1.d 
	@${RM} ${OBJECTDIR}/sys/interrupt.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1  --debugger=pickit3  --double=24 --float=24 --emi=wordwrite --opt=+asm,+asmfile,+speed,-space,-debug,-local --addrqual=ignore --mode=pro -P -N255 -I"../lib" -I"stack" -I"." --warn=-3 --asmlist -DXPRJ_XC8=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"     -o${OBJECTDIR}/sys/interrupt.p1 sys/interrupt.c 
	@-${MV} ${OBJECTDIR}/sys/interrupt.d ${OBJECTDIR}/sys/interrupt.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sys/interrupt.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sys/keypad.p1: sys/keypad.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/sys" 
	@${RM} ${OBJECTDIR}/sys/keypad.p1.d 
	@${RM} ${OBJECTDIR}/sys/keypad.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1  --debugger=pickit3  --double=24 --float=24 --emi=wordwrite --opt=+asm,+asmfile,+speed,-space,-debug,-local --addrqual=ignore --mode=pro -P -N255 -I"../lib" -I"stack" -I"." --warn=-3 --asmlist -DXPRJ_XC8=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"     -o${OBJECTDIR}/sys/keypad.p1 sys/keypad.c 
	@-${MV} ${OBJECTDIR}/sys/keypad.d ${OBJECTDIR}/sys/keypad.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sys/keypad.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sys/pic.p1: sys/pic.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/sys" 
	@${RM} ${OBJECTDIR}/sys/pic.p1.d 
	@${RM} ${OBJECTDIR}/sys/pic.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1  --debugger=pickit3  --double=24 --float=24 --emi=wordwrite --opt=+asm,+asmfile,+speed,-space,-debug,-local --addrqual=ignore --mode=pro -P -N255 -I"../lib" -I"stack" -I"." --warn=-3 --asmlist -DXPRJ_XC8=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"     -o${OBJECTDIR}/sys/pic.p1 sys/pic.c 
	@-${MV} ${OBJECTDIR}/sys/pic.d ${OBJECTDIR}/sys/pic.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sys/pic.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sys/sensor.p1: sys/sensor.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/sys" 
	@${RM} ${OBJECTDIR}/sys/sensor.p1.d 
	@${RM} ${OBJECTDIR}/sys/sensor.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1  --debugger=pickit3  --double=24 --float=24 --emi=wordwrite --opt=+asm,+asmfile,+speed,-space,-debug,-local --addrqual=ignore --mode=pro -P -N255 -I"../lib" -I"stack" -I"." --warn=-3 --asmlist -DXPRJ_XC8=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"     -o${OBJECTDIR}/sys/sensor.p1 sys/sensor.c 
	@-${MV} ${OBJECTDIR}/sys/sensor.d ${OBJECTDIR}/sys/sensor.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sys/sensor.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sys/serial.p1: sys/serial.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/sys" 
	@${RM} ${OBJECTDIR}/sys/serial.p1.d 
	@${RM} ${OBJECTDIR}/sys/serial.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1  --debugger=pickit3  --double=24 --float=24 --emi=wordwrite --opt=+asm,+asmfile,+speed,-space,-debug,-local --addrqual=ignore --mode=pro -P -N255 -I"../lib" -I"stack" -I"." --warn=-3 --asmlist -DXPRJ_XC8=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"     -o${OBJECTDIR}/sys/serial.p1 sys/serial.c 
	@-${MV} ${OBJECTDIR}/sys/serial.d ${OBJECTDIR}/sys/serial.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sys/serial.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sys/settings.p1: sys/settings.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/sys" 
	@${RM} ${OBJECTDIR}/sys/settings.p1.d 
	@${RM} ${OBJECTDIR}/sys/settings.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1  --debugger=pickit3  --double=24 --float=24 --emi=wordwrite --opt=+asm,+asmfile,+speed,-space,-debug,-local --addrqual=ignore --mode=pro -P -N255 -I"../lib" -I"stack" -I"." --warn=-3 --asmlist -DXPRJ_XC8=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"     -o${OBJECTDIR}/sys/settings.p1 sys/settings.c 
	@-${MV} ${OBJECTDIR}/sys/settings.d ${OBJECTDIR}/sys/settings.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sys/settings.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sys/scheduler.p1: sys/scheduler.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/sys" 
	@${RM} ${OBJECTDIR}/sys/scheduler.p1.d 
	@${RM} ${OBJECTDIR}/sys/scheduler.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1  --debugger=pickit3  --double=24 --float=24 --emi=wordwrite --opt=+asm,+asmfile,+speed,-space,-debug,-local --addrqual=ignore --mode=pro -P -N255 -I"../lib" -I"stack" -I"." --warn=-3 --asmlist -DXPRJ_XC8=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"     -o${OBJECTDIR}/sys/scheduler.p1 sys/scheduler.c 
	@-${MV} ${OBJECTDIR}/sys/scheduler.d ${OBJECTDIR}/sys/scheduler.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sys/scheduler.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sys/tick.p1: sys/tick.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/sys" 
	@${RM} ${OBJECTDIR}/sys/tick.p1.d 
	@${RM} ${OBJECTDIR}/sys/tick.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1  --debugger=pickit3  --double=24 --float=24 --emi=wordwrite --opt=+asm,+asmfile,+speed,-space,-debug,-local --addrqual=ignore --mode=pro -P -N255 -I"../lib" -I"stack" -I"." --warn=-3 --asmlist -DXPRJ_XC8=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"     -o${OBJECTDIR}/sys/tick.p1 sys/tick.c 
	@-${MV} ${OBJECTDIR}/sys/tick.d ${OBJECTDIR}/sys/tick.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sys/tick.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/main.p1: main.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/main.p1.d 
	@${RM} ${OBJECTDIR}/main.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1  --debugger=pickit3  --double=24 --float=24 --emi=wordwrite --opt=+asm,+asmfile,+speed,-space,-debug,-local --addrqual=ignore --mode=pro -P -N255 -I"../lib" -I"stack" -I"." --warn=-3 --asmlist -DXPRJ_XC8=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"     -o${OBJECTDIR}/main.p1 main.c 
	@-${MV} ${OBJECTDIR}/main.d ${OBJECTDIR}/main.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/main.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/Utils.p1: Utils.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/Utils.p1.d 
	@${RM} ${OBJECTDIR}/Utils.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1  --debugger=pickit3  --double=24 --float=24 --emi=wordwrite --opt=+asm,+asmfile,+speed,-space,-debug,-local --addrqual=ignore --mode=pro -P -N255 -I"../lib" -I"stack" -I"." --warn=-3 --asmlist -DXPRJ_XC8=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"     -o${OBJECTDIR}/Utils.p1 Utils.c 
	@-${MV} ${OBJECTDIR}/Utils.d ${OBJECTDIR}/Utils.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/Utils.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/stack/ARP.p1: stack/ARP.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/stack" 
	@${RM} ${OBJECTDIR}/stack/ARP.p1.d 
	@${RM} ${OBJECTDIR}/stack/ARP.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1  --debugger=pickit3  --double=24 --float=24 --emi=wordwrite --opt=+asm,+asmfile,+speed,-space,-debug,-local --addrqual=ignore --mode=pro -P -N255 -I"../lib" -I"stack" -I"." --warn=-3 --asmlist -DXPRJ_XC8=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"     -o${OBJECTDIR}/stack/ARP.p1 stack/ARP.c 
	@-${MV} ${OBJECTDIR}/stack/ARP.d ${OBJECTDIR}/stack/ARP.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/stack/ARP.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/stack/BigInt.p1: stack/BigInt.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/stack" 
	@${RM} ${OBJECTDIR}/stack/BigInt.p1.d 
	@${RM} ${OBJECTDIR}/stack/BigInt.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1  --debugger=pickit3  --double=24 --float=24 --emi=wordwrite --opt=+asm,+asmfile,+speed,-space,-debug,-local --addrqual=ignore --mode=pro -P -N255 -I"../lib" -I"stack" -I"." --warn=-3 --asmlist -DXPRJ_XC8=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"     -o${OBJECTDIR}/stack/BigInt.p1 stack/BigInt.c 
	@-${MV} ${OBJECTDIR}/stack/BigInt.d ${OBJECTDIR}/stack/BigInt.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/stack/BigInt.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/stack/Delay.p1: stack/Delay.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/stack" 
	@${RM} ${OBJECTDIR}/stack/Delay.p1.d 
	@${RM} ${OBJECTDIR}/stack/Delay.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1  --debugger=pickit3  --double=24 --float=24 --emi=wordwrite --opt=+asm,+asmfile,+speed,-space,-debug,-local --addrqual=ignore --mode=pro -P -N255 -I"../lib" -I"stack" -I"." --warn=-3 --asmlist -DXPRJ_XC8=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"     -o${OBJECTDIR}/stack/Delay.p1 stack/Delay.c 
	@-${MV} ${OBJECTDIR}/stack/Delay.d ${OBJECTDIR}/stack/Delay.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/stack/Delay.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/stack/DHCP.p1: stack/DHCP.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/stack" 
	@${RM} ${OBJECTDIR}/stack/DHCP.p1.d 
	@${RM} ${OBJECTDIR}/stack/DHCP.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1  --debugger=pickit3  --double=24 --float=24 --emi=wordwrite --opt=+asm,+asmfile,+speed,-space,-debug,-local --addrqual=ignore --mode=pro -P -N255 -I"../lib" -I"stack" -I"." --warn=-3 --asmlist -DXPRJ_XC8=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"     -o${OBJECTDIR}/stack/DHCP.p1 stack/DHCP.c 
	@-${MV} ${OBJECTDIR}/stack/DHCP.d ${OBJECTDIR}/stack/DHCP.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/stack/DHCP.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/stack/DNS.p1: stack/DNS.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/stack" 
	@${RM} ${OBJECTDIR}/stack/DNS.p1.d 
	@${RM} ${OBJECTDIR}/stack/DNS.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1  --debugger=pickit3  --double=24 --float=24 --emi=wordwrite --opt=+asm,+asmfile,+speed,-space,-debug,-local --addrqual=ignore --mode=pro -P -N255 -I"../lib" -I"stack" -I"." --warn=-3 --asmlist -DXPRJ_XC8=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"     -o${OBJECTDIR}/stack/DNS.p1 stack/DNS.c 
	@-${MV} ${OBJECTDIR}/stack/DNS.d ${OBJECTDIR}/stack/DNS.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/stack/DNS.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/stack/ENC28J60.p1: stack/ENC28J60.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/stack" 
	@${RM} ${OBJECTDIR}/stack/ENC28J60.p1.d 
	@${RM} ${OBJECTDIR}/stack/ENC28J60.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1  --debugger=pickit3  --double=24 --float=24 --emi=wordwrite --opt=+asm,+asmfile,+speed,-space,-debug,-local --addrqual=ignore --mode=pro -P -N255 -I"../lib" -I"stack" -I"." --warn=-3 --asmlist -DXPRJ_XC8=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"     -o${OBJECTDIR}/stack/ENC28J60.p1 stack/ENC28J60.c 
	@-${MV} ${OBJECTDIR}/stack/ENC28J60.d ${OBJECTDIR}/stack/ENC28J60.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/stack/ENC28J60.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/stack/Hashes.p1: stack/Hashes.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/stack" 
	@${RM} ${OBJECTDIR}/stack/Hashes.p1.d 
	@${RM} ${OBJECTDIR}/stack/Hashes.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1  --debugger=pickit3  --double=24 --float=24 --emi=wordwrite --opt=+asm,+asmfile,+speed,-space,-debug,-local --addrqual=ignore --mode=pro -P -N255 -I"../lib" -I"stack" -I"." --warn=-3 --asmlist -DXPRJ_XC8=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"     -o${OBJECTDIR}/stack/Hashes.p1 stack/Hashes.c 
	@-${MV} ${OBJECTDIR}/stack/Hashes.d ${OBJECTDIR}/stack/Hashes.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/stack/Hashes.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/stack/Helpers.p1: stack/Helpers.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/stack" 
	@${RM} ${OBJECTDIR}/stack/Helpers.p1.d 
	@${RM} ${OBJECTDIR}/stack/Helpers.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1  --debugger=pickit3  --double=24 --float=24 --emi=wordwrite --opt=+asm,+asmfile,+speed,-space,-debug,-local --addrqual=ignore --mode=pro -P -N255 -I"../lib" -I"stack" -I"." --warn=-3 --asmlist -DXPRJ_XC8=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"     -o${OBJECTDIR}/stack/Helpers.p1 stack/Helpers.c 
	@-${MV} ${OBJECTDIR}/stack/Helpers.d ${OBJECTDIR}/stack/Helpers.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/stack/Helpers.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/stack/ICMP.p1: stack/ICMP.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/stack" 
	@${RM} ${OBJECTDIR}/stack/ICMP.p1.d 
	@${RM} ${OBJECTDIR}/stack/ICMP.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1  --debugger=pickit3  --double=24 --float=24 --emi=wordwrite --opt=+asm,+asmfile,+speed,-space,-debug,-local --addrqual=ignore --mode=pro -P -N255 -I"../lib" -I"stack" -I"." --warn=-3 --asmlist -DXPRJ_XC8=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"     -o${OBJECTDIR}/stack/ICMP.p1 stack/ICMP.c 
	@-${MV} ${OBJECTDIR}/stack/ICMP.d ${OBJECTDIR}/stack/ICMP.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/stack/ICMP.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/stack/IP.p1: stack/IP.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/stack" 
	@${RM} ${OBJECTDIR}/stack/IP.p1.d 
	@${RM} ${OBJECTDIR}/stack/IP.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1  --debugger=pickit3  --double=24 --float=24 --emi=wordwrite --opt=+asm,+asmfile,+speed,-space,-debug,-local --addrqual=ignore --mode=pro -P -N255 -I"../lib" -I"stack" -I"." --warn=-3 --asmlist -DXPRJ_XC8=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"     -o${OBJECTDIR}/stack/IP.p1 stack/IP.c 
	@-${MV} ${OBJECTDIR}/stack/IP.d ${OBJECTDIR}/stack/IP.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/stack/IP.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/stack/SNTP.p1: stack/SNTP.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/stack" 
	@${RM} ${OBJECTDIR}/stack/SNTP.p1.d 
	@${RM} ${OBJECTDIR}/stack/SNTP.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1  --debugger=pickit3  --double=24 --float=24 --emi=wordwrite --opt=+asm,+asmfile,+speed,-space,-debug,-local --addrqual=ignore --mode=pro -P -N255 -I"../lib" -I"stack" -I"." --warn=-3 --asmlist -DXPRJ_XC8=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"     -o${OBJECTDIR}/stack/SNTP.p1 stack/SNTP.c 
	@-${MV} ${OBJECTDIR}/stack/SNTP.d ${OBJECTDIR}/stack/SNTP.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/stack/SNTP.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/stack/SPIEEPROM.p1: stack/SPIEEPROM.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/stack" 
	@${RM} ${OBJECTDIR}/stack/SPIEEPROM.p1.d 
	@${RM} ${OBJECTDIR}/stack/SPIEEPROM.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1  --debugger=pickit3  --double=24 --float=24 --emi=wordwrite --opt=+asm,+asmfile,+speed,-space,-debug,-local --addrqual=ignore --mode=pro -P -N255 -I"../lib" -I"stack" -I"." --warn=-3 --asmlist -DXPRJ_XC8=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"     -o${OBJECTDIR}/stack/SPIEEPROM.p1 stack/SPIEEPROM.c 
	@-${MV} ${OBJECTDIR}/stack/SPIEEPROM.d ${OBJECTDIR}/stack/SPIEEPROM.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/stack/SPIEEPROM.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/stack/SPIFlash.p1: stack/SPIFlash.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/stack" 
	@${RM} ${OBJECTDIR}/stack/SPIFlash.p1.d 
	@${RM} ${OBJECTDIR}/stack/SPIFlash.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1  --debugger=pickit3  --double=24 --float=24 --emi=wordwrite --opt=+asm,+asmfile,+speed,-space,-debug,-local --addrqual=ignore --mode=pro -P -N255 -I"../lib" -I"stack" -I"." --warn=-3 --asmlist -DXPRJ_XC8=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"     -o${OBJECTDIR}/stack/SPIFlash.p1 stack/SPIFlash.c 
	@-${MV} ${OBJECTDIR}/stack/SPIFlash.d ${OBJECTDIR}/stack/SPIFlash.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/stack/SPIFlash.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/stack/SPIRAM.p1: stack/SPIRAM.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/stack" 
	@${RM} ${OBJECTDIR}/stack/SPIRAM.p1.d 
	@${RM} ${OBJECTDIR}/stack/SPIRAM.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1  --debugger=pickit3  --double=24 --float=24 --emi=wordwrite --opt=+asm,+asmfile,+speed,-space,-debug,-local --addrqual=ignore --mode=pro -P -N255 -I"../lib" -I"stack" -I"." --warn=-3 --asmlist -DXPRJ_XC8=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"     -o${OBJECTDIR}/stack/SPIRAM.p1 stack/SPIRAM.c 
	@-${MV} ${OBJECTDIR}/stack/SPIRAM.d ${OBJECTDIR}/stack/SPIRAM.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/stack/SPIRAM.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/stack/StackTsk.p1: stack/StackTsk.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/stack" 
	@${RM} ${OBJECTDIR}/stack/StackTsk.p1.d 
	@${RM} ${OBJECTDIR}/stack/StackTsk.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1  --debugger=pickit3  --double=24 --float=24 --emi=wordwrite --opt=+asm,+asmfile,+speed,-space,-debug,-local --addrqual=ignore --mode=pro -P -N255 -I"../lib" -I"stack" -I"." --warn=-3 --asmlist -DXPRJ_XC8=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"     -o${OBJECTDIR}/stack/StackTsk.p1 stack/StackTsk.c 
	@-${MV} ${OBJECTDIR}/stack/StackTsk.d ${OBJECTDIR}/stack/StackTsk.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/stack/StackTsk.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/stack/TCP.p1: stack/TCP.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/stack" 
	@${RM} ${OBJECTDIR}/stack/TCP.p1.d 
	@${RM} ${OBJECTDIR}/stack/TCP.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1  --debugger=pickit3  --double=24 --float=24 --emi=wordwrite --opt=+asm,+asmfile,+speed,-space,-debug,-local --addrqual=ignore --mode=pro -P -N255 -I"../lib" -I"stack" -I"." --warn=-3 --asmlist -DXPRJ_XC8=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"     -o${OBJECTDIR}/stack/TCP.p1 stack/TCP.c 
	@-${MV} ${OBJECTDIR}/stack/TCP.d ${OBJECTDIR}/stack/TCP.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/stack/TCP.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/stack/Telnet.p1: stack/Telnet.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/stack" 
	@${RM} ${OBJECTDIR}/stack/Telnet.p1.d 
	@${RM} ${OBJECTDIR}/stack/Telnet.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1  --debugger=pickit3  --double=24 --float=24 --emi=wordwrite --opt=+asm,+asmfile,+speed,-space,-debug,-local --addrqual=ignore --mode=pro -P -N255 -I"../lib" -I"stack" -I"." --warn=-3 --asmlist -DXPRJ_XC8=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"     -o${OBJECTDIR}/stack/Telnet.p1 stack/Telnet.c 
	@-${MV} ${OBJECTDIR}/stack/Telnet.d ${OBJECTDIR}/stack/Telnet.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/stack/Telnet.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/stack/Tick.p1: stack/Tick.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/stack" 
	@${RM} ${OBJECTDIR}/stack/Tick.p1.d 
	@${RM} ${OBJECTDIR}/stack/Tick.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1  --debugger=pickit3  --double=24 --float=24 --emi=wordwrite --opt=+asm,+asmfile,+speed,-space,-debug,-local --addrqual=ignore --mode=pro -P -N255 -I"../lib" -I"stack" -I"." --warn=-3 --asmlist -DXPRJ_XC8=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"     -o${OBJECTDIR}/stack/Tick.p1 stack/Tick.c 
	@-${MV} ${OBJECTDIR}/stack/Tick.d ${OBJECTDIR}/stack/Tick.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/stack/Tick.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/stack/UDP.p1: stack/UDP.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/stack" 
	@${RM} ${OBJECTDIR}/stack/UDP.p1.d 
	@${RM} ${OBJECTDIR}/stack/UDP.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1  --debugger=pickit3  --double=24 --float=24 --emi=wordwrite --opt=+asm,+asmfile,+speed,-space,-debug,-local --addrqual=ignore --mode=pro -P -N255 -I"../lib" -I"stack" -I"." --warn=-3 --asmlist -DXPRJ_XC8=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"     -o${OBJECTDIR}/stack/UDP.p1 stack/UDP.c 
	@-${MV} ${OBJECTDIR}/stack/UDP.d ${OBJECTDIR}/stack/UDP.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/stack/UDP.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
else
${OBJECTDIR}/driver/eeprom_25lc.p1: driver/eeprom_25lc.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/driver" 
	@${RM} ${OBJECTDIR}/driver/eeprom_25lc.p1.d 
	@${RM} ${OBJECTDIR}/driver/eeprom_25lc.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=+asm,+asmfile,+speed,-space,-debug,-local --addrqual=ignore --mode=pro -P -N255 -I"../lib" -I"stack" -I"." --warn=-3 --asmlist -DXPRJ_XC8=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"     -o${OBJECTDIR}/driver/eeprom_25lc.p1 driver/eeprom_25lc.c 
	@-${MV} ${OBJECTDIR}/driver/eeprom_25lc.d ${OBJECTDIR}/driver/eeprom_25lc.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/driver/eeprom_25lc.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/driver/graphlcd.p1: driver/graphlcd.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/driver" 
	@${RM} ${OBJECTDIR}/driver/graphlcd.p1.d 
	@${RM} ${OBJECTDIR}/driver/graphlcd.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=+asm,+asmfile,+speed,-space,-debug,-local --addrqual=ignore --mode=pro -P -N255 -I"../lib" -I"stack" -I"." --warn=-3 --asmlist -DXPRJ_XC8=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"     -o${OBJECTDIR}/driver/graphlcd.p1 driver/graphlcd.c 
	@-${MV} ${OBJECTDIR}/driver/graphlcd.d ${OBJECTDIR}/driver/graphlcd.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/driver/graphlcd.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/driver/sram_23k.p1: driver/sram_23k.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/driver" 
	@${RM} ${OBJECTDIR}/driver/sram_23k.p1.d 
	@${RM} ${OBJECTDIR}/driver/sram_23k.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=+asm,+asmfile,+speed,-space,-debug,-local --addrqual=ignore --mode=pro -P -N255 -I"../lib" -I"stack" -I"." --warn=-3 --asmlist -DXPRJ_XC8=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"     -o${OBJECTDIR}/driver/sram_23k.p1 driver/sram_23k.c 
	@-${MV} ${OBJECTDIR}/driver/sram_23k.d ${OBJECTDIR}/driver/sram_23k.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/driver/sram_23k.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/driver/t6963c.p1: driver/t6963c.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/driver" 
	@${RM} ${OBJECTDIR}/driver/t6963c.p1.d 
	@${RM} ${OBJECTDIR}/driver/t6963c.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=+asm,+asmfile,+speed,-space,-debug,-local --addrqual=ignore --mode=pro -P -N255 -I"../lib" -I"stack" -I"." --warn=-3 --asmlist -DXPRJ_XC8=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"     -o${OBJECTDIR}/driver/t6963c.p1 driver/t6963c.c 
	@-${MV} ${OBJECTDIR}/driver/t6963c.d ${OBJECTDIR}/driver/t6963c.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/driver/t6963c.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/lcdui/ForecastScreen.p1: lcdui/ForecastScreen.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/lcdui" 
	@${RM} ${OBJECTDIR}/lcdui/ForecastScreen.p1.d 
	@${RM} ${OBJECTDIR}/lcdui/ForecastScreen.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=+asm,+asmfile,+speed,-space,-debug,-local --addrqual=ignore --mode=pro -P -N255 -I"../lib" -I"stack" -I"." --warn=-3 --asmlist -DXPRJ_XC8=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"     -o${OBJECTDIR}/lcdui/ForecastScreen.p1 lcdui/ForecastScreen.c 
	@-${MV} ${OBJECTDIR}/lcdui/ForecastScreen.d ${OBJECTDIR}/lcdui/ForecastScreen.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/lcdui/ForecastScreen.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/lcdui/image.p1: lcdui/image.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/lcdui" 
	@${RM} ${OBJECTDIR}/lcdui/image.p1.d 
	@${RM} ${OBJECTDIR}/lcdui/image.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=+asm,+asmfile,+speed,-space,-debug,-local --addrqual=ignore --mode=pro -P -N255 -I"../lib" -I"stack" -I"." --warn=-3 --asmlist -DXPRJ_XC8=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"     -o${OBJECTDIR}/lcdui/image.p1 lcdui/image.c 
	@-${MV} ${OBJECTDIR}/lcdui/image.d ${OBJECTDIR}/lcdui/image.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/lcdui/image.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/lcdui/lcdui.p1: lcdui/lcdui.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/lcdui" 
	@${RM} ${OBJECTDIR}/lcdui/lcdui.p1.d 
	@${RM} ${OBJECTDIR}/lcdui/lcdui.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=+asm,+asmfile,+speed,-space,-debug,-local --addrqual=ignore --mode=pro -P -N255 -I"../lib" -I"stack" -I"." --warn=-3 --asmlist -DXPRJ_XC8=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"     -o${OBJECTDIR}/lcdui/lcdui.p1 lcdui/lcdui.c 
	@-${MV} ${OBJECTDIR}/lcdui/lcdui.d ${OBJECTDIR}/lcdui/lcdui.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/lcdui/lcdui.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/lcdui/text.p1: lcdui/text.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/lcdui" 
	@${RM} ${OBJECTDIR}/lcdui/text.p1.d 
	@${RM} ${OBJECTDIR}/lcdui/text.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=+asm,+asmfile,+speed,-space,-debug,-local --addrqual=ignore --mode=pro -P -N255 -I"../lib" -I"stack" -I"." --warn=-3 --asmlist -DXPRJ_XC8=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"     -o${OBJECTDIR}/lcdui/text.p1 lcdui/text.c 
	@-${MV} ${OBJECTDIR}/lcdui/text.d ${OBJECTDIR}/lcdui/text.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/lcdui/text.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/lcdui/MainScreen.p1: lcdui/MainScreen.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/lcdui" 
	@${RM} ${OBJECTDIR}/lcdui/MainScreen.p1.d 
	@${RM} ${OBJECTDIR}/lcdui/MainScreen.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=+asm,+asmfile,+speed,-space,-debug,-local --addrqual=ignore --mode=pro -P -N255 -I"../lib" -I"stack" -I"." --warn=-3 --asmlist -DXPRJ_XC8=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"     -o${OBJECTDIR}/lcdui/MainScreen.p1 lcdui/MainScreen.c 
	@-${MV} ${OBJECTDIR}/lcdui/MainScreen.d ${OBJECTDIR}/lcdui/MainScreen.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/lcdui/MainScreen.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/lcdui/DisplayController.p1: lcdui/DisplayController.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/lcdui" 
	@${RM} ${OBJECTDIR}/lcdui/DisplayController.p1.d 
	@${RM} ${OBJECTDIR}/lcdui/DisplayController.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=+asm,+asmfile,+speed,-space,-debug,-local --addrqual=ignore --mode=pro -P -N255 -I"../lib" -I"stack" -I"." --warn=-3 --asmlist -DXPRJ_XC8=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"     -o${OBJECTDIR}/lcdui/DisplayController.p1 lcdui/DisplayController.c 
	@-${MV} ${OBJECTDIR}/lcdui/DisplayController.d ${OBJECTDIR}/lcdui/DisplayController.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/lcdui/DisplayController.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/lcdui/Menu.p1: lcdui/Menu.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/lcdui" 
	@${RM} ${OBJECTDIR}/lcdui/Menu.p1.d 
	@${RM} ${OBJECTDIR}/lcdui/Menu.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=+asm,+asmfile,+speed,-space,-debug,-local --addrqual=ignore --mode=pro -P -N255 -I"../lib" -I"stack" -I"." --warn=-3 --asmlist -DXPRJ_XC8=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"     -o${OBJECTDIR}/lcdui/Menu.p1 lcdui/Menu.c 
	@-${MV} ${OBJECTDIR}/lcdui/Menu.d ${OBJECTDIR}/lcdui/Menu.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/lcdui/Menu.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/lcdui/MenuScreen.p1: lcdui/MenuScreen.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/lcdui" 
	@${RM} ${OBJECTDIR}/lcdui/MenuScreen.p1.d 
	@${RM} ${OBJECTDIR}/lcdui/MenuScreen.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=+asm,+asmfile,+speed,-space,-debug,-local --addrqual=ignore --mode=pro -P -N255 -I"../lib" -I"stack" -I"." --warn=-3 --asmlist -DXPRJ_XC8=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"     -o${OBJECTDIR}/lcdui/MenuScreen.p1 lcdui/MenuScreen.c 
	@-${MV} ${OBJECTDIR}/lcdui/MenuScreen.d ${OBJECTDIR}/lcdui/MenuScreen.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/lcdui/MenuScreen.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/lcdui/DiagScreen.p1: lcdui/DiagScreen.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/lcdui" 
	@${RM} ${OBJECTDIR}/lcdui/DiagScreen.p1.d 
	@${RM} ${OBJECTDIR}/lcdui/DiagScreen.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=+asm,+asmfile,+speed,-space,-debug,-local --addrqual=ignore --mode=pro -P -N255 -I"../lib" -I"stack" -I"." --warn=-3 --asmlist -DXPRJ_XC8=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"     -o${OBJECTDIR}/lcdui/DiagScreen.p1 lcdui/DiagScreen.c 
	@-${MV} ${OBJECTDIR}/lcdui/DiagScreen.d ${OBJECTDIR}/lcdui/DiagScreen.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/lcdui/DiagScreen.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/_ext/1697446845/crc16.p1: ../../software/lib/crc16.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1697446845" 
	@${RM} ${OBJECTDIR}/_ext/1697446845/crc16.p1.d 
	@${RM} ${OBJECTDIR}/_ext/1697446845/crc16.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=+asm,+asmfile,+speed,-space,-debug,-local --addrqual=ignore --mode=pro -P -N255 -I"../lib" -I"stack" -I"." --warn=-3 --asmlist -DXPRJ_XC8=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"     -o${OBJECTDIR}/_ext/1697446845/crc16.p1 ../../software/lib/crc16.c 
	@-${MV} ${OBJECTDIR}/_ext/1697446845/crc16.d ${OBJECTDIR}/_ext/1697446845/crc16.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/1697446845/crc16.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/lib/TendencyData.p1: lib/TendencyData.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/lib" 
	@${RM} ${OBJECTDIR}/lib/TendencyData.p1.d 
	@${RM} ${OBJECTDIR}/lib/TendencyData.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=+asm,+asmfile,+speed,-space,-debug,-local --addrqual=ignore --mode=pro -P -N255 -I"../lib" -I"stack" -I"." --warn=-3 --asmlist -DXPRJ_XC8=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"     -o${OBJECTDIR}/lib/TendencyData.p1 lib/TendencyData.c 
	@-${MV} ${OBJECTDIR}/lib/TendencyData.d ${OBJECTDIR}/lib/TendencyData.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/lib/TendencyData.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/lib/WeatherDataCollector.p1: lib/WeatherDataCollector.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/lib" 
	@${RM} ${OBJECTDIR}/lib/WeatherDataCollector.p1.d 
	@${RM} ${OBJECTDIR}/lib/WeatherDataCollector.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=+asm,+asmfile,+speed,-space,-debug,-local --addrqual=ignore --mode=pro -P -N255 -I"../lib" -I"stack" -I"." --warn=-3 --asmlist -DXPRJ_XC8=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"     -o${OBJECTDIR}/lib/WeatherDataCollector.p1 lib/WeatherDataCollector.c 
	@-${MV} ${OBJECTDIR}/lib/WeatherDataCollector.d ${OBJECTDIR}/lib/WeatherDataCollector.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/lib/WeatherDataCollector.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/net/ntp.p1: net/ntp.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/net" 
	@${RM} ${OBJECTDIR}/net/ntp.p1.d 
	@${RM} ${OBJECTDIR}/net/ntp.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=+asm,+asmfile,+speed,-space,-debug,-local --addrqual=ignore --mode=pro -P -N255 -I"../lib" -I"stack" -I"." --warn=-3 --asmlist -DXPRJ_XC8=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"     -o${OBJECTDIR}/net/ntp.p1 net/ntp.c 
	@-${MV} ${OBJECTDIR}/net/ntp.d ${OBJECTDIR}/net/ntp.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/net/ntp.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/net/wdata_dloader.p1: net/wdata_dloader.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/net" 
	@${RM} ${OBJECTDIR}/net/wdata_dloader.p1.d 
	@${RM} ${OBJECTDIR}/net/wdata_dloader.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=+asm,+asmfile,+speed,-space,-debug,-local --addrqual=ignore --mode=pro -P -N255 -I"../lib" -I"stack" -I"." --warn=-3 --asmlist -DXPRJ_XC8=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"     -o${OBJECTDIR}/net/wdata_dloader.p1 net/wdata_dloader.c 
	@-${MV} ${OBJECTDIR}/net/wdata_dloader.d ${OBJECTDIR}/net/wdata_dloader.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/net/wdata_dloader.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/net/wdata_parser.p1: net/wdata_parser.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/net" 
	@${RM} ${OBJECTDIR}/net/wdata_parser.p1.d 
	@${RM} ${OBJECTDIR}/net/wdata_parser.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=+asm,+asmfile,+speed,-space,-debug,-local --addrqual=ignore --mode=pro -P -N255 -I"../lib" -I"stack" -I"." --warn=-3 --asmlist -DXPRJ_XC8=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"     -o${OBJECTDIR}/net/wdata_parser.p1 net/wdata_parser.c 
	@-${MV} ${OBJECTDIR}/net/wdata_parser.d ${OBJECTDIR}/net/wdata_parser.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/net/wdata_parser.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/net/wdata.p1: net/wdata.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/net" 
	@${RM} ${OBJECTDIR}/net/wdata.p1.d 
	@${RM} ${OBJECTDIR}/net/wdata.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=+asm,+asmfile,+speed,-space,-debug,-local --addrqual=ignore --mode=pro -P -N255 -I"../lib" -I"stack" -I"." --warn=-3 --asmlist -DXPRJ_XC8=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"     -o${OBJECTDIR}/net/wdata.p1 net/wdata.c 
	@-${MV} ${OBJECTDIR}/net/wdata.d ${OBJECTDIR}/net/wdata.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/net/wdata.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sys/interrupt.p1: sys/interrupt.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/sys" 
	@${RM} ${OBJECTDIR}/sys/interrupt.p1.d 
	@${RM} ${OBJECTDIR}/sys/interrupt.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=+asm,+asmfile,+speed,-space,-debug,-local --addrqual=ignore --mode=pro -P -N255 -I"../lib" -I"stack" -I"." --warn=-3 --asmlist -DXPRJ_XC8=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"     -o${OBJECTDIR}/sys/interrupt.p1 sys/interrupt.c 
	@-${MV} ${OBJECTDIR}/sys/interrupt.d ${OBJECTDIR}/sys/interrupt.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sys/interrupt.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sys/keypad.p1: sys/keypad.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/sys" 
	@${RM} ${OBJECTDIR}/sys/keypad.p1.d 
	@${RM} ${OBJECTDIR}/sys/keypad.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=+asm,+asmfile,+speed,-space,-debug,-local --addrqual=ignore --mode=pro -P -N255 -I"../lib" -I"stack" -I"." --warn=-3 --asmlist -DXPRJ_XC8=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"     -o${OBJECTDIR}/sys/keypad.p1 sys/keypad.c 
	@-${MV} ${OBJECTDIR}/sys/keypad.d ${OBJECTDIR}/sys/keypad.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sys/keypad.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sys/pic.p1: sys/pic.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/sys" 
	@${RM} ${OBJECTDIR}/sys/pic.p1.d 
	@${RM} ${OBJECTDIR}/sys/pic.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=+asm,+asmfile,+speed,-space,-debug,-local --addrqual=ignore --mode=pro -P -N255 -I"../lib" -I"stack" -I"." --warn=-3 --asmlist -DXPRJ_XC8=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"     -o${OBJECTDIR}/sys/pic.p1 sys/pic.c 
	@-${MV} ${OBJECTDIR}/sys/pic.d ${OBJECTDIR}/sys/pic.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sys/pic.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sys/sensor.p1: sys/sensor.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/sys" 
	@${RM} ${OBJECTDIR}/sys/sensor.p1.d 
	@${RM} ${OBJECTDIR}/sys/sensor.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=+asm,+asmfile,+speed,-space,-debug,-local --addrqual=ignore --mode=pro -P -N255 -I"../lib" -I"stack" -I"." --warn=-3 --asmlist -DXPRJ_XC8=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"     -o${OBJECTDIR}/sys/sensor.p1 sys/sensor.c 
	@-${MV} ${OBJECTDIR}/sys/sensor.d ${OBJECTDIR}/sys/sensor.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sys/sensor.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sys/serial.p1: sys/serial.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/sys" 
	@${RM} ${OBJECTDIR}/sys/serial.p1.d 
	@${RM} ${OBJECTDIR}/sys/serial.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=+asm,+asmfile,+speed,-space,-debug,-local --addrqual=ignore --mode=pro -P -N255 -I"../lib" -I"stack" -I"." --warn=-3 --asmlist -DXPRJ_XC8=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"     -o${OBJECTDIR}/sys/serial.p1 sys/serial.c 
	@-${MV} ${OBJECTDIR}/sys/serial.d ${OBJECTDIR}/sys/serial.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sys/serial.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sys/settings.p1: sys/settings.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/sys" 
	@${RM} ${OBJECTDIR}/sys/settings.p1.d 
	@${RM} ${OBJECTDIR}/sys/settings.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=+asm,+asmfile,+speed,-space,-debug,-local --addrqual=ignore --mode=pro -P -N255 -I"../lib" -I"stack" -I"." --warn=-3 --asmlist -DXPRJ_XC8=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"     -o${OBJECTDIR}/sys/settings.p1 sys/settings.c 
	@-${MV} ${OBJECTDIR}/sys/settings.d ${OBJECTDIR}/sys/settings.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sys/settings.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sys/scheduler.p1: sys/scheduler.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/sys" 
	@${RM} ${OBJECTDIR}/sys/scheduler.p1.d 
	@${RM} ${OBJECTDIR}/sys/scheduler.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=+asm,+asmfile,+speed,-space,-debug,-local --addrqual=ignore --mode=pro -P -N255 -I"../lib" -I"stack" -I"." --warn=-3 --asmlist -DXPRJ_XC8=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"     -o${OBJECTDIR}/sys/scheduler.p1 sys/scheduler.c 
	@-${MV} ${OBJECTDIR}/sys/scheduler.d ${OBJECTDIR}/sys/scheduler.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sys/scheduler.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sys/tick.p1: sys/tick.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/sys" 
	@${RM} ${OBJECTDIR}/sys/tick.p1.d 
	@${RM} ${OBJECTDIR}/sys/tick.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=+asm,+asmfile,+speed,-space,-debug,-local --addrqual=ignore --mode=pro -P -N255 -I"../lib" -I"stack" -I"." --warn=-3 --asmlist -DXPRJ_XC8=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"     -o${OBJECTDIR}/sys/tick.p1 sys/tick.c 
	@-${MV} ${OBJECTDIR}/sys/tick.d ${OBJECTDIR}/sys/tick.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sys/tick.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/main.p1: main.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/main.p1.d 
	@${RM} ${OBJECTDIR}/main.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=+asm,+asmfile,+speed,-space,-debug,-local --addrqual=ignore --mode=pro -P -N255 -I"../lib" -I"stack" -I"." --warn=-3 --asmlist -DXPRJ_XC8=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"     -o${OBJECTDIR}/main.p1 main.c 
	@-${MV} ${OBJECTDIR}/main.d ${OBJECTDIR}/main.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/main.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/Utils.p1: Utils.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/Utils.p1.d 
	@${RM} ${OBJECTDIR}/Utils.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=+asm,+asmfile,+speed,-space,-debug,-local --addrqual=ignore --mode=pro -P -N255 -I"../lib" -I"stack" -I"." --warn=-3 --asmlist -DXPRJ_XC8=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"     -o${OBJECTDIR}/Utils.p1 Utils.c 
	@-${MV} ${OBJECTDIR}/Utils.d ${OBJECTDIR}/Utils.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/Utils.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/stack/ARP.p1: stack/ARP.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/stack" 
	@${RM} ${OBJECTDIR}/stack/ARP.p1.d 
	@${RM} ${OBJECTDIR}/stack/ARP.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=+asm,+asmfile,+speed,-space,-debug,-local --addrqual=ignore --mode=pro -P -N255 -I"../lib" -I"stack" -I"." --warn=-3 --asmlist -DXPRJ_XC8=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"     -o${OBJECTDIR}/stack/ARP.p1 stack/ARP.c 
	@-${MV} ${OBJECTDIR}/stack/ARP.d ${OBJECTDIR}/stack/ARP.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/stack/ARP.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/stack/BigInt.p1: stack/BigInt.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/stack" 
	@${RM} ${OBJECTDIR}/stack/BigInt.p1.d 
	@${RM} ${OBJECTDIR}/stack/BigInt.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=+asm,+asmfile,+speed,-space,-debug,-local --addrqual=ignore --mode=pro -P -N255 -I"../lib" -I"stack" -I"." --warn=-3 --asmlist -DXPRJ_XC8=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"     -o${OBJECTDIR}/stack/BigInt.p1 stack/BigInt.c 
	@-${MV} ${OBJECTDIR}/stack/BigInt.d ${OBJECTDIR}/stack/BigInt.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/stack/BigInt.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/stack/Delay.p1: stack/Delay.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/stack" 
	@${RM} ${OBJECTDIR}/stack/Delay.p1.d 
	@${RM} ${OBJECTDIR}/stack/Delay.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=+asm,+asmfile,+speed,-space,-debug,-local --addrqual=ignore --mode=pro -P -N255 -I"../lib" -I"stack" -I"." --warn=-3 --asmlist -DXPRJ_XC8=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"     -o${OBJECTDIR}/stack/Delay.p1 stack/Delay.c 
	@-${MV} ${OBJECTDIR}/stack/Delay.d ${OBJECTDIR}/stack/Delay.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/stack/Delay.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/stack/DHCP.p1: stack/DHCP.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/stack" 
	@${RM} ${OBJECTDIR}/stack/DHCP.p1.d 
	@${RM} ${OBJECTDIR}/stack/DHCP.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=+asm,+asmfile,+speed,-space,-debug,-local --addrqual=ignore --mode=pro -P -N255 -I"../lib" -I"stack" -I"." --warn=-3 --asmlist -DXPRJ_XC8=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"     -o${OBJECTDIR}/stack/DHCP.p1 stack/DHCP.c 
	@-${MV} ${OBJECTDIR}/stack/DHCP.d ${OBJECTDIR}/stack/DHCP.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/stack/DHCP.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/stack/DNS.p1: stack/DNS.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/stack" 
	@${RM} ${OBJECTDIR}/stack/DNS.p1.d 
	@${RM} ${OBJECTDIR}/stack/DNS.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=+asm,+asmfile,+speed,-space,-debug,-local --addrqual=ignore --mode=pro -P -N255 -I"../lib" -I"stack" -I"." --warn=-3 --asmlist -DXPRJ_XC8=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"     -o${OBJECTDIR}/stack/DNS.p1 stack/DNS.c 
	@-${MV} ${OBJECTDIR}/stack/DNS.d ${OBJECTDIR}/stack/DNS.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/stack/DNS.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/stack/ENC28J60.p1: stack/ENC28J60.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/stack" 
	@${RM} ${OBJECTDIR}/stack/ENC28J60.p1.d 
	@${RM} ${OBJECTDIR}/stack/ENC28J60.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=+asm,+asmfile,+speed,-space,-debug,-local --addrqual=ignore --mode=pro -P -N255 -I"../lib" -I"stack" -I"." --warn=-3 --asmlist -DXPRJ_XC8=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"     -o${OBJECTDIR}/stack/ENC28J60.p1 stack/ENC28J60.c 
	@-${MV} ${OBJECTDIR}/stack/ENC28J60.d ${OBJECTDIR}/stack/ENC28J60.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/stack/ENC28J60.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/stack/Hashes.p1: stack/Hashes.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/stack" 
	@${RM} ${OBJECTDIR}/stack/Hashes.p1.d 
	@${RM} ${OBJECTDIR}/stack/Hashes.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=+asm,+asmfile,+speed,-space,-debug,-local --addrqual=ignore --mode=pro -P -N255 -I"../lib" -I"stack" -I"." --warn=-3 --asmlist -DXPRJ_XC8=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"     -o${OBJECTDIR}/stack/Hashes.p1 stack/Hashes.c 
	@-${MV} ${OBJECTDIR}/stack/Hashes.d ${OBJECTDIR}/stack/Hashes.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/stack/Hashes.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/stack/Helpers.p1: stack/Helpers.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/stack" 
	@${RM} ${OBJECTDIR}/stack/Helpers.p1.d 
	@${RM} ${OBJECTDIR}/stack/Helpers.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=+asm,+asmfile,+speed,-space,-debug,-local --addrqual=ignore --mode=pro -P -N255 -I"../lib" -I"stack" -I"." --warn=-3 --asmlist -DXPRJ_XC8=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"     -o${OBJECTDIR}/stack/Helpers.p1 stack/Helpers.c 
	@-${MV} ${OBJECTDIR}/stack/Helpers.d ${OBJECTDIR}/stack/Helpers.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/stack/Helpers.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/stack/ICMP.p1: stack/ICMP.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/stack" 
	@${RM} ${OBJECTDIR}/stack/ICMP.p1.d 
	@${RM} ${OBJECTDIR}/stack/ICMP.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=+asm,+asmfile,+speed,-space,-debug,-local --addrqual=ignore --mode=pro -P -N255 -I"../lib" -I"stack" -I"." --warn=-3 --asmlist -DXPRJ_XC8=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"     -o${OBJECTDIR}/stack/ICMP.p1 stack/ICMP.c 
	@-${MV} ${OBJECTDIR}/stack/ICMP.d ${OBJECTDIR}/stack/ICMP.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/stack/ICMP.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/stack/IP.p1: stack/IP.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/stack" 
	@${RM} ${OBJECTDIR}/stack/IP.p1.d 
	@${RM} ${OBJECTDIR}/stack/IP.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=+asm,+asmfile,+speed,-space,-debug,-local --addrqual=ignore --mode=pro -P -N255 -I"../lib" -I"stack" -I"." --warn=-3 --asmlist -DXPRJ_XC8=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"     -o${OBJECTDIR}/stack/IP.p1 stack/IP.c 
	@-${MV} ${OBJECTDIR}/stack/IP.d ${OBJECTDIR}/stack/IP.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/stack/IP.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/stack/SNTP.p1: stack/SNTP.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/stack" 
	@${RM} ${OBJECTDIR}/stack/SNTP.p1.d 
	@${RM} ${OBJECTDIR}/stack/SNTP.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=+asm,+asmfile,+speed,-space,-debug,-local --addrqual=ignore --mode=pro -P -N255 -I"../lib" -I"stack" -I"." --warn=-3 --asmlist -DXPRJ_XC8=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"     -o${OBJECTDIR}/stack/SNTP.p1 stack/SNTP.c 
	@-${MV} ${OBJECTDIR}/stack/SNTP.d ${OBJECTDIR}/stack/SNTP.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/stack/SNTP.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/stack/SPIEEPROM.p1: stack/SPIEEPROM.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/stack" 
	@${RM} ${OBJECTDIR}/stack/SPIEEPROM.p1.d 
	@${RM} ${OBJECTDIR}/stack/SPIEEPROM.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=+asm,+asmfile,+speed,-space,-debug,-local --addrqual=ignore --mode=pro -P -N255 -I"../lib" -I"stack" -I"." --warn=-3 --asmlist -DXPRJ_XC8=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"     -o${OBJECTDIR}/stack/SPIEEPROM.p1 stack/SPIEEPROM.c 
	@-${MV} ${OBJECTDIR}/stack/SPIEEPROM.d ${OBJECTDIR}/stack/SPIEEPROM.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/stack/SPIEEPROM.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/stack/SPIFlash.p1: stack/SPIFlash.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/stack" 
	@${RM} ${OBJECTDIR}/stack/SPIFlash.p1.d 
	@${RM} ${OBJECTDIR}/stack/SPIFlash.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=+asm,+asmfile,+speed,-space,-debug,-local --addrqual=ignore --mode=pro -P -N255 -I"../lib" -I"stack" -I"." --warn=-3 --asmlist -DXPRJ_XC8=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"     -o${OBJECTDIR}/stack/SPIFlash.p1 stack/SPIFlash.c 
	@-${MV} ${OBJECTDIR}/stack/SPIFlash.d ${OBJECTDIR}/stack/SPIFlash.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/stack/SPIFlash.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/stack/SPIRAM.p1: stack/SPIRAM.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/stack" 
	@${RM} ${OBJECTDIR}/stack/SPIRAM.p1.d 
	@${RM} ${OBJECTDIR}/stack/SPIRAM.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=+asm,+asmfile,+speed,-space,-debug,-local --addrqual=ignore --mode=pro -P -N255 -I"../lib" -I"stack" -I"." --warn=-3 --asmlist -DXPRJ_XC8=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"     -o${OBJECTDIR}/stack/SPIRAM.p1 stack/SPIRAM.c 
	@-${MV} ${OBJECTDIR}/stack/SPIRAM.d ${OBJECTDIR}/stack/SPIRAM.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/stack/SPIRAM.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/stack/StackTsk.p1: stack/StackTsk.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/stack" 
	@${RM} ${OBJECTDIR}/stack/StackTsk.p1.d 
	@${RM} ${OBJECTDIR}/stack/StackTsk.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=+asm,+asmfile,+speed,-space,-debug,-local --addrqual=ignore --mode=pro -P -N255 -I"../lib" -I"stack" -I"." --warn=-3 --asmlist -DXPRJ_XC8=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"     -o${OBJECTDIR}/stack/StackTsk.p1 stack/StackTsk.c 
	@-${MV} ${OBJECTDIR}/stack/StackTsk.d ${OBJECTDIR}/stack/StackTsk.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/stack/StackTsk.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/stack/TCP.p1: stack/TCP.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/stack" 
	@${RM} ${OBJECTDIR}/stack/TCP.p1.d 
	@${RM} ${OBJECTDIR}/stack/TCP.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=+asm,+asmfile,+speed,-space,-debug,-local --addrqual=ignore --mode=pro -P -N255 -I"../lib" -I"stack" -I"." --warn=-3 --asmlist -DXPRJ_XC8=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"     -o${OBJECTDIR}/stack/TCP.p1 stack/TCP.c 
	@-${MV} ${OBJECTDIR}/stack/TCP.d ${OBJECTDIR}/stack/TCP.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/stack/TCP.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/stack/Telnet.p1: stack/Telnet.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/stack" 
	@${RM} ${OBJECTDIR}/stack/Telnet.p1.d 
	@${RM} ${OBJECTDIR}/stack/Telnet.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=+asm,+asmfile,+speed,-space,-debug,-local --addrqual=ignore --mode=pro -P -N255 -I"../lib" -I"stack" -I"." --warn=-3 --asmlist -DXPRJ_XC8=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"     -o${OBJECTDIR}/stack/Telnet.p1 stack/Telnet.c 
	@-${MV} ${OBJECTDIR}/stack/Telnet.d ${OBJECTDIR}/stack/Telnet.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/stack/Telnet.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/stack/Tick.p1: stack/Tick.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/stack" 
	@${RM} ${OBJECTDIR}/stack/Tick.p1.d 
	@${RM} ${OBJECTDIR}/stack/Tick.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=+asm,+asmfile,+speed,-space,-debug,-local --addrqual=ignore --mode=pro -P -N255 -I"../lib" -I"stack" -I"." --warn=-3 --asmlist -DXPRJ_XC8=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"     -o${OBJECTDIR}/stack/Tick.p1 stack/Tick.c 
	@-${MV} ${OBJECTDIR}/stack/Tick.d ${OBJECTDIR}/stack/Tick.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/stack/Tick.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/stack/UDP.p1: stack/UDP.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/stack" 
	@${RM} ${OBJECTDIR}/stack/UDP.p1.d 
	@${RM} ${OBJECTDIR}/stack/UDP.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=+asm,+asmfile,+speed,-space,-debug,-local --addrqual=ignore --mode=pro -P -N255 -I"../lib" -I"stack" -I"." --warn=-3 --asmlist -DXPRJ_XC8=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"     -o${OBJECTDIR}/stack/UDP.p1 stack/UDP.c 
	@-${MV} ${OBJECTDIR}/stack/UDP.d ${OBJECTDIR}/stack/UDP.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/stack/UDP.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: assemble
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
else
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: link
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
dist/${CND_CONF}/${IMAGE_TYPE}/EthernetWS2.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}: ${OBJECTFILES}  nbproject/Makefile-${CND_CONF}.mk    
	@${MKDIR} dist/${CND_CONF}/${IMAGE_TYPE} 
	${MP_CC} $(MP_EXTRA_LD_PRE) --chip=$(MP_PROCESSOR_OPTION) -G -mdist/${CND_CONF}/${IMAGE_TYPE}/EthernetWS2.X.${IMAGE_TYPE}.map  -D__DEBUG=1  --debugger=pickit3  -DXPRJ_XC8=$(CND_CONF)  --double=24 --float=24 --emi=wordwrite --opt=+asm,+asmfile,+speed,-space,-debug,-local --addrqual=ignore --mode=pro -P -N255 -I"../lib" -I"stack" -I"." --warn=-3 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"     --rom=default,-17d30-17fff --ram=default,-cf4-cff,-f9c-f9c,-fd4-fd4,-fdb-fdf,-fe3-fe7,-feb-fef,-ffd-fff  $(COMPARISON_BUILD) --memorysummary dist/${CND_CONF}/${IMAGE_TYPE}/memoryfile.xml -odist/${CND_CONF}/${IMAGE_TYPE}/EthernetWS2.X.${IMAGE_TYPE}.${DEBUGGABLE_SUFFIX}  ${OBJECTFILES_QUOTED_IF_SPACED}     
	@${RM} dist/${CND_CONF}/${IMAGE_TYPE}/EthernetWS2.X.${IMAGE_TYPE}.hex 
	
else
dist/${CND_CONF}/${IMAGE_TYPE}/EthernetWS2.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}: ${OBJECTFILES}  nbproject/Makefile-${CND_CONF}.mk   
	@${MKDIR} dist/${CND_CONF}/${IMAGE_TYPE} 
	${MP_CC} $(MP_EXTRA_LD_PRE) --chip=$(MP_PROCESSOR_OPTION) -G -mdist/${CND_CONF}/${IMAGE_TYPE}/EthernetWS2.X.${IMAGE_TYPE}.map  -DXPRJ_XC8=$(CND_CONF)  --double=24 --float=24 --emi=wordwrite --opt=+asm,+asmfile,+speed,-space,-debug,-local --addrqual=ignore --mode=pro -P -N255 -I"../lib" -I"stack" -I"." --warn=-3 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"     $(COMPARISON_BUILD) --memorysummary dist/${CND_CONF}/${IMAGE_TYPE}/memoryfile.xml -odist/${CND_CONF}/${IMAGE_TYPE}/EthernetWS2.X.${IMAGE_TYPE}.${DEBUGGABLE_SUFFIX}  ${OBJECTFILES_QUOTED_IF_SPACED}     
	
endif


# Subprojects
.build-subprojects:


# Subprojects
.clean-subprojects:

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r build/XC8
	${RM} -r dist/XC8

# Enable dependency checking
.dep.inc: .depcheck-impl

DEPFILES=$(shell mplabwildcard ${POSSIBLE_DEPFILES})
ifneq (${DEPFILES},)
include ${DEPFILES}
endif
