//
// Ethernet Weather Station 2
// Utilities
//
// Author: Tamas Karpati
// This file was created on 2014-08-19
//

#include "Utils.h"
#include "net/ntp.h"
#include "net/wdata.h"
#include "sys/settings.h"

#include <time.h>


bool Utils_IsInTimeRange(uint8_t startHour, uint8_t startMin, uint8_t endHour, uint8_t endMin)
{
    struct tm *t = Utils_CurrentTime();

    uint16_t start = (startHour << 8) + startMin;
    uint16_t end = (endHour << 8) + endMin;
    uint16_t current = (t->tm_hour << 8) + t->tm_min;

    if (start < end && current >= start && current < end)
        return 1;
    else if (start >= end && current >= end && current < start)
        return 1;
    else
        return 0;
}


struct tm* Utils_CurrentTime()
{
    time_t local = NTPGetTime();

    //local += ClampDefault(WDataReadNumber(WD_LOCAL_GMTDIFF, 0).Int8, 1, 2, 0) * 3600ul;
    if (settings.DateTime.TimeZoneOffsetHours != 0)
        local += (time_t)settings.DateTime.TimeZoneOffsetHours * 3600ul;

    struct tm *t = gmtime(&local);

    bool dst = Utils_IsSummerTime(t);

    if (dst) {
        local += 3600ul;
        t = gmtime(&local);
    }

    return t;
}


bool Utils_IsSummerTime(const struct tm* t)
{
    if (t->tm_mon < 2 || t->tm_mon > 9)
        return 0;

    if (t->tm_mon > 2 && t->tm_mon < 9)
        return 1;

    int previousSunday = t->tm_mday - t->tm_wday;

    if (t->tm_mon == 2)
        return previousSunday >= 25;

    if (t->tm_mon == 9)
        return previousSunday < 25;

    return 0;
}