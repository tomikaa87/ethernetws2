/**
 * Ethernet Weather Station 2 Firmware
 * Text drawing module
 *
 * @author Tamas Karpati <tomikaa87>
 * @date 2012-10-29
 */

#ifndef TEXT_H
#define	TEXT_H

#include <stdint.h>

typedef enum {
    FONT_PF_TEMPESTA_BOLD,
    FONT_LCD_LARGE,
    FONT_LCD_SMALL
} FontFamily;

typedef enum {
    TF_RIGHT_ALIGNED    = (1 << 6),
    TF_INVERTED         = (1 << 7)
} TextFlags;

void Text_DrawDefault(uint8_t x, uint8_t y, const char *text, TextFlags flags);
void Text_DrawCustom(uint8_t x, uint8_t y, const char *text,
                     FontFamily font, TextFlags flags);

#endif	/* TEXT_H */

