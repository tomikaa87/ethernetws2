//
// Ethernet Weather Station 2
// Scrollable graphic menu with multiple type items
//
// Author: Tamas Karpati
// This file was created on 2014-08-20
//

#ifndef MENU_H
#define MENU_H

#include <stdint.h>

typedef enum {
    ITEM_SIMPLE,
    ITEM_INT,
    ITEM_PROGRESS,
    ITEM_IPADDR,
    ITEM_TIME,
    ITEM_TOGGLE,
    ITEM_SEPARATOR
} MenuItemType;

typedef void (*ValueChangedCallback)(int32_t value);

typedef struct {
    const char *Text;
    MenuItemType Type;
    ValueChangedCallback Callback;
    int32_t Value;
    int32_t Min;
    int32_t Max;
    //    uint8_t GroupID;
} MenuItem;

typedef struct {
    const char *Title;
    MenuItem **Items;
    uint8_t ItemCount;

    uint8_t _currentItemY;
    uint8_t _selectionIndex;
    uint8_t _indexOffset;
    int8_t _subFieldIndex;
    int32_t _currentFieldOriginalValue;
} Menu;

// Using macros instead of functions saves large amount of space in these cases
#define MenuInit(menu, title, items, itemCount) { \
    menu.Title = title; \
    menu.Items = items; \
    menu.ItemCount = itemCount; \
    MenuReset(menu); \
}

#define MenuReset(menu) { \
    menu._currentItemY = 0; \
    menu._indexOffset = 0; \
    menu._selectionIndex = 0; \
    menu._subFieldIndex = -1; \
    while (menu.Items[menu._selectionIndex]->Type == ITEM_SEPARATOR) menu._selectionIndex++;\
}

void Menu_InitItem(MenuItem *item, const char *text, MenuItemType type,
                   int32_t min, int32_t max, int32_t initial, ValueChangedCallback callback);
void Menu_Draw(Menu *menu);
void Menu_KeyPress(Menu *menu, uint8_t key);

#endif // MENU_H
