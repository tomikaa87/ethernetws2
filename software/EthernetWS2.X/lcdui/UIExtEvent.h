/*
 * File:   UIExtEvent.h
 * Author: tomikaa
 *
 * Created on 2018. december 23., 14:06
 */

#ifndef UIEXTEVENT_H
#define	UIEXTEVENT_H

typedef enum {
    UI_EV_TIMER_TICK,
    UI_EV_SENSOR_DATA_UPDATED,
    UI_EV_CLOCK_UPDATED,
    UI_EV_WDATA_UPDATED
} UIExtEvent;

#endif	/* UIEXTEVENT_H */

