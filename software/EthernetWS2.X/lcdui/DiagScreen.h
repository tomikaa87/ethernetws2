/*
 * File:   DiagScreen.h
 * Author: tomikaa
 *
 * Created on 2018. december 23., 15:10
 */

#ifndef DIAGSCREEN_H
#define	DIAGSCREEN_H

#include "screen.h"

#include <stdint.h>

void DiagScreen_Init();
void DiagScreen_Draw();
ScreenID DiagScreen_KeyPress(uint16_t keyCode);

#endif	/* DIAGSCREEN_H */

