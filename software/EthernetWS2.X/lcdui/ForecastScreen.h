//
// Ethernet Weather Station 2
// LCDUI Forecast screen
//
// Author: Tamas Karpati
// This file was created on 2014-02-05
//

#ifndef FORECASTSCREEN_H
#define	FORECASTSCREEN_H

#include "screen.h"

#include <stdint.h>

extern uint8_t g_forecastScreenDayNumber;

void ForecastScreen_Init();
void ForecastScreen_Draw();
ScreenID ForecastScreen_KeyPress(uint16_t keyCode);
void ForecastScreen_Update();
void ForecastScreen_Task();

#define ForecastScreenSetDay(day)       g_forecastScreenDayNumber = day

#endif	/* FORECASTSCREEN_H */

