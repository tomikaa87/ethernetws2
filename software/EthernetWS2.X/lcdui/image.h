/**
 * Ethernet Weather Station 2 Firmware
 * Image drawing module
 *
 * @author Tamas Karpati <tomikaa87>
 * @date 2013. febru�r 25.
 */

#ifndef IMAGE_H
#define IMAGE_H

#include <stdint.h>

typedef enum {
    SI_NET_OFFLINE = 0,
    SI_NET_ONLINE,
    SI_SENSOR_OFFLINE,
    SI_SENSOR_ONLINE
} StatusIcon;

void Image_DrawFullScreen(uint16_t imageAddress);

void Image_DrawWeatherIcon(uint8_t x, uint8_t y, uint8_t icon);
void Image_DrawStatusIcon(uint8_t x, uint8_t y, StatusIcon icon);
void Image_DrawWarningIcon(uint8_t x, uint8_t y, uint8_t icon);

#endif  /* IMAGE_H */

