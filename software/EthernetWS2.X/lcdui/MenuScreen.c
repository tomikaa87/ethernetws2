//
// Ethernet Weather Station 2
// Main menu screen
//
// Author: Tamas Karpati
// This file was created on 2014-09-04
//

#include "MenuScreen.h"
#include "Menu.h"
#include "screen.h"
#include "sys/keypad.h"
#include "config.h"
#include "sys/settings.h"
#include "lcdui.h"

#include <stddef.h>

//--------------------------------------------------------------------

static struct {

    struct {
        MenuItem group;
        MenuItem backlightNormalLevel;
        MenuItem backlightDimLevel;
        MenuItem backlightAutoControl;
        MenuItem backlightNormalLevelStart;
        MenuItem backlightDimLevelStart;
    } display;

    struct {
        MenuItem group;
        MenuItem updateInterval;
        MenuItem tendencyUpdateInterval;
    } weatherData;

    struct {
        MenuItem group;
        MenuItem timeZoneOffset;
    } dateTime;

    struct {
        MenuItem group;
        MenuItem saveSettings;
        MenuItem loadSettings;
        MenuItem restoreDefaults;
    } settings;

#ifdef ENABLE_NETWORK_CONFIG

    struct {
        MenuItem group;
        MenuItem useDHCP;
        MenuItem staticIP;
        MenuItem subnetMask;
        MenuItem defaultGateway;
        MenuItem useHTTPProxy;
        MenuItem proxyIP;
        MenuItem proxyPort;
    } network;
#endif

    struct {
        MenuItem group;
        MenuItem diagScreen;
    } debug;
} g_items;


static void displayBacklightNormalLevelChanged(int32_t value);
static void displayBacklightDimLevelChanged(int32_t value);
static void displayAutoBlControlChanged(int32_t value);
static void displayBacklightDimStartChanged(int32_t value);
static void displayBacklightDimEndChanged(int32_t value);
static void weatherDataUpdateIntervalChanged(int32_t value);
static void weatherTendencyUpdateIntervalChanged(int32_t value);
static void timeZoneOffsetChanged(int32_t value);
static void settingsSaveTriggered(int32_t _);
static void settingsLoadTriggered(int32_t _);
static void settingsRestoreDefaultsTriggered(int32_t _);

static MenuItem *g_itemPointers[] = {
    &g_items.display.group,
    &g_items.display.backlightNormalLevel,
    &g_items.display.backlightDimLevel,
    &g_items.display.backlightAutoControl,
    &g_items.display.backlightNormalLevelStart,
    &g_items.display.backlightDimLevelStart,

#ifdef ENABLE_NETWORK_CONFIG
    &g_items.network.group,
    &g_items.network.useDHCP,
    &g_items.network.staticIP,
    &g_items.network.subnetMask,
    &g_items.network.defaultGateway,
    &g_items.network.useHTTPProxy,
    &g_items.network.proxyIP,
    &g_items.network.proxyPort,
#endif

    &g_items.dateTime.group,
    &g_items.dateTime.timeZoneOffset,

    &g_items.weatherData.group,
    &g_items.weatherData.updateInterval,
    &g_items.weatherData.tendencyUpdateInterval,

    &g_items.settings.group,
    &g_items.settings.saveSettings,
    &g_items.settings.loadSettings,
    &g_items.settings.restoreDefaults,

    &g_items.debug.group,
    &g_items.debug.diagScreen
};

static Menu g_menu;

//--------------------------------------------------------------------


void MenuScreen_Init()
{
    Menu_InitItem(&g_items.display.group, " Display ", ITEM_SEPARATOR, 0, 0, 0, NULL);

    Menu_InitItem(&g_items.display.backlightNormalLevel, "Backlight normal level", ITEM_PROGRESS,
                  0, 100, settings.Display.NormalLevel, displayBacklightNormalLevelChanged);
    Menu_InitItem(&g_items.display.backlightDimLevel, "Backlight dim level", ITEM_PROGRESS,
                  0, 100, settings.Display.DimLevel, displayBacklightDimLevelChanged);
    Menu_InitItem(&g_items.display.backlightAutoControl, "Automatic backlight control", ITEM_TOGGLE,
                  0, 1, settings.Display.Flags.AutoBacklightControl, displayAutoBlControlChanged); // TODO handle flags
    Menu_InitItem(&g_items.display.backlightNormalLevelStart, "Stop dimming at:", ITEM_TIME,
                  0, 0, settings.Display.DimmingEnd, displayBacklightDimEndChanged);
    Menu_InitItem(&g_items.display.backlightDimLevelStart, "Start dimming at:", ITEM_TIME,
                  0, 0, settings.Display.DimmingStart, displayBacklightDimStartChanged);

    Menu_InitItem(&g_items.weatherData.group, " Weather data ", ITEM_SEPARATOR, 0, 0, 0, NULL);

    Menu_InitItem(&g_items.weatherData.updateInterval, "Update interval (min)", ITEM_INT,
                  1, 60, settings.Weather.UpdateIntervalMinutes, weatherDataUpdateIntervalChanged);
    Menu_InitItem(&g_items.weatherData.tendencyUpdateInterval, "Tendency upd. intval. (min)", ITEM_INT,
                  1, 240, settings.Weather.TendencyUpdateIntervalMinutes, weatherTendencyUpdateIntervalChanged);

#ifdef ENABLE_NETWORK_CONFIG
    Menu_InitItem(&g_items.network.group, " Network ", ITEM_SEPARATOR, 0, 0, 0, NULL);
    Menu_InitItem(&g_items.network.useDHCP, "Use DHCP", ITEM_TOGGLE, 1, 0, 1, NULL);
    Menu_InitItem(&g_items.network.staticIP, "Static IP address", ITEM_IPADDR, 0x1e01a8c0l, 0, 0, NULL);
    Menu_InitItem(&g_items.network.subnetMask, "Subnet mask", ITEM_IPADDR, 0x00ffffffl, 0, 0, NULL);
    Menu_InitItem(&g_items.network.defaultGateway, "Default gateway", ITEM_IPADDR, 0x0101a8c0l, 0, 0, NULL);
    Menu_InitItem(&g_items.network.useHTTPProxy, "Use HTTP proxy", ITEM_TOGGLE, 0, 0, 1, NULL);
    Menu_InitItem(&g_items.network.proxyIP, "Proxy IP address", ITEM_IPADDR, 0x00000000l, 0, 0, NULL);
    Menu_InitItem(&g_items.network.proxyPort, "Proxy port", ITEM_INT, 8080, 0, 65535, NULL);
#endif

    Menu_InitItem(&g_items.dateTime.group, " Date/Time ", ITEM_SEPARATOR, 0, 0, 0, NULL);

    Menu_InitItem(&g_items.dateTime.timeZoneOffset, "Time zone offset (h)", ITEM_INT,
                  -12, 12, settings.DateTime.TimeZoneOffsetHours, timeZoneOffsetChanged);

    Menu_InitItem(&g_items.settings.group, " Save/Load ", ITEM_SEPARATOR, 0, 0, 0, NULL);

    Menu_InitItem(&g_items.settings.saveSettings, "Save settings", ITEM_SIMPLE, 0, 0, 0, settingsSaveTriggered);
    Menu_InitItem(&g_items.settings.loadSettings, "Load settings", ITEM_SIMPLE, 0, 0, 0, settingsLoadTriggered);
    Menu_InitItem(&g_items.settings.restoreDefaults, "Restore defaults", ITEM_SIMPLE, 0, 0, 0, settingsRestoreDefaultsTriggered);

    Menu_InitItem(&g_items.debug.group, "Debug", ITEM_SEPARATOR, 0, 0, 0, NULL);

    Menu_InitItem(&g_items.debug.diagScreen, "Diagnostics", ITEM_SIMPLE, 0, 0, 0, NULL);

    MenuInit(g_menu, "Weather Station Settings", g_itemPointers, sizeof (g_itemPointers) / sizeof (g_itemPointers[0]));
}


void MenuScreen_Draw()
{
    Menu_Draw(&g_menu);
}


ScreenID MenuScreen_KeyPress(uint16_t keyCode)
{
    switch (keyCode) {
    case KEY_CANCEL:
        return SCR_MAIN;

    case KEY_ENTER:
        // FIXME don't use wired-in index value
        if (g_menu._selectionIndex == 16)
            return SCR_DIAG;
        // Fall-through

    default:
        Menu_KeyPress(&g_menu, keyCode);
        return 255;
    }
}


static void displayBacklightNormalLevelChanged(int32_t value)
{
    settings.Display.NormalLevel = value;
}


static void displayBacklightDimLevelChanged(int32_t value)
{
    settings.Display.DimLevel = value;
}


static void displayAutoBlControlChanged(int32_t value)
{
    settings.Display.Flags.AutoBacklightControl = value;
}


static void displayBacklightDimStartChanged(int32_t value)
{
    settings.Display.DimmingStart = value;
}


static void displayBacklightDimEndChanged(int32_t value)
{
    settings.Display.DimmingEnd = value;
}


static void weatherDataUpdateIntervalChanged(int32_t value)
{
    settings.Weather.UpdateIntervalMinutes = value;
}


static void weatherTendencyUpdateIntervalChanged(int32_t value)
{
    settings.Weather.TendencyUpdateIntervalMinutes = value;
}


static void timeZoneOffsetChanged(int32_t value)
{
    settings.DateTime.TimeZoneOffsetHours = value;
}


static void settingsSaveTriggered(int32_t _)
{
    Settings_Save();
}


static void settingsLoadTriggered(int32_t _)
{
    Settings_Load();
}


static void settingsRestoreDefaultsTriggered(int32_t _)
{
    Settings_RestoreDefaults();
}