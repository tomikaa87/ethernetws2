/**
 * Ethernet Weather Station 2 Firmware
 * Image drawing module
 *
 * @author Tamas Karpati <tomikaa87>
 * @date 2013. febru�r 25.
 */

#include "image.h"
#include "driver/graphlcd.h"
#include "driver/eeprom_25lc.h"
#include "hardware_profile.h"
#include "driver/graphlcd.h"

//------------------------------------------------------------------------------
// Utility macros
//------------------------------------------------------------------------------

#define DrawIconFromEEPROM(x, y, icon, \
                           iconSetIconWidth, \
                           iconSetIconHeight, \
                           iconSetIconBoundary, \
                           iconSetIconCount, \
                           iconSetBPL, \
                           iconSetLineBufSize, \
                           baseAddress) { \
    if (icon >= iconSetIconCount) return; \
    uint8_t _HEIGHT = iconSetIconHeight; \
    uint24_t _ADDR = baseAddress; _ADDR += (icon) * (iconSetIconBoundary / 8); \
    uint8_t _BUF[iconSetLineBufSize]; \
    while (_HEIGHT--) { \
        /* Read a whole line of the icon */ \
        EEPROM_ReadBuffer(_ADDR, _BUF, iconSetLineBufSize); \
        /* Write display buffer data and mask bits */ \
        GLCD_CopyToBuffer(x, y++, _BUF, iconSetLineBufSize, \
                (uint8_t)((iconSetIconBoundary - iconSetIconWidth) & 0x7f)); \
        _ADDR += iconSetBPL; \
    } \
}

//------------------------------------------------------------------------------
// API functions
//------------------------------------------------------------------------------


void Image_DrawWeatherIcon(uint8_t x, uint8_t y, uint8_t icon)
{
#define IconSetIconWidth    70
#define IconSetIconHeight   70
#define IconSetIconBoundary 72
#define IconSetIconCount    44

#define IconSetBPL          IconSetIconCount * IconSetIconBoundary / 8
#define IconSetLineBufSize  IconSetIconBoundary / 8

    DrawIconFromEEPROM(x, y, icon - 1,
                       IconSetIconWidth,
                       IconSetIconHeight,
                       IconSetIconBoundary,
                       IconSetIconCount,
                       IconSetBPL,
                       IconSetLineBufSize,
                       EEPROM_LOC_ICON_SET_1);
}

//--------------------------------------------------------------------


void Image_DrawStatusIcon(uint8_t x, uint8_t y, StatusIcon icon)
{
#define IconSetIconWidth    8
#define IconSetIconHeight   11
#define IconSetIconBoundary 8
#define IconSetIconCount    4

#define IconSetBPL          IconSetIconCount * IconSetIconBoundary / 8
#define IconSetLineBufSize  IconSetIconBoundary / 8

    DrawIconFromEEPROM(x, y, (uint8_t)icon,
                       IconSetIconWidth,
                       IconSetIconHeight,
                       IconSetIconBoundary,
                       IconSetIconCount,
                       IconSetBPL,
                       IconSetLineBufSize,
                       EEPROM_LOC_STATUS_ICONS);
}

//--------------------------------------------------------------------


void Image_DrawWarningIcon(uint8_t x, uint8_t y, uint8_t icon)
{
#define IconSetIconWidth    11
#define IconSetIconHeight   10
#define IconSetIconBoundary 16
#define IconSetIconCount    2

#define IconSetBPL          IconSetIconCount * IconSetIconBoundary / 8
#define IconSetLineBufSize  IconSetIconBoundary / 8

    DrawIconFromEEPROM(x, y, (uint8_t)icon,
                       IconSetIconWidth,
                       IconSetIconHeight,
                       IconSetIconBoundary,
                       IconSetIconCount,
                       IconSetBPL,
                       IconSetLineBufSize,
                       EEPROM_LOC_WARNING_ICONS);
}

//--------------------------------------------------------------------


void Image_DrawFullScreen(uint16_t imageAddress)
{
    for (uint16_t i = imageAddress, j = 0; i < imageAddress + 2048; ++i, ++j)
        GLCD_SetBufferByte(j, EEPROM_ReadByte(i), GLCD_DRAW_MODE_OR);
}

//------------------------------------------------------------------------------
// Private functions
//------------------------------------------------------------------------------
