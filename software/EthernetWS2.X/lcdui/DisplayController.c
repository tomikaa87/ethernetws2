//
// Ethernet Weather Station 2
// Display Controller
//
// Author: Tamas Karpati
// This file was created on 2014-08-19
//

#include "DisplayController.h"
#include "Utils.h"
#include "sys/pic.h"
#include "sys/settings.h"

#include <time.h>

bit g_autoBacklightControl = 0;

//--------------------------------------------------------------------


void DisplayController_Task()
{
    uint8_t normalStartHours = (settings.Display.DimmingEnd & 0xff);
    uint8_t normalStartMinutes = (settings.Display.DimmingEnd >> 8) & 0xff;
    uint8_t dimStartHours = (settings.Display.DimmingStart & 0xff);
    uint8_t dimStartMinutes = (settings.Display.DimmingStart >> 8) & 0xff;

    if (settings.Display.Flags.AutoBacklightControl) {
        g_autoBacklightControl = 1;

        if (Utils_IsInTimeRange(normalStartHours,
                                normalStartMinutes,
                                dimStartHours,
                                dimStartMinutes)) {
            PIC_SetLCDBacklightPWMDutyCycle(settings.Display.NormalLevel);
        } else {
            PIC_SetLCDBacklightPWMDutyCycle(settings.Display.DimLevel);
        }
    } else {
        if (g_autoBacklightControl) {
            PIC_SetLCDBacklightPWMDutyCycle(settings.Display.NormalLevel);
            g_autoBacklightControl = 0;
        }
    }
}