//
// Ethernet Weather Station 2
// Scrollable graphic menu with multiple type items
//
// Author: Tamas Karpati
// This file was created on 2014-08-20
//

#include "Menu.h"
#include "sys/keypad.h"
#include "driver/graphlcd.h"
#include "text.h"
#include "config.h"

#include <stdio.h>
#include <stdint.h>

#define DISPLAY_MAX_X                       127
#define DISPLAY_MAX_Y                       127

#define MENU_X                              0
#define MENU_Y                              0
#define MENU_X2                             DISPLAY_MAX_X
#define MENU_Y2                             DISPLAY_MAX_Y

///////////////////////////////////////////////////////////////////////////////

#define MENU_TITLE_X                        MENU_X + 2
#define MENU_TITLE_Y                        MENU_Y + 1

#define MENU_FRAME_X                        MENU_X
#define MENU_FRAME_Y                        MENU_TITLE_Y + 11
#define MENU_FRAME_X2                       MENU_X2
#define MENU_FRAME_Y2                       MENU_Y2

#define MENU_ITEM_AREA_X                    MENU_X + 2
#define MENU_ITEM_AREA_Y                    MENU_FRAME_Y + 2
#define MENU_ITEM_AREA_X2                   MENU_X2 - 2
#define MENU_ITEM_AREA_Y2                   MENU_Y2 - 2

#define MENU_ITEM_SPACING                   1
#define MENU_ITEM_BASE_HEIGHT               10
#define MENU_ITEM_HEIGHT                    23
#define MENU_ITEM_TEXT_X_OFFSET             2
#define MENU_ITEM_TEXT_Y_OFFSET             1
#define MENU_ITEM_FIELD_TEXT_X_OFFSET       2
#define MENU_ITEM_FIELD_TEXT_Y_OFFSET       2
#define MENU_ITEM_INT_FIELD_X_OFFSET        0
#define MENU_ITEM_INT_FIELD_Y_OFFSET        2
#define MENU_ITEM_PROGRESS_FIELD_X_OFFSET   18
#define MENU_ITEM_PROGRESS_FIELD_Y_OFFSET   2
#define MENU_ITEM_PROGRESS_FIELD_WIDTH      MENU_ITEM_AREA_X2 - MENU_ITEM_AREA_X - MENU_ITEM_PROGRESS_FIELD_X_OFFSET - 32
#define MENU_ITEM_PROGRESS_FIELD_HEIGHT     8
#define MENU_ITEM_SEPARATOR_TEXT_X_OFFSET   10
#define MENU_ITEM_SEPARATOR_Y_OFFSET        MENU_ITEM_BASE_HEIGHT / 2

///////////////////////////////////////////////////////////////////////////////

uint8_t calculateItemHeight(MenuItemType itemType, uint8_t selected);
uint8_t calculateYCoordOfSelectedItem(Menu *menu);
void drawItem(Menu *menu, uint8_t y, uint8_t index, uint8_t selected);
void drawItemIntegerField(MenuItem *item, uint8_t y);
void drawItemProgressField(MenuItem *item, uint8_t y);
void drawItemIPAddressField(Menu *menu, MenuItem *item, uint8_t y);
void drawItemTimeField(Menu *menu, MenuItem *item, uint8_t y);
void drawItemToggleField(MenuItem *item, uint8_t y);
void selectedItemPlus(Menu *menu);
void selectedItemMinus(Menu *menu);
void selectedItemLeft(Menu *menu);
void selectedItemRight(Menu *menu);
void selectedItemRestoreOriginal(Menu *menu);
//uint8_t GroupOfItemHasOtherItems(Menu *menu)

int32_t intDiv(int32_t val, int32_t div);
uint8_t getNthOctet(int32_t value, uint8_t nth);
void setNthOctet(uint8_t nth, uint8_t octet, int32_t *value);

static void navigateUp(Menu *menu);
static void navigateDown(Menu* menu);
static void selectCurrentItem(Menu* menu);

///////////////////////////////////////////////////////////////////////////////


void Menu_InitItem(MenuItem *item, const char *text, MenuItemType type,
                   int32_t min, int32_t max, int32_t initial, ValueChangedCallback callback)
{
    item->Text = text;
    item->Type = type;

    if (type != ITEM_TIME && type != ITEM_IPADDR && type != ITEM_SEPARATOR)
        item->Value = initial < min ? min : (initial > max ? max : initial);
    else
        item->Value = initial;

    item->Max = max;
    item->Min = min;

    item->Callback = callback;
}


void Menu_Draw(Menu *menu)
{
    GLCD_ClearBuffer();

    // Draw frame
    GLCD_DrawRect(MENU_X, MENU_Y, MENU_X2, MENU_FRAME_Y, 0, 1);
    GLCD_DrawRect(MENU_FRAME_X, MENU_FRAME_Y, MENU_FRAME_X2, MENU_FRAME_Y2, 0, 1);

    // Draw title text
    Text_DrawDefault(MENU_TITLE_X, MENU_TITLE_Y, menu->Title, 0);

    // Draw (visible) menu items
    uint8_t y = MENU_ITEM_AREA_Y;
    for (uint8_t i = menu->_indexOffset; i < menu->ItemCount; ++i) {
        uint8_t selected = 0;
        if (i == menu->_selectionIndex)
            selected = 1;

        uint8_t itemHeight = calculateItemHeight(menu->Items[i]->Type, selected);
        if (y + itemHeight > MENU_ITEM_AREA_Y2)
            break;

        if (selected)
            menu->_currentItemY = y;

        drawItem(menu, y, i, selected);
        y += itemHeight;
    }

    GLCD_WriteBuffer(0);
}


void Menu_KeyPress(Menu *menu, uint8_t key)
{
    switch (key) {
    case KEY_UP:
        navigateUp(menu);
        break;

    case KEY_DOWN:
        navigateDown(menu);
        break;

    case KEY_ENTER:
        selectCurrentItem(menu);
        break;

    case KEY_PLUS:
        selectedItemPlus(menu);
        break;

    case KEY_MINUS:
        selectedItemMinus(menu);
        break;

    case KEY_LEFT:
        selectedItemLeft(menu);
        break;

    case KEY_RIGHT:
        selectedItemRight(menu);
        break;

    case KEY_CANCEL:
        selectedItemRestoreOriginal(menu);
        break;

    default:
        return;
    }

    Menu_Draw(menu);
}

///////////////////////////////////////////////////////////////////////////////


uint8_t calculateItemHeight(MenuItemType itemType, uint8_t selected)
{
    uint8_t height = MENU_ITEM_BASE_HEIGHT;
    if (itemType != ITEM_SIMPLE && itemType != ITEM_SEPARATOR && selected)
        height = MENU_ITEM_HEIGHT;
    height += MENU_ITEM_SPACING;
    return height;
}


uint8_t calculateYCoordOfSelectedItem(Menu *menu)
{
    uint8_t y = MENU_ITEM_AREA_Y;
    for (uint8_t i = menu->_indexOffset; i < menu->ItemCount; ++i) {
        uint8_t selected = menu->_selectionIndex == i;
        y += calculateItemHeight(menu->Items[i]->Type, selected);
        if (selected)
            break;
    }
    return y;
}


void drawItem(Menu *menu, uint8_t y, uint8_t index, uint8_t selected)
{
    MenuItem *item = menu->Items[index];

    // Draw selection rect and data field
    if (selected) {
        if (item->Type != ITEM_SEPARATOR) {
            GLCD_DrawRect(MENU_ITEM_AREA_X, y, MENU_ITEM_AREA_X2, y + MENU_ITEM_BASE_HEIGHT, 1, 1);
            if (item->Type != ITEM_SIMPLE)
                GLCD_DrawRect(MENU_ITEM_AREA_X, y + MENU_ITEM_BASE_HEIGHT, MENU_ITEM_AREA_X2, y + MENU_ITEM_HEIGHT - MENU_ITEM_SPACING, 0, 1);

            // Usable keys notes
            switch (item->Type) {
            case ITEM_INT:
            case ITEM_PROGRESS:
            case ITEM_TOGGLE:
                Text_DrawDefault(MENU_ITEM_AREA_X + MENU_ITEM_FIELD_TEXT_X_OFFSET, y + MENU_ITEM_BASE_HEIGHT + MENU_ITEM_FIELD_TEXT_Y_OFFSET, "+-", 0);
                break;

            case ITEM_IPADDR:
            case ITEM_TIME:
                Text_DrawDefault(MENU_ITEM_AREA_X + MENU_ITEM_FIELD_TEXT_X_OFFSET, y + MENU_ITEM_BASE_HEIGHT + MENU_ITEM_FIELD_TEXT_Y_OFFSET, "+-<>", 0);
                break;

            default:
                break;
            }
        }

        // Field value
        switch (item->Type) {
        case ITEM_INT:
            drawItemIntegerField(item, y);
            break;
        case ITEM_PROGRESS:
            drawItemProgressField(item, y);
            break;
        case ITEM_IPADDR:
            drawItemIPAddressField(menu, item, y);
            break;
        case ITEM_TIME:
            drawItemTimeField(menu, item, y);
            break;
        case ITEM_TOGGLE:
            drawItemToggleField(item, y);
            break;
        default:
            break;
        }

        menu->_currentItemY = y;
    }

    if (item->Type == ITEM_SEPARATOR) {
        GLCD_DrawLine(MENU_ITEM_AREA_X, y + MENU_ITEM_SEPARATOR_Y_OFFSET, MENU_ITEM_AREA_X2, y + MENU_ITEM_SEPARATOR_Y_OFFSET, 1);
        Text_DrawDefault(MENU_ITEM_AREA_X + MENU_ITEM_SEPARATOR_TEXT_X_OFFSET, y, item->Text, 0);
    } else
        Text_DrawDefault(MENU_ITEM_AREA_X + MENU_ITEM_TEXT_X_OFFSET, y, item->Text, selected ? TF_INVERTED : 0);
}

// Menu is unnecessary here


void drawItemIntegerField(MenuItem *item, uint8_t y)
{
    char text[6];
    sprintf(text, "%d", item->Value);
    Text_DrawDefault(MENU_ITEM_AREA_X2 - MENU_ITEM_INT_FIELD_X_OFFSET, y + MENU_ITEM_BASE_HEIGHT + MENU_ITEM_INT_FIELD_Y_OFFSET, text, TF_RIGHT_ALIGNED);
}


void drawItemProgressField(MenuItem *item, uint8_t y)
{
    drawItemIntegerField(item, y);

    // Draw the progress bar
    GLCD_DrawRect(MENU_ITEM_PROGRESS_FIELD_X_OFFSET,
                  y + MENU_ITEM_BASE_HEIGHT + MENU_ITEM_PROGRESS_FIELD_Y_OFFSET,
                  MENU_ITEM_PROGRESS_FIELD_X_OFFSET + MENU_ITEM_PROGRESS_FIELD_WIDTH,
                  y + MENU_ITEM_BASE_HEIGHT + MENU_ITEM_PROGRESS_FIELD_Y_OFFSET + MENU_ITEM_PROGRESS_FIELD_HEIGHT,
                  0, 1);
    uint8_t w = intDiv(intDiv((item->Value - item->Min) * 100, item->Max - item->Min) * (MENU_ITEM_PROGRESS_FIELD_WIDTH - 3), 100);
    if (w > 0) {
        GLCD_DrawRect(MENU_ITEM_PROGRESS_FIELD_X_OFFSET + 2,
                      y + MENU_ITEM_BASE_HEIGHT + MENU_ITEM_PROGRESS_FIELD_Y_OFFSET + 2,
                      MENU_ITEM_PROGRESS_FIELD_X_OFFSET + 1 + w,
                      y + MENU_ITEM_BASE_HEIGHT + MENU_ITEM_PROGRESS_FIELD_Y_OFFSET + MENU_ITEM_PROGRESS_FIELD_HEIGHT - 2,
                      1, 1);
    }
}


void drawItemIPAddressField(Menu *menu, MenuItem *item, uint8_t y)
{
    uint8_t x = MENU_ITEM_AREA_X2 - MENU_ITEM_INT_FIELD_X_OFFSET;
    for (int8_t i = 3; i >= 0; --i) {
        char buf[4];
        uint8_t octet = (item->Value >> (8 * i)) & 0xff;
        sprintf(buf, "%03d", octet);
        Text_DrawDefault(x, y + MENU_ITEM_BASE_HEIGHT + MENU_ITEM_INT_FIELD_Y_OFFSET, buf, (menu->_subFieldIndex == i ? TF_INVERTED : 0) | TF_RIGHT_ALIGNED);
        x -= 17;
        if (i > 0)
            Text_DrawDefault(x, y + MENU_ITEM_BASE_HEIGHT + MENU_ITEM_INT_FIELD_Y_OFFSET, ".", 0);
    }
}


void drawItemTimeField(Menu *menu, MenuItem *item, uint8_t y)
{
    uint8_t x = MENU_ITEM_AREA_X2 - MENU_ITEM_INT_FIELD_X_OFFSET;
    char buf[3];
    sprintf(buf, "%02d", (uint8_t)((item->Value >> 8) & 0xff));
    Text_DrawDefault(x, y + MENU_ITEM_BASE_HEIGHT + MENU_ITEM_INT_FIELD_Y_OFFSET, buf, (menu->_subFieldIndex == 1 ? TF_INVERTED : 0) | TF_RIGHT_ALIGNED);
    sprintf(buf, "%d", (uint8_t)(item->Value & 0xff));
    x -= 13;
    Text_DrawDefault(x, y + MENU_ITEM_BASE_HEIGHT + MENU_ITEM_INT_FIELD_Y_OFFSET, buf, (menu->_subFieldIndex == 0 ? TF_INVERTED : 0) | TF_RIGHT_ALIGNED);
    Text_DrawDefault(x, y + MENU_ITEM_BASE_HEIGHT + MENU_ITEM_INT_FIELD_Y_OFFSET, ":", 0);
}


void drawItemToggleField(MenuItem *item, uint8_t y)
{
    Text_DrawDefault(MENU_ITEM_AREA_X2 - MENU_ITEM_INT_FIELD_X_OFFSET,
                     y + MENU_ITEM_BASE_HEIGHT + MENU_ITEM_INT_FIELD_Y_OFFSET,
                     item->Value ? "Enabled" : "Disabled", TF_RIGHT_ALIGNED);
}


void selectedItemPlus(Menu *menu)
{
    MenuItem *item = menu->Items[menu->_selectionIndex];
    uint8_t update = 0;

    if (item->Type == ITEM_INT || item->Type == ITEM_PROGRESS) {
        if (item->Value < item->Max) {
            ++item->Value;
            update = 1;
        }
    }
#ifdef ENABLE_NETWORK_CONFIG
    else if (item->Type == ITEM_IPADDR && menu->_subFieldIndex >= 0) {
        uint8_t octet = getNthOctet(item->Value, menu->_subFieldIndex);
        ++octet;
        setNthOctet(menu->_subFieldIndex, octet, &item->Value);
        update = 1;
    }
#endif
    else if (item->Type == ITEM_TIME && menu->_subFieldIndex >= 0) {
        uint8_t octet = getNthOctet(item->Value, menu->_subFieldIndex);
        ++octet;
        if (menu->_subFieldIndex == 0)
            octet %= 24;
        else
            octet %= 60;
        setNthOctet(menu->_subFieldIndex, octet, &item->Value);
        update = 1;
    } else if (item->Type == ITEM_TOGGLE) {
        ++item->Value;
        item->Value %= 2;
        update = 1;
    }

    if (update) {
        if (item->Callback)
            item->Callback(item->Value);
        drawItem(menu, menu->_currentItemY, menu->_selectionIndex, 1);
    }
}


void selectedItemMinus(Menu *menu)
{
    MenuItem *item = menu->Items[menu->_selectionIndex];
    uint8_t update = 0;

    if (item->Type == ITEM_INT || item->Type == ITEM_PROGRESS) {
        if (item->Value > item->Min) {
            --item->Value;
            update = 1;
        }
    }
#ifdef ENABLE_NETWORK_CONFIG
    else if (item->Type == ITEM_IPADDR && menu->_subFieldIndex >= 0) {
        uint8_t octet = getNthOctet(item->Value, menu->_subFieldIndex);
        --octet;
        setNthOctet(menu->_subFieldIndex, octet, &item->Value);
        update = 1;
    }
#endif
    else if (item->Type == ITEM_TIME && menu->_subFieldIndex >= 0) {
        uint8_t octet = getNthOctet(item->Value, menu->_subFieldIndex);
        --octet;
        if (menu->_subFieldIndex == 0) {
            if (octet > 23)
                octet = 23;
            octet %= 24;
        } else {
            if (octet > 59)
                octet = 59;
            octet %= 60;
        }
        setNthOctet(menu->_subFieldIndex, octet, &item->Value);
        update = 1;
    } else if (item->Type == ITEM_TOGGLE) {
        --item->Value;
        item->Value %= 2;
        update = 1;
    }

    if (update) {
        if (item->Callback)
            item->Callback(item->Value);
        drawItem(menu, menu->_currentItemY, menu->_selectionIndex, 1);
    }
}


int32_t intDiv(int32_t val, int32_t div)
{
    int count = 0;
    while (val >= div) {
        count++;
        val -= div;
    }
    return count;
}


void selectedItemLeft(Menu *menu)
{
    MenuItem *item = menu->Items[menu->_selectionIndex];
    uint8_t update = 0;

    --menu->_subFieldIndex;

    switch (item->Type) {
    case ITEM_IPADDR:
        if (menu->_subFieldIndex < 0)
            menu->_subFieldIndex = 3;
        menu->_subFieldIndex %= 4;
        break;
    case ITEM_TIME:
        if (menu->_subFieldIndex < 0)
            menu->_subFieldIndex = 1;
        menu->_subFieldIndex %= 2;
        break;
    default:
        break;
    }

    if (update)
        drawItem(menu, menu->_currentItemY, menu->_selectionIndex, 1);
}


void selectedItemRight(Menu *menu)
{
    MenuItem *item = menu->Items[menu->_selectionIndex];
    uint8_t update = 0;

    switch (item->Type) {
    case ITEM_IPADDR:
        ++menu->_subFieldIndex;
        menu->_subFieldIndex %= 4;
        break;
    case ITEM_TIME:
        ++menu->_subFieldIndex;
        menu->_subFieldIndex %= 2;
        break;
    default:
        break;
    }

    if (update)
        drawItem(menu, menu->_currentItemY, menu->_selectionIndex, 1);
}


void selectedItemRestoreOriginal(Menu *menu)
{
    MenuItem *item = menu->Items[menu->_selectionIndex];
    item->Value = menu->_currentFieldOriginalValue;
    drawItem(menu, menu->_currentItemY, menu->_selectionIndex, 1);
}


uint8_t getNthOctet(int32_t value, uint8_t nth)
{
    if (nth > 3)
        return 0;

    int32_t octet = value;
    octet >>= 8 * nth;

    return (uint8_t)(octet & 0xffl);
}


void setNthOctet(uint8_t nth, uint8_t octet, int32_t *value)
{
    if (nth > 3)
        return;

    int32_t andMask = 0xffffffff - (0xffl << (8 * nth));
    int32_t orMask = (int32_t)octet << (8 * nth);

    *value &= andMask;
    *value |= orMask;
}


static void navigateUp(Menu *menu)
{
    if (menu->_selectionIndex > 0) {
        while (menu->_selectionIndex > 0) {
            menu->_selectionIndex--;
            if (menu->Items[menu->_selectionIndex]->Type != ITEM_SEPARATOR)
                break;
            else if (menu->_selectionIndex == 0) {
                menu->_selectionIndex++;
                menu->_indexOffset = 0;
                break;
            }
        }

        menu->_currentFieldOriginalValue = menu->Items[menu->_selectionIndex]->Value;
        menu->_subFieldIndex = -1;
        if (menu->_selectionIndex < menu->_indexOffset)
            menu->_indexOffset = menu->_selectionIndex;
    }
}


static void navigateDown(Menu* menu)
{
    if (menu->_selectionIndex < menu->ItemCount - 1) {
        while (menu->_selectionIndex < menu->ItemCount - 1) {
            menu->_selectionIndex++;
            if (menu->Items[menu->_selectionIndex]->Type != ITEM_SEPARATOR)
                break;
            else if (menu->_selectionIndex == menu->ItemCount - 1) {
                while (calculateYCoordOfSelectedItem(menu) > (MENU_ITEM_AREA_Y2 - MENU_ITEM_HEIGHT))
                    menu->_indexOffset++;
                menu->_selectionIndex--;
                break;
            }
        }

        menu->_currentFieldOriginalValue = menu->Items[menu->_selectionIndex]->Value;
        menu->_subFieldIndex = -1;
        while (calculateYCoordOfSelectedItem(menu) > MENU_ITEM_AREA_Y2)
            menu->_indexOffset++;
    }
}


static void selectCurrentItem(Menu* menu)
{
    MenuItem *item = menu->Items[menu->_selectionIndex];

    if (item->Type == ITEM_SIMPLE && item->Callback)
        item->Callback(0);
}