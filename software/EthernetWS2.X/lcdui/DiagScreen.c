//
// Ethernet Weather Station 2
// Diagnostic screen
//
// Author: Tamas Karpati
// This file was created on 2018-12-23
//

#include "DiagScreen.h"
#include "Diag.h"
#include "text.h"
#include "driver/graphlcd.h"
#include "driver/t6963c.h"
#include "sys/keypad.h"
#include "sys/tick.h"

#include <stdio.h>


void DiagScreen_Init()
{

}


void DiagScreen_Draw()
{
    GLCD_ClearBuffer();

    GLCD_DrawLine(0, 5, 127, 5, GLCD_COLOR_BLACK);
    Text_DrawDefault(8, 0, "Diagnostics", 0);

    char s[50] = { 0 };

    static const uint8_t LineHeight = 11;
    uint8_t y = LineHeight;

    sprintf(s, "Elapsed: %lu", Tick_GetElapsedSeconds());
    Text_DrawDefault(0, y, s, 0);

    y += LineHeight;

    sprintf(s, "Tend. last upd.: %lu (%lu)", g_tendencyDataLastUpdated,
            Tick_GetElapsedSeconds() - g_tendencyDataLastUpdated);
    Text_DrawDefault(0, y, s, 0);

    y += LineHeight;

    sprintf(s, "Temp: s=%u, c=%u, a=%u", g_temperatureSample.sum,
            g_temperatureSample.count, g_temperatureSample.count > 0
            ? (g_temperatureSample.sum / g_temperatureSample.count) : 0);
    Text_DrawDefault(0, y, s, 0);

    y += LineHeight;

    sprintf(s, "Pres: s=%u, c=%u, a=%u", g_pressureSample.sum,
            g_pressureSample.count, g_pressureSample.count > 0
            ? (g_pressureSample.sum / g_pressureSample.count) : 0);
    Text_DrawDefault(0, y, s, 0);

    GLCD_WriteBuffer(GLCD_WB_NORMAL);
}


ScreenID DiagScreen_KeyPress(uint16_t keyCode)
{
    switch (keyCode) {
    case KEY_CANCEL:
        return SCR_MAIN;

    default:
        break;
    }

    return SCR_STAY_ON_CURRENT;
}
