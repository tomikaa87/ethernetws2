/**
 * Ethernet Weather Station 2 Firmware
 * Header that contains screen IDs
 *
 * @author Tamas Karpati <tomikaa87>
 * @date 2012. november 5.
 */

#ifndef SCREEN_H
#define SCREEN_H

typedef enum {
    // Used in KeyPressHandler
    SCR_STAY_ON_CURRENT = 255,

    SCR_MAIN = 0,
    SCR_FORECAST,
    SCR_MAIN_MENU,
    SCR_DIAG
} ScreenID;

#endif  /* SCREEN_H */

