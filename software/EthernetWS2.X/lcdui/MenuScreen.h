//
// Ethernet Weather Station 2
// Menu menu screen
//
// Author: Tamas Karpati
// This file was created on 2014-09-04
//

#ifndef MENUSCREEN_H
#define	MENUSCREEN_H

#include "screen.h"

#include <stdint.h>

void MenuScreen_Init();
void MenuScreen_Draw();
ScreenID MenuScreen_KeyPress(uint16_t keyCode);

#endif	/* MENUSCREEN_H */

