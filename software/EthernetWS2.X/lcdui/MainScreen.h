//
// Ethernet Weather Station 2
// LCDUI - Main screen
//
// Author: Tamas Karpati
// This file was created on 2012-11-04
//

#ifndef SCR_MAIN_H
#define	SCR_MAIN_H

#include "screen.h"

#include <stdint.h>

void MainScreen_Init();
void MainScreen_Draw();
ScreenID MainScreen_KeyPress(uint16_t key_code);
void MainScreen_Update();
void MainScreen_Task();

#endif	/* SCR_MAIN_H */

