//
// Ethernet Weather Station 2
// LCDUI
//
// Author: Tamas Karpati
// This file was created on 2012-11-04
//

#include "lcdui.h"
#include "screen.h"
#include "sys/keypad.h"
#include "driver/t6963c.h"

// Screens
#include "MainScreen.h"
#include "MenuScreen.h"
#include "ForecastScreen.h"
#include "DiagScreen.h"

#include <stdio.h>

//------------------------------------------------------------------------------
// Screens
//------------------------------------------------------------------------------

typedef ScreenID (*KeyPressHandlerFunc)(uint16_t keyCode);
typedef void (*DrawFunc)();

/**
 * Data structure to store a screen.
 */
typedef struct {
    DrawFunc Draw;
    KeyPressHandlerFunc HandleKeyPress;
} Screen;

// Screen storage

// TODO: avoid using function pointers in the future since it can break the
// compiler's ability to create a call map and this could lead to compilation
// errors, like "recursive function call to ...".

static const Screen g_screens[] = {
    { &MainScreen_Draw, &MainScreen_KeyPress },
    { &ForecastScreen_Draw, &ForecastScreen_KeyPress },
    { &MenuScreen_Draw, &MenuScreen_KeyPress },
    { &DiagScreen_Draw, &DiagScreen_KeyPress }
};

static uint8_t g_currentScreen = 0;

//------------------------------------------------------------------------------
// Private functions
//------------------------------------------------------------------------------


static void SwitchScreen(uint8_t screen)
{
    g_currentScreen = screen;
    GLCD_ClearBuffer();
    g_screens[screen].Draw();
}

//------------------------------------------------------------------------------
// API functions
//------------------------------------------------------------------------------


void UI_Init()
{
    MainScreen_Init();
    MenuScreen_Init();
    ForecastScreen_Init();
    DiagScreen_Init();

    MainScreen_Draw();
}

//--------------------------------------------------------------------


void UI_SendKeyPress(uint16_t keyCode)
{
    if (keyCode == 0)
        return;

    printf("KeyCode: %04X\r\n", keyCode);

    keyCode &= KEY_CODE_MASK;

    // Handle key presses
    uint8_t result = g_screens[g_currentScreen].HandleKeyPress(keyCode);

    // No action if result code is 255
    if (result == 255)
        return;

    // Switch to the corresponding screen
    SwitchScreen(result);
}

//--------------------------------------------------------------------


void UI_SendExternalEvent(UIExtEvent event)
{
    switch (event) {
        // Update sensor data on main screen
    case UI_EV_CLOCK_UPDATED:
        if (g_currentScreen == SCR_MAIN)
            MainScreen_Update();
        break;

    case UI_EV_TIMER_TICK:
        if (g_currentScreen == SCR_MAIN)
            MainScreen_Task();
        else if (g_currentScreen == SCR_FORECAST)
            ForecastScreen_Task();
        else if (g_currentScreen == SCR_DIAG)
            DiagScreen_Draw();
        break;

    case UI_EV_WDATA_UPDATED:
        if (g_currentScreen == SCR_MAIN)
            MainScreen_Update();
        else if (g_currentScreen == SCR_FORECAST)
            ForecastScreen_Update();
        break;

    case UI_EV_SENSOR_DATA_UPDATED:
        if (g_currentScreen == SCR_MAIN)
            MainScreen_Update();
        break;

    default:
        break;
    }
}