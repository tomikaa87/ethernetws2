//
// Ethernet Weather Station 2
// LCDUI - Main screen
//
// Author: Tamas Karpati
// This file was created on 2012-11-04
//

#include "driver/t6963c.h"

#include "MainScreen.h"
#include "sys/keypad.h"
#include "text.h"
#include "driver/graphlcd.h"
#include "sys/sensor.h"
#include "screen.h"
#include "driver/eeprom_25lc.h"
#include "net/ntp.h"
#include "image.h"
#include "net/wdata.h"
#include "Utils.h"
#include "sys/scheduler.h"
#include "sys/settings.h"
#include "lib/TendencyData.h"
#include "lib/WeatherDataCollector.h"
#include "sys/tick.h"

#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>

//------------------------------------------------------------------------------
// Forward declarations
//------------------------------------------------------------------------------

static uint8_t calculateBatteryLevel(uint16_t voltage);
static void setWarningIconVisible(uint8_t icon, uint8_t place, bool visible);
static void updateClock();
static void drawTendencyGraph(const uint8_t x, const uint8_t y,
                              const uint8_t * const values, const uint8_t count);
static void drawTendencyGraphs();

//------------------------------------------------------------------------------
// Constants
//------------------------------------------------------------------------------

#define BATT_MIN_VOLTAGE                        2700
#define BATT_MAX_VOLTAGE                        3700

#define TENDENCY_GRAPH_LEFT_TEMPERATURE         35
#define TENDENCY_GRAPH_TOP_TEMPERATURE          88
#define TENDENCY_GRAPH_LEFT_PRESSURE            68
#define TENDENCY_GRAPH_TOP_PRESSURE             88

#define TENDENCY_GRAPH_BAR_WIDTH                1
#define TENDENCY_GRAPH_BAR_SPACING              0
#define TENDENCY_GRAPH_HEIGHT                   12

//------------------------------------------------------------------------------
// Globals
//------------------------------------------------------------------------------

bit g_networkOnline;
bit g_sensorOnline;
bit g_warningVisible;
bit g_forceClockUpdate;

static uint8_t g_lastClockMinute = 0;

//------------------------------------------------------------------------------
// API functions
//------------------------------------------------------------------------------


void MainScreen_Init()
{
    g_networkOnline = 0;
    g_sensorOnline = 0;
    g_warningVisible = 1;
}

//--------------------------------------------------------------------


void MainScreen_Draw()
{
    if (g_hasWeatherData || g_sensorHasData) {
        MainScreen_Update();
    } else {
        GLCD_ClearBuffer();
        Image_DrawFullScreen(EEPROM_LOC_CURRENT_W_SCR);

        Image_DrawStatusIcon(0, 117, g_networkOnline ? SI_NET_ONLINE : SI_NET_OFFLINE);
        Image_DrawStatusIcon(9, 117, g_sensorOnline ? SI_SENSOR_ONLINE : SI_SENSOR_OFFLINE);
        setWarningIconVisible(0, 0, g_warningVisible);

        g_forceClockUpdate = 1;
        updateClock();
    }

    GLCD_WriteBuffer(GLCD_WB_NORMAL);
}

//--------------------------------------------------------------------


ScreenID MainScreen_KeyPress(uint16_t keyCode)
{
    switch (keyCode) {
    case KEY_MENU:
        // Enter main menu
        return SCR_MAIN_MENU;

    case KEY_RIGHT:
        return SCR_FORECAST;

    default:
        break;
    }

    // No action
    return SCR_STAY_ON_CURRENT;
}

//--------------------------------------------------------------------


void MainScreen_Update()
{
    // Update sensor and Internet availability
    MainScreen_Task();

    GLCD_ClearBuffer();

    TWDataNumber val;
    char str[WD_TEXT_MAX_LEN];

    if (g_hasWeatherData) {
        // RealFeel temperature
        val = WData_GetNumber(WD_C_REALFEEL, 0);
        sprintf(str, "%2d", abs(val.Int8));
        Text_DrawCustom(114, 10, str, FONT_LCD_SMALL, 0);
        if (val.Int8 < 0)
            Text_DrawCustom(110, 10, "-", FONT_LCD_SMALL, 0);
        else
            GLCD_DrawRect(110, 10, 113, 17, 1, 0);

        // Humidity
        val = WData_GetNumber(WD_C_HUMIDITY, 0);
        sprintf(str, "%3d", val.UInt8);
        Text_DrawCustom(107, 34, str, FONT_LCD_SMALL, 0);

        // Wind speed - !owerdrawn!
        val = WData_GetNumber(WD_C_WINDSPEED, 0);
        val.UInt16 = Clamp(val.UInt16, 0u, 1990u);
        sprintf(str, "%3d", val.UInt16 / 10);
        Text_DrawCustom(66, 49, str, FONT_LCD_LARGE, 0);
        sprintf(str, "%d", val.UInt16 % 10);
        Text_DrawCustom(100, 49, str, FONT_LCD_SMALL, 0);

        // Wind gusts - !overdrawn!
        val = WData_GetNumber(WD_C_WINDGUSTS, 0);
        sprintf(str, "%3d", Clamp(val.UInt16 / 10u, 0u, 199u));
        Text_DrawCustom(107, 58, str, FONT_LCD_SMALL, 0);

        // Precipitation
        val = WData_GetNumber(WD_C_PRECIP, 0);
        sprintf(str, "%3d", Clamp(val.UInt16, 0u, 999u));
        Text_DrawCustom(3, 91, str, FONT_LCD_SMALL, 0);

        // Text - testing only, ticker should be implemented
        GLCD_DrawRect(0, 71, 127, 82, 1, 0);
        WData_GetString(WD_C_TEXT, str, WD_TEXT_MAX_LEN, 0);
        Text_DrawDefault(1, 72, str, 0);

        // UV index
        val = WData_GetNumber(WD_C_UV, 0);
        sprintf(str, "%2d", val.UInt8);
        Text_DrawCustom(79, 105, str, FONT_LCD_SMALL, 0);

        // Visibility
        val = WData_GetNumber(WD_C_VISIBILITY, 0);
        sprintf(str, "%2d", val.UInt8);
        Text_DrawCustom(110, 105, str, FONT_LCD_SMALL, 0);
    }

    // Display accuweather data if sensor is offline
    if (!g_sensorHasData && g_hasWeatherData) {
        // Temperature values
        val = WData_GetNumber(WD_C_TEMPERATURE, 0);
        sprintf(str, "%2d", abs(val.Int8));
        Text_DrawCustom(77, 1, str, FONT_LCD_LARGE, 0);
        Text_DrawCustom(100, 1, "0", FONT_LCD_SMALL, 0);
        if (val.Int8 < 0)
            Text_DrawCustom(71, 1, "-", FONT_LCD_LARGE, 0);
        else
            GLCD_DrawRect(71, 1, 76, 19, 1, 0);

        // Humidity
        val = WData_GetNumber(WD_C_HUMIDITY, 0);
        sprintf(str, "%2d", Clamp(val.UInt8, 0, 99));
        Text_DrawCustom(77, 25, str, FONT_LCD_LARGE, 0);
        Text_DrawCustom(100, 25, "0", FONT_LCD_SMALL, 0);

        // Pressure
        val = WData_GetNumber(WD_C_PRESSURE, 0);
        sprintf(str, "%4d", val.UInt16);
        Text_DrawCustom(96, 91, str, FONT_LCD_SMALL, 0);
    } else if (g_sensorHasData) {
        // Temperature values
        sprintf(str, "%2d", abs(RemoteSensor_GetData().Outdoor.TempBMP / 100));
        Text_DrawCustom(77, 1, str, FONT_LCD_LARGE, 0);
        sprintf(str, "%d", abs(RemoteSensor_GetData().Outdoor.TempBMP % 100) / 10);
        Text_DrawCustom(100, 1, str, FONT_LCD_SMALL, 0);
        if (RemoteSensor_GetData().Outdoor.TempBMP < 0)
            Text_DrawCustom(71, 1, "-", FONT_LCD_LARGE, 0);
        else
            // Clear the negative sign
            GLCD_DrawRect(71, 1, 76, 19, 1, 0);

        // Humidity
        sprintf(str, "%2d", RemoteSensor_GetData().Outdoor.RH / 100);
        Text_DrawCustom(77, 25, str, FONT_LCD_LARGE, 0);
        sprintf(str, "%d", RemoteSensor_GetData().Outdoor.RH % 100 / 10);
        Text_DrawCustom(100, 25, str, FONT_LCD_SMALL, 0);

        // Pressure
        sprintf(str, "%4d", RemoteSensor_GetData().Outdoor.SeaLevelPressure / 100);
        Text_DrawCustom(96, 91, str, FONT_LCD_SMALL, 0);
    }

    if (g_sensorHasData) {
        // Indoor temperature
        sprintf(str, "%2d", RemoteSensor_GetData().Indoor.Temp);
        Text_DrawCustom(18, 105, str, FONT_LCD_SMALL, 0);

        // Indoor RH
        sprintf(str, "%2d", RemoteSensor_GetData().Indoor.RH);
        Text_DrawCustom(45, 105, str, FONT_LCD_SMALL, 0);

        // Battery rect 120;19 - 126;31 -> w:12
        uint8_t level = calculateBatteryLevel(RemoteSensor_GetData().Outdoor.Vdd);
        GLCD_DrawRect(19, 120, 30, 125, 1, 0);
        GLCD_DrawRect(19, 120, 19 + level, 125, 1, 1);
    }

    if (g_hasWeatherData) {
        // Draw weather icon.
        // Must tbe drawn after the numbers.
        val = WData_GetNumber(WD_C_ICON, 0);
        Image_DrawWeatherIcon(0, 0, val.UInt8);
    }

    // Draw base of current weather screen.
    // Must be drawn after the numbers with OR mode.
    Image_DrawFullScreen(EEPROM_LOC_CURRENT_W_SCR);

    Image_DrawStatusIcon(0, 117, g_networkOnline ? SI_NET_ONLINE : SI_NET_OFFLINE);
    Image_DrawStatusIcon(9, 117, g_sensorOnline ? SI_SENSOR_ONLINE : SI_SENSOR_OFFLINE);
    setWarningIconVisible(0, 0, g_warningVisible);

    g_forceClockUpdate = 1;
    updateClock();

    drawTendencyGraphs();

    GLCD_WriteBuffer(GLCD_WB_NORMAL);
}

//--------------------------------------------------------------------


void MainScreen_Task()
{
    updateClock();

    bool updateIsNeeded = false;
    uint16_t threshold = settings.Weather.UpdateIntervalMinutes * 60 + 20;

    // Update network status icon
    if (g_networkOnline) {
        if (ElapsedSinceLastDownload() > threshold) {
            g_networkOnline = 0;
            Image_DrawStatusIcon(0, 117, SI_NET_OFFLINE);
            updateIsNeeded = true;
        }
    } else {
        if (ElapsedSinceLastDownload() < threshold) {
            g_networkOnline = 1;
            Image_DrawStatusIcon(0, 117, SI_NET_ONLINE);
            updateIsNeeded = true;
        }
    }

    // Update sensor status icon
    if (g_sensorOnline) {
        if (ElapsedSinceLastSensorUpdate() > WIRELESS_SENSOR_TIMEOUT) {
            g_sensorOnline = 0;
            Image_DrawStatusIcon(9, 117, SI_SENSOR_OFFLINE);
            updateIsNeeded = true;
        }
    } else {
        if (ElapsedSinceLastSensorUpdate() < WIRELESS_SENSOR_TIMEOUT) {
            g_sensorOnline = 1;
            Image_DrawStatusIcon(9, 117, SI_SENSOR_ONLINE);
            updateIsNeeded = true;
        }
    }

    if (updateIsNeeded) {
        // Clear the status area
        GLCD_DrawRect(37, 117, 95, 127, 1, 0);

        // Draw warning icon
        g_warningVisible = !g_networkOnline || !g_sensorOnline;
        setWarningIconVisible(0, 0, g_warningVisible);

        GLCD_WriteBuffer(GLCD_WB_NORMAL);
    }

    if (g_warningVisible) {
        // Flash the status area
        GLCD_DrawInverseRect(37, 117, 95, 127);
        GLCD_WriteBuffer(GLCD_WB_NORMAL);
    }
}

//------------------------------------------------------------------------------
// Private functions
//------------------------------------------------------------------------------


static uint8_t calculateBatteryLevel(uint16_t voltage)
{
    uint16_t intval = BATT_MAX_VOLTAGE - BATT_MIN_VOLTAGE;
    uint8_t step = intval / 11;
    uint8_t level = 0;

    if (voltage <= BATT_MIN_VOLTAGE)
        level = 0;
    else if (voltage >= BATT_MAX_VOLTAGE)
        level = 11;
    else {
        for (uint16_t i = BATT_MIN_VOLTAGE; i < BATT_MAX_VOLTAGE; i += step)
            if (i <= voltage)
                ++level;
    }

    return level;
}

//--------------------------------------------------------------------


static void setWarningIconVisible(uint8_t icon, uint8_t place, bool visible)
{
    static const uint8_t BaseX = 38;
    static const uint8_t BaseY = 118;
    static const uint8_t IconW = 11;
    static const uint8_t IconH = 10;

    uint8_t x = BaseX + (IconW + 1) * place;

    if (visible)
        Image_DrawWarningIcon(x, BaseY, icon);
    else
        GLCD_DrawRect(x, BaseY, x + IconW, BaseY + IconH, 1, 0);
}

//--------------------------------------------------------------------


static void updateClock()
{
    struct tm *t = Utils_CurrentTime();

    putch('.');

    // Update only when time is changed
    if (g_lastClockMinute != t->tm_min || g_forceClockUpdate) {
        g_forceClockUpdate = 0;

        printf("Clock updated\r\n");
        g_lastClockMinute = t->tm_min;

        char buf[3];
        sprintf(buf, "%2d", t->tm_hour);
        Text_DrawCustom(98, 118, buf, FONT_LCD_SMALL, 0);
        sprintf(buf, "%02d", t->tm_min);
        Text_DrawCustom(115, 118, buf, FONT_LCD_SMALL, 0);

        GLCD_WriteBufferRegion(98, 118, 127, 127, GLCD_WB_NORMAL);
    }
}


static void drawTendencyGraph(const uint8_t x, const uint8_t y,
                              const uint8_t * const values, const uint8_t count)
{
    if (count == 0)
        return;

    GLCD_DrawRect(x, y,
                  x + TENDENCY_DATA_MAX_VALUES * (TENDENCY_GRAPH_BAR_WIDTH + TENDENCY_GRAPH_BAR_SPACING) - 1,
                  y + TENDENCY_GRAPH_HEIGHT - 1,
                  GLCD_FILL, GLCD_COLOR_WHITE);

    for (uint8_t i = 0; i < count; ++i) {
        uint8_t barHeight = values[i] + 1u;
        if (barHeight > TENDENCY_GRAPH_HEIGHT)
            barHeight = TENDENCY_GRAPH_HEIGHT;

        uint8_t barX = x + i * (TENDENCY_GRAPH_BAR_WIDTH + TENDENCY_GRAPH_BAR_SPACING);
        uint8_t barY = y + TENDENCY_GRAPH_HEIGHT - barHeight;

        // -1 is needed because glcd_draw_rect() fills the pixel at x2;y2 as well.
        uint8_t barX2 = barX + TENDENCY_GRAPH_BAR_WIDTH - 1;
        uint8_t barY2 = barY + barHeight - 1;

        //        printf("Drawing tendency bar for value %d at %u;%u-%u;%u with height %u px.\r\n",
        //                values[i], barX, barY, barX2, barY2, barHeight);

        GLCD_DrawRect(barX, barY, barX2, barY2, GLCD_FILL, GLCD_COLOR_BLACK);
    }
}


static void drawTendencyGraphs()
{
    uint8_t values[TENDENCY_DATA_MAX_VALUES];

    uint8_t count = TendencyData_GetNormalizedValues(&g_temperatureTendencyData,
                                                     TENDENCY_GRAPH_HEIGHT - 1,
                                                     values);

    drawTendencyGraph(TENDENCY_GRAPH_LEFT_TEMPERATURE,
                      TENDENCY_GRAPH_TOP_TEMPERATURE,
                      values,
                      count);

    count = TendencyData_GetNormalizedValues(&g_pressureTendencyData,
                                             TENDENCY_GRAPH_HEIGHT - 1,
                                             values);

    drawTendencyGraph(TENDENCY_GRAPH_LEFT_PRESSURE,
                      TENDENCY_GRAPH_TOP_PRESSURE,
                      values,
                      count);
}
