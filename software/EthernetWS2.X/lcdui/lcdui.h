//
// Ethernet Weather Station 2
// LCDUI
//
// Author: Tamas Karpati
// This file was created on 2012-11-04
//

#ifndef LCDUI_H
#define	LCDUI_H

#include "lcdui/UIExtEvent.h"

#include <stdint.h>

void UI_Init();
void UI_SendKeyPress(uint16_t keyCode);
void UI_SendExternalEvent(UIExtEvent event);

#endif	/* LCDUI_H */

