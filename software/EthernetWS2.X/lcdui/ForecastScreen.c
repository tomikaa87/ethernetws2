//
// Ethernet Weather Station 2
// LCDUI Forecast screen
//
// Author: Tamas Karpati
// This file was created on 2014-02-05
//

#include "ForecastScreen.h"
#include "text.h"
#include "driver/graphlcd.h"
#include "lcdui.h"
#include "hardware_profile.h"
#include "image.h"
#include "net/wdata.h"
#include "sys/keypad.h"

#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

//------------------------------------------------------------------------------
// Global variables
//------------------------------------------------------------------------------

uint8_t g_forecastScreenDayNumber;
static uint8_t g_onScreenTime;
bit g_nightMode;

//------------------------------------------------------------------------------
// API functions
//------------------------------------------------------------------------------


void ForecastScreen_Init()
{
    g_forecastScreenDayNumber = 1;
    g_nightMode = 0;
    g_onScreenTime = 0;

    printf("Forecast screen initialized\r\n");
}

//--------------------------------------------------------------------


void ForecastScreen_Draw()
{
    printf("Drawing forecast screen\r\n");

    GLCD_ClearBuffer();

    // Draw the base
    Image_DrawFullScreen(EEPROM_LOC_FORE_W_SCR);

    if (g_hasWeatherData)
        ForecastScreen_Update();

    g_onScreenTime = 0;
}

//--------------------------------------------------------------------


ScreenID ForecastScreen_KeyPress(uint16_t keyCode)
{
    uint8_t updateNeeded = 0;

    switch (keyCode) {
    case KEY_CANCEL:
        printf("Switching back from forecast screen\r\n");
        return SCR_MAIN;

    case KEY_DOWN:
        g_nightMode = 1;
        updateNeeded = 1;
        break;

    case KEY_UP:
        g_nightMode = 0;
        updateNeeded = 1;
        break;

    case KEY_LEFT:
        g_forecastScreenDayNumber = max(1, g_forecastScreenDayNumber - 1);
        updateNeeded = 1;
        break;

    case KEY_RIGHT:
        g_forecastScreenDayNumber = min(5, g_forecastScreenDayNumber + 1);
        updateNeeded = 1;
        break;

    default:
        break;
    }

    if (updateNeeded)
        ForecastScreen_Update();

    printf("Forecast screen key press: %d\r\n", keyCode);
    g_onScreenTime = 0;

    return SCR_STAY_ON_CURRENT;
}

//--------------------------------------------------------------------

#define WD_ADDRESS(day, night) (g_nightMode ? night : day)


void ForecastScreen_Update()
{
    GLCD_ClearBuffer();

    if (!g_hasWeatherData) {
        Image_DrawFullScreen(EEPROM_LOC_FORE_W_SCR);
        GLCD_WriteBuffer(g_nightMode);
        return;
    }

    char str[WD_TEXT_MAX_LEN];
    g_forecastScreenDayNumber = Clamp(g_forecastScreenDayNumber, 1, 5);

    TWDataNumber val;

    // Draw weather icon
    val = WData_GetNumber(WD_ADDRESS(WD_FD_ICON, WD_FN_ICON), g_forecastScreenDayNumber);
    Image_DrawWeatherIcon(0, 0, val.UInt8);

    // High temperature + negative sign
    val = WData_GetNumber(WD_ADDRESS(WD_FD_HI_TEMP, WD_FN_HI_TEMP), g_forecastScreenDayNumber);
    sprintf(str, "%2d", abs(val.Int8));
    Text_DrawCustom(77, 1, str, FONT_LCD_LARGE, 0);
    if (val.Int8 < 0)
        Text_DrawCustom(71, 1, "-", FONT_LCD_LARGE, 0);
    else
        GLCD_DrawRect(71, 1, 76, 19, 1, 0);

    // RF high + negative sign
    val = WData_GetNumber(WD_ADDRESS(WD_FD_REALFEEL_HI, WD_FN_REALFEEL_HI), g_forecastScreenDayNumber);
    sprintf(str, "%2d", abs(val.Int8));
    Text_DrawCustom(114, 10, str, FONT_LCD_SMALL, 0);
    if (val.Int8 < 0)
        Text_DrawCustom(110, 10, "-", FONT_LCD_SMALL, 0);
    else
        GLCD_DrawRect(110, 10, 113, 19, 1, 0);

    // Low temperature + negative sign
    val = WData_GetNumber(WD_ADDRESS(WD_FD_LO_TEMP, WD_FN_LO_TEMP), g_forecastScreenDayNumber);
    sprintf(str, "%2d", abs(val.Int8));
    Text_DrawCustom(77, 25, str, FONT_LCD_LARGE, 0);
    if (val.Int8 < 0)
        Text_DrawCustom(71, 25, "-", FONT_LCD_LARGE, 0);
    else
        GLCD_DrawRect(71, 25, 76, 43, 1, 0);

    // RF low + negative sign
    val = WData_GetNumber(WD_ADDRESS(WD_FD_REALFEEL_LO, WD_FN_REALFEEL_LO), g_forecastScreenDayNumber);
    sprintf(str, "%2d", abs(val.Int8));
    Text_DrawCustom(114, 34, str, FONT_LCD_SMALL, 0);
    if (val.Int8 < 0)
        Text_DrawCustom(110, 34, "-", FONT_LCD_SMALL, 0);
    else
        GLCD_DrawRect(110, 34, 113, 43, 1, 0);

    // Wind speed - !overdrawn!
    val = WData_GetNumber(WD_ADDRESS(WD_FD_WINDSPEED, WD_FN_WINDSPEED), g_forecastScreenDayNumber);
    sprintf(str, "%3d", val.UInt16 / 10);
    Text_DrawCustom(66, 49, str, FONT_LCD_LARGE, 0);

    // Wind gusts - !overdrawn!
    val = WData_GetNumber(WD_ADDRESS(WD_FD_WINDGUSTS, WD_FN_WINDGUSTS), g_forecastScreenDayNumber);
    sprintf(str, "%3d", val.UInt16 / 10);
    Text_DrawCustom(107, 58, str, FONT_LCD_SMALL, 0);

    // Forecast text
    WData_GetString(WD_ADDRESS(WD_FD_TEXT, WD_FN_TEXT), str, WD_TEXT_MAX_LEN, g_forecastScreenDayNumber);
    GLCD_DrawRect(0, 71, 128, 81, 1, 0);
    Text_DrawDefault(2, 72, str, 0);

    // Rain amount
    val = WData_GetNumber(WD_ADDRESS(WD_FD_RAIN, WD_FN_RAIN), g_forecastScreenDayNumber);
    sprintf(str, "%2d", val.UInt16);
    Text_DrawCustom(2, 93, str, FONT_LCD_LARGE, 0);

    // Snow amount
    val = WData_GetNumber(WD_ADDRESS(WD_FD_SNOW, WD_FN_SNOW), g_forecastScreenDayNumber);
    sprintf(str, "%2d", val.UInt16);
    Text_DrawCustom(29, 93, str, FONT_LCD_LARGE, 0);

    // Precipitation amount
    val = WData_GetNumber(WD_ADDRESS(WD_FD_PRECIP, WD_FN_PRECIP), g_forecastScreenDayNumber);
    sprintf(str, "%2d", val.UInt16);
    Text_DrawCustom(56, 93, str, FONT_LCD_LARGE, 0);

    // Thunderstorm probability
    val = WData_GetNumber(WD_ADDRESS(WD_FD_TSTORM, WD_FN_TSTORM), g_forecastScreenDayNumber);
    sprintf(str, "%2d", val.UInt8);
    Text_DrawCustom(95, 93, str, FONT_LCD_LARGE, 0);

    // Day number
    sprintf(str, "%d", g_forecastScreenDayNumber - 1);
    Text_DrawCustom(24, 117, str, FONT_LCD_SMALL, 0);

    // Weekday
    WData_GetString(WD_F_WEEKDAY, str, WD_WEEKDAY_LEN, g_forecastScreenDayNumber);
    char *tmp = str;
    while (*tmp) {
        *tmp = toupper(*tmp);
        ++tmp;
    }
    str[3] = 0;
    GLCD_DrawRect(33, 115, 58, 127, 1, 0);
    Text_DrawCustom(36, 117, str, FONT_PF_TEMPESTA_BOLD, 0);

    // Date (raw format: MM/DD/YYYY)
    WData_GetString(WD_F_DATE, str, WD_DATE_LEN, g_forecastScreenDayNumber);
    // Month
    Text_DrawCustom(95, 117, strtok(str, "/"), FONT_LCD_SMALL, 0);
    // Day
    Text_DrawCustom(114, 117, strtok(NULL, "/"), FONT_LCD_SMALL, 0);
    // Year
    Text_DrawCustom(62, 117, strtok(NULL, "/"), FONT_LCD_SMALL, 0);

    // Draw the base after the numbers
    Image_DrawFullScreen(EEPROM_LOC_FORE_W_SCR);

    // Draw the buffer inverted in case of night time mode
    GLCD_WriteBuffer(g_nightMode);
}

//--------------------------------------------------------------------


void ForecastScreen_Task()
{
    if (g_onScreenTime < 255)
        ++g_onScreenTime;

    printf("OnScreen: %d\r\n", g_onScreenTime);

    // Switch back to main screen
    // FIXME - not a nice solution
    if (g_onScreenTime == 60) {
        printf("Forecast screen timeout\r\n");
        UI_SendKeyPress(KEY_CANCEL);
    }
}