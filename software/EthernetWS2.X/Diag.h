/*
 * File:   Diag.h
 * Author: tomikaa
 *
 * Created on 2018. december 23., 15:41
 */

#ifndef DIAG_H
#define	DIAG_H

#include "lib/TendencyData.h"
#include "lib/Sample.h"

#include <stdint.h>

extern TendencyData g_temperatureTendencyData;
extern TendencyData g_pressureTendencyData;
extern uint32_t g_tendencyDataLastUpdated;
extern Sample g_temperatureSample;
extern Sample g_pressureSample;

#endif	/* DIAG_H */

