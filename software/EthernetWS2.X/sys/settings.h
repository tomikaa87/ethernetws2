/**
 * Ethernet Weather Station 2 Firmware
 * Settings storage module
 *
 * @author Tamas Karpati <tomikaa87>
 * @date 2012. november 18.
 */

#ifndef SETTINGS_H
#define	SETTINGS_H

#include "config.h"

#include <stdint.h>

#ifdef ENABLE_NETWORK_CONFIG
#include "stack/TCPIP.h"                // IP_ADDR, WORD
#endif

#define SETTINGS_FLAG_NET_STATIC_IP     1
#define SETTINGS_FLAG_NET_USE_PROXY     2

/**
 * Setings storage structure
 */
typedef union {

    struct {
        uint16_t Checksum;              // Settings struct checksum
        // Display settings

        struct {
            uint8_t DimLevel;          // Dimmed backlight level
            uint8_t NormalLevel;         // Normal backlight level
            uint16_t DimmingStart;          // Time value to dim backlight from
            uint16_t DimmingEnd;         // Time value to set normal bl. from

            struct {
                unsigned AutoBacklightControl : 1;
                unsigned : 0;
            } Flags;
        } Display;

#ifdef ENABLE_NETWORK_CONFIG
        // Network settings

        struct {
            IP_ADDR StaticIP;               // Static IP
            IP_ADDR StaticNetmask;          // Netmask for static IP
            IP_ADDR StaticGateway;          // Default gateway for static IP
            IP_ADDR StaticDNSIP;             // DNS server IP
            IP_ADDR ProxyIP;           // Proxy server IP
            WORD ProxyPort;            // Proxy server port
            uint8_t Flags;              // Settings flags (static ip, proxy etc.)
        } Network;
#endif

        // Date and time settings

        struct {
            int8_t TimeZoneOffsetHours;
        } DateTime;

        // Weather settings

        struct {
            uint8_t UpdateIntervalMinutes;
            uint8_t TendencyUpdateIntervalMinutes;
        } Weather;
    } ;
    uint8_t data[];
} Settings;

extern Settings settings;

void Settings_Load();
void Settings_Save();
void Settings_RestoreDefaults();

#endif	/* SETTINGS_H */

