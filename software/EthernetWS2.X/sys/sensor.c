//
// Ethernet Weather Station 2
// Sensor data handler
//
// Author: Tamas Karpati
// This file was created on 2012-10-28
//

#include "sensor.h"

#include <string.h>

//------------------------------------------------------------------------------
// Global variables
//------------------------------------------------------------------------------

bit g_sensorDataUpdated;
bit g_sensorHasData;
bit g_sensorHasOnlyLocalData;
SensorData g_sensorData;

//------------------------------------------------------------------------------
// API functions
//------------------------------------------------------------------------------


void RemoteSensor_Init()
{
    memset(&g_sensorData, 0, sizeof (g_sensorData));

    g_sensorHasData = 0;
    g_sensorDataUpdated = 0;
    g_sensorHasOnlyLocalData = 0;
}
