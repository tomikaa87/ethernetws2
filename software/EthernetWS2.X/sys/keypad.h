/**
 * Ethernet Weather Station 2 Firmware
 * Keypad peripheral module
 *
 * @author Tamas Karpati
 * @date 2013-05-03
 */

#ifndef KEYPAD_H
#define KEYPAD_H

#include <stdint.h>

/*** Key codes ****************************************************************/

#define KEY_R1_C1                       1
#define KEY_R1_C2                       2
#define KEY_R1_C3                       4
#define KEY_R2_C1                       8
#define KEY_R2_C2                       16
#define KEY_R2_C3                       32
#define KEY_R3_C1                       64
#define KEY_R3_C2                       128
#define KEY_R3_C3                       256
#define KEY_LONG_PRESS                  0x8000
#define KEY_CODE_MASK                   0x7FFF

/*** Key mapping **************************************************************/

/*
 * 1x9 layout:
 *  [R1C1] [R1C2] [R1C3]  [R2C1]  [R2C2] [R2C3][R3C1] [R3C2] [R3C3]
 *  [Menu]  [Up]  [Down]  [Left]  [Right]  [+]  [-]  [Enter]  [Esc]
 *
 * 3x3 layout:
 *  [Menu]      [Up]        [+]
 *  [Left]      [Enter]     [Right]
 *  [Esc]       [Down]      [-]
 */

//#define KBD_LAYOUT_3x3
#define KBD_LAYOUT_1x9

enum {
#ifdef KBD_LAYOUT_3x3
    KEY_MENU        = KEY_R1_C1,
    KEY_UP          = KEY_R1_C2,
    KEY_PLUS        = KEY_R1_C3,
    KEY_LEFT        = KEY_R2_C1,
    KEY_ENTER       = KEY_R2_C2,
    KEY_RIGHT       = KEY_R2_C3,
    KEY_CANCEL      = KEY_R3_C1,
    KEY_DOWN        = KEY_R3_C2,
    KEY_MINUS       = KEY_R3_C3
#elif defined KBD_LAYOUT_1x9
    KEY_MENU        = KEY_R1_C1,
    KEY_RIGHT       = KEY_R1_C2,
    KEY_LEFT        = KEY_R1_C3,
    KEY_UP          = KEY_R2_C1,
    KEY_DOWN        = KEY_R2_C2,
    KEY_PLUS        = KEY_R2_C3,
    KEY_MINUS       = KEY_R3_C1,
    KEY_ENTER       = KEY_R3_C2,
    KEY_CANCEL      = KEY_R3_C3
#endif
} ;

/*** API functions ************************************************************/

void Keypad_Init();
uint16_t Keypad_Task();

#endif // KEYPAD_H