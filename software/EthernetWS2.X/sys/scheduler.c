//
// Ethernet Weather Station 2
// Scheduler
//
// Author: Tamas Karpati
// This file was created on 2014-02-01
//

#include "scheduler.h"
#include "types.h"

#include "lcdui/lcdui.h"
#include "lcdui/DisplayController.h"
#include "net/ntp.h"
#include "net/wdata_dloader.h"
#include "sys/keypad.h"
#include "sys/sensor.h"

#include "TCPIP.h"
#include "settings.h"
#include "lib/WeatherDataCollector.h"

#include <limits.h>

#include <stdio.h>

//------------------------------------------------------------------------------
// Global variables
//------------------------------------------------------------------------------

bit g_schedulerIsEnabled;
bit g_clockUpdated;
bit g_weatherDataUpdateEnabled;
bit g_weatherDataUpdated;
bit g_TCPStackEnabled;
bit g_LCDUIEnabled;
bit g_NTPEnabled;
bit g_weatherDataCollectorEnabled;
volatile bit g_LCDUITick;

volatile uint16_t g_elapsedSinceLastDownload = 0xffff;

volatile uint8_t g_NTPRetryIntervalSecs = 0;
uint8_t g_NTPRetryCounter = 0;
const uint8_t NTPRetryCount = 2;

uint16_t g_NTPSyncIntervalSecs = 600;
volatile uint16_t g_elapsedSinceLastNTPSync = 0xffff;

volatile uint8_t g_MACSetupCounter = 0;
#define IS_MAC_SET_UP() (g_MACSetupCounter == 0)

volatile uint8_t g_suspensionCounter = 0;
static const uint8_t SuspensionTimeout = 5;

volatile uint16_t g_elapsedSinceLastSensorUpdate = WIRELESS_SENSOR_TIMEOUT;

//------------------------------------------------------------------------------
// Forward declarations
//------------------------------------------------------------------------------

void enableNetwork();
void disableNetwork();

//------------------------------------------------------------------------------
// API functions
//------------------------------------------------------------------------------


void Scheduler_Init()
{
    g_schedulerIsEnabled = 1;
    g_clockUpdated = 0;
    g_weatherDataUpdateEnabled = 1;
    g_weatherDataUpdated = 0;
    g_TCPStackEnabled = 1;
    g_LCDUIEnabled = 1;
    g_NTPEnabled = 1;
    g_LCDUITick = 1;
    g_weatherDataCollectorEnabled = 1;

    enableNetwork();
}

//--------------------------------------------------------------------


void Scheduler_Task()
{
    if (!g_schedulerIsEnabled || g_suspensionCounter > 0)
        return;

    if (g_TCPStackEnabled && !IS_MAC_SET_UP())
        MACIsLinked();

    if (g_TCPStackEnabled && IS_MAC_SET_UP()) {
        if (!MACIsLinked()) {
            //printf("[SCH] MAC is unlinked\r\n");
            disableNetwork();
            g_weatherDataUpdateEnabled = 0;
            g_NTPEnabled = 0;
            return;
        }

        StackTask();
        StackApplications();

        if (!AppConfig.Flags.bIsDHCPEnabled || DHCPIsBound(0)) {
            // Download fresh weather data
            if (g_weatherDataUpdateEnabled) {
                switch (WDDownloader_Task()) {
                case WDR_FINISHED:
                    g_weatherDataUpdated = 1;
                    //printf("[SCH] WData downloaded\r\n");
                case WDR_ERROR:
                    //printf("[SCH] WData download disabled\r\n");
                    g_elapsedSinceLastDownload = 0;
                    g_weatherDataUpdateEnabled = 0;
                    break;

                case WDR_IN_PROGRESS:
                    break;
                }
            }

            // Synchronize the clock
            if (g_NTPEnabled && g_NTPRetryIntervalSecs == 0) {
                if (g_NTPRetryCounter >= NTPRetryCount) {
                    //printf("[SCH] NTP update failed, no more retries\r\n");
                    g_NTPEnabled = 0;
                    return;
                }

                switch (NTP_Task()) {
                case NTP_UPDATE_FAILED:
                    // Retry after 10 seconds
                    g_NTPRetryIntervalSecs = 10;
                    ++g_NTPRetryCounter;
                    //printf("[SCH] NTP udpate failed\r\n");
                    break;

                case NTP_UPDATE_SUCCESSFUL:
                    g_clockUpdated = 1;
                    g_elapsedSinceLastNTPSync = 0;
                    //printf("[SCH] NTP update was successful\r\n");
                    g_NTPEnabled = 0;
                    break;
                }
            }
        }

        // Shutdown the network when not used
        if (!g_weatherDataUpdateEnabled && !g_NTPEnabled) {
            //printf("[SCH] Network is shutting down as no one uses it\r\n");
            disableNetwork();
        }
    }

    if (g_LCDUIEnabled) {
        // Process key presses
        UI_SendKeyPress(Keypad_Task());

        if (g_weatherDataUpdated) {
            g_weatherDataUpdated = 0;

            if (g_weatherDataCollectorEnabled)
                WeatherDataCollector_DoSampling();

            //printf("[SCH] Weather data updated\r\n");
            UI_SendExternalEvent(UI_EV_WDATA_UPDATED);
        }

        if (g_clockUpdated) {
            g_clockUpdated = 0;
            //printf("[SCH] Clock updated\r\n");
            UI_SendExternalEvent(UI_EV_CLOCK_UPDATED);
        }

        if (g_LCDUITick) {
            g_LCDUITick = 0;
            UI_SendExternalEvent(UI_EV_TIMER_TICK);
            DisplayController_Task();
        }

        if (RemoteSensor_IsUpdated()) {
            //printf("[SCH] Sensor data updated\r\n");

            if (!RemoteSensor_HasOnlyLocalData())
                g_elapsedSinceLastSensorUpdate = 0;

            RemoteSensor_ClearUpdatedFlag();
            UI_SendExternalEvent(UI_EV_SENSOR_DATA_UPDATED);
        }
    }

    // Do scheduled weather data download
    if (!g_weatherDataUpdateEnabled &&
            g_elapsedSinceLastDownload >= settings.Weather.UpdateIntervalMinutes * 60) {
        //printf("[SCH] Weather data download started\r\n");
        enableNetwork();
        WDDownloader_Reset();
        WDDownload_Start(0);
        g_weatherDataUpdateEnabled = 1;
    }

    // Do scheduled NTP sync
    if (!g_NTPEnabled &&
            g_elapsedSinceLastNTPSync >= g_NTPSyncIntervalSecs) {
        enableNetwork();
        g_NTPEnabled = 1;
    }

    if (g_weatherDataCollectorEnabled) {
        WDCTaskResult result = WeatherDataCollector_Task();

        if (result == WDC_UPDATE_NEEDED)
            UI_SendExternalEvent(UI_EV_WDATA_UPDATED);
    }
}

//--------------------------------------------------------------------


void Scheduler_Tick()
{
    if (g_elapsedSinceLastDownload < UINT_MAX)
        ++g_elapsedSinceLastDownload;

    if (g_NTPRetryIntervalSecs > 0)
        --g_NTPRetryIntervalSecs;

    if (g_MACSetupCounter > 0)
        --g_MACSetupCounter;

    if (g_suspensionCounter > 0)
        --g_suspensionCounter;

    if (g_elapsedSinceLastSensorUpdate < UINT_MAX)
        ++g_elapsedSinceLastSensorUpdate;

    g_LCDUITick = 1;
}

//--------------------------------------------------------------------


void Scheduler_Suspend()
{
    g_suspensionCounter = SuspensionTimeout;
}

//------------------------------------------------------------------------------
// Private functions
//------------------------------------------------------------------------------


void enableNetwork()
{
    //printf("[SCH] Starting the network\r\n");
    if (g_TCPStackEnabled)
        return;

    TMR0IE = 1;
    g_MACSetupCounter = 5;

    g_TCPStackEnabled = 1;

    MACPowerUp();
    MACIsLinked();
}

//--------------------------------------------------------------------


void disableNetwork()
{
    //printf("[SCH] Shutting down the network\r\n");
    MACPowerDown();
    g_TCPStackEnabled = 0;
    TMR0IE = 0;
}