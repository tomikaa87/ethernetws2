#include "tick.h"

volatile uint32_t g_tickCount = 0;
volatile uint32_t g_elapsedSeconds = 0;


/**
 * Calculates the number of ticks elapsed since the given number of ticks.
 * @param since Number of ticks to compare.
 * @return Elapsed ticks.
 */
inline uint32_t Tick_ElapsedSince(uint32_t since)
{
    return g_tickCount - since;
}


/**
 * Calculates the number of ticks from the current ticks to the given number
 * of ticks.
 * @param ticks Number of ticks to compare.
 * @return Number of ticks
 */
inline uint32_t Tick_TicksTo(uint32_t ticks)
{
    return ticks - g_tickCount;
}
