/*
 * File:   tick.h
 * Author: Tam�s
 *
 * Created on 2015. j�lius 12., 23:10
 */

#ifndef TICK_H
#define	TICK_H

#include <stdint.h>

extern volatile uint32_t g_tickCount;
extern volatile uint32_t g_elapsedSeconds;

#define Tick_GetCurrent()			(g_tickCount)
#define Tick_GetElapsedSeconds()	(g_elapsedSeconds)

inline uint32_t Tick_ElapsedSince(uint32_t since);
inline uint32_t Tick_TicksTo(uint32_t ticks);

#endif	/* TICK_H */

