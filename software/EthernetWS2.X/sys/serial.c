//
// Ethernet Weather Station 2 Firmware
// Serial interface
//
// Author: Tamas Karpati
// This file was created on 2012-10-28
//

/*
 * Packet formats:
 *
 * Sensor data:
 *      *@;[ERR_OUT]:[TEMP1_OUT]:[TEMP2_OUT]:[RH_OUT]:[PRES_OUT]:[PRES_SL]:[VDD_OUT]:[ERR_IN]:[TEMP_IN]:[RH_IN]:[VDD_IN];
 *
 * If ERR_OUT = 0xFF, read local measurement data only, don't reset remote sensor timer.
 *
 * EEPROM data write:
 *      W;[ADDRESS]:[LENGTH]:[DATA]:...:[DATA]:[CRC16];
 *        4         1        1      ...1        2       bytes
 *   Answer packet:
 *      W;[RESULT];
 *        1         bytes
 *
 * EEPROM data read:
 *      R;[ADDRESS]:[LENGTH];
 *        4         1           bytes
 *   Answer packet:
 *      R;[RESULT]:[LENGTH]:[DATA]...[DATA]:[CRC16];
 *        1        1        1     ...1      2           bytes
 *
 * *** EEPROM I/O Protocol v2 ***
 *
 * - ':' characters removed from between data bytes
 * - data length field increased to 2 bytes
 *
 * EEPROM data write:
 *      W;[ADDRESS]:[LENGTH]:[DATA]...[DATA]:[CRC16];
 *        4         2        1     ...1       2        bytes
 *   Answer packet:
 *      W;[RESULT];
 *        1         bytes
 */

#include "serial.h"

#include "hardware_profile.h"
#include "types.h"
#include "sensor.h"
#include "crc16.h"

#include "driver/sram_23k.h"
#include "driver/eeprom_25lc.h"

#include "lcdui/image.h"
#include "driver/graphlcd.h"

#include "scheduler.h"

#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>

//------------------------------------------------------------------------------
// Configuration
//------------------------------------------------------------------------------

#define UART_TX_BAUD                115200ul
#define UART_TX_HIGH_SPEED          1
#define UART_RX_BUF_SIZE            200

//------------------------------------------------------------------------------
// Convenience macros
//------------------------------------------------------------------------------

#if (UART_TX_HIGH_SPEED == 1)
#define UART_TX_DIVIDER        16ul
#else
#define UART_TX_DIVIDER        64ul
#endif
#define UART_TX_SPBRG \
    (unsigned char)(_XTAL_FREQ / UART_TX_BAUD / UART_TX_DIVIDER - 1)
#define UART_TX_REAL_BAUD \
    (unsigned long)(_XTAL_FREQ / (UART_TX_DIVIDER * (UART_TX_SPBRG + 1)))
#define UART_TX_BAUD_ERROR \
    (signed char)((((float)UART_TX_REAL_BAUD - (float)UART_TX_BAUD) / (float)UART_TX_BAUD) * 100.0)

char Warning_UartTxErrorTooHigh[UART_TX_BAUD_ERROR <= 3];

//------------------------------------------------------------------------------
// Globals
//------------------------------------------------------------------------------

volatile char g_rxBuf[UART_RX_BUF_SIZE] = {0, };
volatile uint8_t g_bufNextIn = 0;
volatile uint8_t g_bufNextOut = 0;
#define RX_BUF_HAS_CHAR     (g_bufNextIn != g_bufNextOut)

//------------------------------------------------------------------------------
// Standard C I/O functions
//------------------------------------------------------------------------------


void putch(char c)
{
    while (!SERIAL_TRMT)
        continue;
    TXREG = c;
    while (!SERIAL_TRMT)
        continue;
}

//--------------------------------------------------------------------


char getch()
{
    while (!SERIAL_RCIF)
        continue;
    return RCREG;
}

//--------------------------------------------------------------------


char getbufch()
{
    char c;

    // Wait untit a byte is ready to read from the USART buffer
    while (!RX_BUF_HAS_CHAR)
        continue;
    c = g_rxBuf[g_bufNextOut];
    // Step to next index
    g_bufNextOut = (g_bufNextOut + 1) % UART_RX_BUF_SIZE;
    return c;
}

//--------------------------------------------------------------------


void putbufch(char c)
{
    uint8_t next_in;

    g_rxBuf[g_bufNextIn] = c;
    next_in = g_bufNextIn;
    g_bufNextIn = (g_bufNextIn + 1) % UART_RX_BUF_SIZE;
    if (g_bufNextIn == g_bufNextOut)
        g_bufNextIn = next_in;
}

//------------------------------------------------------------------------------
// Forward declarations
//------------------------------------------------------------------------------

void ProcessNextChar(char c);
void ParseSensorData();
void ParseEEPROMReadData();
void ParseEEPROMWriteData();
void ParseIconData();

//------------------------------------------------------------------------------
// API functions
//------------------------------------------------------------------------------


void Serial_Init()
{
    // BRG16 = 1
    SERIAL_BAUDCON = 0b0000100;
    // TXEN = 1, BRGH = UART_TX_HIGH_SPEED
    SERIAL_TXSTA = 0b00100000 | (UART_TX_HIGH_SPEED ? 0b100 : 0);
    // SPEN = 1, CREN = 1
    SERIAL_RCSTA = 0b10010000;
    // Set SPBRG
    SERIAL_SPBRG = UART_TX_SPBRG & 0xFF;
    // Set SPBRGH
    SERIAL_SPBRGH = (UART_TX_SPBRG >> 8) & 0xFF;
}

//--------------------------------------------------------------------


void Serial_Task()
{
    // Check if RX buffer has new characters
    if (!RX_BUF_HAS_CHAR)
        return;
    // Read and parse character
    ProcessNextChar(getbufch());
}

//------------------------------------------------------------------------------
// Private functions
//------------------------------------------------------------------------------

static uint16_t g_extSRAMBufferIndex = 0;


void ProcessNextChar(char c)
{

    static enum {
        M_UNKNOWN,
        M_SENSOR,
        M_EE_WDATA,
        M_EE_RDATA,
        M_ICON_DEBUG
    } mode = M_UNKNOWN;

    static bit is_start_found;
    static bit is_first_char;

    c = toupper(c);

    // Reset state if necessary
    if (c == '*' || g_extSRAMBufferIndex >= RAM_LOC_UART_BUFFER_SIZE) {
    reset:
        mode = M_UNKNOWN;
        g_extSRAMBufferIndex = 0;
        is_start_found = 0;
        is_first_char = 1;
        return;
    }

    if (is_first_char) {
        // Switch mode
        is_first_char = 0;
        switch (c) {
        case '@': mode = M_SENSOR;
            break;
        case 'W': mode = M_EE_WDATA;
            Scheduler_Suspend();
            break;
        case 'R': mode = M_EE_RDATA;
            Scheduler_Suspend();
            break;
        case 'I': mode = M_ICON_DEBUG;
            break;
        default: goto reset;
        }
    } else {
        if (c == ';') {
            // Process start/end data token
            if (is_start_found) {
                // End of data
                switch (mode) {
                    // Wireless sensor data
                case M_SENSOR: ParseSensorData();
                    goto reset;
                    // External EEPROM data write
                case M_EE_WDATA: ParseEEPROMWriteData();
                    goto reset;
                    // External EEPROM data read
                case M_EE_RDATA: ParseEEPROMReadData();
                    goto reset;
                    // Weather icon debug mode
                case M_ICON_DEBUG: ParseIconData();
                    goto reset;
                default: goto reset;
                }
            } else
                is_start_found = 1;
        } else {
            SRAM_WriteByte(RAM_LOC_UART_BUFFER_ADDR + g_extSRAMBufferIndex++, c);
        }
    }
}

//--------------------------------------------------------------------


void ParseSensorData()
{
    char buf[10];
    uint8_t idx = 0, field = 0;
    DecomposedInt32 v;
    uint8_t readOnlyLocalData = 0;

    for (uint16_t i = 0; i <= g_extSRAMBufferIndex; ++i) {
        //char c = sram_buf[i];
        char c = SRAM_ReadByte(RAM_LOC_UART_BUFFER_ADDR + i);

        if (c == ':' || i == g_extSRAMBufferIndex) {
            buf[idx] = 0;
            v.ui = strtoul(buf, NULL, 16);

            // In case of local data parsing, only fields >= 7 are being read
            if (!readOnlyLocalData || field >= 7) {
                switch (field) {
                case 0:
                    if (v.ui == 0xFF) {
                        readOnlyLocalData = 1;
                        break;
                    }
                    RemoteSensor_GetData().Outdoor.ErrorCode = v.ui;
                    break;

                case 1: RemoteSensor_GetData().Outdoor.TempBMP = v.componentWords.lw.si;
                    break;
                case 2: RemoteSensor_GetData().Outdoor.TempDHT = v.componentWords.lw.si;
                    break;
                case 3: RemoteSensor_GetData().Outdoor.RH = v.componentWords.lw.ui;
                    break;
                case 4: RemoteSensor_GetData().Outdoor.Pressure = v.ui;
                    break;
                case 5: RemoteSensor_GetData().Outdoor.SeaLevelPressure = v.ui;
                    break;
                case 6: RemoteSensor_GetData().Outdoor.Vdd = v.componentWords.lw.ui;
                    break;
                case 7: RemoteSensor_GetData().Indoor.ErrorCode = v.ui;
                case 8: RemoteSensor_GetData().Indoor.Temp = v.ui;
                    break;
                case 9: RemoteSensor_GetData().Indoor.RH = v.ui;
                    break;
                case 10:
                    RemoteSensor_GetData().Indoor.Vdd = v.componentWords.lw.ui;

                    if (readOnlyLocalData) {
                        RemoteSensor_SetLocalUpdatedFlag();
                    } else {
                        RemoteSensor_SetUpdatedFlag();
                    }
                    break;

                default: return;
                }
            }

            ++field;
            idx = 0;
        } else {
            buf[idx++] = c;
        }
    }
}

//--------------------------------------------------------------------


void ParseEEPROMReadData()
{
    char buf[10];
    uint8_t idx = 0, field = 0;
    DecomposedInt32 v;
    uint24_t ee_address;

    for (uint16_t i = 0; i <= g_extSRAMBufferIndex; ++i) {
        char c = SRAM_ReadByte(RAM_LOC_UART_BUFFER_ADDR + i);

        if (c == ':' || i == g_extSRAMBufferIndex) {
            buf[idx] = 0;
            v.ui = strtoul(buf, NULL, 16);

            switch (field) {
            case 0:
                if (v.ui > EXT_EEPROM_MAX_ADDRESS) {
                    printf("R;03;\r\n");
                    return;
                }
                ee_address = v.ui;
                break;
            case 1:
            {
                uint8_t length = (uint8_t)v.ui;

                // max: 10
                // a   l
                // 5 + 8 = 13 -> l = 8 - (5 + 8 - 10)

                if (ee_address + length > EXT_EEPROM_MAX_ADDRESS + 1) {
                    length -= ee_address + length - EXT_EEPROM_MAX_ADDRESS - 1;
                }

                if (length == 0) {
                    printf("R;01;\r\n");
                    break;
                }

                CRC16_Reset();

                printf("R;00:%02X", length);
                EEPROM_BeginReadTransaction(ee_address);
                while (length--) {
                    uint8_t data = EEPROM_ReadTransactionGetByte();
                    printf(":%02X", data);
                    CRC16_Update(data);
                }
                EEPROM_CommitReadTransaction();
                printf(":%04X;\r\n", CRC16_GetValue());

                break;
            }

            default: return;
            }

            ++field;
            idx = 0;
        } else {
            buf[idx++] = c;
        }
    }
}

//--------------------------------------------------------------------


void ParseEEPROMWriteData()
{
    char buf[10];
    uint8_t idx = 0, field = 0;
    DecomposedInt32 v;
    uint24_t ee_address;
    uint8_t ee_data_length;

    for (uint16_t i = 0; i <= g_extSRAMBufferIndex; ++i) {
        char c = SRAM_ReadByte(RAM_LOC_UART_BUFFER_ADDR + i);

        if (c == ':' || i == g_extSRAMBufferIndex) {
            buf[idx] = 0;
            v.ui = strtoul(buf, NULL, 16);

            switch (field) {
            case 0: ee_address = v.ui;
                break;
            case 1: ee_data_length = v.ui;
                break;
            default: break;
            }

            if (field >= 2 && field < ee_data_length + 2) {
                if (field == 2) {
                    CRC16_Reset();
                    EEPROM_BeginWriteTransaction(ee_address);
                }

                CRC16_Update(v.ui);

                // Write data to the EEPROM
                if (!EEPROM_CommitWriteTransaction(v.ui)) {
                    // MAX_ADDRESS reached
                    // Result: 02:[WRITTEN_LEN]:[CRC16]
                    printf("W;02:%02X:%04X;\r\n", field - 2, CRC16_GetValue());
                    return;
                }
            } else if (field == ee_data_length + 2) {
                EEPROM_CommitReadTransaction();

                if (CRC16_GetValue() != v.ui) {
                    printf("W;01;\r\n");
                } else {
                    printf("W;00;\r\n");
                }

                return;
            }

            ++field;
            idx = 0;
        } else {
            buf[idx++] = c;
        }
    }
}

//--------------------------------------------------------------------


void ParseIconData()
{
    char buf[10];
    uint8_t idx = 0;
    DecomposedInt32 v;

    for (uint16_t i = 0; i <= g_extSRAMBufferIndex; ++i) {
        //char c = sram_buf[i];
        char c = SRAM_ReadByte(RAM_LOC_UART_BUFFER_ADDR + i);

        if (i == g_extSRAMBufferIndex) {
            buf[idx] = 0;
            v.ui = strtoul(buf, NULL, 10);

            Image_DrawWeatherIcon(0, 0, v.ui & 0xFF);
            GLCD_WriteBuffer(0);
        } else {
            buf[idx++] = c;
        }
    }
}