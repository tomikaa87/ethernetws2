/**
 * Ethernet Weather Station 2 Firmware
 * Keypad peripheral module
 *
 * @author Tamas Karpati
 * @date 2013-05-03
 */

#include "keypad.h"
#include "hardware_profile.h"
#include "tick.h"

/*** Configuration ************************************************************/

// Number of cycles to wait between reading two buttons
static const uint8_t ScanDelay = 4;
// Number of key scans at every read instruction
static const uint8_t ReadSamples = 20;
// Number of ticks before repeating
static const uint8_t DelayBeforeRepeat = 45;
// Maximum number of ticks to wait between repeats
static const uint8_t RepeatDelayBegin = 10;
// Minimum number of tick to wait between repeats
static const uint8_t RepeatDelayEnd = 1;
// Ticks to wait before decreasing repeat wait
static const uint8_t RepeatSpeedUpDelay = 40;
// Delay after resetting, it gives some more bounce filtering
static const uint8_t ResetDelay = 20;

/*** Globals ******************************************************************/

static uint32_t g_pressTicks = 0, g_repeatTicks = 0;
static uint8_t g_repeatDelay;

static enum {
    S_IDLE,
    S_PRESSED,
    S_REPEAT
} g_state = S_IDLE;

#ifndef _DEBUG_SIM_MODE
#define READ_DELAY()    { _delay(ScanDelay); }
#else
#define READ_DELAY() {}
#endif

/*** Forward declarations *****************************************************/

static void scan(uint16_t *pressedKeys);
static void delayTicks(uint32_t ticks);


/*** API functions ************************************************************/

void Keypad_Init()
{
    KEY_ROW_1_TRIS = 0;
    KEY_ROW_2_TRIS = 0;
    KEY_ROW_3_TRIS = 0;
    KEY_COL_1_TRIS = 1;
    KEY_COL_2_TRIS = 1;
    KEY_COL_3_TRIS = 1;
    KEY_ROW_1 = 0;
    KEY_ROW_2 = 0;
    KEY_ROW_3 = 0;
}


uint16_t Keypad_Task()
{
    uint16_t pressedKeys = 0;
    static uint16_t lastPressedKeys = 0;

    scan(&pressedKeys);

    switch (g_state) {
        // Idle state, no keys are being pressed
    case S_IDLE:
        // Key(s) pressed
        if (pressedKeys > 0) {
            g_state = S_PRESSED;
            g_pressTicks = Tick_GetCurrent();
            lastPressedKeys = pressedKeys;
            return pressedKeys;
        }
        break;

    case S_PRESSED:
        // Pressed key(s) changed, reset
        if (lastPressedKeys != pressedKeys) {
            g_state = S_IDLE;
            delayTicks(ResetDelay);
        } else {
            // Press time reached the repeat limit, enter repeat state
            if (Tick_ElapsedSince(g_pressTicks) > DelayBeforeRepeat) {
                g_repeatTicks = Tick_GetCurrent();
                g_repeatDelay = RepeatDelayBegin;
                g_pressTicks = Tick_GetCurrent();
                g_state = S_REPEAT;
                return (pressedKeys | KEY_LONG_PRESS);
            }
        }
        break;

    case S_REPEAT:
        // Pressed key(s) changed, reset
        if (lastPressedKeys != pressedKeys) {
            g_state = S_IDLE;
            break;
        }
        // Repeat key and reset repeat timer
        if (Tick_ElapsedSince(g_repeatTicks) > g_repeatDelay) {
            g_repeatTicks = Tick_GetCurrent();
            return (pressedKeys | KEY_LONG_PRESS);
        }
        // Decrease repeat delay => speed up repeat
        if (g_repeatDelay > RepeatDelayEnd &&
                Tick_ElapsedSince(g_pressTicks) > RepeatSpeedUpDelay) {
            g_repeatDelay--;
            g_pressTicks = Tick_GetCurrent();
        }
        break;
    }

    return 0;
}

/*** Internal functions *******************************************************/


/**
 * Reads the keypad with software debouncing
 */
static void scan(uint16_t *pressedKeys)
{
    for (uint8_t samples = 0; samples < ReadSamples; ++samples) {
        uint16_t code = 0;

        KEY_ROW_1_TRIS = 0;
        KEY_ROW_1 = 0;
        READ_DELAY();
        if (KEY_COL_1 == 0) {
            code |= KEY_R1_C1;
        }
        READ_DELAY();
        if (KEY_COL_2 == 0) {
            code |= KEY_R1_C2;
        }
        READ_DELAY();
        if (KEY_COL_3 == 0) {
            code |= KEY_R1_C3;
        }
        KEY_ROW_1_TRIS = 1;
        READ_DELAY();

        KEY_ROW_2_TRIS = 0;
        KEY_ROW_2 = 0;
        READ_DELAY();
        if (KEY_COL_1 == 0) {
            code |= KEY_R2_C1;
        }
        READ_DELAY();
        if (KEY_COL_2 == 0) {
            code |= KEY_R2_C2;
        }
        READ_DELAY();
        if (KEY_COL_3 == 0) {
            code |= KEY_R2_C3;
        }
        KEY_ROW_2_TRIS = 1;
        READ_DELAY();

        KEY_ROW_3_TRIS = 0;
        KEY_ROW_3 = 0;
        READ_DELAY();
        if (KEY_COL_1 == 0) {
            code |= KEY_R3_C1;
        }
        READ_DELAY();
        if (KEY_COL_2 == 0) {
            code |= KEY_R3_C2;
        }
        READ_DELAY();
        if (KEY_COL_3 == 0) {
            code |= KEY_R3_C3;
        }
        KEY_ROW_3_TRIS = 1;
        READ_DELAY();

        *pressedKeys |= code;
    }
}


static void delayTicks(uint32_t ticks)
{
    uint32_t start = Tick_GetCurrent();
    while (Tick_ElapsedSince(start) < ticks)
        CLRWDT();
}