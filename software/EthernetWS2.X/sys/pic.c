/**
 * Ethernet Weather Station 2 Firmware
 * PIC control module
 *
 * @author Tamas Karpati <tomikaa87>
 * @date 2012. okt�ber 28.
 */

#include "pic.h"
#include "serial.h"

#include <xc.h>


/**
 * Sets up the PIC.
 */
void PIC_Setup()
{
    // Wait for external oscillator to stabilize (IESO)
    while (!OSTS)
        continue;

    // Peripherals
    ADCON1 = 0x0F;      // Disable analog ports
    CVRCON = 0;         // Disable voltage reference
    CMCON = 0x07;       // Disable comparator

    // Disable ECAN module
    CANCON = 0b00100000;
    TRISB3 = 0;
    LATB3 = 1;
    while (CANSTAT & 0x80)
        continue;

    // Interrupts
    IPEN = 1;           // Enable interrupt priorities
    GIEH = 1;           // Global interrupt enable (high)
    GIEL = 1;           // Global interrupt enable (low)
    RCIP = 1;           // USART RX interrupt = high priority
    RCIE = 1;

    // Setup Timer1 as RTC
    T1CON = 0b11001010; // 16 bit, Timer1 OSC, 1:1, OSC on, SYNC, ext. OSC
    T1CON = 0x0F;
    TMR1H = 0x80;       // Preload Timer1 for 1 sec interrupt
    TMR1L = 0;
    TMR1IP = 0;         // Low priority interrupt
    TMR1IF = 0;         // Clear interrupt flag
    TMR1IE = 1;         // Enable interrupt
    TMR1ON = 1;         // Timer1 ON

    // Setup Timer3 as internal tick system timer
    T3CON = 0b10100001; // 16-bit, 1:4, Fosc/4, enabled
    TMR3H = 0x9E;
    TMR3L = 0x58;
    TMR3IP = 0;
    TMR3IF = 0;
    TMR3IE = 1;
}


/**
 * Initializes the PIC's PWM module and Timer2 to be used by PWM.
 */
void PIC_PWMInit()
{
    TRISC2 = 0;

    /*
     * PWM registers configuration
     * Fosc = 40000000 Hz
     * Fpwm = 25000.00 Hz (Requested : 25000 Hz)
     * Duty Cycle = 50 %
     * Resolution is 10 bits
     * Prescaler is 16
     * Ensure that your PWM pin is configured as digital output
     * see more details on http://www.micro-examples.com/
     * this source code is provided 'as is',
     * use it at your own risks
     */
    PR2 = 0b00011000 ;
    T2CON = 0b00000111 ;
    CCPR1L = 0b00001100 ;
    CCP1CON = 0b00011100 ;
}


/**
 * Changes the PWM duty cycle to \p duty.
 */
void PIC_SetLCDBacklightPWMDutyCycle(uint16_t dc)
{
    // Write 8 MSbits of duty to CCPR1L
    CCPR1L = (dc >> 2) & 0xFF;
    // Write 2 LSBits of duty to DC1B0 and DC1B1
    DC1B0 = (dc & 1) ? 1 : 0;
    DC1B1 = (dc & 2) ? 1 : 0;
}