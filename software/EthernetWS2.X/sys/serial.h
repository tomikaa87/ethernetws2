/**
 * Ethernet Weather Station 2 Firmware
 * Serial interface
 *
 * @author Tamas Karpati <tomikaa87>
 * @date 2012. okt�ber 28.
 */

#ifndef SERIAL_H
#define	SERIAL_H

void Serial_Init();
void putch(char c);
char getch();
void putbufch(char c);
char getbufch();
void Serial_Task();

#endif	/* SERIAL_H */

