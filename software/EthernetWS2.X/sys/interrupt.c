/**
 * Ethernet Weather Station 2 Firmware
 * Interrupt Service Routines module
 *
 * @author Tamas Karpati <tomikaa87>
 * @date 2012. november 6.
 */

#include "interrupt.h"
#include "TCPIP.h"
#include "serial.h"
#include "scheduler.h"
#include "net/ntp.h"
#include "tick.h"

#include <xc.h>


void interrupt Interrupt_Handler()
{
    // Serial interrupt
    if (RCIE && RCIF) {
        putbufch(RCREG);
    }
}


void interrupt low_priority Interrupt_LowPrioHandler()
{
    // Interrupt for TCP/IP Stack
    if (TMR0IE && TMR0IF) {
        TickUpdate();
        TMR0IF = 0;
    }

    // RTC timer interrupt
    if (TMR1IE && TMR1IF) {
        TMR1H |= 0x80; // Preload for 1 sec overflow
        ++g_UTCTime;
        ++g_elapsedSeconds;
        Scheduler_Tick();
        TMR1IF = 0;
    }

    // Tick system timer interrupt
    if (TMR3IE && TMR3IF) {
        TMR3H = 0x9E;
        TMR3L = 0x58;
        ++g_tickCount;
        TMR3IF = 0;
    }
}