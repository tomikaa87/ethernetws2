/**
 * Ethernet Weather Station 2 Firmware
 * Settings storage module
 *
 * @author Tamas Karpati <tomikaa87>
 * @date 2012. november 18.
 */

#include "settings.h"
#include "crc16.h"
#include <xc.h>

#include <stdio.h>
#include <string.h>

#define INT_EEPROM_LOC_SETTINGS_ADDR        0

Settings settings;

static uint8_t readEEPROM(uint16_t address);
static void writeEEPROM(uint16_t address, uint8_t data);


uint16_t calculateSettingsCRC()
{
    // Calculate checksum of data (excluding checksum field)
    // !! checksum field must be the first in the settings structure !!
    CRC16_Reset();
    for (uint8_t i = sizeof (settings.Checksum); i < sizeof (Settings); i++)
        CRC16_Update(settings.data[i]);
    return CRC16_GetValue();
}


/**
 * Loads settings from the internal EEPROM.
 */
void Settings_Load()
{
    uint8_t dataIndex = 0;

    printf("Loading settings\r\n");

    for (uint16_t i = INT_EEPROM_LOC_SETTINGS_ADDR;
            i < INT_EEPROM_LOC_SETTINGS_ADDR + sizeof (Settings); i++) {
        settings.data[dataIndex++] = readEEPROM(i);
    }

    if (settings.Checksum != calculateSettingsCRC()) {
        printf("Settings data corrupted\r\n");
        Settings_RestoreDefaults();
    }
}


/**
 * Saves settings to the internal EEPROM.
 */
void Settings_Save()
{
    uint8_t dataIndex = 0;

    printf("Saving settings\r\n");

    settings.Checksum = calculateSettingsCRC();

    // Write data to the EEPROM
    for (uint16_t i = INT_EEPROM_LOC_SETTINGS_ADDR;
            i < INT_EEPROM_LOC_SETTINGS_ADDR + sizeof (Settings); i++) {
        if (readEEPROM(i) != settings.data[dataIndex])
            writeEEPROM(i, settings.data[dataIndex]);
        dataIndex++;
    }
}


void Settings_RestoreDefaults()
{
    printf("Restoring default settings\r\n");

    memset(settings.data, 0, sizeof (Settings));

    settings.Display.DimLevel = 10;
    settings.Display.DimmingEnd = 7;
    settings.Display.DimmingStart = 23;
    settings.Display.NormalLevel = 60;
    settings.Display.Flags.AutoBacklightControl = 1;

    settings.Weather.UpdateIntervalMinutes = 5;
    settings.Weather.TendencyUpdateIntervalMinutes = 60;

    settings.DateTime.TimeZoneOffsetHours = 1;

    Settings_Save();
}


static uint8_t readEEPROM(uint16_t address)
{
    EEADRH = (address >> 8) & 0x03;
    EEADR = address & 0x0ff;
    EECON1bits.CFGS = 0;
    EECON1bits.EEPGD = 0;
    EECON1bits.RD = 1;
    Nop(); // NOP may be required for latency at high frequencies
    Nop();
    return EEDATA;
}


static void writeEEPROM(uint16_t address, uint8_t data)
{
    while (EECON1bits.WR);
    EEADRH = (address >> 8) & 0x03;
    EEADR = (address & 0x0ff);
    EEDATA = data;
    EECON1bits.EEPGD = 0;
    EECON1bits.CFGS = 0;
    EECON1bits.WREN = 1;
    uint8_t gieReg = INTCONbits.GIE;
    INTCONbits.GIE = 0;
    EECON2 = 0x55;
    EECON2 = 0xAA;
    EECON1bits.WR = 1;
    while (EECON1bits.WR);
    INTCONbits.GIE = gieReg;
    EECON1bits.WREN = 0;
}