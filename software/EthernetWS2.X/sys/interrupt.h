/**
 * Ethernet Weather Station 2 Firmware
 * Interrupt Service Routines module
 *
 * @author Tamas Karpati <tomikaa87>
 * @date 2012. november 6.
 */

#ifndef INTERRUPT_H
#define INTERRUPT_H

void interrupt Interrupt_Handler();
void interrupt low_priority Interrupt_LowPrioHandler();

#endif  /* INTERRUPT_H */

