/**
 * Ethernet Weather Station 2 Firmware
 * PIC control module
 *
 * @author Tamas Karpati <tomikaa87>
 * @date 2012. okt�ber 28.
 */

#ifndef PIC_H
#define	PIC_H

#include <stdint.h>

void PIC_Setup();

void PIC_PWMInit();
void PIC_SetLCDBacklightPWMDutyCycle(uint16_t dc);

#endif	/* PIC_H */

