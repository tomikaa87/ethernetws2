//
// Ethernet Weather Station 2
// Sensor data handler
//
// Author: Tamas Karpati
// This file was created on 2012-10-28
//

#ifndef SENSOR_H
#define	SENSOR_H

#include <stdint.h>

extern bit g_sensorDataUpdated;
extern bit g_sensorHasData;
extern bit g_sensorHasOnlyLocalData;

// Sensor data storage structure

typedef struct {

    struct {
        int16_t TempBMP;
        int16_t TempDHT;
        uint16_t RH;
        uint32_t Pressure;
        uint32_t SeaLevelPressure;
        uint16_t Vdd;
        uint8_t ErrorCode;
    } Outdoor;

    struct {
        int8_t Temp;
        uint8_t RH;
        uint16_t Vdd;
        uint8_t ErrorCode;
    } Indoor;
} SensorData;

extern SensorData g_sensorData;

void RemoteSensor_Init();

#define RemoteSensor_GetData()             g_sensorData
#define RemoteSensor_IsUpdated()           g_sensorDataUpdated
#define RemoteSensor_HasOnlyLocalData()    g_sensorHasOnlyLocalData
#define RemoteSensor_SetUpdatedFlag()      g_sensorDataUpdated = 1; g_sensorHasData = 1; g_sensorHasOnlyLocalData = 0
#define RemoteSensor_SetLocalUpdatedFlag() g_sensorDataUpdated = 1; g_sensorHasData = 1; g_sensorHasOnlyLocalData = 1
#define RemoteSensor_ClearUpdatedFlag()    g_sensorDataUpdated = 0

#endif	/* SENSOR_H */

