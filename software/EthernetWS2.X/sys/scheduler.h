//
// Ethernet Weather Station 2
// Scheduler
//
// Author: Tamas Karpati
// This file was created on 2014-02-01
//

#ifndef SCHEDULER_H
#define SCHEDULER_H

#include <stdint.h>

#define WIRELESS_SENSOR_TIMEOUT         300

extern volatile uint16_t g_elapsedSinceLastDownload;
extern volatile uint16_t g_elapsedSinceLastSensorUpdate;

void Scheduler_Init();
void Scheduler_Task();
void Scheduler_Tick();
void Scheduler_Suspend();

#define ElapsedSinceLastDownload()      g_elapsedSinceLastDownload
#define ElapsedSinceLastSensorUpdate()  g_elapsedSinceLastSensorUpdate

#endif // SCHEDULER_H