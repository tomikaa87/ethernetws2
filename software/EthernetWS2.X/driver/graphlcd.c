/**
 * Ethernet Weather Station 2 Firmware
 * Unified GLCD library
 *
 * @author Tamas Karpati <tomikaa87>
 * @date 2011. szeptember 29.
 */

#include "graphlcd.h"

#include <stdlib.h>


/**
 * Draws a rectangle on the LCD (or in the screen buffer). The top-left corner
 * of the rectangle is defined by @p x1 and @p y1, the bottom-right corner by
 * @p x2 and @p y2. If the @p fill parameter is 1, the rectangle will be filled
 * with the given @p color, otherwise only the frame will be drawn.
 */
void GLCD_DrawRect(uint8_t x1, uint8_t y1, uint8_t x2, uint8_t y2, uint8_t fill,
                   uint8_t color)
{
    uint8_t i, j;

    if (fill) {
        for (i = x1; i <= x2; i++)
            for (j = y1; j <= y2; j++)
                GLCD_SetPixel(i, j, color);
    } else {
        for (i = x1; i <= x2; i++) {
            GLCD_SetPixel(i, y1, color);
            GLCD_SetPixel(i, y2, color);
        }
        for (i = y1; i <= y2; i++) {
            GLCD_SetPixel(x1, i, color);
            GLCD_SetPixel(x2, i, color);
        }
    }
}


/**
 * Draws a picture on the LCD to the given @p x and @p y coordinates. The
 * size of the picture is held by @p width and @p height. If the @p invert is
 * set to 1, the picture will be drawn inverted.
 */
void GLCD_DrawBitmap(uint8_t *image, uint8_t width, uint8_t height, uint8_t x,
                     uint8_t y, uint8_t invert)
{
    uint8_t i, j, k;

    for (i = 0; i < height; i++)
        for (j = 0; j < width; j++) {
            k = j + i * width;
            GLCD_SetPixel(j + x, i + y, invert ? 1 - image[k] : image[k]);
        }
}


/**
 * Draws a progess bar to the given @p x and @p y coordinates. The @p value
 * holds the current value of the bar, the @p max determines the maximum value.
 * The @p width and @p height parameters hold the size of the bar. If the
 * @p invert is set to 1, the bar will be drawn inverted.
 */
void GLCD_DrawProgressBar(uint8_t value, uint8_t max, uint8_t x, uint8_t y,
                          uint8_t width, uint8_t height, uint8_t invert)
{
    uint8_t length;

    length = (uint8_t) ((float) value / (float) max * (float) width);

    GLCD_DrawRect(x, y, length + x - 1, height + y - 1, 1, 1 - invert);

    if (length < width - 1)
        GLCD_DrawRect(x + length, y, x + width - 1, height + y - 1, 1, invert);
}


/**
 * Draws a line using Bresenham's line algorithm from the given @p x1 and @p y1
 * corrdinates to the given @p x2 and @p y2 coordinates. The @p color parameter
 * determines the color of the line (1 = pixel on, 0 = pixel off).
 */
void GLCD_DrawLine(uint8_t x1, uint8_t y1, uint8_t x2, uint8_t y2, uint8_t color)
{
    uint8_t steep;
    uint8_t tmp, deltaX, deltaY, x, y;
    int8_t yStep, error;

    steep = abs(y2 - y1) > abs(x2 - x1);

    if (steep) {
        tmp = x1;
        x1 = y1;
        y1 = tmp;

        tmp = x2;
        x2 = y2;
        y2 = tmp;
    }

    if (x1 > x2) {
        tmp = x1;
        x1 = x2;
        x2 = tmp;

        tmp = y1;
        y1 = y2;
        y2 = tmp;
    }

    deltaX = x2 - x1;
    deltaY = abs(y2 - y1);
    error = deltaX / 2;
    y = y1;

    if (y1 < y2)
        yStep = 1;
    else
        yStep = -1;

    for (x = x1; x <= x2; x++) {
        if (steep)
            GLCD_SetPixel(y, x, color);
        else
            GLCD_SetPixel(x, y, color);

        error = error - deltaY;
        if (error < 0) {
            y = y + yStep;
            error = error + deltaX;
        }
    }
}


/**
 * Draws a filled rectangle on the LCD (or in the screen buffer).
 * The top-left corner of the rectangle is defined by @p x1 and @p y1,
 * the bottom-right corner by @p x2 and @p y2. The color of the rectangle's
 * pixels will be the inverse of the corresponding pixel of the background.
 */
void GLCD_DrawInverseRect(uint8_t x1, uint8_t y1, uint8_t x2, uint8_t y2)
{
    uint8_t x, y, x_start, x_end, y_start, y_end;

    x_start = x1 <= x2 ? x1 : x2;
    x_end = x1 >= x2 ? x1 : x2;
    y_start = y1 <= y2 ? y1 : y2;
    y_end = y1 >= y2 ? y1 : y2;

    for (x = x_start; x <= x_end; x++)
        for (y = y_start; y <= y_end; y++)
            GLCD_SetPixel(x, y, !GLCD_GetPixel(x, y));
}