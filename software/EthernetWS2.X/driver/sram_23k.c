/**
 * Ethernet Weather Station 2 Firmware
 * External SPI SRAM (23Kxxx) driver
 *
 * @author Tamas Karpati <tomikaa87>
 * @date 2011. okt�ber 9.
 */

#include "sram_23k.h"

#ifdef HARDWARE_PROFILE_H
// EXT_EEPROM_CS must not be touched
#define LOCK_SPI_BUS() { /*EXT_EEPROM_CS = 1;*/ ENC_CS_IO = 1; }
#else
#define LOCK_SPI_BUS()
#endif

/*** Global variables *********************************************************/

static enum {
    M_BYTE          = 0b00000000,
    M_SEQUENTIAL    = 0b01000000,
    M_PAGE          = 0b10000000
} g_mode = M_BYTE;

/*** Constant definitions *****************************************************/

static enum {
    CMD_READ            = 0b00000011,
    CMD_WRITE           = 0b00000010,
    CMD_READ_STATUS     = 0b00000101,
    CMD_WRITE_STATUS    = 0b00000001
} ;

/*** Low level functions ******************************************************/

#define ClearSSPIF()        { EXT_SRAM_SSPIF = 0; }
#define WaitForData()       { while (!EXT_SRAM_SSPIF); EXT_SRAM_SSPIF = 0; }


/**
 * Sends a \p byte through SPI.
 */
static void writeByte(uint8_t byte)
{
#ifdef EXT_SRAM_USE_HW_SPI
    ClearSSPIF();
    EXT_SRAM_SSPBUF = byte;
    WaitForData();
#else
    uint8_t i, data;

    for (i = 0; i < 8; i++) {
        data = (byte & (1 << (7 - i))) > 0;
        EXT_SRAM_SDO = data;
        EXT_SRAM_SCK = 1;
        EXT_SRAM_SCK = 0;
    }
#endif
}


/**
 * Reads a byte from the SPI bus.
 */
static uint8_t readByte()
{
#ifdef EXT_SRAM_USE_HW_SPI
    ClearSSPIF();
    EXT_SRAM_SSPBUF = 0; // Dummy byte to generate SCK
    WaitForData();
    return EXT_SRAM_SSPBUF;
#else
    uint8_t i, data;

    data = 0;
    for (i = 0; i < 8; i++) {
        data |= EXT_SRAM_SDI << (7 - i);
        EXT_SRAM_SCK = 1;
        EXT_SRAM_SCK = 0;
    }

    return data;
#endif
}


/**
 * Sends the given 16 bit \p address to the SRAM.
 */
static void writeAddress(uint16_t address)
{
    // Write address<15:8> byte
    writeByte((uint8_t) ((address >> 8) & 0xFF));
    // Write address<7:0> byte
    writeByte((uint8_t) (address & 0xFF));
}


/**
 * Reads the Status Register.
 */
static uint8_t readStatusReg()
{
    writeByte(CMD_READ_STATUS);
    return readByte();
}


/**
 * Writes the given \p data to the Status Register.
 */
static void writeStatusReg(uint8_t data)
{
    writeByte(CMD_WRITE_STATUS);
    writeByte(data);
}


/**
 * Changes the data mode (byte, page, sequential).
 */
static void setMode(uint8_t mode)
{
    if (mode != g_mode) {
        EXT_SRAM_CS = 0;

        writeStatusReg(mode);
        g_mode = mode;

        EXT_SRAM_CS = 1;
    }
}

/*** API function implementations *********************************************/


/**
 * Initializes the SPI SRAM and the SPI bus.
 */
void SRAM_Init()
{
    EXT_SRAM_CS = 1;
    EXT_SRAM_CS_TRIS = 0;

    EXT_SRAM_SDI_TRIS = 1;
    EXT_SRAM_SCK_TRIS = 0;
    EXT_SRAM_SDO_TRIS = 0;

#ifdef EXT_SRAM_USE_HW_SPI
#ifndef EXT_SRAM_DONT_MODIFY_SSPCON
    EXT_SRAM_SSPCON1 = EXT_SRAM_SSPCON1_VALUE;
    EXT_SRAM_SSPSTATbits.CKE = 1;
    EXT_SRAM_SSPSTATbits.SMP = 0;
#endif
#endif
}


/**
 * Writes the given \p byte to the given \p address. If the address is above
 * the maximum allowed, the function returs 0, otherwise 1.
 */
uint8_t SRAM_WriteByte(uint16_t address, uint8_t byte)
{
#ifdef EXT_SRAM_DO_SANITY_CHECK
    if (address > EXT_SRAM_MAX_ADDRESS)
        return 0;
#endif

    // Get control over the SPI bus
    LOCK_SPI_BUS();

    setMode(M_BYTE);

    EXT_SRAM_CS = 0;

    writeByte(CMD_WRITE);
    writeAddress(address);
    writeByte(byte);

    EXT_SRAM_CS = 1;

    ClearSSPIF();

    return 1;
}


/**
 * Reads a byte from the given \p address. If the address is above the
 * maximum allowed, the function returns 0xFF, otherwise the read byte.
 */
uint8_t SRAM_ReadByte(uint16_t address)
{
    uint8_t data;

#ifdef EXT_SRAM_DO_SANITY_CHECK
    if (address > EXT_SRAM_MAX_ADDRESS)
        return 0;
#endif

    // Get control over the SPI bus
    LOCK_SPI_BUS();

    setMode(M_BYTE);

    EXT_SRAM_CS = 0;

    writeByte(CMD_READ);
    writeAddress(address);
    data = readByte();

    EXT_SRAM_CS = 1;

    ClearSSPIF();

    return data;
}


/**
 * Writes the content of the given \p buf buffer into the given \p page of the
 * SRAM. Page offset can be adjusted by \p offset. The size of the buffer is
 * held by \p bufSize.
 * The function returns the number of written bytes or 0 if there was an error.
 */
uint8_t SRAM_WritePage(uint16_t page, uint8_t offset, uint8_t *buf, uint8_t length)
{
    uint8_t buf_ptr;

#ifdef EXT_SRAM_DO_SANITY_CHECK
    if (page > EXT_SRAM_MAX_PAGE)
        return 0;

    if (offset > (uint8_t) EXT_SRAM_PAGE_SIZE - 1)
        return 0;

    if (length > (uint8_t) EXT_SRAM_PAGE_SIZE - offset)
        return 0;
#endif

    // Get control over the SPI bus
    LOCK_SPI_BUS();

    setMode(M_PAGE);

    EXT_SRAM_CS = 0;

    writeByte(CMD_WRITE);
    writeAddress(EXT_SRAM_PAGE_SIZE * page + (uint16_t) offset);

    buf_ptr = 0;
    while (buf_ptr < length)
        writeByte(buf[buf_ptr++]);

    EXT_SRAM_CS = 1;

    ClearSSPIF();

    return buf_ptr;
}


/**
 * Reads the content of the given \p page of the SRAM and copies it to the given
 * \p buf buffer. Page offset can be adjusted by \p offset. The size of the
 * buffer is held by \p bufSize.
 * The function returns the number of read bytes or 0 if there was an error.
 */
uint8_t SRAM_ReadPage(uint16_t page, uint8_t offset, uint8_t *buf, uint8_t length)
{
    uint8_t buf_ptr;

#ifdef EXT_SRAM_DO_SANITY_CHECK
    if (page > EXT_SRAM_MAX_PAGE)
        return 0;

    if (offset > (uint8_t) EXT_SRAM_PAGE_SIZE - 1)
        return 0;

    if (length > (uint8_t) EXT_SRAM_PAGE_SIZE - offset)
        return 0;
#endif

    // Get control over the SPI bus
    LOCK_SPI_BUS();

    EXT_SRAM_CS = 0;

    writeByte(CMD_READ);
    writeAddress(EXT_SRAM_PAGE_SIZE * page + (uint16_t) offset);

    buf_ptr = 0;
    while (buf_ptr < length)
        buf[buf_ptr++] = readByte();

    EXT_SRAM_CS = 1;

    ClearSSPIF();

    return buf_ptr;
}


/**
 * Writes the content of the given \p buf buffer to the SRAM. The begining
 * address is held by \p address and buffer size by \p bufSize.
 * The function returns the number of written bytes of 0 if there was an error.
 */
uint16_t SRAM_WriteBuffer(uint16_t address, uint8_t *buf, uint16_t length)
{
    uint16_t buf_ptr;

#ifdef EXT_SRAM_DO_SANITY_CHECK
    if (address > EXT_SRAM_MAX_ADDRESS)
        return 0;
#endif

    // Get control over the SPI bus
    LOCK_SPI_BUS();

    setMode(M_SEQUENTIAL);

    EXT_SRAM_CS = 0;

    writeByte(CMD_WRITE);
    writeAddress(address);

    buf_ptr = 0;
    while (buf_ptr < length && buf_ptr <= EXT_SRAM_MAX_ADDRESS)
        writeByte(buf[buf_ptr++]);

    EXT_SRAM_CS = 1;

    ClearSSPIF();

    return buf_ptr;
}


/**
 * Reads the contents of the SRAM from the given \p address. Read data is
 * copied into the given \p buf buffer. The buffer size is held by \p bufSize.
 * The function returns the number of read bytes or 0 if there was an error.
 */
uint16_t SRAM_ReadBuffer(uint16_t address, uint8_t *buf, uint16_t length)
{
    uint16_t buf_ptr;

#ifdef EXT_SRAM_DO_SANITY_CHECK
    if (address > EXT_SRAM_MAX_ADDRESS)
        return 0;
#endif

    // Get control over the SPI bus
    LOCK_SPI_BUS();

    setMode(M_SEQUENTIAL);

    EXT_SRAM_CS = 0;

    writeByte(CMD_READ);
    writeAddress(address);

    buf_ptr = 0;
    while (buf_ptr < length && buf_ptr <= EXT_SRAM_MAX_ADDRESS)
        buf[buf_ptr++] = readByte();

    EXT_SRAM_CS = 1;

    ClearSSPIF();

    return buf_ptr;
}

/*** Performance and stability tests ******************************************/

#ifdef EXT_SRAM_COMPILE_STRESS_TEST


/**
 * Performs a performance test without checking if the RAM contents
 * are written correctly.
 */
void EXT_SRAM_writePerformanceTest()
{
    uint16_t bufPtr;

    setMode(M_SEQUENTIAL);

    EXT_SRAM_CS = 0;

    writeByte(CMD_WRITE);
    writeAddress(0ul);

    bufPtr = 0;
    while (bufPtr++ <= EXT_SRAM_MAX_ADDRESS)
        writeByte(0xAB);

    EXT_SRAM_CS = 1;

    ClearSSPIF();
}


/**
 * Performs a performance test without checking if the RAM contents
 * are read correctly.
 */
void EXT_SRAM_readPerformanceTest()
{
    uint16_t bufPtr;

    setMode(M_SEQUENTIAL);

    EXT_SRAM_CS = 0;

    writeByte(CMD_READ);
    writeAddress(0ul);

    bufPtr = 0;
    while (bufPtr++ <= EXT_SRAM_MAX_ADDRESS)
        (void)readByte();

    EXT_SRAM_CS = 1;

    ClearSSPIF();
}

#define EXT_SRAM_CRC16_POLYNOMIAL 0x1021
static uint16_t EXT_SRAM_CRC16_runningValue;
#define EXT_SRAM_CRC16_reset() (EXT_SRAM_CRC16_runningValue = 0)
#define EXT_SRAM_CRC16_value() (EXT_SRAM_CRC16_runningValue)


void EXT_SRAM_CRC16_byte(uint8_t ch)
{
    uint8_t i;

    EXT_SRAM_CRC16_runningValue ^= ch;
    i = 8;

    do {
        if (EXT_SRAM_CRC16_runningValue & 0x8000)
            EXT_SRAM_CRC16_runningValue <<= 1;
        else {
            EXT_SRAM_CRC16_runningValue <<= 1;
            EXT_SRAM_CRC16_runningValue ^= EXT_SRAM_CRC16_POLYNOMIAL;
        }
    }    while (--i);
}

uint16_t EXT_SRAM_rnd = 0x1024;


uint16_t EXT_SRAM_random()
{
    uint8_t msb;

    msb = ((EXT_SRAM_rnd >> 0) ^ (EXT_SRAM_rnd >> 2) ^
            (EXT_SRAM_rnd >> 3) ^ (EXT_SRAM_rnd >> 5)) & 1;
    EXT_SRAM_rnd = (EXT_SRAM_rnd >> 1) | (msb << 15);

    return EXT_SRAM_rnd;
}

//#include <stdio.h>


/**
 * Performs a stability test by filling up the RAM with random bytes and
 * checking the CRC of the read data.
 */
uint8_t EXT_SRAM_stabilityTest()
{
    uint16_t bufPtr, crc;
    uint8_t byte;

    setMode(M_SEQUENTIAL);

    // Write random data and calculate CRC
    EXT_SRAM_CS = 0;

    writeByte(CMD_WRITE);
    writeAddress(0ul);

    bufPtr = 0;
    EXT_SRAM_CRC16_reset();
    EXT_SRAM_rnd = 0x1024;
    while (bufPtr < EXT_SRAM_MAX_ADDRESS) {
        byte = (uint8_t) (EXT_SRAM_random() % 256);
        writeByte(byte);
        EXT_SRAM_CRC16_byte(byte);
        bufPtr++;
    }

    crc = EXT_SRAM_CRC16_value();

    EXT_SRAM_CS = 1;

    /*asm("nop");
    asm("nop");*/

    // Read stored data and calculate CRC
    EXT_SRAM_CS = 0;

    writeByte(CMD_READ);
    writeAddress(0ul);

    bufPtr = 0;
    EXT_SRAM_CRC16_reset();
    EXT_SRAM_rnd = 0x1024;
    while (bufPtr < EXT_SRAM_MAX_ADDRESS) {
        byte = readByte();
        EXT_SRAM_CRC16_byte(byte);
        /*if (byte != EXT_SRAM_random() % 256)
            printf("Error @%04X: %u != %u\r\n", bufPtr, byte, EXT_SRAM_rnd % 256);*/
        bufPtr++;
    }

    EXT_SRAM_CS = 1;

    ClearSSPIF();

    return (crc == EXT_SRAM_CRC16_value() ? 1 : 0);
}

#endif // EXT_SRAM_COMPILE_STRESS_TEST
