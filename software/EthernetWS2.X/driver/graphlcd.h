/**
 * Ethernet Weather Station 2 Firmware
 * Unified GLCD library
 *
 * @author Tamas Karpati <tomikaa87>
 * @date 2011. szeptember 29.
 */

#ifndef GRAPHLCD_H
#define GRAPHLCD_H

#include "hardware_profile.h"

#include <stdint.h>

/*** DRIVER CONFIGURATION *****************************************************/

/*
 * Uncomment the following define corresponding to the type of the LCD module
 */
#define GLCD_MODULE_T6963C
//#define GLCD_MODULE_PCD8544

/*
 * Ucomment the following define to use TEXT mode of T6963C
 */
//#define GLCD_MODULE_T6963C_TEXT_MODE

#ifdef GLCD_MODULE_T6963C_TEXT_MODE
#define GLCD_MODULE_T6963C_FONT_6x8
#endif

/*
 * Change these values corresponding to the LCD's resolution
 */
#define GLCD_RESOLUTION_X                   128
#define GLCD_RESOLUTION_Y                   128
//#define GLCD_RESOLUTION_X                   84
//#define GLCD_RESOLUTION_Y                   48

/*
 * Uncomment this define to use buffered drawing.
 * Note, that PCD8544 must use a buffer (504 bytes of RAM needed).
 */
//#define GLCD_USE_DRAWING_BUFFER
#define GLCD_USE_SRAM_DRAWING_BUFFER

/*** SANITY CHECK *************************************************************/

#ifdef GLCD_MODULE_T6963C
#ifdef GLCD_MODULE_PCD8544
#define GLCD_MULTI_MODULE_ERROR
#endif
#endif

#ifdef GLCD_MODULE_PCD8544
#ifdef GLCD_MODULE_T6963C
#define GLCD_MULTI_MODULE_ERROR
#endif
#endif

#ifdef GLCD_MULTI_MODULE_ERROR
#error Only one module can be used at a time.
#endif

#ifdef GLCD_MODULE_PCD8544
#ifndef GLCD_USE_DRAWING_BUFFER
#define GLCD_USE_DRAWING_BUFFER
#endif
#endif

#ifdef GLCD_USE_DRAWING_BUFFER
#define GLCD_BUFFER_SIZE (GLCD_RESOLUTION_X * GLCD_RESOLUTION_Y / 8)
#endif // GLCD_USE_DRAWING_BUFFER

#ifdef GLCD_USE_SRAM_DRAWING_BUFFER
#define GLCD_SRAM_BUFFER_SIZE (GLCD_RESOLUTION_X * GLCD_RESOLUTION_Y / 8)
//#define GLCD_SRAM_BASE_ADDR             0u
#define GLCD_SRAM_BASE_ADDR     RAM_LOC_GLCD_BUFFER
#endif

/*** HARDWARE CONFIGURATION ***************************************************/

/**
 * Change this value corresponding to PIC's Fosc.
 */
#ifndef _XTAL_FREQ
#define _XTAL_FREQ                      40000000ul
#endif

#if (defined GLCD_MODULE_T6963C)
#define GLCD_DATA                       (LATD)
#define GLCD_DATA_READ                  (PORTD)
#define GLCD_DATA_TRIS                  (TRISD)
#define GLCD_WR                         (LATB2)
#define GLCD_WR_TRIS                    (TRISB2)
#define GLCD_RD                         (LATB3)
#define GLCD_RD_TRIS                    (TRISB3)
#define GLCD_CE                         (LATB4)
#define GLCD_CE_TRIS                    (TRISB4)
#define GLCD_CD                         (LATB5)
#define GLCD_CD_TRIS                    (TRISB5)
//#define GLCD_RST                        (LATD4)
//#define GLCD_RST_TRIS                   (TRISD4)
//#define GLCD_FS                         (LATD5)
//#define GLCD_FS_TRIS                    (TRISD5)

#define GLCD_STA0                       (PORTDbits.RD0)
#define GLCD_STA1                       (PORTDbits.RD1)
#define GLCD_STA2                       (PORTDbits.RD2)
#define GLCD_STA3                       (PORTDbits.RD3)
#define GLCD_STA4                       (PORTDbits.RD4)
#define GLCD_STA5                       (PORTDbits.RD5)
#define GLCD_STA6                       (PORTDbits.RD6)
#define GLCD_STA7                       (PORTDbits.RD7)
#elif (defined GLCD_MODULE_PCD8544)
#define PCD_GLCD_USE_HW_SPI

/**
 * Uncomment the define corresponding to the desired SPI clock frequency.
 * PCD8544 can handle maximum 4 MHz.
 */
//#define PCD_GLCD_SPI_CLK_FOSC_64
#define PCD_GLCD_SPI_CLK_FOSC_16
//#define PCD_GLCD_SPI_CLK_FOSC_4

#define PCD_GLCD_SCK                    (LATC3)
#define PCD_GLCD_SCK_TRIS               (TRISC3)
#define PCD_GLCD_MOSI                   (LATC5)
#define PCD_GLCD_MOSI_TRIS              (TRISC5)
#define PCD_GLCD_SCE                    (LATD0)
#define PCD_GLCD_SCE_TRIS               (TRISD0)
#define PCD_GLCD_RST                    (LATD1)
#define PCD_GLCD_RST_TRIS               (TRISD1)
#define PCD_GLCD_DC                     (LATD2)
#define PCD_GLCD_DC_TRIS                (TRISD2)
#define PCD_GLCD_SSPCON1                (SSPCON1)
#define PCD_GLCD_SSPBUF                 (SSPBUF)
#define PCD_GLCD_SSPIF                  (SSPIF)
#endif

/*** GLOBAL CONSTANTS *********************************************************/

// Color Constants
#define GLCD_COLOR_BLACK            1
#define GLCD_COLOR_WHITE            0

// Fill constants
#define GLCD_NO_FILL                0
#define GLCD_FILL                   1

// Flags
#define GLCD_INVERT                 128

// Buffer write options
#define GLCD_WB_NORMAL              0
#define GLCD_WB_INVERTED            1

// Drawing mode for set_buf_byte
#define GLCD_DRAW_NORMAL            0
#define GLCD_DRAW_MODE_OR           1

/*** API FUNCTIONS ************************************************************/

void GLCD_Init();

void GLCD_WriteBuffer(uint8_t invert);
void GLCD_WriteBufferRegion(uint8_t x1, uint8_t y1,
                            uint8_t x2, uint8_t y2, uint8_t invert);

void GLCD_Clear();
void GLCD_ClearBuffer();

void GLCD_SetBufferByte(uint16_t address, uint8_t data, uint8_t draw_mode);

void GLCD_CopyToBuffer(uint8_t x, uint8_t y, uint8_t *data, uint8_t len,
                       uint8_t flags);

void GLCD_SetPosition(uint8_t x, uint8_t y, uint8_t text);

void GLCD_SetPixel(uint8_t x, uint8_t y, uint8_t on);
uint8_t GLCD_GetPixel(uint8_t x, uint8_t y);

void GLCD_DrawRect(uint8_t x1, uint8_t y1, uint8_t x2, uint8_t y2, uint8_t fill,
                   uint8_t color);
void GLCD_DrawBitmap(uint8_t *image, uint8_t width, uint8_t height, uint8_t x, uint8_t y,
                     uint8_t invert);
void GLCD_DrawProgressBar(uint8_t value, uint8_t max, uint8_t x, uint8_t y, uint8_t width,
                          uint8_t height, uint8_t invert);
void GLCD_DrawLine(uint8_t x1, uint8_t y1, uint8_t x2, uint8_t y2, uint8_t color);
void GLCD_DrawInverseRect(uint8_t x1, uint8_t y1, uint8_t x2, uint8_t y2);

#endif // GRAPHLCD_H
