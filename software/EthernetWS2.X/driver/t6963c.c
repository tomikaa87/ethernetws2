/**
 * Ethernet Weather Station 2 Firmware
 * T6963C Graphic LCD Driver
 *
 * @author Tamas Karpati <tomikaa87>
 * @date 2011. okt�ber 29.
 *
 * Written in CCS-C by tomikaa87
 * Modified for HI-TECH PIC C - 2010-09-21
 * Modified for screen buffering - 2011-09-09
 * Modified for Unified GLCD driver - 2011-09-29
 */

#include "graphlcd.h"

#include <xc.h>
#include <stdlib.h>
#include <stdio.h>

#ifdef GLCD_MODULE_T6963C

/*** CONSTANTS ****************************************************************/

// GLCD timing
#define GLCD_COMMAND_TIME           8
#define GLCD_DATA_TIME              8
#define GLCD_READ_TIME              8
#define GLCD_BUSY_CHECK_TIME        8

// GLCD addresses
#define GLCD_GRAPH_HOME_ADDR        0x0000u
#define GLCD_GRAPH_AREA_ADDR        0x0010u
#define GLCD_TEXT_HOME_ADDR         0x0800u
#define GLCD_TEXT_AREA_ADDR         0x0010u
#define GLCD_CGRAM_HOME_ADDR        0x1000u
#define GLCD_GRAPH_HOME_ADDR_6x8    0x0000u
#define GLCD_GRAPH_AREA_ADDR_6x8    0x0016u
#define GLCD_TEXT_HOME_ADDR_6x8     0x0B00u
#define GLCD_TEXT_AREA_ADDR_6x8     0x0016u
#define GLCD_CGRAM_HOME_ADDR_6x8    0x1600u

// GLCD sizes
#define GLCD_MEM_SIZE               2048u
#define GLCD_MEM_SIZE_6x8           2816u

// Register set commands
#define GLCD_CURSOR_PTR_SET         0x21   //Cursor Pointer Set
#define GLCD_OFFSET_REG_SET         0x22   //Offset Register Set
#define GLCD_ADDR_PTR_SET           0x24   //Address Pointer Set

// Control Word Set commands
#define GLCD_TEXT_HOME_SET          0x40   //Text Home Address Set
#define GLCD_TEXT_AREA_SET          0x41   //Text Area Set
#define GLCD_GRAPH_HOME_SET         0x42   //Graphics Home address Set
#define GLCD_GRAPH_AREA_SET         0x43   //Graphics Area Set

// Mode Set commands (OR with CG rom commands)
#define GLCD_OR_MODE                0x80   //OR mode
#define GLCD_XOR_MODE               0x81   //XOR mode
#define GLCD_AND_MODE               0x83   //AND mode
#define GLCD_TEXT_ATTR_MODE         0x84   //Text Attribute mode
#define GLCD_INT_CG_MODE            0x80   //Internal CG ROM mode
#define GLCD_EXT_CG_MODE            0x88   //External CG ROM mode

// Display Mode commands (OR together required bits)
#define GLCD_DISPLAY_OFF            0x90
#define GLCD_BLINK_ON               0x91
#define GLCD_CURSOR_ON              0x92
#define GLCD_TEXT_ON                0x94
#define GLCD_GRAPH_ON               0x98
#define GLCD_TEXT_AND_GRAPH_ON      0x9C

// Cursor Pattern Select
#define GLCD_CURSOR_1LINE           0xA0
#define GLCD_CURSOR_2LINE           0xA1
#define GLCD_CURSOR_3LINE           0xA2
#define GLCD_CURSOR_4LINE           0xA3
#define GLCD_CURSOR_5LINE           0xA4
#define GLCD_CURSOR_6LINE           0xA5
#define GLCD_CURSOR_7LINE           0xA6
#define GLCD_CURSOR_8LINE           0xA7

// Data Auto Read/Write
#define GLCD_DATA_AUTO_WR           0xB0
#define GLCD_DATA_AUTO_RD           0xB1
#define GLCD_AUTO_DATA_RESET        0xB2

// Data Read/Write
#define GLCD_DATA_WR_INC            0xC0   //Data write and increment addr
#define GLCD_DATA_RD_INC            0xC1   //Data read and increment addr
#define GLCD_DATA_WR_DEC            0xC2   //Data write and decrement addr
#define GLCD_DATA_RD_DEC            0xC3   //Data read and decrement addr
#define GLCD_DATA_WR                0xC4   //Data write - no addr change
#define GLCD_DATA_RD                0xC5   //Data read - no addr change

// Screen Peek
#define GLCD_SCREEN_PEEK            0xE0

// Screen Copy
#define GLCD_SCREEN_COPY            0xE8

// Bit Set/Reset (OR with bit number 0-7)
#define GLCD_BIT_RESET              0xF0
#define GLCD_BIT_SET                0xF8

// Font Constants
#define GLCD_FONT_8x8               0
#define GLCD_FONT_6x8               1

#if (defined GLCD_MODULE_T6963C_TEXT_MODE && defined GLCD_MODULE_T6963C_FONT_6x8)
static uint8_t g_font = GLCD_FONT_6x8;
#else
static uint8_t g_font = GLCD_FONT_8x8;
#endif

uint16_t g_graphHomeAddr = GLCD_GRAPH_HOME_ADDR;
uint16_t g_graphArea = GLCD_GRAPH_AREA_ADDR;
uint16_t g_textHomeAddr = GLCD_TEXT_HOME_ADDR;
uint16_t g_textArea = GLCD_TEXT_AREA_ADDR;

#ifdef GLCD_USE_DRAWING_BUFFER
static uint8_t g_screenBuf[GLCD_BUFFER_SIZE];
static uint16_t g_screenBufPtr = 0;
#endif

/*** Stuff for SRAM buffering *************************************************/

#ifdef GLCD_USE_SRAM_DRAWING_BUFFER
#include "sram_23k.h"

//uint8_t sram_buf[32];
//uint16_t sram_base_ptr;
#endif

/*** Low level routines *******************************************************/

#define GLCD_BUSY_CHECK_DELAY()     __delay_us(GLCD_BUSY_CHECK_TIME)
#define GLCD_READ_DELAY()           __delay_us(GLCD_READ_TIME)
#define GLCD_DATA_DELAY()           __delay_us(GLCD_DATA_TIME)
#define GLCD_COMMAND_DELAY()        __delay_us(GLCD_COMMAND_TIME)


/**
 * Checks if the LCD is doing something or not
 */
void isBusy()
{
    uint8_t sta0 = 0, sta1 = 0;

    GLCD_DATA_TRIS = 0xFF;

    do {
        GLCD_CD = 1;
        GLCD_RD = 0;
        GLCD_WR = 1;
        GLCD_CE = 0;
        GLCD_BUSY_CHECK_DELAY();
        sta0 = GLCD_STA0;
        sta1 = GLCD_STA1;
        GLCD_CE = 1;
    } while (!sta0 && !sta1);
}


/**
 * Writes the given @p glcd_data byte to the LCD
 */
void writeByte(uint8_t b)
{
    isBusy();
    GLCD_DATA_TRIS = 0x00;
    GLCD_DATA = b;
    GLCD_CD = 0;
    GLCD_WR = 0;
    GLCD_RD = 1;
    GLCD_CE = 0;
    GLCD_DATA_DELAY();
    GLCD_CE = 1;
}


/**
 * Writes a command to the LCD
 *
 * @param glcd_command The command code
 */
void writeCommand(uint8_t command)
{
    isBusy();
    GLCD_DATA_TRIS = 0x00;
    GLCD_DATA = command;
    GLCD_CD = 1;
    GLCD_WR = 0;
    GLCD_RD = 1;
    GLCD_CE = 0;
    GLCD_COMMAND_DELAY();
    GLCD_CE = 1;
}


/**
 * Writes a command with TWO (2x8 bit) parameters to the LCD
 *
 * @param glcd_data The data parameter for the command (MSB, LSB)
 * @param glcd_command The command code
 */
void writeCommand2Args(uint16_t args, uint8_t command)
{
    writeByte(args & 0xFF);
    writeByte(args >> 8);
    writeCommand(command);
}


/**
 * Writes a command with ONE parameter to the LCD
 *
 * @param glcd_msb The data parameter for the command (MSB)
 * @param glcd_command The command code
 */
void writeCommand1Arg(uint8_t msb, uint8_t command)
{
    writeByte(msb);
    writeCommand(command);
}


/**
 * Reads a byte from the LCD
 */
uint8_t readByte()
{
    uint8_t data;

    writeCommand(GLCD_DATA_RD);
    isBusy();
    GLCD_CD = 0;
    GLCD_RD = 0;
    GLCD_WR = 1;
    GLCD_CE = 0;
    GLCD_READ_DELAY();
    data = GLCD_DATA_READ;
    GLCD_CE = 1;

    return data;
}


/**
 * Reads a data byte from the LCD and increment the LCD's data pointer
 */
uint8_t readByteInc()
{
    uint8_t data;

    writeCommand(GLCD_DATA_RD_INC);
    isBusy();
    GLCD_CD = 0;
    GLCD_RD = 0;
    GLCD_WR = 1;
    GLCD_CE = 0;
    GLCD_READ_DELAY();
    data = GLCD_DATA_READ;
    GLCD_CE = 1;

    return data;
}


/**
 * Sets the memory address pointer of the LCD controller (or the screen buffer)
 * to the given @p address.
 */
void setAddressPtr(uint16_t address)
{
#ifdef GLCD_USE_DRAWING_BUFFER
    if (address >= sizeof (g_screenBuf))
        return;

    g_screenBufPtr = address;
#else
    writeCommand2Args(address, GLCD_ADDR_PTR_SET);
#endif
}


/**
 * Writes the given @p byte into the LCD controller memory
 * (or the screen buffer).
 */
void writeDataByte(uint8_t byte)
{
#ifdef GLCD_USE_DRAWING_BUFFER
    g_screenBuf[g_screenBufPtr] = byte;
#else
    writeCommand1Arg(byte, GLCD_DATA_WR);
#endif
}


/**
 * Writes the given @p byte into the LCD controller memory
 * (or the screen buffer) and increments the memory address pointer.
 */
void GLCD_WriteByteIncremented(uint8_t byte)
{
#ifdef GLCD_USE_DRAWING_BUFFER
    g_screenBuf[g_screenBufPtr++] = byte;
#else
    writeCommand1Arg(byte, GLCD_DATA_WR_INC);
#endif
}

/*** High level routines ******************************************************/


/**
 * Initializes the LCD and clears its RAM. If buffering is enabled, this
 * function clears the screen buffer too.
 */
void GLCD_Init()
{
    uint16_t i;

    // Setup control line pins as digital output
    GLCD_WR_TRIS = 0;
    GLCD_RD_TRIS = 0;
    GLCD_CE_TRIS = 0;
    GLCD_CD_TRIS = 0;

#ifdef GLCD_RST
    GLCD_RST_TRIS = 0;
    // Reset GLCD controller
    GLCD_RST = 0;
    __delay_ms(5);
    GLCD_RST = 1;
    __delay_ms(5);
#endif

    // Pull down FS pin to select 8*8 font
#ifdef GLCD_FS
    GLCD_FS_TRIS = 0;
    GLCD_FS = g_font;
#endif

#if (defined GLCD_MODULE_T6963C_TEXT_MODE && defined GLCD_MODULE_T6963C_FONT_6x8)
    // Adjust addresses for font
    g_graphHomeAddr = GLCD_GRAPH_HOME_ADDR_6x8;
    g_graphArea = GLCD_GRAPH_AREA_ADDR_6x8;
    g_textHomeAddr = GLCD_TEXT_HOME_ADDR_6x8;
    g_textArea = GLCD_TEXT_AREA_ADDR_6x8;
#endif

    // Set graphics home address
    writeCommand2Args(g_graphHomeAddr, GLCD_GRAPH_HOME_SET);

    // Set graphics area
    writeCommand2Args(g_graphArea, GLCD_GRAPH_AREA_SET);

    // Set text home
    writeCommand2Args(g_textHomeAddr, GLCD_TEXT_HOME_SET);

    // Set text area
    writeCommand2Args(g_textArea, GLCD_TEXT_AREA_SET);

    // Set CGRAM offset
    writeCommand2Args(0x0300u, GLCD_OFFSET_REG_SET);

    // Set OR mode
    writeCommand(GLCD_XOR_MODE);

    // Set address pointer
    writeCommand2Args(0x0000u, GLCD_ADDR_PTR_SET);

    // Set graphic mode on
    writeCommand(GLCD_GRAPH_ON);

    // Clear the whole memory
    for (i = 0; i < 0x1FFFu; i++)
        writeCommand1Arg(0x00, GLCD_DATA_WR_INC);

    // Set text mode
    writeCommand(GLCD_TEXT_AND_GRAPH_ON);

    // Set address pointer
    writeCommand2Args(0x0000u, GLCD_ADDR_PTR_SET);

#ifdef GLCD_USE_DRAWING_BUFFER
    // Reset screen buffer
    g_screenBufPtr = 0;
    for (i = 0; i < sizeof (g_screenBuf); i++)
        g_screenBuf[i] = 0;
#endif

#ifdef GLCD_USE_SRAM_DRAWING_BUFFER
    SRAM_Init();

    /*
        sram_base_ptr = 0;
        for (i = 0; i < sizeof (sram_buf); i++)
            sram_buf[i] = 0;
     */

    for (i = GLCD_SRAM_BASE_ADDR; i < GLCD_SRAM_BUFFER_SIZE; i++)
        SRAM_WriteByte(i, 0);

    for (i = GLCD_SRAM_BASE_ADDR; i < GLCD_SRAM_BUFFER_SIZE; i++)
        if (SRAM_ReadByte(i) != 0) {
            printf("SRAM init error: data mismatch\r\n");
            break;
        }
#endif
}


/**
 * Writes the given @p c character into the LCD controller and increments
 * the RAM pointer.
 */
void t6963c_putc(char c)
{
    writeCommand1Arg(c - 0x20, GLCD_DATA_WR_INC);
}


/**
 * Writes a string on the LCD.
 */
void GLCD_PutString(char *str)
{
    while (*str) {
        writeCommand1Arg((*str) - 0x20, GLCD_DATA_WR_INC);
        str++;
    }
}


/**
 * Clears the whole memory of the LCD controller.
 */
void GLCD_Clear()
{
    uint16_t i;

    // Set address pointer
    writeCommand2Args(0x0000u, GLCD_ADDR_PTR_SET);

    for (i = 0; i < 0x1FFFu; i++)
        writeCommand1Arg(0x00, GLCD_DATA_WR_INC);
}


/**
 * Clears the screen buffer, or the LCD if buffering is disabled.
 */
void GLCD_ClearBuffer()
{
#ifdef GLCD_USE_DRAWING_BUFFER
    uint16_t i;

    for (i = 0; i < sizeof (g_screenBuf); i++)
        g_screenBuf[i] = 0;
#elif defined GLCD_USE_SRAM_DRAWING_BUFFER
    uint16_t i;

    for (i = GLCD_SRAM_BASE_ADDR; i < GLCD_SRAM_BUFFER_SIZE; i++)
        SRAM_WriteByte(i, 0);
#else
    GLCD_Clear();
#endif
}


/**
 * Sets the "cursor" to the given @p x and @p y coordinates. If the @p text
 * parameter is set to 1, the LCD will change the text mode cursor position.
 * Otherwise, if @p text is 0, the graphic memory address pointer will be
 * changed to the position corresponding to the given coordinates.
 */
void GLCD_SetPosition(uint8_t x, uint8_t y, uint8_t text)
{
    uint16_t location, home, line;

    if (!text) {
        home = g_graphHomeAddr;
        line = g_graphArea;
    } else {
        home = g_textHomeAddr;
        line = g_textArea;
    }

    location = home + (((uint16_t) y) * line) + x;
    writeCommand2Args(location, GLCD_ADDR_PTR_SET);
}


/**
 * Switches the pixel at the given @p and @p y coordinates according to @p on.
 */
void GLCD_SetPixel(uint8_t x, uint8_t y, uint8_t on)
{
    uint16_t address;

#ifdef GLCD_USE_SRAM_DRAWING_BUFFER
    uint8_t data;

    address = (((uint16_t) y) * g_graphArea) +
            (uint16_t) x / (uint16_t) 8 + GLCD_SRAM_BASE_ADDR;

    data = SRAM_ReadByte(address);

    if (on)
        data |= (1 << (7 - (x % 8)));
    else
        data &= (0xFF - (1 << (7 - (x % 8))));

    SRAM_WriteByte(address, data);

#elif (defined GLCD_USE_DRAWING_BUFFER)
    uint16_t address;

    address = (((uint16_t) y) * g_graphArea) +
            (uint16_t) x / (uint16_t) 8;

    if (on)
        g_screenBuf[address] |= (1 << (7 - (x % 8)));
    else
        g_screenBuf[address] &= (0xFF - (1 << (7 - (x % 8))));
#else
    address = g_graphHomeAddr + (((uint16_t) y) * g_graphArea) + x / 8;
    writeCommand2Args(address, GLCD_ADDR_PTR_SET);

    if (on)
        writeCommand(GLCD_BIT_SET | (7 - (x % 8)));
    else
        writeCommand(GLCD_BIT_RESET | (7 - (x % 8)));
#endif
}


/**
 * Returns with the state of the pixel at the given @p x and @p y coordinates.
 */
uint8_t GLCD_GetPixel(uint8_t x, uint8_t y)
{
    uint16_t address;

#ifdef GLCD_USE_SRAM_DRAWING_BUFFER
    uint8_t data;

    address = (((uint16_t) y) * g_graphArea) + x / 8 +
            GLCD_SRAM_BASE_ADDR;
    data = SRAM_ReadByte(address);

    return (data & (1 << (7 - (x % 8)))) > 0 ? 1 : 0;

#elif (defined GLCD_USE_DRAWING_BUFFER)
    address = (((uint16_t) y) * g_graphArea) + x / 8;

    return (g_screenBuf[address] & (1 << (7 - (x % 8)))) > 0 ? 1 : 0;
#else
    uint8_t data;

    address = g_graphHomeAddr + (((uint16_t) y) * g_graphArea) + x / 8;
    writeCommand2Args(address, GLCD_ADDR_PTR_SET);

    data = readByte();

    return (data & (1 << (7 - (x % 8)))) > 0 ? 1 : 0;
#endif
}


/**
 * Writes the contents of the screen buffer to the LCD controller's memory if
 * screen buffering is enabled. Otherwise it does nothing except when the
 * @p invert parameter is set to 1. In this case it inverts the picture stored
 * in the LCD controller's graphic memory.
 */
void GLCD_WriteBuffer(uint8_t invert)
{
    uint16_t i;

#ifdef GLCD_USE_SRAM_DRAWING_BUFFER
    uint8_t data;

    writeCommand2Args(0, GLCD_ADDR_PTR_SET);

    for (i = GLCD_SRAM_BASE_ADDR; i < GLCD_SRAM_BUFFER_SIZE; i++) {
        data = SRAM_ReadByte(i);
        writeCommand1Arg(invert ? ~data : data, GLCD_DATA_WR_INC);
    }

#elif (defined GLCD_USE_DRAWING_BUFFER)
    writeCommand2Args(0, GLCD_ADDR_PTR_SET);

    if (invert)
        for (i = 0; i < sizeof (g_screenBuf); i++) {
            writeCommand1Arg(~g_screenBuf[i], GLCD_DATA_WR_INC);
        } else
        for (i = 0; i < sizeof (g_screenBuf); i++) {
            writeCommand1Arg(g_screenBuf[i], GLCD_DATA_WR_INC);
        }
#else
    if (!invert)
        return;

    writeCommand2Args(0, GLCD_ADDR_PTR_SET);

    for (i = g_graphHomeAddr; i < g_graphHomeAddr + 2048; i++)
        writeCommand1Arg(~readByte(), GLCD_DATA_WR_INC);
#endif
}


void GLCD_WriteBufferRegion(uint8_t x1, uint8_t y1,
                            uint8_t x2, uint8_t y2, uint8_t invert)
{
    uint16_t i;

#ifdef GLCD_USE_SRAM_DRAWING_BUFFER
    uint8_t data;
#endif

#if (defined GLCD_USE_SRAM_DRAWING_BUFFER || defined GLCD_USE_DRAWING_BUFFER)
    uint16_t from_addr, to_addr;

    from_addr = (((uint16_t) y1) * g_graphArea) +
            (uint16_t) x1 / (uint16_t) 8;
    to_addr = (((uint16_t) y2) * g_graphArea) +
            (uint16_t) x2 / (uint16_t) 8;
#endif

#ifdef GLCD_USE_SRAM_DRAWING_BUFFER
    writeCommand2Args(from_addr, GLCD_ADDR_PTR_SET);

    for (i = GLCD_SRAM_BASE_ADDR + from_addr;
            i <= GLCD_SRAM_BASE_ADDR + to_addr; i++) {
        data = SRAM_ReadByte(i);
        writeCommand1Arg(invert ? ~data : data, GLCD_DATA_WR_INC);
    }

#elif (defined GLCD_USE_DRAWING_BUFFER)
    writeCommand2Args(from_addr, GLCD_ADDR_PTR_SET);

    if (invert)
        for (i = fromAdrress; i < to_addr; i++) {
            writeCommand1Arg(~g_screenBuf[i], GLCD_DATA_WR_INC);
        } else
        for (i = 0; i < sizeof (g_screenBuf); i++) {
            writeCommand1Arg(g_screenBuf[i], GLCD_DATA_WR_INC);
        }
#else
    if (!invert)
        return;

    writeCommand2Args(from_addr, GLCD_ADDR_PTR_SET);

    for (i = from_addr; i < to_addr; i++)
        writeCommand1Arg(~readByte(), GLCD_DATA_WR_INC);
#endif
}


void GLCD_SetBufferByte(uint16_t address, uint8_t data, uint8_t draw_mode)
{
#ifdef GLCD_USE_SRAM_DRAWING_BUFFER

    uint8_t d = data;

    // OR mode
    if (draw_mode == 1) {
        d = SRAM_ReadByte(GLCD_SRAM_BASE_ADDR + address);
        d |= data;
    }

    SRAM_WriteByte(GLCD_SRAM_BASE_ADDR + address, d);

#elif (defined GLCD_USE_DRAWING_BUFFER)

    g_screenBuf[address] = data;

#else

    return;

#endif
}


void GLCD_CopyToBuffer(uint8_t x, uint8_t y, uint8_t *data, uint8_t len,
                       uint8_t flags)
{
    if (x >= GLCD_RESOLUTION_X || y > GLCD_RESOLUTION_Y)
        return;

#if defined(GLCD_USE_SRAM_DRAWING_BUFFER) || defined(GLCD_USE_DRAWING_BUFFER)
    uint8_t offset = x % 8;
    uint16_t addr = (((uint16_t) y) * g_graphArea) +
            (uint16_t) x / (uint16_t) 8 + GLCD_SRAM_BASE_ADDR;
    uint16_t last_addr = (((uint16_t) y + 1) * g_graphArea) +
            GLCD_SRAM_BASE_ADDR;
    uint8_t mask_bits;
    uint8_t invert = flags & GLCD_INVERT;

    mask_bits = flags & 7;

    /*
     * offset == 0 means the 'x' coordinate is on a byte boundary,
     * any other value means the offset bits in the actual data byte.
     */
    if (offset == 0) {
        /*
         * We are on a byte boundary, simply write the data into the buffer.
         */
        while (addr < last_addr && len--) {
            uint8_t output = SRAM_ReadByte(addr);
            uint8_t input = (invert ? ~(*data) : *data);
            if (len == 0 && mask_bits > 0) {
                uint8_t mask = (0xFF >> (8 - mask_bits));
                output &= mask;
                output |= input & ~mask;
#ifdef GLCD_USE_SRAM_DRAWING_BUFFER
                SRAM_WriteByte(addr++, output);
#else
                g_screenBuf[addr++] = output;
#endif
            } else {
#ifdef GLCD_USE_SRAM_DRAWING_BUFFER
                SRAM_WriteByte(addr++, input);
#else
                g_screenBuf[addr++] = input;
#endif
                data++;
            }
        }
    } else {
        /*
         * We are crossing a byte boundary and
         * this is the 'first half (fragment)' of the input data byte.
         */
        uint8_t first_fragment = 1;
        while (addr < last_addr) {
            uint8_t input = (invert ? ~(*data) : *data);
#ifdef GLCD_USE_SRAM_DRAWING_BUFFER
            uint8_t output = SRAM_ReadByte(addr);
#else
            uint8_t output = g_screenBuf[addr];
#endif
            if (first_fragment) {
                if (len == 1 && mask_bits > offset) {
                    uint8_t mask = (0xFF >> (8 - mask_bits + offset));
                    output &= (0xFF << (8 - offset)) + mask;
                    output |= (input >> offset) & ~mask;
#ifdef GLCD_USE_SRAM_DRAWING_BUFFER
                    SRAM_WriteByte(addr, output);
#else
                    g_screenBuf[addr] = output;
#endif
                    break;
                } else {
                    output &= (0xFF << (8 - offset));
                    output |= (input >> offset);
#ifdef GLCD_USE_SRAM_DRAWING_BUFFER
                    SRAM_WriteByte(addr, output);
#else
                    g_screenBuf[addr] = output;
#endif
                    addr++;
                }
                first_fragment = 0;
            } else {
                /*
                 * This is the 'second half' of the input data byte written
                 * into the next byte after the boundary.
                 */
                if (len == 1 && mask_bits > 0) {
                    uint8_t mask = 0xff << (8 - offset + mask_bits);
                    output &= ~mask;
                    output |= (input << (8 - offset)) & mask;
#ifdef GLCD_USE_SRAM_DRAWING_BUFFER
                    SRAM_WriteByte(addr, output);
#else
                    g_screenBuf[addr] = output;
#endif
                } else {
                    output &= (0xFF >> offset);
                    output |= (input << (8 - offset));
#ifdef GLCD_USE_SRAM_DRAWING_BUFFER
                    SRAM_WriteByte(addr, output);
#else
                    g_screenBuf[addr] = output;
#endif
                }

                if (--len == 0)
                    break;
                data++;
                first_fragment = 1;
            }
        }
    }
#else
    return;
#endif
}

#endif
