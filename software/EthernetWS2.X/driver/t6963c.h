/**
 * Ethernet Weather Station 2 Firmware
 * T6963C Graphic LCD Driver
 *
 * @author Tamas Karpati <tomikaa87>
 * @date 2011. okt�ber 29.
 */

#ifndef T6963C_H
#define T6963C_H

#include "hardware_profile.h"

#include <stdint.h>

// Color Constants
#define GLCD_COLOR_BLACK            1
#define GLCD_COLOR_WHITE            0

// Font Constants
#define GLCD_FONT_8x8               0
#define GLCD_FONT_6x8               1

void GLCD_PutChar(char c);
void GLCD_PutString(char *str);

void GLCD_SetAddressPointer(uint16_t address);

void GLCD_WriteByte(uint8_t byte);
void GLCD_WriteByteIncremented(uint8_t byte);

void GLCD_WriteBuffer(uint8_t invert);

void GLCD_Clear();
void GLCD_ClearBuffer();

void GLCD_SetPixel(uint8_t x, uint8_t y, uint8_t on);
uint8_t GLCD_GetPixel(uint8_t x, uint8_t y);

#endif // T6963C_H
