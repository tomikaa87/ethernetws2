/**
 * Ethernet Weather Station 2 Firmware
 * 25LC1024 SPI EEPROM Driver
 *
 * @author Tamas Karpati <tomikaa87>
 * @date 2010. szeptember 21.
 */

#include "eeprom_25lc.h"
#include "hardware_profile.h"

//#include <stdio.h>

#ifdef HARDWARE_PROFILE_H
// EXT_SRAM_CS must not be touched
#define LOCK_SPI_BUS() { /*EXT_SRAM_CS = 1;*/ ENC_CS_IO = 1; }
#else
#define LOCK_SPI_BUS()
#endif

static uint24_t g_transactionAddress;
bit g_transactionStarted = 0;
bit g_readTransaction = 0;

/*** Constant definitions *****************************************************/

static enum {
    CMD_READ    = 0b00000011,
    CMD_WRITE   = 0b00000010,
    CMD_WREN    = 0b00000110,
    CMD_WRDI    = 0b00000100,
    CMD_RDSR    = 0b00000101,
    CMD_WRSR    = 0b00000001,
    CMD_PE      = 0b01000010,
    CMD_SE      = 0b11011000,
    CMD_CE      = 0b11000111,
    CMD_RDID    = 0b10101011,
    CMD_DPD     = 0b10111001
} ;

/*** Low level functions ******************************************************/

#define ClearSSPIF()    { EXT_EEPROM_SSPIF = 0; }
#define WaitForData()   { while (!EXT_EEPROM_SSPIF); EXT_EEPROM_SSPIF = 0; }


/**
 * Sends a byte to the EEPROM.
 */
static void writeByte(uint8_t byte)
{
#ifdef EXT_EEPROM_USE_HW_SPI
    ClearSSPIF();
    EXT_EEPROM_SSPBUF = byte;
    WaitForData();
#else
    uint8_t i, val;

    for (i = 0; i < 8; i++) {
        val = (byte & (1 << (7 - i))) > 0;
        val ? putch('1') : putch('0');
        EXT_EEPROM_SDO = val;
        EXT_EEPROM_SCK = 1;
        EXT_EEPROM_SCK = 0;
    }
#endif
}


/**
 * Ready a data byte from the EEPROM.
 */
static uint8_t readByte()
{
#ifdef EXT_EEPROM_USE_HW_SPI
    ClearSSPIF();
    EXT_EEPROM_SSPBUF = 0; // Send dummy byte to generate SCK
    WaitForData();
    return EXT_EEPROM_SSPBUF;
#else
    uint8_t i, byte;

    byte = 0;
    for (i = 0; i < 8; i++) {
        byte |= EXT_EEPROM_SDI << (7 - i);
        EXT_EEPROM_SDI > 0 ? putch('1') : putch('0');
        EXT_EEPROM_SCK = 1;
        EXT_EEPROM_SCK = 0;
    }

    return byte;
#endif
}


static void enableWrites()
{
    EXT_EEPROM_CS = 0;
    writeByte(CMD_WREN);
    EXT_EEPROM_CS = 1;
}


static void writeAddress(uint24_t address)
{
    // Write address<23:16> byte
    writeByte((uint8_t) ((address >> 16) & 0xFF));
    // Write address<15:8> byte
    writeByte((uint8_t) ((address >> 8) & 0xFF));
    // Write address<7:0> byte
    writeByte((uint8_t) (address & 0xFF));
}


/**
 * Reads the status register of the EEPROM and returns its value.
 */
static uint8_t readStatusReg()
{
    uint8_t data;

    EXT_EEPROM_CS = 0;
    writeByte(CMD_RDSR);
    data = readByte();
    EXT_EEPROM_CS = 1;

    return data;
}


/**
 * Changes the stored value of the EEPROM's status register to the given
 * \p value.
 */
static void writeStatusReg(uint8_t value)
{
    EXT_EEPROM_CS = 0;
    writeByte(CMD_WRSR);
    writeByte(value);
    EXT_EEPROM_CS = 1;
}


/**
 * Checks if the EEPROM has finished the previous write operation.
 */
static uint8_t isReady()
{
    uint8_t data;

    EXT_EEPROM_CS = 0;
    writeByte(CMD_RDSR);
    data = readByte();
    EXT_EEPROM_CS = 1;

    return !(data & 0x01);
}

/*** API function implementations *********************************************/


/**
 * Initializes the external EEPROM.
 */
void EEPROM_Init()
{
    EXT_EEPROM_CS = 1;
    EXT_EEPROM_CS_TRIS = 0;

    EXT_EEPROM_SDI_TRIS = 1;
    EXT_EEPROM_SCK_TRIS = 0;
    EXT_EEPROM_SDO_TRIS = 0;

    // EWS2: (physically) same pin as EXT_SRAM_CS, hardware mutual exclusion
    EXT_EEPROM_nHOLD = 1;
    EXT_EEPROM_nHOLD_TRIS = 0;

#ifdef EXT_EEPROM_USE_HW_SPI
#ifndef EEPROM_DONT_MODIFY_SSPCON
    EXT_EEPROM_SSPCON1 = EXT_EEPROM_SSPCON1_VALUE;
    EXT_EEPROM_SSPSTATbits.CKE = 1;
    EXT_EEPROM_SSPSTATbits.SMP = 0;
#endif
#endif

    // Check write protection and disable it if necessary
    LOCK_SPI_BUS();
    if (readStatusReg() & 0b10001100)
        writeStatusReg(0x00);
}


/**
 * Writes the given \p data byte into the EEPROM. The address is specified
 * by \p address.
 *
 * @return The number of written bytes.
 */
uint8_t EEPROM_WriteByte(uint24_t address, uint8_t byte)
{
    if (address > EXT_EEPROM_MAX_ADDRESS)
        return 0;

    // Get control over the SPI bus
    LOCK_SPI_BUS();

    // Resume transmission
    EXT_EEPROM_nHOLD = 1;

    // Wait for the chip to complete the previous write
    while (!isReady());

    // EEPROM write enable
    enableWrites();
    // Select the chip
    EXT_EEPROM_CS = 0;
    // Instruction
    writeByte(CMD_WRITE);
    // Address
    writeAddress(address);
    // Data
    writeByte(byte);
    // Deselect the chip
    EXT_EEPROM_CS = 1;

    return 1;
}


/**
 *
 *
 * @return The number of written bytes.
 */
uint24_t EEPROM_WriteBuffer(uint24_t address, uint8_t *buf, uint24_t length)
{
    uint24_t buf_ptr;

    // Get control over the SPI bus
    LOCK_SPI_BUS();

    if (address + length > EXT_EEPROM_MAX_ADDRESS + 1)
        return 0;

    // Resume transmission
    EXT_EEPROM_nHOLD = 1;

    //printf("EEP: begining address: %05X\r\n", address);
    //printf("EEP: buffer size: %lu\r\n", buf_size);

    // Wait for the chip to complete the previous write
    while (!isReady());

    // EEPROM write enable
    enableWrites();
    // Select the chip
    EXT_EEPROM_CS = 0;
    // Write instruction
    writeByte(CMD_WRITE);
    // Address
    writeAddress(address);

    buf_ptr = 0;
    while (buf_ptr < length && (address + buf_ptr) <= EXT_EEPROM_MAX_ADDRESS) {
        // Check if the address is the begining of the next page
        if (buf_ptr > 0 && (address + buf_ptr) % EXT_EEPROM_PAGE_SIZE == 0) {
            //printf("EEP: commit page #%u\r\n", (address + buf_ptr - 1) / EXT_EEPROM_PAGE_SIZE);

            // Commit changes of the current page before sending the next byte
            EXT_EEPROM_CS = 1;

            // Wait for the chip to complete the previous write
            while (!isReady());

            // EEPROM write enable
            enableWrites();
            // Select the chip
            EXT_EEPROM_CS = 0;
            // Write instruction
            writeByte(CMD_WRITE);
            // Address
            writeAddress(address + buf_ptr);
        }

        //printf("EEP: write: %02X -> %05X\r\n", buf[buf_ptr], buf_ptr + address);

        writeByte(buf[buf_ptr++]);
    }

    EXT_EEPROM_CS = 1;

    return buf_ptr;
}


/**
 * Reads a byte from the EEPROM. The address is specified by \p address.
 */
uint8_t EEPROM_ReadByte(uint24_t address)
{
    uint8_t data;

    // Get control over the SPI bus
    LOCK_SPI_BUS();

    // Resume transmission
    EXT_EEPROM_nHOLD = 1;

    // Wait for the chip to complete the previous write
    while (!isReady());

    // Select the chip
    EXT_EEPROM_CS = 0;
    // Instruction
    writeByte(CMD_READ);
    // Address
    writeAddress(address);
    // Read data
    data = readByte();
    // Deselect the chip
    EXT_EEPROM_CS = 1;

    return data;
}


/**
 * Writes the contents of the given \p buf buffer into the EEPROM. \p buf_size
 * holds the size of the buffer (in bytes). The address of the first byte is
 * held by \p address.
 */
uint24_t EEPROM_ReadBuffer(uint24_t address, uint8_t *buf, uint24_t length)
{
    uint24_t bufPtr;

    // Get control over the SPI bus
    LOCK_SPI_BUS();

    // Resume transmission
    EXT_EEPROM_nHOLD = 1;

    // Wait for the chip to complete the previous write
    while (!isReady());

    // Select the chip
    EXT_EEPROM_CS = 0;
    // Instruction
    writeByte(CMD_READ);
    // Address
    writeAddress(address);

    // Read data bytes
    bufPtr = 0;
    while (bufPtr < length && bufPtr <= EXT_EEPROM_MAX_ADDRESS)
        buf[bufPtr++] = readByte();

    EXT_EEPROM_CS = 1;

    return bufPtr;
}


void EEPROM_BeginWriteTransaction(uint24_t address)
{
    if (g_transactionStarted || address > EXT_EEPROM_MAX_ADDRESS)
        return;

    g_transactionAddress = address;
    g_readTransaction = 0;

    // Get control over the SPI bus
    LOCK_SPI_BUS();
    // Wait for the chip to complete the previous write
    while (!isReady());
    // EEPROM write enable
    enableWrites();
    // Select the chip
    EXT_EEPROM_CS = 0;
    // Write instruction
    writeByte(CMD_WRITE);
    // Address
    writeAddress(address);

    // HOLD transmission
    EXT_EEPROM_nHOLD = 0;

    g_transactionStarted = 1;
}


uint8_t EEPROM_CommitWriteTransaction(uint8_t byte)
{
    if (!g_transactionStarted || g_readTransaction)
        return 0;

    if (g_transactionAddress > EXT_EEPROM_MAX_ADDRESS) {
        EEPROM_CommitReadTransaction();
        return 0;
    }

    // Get control over the SPI bus
    LOCK_SPI_BUS();

    // Resume transmission
    EXT_EEPROM_nHOLD = 1;

    // Check if the address is the begining of the next page
    if (g_transactionAddress > 0 && g_transactionAddress % EXT_EEPROM_PAGE_SIZE == 0) {
        //printf("EEP: commit page #%u\r\n", (address + buf_ptr - 1) / EXT_EEPROM_PAGE_SIZE);

        // Commit changes of the current page before sending the next byte
        EXT_EEPROM_CS = 1;
        // Wait for the chip to complete the previous write
        while (!isReady());
        // EEPROM write enable
        enableWrites();
        // Select the chip
        EXT_EEPROM_CS = 0;
        // Write instruction
        writeByte(CMD_WRITE);
        // Address
        writeAddress(g_transactionAddress);
    }

    //printf("EEP: write: %02X -> %05X\r\n", buf[buf_ptr], buf_ptr + address);

    writeByte(byte);

    // HOLD transmission
    EXT_EEPROM_nHOLD = 0;

    ++g_transactionAddress;

    return 1;
}


void EEPROM_CommitReadTransaction()
{
    if (g_transactionStarted) {
        // Resume transmission
        EXT_EEPROM_nHOLD = 1;

        EXT_EEPROM_CS = 1;
        g_transactionStarted = 0;
    }
}


void EEPROM_BeginReadTransaction(uint24_t address)
{
    if (g_transactionStarted || address > EXT_EEPROM_MAX_ADDRESS)
        return;

    g_transactionStarted = 1;
    g_readTransaction = 1;

    // Get control over the SPI bus
    LOCK_SPI_BUS();

    // Wait for the chip to complete the previous write
    while (!isReady());

    // Select the chip
    EXT_EEPROM_CS = 0;
    // Instruction
    writeByte(CMD_READ);
    // Address
    writeAddress(address);
}


uint8_t EEPROM_ReadTransactionGetByte()
{
    if (!g_transactionStarted || !g_readTransaction)
        return 0;

    // Resume transmission
    EXT_EEPROM_nHOLD = 1;

    uint8_t data = readByte();

    // Hold transmission
    EXT_EEPROM_nHOLD = 0;

    return data;
}