/**
 * Ethernet Weather Station 2 Firmware
 * 25LC1024 SPI EEPROM Driver
 *
 * @author Tamas Karpati <tomikaa87>
 * @date 2010. szeptember 21.
 */

#ifndef _25LC1024_H
#define _25LC1024_H

#include "hardware_profile.h"

#include <stdint.h>

/*** Constant definitions *****************************************************/

#ifndef HARDWARE_PROFILE_H

#define EXT_EEPROM_SIZE_BYTES           131072ul
#define EXT_EEPROM_MAX_ADDRESS          (EXT_EEPROM_SIZE_BYTES - 1ul)
#define EXT_EEPROM_PAGE_SIZE            256

#define EXT_EEPROM_USE_HW_SPI
//#define EEPROM_DONT_MODIFY_SSPCON

#define EXT_EEPROM_CS                   (LATC2)
#define EXT_EEPROM_SCK                  (LATC3)
#define EXT_EEPROM_SDI                  (RC4)
#define EXT_EEPROM_SDO                  (LATC5)
#define EXT_EEPROM_CS_TRIS              (TRISC2)
#define EXT_EEPROM_SCK_TRIS             (TRISC3)
#define EXT_EEPROM_SDI_TRIS             (TRISC4)
#define EXT_EEPROM_SDO_TRIS             (TRISC5)
#define EXT_EEPROM_SSPBUF               (SSPBUF)
#define EXT_EEPROM_SSPIF                (SSPIF)
#define EXT_EEPROM_SSPCON1              (SSPCON1)
#define EXT_EEPROM_SSPSTATbits          (SSPSTATbits)
#define EXT_EEPROM_SSPCON1_VALUE        0x21

#endif

/*** API functions ************************************************************/

void EEPROM_Init();
uint8_t EEPROM_WriteByte(uint24_t address, uint8_t byte);
uint24_t EEPROM_WriteBuffer(uint24_t address, uint8_t *buf, uint24_t length);
uint8_t EEPROM_ReadByte(uint24_t address);
uint24_t EEPROM_ReadBuffer(uint24_t address, uint8_t *buf, uint24_t length);

void EEPROM_BeginWriteTransaction(uint24_t address);
uint8_t EEPROM_CommitWriteTransaction(uint8_t byte);

void EEPROM_BeginReadTransaction(uint24_t address);
uint8_t EEPROM_ReadTransactionGetByte();
void EEPROM_CommitReadTransaction();

#endif // _25LC1024_H
