/**
 * Ethernet Weather Station 2 Firmware
 * External SPI SRAM (23Kxxx) driver
 *
 * @author Tamas Karpati <tomikaa87>
 * @date 2011. okt�ber 9.
 */

#ifndef SRAM_23K_H
#define SRAM_23K_H

#include "hardware_profile.h"

#include <stdint.h>

/*** Constant definitions *****************************************************/

#ifndef HARDWARE_PROFILE_H

#define EXT_SRAM_SIZE_BYTES             32768u
#define EXT_SRAM_PAGE_SIZE              32u

#define EXT_SRAM_MAX_ADDRESS            (EXT_SRAM_SIZE_BYTES - 1u)
#define EXT_SRAM_MAX_PAGE       (EXT_SRAM_SIZE_BYTES / EXT_SRAM_PAGE_SIZE - 1u)

#define EXT_SRAM_USE_HW_SPI
//#define EXT_SRAM_DONT_MODIFY_SSPCON

#define EXT_SRAM_CS                     (LATD0)
#define EXT_SRAM_SCK                    (LATC3)
#define EXT_SRAM_SDI                    (RC4)
#define EXT_SRAM_SDO                    (LATC5)
#define EXT_SRAM_CS_TRIS                (TRISD0)
#define EXT_SRAM_SCK_TRIS               (TRISC3)
#define EXT_SRAM_SDI_TRIS               (TRISC4)
#define EXT_SRAM_SDO_TRIS               (TRISC5)
#define EXT_SRAM_SSPBUF                 (SSPBUF)
#define EXT_SRAM_SSPIF                  (SSPIF)
#define EXT_SRAM_SSPCON1                (SSPCON1)
#define EXT_SRAM_SSPSTATbits            (SSPSTATbits)
#define EXT_SRAM_SSPCON1_VALUE          0x21

/*
 * Comment out the define to disable sanity checks. It gives more speed on
 * slower microcontrollers, but you must check input addresses and buffer
 * sizes before calling any of the write or read functions.
 */
#define EXT_SRAM_DO_SANITY_CHECK

//#define EXT_SRAM_COMPILE_STRESS_TEST

#endif

/*** API functions ************************************************************/

void SRAM_Init();
uint8_t SRAM_WriteByte(uint16_t address, uint8_t byte);
uint8_t SRAM_ReadByte(uint16_t address);
uint8_t SRAM_WritePage(uint16_t page, uint8_t offset, uint8_t *buf, uint8_t bufSize);
uint8_t SRAM_ReadPage(uint16_t page, uint8_t offset, uint8_t *buf, uint8_t bufSize);
uint16_t SRAM_WriteBuffer(uint16_t address, uint8_t *buf, uint16_t bufSize);
uint16_t SRAM_ReadBuffer(uint16_t address, uint8_t *buf, uint16_t bufSize);

#ifdef EXT_SRAM_COMPILE_STRESS_TEST
void EXT_SRAM_writePerformanceTest();
void EXT_SRAM_readPerformanceTest();
uint8_t EXT_SRAM_stabilityTest();
#endif

#endif // SRAM_23K_H
