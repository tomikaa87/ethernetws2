//
// Ethernet Weather Station 2
// Utilities
//
// Author: Tamas Karpati
// This file was created on 2014-08-19
//

#ifndef UTILS_H
#define UTILS_H

#include <stdint.h>
#include <stdbool.h>

struct tm;

bool Utils_IsInTimeRange(uint8_t startHour, uint8_t startMin, uint8_t endHour, uint8_t endMin);
bool Utils_IsSummerTime(const struct tm* t);
struct tm* Utils_CurrentTime();

#endif	/* UTILS_H */

