/**
 * Ethernet Weather Station 2 Firmware
 * Global hardware settings
 *
 * @author Tamas Karpati <tomikaa87>
 * @date 2011. okt�ber 5.
 */

#ifndef HARDWARE_PROFILE_H
#define HARDWARE_PROFILE_H

#include <xc.h>

/*** ENC28J60 I/O pin configuration *******************************************/

#define ENC_IN_SPI1
#define ENC_RST_TRIS        (TRISE2)
#define ENC_RST_IO          (LATE2)
#define ENC_CS_TRIS         (TRISB1)
#define ENC_CS_IO           (LATB1)
#define ENC_SCK_TRIS        (TRISC3)
#define ENC_SDI_TRIS        (TRISC4)
#define ENC_SDO_TRIS        (TRISC5)
#define ENC_SPI_IF          (SSPIF)
#define ENC_SSPBUF          (SSPBUF)
#define ENC_SPISTAT         (SSPSTAT)
#define ENC_SPISTATbits     (SSPSTATbits)
#define ENC_SPICON1         (SSPCON1)
#define ENC_SPICON1bits     (SSPCON1bits)
#define ENC_SPICON2         (SSPCON2)
//#define ENC_SPICON_VALUE    0x20

/*** Clock frequency macros for TCP stack *************************************/

#define GetSystemClock()        (40000000ul)
#define GetInstructionClock()   (GetSystemClock() / 4)
#define GetPeripheralClock()    GetInstructionClock()
#define _XTAL_FREQ              40000000ul

/*** External EEPROM configuration ********************************************/

#define EXT_EEPROM_SIZE_BYTES           131072ul
#define EXT_EEPROM_MAX_ADDRESS          (EXT_EEPROM_SIZE_BYTES - 1ul)
#define EXT_EEPROM_PAGE_SIZE            256

#define EXT_EEPROM_USE_HW_SPI
///#define EEPROM_DONT_MODIFY_SSPCON

#define EXT_EEPROM_nHOLD                (LATE0)
#define EXT_EEPROM_nHOLD_TRIS           (TRISE0)
#define EXT_EEPROM_CS                   (LATE1)
#define EXT_EEPROM_SCK                  (LATC3)
#define EXT_EEPROM_SDI                  (PORTCbits.RC4)
#define EXT_EEPROM_SDO                  (LATC5)
#define EXT_EEPROM_CS_TRIS              (TRISE1)
#define EXT_EEPROM_SCK_TRIS             (TRISC3)
#define EXT_EEPROM_SDI_TRIS             (TRISC4)
#define EXT_EEPROM_SDO_TRIS             (TRISC5)
#define EXT_EEPROM_SSPBUF               (SSPBUF)
#define EXT_EEPROM_SSPIF                (SSPIF)
#define EXT_EEPROM_SSPCON1              (SSPCON1)
#define EXT_EEPROM_SSPSTATbits          (SSPSTATbits)
#define EXT_EEPROM_SSPCON1_VALUE        0x20

/*** External SRAM configuration **********************************************/

#define EXT_SRAM_SIZE_BYTES             32768u
#define EXT_SRAM_PAGE_SIZE              32u

#define EXT_SRAM_MAX_ADDRESS            (EXT_SRAM_SIZE_BYTES - 1u)
#define EXT_SRAM_MAX_PAGE       (EXT_SRAM_SIZE_BYTES / EXT_SRAM_PAGE_SIZE - 1u)

#define EXT_SRAM_USE_HW_SPI
//#define EXT_SRAM_DONT_MODIFY_SSPCON

#define EXT_SRAM_CS                     (LATE0)
#define EXT_SRAM_SCK                    (LATC3)
#define EXT_SRAM_SDI                    (PORTCbits.RC4)
#define EXT_SRAM_SDO                    (LATC5)
#define EXT_SRAM_CS_TRIS                (TRISE0)
#define EXT_SRAM_SCK_TRIS               (TRISC3)
#define EXT_SRAM_SDI_TRIS               (TRISC4)
#define EXT_SRAM_SDO_TRIS               (TRISC5)
#define EXT_SRAM_SSPBUF                 (SSPBUF)
#define EXT_SRAM_SSPIF                  (SSPIF)
#define EXT_SRAM_SSPCON1                (SSPCON1)
#define EXT_SRAM_SSPSTATbits            (SSPSTATbits)
#define EXT_SRAM_SSPCON1_VALUE          0x20

/*
 * Comment out the define to disable sanity checks. It gives more speed on
 * slower microcontrollers, but you must check input addresses and buffer
 * sizes before calling any of the write or read functions.
 */
//#define EXT_SRAM_DO_SANITY_CHECK

/*** UART macros for TCP stack ************************************************/

/*#define BusyUART            ()BusyUSART()
#define CloseUART()CloseUSART()
#define ConfigIntUART(a)ConfigIntUSART(a)
#define DataRdyUART()DataRdyUSART()
#define OpenUART(a,b,c)OpenUSART(a,b,c)
#define ReadUART()ReadUSART()
#define WriteUART(a)WriteUSART(a)
#define getsUART(a,b,c)getsUSART(b,a)
#define putsUART(a)putsUSART(a)
#define getcUART()ReadUSART()
#define putcUART(a)WriteUSART(a)
#define putrsUART(a)putrsUSART((far rom char*)a)*/

/*** EEPROM configuration *****************************************************/

#define CONFIG_DATA_EEPROM_ADDR         0x0000u

/*** Keyboard pin configuration ***********************************************/

#define KEY_ROW_1                   LATAbits.LA0
#define KEY_ROW_2                   LATAbits.LA1
#define KEY_ROW_3                   LATAbits.LA2
#define KEY_COL_1                   PORTAbits.RA3
#define KEY_COL_2                   PORTAbits.RA4
#define KEY_COL_3                   PORTAbits.RA5
#define KEY_ROW_1_TRIS              TRISAbits.TRISA0
#define KEY_ROW_2_TRIS              TRISAbits.TRISA1
#define KEY_ROW_3_TRIS              TRISAbits.TRISA2
#define KEY_COL_1_TRIS              TRISAbits.TRISA3
#define KEY_COL_2_TRIS              TRISAbits.TRISA4
#define KEY_COL_3_TRIS              TRISAbits.TRISA5

/*** RAM locations ************************************************************/

// Graphic LCD buffer space: 2048 B
#define RAM_LOC_GLCD_BUFFER         0u

// UART buffer
#define RAM_LOC_UART_BUFFER_ADDR    2048u
#define RAM_LOC_UART_BUFFER_SIZE    1024u
#define RAM_LOC_UART_BUFFER_END     RAM_LOC_UART_BUFFER_ADDR + RAM_LOC_UART_BUFFER_SIZE - 1

// Weather data locations
#define RAM_LOC_WDATA_BASE          8048u
#define RAM_LOC_WDATA_XML_BASE      EXT_SRAM_SIZE_BYTES - 6000u

/*** EEPROM locations *********************************************************/

// Weather screen image locations       [0x0000 - 0x27ff]
#define EEPROM_LOC_SPLASH_SCR           0x0000ul
#define EEPROM_LOC_CONFIG_ERR_SCR       0x0800ul
#define EEPROM_LOC_CURRENT_W_SCR        0x1000ul
#define EEPROM_LOC_FORE_W_SCR           0x1800ul
#define EEPROM_LOC_SCREEN_DATA_END      0x27FFul

// Font data locations                  [0x2800 - 0x37ff]
#define EEPROM_LOC_FONT2_PFTB_DATA      0x2800ul
#define EEPROM_LOC_FONT2_PFTB_ATTR      0x2f00ul

#define EEPROM_LOC_FONT2_LCDL_DATA      0x3000ul
#define EEPROM_LOC_FONT2_LCDL_ATTR      0x3300ul

#define EEPROM_LOC_FONT2_LCDS_DATA      0x3400ul
#define EEPROM_LOC_FONT2_LCDS_ATTR      0x3700ul
#define EEPROM_LOC_FONT2_DATA_END       0x37FFul

// Icon Set 1                           [0x3800 - 0xB800]
#define EEPROM_LOC_ICON_SET_1           0x3800ul
#define EEPROM_LOC_ICON_SET_1_END       0xA4FFul

#define EEPROM_LOC_STATUS_ICONS         0xA500ul
#define EEPROM_LOC_STATUS_ICONS_END     0xA5FFul

#define EEPROM_LOC_WARNING_ICONS        0xA600ul
#define EEPROM_LOC_WARNING_ICONS_END    0xA6FFul

#define EEPROM_LOC_LAST_ADDRESS         0x1FFFFul

/*** Serial configuration *****************************************************/

#define SERIAL_SPBRG                    SPBRG
#define SERIAL_SPBRGH                   SPBRGH
#define SERIAL_TXSTA                    TXSTA
#define SERIAL_RCSTA                    RCSTA
#define SERIAL_BAUDCON                  BAUDCON
#define SERIAL_TRMT                     TRMT
#define SERIAL_RCIF                     RCIF

#endif // HARDWARE_PROFILE_H


