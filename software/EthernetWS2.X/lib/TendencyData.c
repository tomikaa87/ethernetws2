#include "TendencyData.h"

#include <limits.h>
#include <string.h>


void TendencyData_Init(TendencyData * const td)
{
    memset(td, 0, sizeof (TendencyData));
}


void TendencyData_AddValue(TendencyData * const td, const int8_t value)
{
    if (td->count < TENDENCY_DATA_MAX_VALUES) {
        td->values[td->count] = value;
        ++td->count;
    } else {
        // Append data to the end of the buffer by shifting all values
        for (uint8_t i = 0; i < TENDENCY_DATA_MAX_VALUES - 1; ++i) {
            td->values[i] = td->values[i + 1];
        }
        td->values[TENDENCY_DATA_MAX_VALUES - 1] = value;
    }
}


uint8_t TendencyData_GetNormalizedValues(const TendencyData * const td, const uint8_t max, uint8_t* output)
{
    int8_t tdMax = SCHAR_MIN;
    int8_t tdMin = SCHAR_MAX;

    for (uint8_t i = 0; i < td->count; ++i) {
        const int8_t v = td->values[i];

        if (v < tdMin)
            tdMin = v;

        if (v > tdMax)
            tdMax = v;
    }

    uint8_t totalDistance = (uint8_t)(tdMax - tdMin);
    for (uint8_t i = 0; i < td->count; ++i) {
        int8_t value = td->values[i];
        uint8_t distance = (uint8_t)(value - tdMin);
        uint8_t normalizedDistance = (uint8_t)((uint16_t)distance * (uint16_t)max / (uint16_t)totalDistance);
        output[i] = normalizedDistance;
    }

    return td->count;
}


bool TendencyData_HasValues(const TendencyData * const td)
{
    return td->count > 0;
}


int8_t TendencyData_GetLastValue(const TendencyData * const td)
{
    if (td->count < TENDENCY_DATA_MAX_VALUES)
        return td->values[td->count];
    else
        return td->values[TENDENCY_DATA_MAX_VALUES - 1];
}


void TendencyData_Fill(TendencyData * const td, int8_t value)
{
    memset(td->values, value, TENDENCY_DATA_MAX_VALUES);
    td->count = TENDENCY_DATA_MAX_VALUES;
}