/*
 * File:   WeatherDataCollector.h
 * Author: tomikaa
 *
 * Created on 2018. december 21., 20:10
 */

#ifndef WEATHERDATACOLLECTOR_H
#define	WEATHERDATACOLLECTOR_H

#include "lib/TendencyData.h"

typedef enum {
    WDC_OK,
    WDC_UPDATE_NEEDED
} WDCTaskResult;

extern TendencyData g_temperatureTendencyData;
extern TendencyData g_pressureTendencyData;

void WeatherDataCollector_Init();
WDCTaskResult WeatherDataCollector_Task();
void WeatherDataCollector_DoSampling();

#endif	/* WEATHERDATACOLLECTOR_H */

