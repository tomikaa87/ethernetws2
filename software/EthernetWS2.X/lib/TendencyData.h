/*
 * File:   TendencyData.h
 * Author: tomikaa
 *
 * Created on 2018. december 20., 12:52
 */

#ifndef TENDENCYDATA_H
#define	TENDENCYDATA_H

#include <stdbool.h>
#include <stdint.h>

#define TENDENCY_DATA_MAX_VALUES    25

typedef int8_t TendencyValues[TENDENCY_DATA_MAX_VALUES];

typedef struct {
    uint8_t count;
    uint8_t lastInsertIndex;
    TendencyValues values;
} TendencyData;

void TendencyData_Init(TendencyData * const td);
void TendencyData_AddValue(TendencyData * const td, const int8_t value);
uint8_t TendencyData_GetNormalizedValues(const TendencyData * const td, const uint8_t max, uint8_t* output);
bool TendencyData_HasValues(const TendencyData * const td);
int8_t TendencyData_GetLastValue(const TendencyData * const td);
void TendencyData_Fill(TendencyData * const td, int8_t value);

#endif	/* TENDENCYDATA_H */

