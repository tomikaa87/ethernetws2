#include "WeatherDataCollector.h"
#include "lib/Sample.h"
#include "lib/TendencyData.h"
#include "sys/settings.h"
#include "sys/tick.h"
#include "net/wdata.h"

#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>

TendencyData g_temperatureTendencyData;
TendencyData g_pressureTendencyData;
uint32_t g_tendencyDataLastUpdated = 0;

Sample g_temperatureSample;
Sample g_pressureSample;

static bool updateData();
static int8_t calculateNormalizedPressureValue(uint16_t pressure);


void WeatherDataCollector_Init()
{
    SampleInit(g_temperatureSample);
    SampleInit(g_pressureSample);

    TendencyData_Init(&g_temperatureTendencyData);
    TendencyData_Init(&g_pressureTendencyData);
}


WDCTaskResult WeatherDataCollector_Task()
{
    if (updateData())
        return WDC_UPDATE_NEEDED;

    return WDC_OK;
}


static bool updateData()
{
    if (!g_hasWeatherData)
        return false;

    if (g_tendencyDataLastUpdated + (uint32_t)settings.Weather.TendencyUpdateIntervalMinutes * 60u > Tick_GetElapsedSeconds()
            && TendencyData_HasValues(&g_temperatureTendencyData))
        return false;

    printf("Updating tendency data at %lu\r\n", Tick_GetElapsedSeconds());

    if (g_temperatureSample.count > 0) {
        int8_t average = g_temperatureSample.sum / g_temperatureSample.count;

        if (TendencyData_HasValues(&g_temperatureTendencyData))
            TendencyData_AddValue(&g_temperatureTendencyData, average);
        else
            TendencyData_Fill(&g_temperatureTendencyData, average);

        printf("Value added to temperature tendency data: %d\r\n", average);

        SampleInit(g_temperatureSample);
    }

    if (g_pressureSample.count > 0) {
        int8_t average = g_pressureSample.sum / g_pressureSample.count;

        if (TendencyData_HasValues(&g_pressureTendencyData))
            TendencyData_AddValue(&g_pressureTendencyData, average);
        else
            TendencyData_Fill(&g_pressureTendencyData, average);

        printf("Value added to pressure tendency data: %d\r\n", average);

        SampleInit(g_pressureSample);
    }

    g_tendencyDataLastUpdated = Tick_GetElapsedSeconds();

    return true;
}


static int8_t calculateNormalizedPressureValue(uint16_t pressure)
{
    // Represent the pressure range 872..1127 as a number between -128..127

    if (pressure < 872)
        return -128;

    if (pressure > 1127)
        return 127;

    return pressure - 1000;
}


void WeatherDataCollector_DoSampling()
{
    TWDataNumber val = WData_GetNumber(WD_C_TEMPERATURE, 0);
    SampleAdd(g_temperatureSample, val.Int8);

    val = WData_GetNumber(WD_C_PRESSURE, 0);
    int8_t normalized = calculateNormalizedPressureValue(val.UInt16);
    SampleAdd(g_pressureSample, normalized);
}