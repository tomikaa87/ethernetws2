/*
 * File:   Sample.h
 * Author: tomikaa
 *
 * Created on 2018. december 23., 16:16
 */

#ifndef SAMPLE_H
#define	SAMPLE_H

#include <stdint.h>

typedef struct {
    int16_t sum;
    int16_t count;
} Sample;

#define SampleInit(SAMPLE) { SAMPLE.sum = 0; SAMPLE.count = 0; }
#define SampleAdd(SAMPLE, VALUE) { SAMPLE.sum += VALUE; ++SAMPLE.count; }

#endif	/* SAMPLE_H */

