/* 
 * File:   config.h
 * Author: Tam�s
 *
 * Created on 2015. j�nius 28., 18:34
 */

#ifndef CONFIG_H
#define	CONFIG_H

#define FW_VER_MAJOR 1
#define FW_VER_MINOR 0
#define FW_VER_PATCH 2

// Enables network configuration options. Disable to save space.
// #define ENABLE_NETWORK_CONFIG

#endif	/* CONFIG_H */

