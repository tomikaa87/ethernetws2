/**
 * Wireless outdoor thermometer transmitter firmware
 * Global configuration header
 *
 * @file Config.h
 * @date 2012-04-04
 * @author Tamas Karpati
 */

#ifndef CONFIG_H
#define CONFIG_H

#include <htc.h>

// PIC configuration
#define _XTAL_FREQ                          2000000ul

// Wireless transmitter configuration
#define RADIO_POWER_PIN                     RC3
#define RADIO_POWER_PIN_TRIS                TRISC3

// Battery voltage measurement parameters
#define BATT_ADC_CAL_VOLTAGE                4.3
#define BATT_ADC_CAL_VALUE                  485

// Serial port configuration
#define UART_TX_PIN                         LATC4
#define UART_TX_PIN_TRIS                    TRISC4
#define UART_BAUD                           600
#define UART_HIGH_SPEED                     0

// BMP085 configuration
#define BMP085_POWER_PIN                    LATC2
#define BMP085_POWER_PIN_TRIS               TRISC2
#define BMP085_EOC_PIN                      RA2
#define BMP085_EOC_TRIS                     TRISA2
#define BMP085_SDA                          LATC1
#define BMP085_SCL                          LATC0
#define BMP085_SCL_TRIS                     TRISC0
#define BMP085_SDA_TRIS                     TRISC1
#define BMP085_SSPCON1_BITS                 SSP1CON1bits
#define BMP085_SSPCON2_BITS                 SSP1CON2bits
#define BMP085_SSPSTAT_BITS                 SSP1STATbits
#define BMP085_SSPADD                       SSP1ADD
#define BMP085_SSPBUF                       SSP1BUF

// DHT22 configuration
#define DHT_DATA_IN                         RA0
#define DHT_DATA_OUT                        LATA0
#define DHT_DATA_TRIS                       TRISA0

#define DHT_USE_POWER_PIN
#ifdef DHT_USE_POWER_PIN
    #define DHT_POWER_PIN                   LATA1
    #define DHT_POWER_PIN_TRIS              TRISA1
#endif

#define DHT_USE_WEAK_PULL_UP
#ifdef DHT_USE_WEAK_PULL_UP
#define DHT_WEAK_PULL_UP                    WPUA0
#endif

#endif // CONFIG_H