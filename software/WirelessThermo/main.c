/**
 * Wireless outdoor thermometer transmitter firmware
 *
 * @file main.c
 * @date 2012-01-03
 * @author Tamas Karpati
 */

#include <htc.h>

#include "Config.h"
#include "DHT1122.h"
#include "BMP085.h"

/*** PIC configuration ********************************************************/

__CONFIG(FOSC_INTOSC & WDTE_SWDTEN & PWRTE_ON & MCLRE_OFF & CP_ON & CPD_OFF &
        BOREN_OFF & CLKOUTEN_OFF & IESO_OFF & FCMEN_OFF);
__CONFIG(WRT_ALL & PLLEN_OFF & STVREN_ON & BORV_LO & LVP_OFF);

/*** UART speed configuration *************************************************/

#ifndef _XTAL_FREQ
    #error "_XTAL_FREQ must be set"
#endif

#if (UART_HIGH_SPEED == 1)
    #define UART_DIVIDER        16
#else
    #define UART_DIVIDER        64
#endif
#define UART_SPBRG (unsigned char)(_XTAL_FREQ / UART_BAUD / UART_DIVIDER - 1)
#define UART_REAL_BAUD (unsigned long)(_XTAL_FREQ / (UART_DIVIDER * (UART_SPBRG + 1)))
#define UART_BAUD_ERROR \
    (signed char)((((float)UART_REAL_BAUD - (float)UART_BAUD) / (float)UART_BAUD) * 100.0)

char Warning_UartErrorTooHigh[UART_BAUD_ERROR <= 3];

void setupUart()
{
    // Workaround to avoid compiler warnings about unused variables
    Warning_UartErrorTooHigh[0] = 0;
    while (Warning_UartErrorTooHigh[0]) continue;

    // Setup UART module
    TX9 = 0;
    SYNC = 0;
    BRGH = UART_HIGH_SPEED;
    SPBRG = UART_SPBRG;
    SPBRGH = 0;
    SPEN = 0;
    CREN = 0;
    SREN = 0;
    TXEN = 0;
    UART_TX_PIN_TRIS = 0;
    //TRISC5 = 1;
}

#define UART_ON()               { SPEN = 1; TXEN = 1; }
#define UART_OFF()              { SPEN = 0; TXEN = 0; }

/*** UART routines ************************************************************/

void putch(char c)
{
    while (!TRMT)
        continue;
    TXREG = c;
    while (!TRMT)
        continue;
}

/*** Utility macros ***********************************************************/

#define DEBUG_ENABLED

#ifdef DEBUG_ENABLED
    #include <stdio.h>
    #define _debug printf
#else
    #define _debug (void)
#endif

/*** CRC16 ********************************************************************/

#define CRC16_POLYNOMIAL    0x1021

static unsigned int CRC16_runningValue;

void CRC16_reset()
{
    CRC16_runningValue = 0;
}

unsigned int CRC16_value()
{
    return CRC16_runningValue;
}

void CRC16_byte(unsigned char ch)
{
    unsigned char i;

    CRC16_runningValue ^= ch;
    i = 8;

    do
    {
        if (CRC16_runningValue & 0x8000)
            CRC16_runningValue <<= 1;
        else
        {
            CRC16_runningValue <<= 1;
            CRC16_runningValue ^= CRC16_POLYNOMIAL;
        }
    }
    while (--i);
}

/*** Wireless transmitter macros **********************************************/

#define RADIO_POWER_ON()    (RADIO_POWER_PIN = 1)
#define RADIO_POWER_OFF()   (RADIO_POWER_PIN = 0)

/*** Data packet settings *****************************************************/

#define PACKET_PADDING    2
#define PACKET_COUNT      2

/*** Manchester ***************************************************************/

void MAN_putch(unsigned char c)
{
    unsigned char i, nibble, tmp;

    nibble = 0;
    tmp = c;
    for (i = 0; i < 8; i++)
    {
        if (tmp & 0x80)
            nibble |= 2; // 10
        else
            nibble |= 1; // 01
        tmp <<= 1;
        if (i == 3 || i == 7)
        {
            putch(nibble);
            nibble = 0;
        }
        else
            nibble <<= 2;
    }
}

void MAN_sendSS()
{
    unsigned char i;

    for (i = 0; i < 4; i++)
        putch(0xF0);
}

/*** Battery management *******************************************************/

unsigned int measureVddMilliVolts()
{
    union {
        struct {
            unsigned char l;
            unsigned char h;
        };
        unsigned int u;
    } result;

    // Setup Fixed Voltage Reference, 1.024 Volts for ADC
    FVRCON = 0b10000010;
    // Wait until FVR stabilizes the output voltage
    while (!FVRRDY)
        continue;

    // Setup ADC, use FVR as input, VDD as Vref+, read from AN2, ADFM = 1
    ADCON0 = 0b01111101;
    ADCON1 = 0b11110000;

    // Start conversion and wait for it to finish
    GO = 1;
    while (GO)
        continue;

    // Read conversion result
    result.h = ADRESH;
    result.l = ADRESL;

    // Calculate VDD
    float vdd = (BATT_ADC_CAL_VOLTAGE * (float)BATT_ADC_CAL_VALUE) /
            (float)(result.u);
    unsigned int vddMilliVolts = (unsigned int)(vdd * 100.0) * 10;

    // Disable ADC and FVR
    ADON = 0;
    FVRCON = 0;
    
    return vddMilliVolts;
}

/*** Main *********************************************************************/

typedef union {
    struct {
        unsigned char lo;
        unsigned char hi;
    };
    unsigned char data[2];
    unsigned int ui;
    signed int si;
} Word;

typedef union {
    struct {
        unsigned char lo;
        unsigned char mid_lo;
        unsigned char mid_hi;
        unsigned char hi;
    };
    unsigned char data[4];
    unsigned long ul;
    signed long sl;
} Long;

void main()
{
    OSCCON = 0b01100000;
    SWDTEN = 0;

    CM1CON0 = 0;
    CM2CON0 = 0;
    ANSELC = 0;
    ANSELA = 0;

    // Default sleep interval: 32 seconds
    WDTCON = 0b00011110;

    TRISA = 0;
    TRISC = 0;
    LATA = 0;
    LATC = 0;

    setupUart();

    while (1) {
        unsigned char i, packet[17];

        SWDTEN = 0;

        /*
         * Packet format
         * [0xFA 0xAF][VOLTAGE][DHT_TEMP][DHT_RH][BMP085_TEMP][BMP085_PRES][ERROR_CODE][CHECKSUM]
         *  __ 2 B __  _ 2 B _  __ 2 B _  _2 B__  __ 2 B ____  __ 4 B ____  __ 1 B ___  __ 2 B _
         *
         * Total size: 17 Bytes
         */

        // Initialize the packet buffer
        for (i = 0; i < sizeof (packet); i++)
            packet[i] = 0;

        packet[0] = 0xFA;
        packet[1] = 0xAF;

        CRC16_reset();

        // Measure Vdd
        int vdd = measureVddMilliVolts();
        if (vdd < 3000) {
            // Send battery low signal and increment sleep time
            RADIO_POWER_PIN = 1;
            UART_ON();

            // Battery voltage (2-3)
            Word w;
            w.ui = vdd;
            packet[2] = w.hi;
            CRC16_byte(w.hi);
            packet[3] = w.lo;
            CRC16_byte(w.lo);

            for (i = 4; i < 15; i++)
                CRC16_byte(0);

            // Checksum (15-16)
            w.ui = CRC16_value();
            packet[15] = w.hi;
            packet[16] = w.lo;

            char j;
            for (j = 0; j < PACKET_COUNT; j++) {
                MAN_sendSS();
                for (i = 0; i < sizeof (packet); i++) {
                    MAN_putch(packet[i]);
                }
                MAN_sendSS();
            }

            UART_OFF();
            RADIO_POWER_PIN = 0;

            // Sleep for 128 seconds
            WDTCON = 0b00100010;
            SWDTEN = 1;
            asm("sleep");

            // Skip reading the sensors
            continue;
        } else if (vdd < 3200) {
            // Low battery, increment sleep interval to 64 seconds
            WDTCON = 0b00100000;
        }

        // Read DHT22
        DHT_power(1);
        char dhtError = DHT_readSensor();
        i = 0;
        while (dhtError == 1 && i++ < 250) {
            __delay_ms(10);
            dhtError = DHT_readSensor();
        }
        DHT_power(0);

        // Read BMP085
        BMP085_power(1);
        __delay_ms(80);
        BMP085_init();
        BMP085_Result bmpRes = BMP085_readSensor(1);
        BMP085_power(0);

        Word w;
        Long l;

        // Battery voltage (2-3)
        w.ui = vdd;
        packet[2] = w.hi;
        CRC16_byte(w.hi);
        packet[3] = w.lo;
        CRC16_byte(w.lo);

        // DHT temperature (4-5)
        w.si = DHT_temperatureMul10() * 10;
        packet[4] = w.hi;
        CRC16_byte(w.hi);
        packet[5] = w.lo;
        CRC16_byte(w.lo);

        // DHT relaitve humidity (6-7)
        w.ui = DHT_relativeHumidityMul10() * 10;
        packet[6] = w.hi;
        CRC16_byte(w.hi);
        packet[7] = w.lo;
        CRC16_byte(w.lo);

        // BMP085 temperature (8-9)
        w.si = bmpRes.temperature;
        packet[8] = w.hi;
        CRC16_byte(w.hi);
        packet[9] = w.lo;
        CRC16_byte(w.lo);

        // BMP085 pressure (10-13)
        l.ul = bmpRes.pressure;
        packet[10] = l.hi;
        CRC16_byte(l.hi);
        packet[11] = l.mid_hi;
        CRC16_byte(l.mid_hi);
        packet[12] = l.mid_lo;
        CRC16_byte(l.mid_lo);
        packet[13] = l.lo;
        CRC16_byte(l.lo);

        // Error code (14)
        packet[14] = dhtError;
        CRC16_byte(dhtError);


        // Checksum (15-16)
        w.ui = CRC16_value();
        packet[15] = w.hi;
        packet[16] = w.lo;

        // Transmit data
        RADIO_POWER_PIN = 1;
        UART_ON();

        char j;
        for (j = 0; j < PACKET_COUNT; j++) {
            MAN_sendSS();
            for (i = 0; i < sizeof (packet); i++) {
                MAN_putch(packet[i]);
            }
            MAN_sendSS();
        }

#if 0
        BMP085_Data *bmpData = BMP085_getData();
        printf("BMP085 clibration parameters:\r\n");
        printf("AC1: %d\r\nAC2: %d\r\nAC3: %d\r\nAC4: %u\r\nAC5: %u\r\n"
                "AC6: %u\r\nB1: %d\r\nB2: %d\r\nMB: %d\r\nMC: %d\r\nMD: %d\r\n",
                bmpData->cp->ac1, bmpData->cp->ac2, bmpData->cp->ac3,
                bmpData->cp->ac4, bmpData->cp->ac5, bmpData->cp->ac6,
                bmpData->cp->b1, bmpData->cp->b2, bmpData->cp->mb,
                bmpData->cp->mc, bmpData->cp->md);
        printf("BMP085 raw readings:\r\n");
        printf("UT: %u\r\nUP: %lu\r\n", bmpRes.ut, bmpRes.up);
        printf("BMP085 compensated readings:\r\n");
        printf("T: %d\r\nP: %lu\r\n", bmpRes.temperature, bmpRes.pressure);
#endif

        UART_OFF();
        RADIO_POWER_PIN = 0;

        SWDTEN = 1;
        asm("sleep");
    }
}
