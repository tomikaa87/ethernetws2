/**
 * Wireless outdoor thermometer transmitter firmware
 * DHT11/22 Digital humidity and temperature sensor driver
 *
 * @file DHT1122.c
 * @date 2012-02-22
 * @author Tamas Karpati
 */

#include "DHT1122.h"
#include "Config.h"

#include <htc.h>

/*** Sensor type configuration ************************************************/

// Uncomment this line to use this driver with DHT22, instead of DHT11
#define DHT_SENSOR_DHT22

/*** DON'T TOUCH ANYTHING BELOW THIS LINE *************************************/

#ifdef DHT_SENSOR_DHT22
    #define DHT_INIT_DELAY               1500
#else
    #define DHT_INIT_DELAY               100
#endif

#ifdef DHT_SENSOR_DHT22
    #define DHT_REQ_DELAY                1
#else
    #define DHT_REQ_DELAY                30
#endif

/*** Global variables *********************************************************/

unsigned char dhtData[5];

/*** Utility functions ********************************************************/

void delayMs(unsigned int ms)
{
    unsigned char i;
    do {
        i = 4;
        do {
            __delay_us(250);
        } while (--i);
    } while (--ms);
}

/*** API function implementations *********************************************/

void DHT_power(unsigned char on)
{
    if (on) {
        DHT_POWER_PIN_TRIS = 0;
        DHT_POWER_PIN = 1;
    } else {
        DHT_POWER_PIN = 0;
    }
}

char DHT_readSensor()
{
    unsigned char rawData[40], i, bitIndex;
    int bitThreshold, bitMin, bitMax;

    // Power up the sensor
//#ifdef DHT_USE_POWER_PIN
//    DHT_POWER_PIN_TRIS = 0;
//    DHT_POWER_PIN = 1;

     // Wait for the sensor to initialize
//    delayMs(DHT_INIT_DELAY);
//#endif

#ifdef DHT_USE_WEAK_PULL_UP
    DHT_WEAK_PULL_UP = 1;
#endif

    // Send 'Request data' signal, wake up the sensor
    DHT_DATA_OUT = 0;
    DHT_DATA_TRIS = 0;
    delayMs(DHT_REQ_DELAY);
    DHT_DATA_TRIS = 1;

    // Skip first two edges of response
    for (i = 0; i < 2; i++) {
        unsigned char counter;
        counter = 0;
        while (DHT_DATA_IN == 0 && counter++ < 254)
            continue;
        if (counter >= 254)
            return DHT_TIMEOUT_ERROR;
        counter = 0;
        while (DHT_DATA_IN == 1 && counter++ < 254)
            continue;
        if (counter >= 254)
            return DHT_TIMEOUT_ERROR;
    }

    // Read 40 data bits from the sensor
    for (i = 0; i < 40; i++) {
        unsigned char counter;
        // l->h
        counter = 0;
        while (DHT_DATA_IN == 0 && counter++ < 254)
            continue;
        if (counter >= 254)
            return DHT_TIMEOUT_ERROR;
        // h->l
        counter = 0;
        while (DHT_DATA_IN == 1 && counter++ < 254)
            continue;
        rawData[i] = counter;
    }

    // Power down the sensor
//#ifdef DHT_USE_POWER_PIN
//    DHT_POWER_PIN = 0;
//#endif

#ifdef DHT_USE_WEAK_PULL_UP
    DHT_WEAK_PULL_UP = 0;
#endif

//    printf("rawData:");
//    for (i = 0; i < 40; i++) {
//        printf(" %u", rawData[i]);
//    }
//    printf("\r\n");

    // Calculate bit threshold (counter value for '1' bits)
    bitMin = 255;
    bitMax = 0;
    for (i = 0; i < 40; i++) {
        if (rawData[i] < bitMin)
            bitMin = rawData[i];
        if (rawData[i] > bitMax)
            bitMax = rawData[i];
    }

    bitThreshold = (bitMin + bitMax) / 2;
//    printf("bitMin = %d, bitMax = %d, bitThreshold = %d\r\n",
//            bitMin, bitMax, bitThreshold);

    if (bitMin == bitMax || bitThreshold == 0)
        return DHT_BIT_THRESHOLD_ERROR;

//    printf("DHT data:");

    // Convert raw data
    for (i = 0; i < 5; i++) {
        unsigned char byte = 0;
        for (bitIndex = 0; bitIndex < 8; bitIndex++) {
            if (rawData[i * 8 + bitIndex] > bitThreshold)
                byte |= (1 << (7 - bitIndex));
        }
        dhtData[i] = byte;
//        printf(" %02X", byte);
    }

//    printf("\r\n");

    return DHT_OK;
}

signed int DHT_relativeHumidityMul10()
{
    union {
        struct {
            unsigned char lo;
            unsigned char hi;
        };
        signed int si;
    } word;

    word.lo = dhtData[1];
    word.hi = dhtData[0];

    return word.si;
}

signed int DHT_temperatureMul10()
{
    union {
        struct {
            unsigned char lo;
            unsigned char hi;
        };
        signed int si;
    } word;

    word.lo = dhtData[3];
    word.hi = dhtData[2] & 0x7F;
    // Handle sign bit
    if (dhtData[2] & 0x80)
        word.si *= -1;

    return word.si;
}
