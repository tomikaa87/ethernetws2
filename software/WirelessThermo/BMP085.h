/**
 * Wireless outdoor thermometer transmitter firmware
 * Bosch BMP085 Digital barometric pressure sensor driver
 *
 * @file BMP085.h
 * @date 2012-04-04
 * @author Tamas Karpati
 */

#ifndef BMP085_H
#define BMP085_H

#include <htc.h>

typedef union {
    struct {
        signed int ac1;
        signed int ac2;
        signed int ac3;
        unsigned int ac4;
        unsigned int ac5;
        unsigned int ac6;
        signed int b1;
        signed int b2;
        signed int mb;
        signed int mc;
        signed int md;
    };
    unsigned char data[22];
} BMP085_CalibParam;

typedef struct {
    BMP085_CalibParam *cp;
    unsigned char chipId;
} BMP085_Data;

typedef struct {
    unsigned int ut;
    unsigned long up;
    signed int temperature;
    long pressure;
} BMP085_Result;

void BMP085_power(unsigned char on);
void BMP085_init();

BMP085_Result BMP085_readSensor(unsigned char oss);

BMP085_Data *BMP085_getData();

#endif // BMP085_H