/**
 * Wireless outdoor thermometer transmitter firmware
 * Bosch BMP085 Digital barometric pressure sensor driver
 *
 * @file BMP085.c
 * @date 2012-04-04
 * @author Tamas Karpati
 */

#include "BMP085.h"
#include "Config.h"

/*** Constants ****************************************************************/

#define BMP085_ADDR_WRITE       0xEE    // Write address
#define BMP085_ADDR_READ        0xEF    // Read address

#define BMP085_REG_MSB          0xF6    // ADC MSG register
#define BMP085_REG_LSB          0xF7    // ADC LSB register
#define BMP085_REG_XLSB         0xF8    // ADC XLSB register
#define BMP085_REG_FIRST        0xAA    // EEPROM first register
#define BMP085_REG_LAST         0xBF    // EEPROM last register
#define BMP085_REG_CTRL         0xF4    // Control register
#define BMP085_REG_CHIP_ID      0xD0    // Chip ID register
#define BMP085_REG_VER          0xD1    // Chip version register
#define BMP085_REG_SR           0xE0    // Soft Reset register

#define BMP085_MES_TEMP         0x2E    // Temperature measurement
#define BMP085_MES_PRES0        0x34    // Pressure measurement, OSRS = 0
#define BMP085_MES_PRES1        0x74    // Pressure measurement, OSRS = 1
#define BMP085_MES_PRES2        0xB4    // Pressure measurement, OSRS = 2
#define BMP085_MES_PRES3        0xF4    // Pressure measurement, OSRS = 3

#define BMP085_waitEoc()        while(!BMP085_EOC_PIN) continue

/*** I2C routines *************************************************************/

void I2C_init()
{
    BMP085_SCL_TRIS = 1;
    BMP085_SDA_TRIS = 1;

    BMP085_SSPSTAT_BITS.SMP = 1;        // Disable slew rate control
    BMP085_SSPCON1_BITS.SSPEN = 1;
    BMP085_SSPCON1_BITS.SSPM3 = 1;      // I2C master
    BMP085_SSPADD = 9;
}

void I2C_start()
{
    BMP085_SSPCON2_BITS.SEN = 1;
    while (BMP085_SSPCON2_BITS.SEN)
        continue;
}

void I2C_stop()
{
    BMP085_SSPCON2_BITS.PEN = 1;
    while (BMP085_SSPCON2_BITS.PEN)
        continue;
}

void I2C_restart()
{
    BMP085_SSPCON2_BITS.RSEN = 1;
    while (BMP085_SSPCON2_BITS.RSEN)
        continue;
}

void I2C_ack()
{
    BMP085_SSPCON2_BITS.ACKDT = 0;
    BMP085_SSPCON2_BITS.ACKEN = 1;
    while (BMP085_SSPCON2_BITS.ACKEN)
        continue;
}

void I2C_nak()
{
    BMP085_SSPCON2_BITS.ACKDT = 1;
    BMP085_SSPCON2_BITS.ACKEN = 1;
    while (BMP085_SSPCON2_BITS.ACKEN)
        continue;
}

void I2C_wait()
{
    while (BMP085_SSPCON2_BITS.ACKEN || BMP085_SSPCON2_BITS.RCEN ||
            BMP085_SSPCON2_BITS.PEN || BMP085_SSPCON2_BITS.RSEN ||
            BMP085_SSPCON2_BITS.SEN || BMP085_SSPSTAT_BITS.R_nW)
        continue;
}

void I2C_send(unsigned char data)
{
    BMP085_SSPBUF = data;
    while (BMP085_SSPSTAT_BITS.BF)
        continue;
    I2C_wait();
}

unsigned char I2C_read()
{
    unsigned char data;

    BMP085_SSPCON2_BITS.RCEN = 1;
    while (!BMP085_SSPSTAT_BITS.BF)
        continue;
    data = BMP085_SSPBUF;
    I2C_wait();

    return data;
}

/*** BMP085 routines **********************************************************/

BMP085_CalibParam calibParam;
BMP085_Data sensorData;

void BMP085_byteSwap(BMP085_CalibParam *cp)
{
    unsigned char i = 0, temp;

    while (i < 22) {
        temp = cp->data[i];
        cp->data[i] = cp->data[i + 1];
        cp->data[i + 1] = temp;
        i += 2;
    }
}

void BMP085_startMeasurement(unsigned char type)
{
    // Start I2C sequence
    I2C_start();
    // Send I2C address
    I2C_send(BMP085_ADDR_WRITE);
    // Send register address
    I2C_send(BMP085_REG_CTRL);
    // Send Control Register data
    I2C_send(type);
    // Stop transmission
    I2C_stop();
}

void BMP085_readReg(unsigned char addr, unsigned char *buf,
        unsigned char length)
{
    unsigned char i;

    // Start I2C sequence
    I2C_start();
    // Send I2C address
    I2C_send(BMP085_ADDR_WRITE);
    // Send register address
    I2C_send(addr);
    // Restart
    I2C_restart();
    // Send I2C address
    I2C_send(BMP085_ADDR_READ);
    // Read 'length' number of bytes
    for (i = 0; i < length; i++) {
        buf[i] = I2C_read();
        if (i == length - 1)
            I2C_nak();
        else
            I2C_ack();
    }
    // Stop transmission
    I2C_stop();
}

void BMP085_readCalibParam(BMP085_CalibParam *cp)
{
    unsigned char i;

    // Start I2C sequence
    I2C_start();
    // Send I2C address
    I2C_send(BMP085_ADDR_WRITE);
    // Send register address
    I2C_send(BMP085_REG_FIRST);
    // Restart
    I2C_restart();
    // Send I2C address
    I2C_send(BMP085_ADDR_READ);
    // Read 22 bytes
    for (i = 0; i < 22; i++) {
        cp->data[i] = I2C_read();
        if (i < 21)
            I2C_ack();
        else
            I2C_nak();
    }
    // Stop transmission
    I2C_stop();
}

/*** API functions ************************************************************/

void BMP085_power(unsigned char on)
{
    BMP085_POWER_PIN_TRIS = 0;
    BMP085_POWER_PIN = on > 0 ? 1 : 0;
    if (!on) {
        SSP1CON1bits.SSPEN = 0;

        BMP085_SCL_TRIS = 0;
        BMP085_SDA_TRIS = 0;
        BMP085_SCL = 0;
        BMP085_SDA = 0;
    }
}

void BMP085_init()
{
    unsigned char id;

    // Initialize EOC pin
    BMP085_EOC_TRIS = 1;
    // Initialize I2C bus
    I2C_init();
    // Read Calibration Parameters from the sensor
    BMP085_readCalibParam(&calibParam);
    // Swap bytes because HI-TECH C is little-endian
    BMP085_byteSwap(&calibParam);
    // Read Chip ID
    BMP085_readReg(BMP085_REG_CHIP_ID, &id, 1);
    // Setup sensor data structure
    sensorData.cp = &calibParam;
    sensorData.chipId = id;
}

BMP085_Result BMP085_readSensor(unsigned char oss)
{
    double utf, upf, ac1, ac2, ac3, ac4, ac5, ac6, b1, b2, mc, md, ossf;
    double x1, x2, x3, b3, b4, b5, b6, b7, tf, pf;
    unsigned char mesType, data[3];
    unsigned int ut;
    unsigned long up;
    BMP085_Result result;

    // Select pressure measurement type and calculate OSS value
    switch (oss) {
        case 0:
            mesType = BMP085_MES_PRES0;
            ossf = 1.0;
            break;
        case 1:
            mesType = BMP085_MES_PRES1;
            ossf = 2.0;
            break;
        case 2:
            mesType = BMP085_MES_PRES2;
            ossf = 4.0;
            break;
        case 3:
            mesType = BMP085_MES_PRES3;
            ossf = 8.0;
            break;
        default:
            return result;
    }

    // Start temperature measurement
    BMP085_startMeasurement(BMP085_MES_TEMP);
    // Wait for operation to finish
    BMP085_waitEoc();
    // Read ADC value
    BMP085_readReg(BMP085_REG_MSB, data, 2);
    // Calculate uncompensated temperature
    ut = (data[0] << 8) + data[1];
    result.ut = ut;
    // Start pressure measurement
    BMP085_startMeasurement(mesType);
    // Wait for operation to finish 
    BMP085_waitEoc();
    // Read ADC value
    BMP085_readReg(BMP085_REG_MSB, data, 3);
//    up = (((long)data[0] << 16) + (data[1] << 8) + data[2]) >> (8 - oss);
    up = (((long)data[0] << 16) + ((long)data[1] << 8) + (long)data[2]) >> (8 - oss);
    result.up = up;
    
    // Convert uncompensated values
    utf = (double)ut;
    upf = (double)up;

    // Convert calibration parameters
    ac1 = (double)calibParam.ac1;
    ac2 = (double)calibParam.ac2;
    ac3 = (double)calibParam.ac3;
    ac4 = (double)calibParam.ac4;
    ac5 = (double)calibParam.ac5;
    ac6 = (double)calibParam.ac6;
    b1 = (double)calibParam.b1;
    b2 = (double)calibParam.b2;
    mc = (double)calibParam.mc;
    md = (double)calibParam.md;

    // Calculate compensated temperature
    x1 = (utf - ac6) * ac5 / 32768.0;
    x2 = mc * 2048.0 / (x1 + md);
    b5 = x1 + x2;
    tf = (b5 + 8.0) / 16.0;

    // Calculate compensated pressure
    b6 = b5 - 4000.0;

    x1 = (b2 * (b6 * b6 / 4096.0)) / 2048.0;
    x2 = ac2 * b6 / 2048.0;
    x3 = x1 + x2;
    b3 = ((ac1 * 4.0 + x3) * ossf + 2.0) / 4.0;

    x1 = ac3 * b6 / 8192.0;
    x2 = (b1 * (b6 * b6 / 4096.0)) / 65536.0;
    x3 = ((x1 + x2) + 2.0) / 4.0;
    b4 = ac4 * (x3 + 32768.0) / 32768.0;

    b7 = (upf - b3) * (50000.0 / ossf);
    if (b7 < 2147483648.0)
        pf = (b7 * 2.0) / b4;
    else
        pf = (b7 / b4) * 2.0;

    x1 = (((pf / 256.0) * (pf / 256.0)) * 3038.0) / 65536.0;
    x2 = (-7357.0 * pf) / 65536.0;
    pf = pf + (x1 + x2 + 3791.0) / 16.0;

    // Convert results
    result.temperature = (signed int)tf;
    result.pressure = (unsigned long)pf;

    return result;
}

BMP085_Data *BMP085_getData()
{
    return &sensorData;
}